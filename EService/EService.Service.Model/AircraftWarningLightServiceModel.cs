﻿using System;

using System.Collections.Generic;
using System.Security.Permissions;

namespace EService.Service.Model
{
    public class AircraftWarningLightServiceModel : EServiceBaseModal
    {
        public AircraftWarningLightServiceModel()
        {
        }
        public int? CurrentIndex { get; set; }
        public DateTime ApplicationDate { get; set; }
        public string ProjectName { get; set; }
        public string ProjectLocation { get; set; }
        public string ProjectDescription { get; set; }

        #region "Applicant"
        public string ApplicantName { get; set; }
        public string ApplicantJobTitle { get; set; }
        public string ApplicantCompanyName { get; set; }
        public string ApplicantAddress { get; set; }
        public string ApplicantContactInfo { get; set; }
        public string ApplicantCellNo { get; set; }
        public string ApplicantTelNo { get; set; }
        public string ApplicantEmail { get; set; }
        public string ApplicantWebsite { get; set; }
        #endregion

        #region "Owner"
        public string OwnerName { get; set; }
        public string OwnerCompanyName { get; set; }
        public string OwnerAddress { get; set; }
        public string OwnerContactInfo { get; set; }
        public string OwnerCellNo { get; set; }
        public string OwnerTelNo { get; set; }
        public string OwnerEmail { get; set; }
        public string OwnerWebsite { get; set; }

        #endregion

        public int NoOfStructures { get; set; }
        public int NoOfObstacleLights { get; set; }
        public string TypesOfObstacleLights { get; set; }

        #region "Aircraft warning Light Project "
        public int ProjectTypeId { get; set; }
        public string ProjectTypeName { get; set; }
        public string ProjectTypeOtherName { get; set; }
        #endregion

        #region "Relation"
        public int ApplicantOwnerRelationShipId { get; set; }
        public string ApplicantOwnerRelationshipName { get; set; }
        public string ApplicantOwnerRelationshipOtherName { get; set; }
        #endregion
    }

}