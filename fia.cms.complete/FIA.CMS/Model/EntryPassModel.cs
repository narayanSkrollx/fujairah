﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FIA.CMS.Model
{
    public class EntryPassModel
    {
        [Required]
        [Display(Name = "Passport no.")]
        public string PassportNr { get; set; }

        [Required]
        [Display(Name = "Religion")]
        public string Religion { get; set; }

        [Required]
        [Display(Name = "Sect")]
        public string Sect { get; set; }

        [Display(Name = "Visa Type")]
        public string VisaType { get; set; }

        [Display(Name = "Emirates Id")]
        public string EmiratesId { get; set; }

        [Required]
        [Display(Name = "Area of Visit")]
        public string VisitArea { get; set; }

        [Required]
        [Display(Name = "Purpose of Visit")]
        public string VisitPurpose { get; set; }

        [Required]
        [Display(Name = "Date of Entry")]
        public DateTime EntryDate { get; set; }

        [Required]
        [Display(Name = "Date of Exit")]
        public DateTime ExitDate { get; set; }

        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Telephone (Off.)")]
        public string OfficePhone { get; set; }

        [Required]
        [Display(Name = "Telephone (Mob.)")]
        public string MobilePhone { get; set; }

        [Required]
        [Display(Name = "Fax")]
        public string Fax { get; set; }

        [Required]
        [Display(Name = "Attachment")]
        public List<EntryPassAttachments> Attachments { get; set; }

    }

    public class EntryPassVehicleModel : EntryPassModel
    {
        [Required]
        [Display(Name = "Vehicle’s Registration No.")]
        public string VehicleRegistrationNr { get; set; }

        [Required]
        [Display(Name = "Name of the Driver")]
        public string DriverName { get; set; }

        [Required]
        [Display(Name = "License No. of the Driver")]
        public string LicenseNr { get; set; }
    }

    public class EntryPassIndividualModel : EntryPassModel
    {
        [Required]
        [Display(Name = "Name of visitor")]
        public string VisitorName { get; set; }

        [Required]
        [Display(Name = "Nationality")]
        public string Nationality { get; set; }

        [Required]
        [Display(Name = "Title/Job")]
        public string TitleJob { get; set; }

        [Required]
        [Display(Name = "Name of the company")]
        public string CompanyName { get; set; }

        [Required]
        [Display(Name = "Address")]
        public string Address { get; set; }
    }

    public class EntryPassAttachments
    {
        public string Document { get; set; }
        [Display(Name = "Attachment")]
        public HttpPostedFileBase File { get; set; }
    }
}