﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using EService.Data.Entity;
using EService.Service.Model;

namespace EService.Web.Models
{
    public class UserRegisterViewModel : UserRegisterModel
    {
        public List<LookupItem> SecurityQuestions { get; set; }
        public List<LookupItem> CompanyCategories { get; set; }
        public List<LookupItem> Countries { get; set; }

        [Required(ErrorMessage = "Confirm Password is required.")]
        [Compare("PasswordHash", ErrorMessage = "Confirm Password does not match to Password.")]
        public string ConfirmPassword { get; set; }
    }
}