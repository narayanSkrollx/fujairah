using System;
using System.Collections.Generic;
using EService.Data.Entity;

namespace EService.Web.Models
{
    public class LandingPermissionRequestCreateViewModel
    {
        #region "Service Request"
        public int UserServiceRequestMasterId { get; set; }
        public int UserId { get; set; }
        public DateTime RequestDateTime { get; set; }
        public int ServiceId { get; set; }
        public int Status { get; set; }
        #endregion

        #region "Company Detail"
        public string AirlineName { get; set; } //required, defaults to username
        public string Email { get; set; }//Required, defaults to user email address
        public string Telephone { get; set; }//required
        public string Fac { get; set; }
        public string Address { get; set; }//multiline

        #endregion

        #region Operator Detail
        public string OperatorName { get; set; } //Required
        public string OperatorEmail { get; set; }//required
        public string OperatorTelephone { get; set; }//required
        public string Fax { get; set; }
        public string OperatorAddress { get; set; }
        #endregion

        #region "Flight Detail"
        public int FlightNatureOfOperationId { get; set; }//required
        public string AircraftRegistration { get; set; }//required
        public string TypeOfAircraft { get; set; }//required
        public int AirportLandingId { get; set; }//required
        /// <summary>
        /// Maximum Take off Weight (in Kg)
        /// required
        /// </summary>
        public decimal MTOW { get; set; }
        #endregion

        public string Remarks { get; set; }
        public LandingPermissionRequestTripViewModel TripAddModel { get; set; }
        public List<LandingPermissionRequestTripViewModel> LandingPermissionRequestTrips { get; set; }

        #region "Drop down list items"
        public List<LookupItem> FlightNatureOfOperations { get; set; } 
        public List<LookupItem> AirportLandings { get; set; } 
        #endregion

    }
}