using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class UserContactPersonConfiguration : EntityTypeConfiguration<UserContactPerson>
    {
        public UserContactPersonConfiguration()
        {
            ToTable("UserContactPerson");
            HasKey(o => o.UserId);

            Property(o => o.UserId).IsRequired();
            Property(o => o.ContactPersonName).IsOptional().HasMaxLength(255);
            Property(o => o.ContactPersonMobile).IsOptional().HasColumnType("bigint");
            Property(o => o.EmiratesId).IsOptional().HasMaxLength(100);
        }
    }

    public class ServiceRequestFlowStageDepartmentHistoryConfiguration :
        EntityTypeConfiguration<ServiceRequestFlowStageDepartmentHistory>
    {
        public ServiceRequestFlowStageDepartmentHistoryConfiguration()
        {
            ToTable("ServiceRequestFlowStageDepartmentHistory");

            HasKey(o => o.ServiceRequestFlowStageDepartmentHistoryId);
            Property(o => o.ServiceRequestFlowStageDepartmentHistoryId)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(o => o.ServiceRequestFlowStageDepartmentId).IsRequired();
            Property(o => o.ServiceRequestFlowStageId).IsRequired();
            Property(o => o.DepartmentId).IsRequired();
            Property(o => o.DepartmentName).IsRequired().HasMaxLength(255);
            Property(o => o.DepartmentUserId).IsOptional();
            Property(o => o.DepartmentUserName).IsOptional().HasMaxLength(255);
            Property(o => o.Status).IsRequired();

            HasRequired(o => o.ServiceRequestFlowStageDepartment).WithMany(o=>o.ServiceRequestFlowStageDepartmentHistories).HasForeignKey(o=>o.ServiceRequestFlowStageDepartmentId);
        }
    }
}