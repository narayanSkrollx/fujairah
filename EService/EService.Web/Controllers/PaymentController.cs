﻿using EService.Web.Controllers.Base;
using System.Web.Mvc;
using EService.Service.Logic;
using EService.Web.Models;
using System;
using com.ni.dp.util;
using System.Linq;
using EService.Web.Helper;
using EService.Data.Entity.Entity;
using EService.Service.Model;
using System.Diagnostics;
using EService.Service.Logic.Enum;

namespace EService.Web.Controllers
{
    public class PaymentController : ExternalUserBaseController
    {
        private PaymentKeyLogic paymentKeyLogic;
        private UserServiceRequestMasterService userServiceRequestMatser;

        public PaymentController()
        {
            paymentKeyLogic = new PaymentKeyLogic(serviceContext);
            userServiceRequestMatser = new UserServiceRequestMasterService(serviceContext);
        }

        public ActionResult Index()
        {
            try
            {
                var tempData = TempData[Constants.PaymentRequestMasterContentKey] as PaymentResponseServiceViewModel;
                if (tempData == null)
                {
                    return ErrorResult();
                }
                else
                {
                    TempData[Constants.PaymentRequestMasterContentKey] = tempData;
                    return View(tempData);
                    //invalid 
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return ErrorResult("Error while verifying service info for payment.");
        }

        [HttpPost]
        public JsonResult Confirm()
        {
            var result = new JsonResponseObject();
            try
            {
                var tempData = TempData[Constants.PaymentRequestMasterContentKey] as PaymentResponseServiceViewModel;
                if (tempData == null)
                {
                    result.IsSuccess = false;
                    result.Message = "Payment request cannot be verified, please go to initial stage and try again.";
                }
                else
                {
                    var paymentKey = paymentKeyLogic.Find(o => true);
                    var receipt = userServiceRequestMatser.AddReceiptBeforePayment(tempData, UserProfile.UserId);
                    var bmp = PrepareModel(paymentKey, tempData, receipt);
                    var strMessage = bmp.Calculate();
                    EncDec aesEncrypt = new EncDec();
                    var requestparams1 = aesEncrypt.Encrypt(paymentKey.EncryptionKey, strMessage);
                    var encContent = paymentKey.MerchantId + "||NI||" + requestparams1;
                    var url = paymentKey.Url;

                    result.Data = new
                    {
                        RequestParameter = encContent,
                        PostUrl = url
                    };
                    //save requesting
                    var serviceRequestInfo = userServiceRequestMatser.GetServiceRequest(tempData.UserServiceRequestMasterId);

                    result.IsSuccess = true;

                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while Confirming your payment";
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult SetCashPayment()
        {
            var result = new JsonResponseObject();
            try
            {
                var tempData = TempData[Constants.PaymentRequestMasterContentKey] as PaymentResponseServiceViewModel;
                if (tempData == null)
                {
                    result.IsSuccess = false;
                    result.Message = "Payment request cannot be verified, please go to initial stage and try again.";
                }
                else
                {
                    //save requesting
                    var serviceRequestInfo = userServiceRequestMatser.GetServiceRequest(tempData.UserServiceRequestMasterId);
                    serviceRequestInfo.PaymentMode = (int)PaymentMode.Manual;
                    serviceRequestInfo.Status = (int)ServiceFlowStateEnum.PaymentPending;
                    serviceContext.Commit();

                    result.Data = new
                    {
                        Url = Url.Action("Detail", "ServiceRequest", new { id = serviceRequestInfo.UserServiceRequestMasterId })
                    };
                    result.IsSuccess = true;

                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while Confirming your payment mode";
            }
            return Json(result);
        }

        private BitMapModel PrepareModel(PaymentKey paymentKeys, PaymentResponseServiceViewModel paymentinfo, UserServiceRequestReceipt receipt = null)
        {
            var bmp = new BitMapModel();
            bmp.fieldExistenceIndicatorTransaction.FirstOrDefault(k => k.Key == "merchantOrderNumber").Value = paymentinfo.RegistrationNo + (receipt != null ? "_" + receipt.UserServiceRequestReceiptId : "");
            bmp.fieldExistenceIndicatorTransaction.FirstOrDefault(k => k.Key == "currency").Value = "AED";
            bmp.fieldExistenceIndicatorTransaction.FirstOrDefault(k => k.Key == "amount").Value = (paymentinfo.Total).ToString();
            bmp.fieldExistenceIndicatorTransaction.FirstOrDefault(k => k.Key == "successUrl").Value = String.IsNullOrEmpty(paymentinfo.SuccessUrl) ? Url.Action("Success", "PaymentResponse", null, this.Request.Url.Scheme) : paymentinfo.SuccessUrl;
            bmp.fieldExistenceIndicatorTransaction.FirstOrDefault(k => k.Key == "failureUrl").Value = String.IsNullOrEmpty(paymentinfo.FailureUrl) ? Url.Action("Failure", "PaymentResponse", null, this.Request.Url.Scheme) : paymentinfo.FailureUrl;
            bmp.fieldExistenceIndicatorTransaction.FirstOrDefault(k => k.Key == "transactionType").Value = "01";
            bmp.fieldExistenceIndicatorTransaction.FirstOrDefault(k => k.Key == "transactionMode").Value = "INTERNET";//"INTERNET"
            bmp.fieldExistenceIndicatorTransaction.FirstOrDefault(k => k.Key == "payModeType").Value = "CC";
            bmp.fieldExistenceIndicatorPayment.FirstOrDefault(k => k.Key == "cardNumber").Value = "";
            bmp.fieldExistenceIndicatorPayment.FirstOrDefault(k => k.Key == "CVV").Value = "";
            bmp.fieldExistenceIndicatorPayment.FirstOrDefault(k => k.Key == "expMonth").Value = "";
            bmp.fieldExistenceIndicatorPayment.FirstOrDefault(k => k.Key == "expYear").Value = "";
            bmp.fieldExistenceIndicatorPayment.FirstOrDefault(k => k.Key == "cardType").Value = "";
            bmp.fieldExistenceIndicatorPayment.FirstOrDefault(k => k.Key == "cardHolderName").Value = "";
            bmp.fieldExistenceIndicatorPayment.FirstOrDefault(k => k.Key == "custMobileNumber").Value = "";
            bmp.fieldExistenceIndicatorPayment.FirstOrDefault(k => k.Key == "cardToken").Value = "";
            bmp.fieldExistenceIndicatorPayment.FirstOrDefault(k => k.Key == "OTP").Value = "";
            bmp.fieldExistenceIndicatorPayment.FirstOrDefault(k => k.Key == "paymentID").Value = "";
            bmp.fieldExistenceIndicatorBilling.FirstOrDefault(k => k.Key == "billToFirstName").Value = paymentinfo.FirstName;
            bmp.fieldExistenceIndicatorBilling.FirstOrDefault(k => k.Key == "billToLastName").Value = paymentinfo.LastName;
            bmp.fieldExistenceIndicatorBilling.FirstOrDefault(k => k.Key == "billToStreet1").Value = paymentinfo.UserStreet1;
            bmp.fieldExistenceIndicatorBilling.FirstOrDefault(k => k.Key == "billToStreet2").Value = paymentinfo.UserStreet2;
            bmp.fieldExistenceIndicatorBilling.FirstOrDefault(k => k.Key == "billToCity").Value = paymentinfo.City;
            bmp.fieldExistenceIndicatorBilling.FirstOrDefault(k => k.Key == "billToState").Value = paymentinfo.State;
            bmp.fieldExistenceIndicatorBilling.FirstOrDefault(k => k.Key == "billtoPostalCode").Value = "";
            bmp.fieldExistenceIndicatorBilling.FirstOrDefault(k => k.Key == "billToCountry").Value = paymentinfo.Country;
            bmp.fieldExistenceIndicatorBilling.FirstOrDefault(k => k.Key == "billToEmail").Value = paymentinfo.UserEmail;
            bmp.fieldExistenceIndicatorBilling.FirstOrDefault(k => k.Key == "billToPhoneNumber1").Value = paymentinfo.PhoneNumber;
            bmp.fieldExistenceIndicatorBilling.FirstOrDefault(k => k.Key == "billToPhoneNumber2").Value = "";
            bmp.fieldExistenceIndicatorBilling.FirstOrDefault(k => k.Key == "billToPhoneNumber3").Value = "";
            bmp.fieldExistenceIndicatorBilling.FirstOrDefault(k => k.Key == "billToMobileNumber").Value = "";
            bmp.fieldExistenceIndicatorPayment.FirstOrDefault(k => k.Key == "gatewayID").Value = "";
            bmp.fieldExistenceIndicatorOtherData.FirstOrDefault(k => k.Key == "custID").Value = "";
            bmp.fieldExistenceIndicatorShipping.FirstOrDefault(k => k.Key == "shipToFirstName").Value = "";
            bmp.fieldExistenceIndicatorShipping.FirstOrDefault(k => k.Key == "shipToLastName").Value = "";
            bmp.fieldExistenceIndicatorShipping.FirstOrDefault(k => k.Key == "shipToStreet1").Value = "";
            bmp.fieldExistenceIndicatorShipping.FirstOrDefault(k => k.Key == "shipToStreet2").Value = "";
            bmp.fieldExistenceIndicatorShipping.FirstOrDefault(k => k.Key == "shipToCity").Value = "";
            bmp.fieldExistenceIndicatorShipping.FirstOrDefault(k => k.Key == "shipToState").Value = "";
            bmp.fieldExistenceIndicatorShipping.FirstOrDefault(k => k.Key == "shipToPostalCode").Value = "";
            bmp.fieldExistenceIndicatorShipping.FirstOrDefault(k => k.Key == "shipToCountry").Value = "";
            bmp.fieldExistenceIndicatorShipping.FirstOrDefault(k => k.Key == "shipToPhoneNumber1").Value = "";
            bmp.fieldExistenceIndicatorShipping.FirstOrDefault(k => k.Key == "shipToPhoneNumber2").Value = "";
            bmp.fieldExistenceIndicatorShipping.FirstOrDefault(k => k.Key == "shipToPhoneNumber3").Value = "";
            bmp.fieldExistenceIndicatorShipping.FirstOrDefault(k => k.Key == "shipToMobileNumber").Value = "";
            bmp.fieldExistenceIndicatorOtherData.FirstOrDefault(k => k.Key == "transactionSource").Value = "";
            bmp.fieldExistenceIndicatorOtherData.FirstOrDefault(k => k.Key == "productInfo").Value = "";
            bmp.fieldExistenceIndicatorOtherData.FirstOrDefault(k => k.Key == "isUserLoggedIn").Value = "";
            bmp.fieldExistenceIndicatorOtherData.FirstOrDefault(k => k.Key == "itemTotal").Value = "";
            bmp.fieldExistenceIndicatorOtherData.FirstOrDefault(k => k.Key == "itemCategory").Value = "";
            bmp.fieldExistenceIndicatorOtherData.FirstOrDefault(k => k.Key == "ignoreValidationResult").Value = "";
            bmp.fieldExistenceIndicatorMerchant.FirstOrDefault(k => k.Key == "UDF1").Value = receipt != null ? receipt.UserServiceRequestReceiptId.ToString() : "";
            bmp.fieldExistenceIndicatorMerchant.FirstOrDefault(k => k.Key == "UDF2").Value = "";
            bmp.fieldExistenceIndicatorMerchant.FirstOrDefault(k => k.Key == "UDF3").Value = "";
            bmp.fieldExistenceIndicatorMerchant.FirstOrDefault(k => k.Key == "UDF4").Value = "";
            bmp.fieldExistenceIndicatorMerchant.FirstOrDefault(k => k.Key == "UDF5").Value = "";
            bmp.fieldExistenceIndicatorMerchant.FirstOrDefault(k => k.Key == "UDF6").Value = "";
            bmp.fieldExistenceIndicatorMerchant.FirstOrDefault(k => k.Key == "UDF7").Value = "";
            bmp.fieldExistenceIndicatorMerchant.FirstOrDefault(k => k.Key == "UDF8").Value = "";
            bmp.fieldExistenceIndicatorMerchant.FirstOrDefault(k => k.Key == "UDF9").Value = "";
            bmp.fieldExistenceIndicatorMerchant.FirstOrDefault(k => k.Key == "UDF10").Value = "";
            bmp.fieldExistenceIndicatorDCC.FirstOrDefault(k => k.Key == "DCCReferenceNumber").Value = "";
            bmp.fieldExistenceIndicatorDCC.FirstOrDefault(k => k.Key == "foreignAmount").Value = "";
            bmp.fieldExistenceIndicatorDCC.FirstOrDefault(k => k.Key == "ForeignCurrency").Value = "";
            bmp.merchantId = paymentKeys.MerchantId;
            bmp.merchantKey = paymentKeys.EncryptionKey;

            return bmp;
        }
    }
}