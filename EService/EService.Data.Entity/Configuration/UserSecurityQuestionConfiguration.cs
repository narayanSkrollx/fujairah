using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class UserSecurityQuestionConfiguration : EntityTypeConfiguration<UserSecruityQuestion>
    {
        public UserSecurityQuestionConfiguration()
        {
            ToTable("UserSecurityQuestion");

            HasKey(o => o.UserId);

            Property(o => o.UserId).IsRequired();
            Property(o => o.SecurityQuestionId).IsRequired();
            Property(o => o.Answer).IsRequired().HasMaxLength(100);

            HasRequired(o => o.SecurityQuestion)
                .WithMany(o => o.UserSecruityQuestions)
                .HasForeignKey(o => o.SecurityQuestionId);
        }
    }
}