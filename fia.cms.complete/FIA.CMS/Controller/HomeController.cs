﻿using System.Collections.Generic;
using System.Web.Mvc;
using FIA.CMS.Model;

namespace FIA.CMS.Controller
{
    public class HomeController : BaseController
    {
        public ActionResult RenderHomePageSlider()
        {
            return PartialView($"{SiteLayoutPartialViewLocation}_HomeSlider.cshtml");
        }

        public ActionResult RenderWhyFIA()
        {
            return PartialView($"{HomePartialViewLocation}_WhyFIA.cshtml");
        }

        public ActionResult RenderMainContent()
        {
            return PartialView($"{HomePartialViewLocation}_MainContent.cshtml");

        }

        public ActionResult GetTeamMemberByDepartmentId(int pageId)
        {
         var department =    Umbraco.Content(pageId);
            var list = new List<TeamMemberModel>();
            foreach (var item in department.Children.Where("Visible").OrderBy("CreateDate desc"))
            {
                var data = new TeamMemberModel()
                {
                    Name = item.memberName,
                    ImageUrl = item.image?.Url,
                    Designation = item.designationTitle,
                    About = item.about,
                };
                list.Add(data);
            }

            return PartialView($"{HomePartialViewLocation}_TeamMemberList.cshtml", list);
        }
    }
}