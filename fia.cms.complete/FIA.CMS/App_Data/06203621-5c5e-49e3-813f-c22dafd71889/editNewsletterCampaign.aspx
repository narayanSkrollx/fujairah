﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="/umbraco/masterpages/umbracoPage.Master" AutoEventWireup="true" CodeBehind="editNewsletterCampaign.aspx.cs" Inherits="Designit.Umb.Newsletter.Custom.plugins.designit.newsletter.EditNewsletterCampaign" %>
<%@ Register Namespace="umbraco.uicontrols" Assembly="controls" TagPrefix="umb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">

<umb:UmbracoPanel ID="UmbracoPanel1" runat="server" Width="608px" Height="336px" hasMenu="true">
      <umb:Pane ID="Pane1" runat="server" Height="44px" Width="528px">
        <umb:PropertyPanel ID="pp_name" runat="server">
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </umb:PropertyPanel>
        <umb:PropertyPanel id="pp_alias" runat="server">
            <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="Repeater1_OnItemCommand">
            <HeaderTemplate>  
            <table>  
                <tr> 
                    <th>  
                        <asp:Label ID="Label1" Text="Title" runat="server"></asp:Label>  
                    </th>  
                    <th>  
                        <asp:Label ID="Label2" Text="Status" runat="server"></asp:Label>  
                    </th>  
                    <th>  
                        <asp:Label ID="Label3" Text="Sent to" runat="server"></asp:Label>  
                    </th>  
                    <th>  
                        <asp:Label ID="Label4" Text="Reports" runat="server"></asp:Label>
                    </th>  
                    <th>  
                        <asp:Label ID="Label5" Text="Actions" runat="server"></asp:Label>  
                    </th>  
                </tr>  
            </HeaderTemplate>  
            <ItemTemplate>  
                <tr class="<%# Eval("campstatus") %>" id="<%# Eval("id") %>"> 
                    <td> 
                        <asp:Literal ID="LiteralName" runat="server" Text='<%# Eval("camptitle") %>' ></asp:Literal>  
                    </td>  
                    <td class="status">  
                        <asp:Literal ID="LiteralStatus" runat="server" Text='<%# Designit.Umb.Newsletter.Bll.Helper.GetStatusText(Eval("campstatus").ToString()) %>' ></asp:Literal>
                    </td>  
                    <td>  
                        <asp:Literal ID="LiteralEmail" runat="server"></asp:Literal>
                    </td> 
                   <td> 
                        <asp:Button ID="btnReport" Visible="false" runat="server" Text="View" CommandName="View" CommandArgument='<%# Eval("campstatus") %>'/>
                    </td> 
                    <td>  
                        <asp:Button ID="btnCopy" runat="server" Text="Copy" CommandName="Copy" CommandArgument='<%# Eval("id") %>'/>
                        <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CommandArgument='<%# Eval("id") %>'/>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CommandName="Delete" CommandArgument='<%# Eval("id") %>'/>
                    </td> 
          
                </tr>  
            </ItemTemplate>  
            <FooterTemplate>  
                </table>  
            </FooterTemplate>  
            </asp:Repeater>
        </umb:PropertyPanel>
      </umb:Pane>
    </umb:UmbracoPanel>

<script language="javascript" type="text/javascript">
    function redirect(whichText) {
        document.location.href = whichText; 
    }

    function checkStatus() {
        var $elements = $("tr.sending");
        if ($elements.length <= 0) {
            clearInterval(itv);
        }
        else {
            $elements.each(function () {
                var $this = $(this);
                $.ajax({
                    type: "GET",
                    cache: false,
                    url: "/base/newsletterhelper/AjaxStatus/" + this.id + ".aspx",
                    dataType: "json",
                    success: function (data) {
                        $this.find(".numSentTo").text(data.SentTo);
                        $this.find(".status").text(data.StatusText);
                        if (data.Status == "sent") {
                            $this.removeClass("sending").addClass("sent");
                        }
                    }
                });
            });
        }
    }

    var itv;

    $(document).ready(function () {
        itv = setInterval(checkStatus, 3000);
    });
</script>
</asp:Content>


