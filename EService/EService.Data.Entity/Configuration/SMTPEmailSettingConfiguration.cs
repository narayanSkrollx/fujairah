using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class SMTPEmailSettingConfiguration : EntityTypeConfiguration<SMTPEmailSetting>
    {
        public SMTPEmailSettingConfiguration()
        {
            ToTable("SMTPEmailSetting");

            HasKey(o => o.Id);

            Property(o => o.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(o => o.Host).IsRequired().HasColumnType("nvarchar(max)");
            Property(o => o.Port).IsRequired();
            Property(o => o.UserName).IsRequired();
            Property(o => o.Password).IsRequired();

        }
    }
}