$(document).ready(function(){
  $('.bxslider').bxSlider({
  	pager: false,
  	controls: false,
  	auto: true,
  	mode: 'fade'
  });
  $('li.search > a').click(function(){
  	$('.search-form').slideToggle();
  });
  $(".upload-option").append('<i class="fa fa-exclamation-triangle" aria-hidden="true"><span>Unsupported Format.</span></i>');
    $('input[type="file"]').change(function(){
        var val = this.value;
        var fileName = val.split('\\').pop();
        var newFile = $(this).parent().parent().find('#uploadFile').val(fileName);
        var fileExt = newFile.val();
    	var valid_extensions = /(\.jpg|\.jpeg|\.png|\.pdf)$/i; 
    	if(!valid_extensions.test(fileExt))
		{ 
			$('.fa').css({'visibility': 'visible', 'opacity': 1});
		}
		else{
			$('.fa').css({'visibility': 'hidden', 'opacity': 0});
		}
    });
});
