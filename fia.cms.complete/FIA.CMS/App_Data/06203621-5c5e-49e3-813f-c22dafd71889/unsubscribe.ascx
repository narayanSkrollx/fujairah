﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="unsubscribe.ascx.cs" Inherits="Designit.Umb.Newsletter.Custom.usercontrols.designit.newsletter.unsubscribe" %>
<asp:Panel ID="Panel1" runat="server">
<asp:Literal ID="LtlEmail" runat="server" Text="Email:"></asp:Literal>
    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
    <asp:Button ID="btnLogin" runat="server" Text="Login" 
        onclick="btnLogin_Click" />
    <br />
    <asp:Panel ID="Panel2" runat="server" Visible="False">
    </asp:Panel>
    <asp:Panel ID="Panel3" runat="server" Visible="False">
        <asp:Literal ID="LtlUnsubSel" runat="server" Text="Unsubscribe selected:"></asp:Literal>
        <asp:Button ID="btnUnsubSelected" runat="server" 
            onclick="btnUnsubSelected_Click" Text="Unsubscribe" />
    </asp:Panel>
    <br />
    <asp:Literal ID="ltlUnsub" runat="server" Text="Unsubscribe to all:"></asp:Literal>
    <asp:Button ID="btnUnsub" runat="server" onclick="btnUnsub_Click" 
        Text="Unsubscribe" />
</asp:Panel>

<asp:Literal ID="ltlConfirm" runat="server" Visible="False"></asp:Literal>


