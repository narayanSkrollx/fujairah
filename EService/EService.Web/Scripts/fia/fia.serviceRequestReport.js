﻿$(function () {
    fia.CreateNamespace("serviceRequestReport");

    (function (context) {
        context.Title = 'Service Report';

        context.Mapping = {
            create: function (options) {
                var vm = ko.mapping.fromJS(options.data);

                vm.SearchClick = function () {
                    context.Search();
                }

                return vm;
            }
        }

        context.Search = function () {
            var vm = ko.toJS(context.ViewModel.SearchModel);
            vm.DateFrom = moment(vm.DateFrom).isValid() ? moment(vm.DateFrom).format('YYYY-MM-DD') : null;
            vm.DateTo = moment(vm.DateTo).isValid() ? moment(vm.DateTo).format('YYYY-MM-DD') : null;
            ajaxRequest('/Report/ServiceRequestSummary/Search/', 'POST', {
                data: vm
            }, function (response) {
                if (response.IsSuccess) {
                    ko.mapping.fromJS(response.Data, {}, context.ViewModel.Records);
                } else {
                    showMessage('Service Request Detail', response.Message, 'error');
                }
            });
        }

        context.Initialize = function () {
            ajaxRequest('/Report/ServiceRequestSummary/Initialize', 'GET', {}, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#mainContent')[0])) {
                        context.ViewModel = ko.mapping.fromJS(resp.Data, context.Mapping);
                        ko.applyBindings(context.ViewModel, $('#mainContent')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel);
                    }
                } else {
                    showMessage('Service Request Detail', resp.Message, 'error');
                }
            });
        };

    })(fia.serviceRequestReport);
});