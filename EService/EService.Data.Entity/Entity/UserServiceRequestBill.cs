using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{

    public class UserServiceRequestReceipt : AuditableEntity
    {
        public int UserServiceRequestReceiptId { get; set; }
        public int UserServiceRequestMasterId { get; set; }

        #region "To be updated only after payment success"
        public string ReceiptNo { get; set; }
        public int? Year { get; set; }
        public int? Index { get; set; }
        public bool IsPaid { get; set; }

        #endregion
        public double? AdminFee { get; set; }
        public double? ServiceDevelopmentCharge { get; set; }
        public double? VATAmount { get; set; }
        public int PaymentStage { get; set; }

        [JsonIgnore]
        public virtual UserServiceRequestMaster UserServiceRequestMaster { get; set; }

        [JsonIgnore]
        public virtual ICollection<UserServiceRequestReceiptDetail> ReceiptDetails { get; set; }
    }

    public class UserServiceRequestReceiptDetail
    {
        public int UserServiceRequestReceiptDetailId { get; set; }
        public int UserServiceRequestReceiptId { get; set; }

        public string Description { get; set; }
        public int Quantity { get; set; }
        public double Rate { get; set; }

        [JsonIgnore]
        public UserServiceRequestReceipt Receipt { get; set; }
    }
    public class UserServiceRequestBill : AuditableEntity
    {
        public int UserServiceRequestMasterId { get; set; }

        public string ReceiptNo { get; set; }
        //public int? ReceiptYear { get; set; }
        //public int? ReceiptIndex { get; set; }

        public double ChargedAmount { get; set; }
        public double? DiscountAmount { get; set; }
        public int? PaymentMode { get; set; }
        public double? PaidAmount { get; set; }
        public int? PaidByUserId { get; set; }
        public DateTime? PaidDateTime { get; set; }
        public string PaidByUserName { get; set; }

        [JsonIgnore]
        public virtual UserServiceRequestMaster UserServiceRequestMaster { get; set; }

        [JsonIgnore]
        public virtual List<UserServiceRequestPaymentResponse> PaymentResponses { get; set; }
    }
}