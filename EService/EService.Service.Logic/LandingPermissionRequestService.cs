using EService.Data.Entity.Entity;
using EService.Service.Logic.Enum;

namespace EService.Service.Logic
{
    public class LandingPermissionRequestService : BaseEService<LandingPermissionRequest>
    {
        public LandingPermissionRequestService(ServiceContext context) : base(context)
        {
        }

        public override ServiceEnum Service => ServiceEnum.Other;
    }
}