﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.BusinessLogic;
using umbraco.cms.presentation.Trees;
using umbraco.DataLayer;

namespace Designit.Umb.Newsletter.Bll
{
    public class loadNewsletterTemplate: BaseTree
    {
        public loadNewsletterTemplate(string application) :
            base(application) { }


        protected override void CreateRootNode(ref XmlTreeNode rootNode)
        {
            rootNode.Icon = FolderIcon;
            rootNode.OpenIcon = FolderIconOpen;
            rootNode.NodeType = TreeAlias;
            rootNode.NodeID = "init";
        }

        public override void RenderJS(ref System.Text.StringBuilder Javascript)
        {
            Javascript.Append(
                @"
                        function openNewsletterTemplate(id)
                        {
                           parent.right.document.location.href = 'plugins/designit/newsletter/editNewsletterTemplate.aspx?id=' + id;
                        }
        
                ");

            
        }


        public override void Render(ref XmlTree tree)
        {
            using (var reader = Application.SqlHelper.ExecuteReader("select * from newsletterTemplate"))
            {
                while (reader.Read())
                {
                    XmlTreeNode xNode = XmlTreeNode.Create(this);
                    xNode.NodeID = reader.GetInt("id").ToString();
                    xNode.Text = reader.GetString("alias");
                    xNode.Icon = "settingTemplate.gif";
                    xNode.Action = "javascript:openNewsletterTemplate(" + reader.GetInt("id").ToString() + ")";
                    tree.Add(xNode);

                }
            }
        }
    }
}

