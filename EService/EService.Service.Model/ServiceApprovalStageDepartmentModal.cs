﻿namespace EService.Service.Model
{
    public class ServiceApprovalStageDepartmentModal
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
    }
}