﻿using System.Web.Mvc;
using EService.Data.Entity;
using EService.Web.Controllers.Base;

namespace EService.Web.Controllers
{
    public class DashboardController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult GetUserDashboard()
        {
            var userType = UserProfile.UserType;
            switch (userType)
            {
                case UserTypeEnum.SystemAdmin:
                    return PartialView("_SystemAdminDashbord");
                    break;
                case UserTypeEnum.FinanceUser:
                    return PartialView("_FinanceDashboard");
                    break;
                case UserTypeEnum.DepartmentUser:
                    return PartialView("_DepartmentDashboard");
                    break;
                case UserTypeEnum.ExternalUser:
                    return PartialView("_EServiceDashboard");
                    break;
                default:
                    return PartialView("_EmptyDashboard");
                    break;
            }
        }

        
    }
}