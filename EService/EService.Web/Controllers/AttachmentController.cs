using System;
using System.Web.Mvc;
using EService.Service.Logic;
using EService.Web.Controllers.Base;
using EService.Web.Models;

namespace EService.Web.Controllers
{
    public class AttachmentController : BaseController
    {
        private UserAttachmentService userAttachmentService;

        public AttachmentController()
        {
            userAttachmentService = new UserAttachmentService(serviceContext);
        }

        [HttpPost]
        public ActionResult SetAttachment(int id)
        {
            var result = new JsonResponseObject();
            try
            {
                var userAttachment = userAttachmentService.Find(o => o.UserAttachmentId == id && o.UserAttachmentData != null);
                if (userAttachment != null)
                {
                    TempData["AttachmentDataContent"] = new
                    {
                        userAttachment.UserAttachmentData.FileContents,
                        userAttachment.FileName
                    };
                    result.IsSuccess = true;

                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "Invalid File Selected.";
                }

            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while trying to download";
            }
            return Json(result);
        }

        public ActionResult Index()
        {
            try
            {
                var data = TempData["AttachmentDataContent"] as dynamic;
                var contents = data.FileContents as byte[];
                var filename = data.FileName as string;

                if (contents != null && contents.Length >= 1)
                {
                    return File(contents, System.Net.Mime.MediaTypeNames.Application.Octet, filename);
                    //Response.AddHeader("Content-Disposition", "inline; filename="+filename);
                    //return File(contents, "application/pdf");
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return ErrorResult("");
        }
    }
}