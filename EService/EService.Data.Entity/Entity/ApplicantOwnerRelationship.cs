using System.Collections.Generic;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class ApplicantOwnerRelationship : ILookupItem
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public bool IsOther { get; set; }
        
        [JsonIgnore]
        public virtual ICollection<BuildingHeightRequest> BuildingHeightRequests { get; set; }
    }
}