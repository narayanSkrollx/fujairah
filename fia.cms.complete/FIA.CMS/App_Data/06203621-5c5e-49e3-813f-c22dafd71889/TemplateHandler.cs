﻿using System.IO;

namespace Designit.Umb.Newsletter.Bll
{
    public class TemplateHandler
    {
        /// <summary>
        /// Gets the template from alias.
        /// </summary>
        /// <param name="alias">The alias.</param>
        /// <returns></returns>
        internal static string GetTemplateFromAlias(string alias)
        {
            var ret = string.Empty;
            //retrieve template code
            var path = Helper.GetTemplateDir();
            path = Path.Combine(path, string.Format("{0}.master", alias));

            if (File.Exists(path))
            {
                ret = File.ReadAllText(path);
            }

            ret = Helper.InsertHostIntoLink(ret);

            return ret;
        }
    }
}
