using System.Collections.Generic;
using EService.Data.Entity.Entity;
using EService.Service.Model;
using Nelibur.ObjectMapper;

namespace EService.Service.Logic
{
    public class ApplicableFeesLookupService : BusinessAccessBase<ApplicableFeesLookUp>
    {
        public ApplicableFeesLookupService(ServiceContext context) : base(context)
        {
        }

        public ResponseObject Save(List<ApplicableFeesLookupServiceModel> records)
        {
            var result = new ResponseObject();
            foreach (var record in records)
            {
                var existingRecord = Find(o => o.Active && o.Id == record.Id);
                if (existingRecord != null)
                {
                    existingRecord.Description = record.Description;
                    existingRecord.Fee = record.Fee;
                    existingRecord.Notes = record.Notes;
                    existingRecord.Fees = record.Fee != null ? record.Fee.Value.ToString("#,###") : "";
                }
                else
                {
                    var addRecord = TinyMapper.Map<ApplicableFeesLookupServiceModel, ApplicableFeesLookUp>(record);
                    addRecord.Notes = addRecord.Description;
                    addRecord.Fees = addRecord.Fee?.ToString("#,###") ?? "";
                    addRecord.Active = true;
                    Add(addRecord);
                }
            }

            Context.Commit();
            result.IsSuccess = true;
            result.Message = "Applicable Fees saved successfully.";
            return result;
        }
    }
}