﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EService.Data.Entity.Entity
{
    public class AircraftMaintenanceRequest : AuditableEntity, IServiceEntity
    {
        public int UserServiceRequestMasterId { get; set; }

        public DateTime DateOfApplication { get; set; }
        public string NameOfApplicant { get; set; }
        public string Designation { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailId { get; set; }
        public string ApplicantRegisteredMaintenanceCompany { get; set; }
        public string AddressOfMaintenanceCompany { get; set; }
        public string PhoneNumberOfRefisteredmaintenanceCompany { get; set; }
        public string MaintenanceCompanyTradeLicenseNumber { get; set; }
        public DateTime? MaintenanceCompanyTradeLicenseExpiryDate { get; set; }
        public string Part145RegistrationNumber { get; set; }
        public DateTime? Part145RegistrationExpiryDate { get; set; }
        public string AircraftType { get; set; }
        public string AircraftModel { get; set; }
        public string AircraftRegistrationNumber { get; set; }
        /// <summary>
        /// Type of Maintenance
        /// True for scheduled
        /// False for Non-Scheduled
        /// </summary>
        public bool? TypeOfMaintenance { get; set; }
        public string DetailsOfMaintenance { get; set; }
        public DateTime? ScheduleDateOfAircraftArrival { get; set; }
        public DateTime? ScheduleDateOfAircraftDeparture { get; set; }

        [JsonIgnore]
        public virtual UserServiceRequestMaster UserServiceRequestMaster { get; set; }
    }
}
