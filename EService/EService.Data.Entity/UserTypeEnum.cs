namespace EService.Data.Entity
{
    public enum UserTypeEnum
    {
        SystemAdmin = 1,
        DepartmentUser = 2,
        ExternalUser = 3,
        FinanceUser = 4
    }
}