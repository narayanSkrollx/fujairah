using System;
using System.Collections.Generic;

namespace EService.Web.Models
{
    public class CranePermitPlanningViewModel
    {
        public int UserServiceRequestMasterId { get; set; }
        public int SavedByUserId { get; set; }
        public bool HasPentrationOfSurface { get; set; }
        public decimal? PenetrationInMeter { get; set; }
        public bool RequireAviationLight { get; set; }
        public string Comments { get; set; }
        public decimal? MaxCraneOperatingHeight { get; set; }
        public decimal? CraneOperatingDistanceFromRunway { get; set; }
        public string Recommendations { get; set; }
        public DateTime SavedDateTime { get; set; }
        public string AssessedBy { get; set; }
        public string AirportPlanner { get; set; }
        public DateTime? AssessedDate { get; set; }
        public List<InfringementTypeViewModel> InfringementTypes { get; set; } 
    }

    public class InfringementTypeViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }

        public bool IsOther { get; set; }
        public string OtherName { get; set; }
    }
}