﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EService.Data.Entity.Entity;
using EService.Service.Logic.Enum;
using EService.Service.Model;

namespace EService.Service.Logic
{
    public class TradeLicenseRequestService : BaseEService<TradeLicenseRequest>
    {
        public TradeLicenseRequestService(ServiceContext context) : base(context)
        {
        }

        public override ServiceEnum Service => ServiceEnum.TradeLicenseRequest;

        public ResponseObject UpdateTradeLicenseRequest(TradeLicenseRequest model, bool isDraft, List<UserServiceRequestAttachment> attachmentList)
        {
            var result = new ResponseObject();
            var contract = Find(x => x.UserServiceRequestMasterId == model.UserServiceRequestMasterId);
            if (contract == null)
            {
                result.IsSuccess = false;
                result.Message = "No request matched to edit.";
            }
            else
            {
                var list = contract.SelectedStaffStrengths.ToList();
                Context.Database.TradeLicenseSelectedStaffStrengths.RemoveRange(list);

                var removeDocList = contract.UserServiceRequestMaster.UserServiceRequestAttachments.ToList();
                Context.Database.UserServiceRequestAttachment.RemoveRange(removeDocList);

                attachmentList.ForEach(o => contract.UserServiceRequestMaster.UserServiceRequestAttachments.Add(o));
                
                contract.UserServiceRequestMaster.IsDraft = isDraft;

                contract.ApplicantName = model.ApplicantName;
                contract.PresentSponsorInUAE = model.PresentSponsorInUAE;
                contract.AddressOfSponser = model.AddressOfSponser;
                contract.ApplicantOfficeNumber = model.ApplicantOfficeNumber;
                contract.ApplicantResidenceNumber = model.ApplicantResidenceNumber;
                contract.ApplicantMobileNumber = model.ApplicantMobileNumber;
                contract.ApplicantFaxNumber = model.ApplicantFaxNumber;
                contract.ApplicantEmailNumber = model.ApplicantEmailNumber;
                contract.ApplicantCountry = model.ApplicantCountry;
                contract.MailingAddress = model.MailingAddress;
                contract.ApplicationDate = model.ApplicationDate;
                contract.CompanyPreferredName = model.CompanyPreferredName;
                contract.CompanyNameOption1 = model.CompanyNameOption1;
                contract.CompanyNameOption2 = model.CompanyNameOption2;
                contract.CompanyNameOption3 = model.CompanyNameOption3;
                contract.BusinessActivities1 = model.BusinessActivities1;
                contract.BusinessActivities2 = model.BusinessActivities2;
                contract.BusinessActivities3 = model.BusinessActivities3;
                contract.Remarks = model.Remarks;
                contract.ExpectedAnnualBusinessTrunoverAmount = model.ExpectedAnnualBusinessTrunoverAmount;
                contract.ExpectedAnnualBusinessTrunoverCurrencyId = model.ExpectedAnnualBusinessTrunoverCurrencyId;
                contract.TotalStaffStrengthRequired = model.TotalStaffStrengthRequired;
                contract.SelectedStaffStrengths = model.SelectedStaffStrengths;

                Context.Commit();
                result.IsSuccess = true;
                result.Message = "Trade License Request updated successfully";
            }
            return result;
        }
    }
}
