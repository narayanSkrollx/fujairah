﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EService.Core;
using EService.Service.Logic;
using EService.Service.Logic.Enum;
using EService.Service.Model;
using EService.Web.Controllers.Base;
using EService.Web.Models;
using EService.Web.Models.ServiceRequestOperation;

namespace EService.Web.Areas.Report.Controllers
{
    public class ServiceRequestSummaryController : BaseController
    {

        private ServiceInfoService serviceInfoService;
        private UserServiceRequestMasterService userRequestService;
        private ServiceContext context;

        public ServiceRequestSummaryController()
        {
            context = new ServiceContext();
            userRequestService = new UserServiceRequestMasterService(context);
            serviceInfoService = new ServiceInfoService(context);
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Initialize()
        {
            var result = new ResponseObject();
            try
            {
                var model = new UserServiceRequestOperationViewModel
                {
                    Services = serviceInfoService.FindAllAsNoTracking(o => o.IsActive && !o.IsDeleted)
                        .Select(o => new SelectListModel()
                        {
                            Description = o.ServiceName,
                            Id = o.ServiceId
                        }).ToList(),
                    Statuses = (ServiceFlowStateEnum.Active).ToDictionary().Select(o => new SelectListModel()
                    {
                        Description = o.Value,
                        Id = o.Key
                    }).ToList()
                };
                result.IsSuccess = true;
                result.Data = new
                {
                    SearchModel = model,
                    Records = new List<UserServiceRequestModel>()
                };
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while obtaining information for service.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        public JsonResult Search(ServiceRequestReportSearchViewModel searchModel)
        {
            var result = new JsonResponseObject();

            try
            {
                var serviceId = searchModel.ServiceStatus != null ? (int?) searchModel.ServiceStatus : null;
                var summaryData = userRequestService.GetServiceStatusSummary(searchModel.DateFrom, searchModel.DateTo,
                    searchModel.ServiceId, serviceId);
                result.Data = summaryData;
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "";
            }

            return Json(result);
        }
    }
}