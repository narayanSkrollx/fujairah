﻿using System;
using System.Collections.Generic;

namespace EService.Service.Model
{
    public class MeteorologicalDataRequestServiceModal :EServiceBaseModal
    {

        #region "User Service Request Master Columns"

        public int? CurrentIndex { get; set; }

        #endregion "User Service Request Master Columns"

        #region "Contact Information"

        public string Email { get; set; }
        public string Department { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Address { get; set; }

        #endregion

        public bool DataRangeDaily { get; set; }
        public bool DataRangeHourly { get; set; }
        public bool DataRangeMonthly { get; set; }
        public bool DataRangeYearly { get; set; }
        public bool DataRangeSelectPeriod { get; set; }
        public DateTime? CustomRangeStartDate { get; set; }
        public DateTime? CustomRangeEndDate { get; set; }
        public bool IsChargeAmountUpdated { get; set; }
        public double ChargedAmount { get; set; }
        public string METDepartmentRemrks { get; set; }
        public List<MetWeatherTypeReportServiceModel> WeatherTypes { get; set; }
        public List<MetWeatherElementReportServiceModel> WeatherElements { get; set; }
    }

    public class MetWeatherTypeReportServiceModel
    {
        public int WeatherTypeReportId { get; set; }
        public string WeatherTypeDescription { get; set; }
        public bool IsActive { get; set; }
    }

    public class MetWeatherElementReportServiceModel
    {
        public int WeatherElementId { get; set; }
        public bool IsActive { get; set; }
        public string OtherName { get; set; }

        public string WeatherElementDescription { get; set; }
        public bool IsOther { get; set; }
    }
}