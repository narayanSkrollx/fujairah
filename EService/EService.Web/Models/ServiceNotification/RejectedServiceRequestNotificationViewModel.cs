using System;

namespace EService.Web.Models.ServiceNotification
{
    public class RejectedServiceRequestNotificationViewModel : ServiceNotificationBaseViewModel
    {
        public int RejectedByDepartmentUserId { get; set; }
        public string RejectedByDepartmentUserName { get; set; }
        public DateTime RejectedByDepartmentDateTime { get; set; }
        public int PassedToUserId { get; set; }
        public int PassedToUserName { get; set; }
        public bool IsPassedToUser { get; set; }
        public string RejectedToDepartmentName { get; set; }
    }
}