using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class TradeLicenseSelectedStaffStrengthConfiguration : EntityTypeConfiguration<TradeLicenseSelectedStaffStrength>
    {
        public TradeLicenseSelectedStaffStrengthConfiguration()
        {
            ToTable("TradeLicenseSelectedStaffStrength");
            HasKey(x => x.TradeLicenseSelectedStaffStrengthId);

            Property(x => x.TradeLicenseSelectedStaffStrengthId).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.TradeLicenseRequestId).IsRequired();
            Property(x => x.StaffStrengthId).IsRequired();

            HasRequired(x=>x.StaffStrength).WithMany(x=>x.TradeLicenseSelectedStaffStrengths).HasForeignKey(x=>x.StaffStrengthId).WillCascadeOnDelete(false);
            HasRequired(x => x.TradeLicenseRequest).WithMany(x => x.SelectedStaffStrengths)
                .HasForeignKey(x => x.TradeLicenseRequestId).WillCascadeOnDelete(false);
        }
    }
}