﻿using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class PaymentKeyLogic : BusinessAccessBase<PaymentKey>
    {
        public PaymentKeyLogic(ServiceContext context) : base(context)
        {
        }
    }
}
