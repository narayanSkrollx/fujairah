using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class CompanyCategoryConfiguration : EntityTypeConfiguration<CompanyCategory>
    {
        public CompanyCategoryConfiguration()
        {
            ToTable("CompanyCategory");

            HasKey(o => o.Id);

            Property(o => o.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.Description).HasMaxLength(100).IsRequired();
        }
    }
}