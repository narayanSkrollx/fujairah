using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class CranePermitRequestFirstStageInfringementType
    {
        public int CranePermitRequestFirstStageInfringementTypeId { get; set; }
        public int InfringementTypeId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public string OtherName { get; set; }

        [JsonIgnore]
        public virtual CranePermitRequestFirstStageEntry CranePermitRequestFirstStageEntry { get; set; }
        [JsonIgnore]
        public virtual InfringementType InfringementType { get; set; }
    }
}