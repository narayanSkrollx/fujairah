﻿using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class PaymentKeysService : BusinessAccessBase<PaymentKey>
    {
        public PaymentKeysService(ServiceContext context) : base(context)
        {
        }
    }
}