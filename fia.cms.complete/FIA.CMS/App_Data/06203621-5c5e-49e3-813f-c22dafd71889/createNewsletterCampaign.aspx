﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="/umbraco/masterpages/umbracoPage.Master"
    AutoEventWireup="true" CodeBehind="createNewsletterCampaign.aspx.cs" Inherits="Designit.Umb.Newsletter.Custom.plugins.designit.newsletter.CreateNewsletterCampaign" %>

<%@ Register Namespace="umbraco.uicontrols" Assembly="controls" TagPrefix="umb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <umb:UmbracoPanel ID="UmbracoPanel1" runat="server" Width="608px" Height="336px"
        hasMenu="true">
        <umb:Pane ID="Pane1" runat="server" Height="44px" Width="528px">
            <umb:PropertyPanel ID="pp_Step" runat="server">
                <div style="text-align: left; float: left;">
                    <asp:Literal ID="ProgressLiteral" runat="server"></asp:Literal></div>
                <div style="text-align: right; float: right;">
                    <asp:Button ID="BackButton" runat="server" Text="Back" Enabled="false" OnClick="BackButton_Click" />
                    <asp:Button ID="NextButton" runat="server" Text="Next" OnClick="NextButton_Click" />
                </div>
                <br style="clear: both;" />
                <umb:PropertyPanel ID="pp_step1" runat="server">
                    <asp:Literal ID="Literal2" runat="server" Text="Which list would you like to send this campaign to"></asp:Literal>
                    <br />
                    <asp:Repeater ID="Repeater1" runat="server">
                        <HeaderTemplate>
                            <table>
                                <thead>
                                <tr>
                                    <th>
                                        <label>Name</label>
                                    </th>
                                    <th>
                                        <label>Send to list</label>
                                    </th>
                                    <th>
                                        <label>Recipients in list</label>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Literal ID="LiteralName" runat="server" Text='<%# Eval("Text") %>'></asp:Literal>
                                </td>
                                <td>
                                    <asp:CheckBox ID="CheckboxGroup" Enabled="True" runat="server" />
                                </td>
                                <td>
                                    <asp:Literal ID="LiteralRecipents" runat="server" Text='<%# Eval("Text") %>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </umb:PropertyPanel>
                <umb:PropertyPanel ID="pp_step2" runat="server" Visible="false">
                    <asp:Literal ID="LiteralCampaignName" runat="server" Text="Name of campaign"></asp:Literal><br />
                    <asp:TextBox ID="txtCampaignName" runat="server" Width="200"></asp:TextBox><br /><br />
                    <asp:Literal ID="LiteralSubject" runat="server" Text="Subject"></asp:Literal><br />
                    <asp:TextBox ID="txtSubjct" runat="server" Width="200"></asp:TextBox><br /><br />
                    <asp:Literal ID="LiteralFrom" runat="server" Text="From name"></asp:Literal><br />
                    <asp:TextBox ID="txtFrom" runat="server" Width="200"></asp:TextBox><br /><br />
                    <asp:Literal ID="LiteralReply" runat="server" Text="Reply-to-e-mail"></asp:Literal><br />
                    <asp:TextBox ID="TxtReply" runat="server" Width="200"></asp:TextBox><br /><br />
                    <asp:Literal ID="LiteralTemplate" runat="server" Text="Choose mail template"></asp:Literal><br />
                    <asp:ListBox ID="ListBox1" runat="server" Width="200"></asp:ListBox>
                    <br /><br />
                    <asp:Literal ID="LiteralLinkText" runat="server" Text="Link tracking"></asp:Literal><br />
                    <asp:CheckBox ID="checkLinkTrack" OnCheckedChanged="CheckLinkTrackCheckedChanged" Text="Track clicked links?" runat="server" AutoPostBack="true" /><br />
                    <asp:CheckBox ID="checkLinkGoogle" Text="Track local links with Google Analytics" runat="server" />
                </umb:PropertyPanel>
                <umb:PropertyPanel ID="pp_step3" runat="server" Visible="false">
                    <iframe id="iframe1" name="iframe1" frameborder="0" marginheight="0" marginwidth="0"
                        runat="server" width="100%"></iframe>
                    <br />
                    <asp:TextBox ID="contentTextbox" runat="server" Style="display: none;"></asp:TextBox>
                </umb:PropertyPanel>
                <umb:PropertyPanel ID="pp_step4" runat="server" Visible="false">
                    <iframe id="iframePreview" frameborder="0" marginheight="0" marginwidth="0" name="iframePreview"
                        runat="server" width="100%" height=""></iframe>
                    <br />
                </umb:PropertyPanel>
            </umb:PropertyPanel>
        </umb:Pane>
    </umb:UmbracoPanel>

    <script type="text/javascript">
        function loadData() {
            var a = '<%=Session["step"]%>';
            if (a == 3) {
                var html = window.frames['iframe1'].getTinyContent();
                document.getElementById('<%=contentTextbox.ClientID%>').value = html;
                resizeIframe('iframe1');
            }
        }

        function setData() {


            var html = document.getElementById('<%=contentTextbox.ClientID%>').value;
            window.frames['iframe1'].setTinyContent(html);
            resizeIframe('iframe1');

        }

        function resizeIframe(iframename) {
            var obj;
            if (iframename == 'iframe1') {
                obj = document.getElementById('<%=iframe1.ClientID%>')
                if (obj.contentWindow.document.body.scrollHeight > 650) {
                    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
                }
                else {
                    obj.style.height = 650 + 'px';
                }
            }
            else if (iframename == 'iframePreview') {
                obj = document.getElementById('<%=iframePreview.ClientID%>')
                obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
            }

        }

    </script>

    );
</asp:Content>
