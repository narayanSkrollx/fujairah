using System.Collections.Generic;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class CraneType : AuditableEntity
    {
        public int CraneTypeId { get; set; }
        public string CraneTypeName { get; set; }
        public bool IsOther { get; set; }
        
        [JsonIgnore]
        public virtual ICollection<CranePermitRequestSelectedCrane> CranePermitRequestSelectedCranes { get; set; } 
    }

    public class CranePermitRequestSelectedCrane
    {
        public int CranePermitRequestSelectedCraneId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public int CraneTypeId { get; set; }
        public string OtherCraneType { get; set; }

        [JsonIgnore]
        public virtual CranePermitRequest CranePermitRequest { get; set; }
        [JsonIgnore]
        public virtual CraneType CraneType { get; set; }
    }

}