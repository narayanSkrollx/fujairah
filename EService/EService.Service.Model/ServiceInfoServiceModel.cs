namespace EService.Service.Model
{
    public class ServiceInfoServiceModel
    {
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public bool IsDefault { get; set; }
        public string ServiceCode { get; set; }
        public decimal DefaultRate { get; set; }
        public decimal VATPercentage { get; set; }
    }
}