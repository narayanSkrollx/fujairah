﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EService.Service.Logic;
using EService.Service.Model;
using EService.Web.Controllers.Base;

namespace EService.Web.Controllers
{
    public class LookupController : BaseController
    {
        private SecurityQuestionService securityQuestionService;
        private ServiceInfoService serviceInfoService;

        public LookupController()
        {
            securityQuestionService = new SecurityQuestionService(serviceContext);
            serviceInfoService = new ServiceInfoService(serviceContext);
        }

        public JsonResult SecurityQuestion()
        {
            var result = new ResponseObject();
            try
            {
                result.Data = securityQuestionService.FindAllAsNoTracking(o => !o.Active).OrderBy(o => o.Description);
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while searching for security questions.";
            }
            return Json(result);
        }

        public JsonResult Service()
        {
            var result = new ResponseObject();
            try
            {
                result.Data = serviceInfoService.FindAllAsNoTracking(o => o.IsActive).Select(o => new { o.ServiceId, o.ServiceName }).ToList();
                result.IsSuccess = true;

            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while obtaining Service list";
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }

}