namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReceiptRelatedTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserServiceRequestReceipt",
                c => new
                    {
                        UserServiceRequestReceiptId = c.Int(nullable: false, identity: true),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        ReceiptNo = c.String(maxLength: 255),
                        Year = c.Int(),
                        Index = c.Int(),
                        IsPaid = c.Boolean(nullable: false),
                        AdminFee = c.Double(),
                        ServiceDevelopmentCharge = c.Double(),
                        VATAmount = c.Double(),
                        PaymentStage = c.Int(nullable: false),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserServiceRequestReceiptId)
                .ForeignKey("dbo.UserServiceRequestMaster", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId);
            
            CreateTable(
                "dbo.UserServiceRequestReceiptDetail",
                c => new
                    {
                        UserServiceRequestReceiptDetailId = c.Int(nullable: false, identity: true),
                        UserServiceRequestReceiptId = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 1024),
                        Quantity = c.Int(nullable: false),
                        Rate = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.UserServiceRequestReceiptDetailId)
                .ForeignKey("dbo.UserServiceRequestReceipt", t => t.UserServiceRequestReceiptId)
                .Index(t => t.UserServiceRequestReceiptId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserServiceRequestReceipt", "UserServiceRequestMasterId", "dbo.UserServiceRequestMaster");
            DropForeignKey("dbo.UserServiceRequestReceiptDetail", "UserServiceRequestReceiptId", "dbo.UserServiceRequestReceipt");
            DropIndex("dbo.UserServiceRequestReceiptDetail", new[] { "UserServiceRequestReceiptId" });
            DropIndex("dbo.UserServiceRequestReceipt", new[] { "UserServiceRequestMasterId" });
            DropTable("dbo.UserServiceRequestReceiptDetail");
            DropTable("dbo.UserServiceRequestReceipt");
        }
    }
}
