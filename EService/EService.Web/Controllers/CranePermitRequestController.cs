﻿using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Service.Logic.Enum;
using EService.Web.Helper;
using EService.Web.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using EService.Web.Controllers.Base;
using EService.Web.Models.ServiceRequestOperation;
using Nelibur.ObjectMapper;
using System.Collections.Generic;
using EService.Core;
using EService.Web.Helper.EmailSender;

namespace EService.Web.Controllers
{
    public class CranePermitRequestController : ServiceRequestBaseController
    {
        private CranePermitRequestService cranePermitRequestService;
        private CraneTypeService craneTypeService;
        private ServiceApprovalStageService serviceApprovalStageService;
        private UserServiceRequestMasterService userRequestService;
        private ServiceInfoService serviceInfoService;
        private UserService userService;

        public CranePermitRequestController()
        {
            cranePermitRequestService = new CranePermitRequestService(serviceContext);
            serviceApprovalStageService = new ServiceApprovalStageService(serviceContext);
            craneTypeService = new CraneTypeService(serviceContext);
            userRequestService = new UserServiceRequestMasterService(serviceContext);
            serviceInfoService = new ServiceInfoService(serviceContext);
            userService = new UserService(serviceContext);
        }

        public override ServiceEnum Service => ServiceEnum.CranePermitRequest;

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CranePermitRequestViewModel model, bool isDraft = false)
        {
            var result = new JsonResponseObject();
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.UserServiceRequestMasterId > 0)
                    {
                        var cranePermitRequest = new CranePermitRequest
                        {
                            ApplicantEmail = model.ApplicantEmail,
                            ApplicantName = model.ApplicantName,
                            ApplicationDate = model.ApplicationDate.Value,
                            CompanyName = model.CompanyName,
                            ContactNumber = model.ContactNumber,
                            CraneLocation = model.CraneLocation,
                            CraneOperatingCompany = model.CraneOperatingCompany,
                            IsNewApplication = model.IsNewApplication,
                            PurposeOfUser = model.PurposeOfUser,
                            PreviousPermitNumbers = model.PreviousPermitNumbers,
                            MaxOperatingHeight = model.MaxOperatingHeight.Value,
                            NorthingsEastings = model.NorthingsEastings,
                            OperationStartDate = model.OperationStartDate.Value,
                            OperationEndDate = model.OperationEndDate.Value,
                            OperationStartTime = model.OperationStartTime,
                            OperationEndTime = model.OperationEndTime,
                            OperatorContactNumber = model.OperatorContactNumber,
                            OperatorName = model.OperatorName,
                            Position = model.Position,
                            CranePermitRequestSelectedCranes = model.CraneTypeSelectionViewModels.Where(o => o.IsSelected)
                            .Select(o => new CranePermitRequestSelectedCrane()
                            {
                                CraneTypeId = o.CraneTypeId,
                                OtherCraneType = o.OtherCraneTypeName,
                            }).ToList(),
                            UserServiceRequestMasterId = model.UserServiceRequestMasterId
                        };
                        var attachmentList = new List<UserServiceRequestAttachment>();
                        #region "Uploaded Files Edit"
                        if (model.EditUploadedFiles != null && model.EditUploadedFiles.Any())
                        {
                            attachmentList.AddRange(model.EditUploadedFiles.Where(o => o.ObjectState != ObjectState.Deleted).Select(o => new UserServiceRequestAttachment { UserAttachmentId = o.Id }));

                        }
                        if (model.UploadedFiles != null && model.UploadedFiles.Any())
                        {
                            attachmentList.AddRange(model.UploadedFiles.Select(o => new UserServiceRequestAttachment { UserAttachmentId = o.Id }));

                        }

                        #endregion
                        cranePermitRequestService.UpdateCranePemitRequest(cranePermitRequest, isDraft, attachmentList);
                        result.IsSuccess = true;
                        result.Message = "Crane Permit request updated successfully.";

                        if (!isDraft)
                        {
                            //TriggerEmail(FIAEmailType.New, model.UserServiceRequestMasterId);
                            EmailSenderHelper.SendNewRequestEmail(ControllerContext, model.UserServiceRequestMasterId);
                        }
                    }
                    else
                    {

                        var serviceFlowStages =
                            serviceApprovalStageService.FindAllAsNoTracking(o => o.ServiceId == ServiceId).ToList();
                        var serviceCodeModal = userRequestService.GetNewServiceCode(ServiceId);
                        var serviceInfo = serviceInfoService.Find(o => o.ServiceId == ServiceId);
                        var userServiceRequest = new UserServiceRequestMaster()
                        {
                            RequestDateTime = DateTime.Now.DubaiTime(),
                            ServiceId = ServiceId,
                            Status = (int)ServiceFlowStateEnum.New,
                            UserId = UserProfile.UserId,
                            IsDeleted = false,
                            IsDraft = isDraft,
                            RegistrationNoYear = serviceCodeModal.Data.Year,
                            RegistrationNoMonth = serviceCodeModal.Data.Month,
                            RegistrationNoIndex = serviceCodeModal.Data.RegistrationNoIndex,
                            RegistrationNoServiceCode = serviceCodeModal.Data.ServiceCode,
                            RegistrationNo = serviceCodeModal.Data.RegistrationNo,
                            ParentUserServiceRequestMasterId = model.ParentUserServiceRequestMasterid,
                            VAT = serviceInfo.VATPercentage,
                            ServiceRequestFlowStages = serviceFlowStages
                            .Select(o => new ServiceRequestFlowStage()
                            {
                                Index = o.Index,
                                IsCurrentStage = o.Index == 1,
                                ServiceRequestFlowStageDepartments = o.ServiceApprovalStageDepartments.Select(p =>
                                new ServiceRequestFlowStageDepartment()
                                {
                                    DepartmentId = p.DepartmentId,
                                    Status = (int)ServiceFlowStateEnum.New,
                                    IsApproved = false,
                                }).ToList()
                            }).ToList(),
                            ServieBill = new UserServiceRequestBill()
                            {
                                ChargedAmount = (double)serviceInfo.DefaultRate
                            },
                            UserServiceRequestAttachments = model.UploadedFiles.Select(o => new UserServiceRequestAttachment { UserAttachmentId = o.Id }).ToList()
                        };

                        var cranePermitRequest = new CranePermitRequest
                        {
                            ApplicantEmail = model.ApplicantEmail,
                            ApplicantName = model.ApplicantName,
                            ApplicationDate = model.ApplicationDate.Value,
                            CompanyName = model.CompanyName,
                            ContactNumber = model.ContactNumber,
                            CraneLocation = model.CraneLocation,
                            CraneOperatingCompany = model.CraneOperatingCompany,
                            IsNewApplication = model.IsNewApplication,
                            PurposeOfUser = model.PurposeOfUser,
                            PreviousPermitNumbers = model.PreviousPermitNumbers,
                            MaxOperatingHeight = model.MaxOperatingHeight.Value,
                            NorthingsEastings = model.NorthingsEastings,
                            OperationStartTime = model.OperationStartTime,
                            OperationEndTime = model.OperationEndTime,
                            OperationStartDate = model.OperationStartDate.Value,
                            OperationEndDate = model.OperationEndDate.Value,
                            OperatorContactNumber = model.OperatorContactNumber,
                            OperatorName = model.OperatorName,
                            Position = model.Position,
                            CranePermitRequestSelectedCranes =
                                model.CraneTypeSelectionViewModels.Where(o => o.IsSelected)
                                    .Select(o => new CranePermitRequestSelectedCrane()
                                    {
                                        CraneTypeId = o.CraneTypeId,
                                        OtherCraneType = o.OtherCraneTypeName,
                                    }).ToList(),
                            UserServiceRequestMaster = userServiceRequest
                        };
                        cranePermitRequestService.Add(cranePermitRequest);
                        //userServiceRequest.CranePermitRequest = cranePermitRequest;
                        serviceContext.Commit();
                        if (!isDraft)
                        {
                            //TriggerEmail(FIAEmailType.New, userServiceRequest.UserServiceRequestMasterId);
                            EmailSenderHelper.SendNewRequestEmail(ControllerContext, userServiceRequest.UserServiceRequestMasterId);
                        }
                        result.IsSuccess = true;
                        result.Message = "Crane Permit request saved successfully";
                    }
                }
                else
                {
                    result.Message = "Server Validation Error: " + ModelState.GetModelStateError();
                    result.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving Crane Permit Request.";
            }
            return Json(result);
        }

        public ActionResult Detail(int id)
        {
            try
            {
                TempData["UserServiceRequestMasterId"] = id;
                var model = userRequestService.GetCranePermitRequestDetail(id);
                return View(model);
            }
            catch (Exception ex)
            {
                LogError(ex);
                return ErrorResult();
            }
        }

        public ActionResult Edit(int id)
        {
            ViewBag.UserServiceRequestMasterId = id;
            return View("Create");
        }


        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Initialize()
        {
            var result = new JsonResponseObject();
            try
            {
                var data = userRequestService.GetServiceRequests(UserProfile.UserId, ServiceId);
                result.Data = data;
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while obtaining list";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InitializeCreate(int? id)
        {
            try
            {
                var model = new CranePermitRequestViewModel()
                {
                    ApplicationDate = DateTime.Now.DubaiTime(),
                    //CraneTypes = craneTypeService.FindAllAsNoTracking(o => !o.IsDeleted).ToList(),
                    CraneTypeSelectionViewModels = craneTypeService.FindAllAsNoTracking(o => !o.IsDeleted).Select(o => new CraneTypeSelectionViewModel()
                    {
                        CraneTypeId = o.CraneTypeId,
                        IsOther = o.IsOther,
                        CraneTypeName = o.CraneTypeName,
                        IsSelected = false
                    }).ToList()
                };

                var reapplyData = TempData[Constants.ServiceRequestReapplyContentTempDataKey] as ServiceReapplyTempDataViewModel;
                if (reapplyData != null)
                {
                    var userServiceRequest =
                        userRequestService.Find(
                            o => o.UserServiceRequestMasterId == reapplyData.UserServiceRequestMasterId &&
                                 !o.IsDeleted && o.UserId == UserProfile.UserId && o.ServiceId == ServiceId);

                    if (userServiceRequest == null || userServiceRequest.CranePermitRequest == null)
                    {
                        return Json(new JsonResponseObject() { IsSuccess = false, Message = "Invalid User Service Request for reapply." });
                    }
                    else
                    {
                        var cranePermit = userServiceRequest.CranePermitRequest;


                        TinyMapper.Bind<CranePermitRequest, CranePermitPlanningViewModel>();

                        var cranePermitRequestViewModel = TinyMapper.Map<CranePermitRequestViewModel>(cranePermit);

                        cranePermitRequestViewModel.ParentUserServiceRequestMasterid = reapplyData.UserServiceRequestMasterId;

                        cranePermitRequestViewModel.UserServiceRequestMasterId = 0;
                        cranePermitRequestViewModel.CraneTypeSelectionViewModels = (from craneType in model.CraneTypeSelectionViewModels
                                                                                    join savedCraneType in cranePermit.CranePermitRequestSelectedCranes on craneType.CraneTypeId equals savedCraneType.CraneTypeId
                                                                                    into leftJoinCraneType
                                                                                    from defaultCraneType in leftJoinCraneType.DefaultIfEmpty()
                                                                                    select new CraneTypeSelectionViewModel()
                                                                                    {
                                                                                        CraneTypeId = craneType.CraneTypeId,
                                                                                        CraneTypeName = craneType.CraneTypeName,
                                                                                        IsOther = craneType.IsOther,
                                                                                        IsSelected = defaultCraneType != null,
                                                                                        OtherCraneTypeName = defaultCraneType != null ? defaultCraneType.OtherCraneType : ""
                                                                                    }).ToList();
                        model = cranePermitRequestViewModel;
                        //model.UploadedFiles = userRequestService.get

                    }
                }
                else if (id.HasValue)
                {
                    var userServiceRequest =
                       userRequestService.Find(
                           o => o.UserServiceRequestMasterId == id &&
                                !o.IsDeleted && o.UserId == UserProfile.UserId && o.ServiceId == ServiceId);

                    if (userServiceRequest == null || userServiceRequest.CranePermitRequest == null)
                    {
                        return Json(new JsonResponseObject() { IsSuccess = false, Message = "Invalid User Service Request for edit." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var cranePermit = userServiceRequest.CranePermitRequest;


                        TinyMapper.Bind<CranePermitRequest, CranePermitPlanningViewModel>();

                        var cranePermitRequestViewModel = TinyMapper.Map<CranePermitRequestViewModel>(cranePermit);

                        cranePermitRequestViewModel.CraneTypeSelectionViewModels = (from craneType in model.CraneTypeSelectionViewModels
                                                                                    join savedCraneType in cranePermit.CranePermitRequestSelectedCranes on craneType.CraneTypeId equals savedCraneType.CraneTypeId
                                                                                    into leftJoinCraneType
                                                                                    from defaultCraneType in leftJoinCraneType.DefaultIfEmpty()
                                                                                    select new CraneTypeSelectionViewModel()
                                                                                    {
                                                                                        CraneTypeId = craneType.CraneTypeId,
                                                                                        CraneTypeName = craneType.CraneTypeName,
                                                                                        IsOther = craneType.IsOther,
                                                                                        IsSelected = defaultCraneType != null,
                                                                                        OtherCraneTypeName = defaultCraneType != null ? defaultCraneType.OtherCraneType : ""
                                                                                    }).ToList();
                        model = cranePermitRequestViewModel;
                        //model.UploadedFiles = cranePermit.UserServiceRequestMaster.UserServiceRequestAttachments.Select(o => new FileUploadedViewModel
                        //{
                        //    ContentType = o.UserAttachment.ContentType,
                        //    FileName = o.UserAttachment.FileName,
                        //    Id = o.UserAttachmentId,
                        //    Content = o.UserAttachment.UserAttachmentData.FileContents
                        //}).ToList();

                    }
                }
                model.UserInfo = userService.GetUserBasicInfo(UserProfile.UserId).Data;
                model.EditUploadedFiles = GetUploadedFiles(reapplyData?.UserServiceRequestMasterId ?? (id ?? 0));

                return Json(new JsonResponseObject() { IsSuccess = true, Data = new { AddNewModel = model } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return Json(new JsonResponseObject() { IsSuccess = false, Message = "Error" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Reapply()
        {
            return View("Create");
        }
    }
}