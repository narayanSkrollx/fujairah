namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Dummy : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.UserServiceRequestPaymentResponses", newName: "UserServiceRequestPaymentResponse");
            AlterColumn("dbo.UserServiceRequestPaymentResponse", "OrderNumber", c => c.String(nullable: false, maxLength: 1024));
            AlterColumn("dbo.UserServiceRequestPaymentResponse", "Currency", c => c.String(nullable: false, maxLength: 1024));
            AlterColumn("dbo.UserServiceRequestPaymentResponse", "StatusFlag", c => c.String(nullable: false, maxLength: 1024));
            AlterColumn("dbo.UserServiceRequestPaymentResponse", "ErrorCode", c => c.String(nullable: false, maxLength: 1024));
            AlterColumn("dbo.UserServiceRequestPaymentResponse", "AllResponseJson", c => c.String(nullable: false, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserServiceRequestPaymentResponse", "AllResponseJson", c => c.String());
            AlterColumn("dbo.UserServiceRequestPaymentResponse", "ErrorCode", c => c.String());
            AlterColumn("dbo.UserServiceRequestPaymentResponse", "StatusFlag", c => c.String());
            AlterColumn("dbo.UserServiceRequestPaymentResponse", "Currency", c => c.String());
            AlterColumn("dbo.UserServiceRequestPaymentResponse", "OrderNumber", c => c.String());
            RenameTable(name: "dbo.UserServiceRequestPaymentResponse", newName: "UserServiceRequestPaymentResponses");
        }
    }
}
