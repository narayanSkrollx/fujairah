using System.Collections.Generic;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class Service : AuditableEntity
    {
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public bool IsDefault { get; set; }
        public string ServiceCode { get; set; }
        public decimal DefaultRate { get; set; }
        public decimal VATPercentage { get; set; }

        [JsonIgnore]
        public virtual ICollection<ServiceApprovalStage> ServiceApprovalStages { get; set; }
        [JsonIgnore]
        public virtual ICollection<UserAttachment> UserAttachments { get; set; }
        [JsonIgnore]
        public ICollection<UserServiceRequestMaster> UserServiceRequestMasters { get; set; }
    }
}