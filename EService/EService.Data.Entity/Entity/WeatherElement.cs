using System.Collections.Generic;

namespace EService.Data.Entity.Entity
{
    public class WeatherElement : ILookupItem
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public bool IsOther { get; set; }
        public virtual ICollection<MeteorologicalDataRequestWeatherElement> MeteorologicalDataRequestWeatherElements
        {
            get; set;
        }
    }
}