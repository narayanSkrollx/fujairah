﻿using System.Collections.Generic;

namespace EService.Service.Model
{
    public class ServiceApprovalStageModal
    {
        public int Index { get; set; }
        public List<ServiceApprovalStageDepartmentModal> ServiceApprovalStageDepartments { get; set; }

    }
}