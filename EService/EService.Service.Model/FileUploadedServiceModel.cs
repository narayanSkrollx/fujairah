namespace EService.Service.Model
{
    public class FileUploadedServiceModel
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
    }
}