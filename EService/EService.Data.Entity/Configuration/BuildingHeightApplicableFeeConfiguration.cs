using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class BuildingHeightApplicableFeeConfiguration : EntityTypeConfiguration<BuildingHeightApplicableFee>
    {
        public BuildingHeightApplicableFeeConfiguration()
        {
            ToTable("BuildingHeightApplicableFee");
            HasKey(x => new {x.ApplicableFeesLookUpId, x.UserServiceRequestMasterId});

            Property(x => x.ApplicableFeesLookUpId).IsRequired();
            Property(x => x.UserServiceRequestMasterId).IsRequired();
            Property(x => x.IsSelected).IsRequired();
            Property(x => x.Quantity).IsOptional();
            Property(x => x.TotalFee).IsOptional();

            HasRequired(x => x.ApplicableFeesLookUp).WithMany(x => x.BuildingHeightApplicableFees)
                .HasForeignKey(x => x.ApplicableFeesLookUpId).WillCascadeOnDelete(false);
            HasRequired(x => x.BuildingHeightRequest).WithMany(x => x.BuildingHeightApplicableFees)
                .HasForeignKey(x => x.UserServiceRequestMasterId).WillCascadeOnDelete(false);
        }
    }
}