using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class AircraftWarningLightConfiguration : EntityTypeConfiguration<AircraftWarningLightServiceRequest>
    {
        public AircraftWarningLightConfiguration()
        {
            ToTable("AircraftWarningLight");

            HasKey(o => o.UserServiceRequestMasterId);
            Property(o => o.UserServiceRequestMasterId).IsRequired();
            Property(o => o.ApplicationDate).IsRequired();
            Property(o => o.ProjectName).IsRequired().HasMaxLength(1024);
            Property(o => o.ProjectLocation).IsOptional().HasColumnType("nvarchar(max)");
            Property(o => o.ProjectDescription).IsOptional().HasColumnType("nvarchar(max)");
            #region "Applicant"

            Property(o => o.ApplicantName).IsRequired().HasMaxLength(255);
            Property(o => o.ApplicantJobTitle).IsRequired().HasMaxLength(255);
            Property(o => o.ApplicantCompanyName).IsRequired().HasMaxLength(255);
            Property(o => o.ApplicantAddress).IsOptional().HasMaxLength(255);
            Property(o => o.ApplicantContactInfo).IsOptional().HasMaxLength(255);
            Property(o => o.ApplicantCellNo).IsOptional().HasMaxLength(255);
            Property(o => o.ApplicantTelNo).IsOptional().HasMaxLength(255);
            Property(o => o.ApplicantEmail).IsOptional().HasMaxLength(255);
            Property(o => o.ApplicantWebsite).IsOptional().HasMaxLength(255);

            #endregion

            #region "Owner"
            Property(o => o.OwnerName).IsRequired().HasMaxLength(255);
            Property(o => o.OwnerCompanyName).IsRequired().HasMaxLength(255);
            Property(o => o.OwnerAddress).IsOptional().HasMaxLength(255);
            Property(o => o.OwnerContactInfo).IsOptional().HasMaxLength(255);
            Property(o => o.OwnerCellNo).IsOptional().HasMaxLength(255);
            Property(o => o.OwnerTelNo).IsOptional().HasMaxLength(255);
            Property(o => o.OwnerEmail).IsOptional().HasMaxLength(255);
            Property(o => o.OwnerWebsite).IsOptional().HasMaxLength(255);

            #endregion

            Property(o => o.NoOfStructures).IsRequired();
            Property(o => o.NoOfObstacleLights).IsRequired();
            Property(o => o.TypesOfObstacleLights).IsOptional().HasMaxLength(1024);


            Property(o => o.ProjectTypeId).IsRequired();
            Property(o => o.ProjectTypeOtherName).IsOptional().HasMaxLength(1024);

            Property(o => o.ApplicantOwnerRelationShipId).IsRequired();
            Property(o => o.ApplicantOwnerRelationshipOtherName).IsOptional().HasMaxLength(1024);

            HasRequired(o => o.ProjectType).WithMany().HasForeignKey(o => o.ProjectTypeId).WillCascadeOnDelete(false);
            HasRequired(o=>o.ApplicantOwnerRelationship).WithMany().HasForeignKey(o=>o.ApplicantOwnerRelationShipId).WillCascadeOnDelete(false);

            HasRequired(o => o.UserServiceRequestMaster)
                .WithRequiredDependent(o => o.AircraftWarningLight)
                .WillCascadeOnDelete(false);
        }
    }

    //public class AircraftWarningLightOwnerApplicantRelationConfiguration :
    //    EntityTypeConfiguration<AircraftWarningLightOwnerApplicantRelation>
    //{
    //    public AircraftWarningLightOwnerApplicantRelationConfiguration()
    //    {
    //        ToTable("AircraftWarningLightOwnerApplicantRelation");

    //        HasKey(o => o.AircraftWarningLightOwnerApplicantRelationId);
    //        Property(o => o.AircraftWarningLightOwnerApplicantRelationId)
    //            .IsRequired()
    //            .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
    //        Property(o => o.UserServiceRequestMasterId).IsRequired();
    //        Property(o => o.ApplicantOwnerRelationShipId).IsRequired();
    //        Property(o => o.OtherName).IsOptional().HasMaxLength(1024);

    //        HasRequired(o => o.ApplicantOwnerRelationship)
    //            .WithMany(o => o.AircraftWarningLightOwnerApplicantRelations)
    //            .HasForeignKey(o => o.ApplicantOwnerRelationShipId).WillCascadeOnDelete(false);
         

    //    }
    //}

    //public class AircraftWarningLightProjectTypesConfiguration :
    //    EntityTypeConfiguration<AircraftWarningLightProjectType>
    //{
    //    public AircraftWarningLightProjectTypesConfiguration()
    //    {
    //        ToTable("AircraftWarningLightProjectTypes");
    //        HasKey(o => o.UserServiceRequestMasterId);

    //        Property(o => o.UserServiceRequestMasterId).IsRequired();
    //        Property(o => o.ProjectTypeId).IsRequired();
    //        Property(o => o.OtherName).IsOptional().HasMaxLength(1024);

    //        HasRequired(o => o.ProjectType)
    //            .WithMany(o => o.AircraftWarningLightProjectTypes)
    //            .HasForeignKey(o => o.ProjectTypeId).WillCascadeOnDelete(false);

    //    }
    //}

    public class ApplicantOwnerRelationshipConfiguration : EntityTypeConfiguration<ApplicantOwnerRelationship>
    {
        public ApplicantOwnerRelationshipConfiguration()
        {
            ToTable("ApplicantOwnerRelationship");
            HasKey(o => o.Id);
            Property(o => o.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(o => o.Description).IsRequired().HasMaxLength(255);

        }
    }

    public class ProjectTypeConfiguration : EntityTypeConfiguration<ProjectType>
    {
        public ProjectTypeConfiguration()
        {
            ToTable("ProjectType");

            HasKey(o => o.Id);
            Property(o => o.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(o => o.Description).IsRequired().HasMaxLength(255);
        }
    }

    public class UserServiceRequestAttachmentConfiguration :
        EntityTypeConfiguration<UserServiceRequestAttachment>
    {
        public UserServiceRequestAttachmentConfiguration()
        {
            ToTable("UserServiceRequestAttachment");

            HasKey(o => o.UserServiceRequestAttachmentId);
            Property(o => o.UserServiceRequestAttachmentId)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(o => o.UserAttachmentId).IsRequired();
            Property(o => o.UserServiceRequestMasterId).IsRequired();

            HasRequired(o => o.UserServiceRequestMaster)
                .WithMany(o => o.UserServiceRequestAttachments)
                .HasForeignKey(o => o.UserServiceRequestMasterId);
            HasRequired(o => o.UserAttachment)
                .WithMany(o => o.UserServiceRequestAttachments)
                .HasForeignKey(o => o.UserAttachmentId);
        }
    }
}