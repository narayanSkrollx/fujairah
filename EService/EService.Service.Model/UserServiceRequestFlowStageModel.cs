﻿using System;

namespace EService.Service.Model
{
    public class UserServiceRequestFlowStageModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime RequestDateTime { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string RegistrationNo { get; set; }
        public int RequestStatus { get; set; }
        public int ServiceRequestFlowStageId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public int Index { get; set; }
        public bool IsCurrentStage { get; set; }

        public string ServiceRequestStatusName { get; set; }
        public string DepartmentServiceRequestStatusName { get; set; }
        public int ServiceRequestFlowStageDepartmentId { get; set; }
        public int DepartmentId { get; set; }
        public int? DepartmentUserId { get; set; }
        public int DepartmentStatus { get; set; }
        public DateTime? StatusChangedDateTime { get; set; }
        public bool? IsApproved { get; set; }
    }
}