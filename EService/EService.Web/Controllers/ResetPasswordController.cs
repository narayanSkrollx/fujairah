﻿using EService.Core;
using EService.Service.Logic;
using EService.Web.Controllers.Base;
using EService.Web.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace EService.Web.Controllers
{
    public class ResetPasswordController : SystemAdminBaseController
    {
        public UserService userService;

        public ResetPasswordController()
        {
            userService = new UserService(serviceContext);
        }

        public ActionResult Index()
        {
            try
            {
                return View();
            }
            catch (System.Exception ex)
            {
                LogError(ex);

                return ErrorResult("Error while initializing Reset Password");
            }
        }

        [HttpPost]
        public JsonResult Index(int userId, string password)
        {
            var result = new JsonResponseObject();
            try
            {
                if (userId != UserProfile.UserId)
                {
                    var changePasswordResult = userService.ResetPassword(userId, password);
                    result.IsSuccess = changePasswordResult.IsSuccess;
                    result.Message = changePasswordResult.Message;
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "Cannot reset own password.";
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while resetting password.";
            }
            return Json(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public JsonResult Initialize()
        {
            var result = new JsonResponseObject();
            try
            {
                var users = userService.FindAllAsNoTracking(o => !o.IsDeleted && o.UserId != UserProfile.UserId);
                result.Data = new
                {
                    Users = users.Select(o => new
                    {
                        UserId = o.UserId,
                        UserName = o.UserName
                    }),
                    UserId = (int?)null,
                    Password = ""
                };
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error whlie initializing Reset Password option.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetNewPassword()
        {
            var result = new JsonResponseObject();
            try
            {
                var newPassword = GeneratePassword.Generate();
                result.Data = newPassword;
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while generating password.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}