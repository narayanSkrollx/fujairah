﻿using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class UserServiceRequestOtherPaymentService : BusinessAccessBase<UserServiceRequestOtherPayment>
    {
        public UserServiceRequestOtherPaymentService(ServiceContext context) : base(context)
        {
        }
    }
}