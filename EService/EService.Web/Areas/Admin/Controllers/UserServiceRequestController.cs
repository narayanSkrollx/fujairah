﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EService.Core;
using EService.Service.Logic;
using EService.Service.Logic.Enum;
using EService.Service.Model;
using EService.Web.Controllers.Base;
using EService.Web.Helper;
using EService.Web.Models;
using EService.Web.Models.ServiceRequestOperation;

namespace EService.Web.Areas.Admin.Controllers
{
    public class UserServiceRequestController : SystemAdminBaseController
    {
        private ServiceInfoService serviceInfoService;
        private UserServiceRequestMasterService userRequestService;
        private ServiceContext context;

        public UserServiceRequestController()
        {
            context = new ServiceContext();
            userRequestService = new UserServiceRequestMasterService(context);
            serviceInfoService = new ServiceInfoService(context);
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Initialize()
        {
            var result = new ResponseObject();
            try
            {
                var model = new UserServiceRequestOperationViewModel
                {
                    Services = serviceInfoService.FindAllAsNoTracking(o => o.IsActive && !o.IsDeleted)
                        .Select(o => new SelectListModel()
                        {
                            Description = o.ServiceName,
                            Id = o.ServiceId
                        }).ToList(),
                    Statuses = (ServiceFlowStateEnum.Active).ToDictionary().Select(o => new SelectListModel()
                    {
                        Description = o.Value,
                        Id = o.Key
                    }).ToList()
                };
                result.IsSuccess = true;
                result.Data = new
                {
                    SearchModel = model,
                    Records = new List<UserServiceRequestModel>()
                };
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while obtaining information for service.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Search(UserServiceRequestOperationViewModel searchModel)
        {
            var result = new ResponseObject();
            try
            {
                var data = userRequestService.GetServiceRequests(null, searchModel.ServiceId, searchModel.DateFrom, searchModel.DateTo, searchModel.ServiceStatus, searchModel.ApplicantEmail, true).ToList();
                result.Data = data;
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while obtaining information for service.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InitializeDetail()
        {
            var result = new JsonResponseObject();
            try
            {
                var serviceId = TempData.Peek("ServiceRequestDetailViewKey").ToNInt32();
                if (serviceId == null)
                {
                    result.IsSuccess = false;
                    result.Message = "Invalid Service Request information provided";
                }
                else
                {
                    var id = serviceId.Value;
                    var data = userRequestService.Find(o => !o.IsDeleted && o.UserServiceRequestMasterId == id);
                    if (data == null)
                    {
                        result.IsSuccess = false;
                        result.Message = "Invalid Service Request information provided";
                    }
                    else
                    {
                        var html = "";
                        object objectData = null;
                        switch (data.ServiceId)
                        {
                            case (int)ServiceEnum.CranePermitRequest:
                                var serviceData = userRequestService.GetCranePermitRequest(id, true);
                                objectData = serviceData;
                                html = RenderPartialView("_CranePermitRequestDetail", serviceData);
                                break;
                            case (int)ServiceEnum.AircraftMaintenanceRequest:
                                var amData = userRequestService.GetAircraftMaintenanceRequest(id, true);
                                objectData = amData;
                                html = RenderPartialView("_AircraftMaintenanceRequestDetail", amData);
                                break;
                            case (int)ServiceEnum.AircraftDismantlingRequest:
                                var adData = userRequestService.GetAircraftDismantlingRequest(id, true);
                                objectData = adData;
                                html = RenderPartialView("_AircraftDsimantlingRequestDetail", adData);
                                break;
                            case (int)ServiceEnum.AircraftWarningLight:
                                var awData = userRequestService.GetAircraftWarningRequest(id);
                                objectData = awData;
                                html = RenderPartialView("_AircraftWarningLightDetail", awData);
                                break;
                            case (int)ServiceEnum.TradeLicenseRequest:
                                var tlData = userRequestService.GetTradeLicenseRequest(id, true);
                                objectData = tlData;
                                html = RenderPartialView("_TradeLicenseRequestDetail", tlData);
                                break;
                            case (int)ServiceEnum.MeteorologicalDataRequest:
                                var mrData = userRequestService.GetMeteorologicalDataRequest(id);
                                objectData = mrData;
                                html = RenderPartialView("_MeteorologicalDataRequest", mrData);
                                break;
                            case (int)ServiceEnum.ExitOfAircraftSparePartsRequest:
                                var eoSData = userRequestService.GetExitOfSparePartsRequest(id);
                                objectData = eoSData;
                                html = RenderPartialView("_ExitOfAircraftSpareParts", eoSData);
                                break;
                            case (int)ServiceEnum.BuildingHeightRequest:
                                var bHRData = userRequestService.GetBuildingHeightRequest(id);
                                objectData = bHRData;
                                html = RenderPartialView("_BuildingHeightRequestDetail", bHRData);
                                break;
                            case (int)ServiceEnum.GroundHandlingRequest:
                                var ghData = userRequestService.GetGroundHandlingRequset(id);
                                objectData = ghData;
                                html = RenderPartialView("_GroundHandlingDetail", ghData);
                                break;
                            default:
                                html = "Service Not Implemented Yet";
                                break;
                        }
                        result.IsSuccess = true;
                        var history = userRequestService.GetRequestFlowStageDepartmentHistories(id).ToList();
                        result.Data = new { Html = html, Data = objectData, History = history };
                    }
                }

            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while loading detail data.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail(int id)
        {
            try
            {
                var data = userRequestService.Find(o => !o.IsDeleted && o.UserServiceRequestMasterId == id);
                if (data == null)
                {
                    return ErrorResult("Invalid Service Request information selected. Please make sure you selected proper request.");
                }
                TempData["ServiceRequestDetailViewKey"] = id;
                return View();
            }
            catch (Exception ex)
            {
                LogError(ex);

            }
            return ErrorResult("Error while obtaining record");
        }

        [HttpPost]
        public JsonResult Approve(int id)
        {
            var result = new JsonResponseObject();
            try
            {
                var approveResult = userRequestService.Approve(id, 0, UserProfile.UserId, true);
                result.IsSuccess = approveResult.Item1;
                result.Message = approveResult.Item2;
                if (approveResult.Item1)
                {
                    //
                    TriggerEmail(FIAEmailType.Approved, approveResult.Item3.ServiceRequestFlowStageDepartmentId);
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while approving selected request.";
            }

            return Json(result);
        }
    }
}