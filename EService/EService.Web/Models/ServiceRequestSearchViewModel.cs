using System;
using System.Collections.Generic;
using EService.Service.Logic.Enum;
using EService.Service.Model;

namespace EService.Web.Models
{
    public class ServiceRequestSearchViewModel
    {
        public ServiceRequestSearchViewModel()
        {
            PageSize = 50;
            PageNo = 1;
        }

        public int? ServiceId { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public ServiceFlowStateEnum? ServiceStatus { get; set; }

        public int PageSize { get; set; }
        public int PageNo { get; set; }

        public int Skip => (PageNo - 1) * PageSize;

        public List<SelectListModel> ServiceStatusList { get; set; }
    }
}