﻿namespace EService.Web.Models
{
    public class NewRegistrationEmailModel
    {
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}