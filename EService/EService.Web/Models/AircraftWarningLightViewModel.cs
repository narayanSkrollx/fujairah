using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EService.Data.Entity;
using EService.Data.Entity.Entity;

namespace EService.Web.Models
{
    public class AircraftWarningLightViewModel : EServiceCreateBaseViewModel
    {
        public AircraftWarningLightViewModel()
        {
        }

        public int UserServiceRequestMasterId { get; set; }
        public DateTime ApplicationDate { get; set; }


        #region "Applicant"
        [Required(ErrorMessage = "Applicant Name is required.")]
        [StringLength(255, ErrorMessage = "Applicant Name length cannot be greater than 255")]
        public string ApplicantName { get; set; }

        [Required(ErrorMessage = "Applicant Job Title is required.")]
        [StringLength(255, ErrorMessage = "Applicant Job Title length cannot be greater than 255")]
        public string ApplicantJobTitle { get; set; }
        [Required(ErrorMessage = "Applicant Company Name is required.")]
        [StringLength(255, ErrorMessage = "Applicant Company Name length cannot be greater than 255")]
        public string ApplicantCompanyName { get; set; }
        [Required(ErrorMessage = "Applicant Address is required.")]
        [StringLength(255, ErrorMessage = "Applicant Address length cannot be greater than 255")]
        public string ApplicantAddress { get; set; }
        [Required(ErrorMessage = "Applicant Contact Info is required.")]
        [StringLength(255, ErrorMessage = "Applicant Contact Info length cannot be greater than 255")]
        public string ApplicantContactInfo { get; set; }
        [Required(ErrorMessage = "Applicant Cell No is required.")]
        [StringLength(255, ErrorMessage = "Applicant Cell No length cannot be greater than 255")]
        public string ApplicantCellNo { get; set; }
        [Required(ErrorMessage = "Applicant Tel No is required.")]
        [StringLength(20, ErrorMessage = "Applicant Tel No length cannot be greater than 20")]
        public string ApplicantTelNo { get; set; }
        [Required(ErrorMessage = "Applicant Email is required.")]
        [StringLength(255, ErrorMessage = "Applicant Email length cannot be greater than 255")]
        public string ApplicantEmail { get; set; }
        [StringLength(255, ErrorMessage = "Applicant Website length cannot be greater than 255")]
        public string ApplicantWebsite { get; set; }
        #endregion

        #region "Owner" 
        [Required(ErrorMessage = "Owner Name is required.")]
        [StringLength(255, ErrorMessage = "Owner Name length cannot be greater than 255")]

        public string OwnerName { get; set; }
        [Required(ErrorMessage = "Owner Company Name is required.")]
        [StringLength(255, ErrorMessage = "Owner Company Name length cannot be greater than 255")]

        public string OwnerCompanyName { get; set; }
        [Required(ErrorMessage = "Owner Address is required.")]
        [StringLength(255, ErrorMessage = "Owner Address length cannot be greater than 255")]

        public string OwnerAddress { get; set; }
        [Required(ErrorMessage = "Owner Contact Info is required.")]
        [StringLength(255, ErrorMessage = "Owner Contact Info length cannot be greater than 255")]

        public string OwnerContactInfo { get; set; }
        [Required(ErrorMessage = "Owner Cell No is required.")]
        [StringLength(255, ErrorMessage = "Owner Cell No length cannot be greater than 255")]

        public string OwnerCellNo { get; set; }
        [Required(ErrorMessage = "Owner Tel No is required.")]
        [StringLength(255, ErrorMessage = "Owner Tel No length cannot be greater than 255")]

        public string OwnerTelNo { get; set; }
        [Required(ErrorMessage = "Owner Email is required.")]
        [StringLength(255, ErrorMessage = "Owner Email length cannot be greater than 255")]

        public string OwnerEmail { get; set; }
        [StringLength(255, ErrorMessage = "Owner Website length cannot be greater than 255")]

        public string OwnerWebsite { get; set; }

        #endregion

        [Required(ErrorMessage = "Project Name is required.")]
        [StringLength(1024, ErrorMessage = "Project Name Length cannot be greater than  1024")]
        public string ProjectName { get; set; }
        public string ProjectLocation { get; set; }
        public string ProjectDescription { get; set; }
        [Required(ErrorMessage = "No of Structures is required.")]
        [Range(1, maximum: 10000, ErrorMessage = "No of structures must be between 1, 10000")]
        public int? NoOfStructures { get; set; }
        [Required(ErrorMessage = "No of Obstacles Light is required.")]
        [Range(1, maximum: 10000, ErrorMessage = "No of Obstacles Light must be between 1, 10000")]
        public int? NoOfObstacleLights { get; set; }
        [Required(ErrorMessage = "Types of Obstacles Light is required.")]
        [StringLength(1024, ErrorMessage = "Types of Obstacles Light Length cannot be greater than  1024")]
        public string TypesOfObstacleLights { get; set; }
        #region "Aircraft warning Light Project "
        public int ProjectTypeId { get; set; }
        public string ProjectTypeName { get; set; }
        public string ProjectTypeOtherName { get; set; }
        #endregion

        #region "Relation"
        public int ApplicantOwnerRelationShipId { get; set; }
        public string ApplicantOwnerRelationshipName { get; set; }
        public string ApplicantOwnerRelationshipOtherName { get; set; }
        #endregion

        public List<ProjectType> ProjectTypes { get; set; }
        public List<ApplicantOwnerRelationship> Relationships { get; set; }
    }
}