﻿$(function () {
    fia.CreateNamespace("resetPassword");

    (function (context) {
        context.Title = 'Reset Password';
        context.createForm = '#frmResetPassword';
        context.ViewModel = {};

        context.Mapping = {
            create: function (options) {
                var vm = ko.mapping.fromJS(options.data);

                vm.UserId.subscribe(function (newValue) {
                    vm.Password('');
                    if (newValue != null) {
                        ajaxRequest('/ResetPassword/GetNewPassword', 'GET', { data: {} }, function (response) {
                            if (response.IsSuccess) {
                                vm.Password(response.Data);
                            }
                            else {
                                showMessage(resp.Message, context.Title, 'error');
                            }
                        });
                    }
                });

                vm.ResetPasswordClick = function () {
                    context.ResetPassword();
                }

                return vm;
            }
        }

        context.ResetPassword = function () {
            var model = ko.toJS(context.ViewModel);
            delete model.Users;

            ajaxRequest('/ResetPassword/Index', 'POST', { data: {  userId: model.UserId, password: model.Password  } }, function (response) {
                if (response.IsSuccess) {
                    showMessage(response.Message, context.Title, 'success');
                }
                else {
                    showMessage(response.Message, context.Title, 'error');
                }
            });
        }

        context.Initialize = function () {
            ajaxRequest('/ResetPassword/Initialize', 'GET', {}, function (response) {
                if (response.IsSuccess) {
                    if (!ko.dataFor($('#mainContent')[0])) {
                        context.ViewModel = ko.mapping.fromJS(response.Data, context.Mapping);

                        ko.applyBindings(context.ViewModel, $('#mainContent')[0]);
                    }
                    else {
                        ko.mapping.fromJS(response.Data, {}, context.ViewModel);
                    }
                }
            });
        }

    })(fia.resetPassword);
});