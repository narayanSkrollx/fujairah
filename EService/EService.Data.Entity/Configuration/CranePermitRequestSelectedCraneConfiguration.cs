using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class CranePermitRequestSelectedCraneConfiguration : EntityTypeConfiguration<CranePermitRequestSelectedCrane>
    {
        public CranePermitRequestSelectedCraneConfiguration()
        {
            ToTable("CranePermitRequestSelectedCrane");
            HasKey(o => o.CranePermitRequestSelectedCraneId);
            Property(o => o.CranePermitRequestSelectedCraneId)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(o => o.UserServiceRequestMasterId).IsRequired();
            Property(o => o.CraneTypeId).IsRequired();
            Property(o => o.OtherCraneType).IsOptional().HasMaxLength(255);

            HasRequired(o => o.CraneType).WithMany(o => o.CranePermitRequestSelectedCranes).HasForeignKey(o => o.CraneTypeId).WillCascadeOnDelete(false);
            HasRequired(o => o.CranePermitRequest).WithMany(o => o.CranePermitRequestSelectedCranes).HasForeignKey(o => o.UserServiceRequestMasterId).WillCascadeOnDelete(false);
        }
    }
}