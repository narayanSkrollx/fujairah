﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EService.Core;
using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Service.Logic.Enum;
using EService.Service.Model;
using EService.Web.Controllers.Base;
using EService.Web.Helper;
using EService.Web.Models;
using EService.Web.Models.ServiceRequestOperation;
using Nelibur.ObjectMapper;

namespace EService.Web.Controllers
{
    /// <summary>
    /// controller for requesting service
    /// </summary>
    public class ServiceRequestController : ExternalUserBaseController
    {
        private ServiceInfoService serviceInfoService;
        private UserServiceRequestMasterService userRequestService;
        private ServiceContext context;
        private GroundHandlingRequestService groundHandlingRequestService;

        public ServiceRequestController()
        {
            context = new ServiceContext();
            userRequestService = new UserServiceRequestMasterService(context);
            serviceInfoService = new ServiceInfoService(context);
            groundHandlingRequestService = new GroundHandlingRequestService(context);
        }

        public ActionResult Index(int serviceId)
        {
            try
            {
                var service = serviceInfoService.Find(o => o.ServiceId == serviceId && o.IsActive && !o.IsDeleted);
                if (service != null)
                {
                    TempData["ServiceRequestViewKey"] = serviceId;
                    return View();
                }
                else
                {
                    return ErrorResult("Invalid Service info selected.");
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                return ErrorResult("Error while selecting service information.");
            }
        }

        public JsonResult Initialize()
        {
            var result = new ResponseObject();
            try
            {
                var serviceId = TempData.Peek("ServiceRequestViewKey").ToNInt32();
                if (serviceId == null)
                {
                    result.IsSuccess = false;
                    result.Message = "Invalid Operation. Please try again by navigating from dashboard.";
                }
                else
                {
                    var serviceInfo = serviceInfoService.Find(o => o.IsActive && !o.IsDeleted && o.ServiceId == serviceId);
                    var data = userRequestService.GetServiceRequests(UserProfile.UserId, serviceId).ToList();
                    result.Data = new
                    {
                        Records = data,
                        ServiceName = serviceInfo.ServiceName,
                        SearchModel = new ServiceRequestSearchViewModel()
                        {
                            ServiceId = serviceId,
                            ServiceStatusList = ServiceFlowStateEnum.Active.ToDictionary().Select(o => new SelectListModel() { Id = o.Key, Description = o.Value }).ToList()
                        }
                    };
                    result.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                LogError(ex);
                result.Message = "Error while obtaining information for service.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Search(ServiceRequestSearchViewModel searchModel)
        {
            var result = new ResponseObject();
            try
            {
                if (searchModel.ServiceId == null)
                {
                    result.IsSuccess = false;
                    result.Message = "Invalid Service information selected. Please try again by navigating to dashboard.";
                }
                else
                {
                    //var serviceInfo = serviceInfoService.Find(o => o.IsActive && !o.IsDeleted && o.ServiceId == searchModel.ServiceId);
                    var data = userRequestService.GetServiceRequests(UserProfile.UserId, searchModel.ServiceId.Value, searchModel.DateFrom, searchModel.DateTo, searchModel.ServiceStatus, "", false, searchModel.Skip, searchModel.PageSize).ToList();
                    result.Data = data;
                    result.IsSuccess = true;
                }

            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while obtaining information for service.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var serviceId = TempData["ServiceRequestViewKey"].ToNInt32();
            if (serviceId == null)
            {
                return ErrorResult("Invalid Operation. Please try again.");
            }
            var service = (ServiceEnum)serviceId.Value;
            switch (service)
            {
                case ServiceEnum.AircraftDismantlingRequest:
                    return RedirectToAction("Create", "AircraftDismantlingRequest");
                case ServiceEnum.AircraftMaintenanceRequest:
                    return RedirectToAction("Create", "AircraftMaintenanceRequest");
                case ServiceEnum.AircraftWarningLight:
                    return RedirectToAction("Create", "AircraftWarningLight");
                case ServiceEnum.BuildingHeightRequest:
                    return RedirectToAction("Create", "BuildingHeightRequest");
                case ServiceEnum.CranePermitRequest:
                    return RedirectToAction("Create", "CranePermitRequest");
                case ServiceEnum.ExitOfAircraftSparePartsRequest:
                    return RedirectToAction("Create", "ExitSparePartsRequest");
                case ServiceEnum.MeteorologicalDataRequest:
                    return RedirectToAction("Create", "MeteorologicalData");
                case ServiceEnum.TradeLicenseRequest:
                    return RedirectToAction("Create", "TradeLicenseRequest");
                case ServiceEnum.GroundHandlingRequest:
                    return RedirectToAction("Create", "GroundHandlingRequest");
                default:
                    return ErrorResult("Matching Service Information not found");
            }

        }

        public JsonResult InitializeDetail()
        {
            var result = new JsonResponseObject();
            try
            {
                var serviceId = TempData.Peek(Constants.ServiceRequestDetailTempDataKey).ToNInt32();
                if (serviceId == null)
                {
                    result.IsSuccess = false;
                    result.Message = "Invalid Service Request information provided";
                }
                else
                {
                    var id = serviceId.Value;
                    var data = userRequestService.Find(o => !o.IsDeleted && o.UserServiceRequestMasterId == id && o.UserId == UserProfile.UserId);
                    if (data == null)
                    {
                        result.IsSuccess = false;
                        result.Message = "Invalid Service Request information provided";
                    }
                    else
                    {
                        var html = "";
                        object objectData = null;
                        switch (data.ServiceId)
                        {
                            case (int)ServiceEnum.CranePermitRequest:
                                var serviceData = userRequestService.GetCranePermitRequest(id, true);
                                objectData = serviceData;
                                html = RenderPartialView("_CranePermitRequestDetail", serviceData);
                                break;
                            case (int)ServiceEnum.AircraftMaintenanceRequest:
                                var amData = userRequestService.GetAircraftMaintenanceRequest(id, true);
                                objectData = amData;
                                html = RenderPartialView("_AircraftMaintenanceRequestDetail", amData);
                                break;
                            case (int)ServiceEnum.AircraftDismantlingRequest:
                                var adData = userRequestService.GetAircraftDismantlingRequest(id, true);
                                objectData = adData;
                                html = RenderPartialView("_AircraftDsimantlingRequestDetail", adData);
                                break;
                            case (int)ServiceEnum.AircraftWarningLight:
                                var awData = userRequestService.GetAircraftWarningRequest(id);
                                objectData = awData;
                                html = RenderPartialView("_AircraftWarningLightDetail", awData);
                                break;
                            case (int)ServiceEnum.TradeLicenseRequest:
                                var tlData = userRequestService.GetTradeLicenseRequest(id, true);
                                objectData = tlData;
                                html = RenderPartialView("_TradeLicenseRequestDetail", tlData);
                                break;
                            case (int)ServiceEnum.MeteorologicalDataRequest:
                                var mrData = userRequestService.GetMeteorologicalDataRequest(id);
                                objectData = mrData;
                                html = RenderPartialView("_MeteorologicalDataRequest", mrData);
                                break;
                            case (int)ServiceEnum.ExitOfAircraftSparePartsRequest:
                                var eoSData = userRequestService.GetExitOfSparePartsRequest(id);
                                objectData = eoSData;
                                html = RenderPartialView("_ExitOfAircraftSpareParts", eoSData);
                                break;
                            case (int)ServiceEnum.BuildingHeightRequest:
                                var bHRData = userRequestService.GetBuildingHeightRequest(id);
                                objectData = bHRData;
                                html = RenderPartialView("_BuildingHeightRequestDetail", bHRData);
                                break;
                            case (int)ServiceEnum.GroundHandlingRequest:
                                var ghrData = userRequestService.GetGroundHandlingRequset(id);
                                objectData = ghrData;
                                html = RenderPartialView("_GroundHandlingDetail", ghrData);
                                break;
                            default:
                                html = "Service Not Implemented Yet";
                                break;
                        }
                        var history = userRequestService.GetRequestFlowStageDepartmentHistories(id).ToList();
                        var paidReceipts = userRequestService.GetReceiptModel(id, UserProfile.UserId);
                        var currentStageStatus = (ServiceFlowStateEnum)(data.Status);
                        if (currentStageStatus == ServiceFlowStateEnum.PaymentPending)
                        {
                            //allow payment button
                            var amounts = new
                            {
                                data.ServieBill.ChargedAmount,
                                data.ServieBill.DiscountAmount,
                            };
                        }
                        result.IsSuccess = true;
                        result.Data = new
                        {
                            Html = html,
                            Data = objectData,
                            History = history,
                            ServiceInfo = new
                            {
                                ServiceName = data.Service.ServiceName,
                                ServiceRequestDateTime = data.RequestDateTime.ToString("dd/MM/yyyy"),
                                CurrentStatus = currentStageStatus.ToString()
                            },
                            PaymentInfo = new
                            {
                                IsPaymentStage = currentStageStatus == ServiceFlowStateEnum.PaymentPending,
                                IsPaymentCompleted = currentStageStatus == ServiceFlowStateEnum.PaymentCompleted,
                                HasReceipts = paidReceipts != null && paidReceipts.IsSuccess && paidReceipts.Data.Count() > 0,
                                PaidReceipts = paidReceipts != null && paidReceipts.IsSuccess && paidReceipts.Data.Count() > 0 ? paidReceipts.Data : new List<ReceiptPrintModel>(),
                                ServiceCharge = data.ServieBill.ChargedAmount.ToString("N2")
                            }
                        };
                    }
                }

            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while loading detail data.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail(int id)
        {
            try
            {
                var data = userRequestService.Find(o => !o.IsDeleted && o.UserServiceRequestMasterId == id && o.UserId == UserProfile.UserId);
                if (data == null)
                {
                    return ErrorResult("Invalid Service Request information selected. Please make sure you selected proper request.");
                }
                TempData[Constants.ServiceRequestDetailTempDataKey] = id;
                return View();
            }
            catch (Exception ex)
            {
                LogError(ex);

            }
            return ErrorResult("Error while obtaining record");
        }

        public ActionResult Edit(int id, int serviceId)
        {
            try
            {
                var data = userRequestService.Find(o => !o.IsDeleted && o.IsDraft && o.UserServiceRequestMasterId == id && o.UserId == UserProfile.UserId);
                if (data == null)
                {
                    return ErrorResult("Invalid Service Request information selected. Please make sure you selected proper request.");
                }
                TempData[Constants.ServiceRequestDetailTempDataKey] = id;

                var service = (ServiceEnum)serviceId;
                switch (service)
                {
                    case ServiceEnum.AircraftDismantlingRequest:
                        return RedirectToAction("Edit", "AircraftDismantlingRequest", new { id = id });
                    case ServiceEnum.AircraftMaintenanceRequest:
                        return RedirectToAction("Edit", "AircraftMaintenanceRequest", new { id = id });
                    case ServiceEnum.AircraftWarningLight:
                        return RedirectToAction("Edit", "AircraftWarningLight", new { id = id });
                    case ServiceEnum.BuildingHeightRequest:
                        return RedirectToAction("Edit", "BuildingHeightRequest", new { id = id });
                    case ServiceEnum.CranePermitRequest:
                        return RedirectToAction("Edit", "CranePermitRequest", new { id = id });
                    case ServiceEnum.ExitOfAircraftSparePartsRequest:
                        return RedirectToAction("Edit", "ExitSparePartsRequest", new { id = id });
                    case ServiceEnum.MeteorologicalDataRequest:
                        return RedirectToAction("Edit", "MeteorologicalData", new { id = id });
                    case ServiceEnum.TradeLicenseRequest:
                        return RedirectToAction("Edit", "TradeLicenseRequest", new { id = id });
                    case ServiceEnum.GroundHandlingRequest:
                        return RedirectToAction("Edit", "GroundHandlingRequest", new { id = id });
                    default:
                        return ErrorResult("Matching Service Information not found");
                }
            }
            catch (Exception ex)
            {
                LogError(ex);

            }
            return ErrorResult("Error while obtaining record");
        }

        public ActionResult ReApply(int id)
        {
            try
            {
                var data = userRequestService.Find(o => !o.IsDeleted && o.UserServiceRequestMasterId == id && o.UserId == UserProfile.UserId);
                if (data == null)
                {
                    return ErrorResult("Invalid Service Request information provided");
                }
                else
                {
                    if (data.Status == (int)ServiceFlowStateEnum.Rejected)
                    {
                        var temp = new ServiceReapplyTempDataViewModel()
                        {
                            UserServiceRequestMasterId = id,
                            Service = (ServiceEnum)data.ServiceId
                        };
                        TempData[Constants.ServiceRequestReapplyContentTempDataKey] = temp;
                        var controller = "";
                        switch (data.ServiceId)
                        {
                            case (int)ServiceEnum.CranePermitRequest:
                                controller = "CranePermitRequest";
                                break;
                            case (int)ServiceEnum.AircraftMaintenanceRequest:
                                controller = "AircraftMaintenanceRequest";
                                break;
                            case (int)ServiceEnum.AircraftDismantlingRequest:
                                controller = "AircraftDismantlingRequest";
                                break;
                            case (int)ServiceEnum.AircraftWarningLight:
                                controller = "AircraftWarningLight";
                                break;
                            case (int)ServiceEnum.TradeLicenseRequest:
                                controller = "TradeLicenseRequest";
                                break;
                            case (int)ServiceEnum.MeteorologicalDataRequest:
                                controller = "MeteorologicalData";
                                break;
                            case (int)ServiceEnum.ExitOfAircraftSparePartsRequest:
                                controller = "ExitSparePartsRequest";
                                break;
                            case (int)ServiceEnum.GroundHandlingRequest:
                                controller = "GroundHandlingRequest";
                                break;
                            case (int)ServiceEnum.BuildingHeightRequest:
                                controller = "BuildingHeightRequest";
                                break;
                            default:
                                break;
                        }
                        if (!string.IsNullOrEmpty(controller))
                        {
                            return RedirectToAction("ReApply", controller);
                        }
                        else
                        {
                            return ErrorResult("Not Implemented for particular service type.");
                        }
                    }
                    else
                    {
                        return ErrorResult("Selected Service Request is not valid for reapply.");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                return ErrorResult("Error while trying to verify if service request is valid for reapply.");
            }
        }

        [HttpPost]
        public JsonResult ProceedToPayment(int id)
        {
            var result = new JsonResponseObject();
            try
            {
                var paymentRelatedInfo = userRequestService.GetPaymentRelatedServiceInfo(id, UserProfile.UserId);
                if (!paymentRelatedInfo.IsSuccess)
                {
                    result.IsSuccess = false;
                    result.Message = paymentRelatedInfo.Message;
                }
                else
                {
                    TempData[Constants.PaymentRequestMasterContentKey] = paymentRelatedInfo.Data;
                    result.IsSuccess = true;
                    result.Message = "Proceeding to payment";
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while proceeding to payment section";
            }
            return Json(result);
        }

        private GroundHandlingRequestViewModel GetGroundHandlingRequest(int id)
        {
            var contract =
                groundHandlingRequestService.Find(
                    o => o.UserServiceRequestMasterId == id && o.UserServiceRequestMaster.UserId == UserProfile.UserId);
            TinyMapper.Bind<GroundHandlingRequest, GroundHandlingRequestViewModel>();
            TinyMapper.Bind<AircraftInteriorCleaning, AircraftInteriorCleaningViewModel>();
            var model = TinyMapper.Map<GroundHandlingRequest, GroundHandlingRequestViewModel>(contract);

            //model.ServiceId = contract.UserServiceRequestMaster.ServiceId;
            model.AircraftCleaning = TinyMapper.Map<AircraftInteriorCleaning, AircraftInteriorCleaningViewModel>(contract.AircraftCleaning);
            model.GroundHandlingArcraftTypes =
                contract.AircraftTypes
                    .Select(o => new GroundHandlingArcraftTypeViewModel()
                    {
                        Description = o.AircraftType.Description,
                        IsOther = o.AircraftType.IsOther,
                        IsSelected = true,
                        AircraftTypeId = o.AircraftTypeId
                    })
                    .ToList();
            return model;
        }

        public ActionResult Receipt(int id)
        {
            try
            {
                var data = userRequestService.Find(o => !o.IsDeleted && o.UserServiceRequestMasterId == id && o.UserId == UserProfile.UserId);
                if (data == null)
                {
                    return ErrorResult("Invalid Service Request information selected. Please make sure you selected proper request.");
                }
                var model = userRequestService.GetReceiptModel(id, UserProfile.UserId);
                if (model.IsSuccess)
                {
                    TempData[Constants.ServiceRequestDetailTempDataKey] = id;
                    return View(model.Data);
                }
            }
            catch (Exception ex)
            {
                LogError(ex);

            }
            return ErrorResult("Error while obtaining record");
        }

        public ActionResult DownloadReceipt(int receiptId)
        {
            try
            {
                var receiptInfo = userRequestService.GetSingleReceipt(receiptId);
                if (receiptInfo.IsSuccess)
                {
                    var html = RenderPartialView("_ReceiptFormat", receiptInfo.Data);
                    var pdfBytes = PdfHelper.ConvertToPdf(html);
                    //SelectPdf.HtmlToPdf converter = new SelectPdf.HtmlToPdf();
                    //var baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                    //SelectPdf.PdfDocument doc = converter.ConvertHtmlString(html, baseUrl);
                    //var pdfBytes = doc.Save();
                    //doc.Close();
                    var filename = string.Format("{0}-{1}.pdf", "Receipt", receiptInfo.Data.RequestRegistrationNo);
                    return File(pdfBytes, "application/pdf", filename);
                }

                return View(receiptInfo);
            }
            catch (Exception ex)
            {
                LogError(ex);
                return ErrorResult("Error while showing receipt");
            }
        }
        public ActionResult Description(int serviceId)
        {
            try
            {
                var data = serviceInfoService.Find(o => o.ServiceId == serviceId);
                if (data == null)
                {
                    return ErrorResult("Invalid Service Request information selected. Please make sure you selected proper request.");
                }
                return View(data);
            }
            catch (Exception ex)
            {
                LogError(ex);

            }
            return ErrorResult("Error while obtaining record");
        }
    }
}