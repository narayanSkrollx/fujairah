namespace EService.Data.Entity.Entity
{
    public class MeteorologicalDataRequestWeatherElement
    {
        public int MeteorologicalDataRequestWeatherElementId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public int WeatherElementId { get; set; }
        public bool IsActive { get; set; }
        public string OtherName { get; set; }

        public virtual WeatherElement WeatherElement { get; set; }
        public virtual MeteorologicalDataRequest MeteorologicalDataRequest { get; set; }
    }
}