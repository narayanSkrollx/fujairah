using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class ForgotPasswordRequestConfguration : EntityTypeConfiguration<ForgotPasswordRequest>
    {
        public ForgotPasswordRequestConfguration()
        {
            ToTable("ForgotPasswordRequest");


            HasKey(o => o.Id);

            Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.UserId).IsRequired();
            Property(o => o.Email).IsRequired().HasMaxLength(1024);
            Property(o => o.Token).IsRequired();
            Property(o => o.ExpiryTime).IsRequired();
            Property(o => o.IsUsed).IsRequired();

            HasRequired(o => o.User).WithMany().HasForeignKey(o => o.UserId).WillCascadeOnDelete(false);

        }
    }
}