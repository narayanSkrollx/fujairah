namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PaymentResponseTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserServiceRequestPaymentResponses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderNumber = c.String(),
                        IsReceivedOnSuccess = c.Boolean(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Currency = c.String(),
                        StatusFlag = c.String(),
                        ErrorCode = c.String(),
                        AllResponseJson = c.String(),
                        UserServiceRequestMasterId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserServiceRequestBill", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserServiceRequestPaymentResponses", "UserServiceRequestMasterId", "dbo.UserServiceRequestBill");
            DropIndex("dbo.UserServiceRequestPaymentResponses", new[] { "UserServiceRequestMasterId" });
            DropTable("dbo.UserServiceRequestPaymentResponses");
        }
    }
}
