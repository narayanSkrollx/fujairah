using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class UserTypeService : BusinessAccessBase<UserType>
    {
        public UserTypeService(ServiceContext context) : base(context)
        {
        }
    }
}