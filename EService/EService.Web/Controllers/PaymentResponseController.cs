﻿using EService.Web.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using EService.Service.Logic;
using EService.Data.Entity.Entity;
using com.ni.dp.util;
using System.Linq;
using EService.Core;
using EService.Service.Logic.Enum;
using Nelibur.ObjectMapper;
using System.IO;
using EService.Web.Helper;
using EService.Web.Models.ServiceNotification;
using EService.Web.Helper.EmailSender;

namespace EService.Web.Controllers
{
    public class PaymentResponseController : Controller
    {

        private PaymentKeyLogic paymentKeyLogic;
        private ServiceContext serviceContext;
        private PaymentKey paymentKey;
        private UserServiceRequestMasterService userServiceRequestMasterService;
        private PaymentResponseService paymentResponseService;
        private UserServiceRequestOtherPaymentService otherPaymentResponseService;
        private EmailService emailService;
        private SMTPEmailSettingService emailSettingService;
        private UserServiceRequestReceiptService receiptService;

        public Dictionary<string, bool> block_Existence_Indicator = new Dictionary<string, bool>();
        public List<KeyValue> fieldExistenceIndicatorPayment = new List<KeyValue>();
        public List<KeyValue> fieldExistenceIndicatorCard = new List<KeyValue>(7);
        public List<KeyValue> fieldExistenceIndicatorStatus = new List<KeyValue>(3);
        public List<KeyValue> fieldExistenceIndicatorMerchant = new List<KeyValue>(10);
        public List<KeyValue> fieldExistenceIndicatorFraud = new List<KeyValue>(2);
        public List<KeyValue> fieldExistenceIndicatorDCC = new List<KeyValue>(5);
        public List<KeyValue> fieldExistenceIndicatorToken = new List<KeyValue>(2);
        string[] paymentKeys = { "MerchantOrderNo", "Currency", "Amount", "PayMode", "CardType", "TransactionType" };
        string[] cardKeys = { "ReferenceNumber", "TxnDate", "CardEnrollmentResponse", "EciIndicator", "GtwTraceNo", "GtwIdentifier", "AuthCode" };
        string[] statusKeys = { "StatusFlag", "ErrorCode", "ErrorMessage" };
        string[] merchantKeys = { "udf1", "udf2", "udf3", "udf4", "udf5", "udf6", "udf7", "udf8", "udf9", "udf10" };
        string[] fraudKeys = { "frauddecision", "fraudreason" };
        string[] dccKeys = { "DCCConverted", "DCCConverted Amount", "DCC Currency", "DCCMargin", "DCCExchangeRate" };
        string[] tokenKeys = { "CardToken", "CardNumber" };

        private void Initialize()
        {
            serviceContext = new ServiceContext();
            paymentKeyLogic = new PaymentKeyLogic(serviceContext);
            userServiceRequestMasterService = new UserServiceRequestMasterService(serviceContext);
            paymentKey = paymentKeyLogic.Find(o => true);
            paymentResponseService = new PaymentResponseService(serviceContext);
            otherPaymentResponseService = new UserServiceRequestOtherPaymentService(serviceContext);
            emailService = new EmailService(serviceContext);
            emailSettingService = new SMTPEmailSettingService(serviceContext);
            receiptService = new UserServiceRequestReceiptService(serviceContext);

            //
            fieldExistenceIndicatorPayment.Add(new KeyValue("MerchantOrderNo", ""));
            fieldExistenceIndicatorPayment.Add(new KeyValue("Currency", ""));
            fieldExistenceIndicatorPayment.Add(new KeyValue("Amount", ""));
            fieldExistenceIndicatorPayment.Add(new KeyValue("PayMode", ""));
            fieldExistenceIndicatorPayment.Add(new KeyValue("CardType", ""));
            fieldExistenceIndicatorPayment.Add(new KeyValue("TransactionType", ""));
            fieldExistenceIndicatorStatus.Add(new KeyValue("StatusFlag", ""));
            fieldExistenceIndicatorStatus.Add(new KeyValue("ErrorCode", ""));
            fieldExistenceIndicatorStatus.Add(new KeyValue("ErrorMessage", ""));
            fieldExistenceIndicatorMerchant.Add(new KeyValue("udf1", ""));
            fieldExistenceIndicatorMerchant.Add(new KeyValue("udf2", ""));
            fieldExistenceIndicatorMerchant.Add(new KeyValue("udf3", ""));
            fieldExistenceIndicatorMerchant.Add(new KeyValue("udf4", ""));
            fieldExistenceIndicatorMerchant.Add(new KeyValue("udf5", ""));
            fieldExistenceIndicatorMerchant.Add(new KeyValue("udf6", ""));
            fieldExistenceIndicatorMerchant.Add(new KeyValue("udf7", ""));
            fieldExistenceIndicatorMerchant.Add(new KeyValue("udf8", ""));
            fieldExistenceIndicatorMerchant.Add(new KeyValue("udf9", ""));
            fieldExistenceIndicatorMerchant.Add(new KeyValue("udf10", ""));
            fieldExistenceIndicatorMerchant.Add(new KeyValue("udf11", ""));
            fieldExistenceIndicatorMerchant.Add(new KeyValue("udf12", ""));
            fieldExistenceIndicatorMerchant.Add(new KeyValue("udf13", ""));
            fieldExistenceIndicatorFraud.Add(new KeyValue("frauddecision", ""));
            fieldExistenceIndicatorFraud.Add(new KeyValue("fraudreason", ""));
            fieldExistenceIndicatorDCC.Add(new KeyValue("DCCConverted", ""));
            fieldExistenceIndicatorDCC.Add(new KeyValue("DCCConverted Amount", ""));
            fieldExistenceIndicatorDCC.Add(new KeyValue("DCC Currency", ""));
            fieldExistenceIndicatorDCC.Add(new KeyValue("DCCMargin", ""));
            fieldExistenceIndicatorDCC.Add(new KeyValue("DCCExchangeRate", ""));
            fieldExistenceIndicatorToken.Add(new KeyValue("CardToken", ""));
            fieldExistenceIndicatorToken.Add(new KeyValue("CardNumber", ""));
        }

        public ActionResult Success()
        {
            Initialize();
            //
            List<KeyValue> decodedFields = DecodeSuccess();

            var model = Save(decodedFields, true);

            if (model.StatusFlag.ToLower() == "failure")
            {
                return View("Failure", model);
            }
            else
            {
                //send payment completed email
                EmailSenderHelper.SendCompletedEmail(ControllerContext, model.OrderNumber);
            }
            return View(model);
        }

        private UserServiceRequestPaymentResponseViewModel Save(List<KeyValue> decodedFields, bool isSuccess)
        {
            var dict = decodedFields.ToDictionary(o => o.Key, o => o.Value);
            var model = new UserServiceRequestPaymentResponse()
            {
                Amount = Convert.ToDecimal(decodedFields.FirstOrDefault(o => o.Key == "Amount").Value),
                AllResponseJson = Newtonsoft.Json.JsonConvert.SerializeObject(dict),
                Currency = decodedFields.FirstOrDefault(o => o.Key == "Currency").Value,
                ErrorCode = decodedFields.FirstOrDefault(o => o.Key == "ErrorCode").Value,
                IsReceivedOnSuccess = isSuccess && decodedFields.FirstOrDefault(o => o.Key == "StatusFlag").Value != "FAILURE",
                OrderNumber = decodedFields.FirstOrDefault(o => o.Key == "MerchantOrderNo").Value,
                StatusFlag = decodedFields.FirstOrDefault(o => o.Key == "StatusFlag").Value,
            };
            int? receiptId = null;
            var splits = model.OrderNumber.Split('_');
            model.OrderNumber = splits[0];
            if (splits.Length > 1)
            {
                receiptId = Convert.ToInt32(splits[1]);
            }
            if (model.StatusFlag != "FAILURE")
            {
                var serviceRequest = userServiceRequestMasterService.Find(o => o.RegistrationNo == model.OrderNumber);
                if (serviceRequest != null)
                {
                    var serviceBill = serviceRequest.ServieBill;
                    serviceBill.PaymentResponses.Add(model);
                    serviceBill.PaidAmount = (double)model.Amount;
                    serviceBill.PaidDateTime = DateTime.Now.DubaiTime();
                    serviceBill.PaymentMode = (int)PaymentMode.EPayment;
                    serviceRequest.Status = (int)ServiceFlowStateEnum.PaymentCompleted;
                    var receipt = receiptService.Find(o => o.UserServiceRequestReceiptId == receiptId);
                    var newNo = receiptService.GetNewReceiptNo();
                    receipt.IsPaid = true;
                    receipt.Year = newNo.Year;
                    receipt.Index = newNo.Index;
                    receipt.ReceiptNo = newNo.ReceiptNo;
                    //serviceRequest.UserServiceRequestReceipts.Add(new UserServiceRequestReceipt
                    //{
                    //    Index = 1,
                    //    //AdminFee
                    //});
                    serviceContext.Commit();
                }
                else
                {
                    paymentResponseService.Add(model);
                    serviceContext.Commit();
                }
            }

            TinyMapper.Bind<UserServiceRequestPaymentResponse, UserServiceRequestPaymentResponseViewModel>();
            var result = TinyMapper.Map<UserServiceRequestPaymentResponseViewModel>(model);
            result.AllKeyValuePairs = decodedFields;
            return result;
        }

        public ActionResult Failure()
        {
            Initialize();
            List<KeyValue> decodedFields = DecodeFailure();

            var model = Save(decodedFields, false);
            return View(model);
        }

        //*****************************************************************************************************
        /* Decodes the Data blocks using KeyArray(string Array containing Keys ) */
        //*****************************************************************************************************
        private List<KeyValue> DecodeFields(string data, string[] keyArray)
        {
            List<KeyValue> valuesDecrypted = new List<KeyValue>();
            String fieldExistenceBlock = data.Substring(0, data.IndexOf("|", 0));
            char[] charArr = fieldExistenceBlock.ToCharArray();
            string[] fieldData;

            fieldData = data.Substring(data.IndexOf("|", 0) + 1).Split(new[] { "|" }, StringSplitOptions.None);
            int j = 0;
            for (int i = 0; i < charArr.Length; i++)
            {
                if (charArr[i].ToString().Equals("1"))
                {
                    valuesDecrypted.Add(new KeyValue(keyArray[i], fieldData[j]));
                    j++;
                }
                else
                {
                    valuesDecrypted.Add(new KeyValue(keyArray[i], ""));
                }

            }

            return valuesDecrypted;
        }
        private List<KeyValue> DecodeSuccess()
        {
            string key = paymentKey.EncryptionKey;
            string strMessage = Request["responseParameter"];

            var responseparams1 = EncDec.Decrypt(key, strMessage.Substring(strMessage.IndexOf("||", 0) + 2));
            var dataWithoutMerchantID = responseparams1;
            var blockExistanceField = dataWithoutMerchantID.Substring(0, dataWithoutMerchantID.IndexOf("||", 0));
            var dataWithoutBlockExistenceField = dataWithoutMerchantID.Substring(dataWithoutMerchantID.IndexOf("||", 0) + 2);
            var splittedDataBlock = dataWithoutBlockExistenceField.Split(new[] { "||" }, StringSplitOptions.None);
            char[] charArr = blockExistanceField.ToCharArray();
            List<KeyValue> decodedFields = new List<KeyValue>();
            for (int i = 0, j = 0; i < charArr.Length; i++)
            {

                switch (i)
                {
                    case 0:
                        {
                            if (charArr[i] == '1')
                            {
                                decodedFields.AddRange(DecodeFields(splittedDataBlock[j], paymentKeys));
                                j++;
                            }
                            else
                                continue;
                        }
                        break;
                    case 1:
                        {
                            if (charArr[i] == '1')
                            {
                                decodedFields.AddRange(DecodeFields(splittedDataBlock[j], cardKeys));
                                j++;
                            }
                            else
                                continue;
                        }
                        break;
                    case 2:
                        {
                            if (charArr[i] == '1')
                            {
                                decodedFields.AddRange(DecodeFields(splittedDataBlock[j], statusKeys));
                                j++;
                            }
                            else
                                continue;
                        }
                        break;
                    case 3:
                        {
                            if (charArr[i] == '1')
                            {
                                decodedFields.AddRange(DecodeFields(splittedDataBlock[j], merchantKeys));
                                j++;
                            }
                            else
                                continue;
                        }
                        break;
                    case 4:
                        {
                            if (charArr[i] == '1')
                            {
                                decodedFields.AddRange(DecodeFields(splittedDataBlock[j], fraudKeys));
                                j++;
                            }
                            else
                                continue;
                        }
                        break;
                    case 5:
                        {
                            if (charArr[i] == '1')
                            {
                                decodedFields.AddRange(DecodeFields(splittedDataBlock[j], dccKeys));
                                j++;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        break;
                    case 6:
                        {
                            if (charArr[i] == '1')
                            {
                                decodedFields.AddRange(DecodeFields(splittedDataBlock[j], tokenKeys));
                                j++;
                            }
                            else
                                continue;
                        }
                        break;
                }
            }

            return decodedFields;
        }
        private List<KeyValue> DecodeFailure()
        {
            string strMessage = Request["responseParameter"];
            var responseparams1 = EncDec.Decrypt(paymentKey.EncryptionKey, strMessage.Substring(strMessage.IndexOf("||", 0) + 2));
            var dataWithoutMerchantID = responseparams1;
            var blockExistanceField = dataWithoutMerchantID.Substring(0, dataWithoutMerchantID.IndexOf("||", 0));
            var dataWithoutBlockExistenceField = dataWithoutMerchantID.Substring(dataWithoutMerchantID.IndexOf("||", 0) + 2);
            var splittedDataBlock = dataWithoutBlockExistenceField.Split(new[] { "||" }, StringSplitOptions.None);
            char[] charArr = blockExistanceField.ToCharArray();
            List<KeyValue> decodedFields = new List<KeyValue>();

            //*****************************************************************************************************
            /* calls Decode method for each data block fetched using blockExistanceField*/
            //*****************************************************************************************************

            for (int i = 0, j = 0; i < charArr.Length; i++)
            {

                switch (i)
                {
                    case 0:
                        {
                            if (charArr[i] == '1')
                            {
                                decodedFields.AddRange(DecodeFields(splittedDataBlock[j], paymentKeys));
                                j++;
                            }
                            else
                                continue;
                        }
                        break;
                    case 1:
                        {
                            if (charArr[i] == '1')
                            {
                                dataWithoutBlockExistenceField = dataWithoutBlockExistenceField.Substring(dataWithoutBlockExistenceField.IndexOf("||", 0) + 2);
                                decodedFields.AddRange(DecodeFields(splittedDataBlock[j], cardKeys));
                                j++;
                            }
                            else
                                continue;
                        }
                        break;
                    case 2:
                        {
                            if (charArr[i] == '1')
                            {
                                decodedFields.AddRange(DecodeFields(splittedDataBlock[j], statusKeys));
                                j++;
                            }
                            else
                                continue;
                        }
                        break;
                    case 3:
                        {
                            if (charArr[i] == '1')
                            {
                                dataWithoutBlockExistenceField = dataWithoutBlockExistenceField.Substring(dataWithoutBlockExistenceField.IndexOf("||", 0) + 2);
                                decodedFields.AddRange(DecodeFields(splittedDataBlock[j], merchantKeys));
                                j++;
                            }
                            else
                                continue;
                        }
                        break;
                    case 4:
                        {
                            if (charArr[i] == '1')
                            {
                                decodedFields.AddRange(DecodeFields(splittedDataBlock[j], fraudKeys));
                                j++;
                            }
                            else
                                continue;
                        }
                        break;
                    case 5:
                        {
                            if (charArr[i] == '1')
                            {
                                decodedFields.AddRange(DecodeFields(splittedDataBlock[j], dccKeys));
                                j++;
                            }
                            else
                                continue;
                        }
                        break;
                    case 6:
                        {
                            if (charArr[i] == '1')
                            {
                                decodedFields.AddRange(DecodeFields(splittedDataBlock[j], tokenKeys));
                                j++;
                            }
                            else
                                continue;
                        }
                        break;
                }

            }

            return decodedFields;

        }


        #region "BH Pre submit payment'
        public ActionResult PreSubmitPaymentSuccess()
        {
            Initialize();
            //

            List<KeyValue> decodedFields = DecodeSuccess();
            var dict = decodedFields.ToDictionary(o => o.Key, o => o.Value);
            var model = new UserServiceRequestOtherPayment()
            {
                Amount = Convert.ToDecimal(decodedFields.FirstOrDefault(o => o.Key == "Amount").Value),
                AllResponseJson = Newtonsoft.Json.JsonConvert.SerializeObject(dict),
                Currency = decodedFields.FirstOrDefault(o => o.Key == "Currency").Value,
                ErrorCode = decodedFields.FirstOrDefault(o => o.Key == "ErrorCode").Value,
                IsReceivedOnSuccess = true && decodedFields.FirstOrDefault(o => o.Key == "StatusFlag").Value.ToLower() != "failure",
                OrderNumber = decodedFields.FirstOrDefault(o => o.Key == "MerchantOrderNo").Value,
                StatusFlag = decodedFields.FirstOrDefault(o => o.Key == "StatusFlag").Value,
                PaymentMode = (int)PaymentMode.EPayment,

            };
            if (model.IsReceivedOnSuccess)
            {

                int? receiptId = null;
                var splits = model.OrderNumber.Split('_');
                model.OrderNumber = splits[0];
                var serviceRequest = userServiceRequestMasterService.Find(o => o.RegistrationNo == model.OrderNumber);
                if (serviceRequest != null)
                {
                    serviceRequest.UserServiceRequestOtherPayments.Add(model);
                    serviceRequest.Status = (int)ServiceFlowStateEnum.New;
                    serviceRequest.IsDraft = false;
                    var receipt = receiptService.Find(o => o.UserServiceRequestReceiptId == receiptId);
                    var newNo = receiptService.GetNewReceiptNo();
                    receipt.IsPaid = true;
                    receipt.Year = newNo.Year;
                    receipt.Index = newNo.Index;
                    receipt.ReceiptNo = newNo.ReceiptNo;
                    serviceContext.Commit();

                    //new request email
                    EmailSenderHelper.SendNewRequestEmail(ControllerContext, serviceRequest.UserServiceRequestMasterId);
                }
                else
                {
                    otherPaymentResponseService.Add(model);
                    serviceContext.Commit();
                }
                //do BH Specific pre payment 
            }

            var amodel = new UserServiceRequestPaymentResponse()
            {
                Amount = Convert.ToDecimal(decodedFields.FirstOrDefault(o => o.Key == "Amount").Value),
                AllResponseJson = Newtonsoft.Json.JsonConvert.SerializeObject(dict),
                Currency = decodedFields.FirstOrDefault(o => o.Key == "Currency").Value,
                ErrorCode = decodedFields.FirstOrDefault(o => o.Key == "ErrorCode").Value,
                IsReceivedOnSuccess = true,
                OrderNumber = decodedFields.FirstOrDefault(o => o.Key == "MerchantOrderNo").Value,
                StatusFlag = decodedFields.FirstOrDefault(o => o.Key == "StatusFlag").Value,
            };
            TinyMapper.Bind<UserServiceRequestPaymentResponse, UserServiceRequestPaymentResponseViewModel>();
            var result = TinyMapper.Map<UserServiceRequestPaymentResponseViewModel>(amodel);
            result.AllKeyValuePairs = decodedFields;
            if (!model.IsReceivedOnSuccess)
            {
                return View("PreSubmitPaymentFailure", result);
            }
            return View(result);
        }


        public ActionResult PreSubmitPaymentFailure()
        {
            Initialize();
            List<KeyValue> decodedFields = DecodeFailure();

            //var model = Save(decodedFields, false);
            //bh specific payment failure response

            var dict = decodedFields.ToDictionary(o => o.Key, o => o.Value);
            var model = new UserServiceRequestOtherPayment()
            {
                Amount = Convert.ToDecimal(decodedFields.FirstOrDefault(o => o.Key == "Amount").Value),
                AllResponseJson = Newtonsoft.Json.JsonConvert.SerializeObject(dict),
                Currency = decodedFields.FirstOrDefault(o => o.Key == "Currency").Value,
                ErrorCode = decodedFields.FirstOrDefault(o => o.Key == "ErrorCode").Value,
                IsReceivedOnSuccess = false,
                OrderNumber = decodedFields.FirstOrDefault(o => o.Key == "MerchantOrderNo").Value,
                StatusFlag = decodedFields.FirstOrDefault(o => o.Key == "StatusFlag").Value,
                PaymentMode = (int)PaymentMode.EPayment,

            };
            var serviceRequest = userServiceRequestMasterService.Find(o => o.RegistrationNo == model.OrderNumber);
            if (serviceRequest != null)
            {
                serviceRequest.UserServiceRequestOtherPayments.Add(model);
                //serviceRequest.Status = (int)ServiceFlowStateEnum.New;
                //serviceRequest.IsDraft = false;
                serviceContext.Commit();
            }
            else
            {
            }
            var amodel = new UserServiceRequestPaymentResponse()
            {
                Amount = Convert.ToDecimal(decodedFields.FirstOrDefault(o => o.Key == "Amount").Value),
                AllResponseJson = Newtonsoft.Json.JsonConvert.SerializeObject(dict),
                Currency = decodedFields.FirstOrDefault(o => o.Key == "Currency").Value,
                ErrorCode = decodedFields.FirstOrDefault(o => o.Key == "ErrorCode").Value,
                IsReceivedOnSuccess = false,
                OrderNumber = decodedFields.FirstOrDefault(o => o.Key == "MerchantOrderNo").Value,
                StatusFlag = decodedFields.FirstOrDefault(o => o.Key == "StatusFlag").Value,
            };
            TinyMapper.Bind<UserServiceRequestPaymentResponse, UserServiceRequestPaymentResponseViewModel>();
            var result = TinyMapper.Map<UserServiceRequestPaymentResponseViewModel>(amodel);
            result.AllKeyValuePairs = decodedFields;
            return View(result);
        }
        #endregion

        protected virtual string RenderPartialView(string partialViewName, object model = null)
        {
            if (ControllerContext == null)
                return string.Empty;

            if (string.IsNullOrEmpty(partialViewName))
                throw new ArgumentNullException("partialViewName");

            ModelState.Clear();//Remove possible model binding error.

            ViewData.Model = model;//Set the model to the partial view

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, partialViewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }

        protected void TriggerEmail(string regNo)
        {
            //var userServiceRequest = userServiceRequestMasterService.GetServiceRequest(userServiceRequestMasterId);
            var smtpSetting = emailSettingService.Find(o => true);
            //var receipients = emailService.GetApproveRejectEmailRecepients(serviceFlowRequestDepartmentId);
            var request =
             userServiceRequestMasterService.Find(
                 o => o.RegistrationNo == regNo);
            var htmlContent = "";

            var aModel = new NOCCollectionNotificationViewModel()
            {
                RequestRegistrationNo = request.RegistrationNo
            };
            String[] receipients = new string[] { request.User.Email };

            htmlContent = RenderPartialView("_NoticeForNOCCollectionEmailTemplate", aModel);

            EmailHelper.SendEmail(smtpSetting.From, receipients, new string[] { }, new string[] { }, "NOC Collection", htmlContent, smtpSetting);
        }
    }
}