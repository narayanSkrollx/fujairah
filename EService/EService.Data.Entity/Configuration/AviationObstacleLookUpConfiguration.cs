using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class AviationObstacleLookUpConfiguration : EntityTypeConfiguration<AviationObstacleLookUp>
    {
        public AviationObstacleLookUpConfiguration()
        {
            ToTable("AviationObstacleLookUp");
            HasKey(x => x.Id);

            Property(x => x.Id).IsRequired();
            Property(x => x.Description).IsRequired().HasMaxLength(225);
            Property(x => x.Active).IsRequired();
        }
    }
}