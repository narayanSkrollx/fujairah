using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class UserServiceRequestMaster : AuditableEntity
    {
        public int UserServiceRequestMasterId { get; set; }
        public int UserId { get; set; }
        public DateTime RequestDateTime { get; set; }
        public int ServiceId { get; set; }
        public int Status { get; set; }
        public string RegistrationNoServiceCode { get; set; }
        public int RegistrationNoYear { get; set; }
        public int RegistrationNoMonth { get; set; }
        public int RegistrationNoIndex { get; set; }
        public decimal VAT { get; set; }
        public int? ParentUserServiceRequestMasterId { get; set; }
        public string RegistrationNo { get; set; }

        public bool IsDraft { get; set; }
        public int PaymentMode { get; set; }
        [JsonIgnore]
        public UserServiceRequestMaster ParentUserServiceRequestMaster { get; set; }
        [JsonIgnore]
        public virtual User User { get; set; }
        [JsonIgnore]
        public virtual Service Service { get; set; }
        [JsonIgnore]
        public virtual LandingPermissionRequest LandingPermissionRequest { get; set; }
        [JsonIgnore]
        public virtual CranePermitRequest CranePermitRequest { get; set; }
        [JsonIgnore]
        public virtual AircraftWarningLightServiceRequest AircraftWarningLight { get; set; }
        [JsonIgnore]
        public virtual ICollection<ServiceRequestFlowStage> ServiceRequestFlowStages { get; set; }
        [JsonIgnore]
        public virtual AircraftDismantlingNOCRequest AircraftDismantlingNOCRequest { get; set; }
        [JsonIgnore]
        public virtual AircraftMaintenanceRequest AircraftMaintenanceRequest { get; set; }
        [JsonIgnore]
        public virtual TradeLicenseRequest TradeLicenseRequest { get; set; }

        [JsonIgnore]
        public virtual ExitOfAircraftSparePartsRequest ExitOfAircraftSparePartsRequest { get; set; }
        [JsonIgnore]
        public virtual MeteorologicalDataRequest MeteorologicalDataRequest { get; set; }
        [JsonIgnore]
        public virtual BuildingHeightRequest BuildingHeightRequest { get; set; }
        [JsonIgnore]
        public virtual ICollection<UserServiceRequestAttachment> UserServiceRequestAttachments { get; set; }
        [JsonIgnore]
        public virtual UserServiceRequestBill ServieBill { get; set; }
        [JsonIgnore]
        public virtual GroundHandlingRequest GroundHandlingRequest { get; set; }
        [JsonIgnore]
        public virtual ICollection<UserServiceRequestOtherPayment> UserServiceRequestOtherPayments { get; set; }
        [JsonIgnore]
        public virtual ICollection<UserServiceRequestReceipt> UserServiceRequestReceipts { get; set; }
    }
}