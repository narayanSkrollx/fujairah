using EService.Data.Entity.Entity;
using EService.Service.Logic.Enum;

namespace EService.Service.Logic
{
    public class ServiceRequestFlowStageDepartmentService : BaseEService<ServiceRequestFlowStageDepartment>
    {
        public ServiceRequestFlowStageDepartmentService(ServiceContext context) : base(context)
        {
        }

        public override ServiceEnum Service { get; }
    }
}