using System;
using EService.Data.Entity.Entity;
using EService.Service.Logic.Enum;

namespace EService.Service.Logic
{
    public class ServiceResolver : BusinessAccessBase<UserServiceRequestMaster>
    {
        public IServiceLogic ResolveServiceFromUserServiceRequestMasterId(int id)
        {
            var service = Find(o => o.UserServiceRequestMasterId == id);
            switch (service.ServiceId)
            {
                case (int)ServiceEnum.CranePermitRequest:
                    return new CranePermitRequestService(Context);
                case (int)ServiceEnum.AircraftMaintenanceRequest:
                    return new AircraftMaintenanceRequestService(Context);
                case (int)ServiceEnum.AircraftDismantlingRequest:
                    return new AircraftDismantlingNOCRequestService(Context);
                case (int)ServiceEnum.AircraftWarningLight:
                    return new AircraftWarningLightService(Context);
                case (int)ServiceEnum.TradeLicenseRequest:
                    return new TradeLicenseRequestService(Context);
                case (int)ServiceEnum.MeteorologicalDataRequest:
                    return new MeteorologicalDataRequestService(Context);
                case (int)ServiceEnum.ExitOfAircraftSparePartsRequest:
                    return new ExitOfAircraftSparePartsRequestService(Context);
                case (int)ServiceEnum.BuildingHeightRequest:
                    return new BuildingHeightRequestService(Context);
                case (int)ServiceEnum.GroundHandlingRequest:
                    return new GroundHandlingRequestService(Context);
                default:
                    throw new NotImplementedException();
                    break;
            }
        }

        public ServiceResolver(ServiceContext context) : base(context)
        {
        }
    }
}