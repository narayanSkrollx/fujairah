using System.Net.Configuration;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class CranePermitRequestUserAttachment
    {
        public int CranePermitRequestUserAttachmentId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public int UserAttachmentId { get; set; }
        [JsonIgnore]
        public virtual CranePermitRequest CranePermitRequest { get; set; }
        [JsonIgnore]
        public virtual UserAttachment UserAttachment { get; set; }
    }
}
