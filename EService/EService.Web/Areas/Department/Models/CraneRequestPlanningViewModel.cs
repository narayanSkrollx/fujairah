﻿using EService.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EService.Web.Areas.Department.Models
{
    public class CraneRequestPlanningViewModel
    {
        public CraneRequestPlanningModel PlanningModel { get; set; }
        public string InfringementTypes { get; set; }
        public HttpPostedFileBase[] FileUpload { get; set; }
    }

    public class CraneRequestSafetyViewModel
    {
        public CraneRequestQASafetyModel QASafetyModel { get; set; }
        public HttpPostedFileBase[] FileUpload { get; set; }
    }
}