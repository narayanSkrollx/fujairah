﻿using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class UserServiceRequestPaymentResponse
    {
        public int Id { get; set; }
        public string OrderNumber { get; set; }
        public bool IsReceivedOnSuccess { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string StatusFlag { get; set; }
        public string ErrorCode { get; set; }
        public string AllResponseJson { get; set; }
        public int? UserServiceRequestMasterId { get; set; }
        [JsonIgnore]
        public virtual UserServiceRequestBill ServiceRequestBill { get; set; }
    }
}