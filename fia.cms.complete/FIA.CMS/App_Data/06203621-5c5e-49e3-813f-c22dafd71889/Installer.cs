﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Hosting;
using System.Xml;
using umbraco;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.packager.standardPackageActions;
using umbraco.interfaces;
using umbraco.DataLayer;
using umbraco.cms.businesslogic.member;
using System.Web;

namespace Designit.Umb.Newsletter.PackageInstal
{
    public class Installer : IPackageAction
    {
        #region IPackageAction Members

        public string Alias()
        {

            return "dinewsletter";
        }

        public bool Execute(string packageName, System.Xml.XmlNode xmlData)
        {
            CreateTables();
            SqlEntries();
            WriteToFiles();
            MemberGroup.MakeNew("Bounced", new User(0));
            string path = HostingEnvironment.MapPath("/designit/newsletter/masterpages");
            if (path != null) Directory.CreateDirectory(path);

            return true;
        }


        public System.Xml.XmlNode SampleXml()
        {
            string sample = "<Action runat=\"install\" undo=\"true/false\" alias=\"dinewsletterAction \"/>";
            return umbraco.cms.businesslogic.packager.standardPackageActions.helper.parseStringToXmlNode(sample);
        }

        public bool Undo(string packageName, System.Xml.XmlNode xmlData)
        {
            umbraco.BusinessLogic.Application.SqlHelper.ExecuteNonQuery("delete from umbracoUser2app where app = 'dinewsletter'");
            DropTables();
            DeleteSqlEntries();
            DeleteFromFiles();
            MemberGroup.GetByName("Bounced").delete();
            string path = HostingEnvironment.MapPath("/designit");
            Directory.Delete(path, true);
            path = HostingEnvironment.MapPath("/config/designit");
            Directory.Delete(path, true);
            path = HostingEnvironment.MapPath("/umbraco/plugins/designit");
            Directory.Delete(path, true);
            return true;
        }

        private void CreateTables()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("CREATE TABLE [dbo].[newsLetterUnsubscribeMember](");
            sb.AppendLine("	[id] [int] IDENTITY(1,1) NOT NULL,");
            sb.AppendLine("	[guid] [uniqueidentifier] NULL,");
            sb.AppendLine("	[groupId] [int] NULL,");
            sb.AppendLine("	[memberId] [int] NULL,");
            sb.AppendLine(" CONSTRAINT [PK_newsLetterUnsubscribeMember] PRIMARY KEY CLUSTERED ");
            sb.AppendLine("(");
            sb.AppendLine("	[id] ASC");
            sb.AppendLine(")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]");
            sb.AppendLine(") ON [PRIMARY]");
            sb.AppendLine(" ");
            sb.AppendLine("CREATE TABLE [dbo].[newsletterTracking](");
            sb.AppendLine("	[id] [int] IDENTITY(1,1) NOT NULL,");
            sb.AppendLine("	[campaignId] [int] NULL,");
            sb.AppendLine("	[url] [nvarchar](200) NULL,");
            sb.AppendLine("	[clicks] [int] NULL,");
            sb.AppendLine(" CONSTRAINT [PK_newsletterTracking] PRIMARY KEY CLUSTERED");
            sb.AppendLine("(");
            sb.AppendLine("	[id] ASC");
            sb.AppendLine(")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]");
            sb.AppendLine(") ON [PRIMARY]");
            sb.AppendLine(" ");
            sb.AppendLine("CREATE TABLE [dbo].[newsLetterTemplate](");
            sb.AppendLine("	[id] [int] IDENTITY(1,1) NOT NULL,");
            sb.AppendLine("	[name] [nvarchar](100) NULL,");
            sb.AppendLine("	[alias] [nvarchar](100) NULL,");
            sb.AppendLine("	[design] [ntext] NULL,");
            sb.AppendLine(" CONSTRAINT [PK_newsLetterTemplate] PRIMARY KEY CLUSTERED ");
            sb.AppendLine("(");
            sb.AppendLine("	[id] ASC");
            sb.AppendLine(")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]");
            sb.AppendLine(") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]");
            sb.AppendLine(" ");
            sb.AppendLine("CREATE TABLE [dbo].[newsLetterSoftBounces](");
            sb.AppendLine("	[id] [int] IDENTITY(1,1) NOT NULL,");
            sb.AppendLine("	[memberId] [int] NULL,");
            sb.AppendLine("	[bounces] [int] NULL,");
            sb.AppendLine(" CONSTRAINT [PK_newsLetterSoftBounces] PRIMARY KEY CLUSTERED");
            sb.AppendLine("(");
            sb.AppendLine("	[id] ASC");
            sb.AppendLine(")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]");
            sb.AppendLine(") ON [PRIMARY]");
            sb.AppendLine(" ");
            sb.AppendLine("CREATE TABLE [dbo].[newsLetterSendMail](");
            sb.AppendLine("	[id] [int] IDENTITY(1,1) NOT NULL,");
            sb.AppendLine("	[campaignId] [int] NULL,");
            sb.AppendLine("	[memberId] [int] NULL,");
            sb.AppendLine("	[Send] [nvarchar](5) NULL,");
            sb.AppendLine(" CONSTRAINT [PK_newsLetterSendMail] PRIMARY KEY CLUSTERED");
            sb.AppendLine("(");
            sb.AppendLine("	[id] ASC");
            sb.AppendLine(")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]");
            sb.AppendLine(") ON [PRIMARY]");
            sb.AppendLine(" ");
            sb.AppendLine("CREATE TABLE [dbo].[newsLetterCampaignTempContent](");
            sb.AppendLine("	[id] [int] IDENTITY(1,1) NOT NULL,");
            sb.AppendLine("	[tempContent] [ntext] NULL,");
            sb.AppendLine(" CONSTRAINT [PK_newsLetterCampaignTempContent] PRIMARY KEY CLUSTERED");
            sb.AppendLine("(");
            sb.AppendLine("	[id] ASC");
            sb.AppendLine(")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]");
            sb.AppendLine(") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]");
            sb.AppendLine(" ");
            sb.AppendLine("CREATE TABLE [dbo].[newsLetterCampaignNewsletter](");
            sb.AppendLine("	[id] [int] IDENTITY(1,1) NOT NULL,");
            sb.AppendLine("	[camptitle] [nvarchar](100) NULL,");
            sb.AppendLine("	[campsubject] [nvarchar](100) NULL,");
            sb.AppendLine("	[campstatus] [nvarchar](100) NULL,");
            sb.AppendLine("	[campfrom] [nvarchar](100) NULL,");
            sb.AppendLine("	[campreplyMail] [nvarchar](100) NULL,");
            sb.AppendLine("	[campTemplate] [nvarchar](100) NULL,");
            sb.AppendLine("	[campcontent] [ntext] NULL,");
            sb.AppendLine("	[campTemplatecontent] [ntext] NULL,");
            sb.AppendLine("	[campTrack] [nvarchar](5) NULL,");
            sb.AppendLine("	[campGoogle] [nvarchar](5) NULL,");
            sb.AppendLine("	[campSentTime] [nvarchar](100) NULL,");
            sb.AppendLine(" CONSTRAINT [PK_newsLetterCampaignNewsletter] PRIMARY KEY CLUSTERED");
            sb.AppendLine("(");
            sb.AppendLine("	[id] ASC");
            sb.AppendLine(")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]");
            sb.AppendLine(") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]");
            sb.AppendLine(" ");
            sb.AppendLine("CREATE TABLE [dbo].[newsLetterCampaignDelayedSend](");
            sb.AppendLine("	[id] [int] IDENTITY(1,1) NOT NULL,");
            sb.AppendLine("	[campaignId] [int] NULL,");
            sb.AppendLine("	[timeStamp] [nvarchar](200) NULL,");
            sb.AppendLine("	[timezone] [nvarchar](100) NULL,");
            sb.AppendLine(" CONSTRAINT [PK_newsLetterCampaignDelayedSend] PRIMARY KEY CLUSTERED ");
            sb.AppendLine("(");
            sb.AppendLine("	[id] ASC");
            sb.AppendLine(")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]");
            sb.AppendLine(") ON [PRIMARY]");
            sb.AppendLine(" ");
            sb.AppendLine("CREATE TABLE [dbo].[newsLetterCampaign2Group](");
            sb.AppendLine("	[id] [int] IDENTITY(1,1) NOT NULL,");
            sb.AppendLine("	[campaignId] [int] NULL,");
            sb.AppendLine("	[groupName] [nvarchar](100) NULL,");
            sb.AppendLine("	[groupId] [int] NULL,");
            sb.AppendLine(" CONSTRAINT [PK_newsLetterCampaign2Group] PRIMARY KEY CLUSTERED");
            sb.AppendLine("(");
            sb.AppendLine("	[id] ASC");
            sb.AppendLine(")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]");
            sb.AppendLine(") ON [PRIMARY]");
            sb.AppendLine(" ");
            sb.AppendLine("CREATE TABLE [dbo].[newsLetterCampaign](");
            sb.AppendLine("	[id] [int] IDENTITY(1,1) NOT NULL,");
            sb.AppendLine("	[alias] [nvarchar](100) NULL,");
            sb.AppendLine(" CONSTRAINT [PK_newsLetterCampaign] PRIMARY KEY CLUSTERED");
            sb.AppendLine("(");
            sb.AppendLine("	[id] ASC");
            sb.AppendLine(")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]");
            sb.AppendLine(") ON [PRIMARY]");

            umbraco.BusinessLogic.Application.SqlHelper.ExecuteNonQuery(sb.ToString());
        }

        private void SqlEntries()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("insert into newsLetterCampaignTempContent(tempContent) values('');");
            sb.AppendLine("");
            sb.AppendLine("insert into newsletterCampaign(alias) values('All Campaigns');");
            sb.AppendLine("");
            sb.AppendLine("insert into newsletterCampaign(alias) values('Drafts');");
            sb.AppendLine("");
            sb.AppendLine("insert into newsletterCampaign(alias) values('Archive');");
            sb.AppendLine("");
            sb.AppendLine("insert into umbracoApp(sortOrder, appAlias, appIcon, appName) values(10, 'dinewsletter', 'designit/newsletter/newsletter1.gif', 'Newsletter');");
            sb.AppendLine("");
            sb.AppendLine("insert into umbracoUser2app ([user], app) values(0, 'dinewsletter');");
            sb.AppendLine("");
            sb.AppendLine("insert into umbracoAppTree(treeSilent, treeInitialize, treeSortOrder, appAlias, treeAlias, treeTitle, treeIconClosed, treeIconOpen, treeHandlerAssembly, treeHandlerType)");
            sb.AppendLine("values('False', 'True', 0, 'dinewsletter', 'dinewsletterCampaign', 'Campaign', '.sprTreeFolder', '.sprTreeFolder_o', 'Designit.Umb.Newsletter.Bll', 'loadNewsletterCampaign');");
            sb.AppendLine("");
            sb.AppendLine("insert into umbracoAppTree(treeSilent, treeInitialize, treeSortOrder, appAlias, treeAlias, treeTitle, treeIconClosed, treeIconOpen, treeHandlerAssembly, treeHandlerType)");
            sb.AppendLine("values('False', 'True', 2, 'dinewsletter', 'dinewsletterMailSettings', 'Mail settings', '.sprTreeFolder', '.sprTreeFolder_o', 'Designit.Umb.Newsletter.Bll', 'loadNewsletterMailSettings');");
            sb.AppendLine("");
            sb.AppendLine("insert into umbracoAppTree(treeSilent, treeInitialize, treeSortOrder, appAlias, treeAlias, treeTitle, treeIconClosed, treeIconOpen, treeHandlerAssembly, treeHandlerType)");
            sb.AppendLine("values('False', 'True', 1, 'dinewsletter', 'dinewsletterTemplate', 'Template', '.sprTreeFolder', '.sprTreeFolder_o', 'Designit.Umb.Newsletter.Bll', 'loadNewsletterTemplate');");
            umbraco.BusinessLogic.Application.SqlHelper.ExecuteNonQuery(sb.ToString());
        }

        private void WriteToFiles()
        {
            //writing to restExtensions
            var path = HostingEnvironment.MapPath("/config/restExtensions.config");
            var document = new XmlDocument();
            document.Load(path);
            var restNode = document.SelectSingleNode("/RestExtensions");

            XmlNode newChild = document.CreateElement("ext");
            XmlAttribute attribute = document.CreateAttribute("assembly");
            attribute.Value = "Designit.Umb.Newsletter.Bll";
            newChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("type");
            attribute.Value = "Designit.Umb.Newsletter.Bll.EmbedNewsletter";
            newChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("alias");
            attribute.Value = "embedNewsletter";
            newChild.Attributes.Append(attribute);
            restNode.AppendChild(newChild);

            var node = document.SelectSingleNode("/RestExtensions/ext[@alias = 'embedNewsletter']");
            XmlNode permissionChild = document.CreateElement("permission");
            attribute = document.CreateAttribute("method");
            attribute.Value = "Emb";
            permissionChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("returnXml");
            attribute.Value = "false";
            permissionChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("allowAll");
            attribute.Value = "true";
            permissionChild.Attributes.Append(attribute);
            node.AppendChild(permissionChild);

            permissionChild = document.CreateElement("permission");
            attribute = document.CreateAttribute("method");
            attribute.Value = "Embstep4";
            permissionChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("returnXml");
            attribute.Value = "false";
            permissionChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("allowAll");
            attribute.Value = "true";
            permissionChild.Attributes.Append(attribute);
            node.AppendChild(permissionChild);

            permissionChild = document.CreateElement("permission");
            attribute = document.CreateAttribute("method");
            attribute.Value = "EmbSend";
            permissionChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("returnXml");
            attribute.Value = "false";
            permissionChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("allowAll");
            attribute.Value = "true";
            permissionChild.Attributes.Append(attribute);
            node.AppendChild(permissionChild);

            permissionChild = document.CreateElement("permission");
            attribute = document.CreateAttribute("method");
            attribute.Value = "SendCampaign";
            permissionChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("returnXml");
            attribute.Value = "false";
            permissionChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("allowAll");
            attribute.Value = "true";
            permissionChild.Attributes.Append(attribute);
            node.AppendChild(permissionChild);

            newChild = document.CreateElement("ext");
            attribute = document.CreateAttribute("assembly");
            attribute.Value = "Designit.Umb.Newsletter.Bll";
            newChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("type");
            attribute.Value = "Designit.Umb.Newsletter.Bll.Helper";
            newChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("alias");
            attribute.Value = "newsletterhelper";
            newChild.Attributes.Append(attribute);
            restNode.AppendChild(newChild);

            node = document.SelectSingleNode("/RestExtensions/ext[@alias = 'newsletterhelper']");
            permissionChild = document.CreateElement("permission");
            attribute = document.CreateAttribute("method");
            attribute.Value = "AjaxStatus";
            permissionChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("returnXml");
            attribute.Value = "false";
            permissionChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("allowAll");
            attribute.Value = "true";
            permissionChild.Attributes.Append(attribute);
            node.AppendChild(permissionChild);

            document.Save(path);

            //writing to language file
            path = HostingEnvironment.MapPath("/umbraco/config/lang/en.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("language/area[@alias='sections']");
            XmlNode languageChild = document.CreateElement("key");
            attribute = document.CreateAttribute("alias");
            attribute.Value = "dinewsletter";
            languageChild.Attributes.Append(attribute);
            languageChild.InnerText = "Newsletter";
            node.AppendChild(languageChild);
            document.Save(path);

            path = HostingEnvironment.MapPath("/umbraco/config/lang/da.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("language/area[@alias='sections']");
            languageChild = document.CreateElement("key");
            attribute = document.CreateAttribute("alias");
            attribute.Value = "dinewsletter";
            languageChild.Attributes.Append(attribute);
            languageChild.InnerText = "Nyhedsbrev";
            node.AppendChild(languageChild);
            document.Save(path);

            path = HostingEnvironment.MapPath("/umbraco/config/lang/de.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("language/area[@alias='sections']");
            languageChild = document.CreateElement("key");
            attribute = document.CreateAttribute("alias");
            attribute.Value = "dinewsletter";
            languageChild.Attributes.Append(attribute);
            languageChild.InnerText = "Newsletter";
            node.AppendChild(languageChild);
            document.Save(path);

            path = HostingEnvironment.MapPath("/umbraco/config/lang/es.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("language/area[@alias='sections']");
            languageChild = document.CreateElement("key");
            attribute = document.CreateAttribute("alias");
            attribute.Value = "dinewsletter";
            languageChild.Attributes.Append(attribute);
            languageChild.InnerText = "Boletín";
            node.AppendChild(languageChild);
            document.Save(path);

            path = HostingEnvironment.MapPath("/umbraco/config/lang/fr.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("language/area[@alias='sections']");
            languageChild = document.CreateElement("key");
            attribute = document.CreateAttribute("alias");
            attribute.Value = "dinewsletter";
            languageChild.Attributes.Append(attribute);
            languageChild.InnerText = "Bulletin";
            node.AppendChild(languageChild);
            document.Save(path);

            path = HostingEnvironment.MapPath("/umbraco/config/lang/it.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("language/area[@alias='sections']");
            languageChild = document.CreateElement("key");
            attribute = document.CreateAttribute("alias");
            attribute.Value = "dinewsletter";
            languageChild.Attributes.Append(attribute);
            languageChild.InnerText = "Notiziario";
            node.AppendChild(languageChild);
            document.Save(path);

            path = HostingEnvironment.MapPath("/umbraco/config/lang/nl.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("language/area[@alias='sections']");
            languageChild = document.CreateElement("key");
            attribute = document.CreateAttribute("alias");
            attribute.Value = "dinewsletter";
            languageChild.Attributes.Append(attribute);
            languageChild.InnerText = "Nieuwsbrief";
            node.AppendChild(languageChild);
            document.Save(path);

            path = HostingEnvironment.MapPath("/umbraco/config/lang/no.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("language/area[@alias='sections']");
            languageChild = document.CreateElement("key");
            attribute = document.CreateAttribute("alias");
            attribute.Value = "dinewsletter";
            languageChild.Attributes.Append(attribute);
            languageChild.InnerText = "Nyhetsbrev";
            node.AppendChild(languageChild);
            document.Save(path);

            path = HostingEnvironment.MapPath("/umbraco/config/lang/sv.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("language/area[@alias='sections']");
            languageChild = document.CreateElement("key");
            attribute = document.CreateAttribute("alias");
            attribute.Value = "dinewsletter";
            languageChild.Attributes.Append(attribute);
            languageChild.InnerText = "Nyhetsbrev";
            node.AppendChild(languageChild);
            document.Save(path);

            //writing to UI file
            path = HostingEnvironment.MapPath("/umbraco/config/create/UI.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("createUI");
            XmlNode uiChild = document.CreateElement("nodeType");
            attribute = document.CreateAttribute("alias");
            attribute.Value = "dinewsletterTemplate";
            uiChild.Attributes.Append(attribute);
            node.AppendChild(uiChild);
            node = document.SelectSingleNode("createUI/nodeType[@alias='dinewsletterTemplate']");
            uiChild = document.CreateElement("header");
            uiChild.InnerText = "Template";
            node.AppendChild(uiChild);
            uiChild = document.CreateElement("usercontrol");
            uiChild.InnerText = "/create/simple.ascx";
            node.AppendChild(uiChild);
            uiChild = document.CreateElement("tasks");
            node.AppendChild(uiChild);
            node = document.SelectSingleNode("createUI/nodeType[@alias='dinewsletterTemplate']/tasks");
            uiChild = document.CreateElement("create");
            attribute = document.CreateAttribute("assembly");
            attribute.Value = "Designit.Umb.Newsletter.Bll";
            uiChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("type");
            attribute.Value = "newsletterTemplateTasks";
            uiChild.Attributes.Append(attribute);
            node.AppendChild(uiChild);
            uiChild = document.CreateElement("delete");
            attribute = document.CreateAttribute("assembly");
            attribute.Value = "Designit.Umb.Newsletter.Bll";
            uiChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("type");
            attribute.Value = "newsletterTemplateTasks";
            uiChild.Attributes.Append(attribute);
            node.AppendChild(uiChild);

            node = document.SelectSingleNode("createUI");
            uiChild = document.CreateElement("nodeType");
            attribute = document.CreateAttribute("alias");
            attribute.Value = "dinewsletterCampaign";
            uiChild.Attributes.Append(attribute);
            node.AppendChild(uiChild);
            node = document.SelectSingleNode("createUI/nodeType[@alias='dinewsletterCampaign']");
            uiChild = document.CreateElement("header");
            uiChild.InnerText = "Campaign";
            node.AppendChild(uiChild);
            uiChild = document.CreateElement("usercontrol");
            uiChild.InnerText = "/create/simple.ascx";
            node.AppendChild(uiChild);
            uiChild = document.CreateElement("tasks");
            node.AppendChild(uiChild);
            node = document.SelectSingleNode("createUI/nodeType[@alias='dinewsletterCampaign']/tasks");
            uiChild = document.CreateElement("create");
            attribute = document.CreateAttribute("assembly");
            attribute.Value = "Designit.Umb.Newsletter.Bll";
            uiChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("type");
            attribute.Value = "newsletterCampaignTasks";
            uiChild.Attributes.Append(attribute);
            node.AppendChild(uiChild);
            uiChild = document.CreateElement("delete");
            attribute = document.CreateAttribute("assembly");
            attribute.Value = "Designit.Umb.Newsletter.Bll";
            uiChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("type");
            attribute.Value = "newsletterCampaignTasks";
            uiChild.Attributes.Append(attribute);
            node.AppendChild(uiChild);
            Console.WriteLine("test");

            document.Save(path);

            //writing to web
            path = HostingEnvironment.MapPath("/web.config");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("configuration/system.web/httpModules");
            XmlComment comment = document.CreateComment("Designit");
            node.AppendChild(comment);
            //XmlNode webChild = document.CreateElement("add");
            //attribute = document.CreateAttribute("name");
            //attribute.Value = "DelayedSend";
            //webChild.Attributes.Append(attribute);
            //attribute = document.CreateAttribute("type");
            //attribute.Value = "Designit.Umb.Newsletter.Bll.DelayedSend";
            //webChild.Attributes.Append(attribute);
            //node.AppendChild(webChild);
            var webChild = document.CreateElement("add");
            attribute = document.CreateAttribute("name");
            attribute.Value = "BounceHandling";
            webChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("type");
            attribute.Value = "Designit.Umb.Newsletter.Bll.BounceHandling";
            webChild.Attributes.Append(attribute);
            node.AppendChild(webChild);

            //vista/win7 support for asp.net            
            node = document.SelectSingleNode("configuration/system.webServer/modules");
            XmlComment comment2 = document.CreateComment("Designit");
            node.AppendChild(comment2);
            //webChild = document.CreateElement("add");
            //attribute = document.CreateAttribute("name");
            //attribute.Value = "DelayedSend";
            //webChild.Attributes.Append(attribute);
            //attribute = document.CreateAttribute("type");
            //attribute.Value = "Designit.Umb.Newsletter.Bll.DelayedSend";
            //webChild.Attributes.Append(attribute);
            //node.AppendChild(webChild);
            webChild = document.CreateElement("add");
            attribute = document.CreateAttribute("name");
            attribute.Value = "BounceHandling";
            webChild.Attributes.Append(attribute);
            attribute = document.CreateAttribute("type");
            attribute.Value = "Designit.Umb.Newsletter.Bll.BounceHandling";
            webChild.Attributes.Append(attribute);
            node.AppendChild(webChild);

            //writing to appSettings
            node = document.SelectSingleNode("configuration/appSettings/add[@key='umbracoReservedUrls']");
            string urlString = node.Attributes[1].Value;
            urlString += @",/designit/newsletter/tracking/tracking.aspx,/designit/newsletter/ScheduledSend.aspx";
            node.Attributes[1].Value = urlString;
            document.Save(path);

            //Writing to umbracosettings.config
            path = HostingEnvironment.MapPath("/config/umbracosettings.config");
            document = new XmlDocument();
            document.Load(path);
            //Setting scheduled tasks
            node = document.SelectSingleNode("settings/scheduledTasks");
            comment = document.CreateComment("Designit");
            node.AppendChild(comment);
            webChild = document.CreateElement("task");

            attribute = document.CreateAttribute("log");
            attribute.Value = "true";
            webChild.Attributes.Append(attribute);

            attribute = document.CreateAttribute("alias");
            attribute.Value = "ScheduledNewsletterSend";
            webChild.Attributes.Append(attribute);

            attribute = document.CreateAttribute("interval");
            attribute.Value = "900";
            webChild.Attributes.Append(attribute);

            attribute = document.CreateAttribute("url");
            attribute.Value = string.Format("http://{0}/designit/newsletter/ScheduledSend.aspx", HttpContext.Current.Request.ServerVariables["http_host"]);
            webChild.Attributes.Append(attribute);

            node.AppendChild(webChild);

            document.Save(path);

        }

        private void DropTables()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("drop table newsLetterCampaign;");
            sb.AppendLine("drop table newsLetterCampaign2Group;");
            sb.AppendLine("drop table newsLetterCampaignDelayedSend;");
            sb.AppendLine("drop table newsLetterCampaignNewsletter;");
            sb.AppendLine("drop table newsLetterCampaignTempContent;");
            sb.AppendLine("drop table newsLetterTemplate;");
            sb.AppendLine("drop table newsLetterSoftBounces;");
            sb.AppendLine("drop table newsletterSendMail;");
            sb.AppendLine("drop table newsletterTracking;");
            sb.AppendLine("drop table newsLetterUnsubscribeMember;");

            umbraco.BusinessLogic.Application.SqlHelper.ExecuteNonQuery(sb.ToString());
        }

        private void DeleteSqlEntries()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("delete from umbracoAppTree where appAlias='dinewsletter';");
            sb.AppendLine("delete from umbracoApp where appAlias='dinewsletter';");
            umbraco.BusinessLogic.Application.SqlHelper.ExecuteNonQuery(sb.ToString());
        }

        private void DeleteFromFiles()
        {
            //remove from restExtensions
            string path = HostingEnvironment.MapPath("/config/restExtensions.config");
            XmlDocument document = new XmlDocument();
            document.Load(path);
            XmlNode node = document.SelectSingleNode("RestExtensions");
            node.RemoveChild(document.SelectSingleNode("RestExtensions/ext[@alias='embedNewsletter']"));
            node.RemoveChild(document.SelectSingleNode("RestExtensions/ext[@alias='newsletterhelper']"));
            document.Save(path);

            //remove from language files
            path = HostingEnvironment.MapPath("/umbraco/config/lang/da.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("language/area[@alias='sections']");
            node.RemoveChild(document.SelectSingleNode("language/area[@alias='sections']/key[@alias='dinewsletter']"));
            document.Save(path);

            path = HostingEnvironment.MapPath("/umbraco/config/lang/de.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("language/area[@alias='sections']");
            node.RemoveChild(document.SelectSingleNode("language/area[@alias='sections']/key[@alias='dinewsletter']"));
            document.Save(path);

            path = HostingEnvironment.MapPath("/umbraco/config/lang/en.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("language/area[@alias='sections']");
            node.RemoveChild(document.SelectSingleNode("language/area[@alias='sections']/key[@alias='dinewsletter']"));
            document.Save(path);

            path = HostingEnvironment.MapPath("/umbraco/config/lang/es.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("language/area[@alias='sections']");
            node.RemoveChild(document.SelectSingleNode("language/area[@alias='sections']/key[@alias='dinewsletter']"));
            document.Save(path);

            path = HostingEnvironment.MapPath("/umbraco/config/lang/fr.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("language/area[@alias='sections']");
            node.RemoveChild(document.SelectSingleNode("language/area[@alias='sections']/key[@alias='dinewsletter']"));
            document.Save(path);

            path = HostingEnvironment.MapPath("/umbraco/config/lang/it.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("language/area[@alias='sections']");
            node.RemoveChild(document.SelectSingleNode("language/area[@alias='sections']/key[@alias='dinewsletter']"));
            document.Save(path);

            path = HostingEnvironment.MapPath("/umbraco/config/lang/nl.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("language/area[@alias='sections']");
            node.RemoveChild(document.SelectSingleNode("language/area[@alias='sections']/key[@alias='dinewsletter']"));
            document.Save(path);

            path = HostingEnvironment.MapPath("/umbraco/config/lang/no.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("language/area[@alias='sections']");
            node.RemoveChild(document.SelectSingleNode("language/area[@alias='sections']/key[@alias='dinewsletter']"));
            document.Save(path);

            path = HostingEnvironment.MapPath("/umbraco/config/lang/sv.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("language/area[@alias='sections']");
            node.RemoveChild(document.SelectSingleNode("language/area[@alias='sections']/key[@alias='dinewsletter']"));
            document.Save(path);

            //remove from ui file
            path = HostingEnvironment.MapPath("/umbraco/config/create/UI.xml");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("createUI");
            node.RemoveChild(document.SelectSingleNode("createUI/nodeType[@alias='dinewsletterTemplate']"));
            node.RemoveChild(document.SelectSingleNode("createUI/nodeType[@alias='dinewsletterCampaign']"));
            document.Save(path);

            //remove from umbracosettings.config
            path = HostingEnvironment.MapPath("/config/umbracosettings.config");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("settings/scheduledTasks");
            node.RemoveChild(document.SelectSingleNode("settings/scheduledTasks/task[@alias='ScheduledNewsletterSend']"));
            document.Save(path);

            //remove from web
            path = HostingEnvironment.MapPath("/web.config");
            document = new XmlDocument();
            document.Load(path);
            node = document.SelectSingleNode("configuration/system.web/httpModules");
            //node.RemoveChild(document.SelectSingleNode("configuration/system.web/httpModules/add[@type='Designit.Umb.Newsletter.Bll.DelayedSend']").PreviousSibling);
            //node.RemoveChild(document.SelectSingleNode("configuration/system.web/httpModules/add[@type='Designit.Umb.Newsletter.Bll.DelayedSend']"));
            node.RemoveChild(document.SelectSingleNode("configuration/system.web/httpModules/add[@type='Designit.Umb.Newsletter.Bll.BounceHandling']"));
            node = document.SelectSingleNode("configuration/system.webServer/modules");
            //node.RemoveChild(document.SelectSingleNode("configuration/system.webServer/modules/add[@type='Designit.Umb.Newsletter.Bll.DelayedSend']").PreviousSibling);
            //node.RemoveChild(document.SelectSingleNode("configuration/system.webServer/modules/add[@type='Designit.Umb.Newsletter.Bll.DelayedSend']"));
            node.RemoveChild(document.SelectSingleNode("configuration/system.webServer/modules/add[@type='Designit.Umb.Newsletter.Bll.BounceHandling']"));
            node = document.SelectSingleNode("configuration/appSettings/add[@key='umbracoReservedUrls']");
            string urlString = node.Attributes[1].Value;
            urlString = Regex.Replace(urlString, @",/designit/newsletter/tracking/tracking.aspx,/designit/newsletter/ScheduledSend.aspx", "", RegexOptions.IgnoreCase);
            node.Attributes[1].Value = urlString;
            document.Save(path);
        }
    }
        #endregion
}

