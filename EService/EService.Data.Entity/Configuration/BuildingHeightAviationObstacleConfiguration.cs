using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class BuildingHeightAviationObstacleConfiguration : EntityTypeConfiguration<BuildingHeightAviationObstacle>
    {
        public BuildingHeightAviationObstacleConfiguration()
        {
            ToTable("BuildingHeightAviationObstacle");
            HasKey(x=>x.BuildingHeightAviationObstacleId);

            Property(x => x.BuildingHeightAviationObstacleId).IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.UserServiceRequestMasterId).IsRequired();
            Property(x => x.AviationObstacleLookUpId).IsRequired();

            HasRequired(x=>x.BuildingHeightRequest).WithMany(x=>x.BuildingHeightAviationObstacles).HasForeignKey(x=>x.UserServiceRequestMasterId).WillCascadeOnDelete(false);
            HasRequired(x=>x.AviationObstacleLookUp).WithMany(x=>x.BuildingHeightAviationObstacles).HasForeignKey(x=>x.AviationObstacleLookUpId).WillCascadeOnDelete(false);
        }
    }
}