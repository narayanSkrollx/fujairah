using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Service.Model;
using EService.Web.Controllers.Base;
using Nelibur.ObjectMapper;

namespace EService.Web.Areas.Admin.Controllers
{
    public class ApplicableFeesController : SystemAdminBaseController
    {
        private ApplicableFeesLookupService applicableFeesLookupService;

        public ApplicableFeesController()
        {
            applicableFeesLookupService = new ApplicableFeesLookupService(serviceContext);
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Initialize()
        {
            var result = new ResponseObject();
            try
            {
                var applicableFees = applicableFeesLookupService.FindAllAsNoTracking(o => o.Active).ToList();
                var records = TinyMapper.Map<List<ApplicableFeesLookUp>, List<ApplicableFeesLookupServiceModel>>(applicableFees);
                var model = new
                {
                    Records = records,
                    AddNewModel = new ApplicableFeesLookupServiceModel()
                };

                result.Data = model;
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while initializing Applicable fees record.";
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Index(List<ApplicableFeesLookupServiceModel> model)
        {
            var result = new ResponseObject();
            try
            {
                var saveResult = applicableFeesLookupService.Save(model);
                result.IsSuccess = saveResult.IsSuccess;
                result.Message = saveResult.Message;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving applicable fees records.";
            }
            return Json(result);
        }
    }
}