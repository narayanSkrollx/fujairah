﻿using System;
using System.Collections.Generic;
using System.Linq;
using EService.Data.Entity.Entity;
using EService.Service.Logic.Enum;
using EService.Service.Model;
using Nelibur.ObjectMapper;
using EService.Core;

namespace EService.Service.Logic
{
    public class GroundHandlingRequestService : BaseEService<GroundHandlingRequest>
    {
        public GroundHandlingRequestService(ServiceContext context) : base(context)
        {
        }

        public override ServiceEnum Service => ServiceEnum.GroundHandlingRequest;

        public ResponseObject Update(GroundHandlingRequest model, int userid, bool isDraft, List<UserServiceRequestAttachment> addAttachments)
        {
            var request = Find(o => o.UserServiceRequestMasterId == model.UserServiceRequestMasterId && o.UserServiceRequestMaster.IsDraft && !o.UserServiceRequestMaster.IsDeleted);
            var list = request.AircraftTypes.ToList();
            Context.Database.GroundHandlingRequestAircraftTypes.RemoveRange(list);
            var userServiceRequestMaster = request.UserServiceRequestMaster;
            var aircraftCleaning = request.AircraftCleaning;

            var removeDocList = request.UserServiceRequestMaster.UserServiceRequestAttachments.ToList();
            Context.Database.UserServiceRequestAttachment.RemoveRange(removeDocList);
            addAttachments.ForEach(o => request.UserServiceRequestMaster.UserServiceRequestAttachments.Add(o));

            request.Address = model.Address;
            request.AircraftRegistration = model.AircraftRegistration;
            request.CargoLoadingType = model.CargoLoadingType;
            request.CargoOffLoadingType = model.CargoOffLoadingType;
            request.CLDGWeight = model.CLDGWeight;
            request.CLNoOfULD = model.CLNoOfULD;
            request.CLOtherSpecialCargo = model.CLOtherSpecialCargo;
            request.CLQuantityDG = model.CLQuantityDG;
            request.CLTypeOfDG = model.CLTypeOfDG;
            request.CLULDWeight = model.CLULDWeight;
            request.CoLNoOfULD = model.CoLNoOfULD;
            request.CoLOtherSpecialCargo = model.CoLOtherSpecialCargo;
            request.CoLQuantityOfDG = model.CoLQuantityOfDG;
            request.CoLTypeOfDG = model.CoLTypeOfDG;
            request.CoLULDWeight = model.CoLULDWeight;
            request.CompanyName = model.CompanyName;
            request.Email = model.Email;
            request.ETAUTC = model.ETAUTC;
            request.ETDUTC = model.ETDUTC;
            request.Fax = model.Fax;
            request.FirstName = model.FirstName;
            request.From = model.From;
            request.HasSpecialPermission = model.HasSpecialPermission;
            request.InboundDateTimeUTC = model.InboundDateTimeUTC;
            request.LastName = model.LastName;
            request.LoadingInstruction = model.LoadingInstruction;
            request.METOrATSDelivery = model.METOrATSDelivery;
            request.ModifiedByUserId = userid;
            request.ModifiedDate = DateTime.Now.DubaiTime();
            request.NoOfArrivingPassengers = model.NoOfArrivingPassengers;
            request.NoOfDepartingPassengers = model.NoOfDepartingPassengers;
            request.OutboundDateTimeUTC = model.OutboundDateTimeUTC;
            request.PermittedToAddGood = model.PermittedToAddGood;
            request.Phone = model.Phone;
            request.RequestWBCalculation = model.RequestWBCalculation;
            request.SITATEX = model.SITATEX;
            request.To = model.To;
            request.Weight = model.Weight;
            if (model.AircraftCleaning != null)
            {
                request.AircraftCleaning.AdditionalManpower = model.AircraftCleaning.AdditionalManpower;
                request.AircraftCleaning.AdditionalPlatformCRJ = model.AircraftCleaning.AdditionalPlatformCRJ;
                request.AircraftCleaning.AdditionalRequirements = model.AircraftCleaning.AdditionalRequirements;
                request.AircraftCleaning.ASUHours = model.AircraftCleaning.ASUHours;
                request.AircraftCleaning.FumigationRequired = model.AircraftCleaning.FumigationRequired;
                request.AircraftCleaning.HotelBooking = model.AircraftCleaning.HotelBooking;
                request.AircraftCleaning.PassengerStairs = model.AircraftCleaning.PassengerStairs;
                request.AircraftCleaning.PortableWater = model.AircraftCleaning.PortableWater;
                request.AircraftCleaning.PowerType = model.AircraftCleaning.PowerType;
                request.AircraftCleaning.Pushback = model.AircraftCleaning.Pushback;
                request.AircraftCleaning.SecuritySurveillanceForRAmpHours = model.AircraftCleaning.SecuritySurveillanceForRAmpHours;
                request.AircraftCleaning.StandbyFireFighting = model.AircraftCleaning.StandbyFireFighting;
                request.AircraftCleaning.ToiletService = model.AircraftCleaning.ToiletService;
                request.AircraftCleaning.WasingRequired = model.AircraftCleaning.WasingRequired;
            }

            request.UserServiceRequestMaster.IsDraft = isDraft;
            request.AircraftTypes = new List<GroundHandlingRequestAircraftType>();
            foreach (var type in model.AircraftTypes)
            {
                type.UserServiceRequestMasterId = model.UserServiceRequestMasterId;
                request.AircraftTypes.Add(type);
            }
            Context.Commit();
            return new ResponseObject { IsSuccess = true, Message = "Ground Handling Request information updated successfully." };
        }
    }
}