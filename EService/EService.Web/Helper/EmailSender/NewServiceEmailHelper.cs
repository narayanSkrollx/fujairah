using System.Collections.Generic;
using System.Linq;
using EService.Data.Entity.Entity;

namespace EService.Web.Helper.EmailSender
{
    public class NewServiceEmailHelper 
    {
        //public bool SendEmail(UserServiceRequestMaster serviceRequest, SMTPEmailSetting smtpEmailSetting, List<User> users)
        //{
        //    var toEmailList = users.Where(o => o.DepartmentId.HasValue && serviceRequest.ServiceRequestFlowStages.SelectMany(r => r.ServiceRequestFlowStageDepartments)
        //        .Select(d => d.DepartmentId).ToList().Contains(o.DepartmentId.Value)).Select(o => o.Email).ToArray();
        //    EmailHelper.SendEmail(smtpEmailSetting.From, toEmailList, new string[] { }, new string[] { }, "New Request", "New Request", smtpEmailSetting);
        //    return false;
        //}

        public bool SendEmail(string[] reciepients, SMTPEmailSetting smtpEmailSetting, string htmlContent, string subject)
        {
            EmailHelper.SendEmail(smtpEmailSetting.From, reciepients, new string[] { }, new string[] { }, subject, htmlContent, smtpEmailSetting);
            return true;
        }
    }
}