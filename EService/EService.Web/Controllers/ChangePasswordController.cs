using System;
using System.Web.Mvc;
using EService.Service.Logic;
using EService.Service.Model;
using EService.Web.Controllers.Base;
using EService.Web.Helper;
using EService.Web.Models;

namespace EService.Web.Controllers
{
    public class ChangePasswordController : BaseController
    {
        private ServiceContext context;
        private UserService userService;

        public ChangePasswordController()
        {
            context = new ServiceContext();
            userService = new UserService(context);
        }

        public ActionResult Index()
        {
            var model = new ChangePasswordViewModel();
            try
            {
                model.UserName = UserProfile.UserName;
                return View(model);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return ErrorResult();
        }

        [HttpPost]
        public JsonResult Index(ChangePasswordViewModel model)
        {
            var result = new JsonResponseObject();
            try
            {
                if (ModelState.IsValid)
                {
                    ResponseObject saveResult = userService.ChangePassword(UserProfile.UserId, model.CurrentPassword,
                        model.NewPassword);
                    result.IsSuccess = saveResult.IsSuccess;
                    result.Message = saveResult.Message;
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = ModelState.GetModelStateError();
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "Error while changing password.";
                LogError(ex);
            }
            return Json(result);
        }
    }
}