﻿using FIA.CMS.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using FIA.CMS.Helper;

namespace FIA.CMS.Controller
{
    public class LandingPermissionController : BaseController
    {
      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitForm(LandingPermissionModel model)
        {
            if (ModelState.IsValid)
            {
                var attachments = new List<Attachment>();
                var to = EmailSenderAddress.GetSenderEmail("LandingPermission");

                if (model.Attachments != null && model.Attachments.Any())
                {
                    foreach (var item in model.Attachments.Where(x => x.File != null))
                    {
                        var attachment = new Attachment(item.File.InputStream, item.File.FileName);
                        attachments.Add(attachment);
                    }
                }
                var genericEmailModel = new GenericEmailModel()
                {
                    Context = "Landing Permission Request"
                };
                base.SendEmail(AutoReplyToUser(genericEmailModel), model.EmailAddress, "NO REPLY-FIA");
                base.SendEmail(GetMessageBody(model), to, $"Landing permission for {model.Name} - {model.Operator} ", attachments);
                TempData["SuccessMsg"] = "Thank you for submitting us your query. We will get back to you shortly.";
                return RedirectToCurrentUmbracoPage();
            }

            return CurrentUmbracoPage();
        }

        private string AutoReplyToUser(GenericEmailModel model)
        {
            return RenderPartialView("~/Views/Partials/EmailTemplates/_GenericEmailResponse.cshtml", model);
        }

        private string GetMessageBody(LandingPermissionModel model)
        {
            var messagebody = "<table>";
            if (!string.IsNullOrWhiteSpace(model.Name))
            {
                messagebody += string.Format("<tr><td>Name</td><td>{0}</td></tr>", model.Name);
            }
            if (!string.IsNullOrWhiteSpace(model.Mobile))
            {
                messagebody += string.Format("<tr><td>Mobile</td><td>{0}</td></tr>", model.Mobile);
            }
            if (!string.IsNullOrWhiteSpace(model.EmailAddress))
            {
                messagebody += string.Format("<tr><td>Email Adress</td><td>{0}</td></tr>", model.EmailAddress);
            }
            if (!string.IsNullOrWhiteSpace(model.Operator))
            {
                messagebody += string.Format("<tr><td>Operator</td><td>{0}</td></tr>", model.Operator);
            }
            if (!string.IsNullOrWhiteSpace(model.TypeOfAC))
            {
                messagebody += string.Format("<tr><td>Type of A/C</td><td>{0}</td></tr>", model.TypeOfAC);
            }
            if (!string.IsNullOrWhiteSpace(model.RegistrationMTOW))
            {
                messagebody += string.Format("<tr><td>Registration/MTOW</td><td>{0}</td></tr>", model.RegistrationMTOW);
            }
            if (!string.IsNullOrWhiteSpace(model.IcoCallSign))
            {
                messagebody += string.Format("<tr><td>ICAO Call Sign</td><td>{0}</td></tr>", model.IcoCallSign);
            }
           
                messagebody += string.Format("<tr><td>Date of OPS</td><td>{0}</td></tr>", model.DateOfOps.ToString("d"));
            
            if (!string.IsNullOrWhiteSpace(model.Crew))
            {
                messagebody += string.Format("<tr><td>Crew</td><td>{0}</td></tr>", model.Crew);
            }
            if (!string.IsNullOrWhiteSpace(model.PurposeOfFlt))
            {
                messagebody += string.Format("<tr><td>Purpose Of FLT</td><td>{0}</td></tr>", model.PurposeOfFlt);
            }
            if (!string.IsNullOrWhiteSpace(model.Route))
            {
                messagebody += string.Format("<tr><td>Route</td><td>{0}</td></tr>", model.Route);
            }
            if (!string.IsNullOrWhiteSpace(model.Route))
            {
                messagebody += string.Format("<tr><td>Message</td><td>{0}</td></tr>", model.Message);
            }
            messagebody += string.Format("<tr><td>Schedule</td><td>{0}</td></tr>", model.Schedule.ToString("d"));
            messagebody += "</table>";
            return messagebody;
        }
    }
}