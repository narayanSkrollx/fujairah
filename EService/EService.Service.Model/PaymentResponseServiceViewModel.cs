﻿using System;

namespace EService.Service.Model
{
    public class PaymentResponseServiceViewModel
    {
        public int UserServiceRequestMasterId { get; set; }
        public int UserId { get; set; }
        public DateTime RequestDateTime { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public int Status { get; set; }
        public string RegistrationNoServiceCode { get; set; }
        public int RegistrationNoYear { get; set; }
        public int RegistrationNoMonth { get; set; }
        public int RegistrationNoIndex { get; set; }

        public int? ParentUserServiceRequestMasterId { get; set; }
        public string RegistrationNo { get; set; }

        #region ""
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserStreet1 { get; set; }
        public string UserStreet2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string UserEmail { get; set; }
        public string PhoneNumber { get; set; }

        #endregion  

        public double ChargedAmount { get; set; }

        public double AdminFee => ChargedAmount * 0.02;
        public double SocialDevelopmentFee => ChargedAmount > 50 ? 5 : 0;
        public decimal VATPercentage { get; set; }
        public double VAT => ChargedAmount * ((double)VATPercentage / 100);

        public double Total => ChargedAmount + AdminFee + SocialDevelopmentFee + VAT;
        #region "Post payment url"
        public string SuccessUrl { get; set; }
        public string FailureUrl { get; set; }
        #endregion
    }
}