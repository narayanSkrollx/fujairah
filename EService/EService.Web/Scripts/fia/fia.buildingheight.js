﻿$(function () {
    fia.CreateNamespace("buildingheight");

    (function (context) {
        context.Title = 'Building Height Request';
        context.ServiceId = fia.ServiceEnum.BuildingHeightRequest;
        context.CreateFormId = '#frmBuildingHeightRequest';

        context.InitializeCreateUrl = ApplicationRootUrl('InitializeCreate', 'BuildingHeightRequest');
        context.CreateUrl = ApplicationRootUrl('Create', 'BuildingHeightRequest');
        context.CreateSuccessUrl = ApplicationRootUrl('Index', 'Dashboard');
        context.InitializeIndexUrl = ApplicationRootUrl('Initialize', 'BuildingHeightRequest');
        context.DetailsUrl = ApplicationRootUrl('Detail', 'BuildingHeightRequest');
        context.InitializeDetailUrl = ApplicationRootUrl('InitializeDetail', 'BuildingHeightRequest');

        context.SaveRequest = function (isDraft) {
            if (!$('#frmBuildingHeightRequest').valid()) {
                return false;
            }
            var model = ko.mapping.toJS(context.ViewModel.AddNewModel);
            delete model.OHLStructureCoordinateAndHeightAddModel;
            var lightModel = JSON.stringify({ model: model, isDraft: isDraft });
            var submitControl = isDraft ? 'draft' : 'save';
            var ajaxOption = {
                dataType: "JSON",
                contentType: "application/json; charset=UTF-8",
                data: lightModel,
                enableLadda: true,
                targetLaddaElement: `[data-role=${submitControl}]`,
            };
            ajaxRequest(context.CreateUrl, 'POST', ajaxOption, function (resp) {
                if (resp.IsSuccess) {
                    showMessage(context.Title, resp.Message, 'success', function () {
                        //
                        if (resp.Data != null && resp.Data.ToPayment === true) {
                            window.location = '/Payment/Index';
                        }
                        else {
                            window.location = '/ServiceRequest/Index?serviceId=' + fia.ServiceEnum.BuildingHeightRequest;
                        }
                    });
                } else {
                    showMessage(context.Title, resp.Message, 'error');
                }
            });
        }

        context.ViewModel = {
            SaveRequest: function () {
                context.SaveRequest(false)
            },
            SaveDraftRequest: function () {
                context.SaveRequest(true);
            },
            SetUserInfo: function () {
                context.SetUserInfo();
            },
            ViewDetails: function (item) {
                window.location = context.DetailsUrl + '?id=' + item.UserServiceRequestMasterId();
            }
        };

        context.SetUserInfo = function () {
            var addNewModel = context.ViewModel.AddNewModel;
            var userInfo = context.ViewModel.AddNewModel.UserInfo;
            if (addNewModel != null && userInfo != null) {
                addNewModel.ApplicantName(userInfo.FirstName() + ' ' + userInfo.LastName());
                addNewModel.ApplicantAddress(userInfo.Address());
                addNewModel.ApplicantCell(userInfo.Phone());
                addNewModel.ApplicantEmail(userInfo.Email());
                addNewModel.ApplicantCompanyName(userInfo.CompanyName());
            }
        }


        context.InitializeCreate = function () {
            var masterId = parseInt($("#CurrentUserServiceRequestMasterId.forKnockout").val());
            context.ViewModel.IsCreate = Number.isInteger(masterId);
            var id = context.ViewModel.IsCreate ? parseInt(masterId) : null;

            ajaxRequest(context.InitializeCreateUrl, 'GET', { data: { id: id } }, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#createContent')[0])) {
                        context.ViewModel.AddNewModel = ko.mapping.fromJS(resp.Data);
                        context.ViewModel.RemoveUploadedFile = function (item) {
                            item.ObjectState(2);
                        }
                        var vm = context.ViewModel.AddNewModel;
                        vm.AddOHLCoordinateAndHeightClick = function () {
                            var addItem = ko.toJS(vm.OHLStructureCoordinateAndHeightAddModel);
                            //addItem.ObjectState = context.ObjectState.Added;
                            vm.OHLStructureCoordinateAndHeights.push(ko.mapping.fromJS(addItem));
                        }
                        vm.AfterRenderComplete = function () {
                            context.ApplyValidation();
                        }

                        ko.applyBindings(context.ViewModel, $('#createContent')[0]);


                        //init file uploader
                        fia.fileinputhelper.InitializeFileUpload('#kv-explorer', fia.ServiceEnum.BuildingHeightRequest);
                        fia.fileinputhelper.UploadCompleteCallback = function (response) {
                            context.ViewModel.AddNewModel.UploadedFiles.push(response.Data);
                        }
                        fia.fileinputhelper.DeleteCompleteCallback = function (removedData) {
                            context.ViewModel.AddNewModel.UploadedFiles.remove(function (data) { return data == removedData });
                        }
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel.AddNewModel);
                    }


                    //context.ApplyValidation();
                } else {
                    console.log(resp.Message);
                }
            });
        };

        context.ApplyValidation = function () {
            $(context.CreateFormId).validate({
                rules: {
                    ApplicantName: { required: true, maxlength: 225 },
                    ApplicantJobTitle: { required: true, maxlength: 225 },
                    ApplicantCompanyName: { required: true, maxlength: 225 },
                    ApplicantAddress: { required: true, maxlength: 225 },
                    ApplicantCell: { required: true, maxlength: 225, regex: phoneNoRegex },
                    ApplicantTel: { required: true, maxlength: 20, regex: phoneNoRegex },
                    ApplicantEmail: { required: true, maxlength: 225, email: true },
                    ApplicantWebsite: { maxlength: 225 },

                    OwnerName: { required: true, maxlength: 225 },
                    OwnerCompanyName: { required: true, maxlength: 225 },
                    OwnerAddress: { required: true, maxlength: 225 },
                    OwnerCell: { required: true, maxlength: 225, regex: phoneNoRegex },
                    OwnerTel: { required: true, maxlength: 225, regex: phoneNoRegex },
                    OwnerEmail: { required: true, maxlength: 225, email: true },
                    OwnerWebsite: { maxlength: 225 },

                    ApplicantOwnerRelationship: { required: true },

                    ProjectName: { required: true, maxlength: 225 },
                    ApplicationDate: { required: true, },
                    ExpectedConstructionStartDate: { required: true, },
                    ExpectedConstructionEndDate: { required: true, },
                    Location: { required: true },
                    ProjectDescription: { required: true },
                    //IsApplicantLegalRepresentative: { required: true },
                    //NoOfStructures: { required: true, max: 10000, min: 1 },
                    //NoOfObstacleLights: { required: true, max: 10000, min: 1 },
                    //TypesOfObstacleLights: { required: true, maxlength: 1024 },

                    ProjectTypes: { required: true }

                },
                messages: {
                    ApplicantName: { required: 'Applicant Name is required', maxlength: 'Applicant Name length cannot be greater than 255' },
                    ApplicantJobTitle: { required: 'Applicant Job Title is required', maxlength: 'Applicant Job Title length cannot be greater than 255' },
                    ApplicantCompanyName: { required: 'Applicant Company Name is required.', maxlength: 'Applican Company Name length cannot be greater than 255' },
                    ApplicantAddress: { required: 'Applicant Address is required.', maxlength: 'Applicant Address length cannot be greater than 255' },
                    ApplicantCell: { required: 'Applicant Cell No is required.', maxlength: 'Applicant Cell No length cannot be greater than 255' },
                    ApplicantTel: { required: 'Applicant Tel No is required.', maxlength: 'Applicant Tel No Length cannot be greater than 20.' },
                    ApplicantEmail: { required: 'Applicant Email is required', maxlength: 'Email length cannot be greater than 255', email: 'Applicant Email must be a valid Email' },
                    ApplicantWebsite: { maxlength: 'Applicant Website length cannot be greater than 255' },

                    OwnerName: { required: 'Owner Name is required', maxlength: 'Owner Name length cannot be greater than 255' },
                    OwnerCompanyName: { required: 'Owner Company Name is required.', maxlength: 'Applican Company Name length cannot be greater than 255' },
                    OwnerAddress: { required: 'Owner Address is required.', maxlength: 'Owner Address length cannot be greater than 255' },
                    OwnerCell: { required: 'Owner Cell No is required.', maxlength: 'Owner Cell No length cannot be greater than 255' },
                    OwnerTel: { required: 'Owner Tel No is required.', maxlength: 'Owner Tel No Length cannot be greater than 20.' },
                    OwnerEmail: { required: 'Owner Email is required', maxlength: 'Email length cannot be greater than 255', email: 'Owner Email must be a valid Email' },
                    OwnerWebsite: { maxlength: 'Owner Website length cannot be greater than 255' },

                    //ProjectName: { required: 'Project Name is required', maxlength: 'Project Name max length cannot be greater than ' },
                    //IsApplicantLegalRepresentative: { required: 'Is Applicant Legal Representative must be selected.' },
                    //NoOfStructures: { required: 'No of structure is required', max: 'No of Structures cannot be greater than 10000', min: 'No of structure cannot be less than 1' },
                    //NoOfObstacleLights: { required: 'No of Obstacles light is required.', max: 'No of obstacles light cannot be greater than 10000', min: 'No of obstacles light cannot be less than 1' },
                    //TypesOfObstacleLights: { required: 'Types of Obstacles light is required.', maxlength: 'Types of obstacle light length cannot be greater than 1024' },
                    ProjectTypes: { required: "Project Type must be selected" }
                }
            });
        }

        context.InitializeIndex = function () {
            ajaxRequest(context.InitializeIndexUrl, 'GET', {}, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#mainContent')[0])) {
                        context.ViewModel.Records = ko.mapping.fromJS(resp.Data);
                        ko.applyBindings(context.ViewModel, $('#mainContent')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel.Records);
                    }
                } else {
                    showMessage('Service Request Detail', resp.Message, 'error');
                }
            });
        };

        context.InitializeDetail = function () {
            context.CurrentUserServiceRequestMasterId = $("#CurrentUserServiceRequestMasterId.forKnockout").val();

            ajaxRequest(context.InitializeDetailUrl + "?id=" + context.CurrentUserServiceRequestMasterId, 'GET', {}, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#buildingheightsDetailsdiv')[0])) {
                        context.ViewModel.Details = ko.mapping.fromJS(resp.Data);
                        ko.applyBindings(context.ViewModel, $('#buildingheightsDetailsdiv')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel.Details);
                    }
                } else {
                    showMessage('Service Request Detail', resp.Message, 'error');
                }
            });
        };

    })(fia.buildingheight);
});