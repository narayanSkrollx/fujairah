using System.Collections.Generic;

namespace EService.Data.Entity.Entity
{
    public class AirportLanding : ILookupItem
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }

        public virtual ICollection<LandingPermissionRequest> LandingPermissionRequests { get; set; }
    }
}