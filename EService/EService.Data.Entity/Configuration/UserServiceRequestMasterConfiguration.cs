using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class UserServiceRequestMasterConfiguration : EntityTypeConfiguration<UserServiceRequestMaster>
    {
        public UserServiceRequestMasterConfiguration()
        {
            ToTable("UserServiceRequestMaster");

            HasKey(o => o.UserServiceRequestMasterId);

            Property(o => o.UserServiceRequestMasterId);
            Property(o => o.UserId).IsRequired();
            Property(o => o.RequestDateTime).IsRequired();
            Property(o => o.ServiceId).IsRequired();
            Property(o => o.Status).IsRequired();
            Property(o => o.RegistrationNoYear).IsRequired();
            Property(o => o.RegistrationNoMonth).IsRequired();
            Property(o => o.RegistrationNoIndex).IsRequired();
            Property(o => o.RegistrationNoServiceCode).IsRequired().HasMaxLength(50);
            Property(o => o.RegistrationNo).IsRequired().HasMaxLength(1024);
            Property(o => o.IsDraft).IsRequired();

            HasRequired(o => o.User).WithMany(o => o.UserServiceRequests).HasForeignKey(o => o.UserId).WillCascadeOnDelete(false);
            HasRequired(o => o.Service).WithMany(o => o.UserServiceRequestMasters).HasForeignKey(o => o.ServiceId).WillCascadeOnDelete(false);
            HasOptional(o=>o.ParentUserServiceRequestMaster).WithMany().HasForeignKey(o=>o.ParentUserServiceRequestMasterId).WillCascadeOnDelete(false);

        }
    }
}