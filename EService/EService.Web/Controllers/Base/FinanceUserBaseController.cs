using System.Web.Mvc;
using EService.Data.Entity;

namespace EService.Web.Controllers.Base
{
    public class FinanceUserBaseController : BaseController
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            if (UserProfile.UserType != UserTypeEnum.FinanceUser)
            {
                filterContext.Result = UnAuthroizedResult();
            }
        }
    }
}