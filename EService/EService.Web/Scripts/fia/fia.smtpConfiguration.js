﻿$(function () {
    fia.CreateNamespace("smtpConfiguration");

    (function(context) {

        context.Title = 'SMTP Configuration';
        context.ViewModel = {
            
        };
        context.ViewModel.Save = function() {
            if (!$('#smtpForm').valid()) {
                return false;
            }

            var model = ko.toJS(context.ViewModel.SMTPConfigurationVM);
            delete model.CreatedByUserId;
            delete model.CreatedDate;
            delete model.ModifiedDate;
            delete model.ModifiedByUserId;

            ajaxRequest('/Configure/SMTP/Index', 'POST', { data: { model: model } }, function(response) {
                if (response.IsSuccess) {
                    showMessage(context.Title, 'Save Success', 'success', function() {
                        window.location = window.location;
                    });
                } else {
                    showMessage(context.Title, response.Message, 'error');
                }
            });
        }

        context.Initialize = function() {
            ajaxRequest('/Configure/SMTP/Initialize', 'GET', { data: {} }, function(response) {
                if (response.IsSuccess) {
                    if (!ko.dataFor($('#mainContent')[0])) {
                        context.ViewModel.SMTPConfigurationVM = ko.mapping.fromJS(response.Data, {});

                        ko.applyBindings(context.ViewModel, $('#mainContent')[0]);
                    } else {
                        ko.mapping.fromJS(response.Data, {}, context.ViewModel.SMTPConfigurationVM);
                    }
                    context.ApplyValidation();
                } else {
                    showMessage(context.Title, response.Message, 'error');
                }
            });
        }

        context.ApplyValidation = function() {
            $('#smtpForm').validate({
                rules: {
                    Host: { required: true },
                    port: { required: true },
                    UserName: { required: true },
                    Password: { required: true },
                    From : {required: true}
                },
                messages: {
                    Host: { required: 'Host is required' },
                    port: { required: 'Port is required.' },
                    UserName: { required: 'User Name is required.' },
                    Password: { required: 'Password is required' },
                    From: { required: 'From Email is required.' }
                }
            });
        }

    })(fia.smtpConfiguration);
});