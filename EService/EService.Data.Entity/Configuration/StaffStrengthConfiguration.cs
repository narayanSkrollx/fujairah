using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class StaffStrengthConfiguration : EntityTypeConfiguration<StaffStrength>
    {
        public StaffStrengthConfiguration()
        {
            ToTable("StaffStrength");
            HasKey(x => x.StaffStrengthId);

            Property(x => x.StaffStrengthId).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.StaffStrengthName).HasMaxLength(225).IsRequired();
        }
    }
}