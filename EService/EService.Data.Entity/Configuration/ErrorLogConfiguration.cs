using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class ErrorLogConfiguration : EntityTypeConfiguration<ErrorLog>
    {
        public ErrorLogConfiguration()
        {
            ToTable("ErrorLog");

            HasKey(o => o.ErrorLogId);

            Property(o => o.ErrorLogId).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.Timestamp).IsRequired();
            Property(o => o.File).IsOptional().HasColumnType("nvarchar(max)");
            Property(o => o.Method).IsOptional().HasColumnType("nvarchar(max)");
            Property(o => o.Exception).IsOptional().HasColumnType("nvarchar(max)");
            Property(o => o.InnerException).IsOptional().HasColumnType("nvarchar(max)");
            Property(o => o.StackTrace).IsOptional().HasColumnType("nvarchar(max)");
            Property(o => o.Source).IsOptional().HasColumnType("nvarchar(max)");
            Property(o => o.Data).IsOptional().HasColumnType("nvarchar(max)");
            Property(o => o.ClientBrowserAgent).IsOptional().HasColumnType("nvarchar(max)");
            Property(o => o.ClientIp).IsOptional().HasColumnType("nvarchar(max)");
            Property(o => o.UserId).IsOptional();
            Property(o => o.UserName).IsOptional().HasColumnType("nvarchar(max)");
        }
    }
}