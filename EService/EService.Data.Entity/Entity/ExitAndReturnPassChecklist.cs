using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class ExitSparePartsPassChecklist
    {
        public int ExitSparePartsPassChecklistId { get; set; }
        public int UserServiceRequestMasterId { get; set; }

        public string Description { get; set; }
        public string PartNumber { get; set; }
        public int? NumberOfPieces { get; set; }
        public string Origin { get; set; }
        public string ExitToWhere { get; set; }

        [JsonIgnore]
        public ExitOfAircraftSparePartsRequest ExitOfAircraftSparePartsRequest { get; set; }
    }
}