using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class ServiceApprovalStageConfiguration : EntityTypeConfiguration<ServiceApprovalStage>
    {
        public ServiceApprovalStageConfiguration()
        {
            ToTable("ServiceApprovalStage");

            HasKey(o => o.ServiceApprovalStageId);

            Property(o => o.ServiceApprovalStageId)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.ServiceId).IsRequired().HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_ServiceApprovalStageUnique", 1) { IsUnique = true }));
            Property(o => o.Index).IsRequired().HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_ServiceApprovalStageUnique", 2) { IsUnique = true }));

            HasRequired(o => o.Service)
                .WithMany(o => o.ServiceApprovalStages)
                .HasForeignKey(o => o.ServiceId);
        }
    }
}