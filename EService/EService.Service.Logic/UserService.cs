﻿using System;
using System.Collections.Generic;
using System.Linq;
using EService.Core.Security;
using EService.Data.Entity;
using EService.Data.Entity.Entity;
using EService.Service.Model;
using EService.Core;
namespace EService.Service.Logic
{
    public class UserService : BusinessAccessBase<User>
    {
        public UserService(ServiceContext context) : base(context)
        {
        }

        public bool RegisterUser(UserRegisterModel model, out User user, out string message)
        {
            user = null;
            var existingUser = Find(o => o.UserName == model.UserName);
            if (existingUser != null)
            {
                message = "Provided User name already exists. Please use another one.";
                return false;
            }
            var existingEmail = Find(o => o.Email == model.Email);
            if (existingEmail != null)
            {
                message = "Provided Email already exists. Please use another one.";
                return false;
            }
            user = new User()
            {
                UserName = model.UserName,
                Password = model.PasswordHash,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                UserTypeId = (int)UserTypeEnum.ExternalUser,
                UserCompanyProfile = new UserCompanyProfile()
                {
                    City = model.City,
                    CompanyAddress = model.CompanyAddress,
                    CompanyCategoryId = model.CompanyCategoryId,
                    CompanyName = model.CompanyName,
                    Fax = model.Fax,
                    POBox = model.POBox,
                    Phone = model.Phone,
                },
                UserSecruityQuestion = new UserSecruityQuestion()
                {
                    SecurityQuestionId = model.SecurityQuestionId,
                    Answer = model.Answer
                },
                UserContactPerson = new UserContactPerson()
                {
                    ContactPersonMobile = model.ContactPersonMobile ?? 0,
                    ContactPersonName = model.ContactPersonName,
                    EmiratesId = model.EmiratesId,
                }
            };

            this.Add(user);
            Context.Commit();
            message = "User registered successully.";
            return true;
        }

        public IEnumerable<UserModel> GetUsers()
        {
            return (
                from user in Context.Database.Users
                join userType in Context.Database.UserTypes
                on user.UserTypeId equals userType.UserTypeId
                join department in Context.Database.Departments
                on user.DepartmentId equals department.DepartmentId
                into departmentGroup
                from defaultDepartment in departmentGroup.DefaultIfEmpty()
                select new
                {
                    user,
                    userType.UserTypeName,
                    Departmentname = defaultDepartment != null ? defaultDepartment.DepartmentName : ""
                })
                .AsEnumerable()
                .Select(o => new UserModel()
                {
                    DepartmentId = o.user.DepartmentId,
                    UserTypeId = o.user.UserTypeId,
                    UserId = o.user.UserId,
                    UserName = o.user.UserName,
                    DepartmentName = o.Departmentname,
                    Email = o.user.Email,
                    Password = o.user.Password,
                    UserTypeName = o.UserTypeName,
                });
        }

        public ResponseObject<User> Authenticate(string userName, string password)
        {
            var authenticatedUser = Context.Database.Users.FirstOrDefault(o => o.UserName == userName && o.Password == password && !o.IsDeleted);

            return new ResponseObject<User>()
            {
                IsSuccess = authenticatedUser != null,
                Data = authenticatedUser,
            };

        }

        public ResponseObject SaveDepartmentUser(User model)
        {
            var userNameExists = Find(o => o.UserName == model.UserName);
            if (userNameExists != null)
            {
                return new ResponseObject() { IsSuccess = false, Message = "Provided User Name is already registered. Please use a different user name." };
            }
            var emailExists = Find(o => o.Email == model.Email);
            if (emailExists != null)
            {
                return new ResponseObject() { IsSuccess = false, Message = "Provided Email is already registered. Please use a different email." };
            }
            this.Add(model);
            Context.Commit();
            return new ResponseObject() { IsSuccess = true };
        }

        public ResponseObject ChangePassword(int userId, string currentPassword, string newPassword)
        {
            var result = new ResponseObject();
            var user = Find(o => !o.IsDeleted && o.UserId == userId);
            if (user != null)
            {
                if (user.Password == currentPassword.ToMd5())
                {
                    user.Password = newPassword.ToMd5();
                    Context.Database.SaveChanges();
                    result.IsSuccess = true;
                    result.Message = "User's password changed successfully.";
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "User's Password is invalid.";
                }
            }
            else
            {
                result.IsSuccess = false;
                result.Message = "Invalid User.";
            }
            return result;
        }

        public ResponseObject ResetPassword(string newPassword, Guid token)
        {
            var result = new ResponseObject();
            var forgotPasswordRequest =
                   Context.Database.ForgotPasswordRequests.FirstOrDefault(
                       o => !o.IsUsed && !o.IsDeleted && o.Token == token && o.ExpiryTime >= DateTime.Now.DubaiTime());
            var isTokenValid = forgotPasswordRequest != null;
            var user = Find(o => !o.IsDeleted && o.UserId == forgotPasswordRequest.UserId);
            if (user != null && isTokenValid)
            {
                user.Password = newPassword.ToMd5();
                forgotPasswordRequest.IsUsed = true;
                Context.Database.SaveChanges();
                result.IsSuccess = true;
                result.Message = "Password reset performed successfully.";
            }
            else
            {
                result.IsSuccess = false;
                result.Message = "Invalid User.";
            }
            return result;
        }
        public ResponseObject ResetPassword(int userId, string newPassword)
        {
            var result = new ResponseObject();

            var user = Find(o => !o.IsDeleted && o.UserId == userId);
            if (user != null )
            {
                user.Password = newPassword.ToMd5();
                Context.Database.SaveChanges();
                result.IsSuccess = true;
                result.Message = "Password reset performed successfully.";
            }
            else
            {
                result.IsSuccess = false;
                result.Message = "Invalid User.";
            }
            return result;
        }
        public ResponseObject<UserBasicInfoServiceModal> GetUserBasicInfo(int userId)
        {
            var result = new ResponseObject<UserBasicInfoServiceModal>();

            var user = Find(o => !o.IsDeleted && o.UserId == userId);
            if (user != null)
            {
                var model = new UserBasicInfoServiceModal()
                {
                    UserId = user.UserId,
                    UserName = user.UserName,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email,
                    CompanyCategory = user.UserCompanyProfile.CompanyCategory != null ? user.UserCompanyProfile.CompanyCategory.Description : "",
                    Fax = user.UserCompanyProfile.Fax,
                    Phone = user.UserCompanyProfile.Phone,
                    CompanyName = user.UserCompanyProfile.CompanyName,
                    POBox = user.UserCompanyProfile.POBox,
                    City = user.UserCompanyProfile.City,
                    Address = user.UserCompanyProfile.CompanyAddress
                };
                result.Data = model;
                result.IsSuccess = true;
            }
            else
            {
                result.IsSuccess = false;
                result.Data = null;
                result.Message = "Invalid User information provided.";
            }

            return result;
        }

    }
}