﻿using System;

namespace EService.Data.Entity.Entity
{
    public class ForgotPasswordRequest : AuditableEntity
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Email { get; set; }
        public Guid Token { get; set; }
        public DateTime ExpiryTime { get; set; }
        public bool IsUsed { get; set; }

        public virtual User User { get; set; }
    }
}