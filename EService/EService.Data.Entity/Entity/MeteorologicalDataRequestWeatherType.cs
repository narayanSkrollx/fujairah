namespace EService.Data.Entity.Entity
{
    public class MeteorologicalDataRequestWeatherType
    {
        public int MeteorologicalDataRequestWeatherTypeid { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public int WeatherTypeReportId { get; set; }
        public bool IsActive { get; set; }
        public virtual WeatherTypeReport WeatherTypeReport { get; set; }
        public virtual MeteorologicalDataRequest MeteorologicalDataRequest { get; set; }
    }
}