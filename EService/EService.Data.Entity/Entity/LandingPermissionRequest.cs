using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class LandingPermissionRequest : AuditableEntity, IServiceEntity
    {
        public int UserServiceRequestMasterId { get; set; }

        #region "Company Detail"
        public string AirlineName { get; set; } //required, defaults to username
        public string Email { get; set; }//Required, defaults to user email address
        public string Telephone { get; set; }//required
        public string Fac { get; set; }
        public string Address { get; set; }//multiline

        #endregion

        #region Operator Detail
        public string OperatorName { get; set; } //Required
        public string OperatorEmail { get; set; }//required
        public string OperatorTelephone { get; set; }//required
        public string Fax { get; set; }
        public string OperatorAddress { get; set; }
        #endregion

        #region "Flight Detail"
        public int FlightNatureOfOperationId { get; set; }//required
        public string AircraftRegistration { get; set; }//required
        public string TypeOfAircraft { get; set; }//required
        public int AirportLandingId { get; set; }//required
        /// <summary>
        /// Maximum Take off Weight (in Kg)
        /// required
        /// </summary>
        public decimal MTOW { get; set; }
        #endregion

        public string Remarks { get; set; }
        
        [JsonIgnore]
        public UserServiceRequestMaster UserServiceRequestMaster { get; set; }

        [JsonIgnore]
        public virtual FlightNatureOfOperation FlightNatureOfOperation { get; set; }
        [JsonIgnore]
        public virtual AirportLanding AirportLanding { get; set; }
        [JsonIgnore]
        public virtual ICollection<LandingPermissionRequestAttachment> LandingPermissionRequestAttachments { get; set; }
        [JsonIgnore]
        public virtual ICollection<LandingPermissionRequestTrip> LandingPermissionRequestTrips { get; set; }
    }
}