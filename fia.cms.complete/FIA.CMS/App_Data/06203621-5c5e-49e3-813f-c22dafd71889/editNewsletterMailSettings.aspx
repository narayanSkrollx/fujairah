<%@ Page Language="C#"  MasterPageFile="/umbraco/masterpages/umbracoPage.Master" AutoEventWireup="true" CodeBehind="editNewsletterMailSettings.aspx.cs" Inherits="Designit.Umb.Newsletter.Custom.plugins.designit.newsletter.editNewsletterMailSettings" %>
<%@ Register TagPrefix="uc" TagName="editNewsletter" Src="~/usercontrols/designit/newsletter/NewsletterSettings.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <uc:editNewsletter id="ucEditNewsletter" runat="server" HasMenu="true" />
</asp:Content>