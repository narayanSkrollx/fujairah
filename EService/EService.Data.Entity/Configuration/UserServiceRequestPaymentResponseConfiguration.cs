﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class UserServiceRequestPaymentResponseConfiguration : EntityTypeConfiguration<UserServiceRequestPaymentResponse>
    {
        public UserServiceRequestPaymentResponseConfiguration()
        {
            ToTable("UserServiceRequestPaymentResponse");

            HasKey(o => o.Id);

            Property(o => o.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.OrderNumber).IsRequired().HasMaxLength(1024);
            Property(o => o.Amount).IsRequired();
            Property(o => o.Currency).IsRequired().HasMaxLength(1024);
            Property(o => o.StatusFlag).IsRequired().HasMaxLength(1024);
            Property(o => o.ErrorCode).IsRequired().HasMaxLength(1024);
            Property(o => o.AllResponseJson).IsRequired().HasColumnType("varchar(max)");
            Property(o => o.IsReceivedOnSuccess).IsRequired();

            Property(o => o.UserServiceRequestMasterId).IsOptional();

            HasOptional(o => o.ServiceRequestBill).WithMany(o => o.PaymentResponses).HasForeignKey(o => o.UserServiceRequestMasterId).WillCascadeOnDelete(false);
        }
    }
}