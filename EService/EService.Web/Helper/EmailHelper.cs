using System;
using System.Net.Mail;
using System.Threading;
using EService.Data.Entity.Entity;

namespace EService.Web.Helper
{
    public class EmailHelper
    {
        public static void SendEmail(string from, string[] to, string[] cc, string[] bcc, string subject, string body, SMTPEmailSetting _smtp = null)
        {
            var email = new Thread(delegate()
            {
                SendAsyncEmail(from, to, cc, bcc, subject, body, _smtp);
            })
            {
                IsBackground = true
            };
            email.Start();
        }

        private static void SendAsyncEmail(string from, string[] to, string[] cc, string[] bcc, string subject, string body, SMTPEmailSetting _smtp = null)
        {
            try
            {
                MailMessage message = new MailMessage();
                SmtpClient client = new SmtpClient();
                if (_smtp != null)
                {
                    client.Host = _smtp.Host;
                    client.Port = _smtp.Port;
                    client.EnableSsl = _smtp.EnableSsl;
                    client.Credentials = new System.Net.NetworkCredential(_smtp.UserName, _smtp.Password);
                }

                message.From = new MailAddress(from);
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = true;
                foreach (string t in to)
                {
                    message.To.Add(new MailAddress(t));
                }

                if (cc != null)
                {
                    foreach (string c in cc)
                    {
                        message.CC.Add(new MailAddress(c));
                    }
                }
                if (bcc != null)
                {
                    foreach (string b in bcc)
                    {
                        message.Bcc.Add(new MailAddress(b));
                    }
                }
                client.Send(message);
            }
            catch (Exception ex)
            {

            }
        }
    }
}