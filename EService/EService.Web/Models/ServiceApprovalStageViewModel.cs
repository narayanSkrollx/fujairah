using System.Collections.Generic;
using EService.Data.Entity;
using EService.Service.Model;

namespace EService.Web.Models
{
    public class ServiceApprovalStageViewModel : ServiceApproveModal
    {
        
        public List<LookupItem> Departments { get; set; }

        public ServiceApprovalStageModal NewApprovalStageModal => new ServiceApprovalStageModal()
        {
            ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartmentModal>() { new ServiceApprovalStageDepartmentModal() }
        };

        public ServiceApprovalStageDepartmentModal NewApprovalStageDepartmentModal => new ServiceApprovalStageDepartmentModal()
        {

        };
    }
}