﻿using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class UserCompanyProfileConfiguration : EntityTypeConfiguration<UserCompanyProfile>
    {
        public UserCompanyProfileConfiguration()
        {
            ToTable("UserCompanyProfile");

            HasKey(o => o.UserId);

            Property(o => o.UserId).IsRequired();
            Property(o => o.CompanyCategoryId).IsRequired();
            Property(o => o.CompanyName).IsOptional().HasMaxLength(255);
            Property(o => o.CompanyAddress).IsOptional().HasMaxLength(255);
            Property(o => o.Phone).IsOptional().HasMaxLength(100);
            Property(o => o.Fax).IsOptional().HasMaxLength(100);
            Property(o => o.POBox).IsOptional().HasMaxLength(100);
            Property(o => o.City).IsOptional().HasMaxLength(255);
            Property(o => o.Country).IsOptional().HasMaxLength(255);
            Property(o => o.VATRegistrationNumber).IsOptional().HasMaxLength(255);

            HasRequired(o => o.CompanyCategory)
                .WithMany(o => o.UserCompanyProfiles)
                .HasForeignKey(o => o.CompanyCategoryId);

        }


    }
}