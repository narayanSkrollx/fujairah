namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FileAttachmentforCP : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CranePermitPlanningAttachment",
                c => new
                    {
                        CranePermitPlanningAttachmentId = c.Int(nullable: false, identity: true),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        UserAttachmentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CranePermitPlanningAttachmentId)
                .ForeignKey("dbo.UserAttachment", t => t.UserAttachmentId)
                .ForeignKey("dbo.CranePermitRequestFirstStageEntry", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId)
                .Index(t => t.UserAttachmentId);
            
            CreateTable(
                "dbo.CranePermitSafetyFileAttachment",
                c => new
                    {
                        CranePermitSafetyFileAttachmentId = c.Int(nullable: false, identity: true),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        UserAttachmentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CranePermitSafetyFileAttachmentId)
                .ForeignKey("dbo.UserAttachment", t => t.UserAttachmentId)
                .ForeignKey("dbo.CranePermitSafetyQAApproval", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId)
                .Index(t => t.UserAttachmentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CranePermitSafetyFileAttachment", "UserServiceRequestMasterId", "dbo.CranePermitSafetyQAApproval");
            DropForeignKey("dbo.CranePermitSafetyFileAttachment", "UserAttachmentId", "dbo.UserAttachment");
            DropForeignKey("dbo.CranePermitPlanningAttachment", "UserServiceRequestMasterId", "dbo.CranePermitRequestFirstStageEntry");
            DropForeignKey("dbo.CranePermitPlanningAttachment", "UserAttachmentId", "dbo.UserAttachment");
            DropIndex("dbo.CranePermitSafetyFileAttachment", new[] { "UserAttachmentId" });
            DropIndex("dbo.CranePermitSafetyFileAttachment", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.CranePermitPlanningAttachment", new[] { "UserAttachmentId" });
            DropIndex("dbo.CranePermitPlanningAttachment", new[] { "UserServiceRequestMasterId" });
            DropTable("dbo.CranePermitSafetyFileAttachment");
            DropTable("dbo.CranePermitPlanningAttachment");
        }
    }
}
