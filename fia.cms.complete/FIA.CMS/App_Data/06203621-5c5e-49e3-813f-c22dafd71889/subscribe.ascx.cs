﻿using System;
using System.Collections;
using System.Web.Security;
using System.Web.UI.WebControls;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.member;


namespace Designit.Umb.Newsletter.Custom.usercontrols.designit.newsletter
{
    public partial class subscribe : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadGroups();
        }

        private void loadGroups()
        {
            MemberGroup[] memberGroups = MemberGroup.GetAll;
            foreach (MemberGroup group in memberGroups)
            {
                if (group.Text != "Bounced")
                {
                    CheckBox checkBox = new CheckBox();
                    checkBox.Text = group.Text;
                    Panel2.Controls.Add(checkBox);
                }

            }
        }

        private bool checkBoxes()
        {

            foreach (var control in Panel2.Controls)
            {
                if (control is CheckBox)
                {
                    if (((CheckBox)control).Checked)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //check if a newsletter group has been chosen and the textbox with e-mail ain't empty
            if (checkBoxes() && txtEmail.Text.Length > 0)
            {
                //check if member exist in database
                if (Member.GetMemberFromEmail(txtEmail.Text) == null)
                {
                    MemberType memberType = MemberType.GetByAlias("Newsletter");
                    Member newMember = Member.MakeNew(txtEmail.Text, txtEmail.Text, memberType, new User(0));
                    string password = Membership.GeneratePassword(7, 1);

                    newMember.Email = txtEmail.Text;
                    newMember.LoginName = txtEmail.Text;
                    newMember.Password = password;

                    newMember.getProperty("name").Value = txtName.Text; //set value of property with alias name
                    newMember.getProperty("address").Value = txtAddress.Text; //set value of property with alias address
                    newMember.getProperty("postalCode").Value = txtPostal.Text; //set value of property with alias postalCode
                    newMember.getProperty("city").Value = txtCity.Text; //set value of property with alias city

                    newMember.Save();

                    Subscribe(newMember);

                    Panel1.Visible = false;

                    Literal2.Text = txtEmail.Text + " has succesfully subscribed";
                }
                //if member exists
                else
                {
                    Member member = Member.GetMemberFromEmail(txtEmail.Text);
                    Subscribe(member);
                    Panel1.Visible = false;
                    Literal2.Text = txtEmail.Text + " has succesfully subscribed";
                }
            }
            else
            {
                Literal2.Text = "Please choose atleast one newsletter to subscribe to";
            }
        }
        private void Subscribe(Member member)
        {
            foreach (var control in Panel2.Controls)
            {
                if (control is CheckBox)
                {
                    if (((CheckBox)control).Checked)
                    {

                        Designit.Umb.Newsletter.Bll.Subscribe.AssignMemberToGroup(member, ((CheckBox)control).Text);
                    }
                }
            }
        }

    }
}