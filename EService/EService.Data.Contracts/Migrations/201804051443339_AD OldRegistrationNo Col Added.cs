namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ADOldRegistrationNoColAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AircraftDismantlingNOCRequest", "OldRegistrationNo", c => c.String(maxLength: 225));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AircraftDismantlingNOCRequest", "OldRegistrationNo");
        }
    }
}
