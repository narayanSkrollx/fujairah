using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class UserSecruityQuestion
    {
        public int UserId { get; set; }
        public int SecurityQuestionId { get; set; }
        public string  Answer { get; set; }

        [JsonIgnore]
        public virtual User User { get; set; }
        [JsonIgnore]
        public virtual SecurityQuestion SecurityQuestion { get; set; }
    }
}