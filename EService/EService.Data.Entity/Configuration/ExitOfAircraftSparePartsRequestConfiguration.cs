﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class ExitOfAircraftSparePartsRequestConfiguration : EntityTypeConfiguration<ExitOfAircraftSparePartsRequest>
    {
        public ExitOfAircraftSparePartsRequestConfiguration()
        {
            ToTable("ExitOfAircraftSparePartsRequest");
            HasKey(x => x.UserServiceRequestMasterId);

            Property(x => x.UserServiceRequestMasterId).IsRequired();
            Property(x => x.DateOfApplication).IsRequired();
            Property(x => x.ApplicantCompany).IsOptional().HasMaxLength(225);
            Property(x => x.AircraftType).IsOptional().HasMaxLength(225);
            Property(x => x.AircraftRegistrationNumber).IsOptional().HasMaxLength(225);
            Property(x => x.OwnerOfAircraft).IsOptional().HasMaxLength(225);
            Property(x => x.NoObjectionLetterOfTheOwner).IsOptional().HasMaxLength(225);
            Property(x => x.AircraftMaintenanceOrganisationOrCar145GCAARegisteredCo).IsOptional();
            Property(x => x.ApplicantMailIdOrContactDetails).IsOptional().HasMaxLength(225);
            Property(x => x.HasNoObjectionLetter).IsOptional();

            HasRequired(x => x.UserServiceRequestMaster)
                .WithRequiredDependent(x => x.ExitOfAircraftSparePartsRequest).WillCascadeOnDelete(false);
        }
    }
}
