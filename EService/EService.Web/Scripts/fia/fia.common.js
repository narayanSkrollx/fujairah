﻿$(function () {
    fia.ServiceEnum = fia.ServiceEnum || {
        GroundHandlingRequest: 1, CranePermitRequest: 2, AircraftWarningLight: 3, AircraftDismantlingRequest: 4,
        AircraftMaintenanceRequest: 5, TradeLicenseRequest: 6, ExitOfAircraftSparePartsRequest: 7, MeteorologicalDataRequest: 8, BuildingHeightRequest: 9,
        Other: 1000
    };

    fia.ServiceFlowStateEnum = fia.ServiceFlowStateEnum ||
    {
        New: 1, Active: 2, Approved: 3, Rejected: 4, PaymentPending: 5, PaymentCompleted: 6
    };
});
