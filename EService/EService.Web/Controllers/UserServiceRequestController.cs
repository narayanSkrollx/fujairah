using System;
using System.Web.Mvc;
using EService.Service.Logic;
using EService.Web.Controllers.Base;
using EService.Web.Models;

namespace EService.Web.Controllers
{
    public class UserServiceRequestMasterController : BaseController
    {
        private UserServiceRequestMasterService userServiceRequestMasterService;
        private UserAttachmentService userAttachmentService;

        public UserServiceRequestMasterController()
        {
            userServiceRequestMasterService = new UserServiceRequestMasterService(serviceContext);
            userAttachmentService = new UserAttachmentService(serviceContext);
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult UploadedFiles(int userServiceRequestmasterId)
        {
            var result = new JsonResponseObject();

            try
            {
                result.Data = userAttachmentService.GetUploadedFileInfoServiceModels(userServiceRequestmasterId);
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while ";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}