﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EService.Data.Entity;
using EService.Data.Entity.Entity;

namespace EService.Web.Models
{
    public class BuildingHeightRequestViewModel : EServiceCreateBaseViewModel , IValidatableObject
    {
        public int UserServiceRequestMasterId { get; set; }

        #region "General Project Information"

        [Display(Name = "Project Name")]
        [Required(ErrorMessage = "Project Name is required")]
        [StringLength(225, ErrorMessage = "Project Name length cannot be greater than 225")]
        public string ProjectName { get; set; }
        [Display(Name = "Application Date")]
        [Required(ErrorMessage = "Application Date is required")]
        public DateTime? ApplicationDate { get; set; }
        [Display(Name = "Expected Construction Start Date")]
        public DateTime? ExpectedConstructionStartDate { get; set; }
        [Display(Name = "Expected Construction End Date")]
        public DateTime? ExpectedConstructionEndDate { get; set; }
        [Display(Name = "Project Location")]
        [StringLength(225, ErrorMessage = "Project Location length cannot be greater than 225")]
        public string Location { get; set; }
        [Display(Name = "Project Description")]
        [StringLength(225, ErrorMessage = "Project Description length cannot be greater than 225")]
        public string ProjectDescription { get; set; }


        #region "Applicant"
        [Display(Name = "Signature")]
        [StringLength(225, ErrorMessage = "Applicant Signature length cannot be greater than 225")]
        //[Required(ErrorMessage = "Signature is Required")]
        public string ApplicantSignature { get; set; }

        [Display(Name = "Applicant's Name")]
        [StringLength(225, ErrorMessage = "Applicant Name length cannot be greater than 225")]
        [Required(ErrorMessage = "Applicant Name is Required")]
        public string ApplicantName { get; set; }
        [Display(Name = "Applicant Job Title")]
        [StringLength(225, ErrorMessage = "Applicant Job Title length cannot be greater than 225")]
        [Required(ErrorMessage = "Applicant Job Title is Required")]
        public string ApplicantJobTitle { get; set; }
        [Display(Name = "Applicant’s Company Name")]
        [StringLength(225, ErrorMessage = "Applicant Company Name length cannot be greater than 225")]
        [Required(ErrorMessage = "Applicant Company Name is Required")]
        public string ApplicantCompanyName { get; set; }
        [Display(Name = "Applicant Address")]
        [StringLength(225, ErrorMessage = "Applicant Address length cannot be greater than 225")]
        [Required(ErrorMessage = "Applicant Address is Required")]
        public string ApplicantAddress { get; set; }
        [Display(Name = "Cell")]
        [StringLength(225, ErrorMessage = "Applicant Cell length cannot be greater than 225")]
        [Required(ErrorMessage = "Applicant Cell is Required")]
        public string ApplicantCell { get; set; }
        [Display(Name = "Tel")]
        [StringLength(225, ErrorMessage = "Applicant Tel No length cannot be greater than 225")]
        [Required(ErrorMessage = "Applicant Tel No is Required")]
        public string ApplicantTel { get; set; }
        [Display(Name = "Email")]
        [StringLength(225, ErrorMessage = "Applicant Email length cannot be greater than 225")]
        [Required(ErrorMessage = "Applicant Email is Required")]
        public string ApplicantEmail { get; set; }
        [Display(Name = "Website")]
        [StringLength(225, ErrorMessage = "Applicant Website length cannot be greater than 225")]
        public string ApplicantWebsite { get; set; }
        #endregion

        #region "Owner Name"
        [Display(Name = "Owner’s Name")]
        [StringLength(225, ErrorMessage = "Owner Name length cannot be greater than 225")]
        [Required(ErrorMessage = "Owner Name is Required")]
        public string OwnerName { get; set; }
        [Display(Name = "Owner’s Company Name")]
        [StringLength(225, ErrorMessage = "Owner Company Name length cannot be greater than 225")]
        [Required(ErrorMessage = "Owner Company Name is Required")]
        public string OwnerCompanyName { get; set; }
        [Display(Name = "Owner's Address")]
        [StringLength(225, ErrorMessage = "Owner Address length cannot be greater than 225")]
        [Required(ErrorMessage = "Owner Address is Required")]
        public string OwnerAddress { get; set; }
        [Display(Name = "Cell")]
        [StringLength(225, ErrorMessage = "Owner Cell length cannot be greater than 225")]
        [Required(ErrorMessage = "Owner Cell is Required")]
        public string OwnerCell { get; set; }
        [Display(Name = "Tel")]
        [StringLength(225, ErrorMessage = "Owner Tel length cannot be greater than 225")]
        [Required(ErrorMessage = "Owner Tel is Required")]
        public string OwnerTel { get; set; }
        [Display(Name = "Email")]
        [StringLength(225, ErrorMessage = "Owner Email length cannot be greater than 225")]
        [Required(ErrorMessage = "Owner Email is Required")]
        public string OwnerEmail { get; set; }
        [Display(Name = "Website")]
        [StringLength(225, ErrorMessage = "Owner Website length cannot be greater than 225")]
        public string OwnerWebsite { get; set; }
        #endregion

        [Display(Name = "Building")]
        public bool IsBuilding { get; set; }
        [Display(Name = "OHL/Communication Tower")]
        public bool IsOHLCommunicationTower { get; set; }
        [Display(Name = "Other")]
        public bool IsOtherType { get; set; }
        [Display(Name = "")]
        public string OtherTypeDescription { get; set; }
        #endregion

        #region "Buildings Details Only"
        /// <summary>
        /// true for singular building and false for multiple buildings
        /// </summary>
        [Display(Name = "Development Type?")]
        public bool? BuildingType { get; set; }
        [Display(Name = "How many?")]
        public int? NumberOfBuildings { get; set; }

        [Display(Name = "No. of floors")]
        public int? NumberOfFloors { get; set; }
        [Display(Name = "Tallest Height (in meter)")]
        public double? TallestHeight { get; set; }

        [Display(Name = "Do you plan to use any obstacle lights on your building(s)?")]
        public bool? HasObstacleLight { get; set; }
        [Display(Name = "If yes, provide details & locations in your drawings")]
        public string DetailsOfLightinDrawing { get; set; }

        #endregion

        #region "OHL/Communication tower projet details only"

        [Display(Name = "Type")]
        public bool? OHLOrCommunicationType { get; set; }
        [Display(Name = "No. of Structures")]
        public int? NumberOfStructure { get; set; }
        [Display(Name = "Type of OHLs/Comm. Tower ? ")]
        public string TypeOfOHLCommTower { get; set; }
        [Display(Name = "Are there mountains within 1000 meters of your structures ? ")]
        public bool? AreThereMountains { get; set; }

        #endregion


        #region"Applicant Owner Relationship"
        [Display(Name = "Applicant Owner Relationship")]
        [Required(ErrorMessage = "Applicant Owner Relationship must be selected.")]
        public int ApplicantOwnerRelationshipId { get; set; }
        public string ApplicantOwnerRelationshipName { get; set; }
        public string ApplicantOwnerRelationshipOtherName { get; set; }
        #endregion
        public int ProjectTypeId { get; set; }
        public string ProjectTypeDescription { get; set; }
        public string OtherProjectTypeDescription { get; set; }
        public OHLStructureCoordinateAndHeightViewModel OHLStructureCoordinateAndHeightAddModel { get; set; }


        //public List<ApplicantOwnerRelationshipViewModel> ApplicantRelationshipToProjectOwners { get; set; }
        public List<BuildingCoordinateAndHeightViewModel> BuildingCoordinateAndHeights { get; set; }
        public List<AdditionalItemsForBuildingSubmittalViewModel> AdditionalItemsForBuildingSubmittals { get; set; }
        public List<AviationObstacleViewModel> AviationObstacles { get; set; }
        public List<ApplicantOwnerRelationship> ApplicantOwnerRelationships { get; set; }
        public List<OHLStructureCoordinateAndHeightViewModel> OHLStructureCoordinateAndHeights { get; set; }
        public List<AdditionalItemsForOHLSubmittalViewModel> AdditionalItemsForOHLSubmittals { get; set; }
        public List<BuildingHeightApplicableFeeViewModel> BuildingHeightApplicableFees { get; set; }
        public List<ProjectTypesViewModel> ProjectTypes { get; set; }

        public BuildingHeightRequestViewModel()
        {
            ProjectTypes = new List<ProjectTypesViewModel>();
            OHLStructureCoordinateAndHeights = new List<OHLStructureCoordinateAndHeightViewModel>();
            OHLStructureCoordinateAndHeightAddModel = new OHLStructureCoordinateAndHeightViewModel();
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var result = new List<ValidationResult>();

            if(ExpectedConstructionStartDate > ExpectedConstructionEndDate)
            {
                result.Add(new ValidationResult("Expected Construction End Date must be greater than Start Date"));
            }
            return result;
        }
    }

    //public class ApplicantOwnerRelationshipViewModel
    //{
    //    public int Id { get; set; }
    //    public string Description { get; set; }
    //    public bool IsSelected { get; set; }
    //    public bool IsOther { get; set; }
    //    public string OtherValue { get; set; }
    //}

    public class BuildingCoordinateAndHeightViewModel
    {
        public int UserServiceRequestMasterId { get; set; }
        public int BuildingCoordinateAndHeightLookUpId { get; set; }
        public string BuildingCoordinateAndHeightLookUpDescription { get; set; }
        public string Easting { get; set; }
        public string Northing { get; set; }
        public double? GroundHeight { get; set; }
        public double? StructureHeight { get; set; }
    }

    public class AdditionalItemsForBuildingSubmittalViewModel
    {
        public int AdditionalItemsForBuildingSubmittalId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public int AdditionalItemsForBuildingSubmittalLookUpId { get; set; }
        public string AdditionalItemsForBuildingSubmittalLookUpDescription { get; set; }
        public bool IsOther { get; set; }
        public string OtherDescription { get; set; }
        public bool IsSelected { get; set; }
    }

    public class AviationObstacleViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool IsSelected { get; set; }
    }

    public class OHLStructureCoordinateAndHeightViewModel
    {
        public int OHLStructureCoordinateAndHeightId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public string StructureReferenceNumber { get; set; }
        public string Easting { get; set; }
        public string Northing { get; set; }
        public double GroundHeight { get; set; }
        public double StructureHeight { get; set; }
    }

    public class AdditionalItemsForOHLSubmittalViewModel
    {
        public int AdditionalItemsForOHLSubmittalId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public int AdditionalItemsForOHLSubmittalLookUpId { get; set; }
        public string AdditionalItemsForOHLSubmittalLookUpDescription { get; set; }
        public bool IsOther { get; set; }
        public string OtherDescription { get; set; }
        public bool IsSelected { get; set; }
    }

    public class ProjectTypesViewModel
    {
        public int ProjectTypeId { get; set; }
        public string Description { get; set; }
        public bool IsOther { get; set; }
        public string OtherDescription { get; set; }
        public bool IsSelected { get; set; }
    }

    public class BuildingHeightApplicableFeeViewModel
    {
        public int ApplicableFeesLookUpId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public string ApplicableFeesLookUpDescription { get; set; }
        public string Notes { get; set; }
        public string Fees { get; set; }
        public decimal? Fee { get; set; }

        public bool IsSelected { get; set; }
        public double Quantity { get; set; }
        public double TotalFee { get; set; }
    }
}