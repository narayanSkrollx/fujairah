﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Service.Logic.Enum;
using EService.Web.Controllers.Base;
using EService.Web.Helper;
using EService.Web.Models;
using EService.Web.Models.ServiceRequestOperation;
using EService.Core;
using EService.Web.Helper.EmailSender;

namespace EService.Web.Controllers
{
    public class ExitSparePartsRequestController : ServiceRequestBaseController
    {
        private ExitOfAircraftSparePartsRequestService exitOfSparePartsRequestService;
        private ServiceApprovalStageService serviceApprovalStageService;
        private UserServiceRequestMasterService userRequestService;
        private ServiceInfoService serviceInfoService;
        private UserService userService;

        public ExitSparePartsRequestController()
        {
            serviceApprovalStageService = new ServiceApprovalStageService(serviceContext);
            userRequestService = new UserServiceRequestMasterService(serviceContext);
            exitOfSparePartsRequestService = new ExitOfAircraftSparePartsRequestService(serviceContext);
            serviceInfoService = new ServiceInfoService(serviceContext);
            userService = new UserService(serviceContext);
        }

        public override ServiceEnum Service => ServiceEnum.ExitOfAircraftSparePartsRequest;

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ExitOfAircraftSparePartsRequestViewModel model, bool isDraft = false)
        {
            var result = new JsonResponseObject();
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.UserServiceRequestMasterId > 0)
                    {
                        var contract = MapViewModelToContract(model);
                        var attachmentList = new List<UserServiceRequestAttachment>();
                        #region "Uploaded Files Edit"
                        if (model.EditUploadedFiles != null && model.EditUploadedFiles.Any())
                        {
                            attachmentList.AddRange(model.EditUploadedFiles.Where(o => o.ObjectState != ObjectState.Deleted).Select(o => new UserServiceRequestAttachment { UserAttachmentId = o.Id }));

                        }
                        if (model.UploadedFiles != null && model.UploadedFiles.Any())
                        {
                            attachmentList.AddRange(model.UploadedFiles.Select(o => new UserServiceRequestAttachment { UserAttachmentId = o.Id }));

                        }

                        #endregion
                        var updateResult = exitOfSparePartsRequestService.UpdateExitSparePartsRequest(contract, isDraft, attachmentList);
                        result.IsSuccess = updateResult.IsSuccess;
                        result.Message = updateResult.Message;
                        if (!isDraft && updateResult.IsSuccess)
                        {
                            //TriggerEmail(FIAEmailType.New, model.UserServiceRequestMasterId);
                            EmailSenderHelper.SendNewRequestEmail(ControllerContext, model.UserServiceRequestMasterId);
                        }
                    }
                    else
                    {
                        var userServiceRequestMaster = SaveExitOfSparePartsRequest(model, isDraft);
                        if (!isDraft)
                        {
                            //TriggerEmail(FIAEmailType.New, userServiceRequestMaster.UserServiceRequestMasterId);
                            EmailSenderHelper.SendNewRequestEmail(ControllerContext, userServiceRequestMaster.UserServiceRequestMasterId);
                        }
                        result.IsSuccess = true;
                        result.Message = "Exit of Spare Parts Request saved successfully";
                    }
                }
                else
                {
                    result.Message = "Server Validation Error: " + ModelState.GetModelStateError();
                    result.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving Exit of Spare Parts Request.";
            }
            return Json(result);
        }

        public ActionResult Detail(int id)
        {
            ViewBag.UserServiceRequestMasterId = id;
            return View();
        }

        public ActionResult Edit(int id)
        {
            ViewBag.UserServiceRequestMasterId = id;
            return View("Create");
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Initialize()
        {
            var result = new JsonResponseObject();
            try
            {
                var data = userRequestService.GetServiceRequests(UserProfile.UserId, ServiceId);
                result.Data = data;
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while obtaining list";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InitializeCreate(int? id)
        {
            try
            {
                var model = new ExitOfAircraftSparePartsRequestViewModel();
                var reapplyData = TempData[Constants.ServiceRequestReapplyContentTempDataKey] as ServiceReapplyTempDataViewModel;
                if (reapplyData != null || id.HasValue)
                {
                    model = GetExitOfSparePartsRequest(id.Value);
                    if (reapplyData != null)
                    {
                        model.ParentUserServiceRequestMasterid = reapplyData.UserServiceRequestMasterId;
                        model.UserServiceRequestMasterId = 0;
                    }
                }
                else
                {
                    model.DateOfApplication = DateTime.Now.DubaiTime();
                }
                model.UserInfo = userService.GetUserBasicInfo(UserProfile.UserId).Data;
                model.EditUploadedFiles = GetUploadedFiles(reapplyData?.UserServiceRequestMasterId ?? (id ?? 0));

                return Json(new JsonResponseObject() { IsSuccess = true, Data = model }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return Json(new JsonResponseObject() { IsSuccess = false, Message = "Error" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InitializeDetail(int id)
        {
            try
            {
                var model = GetExitOfSparePartsRequest(id);
                return Json(new JsonResponseObject() { IsSuccess = true, Data = model }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return Json(new JsonResponseObject() { IsSuccess = false, Message = "Error" });
        }

        #region "Private Methods"

        public ExitOfAircraftSparePartsRequestViewModel GetExitOfSparePartsRequest(int id)
        {
            var contract = exitOfSparePartsRequestService.Find(x => x.UserServiceRequestMasterId == id);
            var model = MapContractToViewModel(contract);
            return model;
        }

        public UserServiceRequestMaster SaveExitOfSparePartsRequest(ExitOfAircraftSparePartsRequestViewModel model, bool isDraft = false)
        {
            var serviceFlowStages = serviceApprovalStageService.FindAllAsNoTracking(o => o.ServiceId == ServiceId).ToList();
            var serviceCode = userRequestService.GetNewServiceCode(ServiceId);
            var serviceInfo = serviceInfoService.Find(o => o.ServiceId == ServiceId);
            var userServiceRequest = new UserServiceRequestMaster()
            {
                RequestDateTime = DateTime.Now.DubaiTime(),
                ServiceId = ServiceId,
                Status = (int)ServiceFlowStateEnum.New,
                UserId = UserProfile.UserId,
                IsDeleted = false,
                IsDraft = isDraft,
                RegistrationNoYear = serviceCode.Data.Year,
                RegistrationNoMonth = serviceCode.Data.Month,
                RegistrationNoIndex = serviceCode.Data.RegistrationNoIndex,
                RegistrationNoServiceCode = serviceCode.Data.ServiceCode,
                RegistrationNo = serviceCode.Data.RegistrationNo,
                ParentUserServiceRequestMasterId = model.ParentUserServiceRequestMasterid,
                VAT = serviceInfo.VATPercentage,
                ServiceRequestFlowStages = serviceFlowStages
                    .Select(o => new ServiceRequestFlowStage()
                    {
                        Index = o.Index,
                        IsCurrentStage = o.Index == 1,
                        ServiceRequestFlowStageDepartments = o.ServiceApprovalStageDepartments.Select(p =>
                            new ServiceRequestFlowStageDepartment()
                            {
                                DepartmentId = p.DepartmentId,
                                Status = (int)ServiceFlowStateEnum.New,
                                IsApproved = false,
                            }).ToList()
                    }).ToList(),
                UserServiceRequestAttachments = model.UploadedFiles.Select(o => new UserServiceRequestAttachment()
                {
                    UserAttachmentId = o.Id
                }).ToList(),
                ServieBill = new UserServiceRequestBill()
                {
                    ChargedAmount = (double)serviceInfo.DefaultRate
                }
            };
            var request = MapViewModelToContract(model);
            request.UserServiceRequestMaster = userServiceRequest;
            exitOfSparePartsRequestService.Add(request);
            serviceContext.Commit();
            return userServiceRequest;
        }

        private ExitOfAircraftSparePartsRequestViewModel MapContractToViewModel(ExitOfAircraftSparePartsRequest contract)
        {
            var model = new ExitOfAircraftSparePartsRequestViewModel()
            {
                UserServiceRequestMasterId = contract.UserServiceRequestMasterId,
                DateOfApplication = contract.DateOfApplication,
                ApplicantCompany = contract.ApplicantCompany,
                AircraftType = contract.AircraftType,
                AircraftRegistrationNumber = contract.AircraftRegistrationNumber,
                OwnerOfAircraft = contract.OwnerOfAircraft,
                HasNoObjectionLetter = contract.HasNoObjectionLetter,
                NoObjectionLetterOfTheOwner = contract.NoObjectionLetterOfTheOwner,
                AircraftMaintenanceOrganisationOrCar145GCAARegisteredCo = contract.AircraftMaintenanceOrganisationOrCar145GCAARegisteredCo,
                ApplicantMailIdOrContactDetails = contract.ApplicantMailIdOrContactDetails,
                ExitAndReturnPassChecklists = contract.ExitAndReturnPassChecklists
                    .Select(x => new ExitAndReturnPassChecklistViewModel()
                    {
                        Description = x.Description,
                        PartNumber = x.PartNumber,
                        ExitToWhere = x.ExitToWhere,
                        NumberOfPieces = x.NumberOfPieces,
                        Origin = x.Origin,
                        ObjectState = ObjectState.Added
                    }).ToList()
            };
            return model;
        }

        private ExitOfAircraftSparePartsRequest MapViewModelToContract(ExitOfAircraftSparePartsRequestViewModel model)
        {
            var contract = new ExitOfAircraftSparePartsRequest
            {
                UserServiceRequestMasterId = model.UserServiceRequestMasterId,
                DateOfApplication = model.DateOfApplication,
                ApplicantCompany = model.ApplicantCompany,
                AircraftType = model.AircraftType,
                AircraftRegistrationNumber = model.AircraftRegistrationNumber,
                OwnerOfAircraft = model.OwnerOfAircraft,
                HasNoObjectionLetter = model.HasNoObjectionLetter,
                NoObjectionLetterOfTheOwner = model.NoObjectionLetterOfTheOwner,
                AircraftMaintenanceOrganisationOrCar145GCAARegisteredCo = model.AircraftMaintenanceOrganisationOrCar145GCAARegisteredCo,
                ApplicantMailIdOrContactDetails = model.ApplicantMailIdOrContactDetails,
                ExitAndReturnPassChecklists = model.ExitAndReturnPassChecklists
                    .Where(x => x.ObjectState == ObjectState.Added).Select(x => new ExitSparePartsPassChecklist()
                    {
                        Description = x.Description,
                        PartNumber = x.PartNumber,
                        ExitToWhere = x.ExitToWhere,
                        NumberOfPieces = x.NumberOfPieces,
                        Origin = x.Origin
                    }).ToList()
            };

            return contract;
        }

        #endregion "Private Methods"
    }
}