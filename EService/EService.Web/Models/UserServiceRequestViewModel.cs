﻿using System;

namespace EService.Web.Models
{
    public class UserServiceRequestViewModel
    {
        public int UserServiceRequestMasterId { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime RequestDateTime { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public int Status { get; set; }

        #region "Reject"
        public string Comments { get; set; }
        public bool IsUserSelectedForReject { get; set; }
        public int? SelectedStageId { get; set; }
        #endregion
    }
}