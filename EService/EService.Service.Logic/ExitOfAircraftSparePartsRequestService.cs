﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EService.Data.Entity.Entity;
using EService.Service.Logic.Enum;
using EService.Service.Model;

namespace EService.Service.Logic
{
    public class ExitOfAircraftSparePartsRequestService : BaseEService<ExitOfAircraftSparePartsRequest>
    {
        public ExitOfAircraftSparePartsRequestService(ServiceContext context) : base(context)
        {
        }

        public override ServiceEnum Service => ServiceEnum.ExitOfAircraftSparePartsRequest;

        public ResponseObject UpdateExitSparePartsRequest(ExitOfAircraftSparePartsRequest model, bool isDraft, List<UserServiceRequestAttachment> addAttachments)
        {
            var result = new ResponseObject();
            var contract = Find(x => x.UserServiceRequestMasterId == model.UserServiceRequestMasterId);
            if (contract == null)
            {
                result.IsSuccess = false;
                result.Message = "No request matched to edit.";
            }
            else
            {
                var list = contract.ExitAndReturnPassChecklists.ToList();
                Context.Database.ExitSparePartsPassChecklists.RemoveRange(list);

                var removeDocList = contract.UserServiceRequestMaster.UserServiceRequestAttachments.ToList();
                Context.Database.UserServiceRequestAttachment.RemoveRange(removeDocList);
                addAttachments.ForEach(o => contract.UserServiceRequestMaster.UserServiceRequestAttachments.Add(o));

                contract.UserServiceRequestMaster.IsDraft = isDraft;
                contract.DateOfApplication = model.DateOfApplication;
                contract.ApplicantCompany = model.ApplicantCompany;
                contract.AircraftType = model.AircraftType;
                contract.AircraftRegistrationNumber = model.AircraftRegistrationNumber;
                contract.HasNoObjectionLetter = model.HasNoObjectionLetter;
                contract.NoObjectionLetterOfTheOwner = model.NoObjectionLetterOfTheOwner;
                contract.OwnerOfAircraft = model.OwnerOfAircraft;
                contract.AircraftMaintenanceOrganisationOrCar145GCAARegisteredCo = model.AircraftMaintenanceOrganisationOrCar145GCAARegisteredCo;
                contract.ApplicantMailIdOrContactDetails = model.ApplicantMailIdOrContactDetails;
                contract.ExitAndReturnPassChecklists = model.ExitAndReturnPassChecklists;

                Context.Commit();
                result.IsSuccess = true;
                result.Message = "Exit of Aircraft spare parts request updated successfully";
            }
            return result;
        }
    }
}
