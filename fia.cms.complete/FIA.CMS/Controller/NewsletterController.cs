﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FIA.CMS.Entities;
using Umbraco.Core;

namespace FIA.CMS.Controller
{
    public class NewsletterController : BaseController
    {
        // GET: Newsletter
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Subscribe(string Email)
        {
            var db = ApplicationContext.DatabaseContext.Database;
            var list = db.Query<UserSubscription>("select * from UserSubscription Where Email =@0", Email);
            if (!list.Any())
            {
                var data = new UserSubscription()
                {
                    Email = Email,
                    //   IsSubscribe = true
                };
                db.Insert(data);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveNewsletterTemplate(NewsletterTemplate model)
        {
            var db = ApplicationContext.DatabaseContext.Database;
            var data = db.Query<NewsletterTemplate>("select * from NewsletterTemplate").FirstOrDefault();

            if (data != null)
            {
                data.Body = model.Body;
                data.Subject = model.Subject;
                db.Save(data);
            }
            else
            {
                db.Insert(new NewsletterTemplate
                {
                    Subject = model.Subject,
                    Body = model.Body
                });
            }
            return Content("true");
        }

        public ActionResult GetNewsletterTemplate()
        {
            var db = ApplicationContext.DatabaseContext.Database;
            var data = db.Query<NewsletterTemplate>("select * from NewsletterTemplate").FirstOrDefault();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetSubScriber()
        {
            var db = ApplicationContext.DatabaseContext.Database;
            var data = db.Query<UserSubscription>("select * from UserSubscription where Email !=''");

            return Json(data.DistinctBy(x => x.Email), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendNewsLetterEmail()
        {
            int emailSendCompleted = 0;
            int totalEmailSent = 0;
            try
            {
                var db = ApplicationContext.DatabaseContext.Database;
                var newsletterTemplate = db.Query<NewsletterTemplate>("select * from NewsletterTemplate").FirstOrDefault();
                if (newsletterTemplate != null)
                {
                    var userSubscrptions = db.Query<UserSubscription>("select * from UserSubscription");
                    totalEmailSent = userSubscrptions.Count();
                    foreach (var item in userSubscrptions)
                    {
                        base.SendEmail(newsletterTemplate.Body, item.Email, newsletterTemplate.Subject);
                        emailSendCompleted++;
                    }
                }
                return Content("true");
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Message = "Error while sending newsletter email. " });
            }
        }

        [HttpPost]
        public ActionResult SendTestNewsLetterEmail(string toEmail)
        {
            try
            {
                var db = ApplicationContext.DatabaseContext.Database;
                var newsletterTemplate = db.Query<NewsletterTemplate>("select * from NewsletterTemplate").FirstOrDefault();
                if (newsletterTemplate != null)
                {
                    base.SendEmail(newsletterTemplate.Body, toEmail, newsletterTemplate.Subject);
                }
                return Json(new { IsSuccess = true, Message = "Test Newsletter email sent successfully." });
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Message = "Error while sending Test Newsletter email." });
            }
        }
    }
}