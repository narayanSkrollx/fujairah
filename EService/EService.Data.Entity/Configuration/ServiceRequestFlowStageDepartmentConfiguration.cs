using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class ServiceRequestFlowStageDepartmentConfiguration :
        EntityTypeConfiguration<ServiceRequestFlowStageDepartment>
    {
        public ServiceRequestFlowStageDepartmentConfiguration()
        {
            ToTable("ServiceRequestFlowStageDepartment");

            HasKey(o => o.ServiceRequestFlowStageDepartmentId);
            Property(o => o.ServiceRequestFlowStageDepartmentId)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.ServiceRequestFlowStageId).IsRequired();
            Property(o => o.DepartmentId).IsRequired();
            Property(o => o.DepartmentUserId).IsOptional();
            Property(o => o.Status).IsRequired();
            Property(o => o.StatusChangedDateTime).IsOptional();
            Property(o => o.IsApproved).IsOptional();

            HasRequired(o => o.ServiceRequestFlowStage)
                .WithMany(o => o.ServiceRequestFlowStageDepartments)
                .HasForeignKey(o => o.ServiceRequestFlowStageId);
            HasRequired(o => o.Department)
                .WithMany(o => o.ServiceRequestFlowStageDepartments)
                .HasForeignKey(o => o.DepartmentId);
            HasOptional(o => o.DepartmentUser).
                WithMany().
                HasForeignKey(o => o.DepartmentUserId);
        }
    }
}