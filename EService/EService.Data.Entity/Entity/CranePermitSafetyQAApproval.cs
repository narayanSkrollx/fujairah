using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class CranePermitSafetyQAApproval :AuditableEntity
    {
        public int UserServiceRequestMasterId { get; set; }
        public bool HasSafetyAssessmentBeenDone { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedByPosition { get; set; }
        public DateTime ApprovedDateTime { get; set; }
        public string OperatingRestrictions { get; set; }
        public string AdditionalComments { get; set; }
        [JsonIgnore]
        public virtual  CranePermitRequest CranePermitRequest { get; set; }
        public virtual ICollection<CranePermitSafetyFileAttachment> Attachments { get; set; }
    }

    public class CranePermitSafetyFileAttachment
    {
        public int CranePermitSafetyFileAttachmentId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public int UserAttachmentId { get; set; }

        public virtual CranePermitSafetyQAApproval SafetyUserApproval { get; set; }
        public virtual UserAttachment Attachment { get; set; }
    }
}