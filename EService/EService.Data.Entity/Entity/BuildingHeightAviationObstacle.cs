﻿using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class BuildingHeightAviationObstacle
    {
        public int BuildingHeightAviationObstacleId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public int AviationObstacleLookUpId { get; set; }

        [JsonIgnore]
        public virtual AviationObstacleLookUp AviationObstacleLookUp { get; set; }

        [JsonIgnore]
        public virtual BuildingHeightRequest BuildingHeightRequest { get; set; }
    }
}