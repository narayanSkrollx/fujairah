﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.BusinessLogic;
using umbraco.BusinessLogic.Actions;
using umbraco.cms.presentation.Trees;
using umbraco.DataLayer;
using umbraco.interfaces;

namespace Designit.Umb.Newsletter.Bll
{
    public class loadNewsletterCampaign: BaseTree
    {
        public loadNewsletterCampaign(string application) :
            base(application) { }


        protected override void CreateRootNode(ref XmlTreeNode rootNode)
        {
            rootNode.Icon = FolderIcon;
            rootNode.OpenIcon = FolderIconOpen;
            rootNode.NodeType = TreeAlias;
            rootNode.NodeID = "init";
            
            
        }

        public override void RenderJS(ref System.Text.StringBuilder Javascript)
        {
            Javascript.Append(
                @"
                        function openNewsletterCampaign(id)
                        {
                           parent.right.document.location.href = 'plugins/designit/newsletter/editNewsletterCampaign.aspx?id=' + id;
                        }
        
                ");

            
        }


        public override void Render(ref XmlTree tree)
        {
            using (var reader = Application.SqlHelper.ExecuteReader("select * from newsletterCampaign"))
            {
                while (reader.Read())
                {
                    XmlTreeNode xNode = XmlTreeNode.Create(this);
                    xNode.NodeID = reader.GetInt("id").ToString();
                    xNode.Text = reader.GetString("alias");
                    xNode.Icon = "folder.gif";
                    //xNode.HasChildren = true;
                    xNode.Menu.Clear();
                    xNode.Action = "javascript:openNewsletterCampaign(" + reader.GetInt("id").ToString() + ")";
                    tree.Add(xNode);

                }
            }
        }
    }
}

