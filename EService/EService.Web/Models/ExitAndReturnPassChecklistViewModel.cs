﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace EService.Web.Models
{
    public class ExitAndReturnPassChecklistViewModel
    {
        public int ExitSparePartsPassChecklistId { get; set; }
        public int UserServiceRequestMasterId { get; set; }

        [Required(ErrorMessage = "Required")]
        public string Description { get; set; }
        public string PartNumber { get; set; }
        public int? NumberOfPieces { get; set; }
        public string Origin { get; set; }
        public string ExitToWhere { get; set; }

        public ObjectState ObjectState { get; set; }
    }

    public enum ObjectState
    {
        Added = 0,
        Edited = 1,
        Deleted = 2,
        Unchanged = 3
    }
}