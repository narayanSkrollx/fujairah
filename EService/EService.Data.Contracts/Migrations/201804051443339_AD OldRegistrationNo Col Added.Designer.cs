// <auto-generated />
namespace EService.Data.Contracts.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ADOldRegistrationNoColAdded : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ADOldRegistrationNoColAdded));
        
        string IMigrationMetadata.Id
        {
            get { return "201804051443339_AD OldRegistrationNo Col Added"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
