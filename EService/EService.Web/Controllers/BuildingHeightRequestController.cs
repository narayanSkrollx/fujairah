﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Service.Logic.Enum;
using EService.Web.Controllers.Base;
using EService.Web.Helper;
using EService.Web.Models;
using EService.Web.Models.ServiceRequestOperation;
using Nelibur.ObjectMapper;
using EService.Core;
namespace EService.Web.Controllers
{
    public class BuildingHeightRequestController : ServiceRequestBaseController
    {
        private BuildingHeightRequestService buildingHeightRequestService;
        private ServiceApprovalStageService serviceApprovalStageService;
        private UserServiceRequestMasterService userRequestService;
        private LookupService _list;
        private ServiceInfoService serviceInfoService;
        private UserService userService;
        private ApplicantOwnerRelationshipService applicantOwnerRelationshipService;

        public BuildingHeightRequestController()
        {
            _list = new LookupService();
            serviceApprovalStageService = new ServiceApprovalStageService(serviceContext);
            userRequestService = new UserServiceRequestMasterService(serviceContext);
            buildingHeightRequestService = new BuildingHeightRequestService(serviceContext);
            serviceInfoService = new ServiceInfoService(serviceContext);
            userService = new UserService(serviceContext);
            applicantOwnerRelationshipService = new ApplicantOwnerRelationshipService(serviceContext);
        }

        public override ServiceEnum Service => ServiceEnum.BuildingHeightRequest;

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(BuildingHeightRequestViewModel model, bool isDraft = false)
        {
            var result = new JsonResponseObject();
            try
            {
                if (ModelState.IsValid)
                {
                    var okayForPayment = false;
                    var requestid = 0;
                    if (model.UserServiceRequestMasterId > 0)
                    {
                        var contract = MapViewModelToContract(model);
                        var attachmentList = new List<UserServiceRequestAttachment>();
                        #region "Uploaded Files Edit"
                        if (model.EditUploadedFiles != null && model.EditUploadedFiles.Any())
                        {
                            attachmentList.AddRange(model.EditUploadedFiles.Where(o => o.ObjectState != ObjectState.Deleted).Select(o => new UserServiceRequestAttachment { UserAttachmentId = o.Id }));

                        }
                        if (model.UploadedFiles != null && model.UploadedFiles.Any())
                        {
                            attachmentList.AddRange(model.UploadedFiles.Select(o => new UserServiceRequestAttachment { UserAttachmentId = o.Id }));

                        }

                        #endregion
                        var updateResult = buildingHeightRequestService.UpdateBuildingHeightRequest(contract, true, attachmentList);
                        result.IsSuccess = updateResult.IsSuccess;
                        result.Message = updateResult.Message;
                        if (updateResult.IsSuccess)
                        {
                            okayForPayment = true;
                            requestid = contract.UserServiceRequestMasterId;
                        }
                        //if (!isDraft && updateResult.IsSuccess)
                        //{
                        //    TriggerEmail(FIAEmailType.New, model.UserServiceRequestMasterId);
                        //}
                    }
                    else
                    {
                        var userServiceRequestMaster = SaveBuildingHeightRequest(model, true);
                        //if (!isDraft)
                        //{
                        //    TriggerEmail(FIAEmailType.New, userServiceRequestMaster.UserServiceRequestMasterId);
                        //}
                        okayForPayment = true;
                        requestid = userServiceRequestMaster.UserServiceRequestMasterId;
                        result.IsSuccess = true;
                        result.Message = "Building Height Request saved successfully";
                    }

                    if (!isDraft && okayForPayment && requestid > 0)
                    {
                        //proceed to payment
                        var paymentRelatedInfo = userRequestService.GetPaymentRelatedServiceInfo(requestid, UserProfile.UserId, true);
                        if (!paymentRelatedInfo.IsSuccess)
                        {
                            result.IsSuccess = false;
                            result.Message = paymentRelatedInfo.Message;
                        }
                        else
                        {
                            var serviceInfo = userRequestService.Find(o => o.UserServiceRequestMasterId == requestid);
                            paymentRelatedInfo.Data.ChargedAmount = (double)model.BuildingHeightApplicableFees.Sum(o => o.Quantity * (double)(o.Fee ?? 0));
                            paymentRelatedInfo.Data.VATPercentage = serviceInfo.VAT;
                            paymentRelatedInfo.Data.SuccessUrl = Url.Action("PreSubmitPaymentSuccess", "PaymentResponse", null, this.Request.Url.Scheme);
                            paymentRelatedInfo.Data.FailureUrl = Url.Action("PreSubmitPaymentSuccess", "PaymentResponse", null, this.Request.Url.Scheme);

                            TempData[Constants.PaymentRequestMasterContentKey] = paymentRelatedInfo.Data;
                            result.IsSuccess = true;
                            result.Message = "Proceeding to payment";
                            result.Data = new
                            {
                                ToPayment = true
                            };
                        }
                    }
                }
                else
                {
                    result.Message = "Server Validation Error: " + ModelState.GetModelStateError();
                    result.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "Error while saving Building Height Request.";
                LogError(ex);
            }
            return Json(result);
        }

        public ActionResult Detail(int id)
        {
            ViewBag.UserServiceRequestMasterId = id;
            return View();
        }

        public ActionResult Edit(int id)
        {
            ViewBag.UserServiceRequestMasterId = id;
            return View("Create");
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Initialize()
        {
            var result = new JsonResponseObject();
            try
            {
                var data = userRequestService.GetServiceRequests(UserProfile.UserId, ServiceId);
                result.Data = data;
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "Error while obtaining list";
                LogError(ex);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InitializeCreate(int? id)
        {
            try
            {
                var model = new BuildingHeightRequestViewModel();

                model.ApplicationDate = DateTime.Now.DubaiTime();

                model.BuildingCoordinateAndHeights = _list.GetGenericList<BuildingCoordinateAndHeightLookUp>()
                    .Select(x => new BuildingCoordinateAndHeightViewModel()
                    {
                        BuildingCoordinateAndHeightLookUpId = x.Id,
                        BuildingCoordinateAndHeightLookUpDescription = x.Description
                    }).ToList();

                model.AdditionalItemsForBuildingSubmittals = _list.GetGenericList<AdditionalItemsForBuildingSubmittalLookUp>()
                    .Select(x => new AdditionalItemsForBuildingSubmittalViewModel()
                    {
                        AdditionalItemsForBuildingSubmittalLookUpId = x.Id,
                        AdditionalItemsForBuildingSubmittalLookUpDescription = x.Description,
                        IsOther = x.IsOther,
                        IsSelected = false
                    }).ToList();

                model.AviationObstacles = _list.GetGenericList<AviationObstacleLookUp>()
                    .Select(x => new AviationObstacleViewModel()
                    {
                        Id = x.Id,
                        Description = x.Description,
                        IsSelected = false
                    }).ToList();

                model.AdditionalItemsForOHLSubmittals = _list.GetGenericList<AdditionalItemsForOHLSubmittalLookUp>()
                    .Select(x => new AdditionalItemsForOHLSubmittalViewModel()
                    {
                        AdditionalItemsForOHLSubmittalLookUpId = x.Id,
                        AdditionalItemsForOHLSubmittalLookUpDescription = x.Description,
                        IsOther = x.IsOther,
                        IsSelected = false
                    }).ToList();
                model.BuildingHeightApplicableFees = _list.GetGenericList<ApplicableFeesLookUp>()
                    .Select(x => new BuildingHeightApplicableFeeViewModel()
                    {
                        ApplicableFeesLookUpId = x.Id,
                        ApplicableFeesLookUpDescription = x.Description,
                        Notes = x.Notes,
                        Fees = x.Fees,
                        Fee = x.Fee,
                        IsSelected = false
                    }).ToList();
                model.ProjectTypes = _list.GetGenericList<ProjectType>()
                  .Select(x => new ProjectTypesViewModel()
                  {
                      Description = x.Description,
                      IsOther = x.IsOther,
                      OtherDescription = "",
                      ProjectTypeId = x.Id,
                      IsSelected = false
                  }).ToList();
                model.ApplicantOwnerRelationships = applicantOwnerRelationshipService.FindAllAsNoTracking(o => o.Active)
                                .AsEnumerable()
                                .Select(o => new ApplicantOwnerRelationship() { Id = o.Id, Description = o.Description, IsOther = o.IsOther })
                                .ToList();
                //todo: reapply check for building height

                var reapplyData = TempData[Constants.ServiceRequestReapplyContentTempDataKey] as ServiceReapplyTempDataViewModel;
                if (reapplyData != null || id.HasValue)
                {
                    var editId = reapplyData != null ? reapplyData.UserServiceRequestMasterId : id;
                    var userServiceRequest =
                        userRequestService.Find(
                            o => o.UserServiceRequestMasterId == editId &&
                                 !o.IsDeleted && o.UserId == UserProfile.UserId && o.ServiceId == ServiceId);

                    if (userServiceRequest == null || userServiceRequest.BuildingHeightRequest == null)
                    {
                        return Json(new JsonResponseObject() { IsSuccess = false, Message = $"Invalid User Service Request for  {(reapplyData != null ? @"Reapply" : "Edit")}." });
                    }
                    else
                    {
                        var buildingHeightRequest = userServiceRequest.BuildingHeightRequest;


                        TinyMapper.Bind<BuildingHeightRequest, BuildingHeightRequestViewModel>();

                        var buildingHeightRequestViewModel = TinyMapper.Map<BuildingHeightRequestViewModel>(buildingHeightRequest);

                        buildingHeightRequestViewModel.ParentUserServiceRequestMasterid = reapplyData?.UserServiceRequestMasterId ?? null;
                        //buildingHeightRequestViewModel.ApplicantRelationshipToProjectOwners
                        buildingHeightRequestViewModel.ApplicantOwnerRelationships =
                            applicantOwnerRelationshipService.FindAllAsNoTracking(o => o.Active)
                                .AsEnumerable()
                                .Select(o => new ApplicantOwnerRelationship() { Id = o.Id, Description = o.Description, IsOther = o.IsOther })
                                .ToList();
                        buildingHeightRequestViewModel.ProjectTypes = _list.GetGenericList<ProjectType>()
                            .Select(x => new ProjectTypesViewModel()
                            {
                                Description = x.Description,
                                IsOther = x.IsOther,
                                OtherDescription = "",
                                ProjectTypeId = x.Id,
                                IsSelected = false
                            }).ToList();
                        buildingHeightRequestViewModel.BuildingCoordinateAndHeights = (from coordinate in model.BuildingCoordinateAndHeights
                                                                                       join savedCoordinate in buildingHeightRequest.BuildingCoordinateAndHeights on coordinate.BuildingCoordinateAndHeightLookUpId equals savedCoordinate.BuildingCoordinateAndHeightLookUpId
                                                                                                                   into leftJoinCoordinate
                                                                                       from defaultCoordinate in leftJoinCoordinate.DefaultIfEmpty()
                                                                                       select new BuildingCoordinateAndHeightViewModel()
                                                                                       {
                                                                                           BuildingCoordinateAndHeightLookUpDescription = coordinate.BuildingCoordinateAndHeightLookUpDescription,
                                                                                           BuildingCoordinateAndHeightLookUpId = coordinate.BuildingCoordinateAndHeightLookUpId,
                                                                                           Easting = defaultCoordinate?.Easting,
                                                                                           GroundHeight = defaultCoordinate?.GroundHeight,
                                                                                           Northing = defaultCoordinate?.Northing,
                                                                                           StructureHeight = defaultCoordinate?.StructureHeight,

                                                                                       }).ToList();

                        buildingHeightRequestViewModel.AdditionalItemsForBuildingSubmittals = (from item in model.AdditionalItemsForBuildingSubmittals
                                                                                               join savedItem in buildingHeightRequest.AdditionalItemsForBuildingSubmittals on item.AdditionalItemsForBuildingSubmittalId
                                                                                               equals savedItem.AdditionalItemsForBuildingSubmittalId
                                                                                                                   into leftJoinItem
                                                                                               from defaultItem in leftJoinItem.DefaultIfEmpty()
                                                                                               select new AdditionalItemsForBuildingSubmittalViewModel()
                                                                                               {
                                                                                                   AdditionalItemsForBuildingSubmittalLookUpDescription = item.AdditionalItemsForBuildingSubmittalLookUpDescription,
                                                                                                   AdditionalItemsForBuildingSubmittalLookUpId = item.AdditionalItemsForBuildingSubmittalLookUpId,
                                                                                                   IsOther = item.IsOther,
                                                                                                   IsSelected = defaultItem != null,
                                                                                                   OtherDescription = defaultItem?.OtherDescription
                                                                                               }).ToList();
                        buildingHeightRequestViewModel.AviationObstacles = (from item in model.AviationObstacles
                                                                            join savedItem in buildingHeightRequest.BuildingHeightAviationObstacles on item.Id
                                                                            equals savedItem.AviationObstacleLookUpId
                                                                                                into leftJoinItem
                                                                            from defaultItem in leftJoinItem.DefaultIfEmpty()
                                                                            select new AviationObstacleViewModel()
                                                                            {
                                                                                Description = item.Description,
                                                                                Id = item.Id,
                                                                                IsSelected = defaultItem != null,
                                                                            }).ToList();

                        buildingHeightRequestViewModel.AdditionalItemsForOHLSubmittals = (from item in model.AdditionalItemsForOHLSubmittals
                                                                                          join savedItem in buildingHeightRequest.AdditionalItemsForOHLSubmittals on item.AdditionalItemsForOHLSubmittalLookUpId
                                                                                                     equals savedItem.AdditionalItemsForOHLSubmittalLookUpId
                                                                                                                         into leftJoinItem
                                                                                          from defaultItem in leftJoinItem.DefaultIfEmpty()
                                                                                          select new AdditionalItemsForOHLSubmittalViewModel()
                                                                                          {
                                                                                              IsOther = item.IsOther,
                                                                                              IsSelected = defaultItem != null,
                                                                                              OtherDescription = defaultItem?.OtherDescription,
                                                                                              AdditionalItemsForOHLSubmittalLookUpDescription = item.AdditionalItemsForOHLSubmittalLookUpDescription,
                                                                                              AdditionalItemsForOHLSubmittalLookUpId = item.AdditionalItemsForOHLSubmittalLookUpId
                                                                                          }).ToList();

                        buildingHeightRequestViewModel.BuildingHeightApplicableFees = (from item in model.BuildingHeightApplicableFees
                                                                                       join savedItem in buildingHeightRequest.BuildingHeightApplicableFees on item.ApplicableFeesLookUpId
                                                                                                       equals savedItem.ApplicableFeesLookUpId
                                                                                                                           into leftJoinItem
                                                                                       from defaultItem in leftJoinItem.DefaultIfEmpty()
                                                                                       select new BuildingHeightApplicableFeeViewModel()
                                                                                       {
                                                                                           ApplicableFeesLookUpDescription = item.ApplicableFeesLookUpDescription,
                                                                                           ApplicableFeesLookUpId = item.ApplicableFeesLookUpId,
                                                                                           IsSelected = defaultItem != null,
                                                                                           Fee = item.Fee,
                                                                                           Fees = item.Fees,
                                                                                           Quantity = defaultItem?.Quantity ?? 0,
                                                                                           Notes = item.Notes,
                                                                                           TotalFee = ((double)(item.Fee ?? 0)) * (defaultItem?.Quantity ?? 0)
                                                                                       }).ToList();


                        model = buildingHeightRequestViewModel;
                        model.ParentUserServiceRequestMasterid = reapplyData?.UserServiceRequestMasterId;
                        model.UserServiceRequestMasterId = reapplyData != null ? 0 : id ?? 0;

                    }
                }

                model.UserInfo = userService.GetUserBasicInfo(UserProfile.UserId).Data;
                model.EditUploadedFiles = GetUploadedFiles(reapplyData?.UserServiceRequestMasterId ?? (id ?? 0));
                //model.UploadedFiles = 
                return Json(new JsonResponseObject() { IsSuccess = true, Data = model }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return Json(new JsonResponseObject() { IsSuccess = false, Message = "Error" });
        }

        public JsonResult InitializeDetail(int id)
        {
            try
            {
                var model = GetBuildingHeightRequest(id);
                return Json(new JsonResponseObject() { IsSuccess = true, Data = model }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return Json(new JsonResponseObject() { IsSuccess = false, Message = "Error" });
        }

        public ActionResult Reapply()
        {
            return View("Create");
        }

        #region "Private Methods"

        public BuildingHeightRequestViewModel GetBuildingHeightRequest(int id)
        {
            var contract = buildingHeightRequestService.Find(x => x.UserServiceRequestMasterId == id);
            var model = MapContractToViewModel(contract);
            return model;
        }

        public UserServiceRequestMaster SaveBuildingHeightRequest(BuildingHeightRequestViewModel model, bool isDraft = false)
        {
            var serviceFlowStages = serviceApprovalStageService.FindAllAsNoTracking(o => o.ServiceId == ServiceId).ToList();
            var serviceCode = userRequestService.GetNewServiceCode(ServiceId);
            var serviceInfo = serviceInfoService.Find(o => o.ServiceId == ServiceId);
            var userServiceRequest = new UserServiceRequestMaster()
            {
                RequestDateTime = DateTime.Now.DubaiTime(),
                ServiceId = ServiceId,
                Status = (int)ServiceFlowStateEnum.New,
                UserId = UserProfile.UserId,
                IsDeleted = false,
                IsDraft = isDraft,
                RegistrationNoYear = serviceCode.Data.Year,
                RegistrationNoMonth = serviceCode.Data.Month,
                RegistrationNoIndex = serviceCode.Data.RegistrationNoIndex,
                RegistrationNoServiceCode = serviceCode.Data.ServiceCode,
                RegistrationNo = serviceCode.Data.RegistrationNo,
                ParentUserServiceRequestMasterId = model.ParentUserServiceRequestMasterid,
                VAT = serviceInfo.VATPercentage,
                ServiceRequestFlowStages = serviceFlowStages
                    .Select(o => new ServiceRequestFlowStage()
                    {
                        Index = o.Index,
                        IsCurrentStage = o.Index == 1,
                        ServiceRequestFlowStageDepartments = o.ServiceApprovalStageDepartments.Select(p =>
                            new ServiceRequestFlowStageDepartment()
                            {
                                DepartmentId = p.DepartmentId,
                                Status = (int)ServiceFlowStateEnum.New,
                                IsApproved = false,
                            }).ToList()
                    }).ToList(),
                ServieBill = new UserServiceRequestBill()
                {
                    ChargedAmount = (double)serviceInfo.DefaultRate
                },
                UserServiceRequestAttachments = model.UploadedFiles.Select(o => new UserServiceRequestAttachment { UserAttachmentId = o.Id }).ToList()
            };
            var request = MapViewModelToContract(model);
            request.UserServiceRequestMaster = userServiceRequest;
            buildingHeightRequestService.Add(request);
            serviceContext.Commit();
            return userServiceRequest;
        }

        private BuildingHeightRequestViewModel MapContractToViewModel(BuildingHeightRequest contract)
        {
            var model = new BuildingHeightRequestViewModel
            {
                UserServiceRequestMasterId = contract.UserServiceRequestMasterId,
                ProjectName = contract.ProjectName,
                ApplicationDate = contract.ApplicationDate,
                ExpectedConstructionStartDate = contract.ExpectedConstructionStartDate,
                ExpectedConstructionEndDate = contract.ExpectedConstructionEndDate,
                Location = contract.Location,
                ProjectDescription = contract.ProjectDescription,
                ApplicantSignature = contract.ApplicantSignature,
                ApplicantName = contract.ApplicantName,
                ApplicantJobTitle = contract.ApplicantJobTitle,
                ApplicantCompanyName = contract.ApplicantCompanyName,
                ApplicantAddress = contract.ApplicantAddress,
                ApplicantCell = contract.ApplicantCell,
                ApplicantTel = contract.ApplicantTel,
                ApplicantEmail = contract.ApplicantEmail,
                ApplicantWebsite = contract.ApplicantWebsite,
                OwnerName = contract.OwnerName,
                OwnerCompanyName = contract.OwnerCompanyName,
                OwnerAddress = contract.OwnerAddress,
                OwnerCell = contract.OwnerCell,
                OwnerTel = contract.OwnerTel,
                OwnerEmail = contract.OwnerEmail,
                OwnerWebsite = contract.OwnerWebsite,
                IsBuilding = contract.IsBuilding,
                IsOHLCommunicationTower = contract.IsOHLCommunicationTower,
                IsOtherType = contract.IsOtherType,
                OtherTypeDescription = contract.OtherTypeDescription,
                BuildingType = contract.BuildingType,
                NumberOfBuildings = contract.NumberOfBuildings,
                NumberOfFloors = contract.NumberOfFloors,
                TallestHeight = contract.TallestHeight,
                HasObstacleLight = contract.HasObstacleLight,
                DetailsOfLightinDrawing = contract.DetailsOfLightinDrawing,
                OHLOrCommunicationType = contract.OHLOrCommunicationType,
                NumberOfStructure = contract.NumberOfStructure,
                TypeOfOHLCommTower = contract.TypeOfOHLCommTower,
                AreThereMountains = contract.AreThereMountains,
                ApplicantOwnerRelationshipOtherName = contract.ApplicantOwnerRelationshipOtherName,
                ApplicantOwnerRelationshipId = contract.ApplicantOwnerRelationshipId,
                ApplicantOwnerRelationshipName = contract.ApplicantOwnerRelationship.Description,
                ProjectTypeId = contract.ProjectTypeId,
                ProjectTypeDescription = contract.ProjectType.Description,
                OtherProjectTypeDescription = contract.OtherProjectTypeDescription,
                BuildingCoordinateAndHeights = contract.BuildingCoordinateAndHeights.Select(x => new BuildingCoordinateAndHeightViewModel()
                {
                    BuildingCoordinateAndHeightLookUpId = x.BuildingCoordinateAndHeightLookUpId,
                    Easting = x.Easting,
                    Northing = x.Northing,
                    GroundHeight = x.GroundHeight,
                    StructureHeight = x.StructureHeight
                }).ToList(),
                AdditionalItemsForBuildingSubmittals = contract.AdditionalItemsForBuildingSubmittals.Select(x => new AdditionalItemsForBuildingSubmittalViewModel()
                {
                    AdditionalItemsForBuildingSubmittalLookUpId = x.AdditionalItemsForBuildingSubmittalLookUpId,
                    OtherDescription = x.AdditionalItemsForBuildingSubmittalLookUp.IsOther ? x.OtherDescription : string.Empty
                }).ToList(),
                AviationObstacles = contract.BuildingHeightAviationObstacles.Select(x => new AviationObstacleViewModel()
                {
                    Id = x.AviationObstacleLookUpId
                }).ToList(),
                OHLStructureCoordinateAndHeights = contract.OHLStructureCoordinateAndHeights.Select(x => new OHLStructureCoordinateAndHeightViewModel()
                {
                    StructureReferenceNumber = x.StructureReferenceNumber,
                    Easting = x.Easting,
                    Northing = x.Northing,
                    GroundHeight = x.GroundHeight,
                    StructureHeight = x.StructureHeight
                }).ToList(),
                AdditionalItemsForOHLSubmittals = contract.AdditionalItemsForOHLSubmittals.Select(x => new AdditionalItemsForOHLSubmittalViewModel()
                {
                    AdditionalItemsForOHLSubmittalLookUpId = x.AdditionalItemsForOHLSubmittalLookUpId,
                    OtherDescription = x.AdditionalItemsForOHLSubmittalLookUp.IsOther ? x.OtherDescription : string.Empty,
                    IsOther = x.AdditionalItemsForOHLSubmittalLookUp.IsOther,
                    AdditionalItemsForOHLSubmittalLookUpDescription = x.AdditionalItemsForOHLSubmittalLookUp.Description,
                    IsSelected = true
                }).ToList(),
                BuildingHeightApplicableFees = contract.BuildingHeightApplicableFees.Select(x => new BuildingHeightApplicableFeeViewModel()
                {
                    ApplicableFeesLookUpId = x.ApplicableFeesLookUpId,
                    ApplicableFeesLookUpDescription = x.ApplicableFeesLookUp.Description,
                    Fees = x.ApplicableFeesLookUp.Fee.ToString(),
                    IsSelected = x.IsSelected,
                    Quantity = x.Quantity,
                    Fee = x.ApplicableFeesLookUp.Fee,
                    UserServiceRequestMasterId = x.UserServiceRequestMasterId,

                    TotalFee = (double)(x.TotalFee) * x.Quantity
                }).ToList(),
                ProjectTypes = _list.GetGenericList<ProjectType>().Select(o => new ProjectTypesViewModel
                {
                    Description = o.Description,
                    IsOther = o.IsOther,
                    IsSelected = false,
                    ProjectTypeId = o.Id
                }).ToList()
            };
            return model;
        }

        private BuildingHeightRequest MapViewModelToContract(BuildingHeightRequestViewModel model)
        {
            var contract = new BuildingHeightRequest
            {
                UserServiceRequestMasterId = model.UserServiceRequestMasterId,
                ProjectName = model.ProjectName,
                ApplicationDate = model.ApplicationDate,
                ExpectedConstructionStartDate = model.ExpectedConstructionStartDate,
                ExpectedConstructionEndDate = model.ExpectedConstructionEndDate,
                Location = model.Location,
                ProjectDescription = model.ProjectDescription,
                ApplicantSignature = model.ApplicantSignature,
                ApplicantName = model.ApplicantName,
                ApplicantJobTitle = model.ApplicantJobTitle,
                ApplicantCompanyName = model.ApplicantCompanyName,
                ApplicantAddress = model.ApplicantAddress,
                ApplicantCell = model.ApplicantCell,
                ApplicantTel = model.ApplicantTel,
                ApplicantEmail = model.ApplicantEmail,
                ApplicantWebsite = model.ApplicantWebsite,
                OwnerName = model.OwnerName,
                OwnerCompanyName = model.OwnerCompanyName,
                OwnerAddress = model.OwnerAddress,
                OwnerCell = model.OwnerCell,
                OwnerTel = model.OwnerTel,
                OwnerEmail = model.OwnerEmail,
                OwnerWebsite = model.OwnerWebsite,
                IsBuilding = model.IsBuilding,
                IsOHLCommunicationTower = model.IsOHLCommunicationTower,
                IsOtherType = model.IsOtherType,
                OtherTypeDescription = model.OtherTypeDescription,
                BuildingType = model.BuildingType,
                NumberOfBuildings = model.NumberOfBuildings,
                NumberOfFloors = model.NumberOfFloors,
                TallestHeight = model.TallestHeight,
                HasObstacleLight = model.HasObstacleLight,
                DetailsOfLightinDrawing = model.DetailsOfLightinDrawing,
                OHLOrCommunicationType = model.OHLOrCommunicationType,
                NumberOfStructure = model.NumberOfStructure,
                TypeOfOHLCommTower = model.TypeOfOHLCommTower,
                AreThereMountains = model.AreThereMountains,
                ApplicantOwnerRelationshipId = model.ApplicantOwnerRelationshipId,
                ApplicantOwnerRelationshipOtherName = model.ApplicantOwnerRelationshipOtherName,
                ProjectTypeId = model.ProjectTypeId,
                OtherProjectTypeDescription = model.OtherProjectTypeDescription,
                BuildingCoordinateAndHeights = model.BuildingCoordinateAndHeights.Select(x => new BuildingCoordinateAndHeight()
                {
                    BuildingCoordinateAndHeightLookUpId = x.BuildingCoordinateAndHeightLookUpId,
                    Easting = x.Easting,
                    Northing = x.Northing,
                    GroundHeight = x.GroundHeight,
                    StructureHeight = x.StructureHeight
                }).ToList(),
                AdditionalItemsForBuildingSubmittals = model.AdditionalItemsForBuildingSubmittals.Where(x => x.IsSelected).Select(x => new AdditionalItemsForBuildingSubmittal()
                {
                    AdditionalItemsForBuildingSubmittalLookUpId = x.AdditionalItemsForBuildingSubmittalLookUpId,
                    OtherDescription = x.IsOther ? x.OtherDescription : string.Empty
                }).ToList(),
                BuildingHeightAviationObstacles = model.AviationObstacles.Where(x => x.IsSelected).Select(x => new BuildingHeightAviationObstacle()
                {
                    AviationObstacleLookUpId = x.Id
                }).ToList(),
                OHLStructureCoordinateAndHeights = model.OHLStructureCoordinateAndHeights.Select(x => new OHLStructureCoordinateAndHeight()
                {
                    StructureReferenceNumber = x.StructureReferenceNumber,
                    Easting = x.Easting,
                    Northing = x.Northing,
                    GroundHeight = x.GroundHeight,
                    StructureHeight = x.StructureHeight
                }).ToList(),
                AdditionalItemsForOHLSubmittals = model.AdditionalItemsForOHLSubmittals.Where(x => x.IsSelected).Select(x => new AdditionalItemsForOHLSubmittal()
                {
                    AdditionalItemsForOHLSubmittalLookUpId = x.AdditionalItemsForOHLSubmittalLookUpId,
                    OtherDescription = x.IsOther ? x.OtherDescription : string.Empty
                }).ToList(),
                BuildingHeightApplicableFees = model.BuildingHeightApplicableFees.Select(x => new BuildingHeightApplicableFee()
                {
                    ApplicableFeesLookUpId = x.ApplicableFeesLookUpId,
                    IsSelected = x.IsSelected,
                    Quantity = x.Quantity,
                    TotalFee = (double)(x.Fee ?? 0) * x.Quantity
                }).ToList()
            };
            return contract;
        }

        #endregion "Private Methods"
    }
}