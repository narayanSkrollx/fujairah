using System.ComponentModel.DataAnnotations;

namespace EService.Web.Models
{
    public class DepartmentUserCreateViewModel
    {
        [Required(ErrorMessage = "User name is required.")]
        public string UserName { get; set; }
        public int? UserTypeId { get; set; }
        [Required(ErrorMessage = "First name is required.")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last name is required.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        public string Password { get; set; }
        [Compare("Password", ErrorMessage = "Confirm Password must match Password")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "Email is Required")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required(ErrorMessage = "Department must be selected.")]
        public int? DepartmentId { get; set; }
    }
}