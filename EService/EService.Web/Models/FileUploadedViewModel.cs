namespace EService.Web.Models
{
    public class FileUploadedViewModel
    {
        public FileUploadedViewModel()
        {
            ObjectState = ObjectState.Unchanged;
        }
        public ObjectState ObjectState { get; set; }
        public int Id { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] Content { get; set; }
    }
}