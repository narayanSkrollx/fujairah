﻿using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class PaymentResponseService : BusinessAccessBase<UserServiceRequestPaymentResponse>
    {
        public PaymentResponseService(ServiceContext context) : base(context)
        {
        }
    }
}