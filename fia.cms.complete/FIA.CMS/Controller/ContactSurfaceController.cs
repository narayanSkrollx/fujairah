﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FIA.CMS.Model;
using Umbraco.Web.Mvc;
using System.Net.Mail;
using Newtonsoft.Json;
using System.Net;
using System.Configuration;
using FIA.CMS.Helper;

namespace FIA.CMS.Controller
{
    public class ContactSurfaceController : BaseController
    {
        private void SendEmail(ContactModel model)
        {
            var server = ConfigurationManager.AppSettings["smtpServer"];
            var port = Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]);
            var username = ConfigurationManager.AppSettings["smtpEmail"];
            var password = ConfigurationManager.AppSettings["smtpPassword"];
            var to = ConfigurationManager.AppSettings["emailTo"];
            var message = new MailMessage(model.EmailAddress, to);
            message.Subject = $"Enquiry from {model.FirstName} {model.LastName} - {model.EmailAddress}";
            message.Body = GetMessageBody(model);
            message.IsBodyHtml = true;
            var client = new SmtpClient(server, port);
            client.Credentials = new NetworkCredential(username, password);
            //client.EnableSsl = true;
            client.Send(message);
        }

        public ActionResult RenderForm()
        {
            return PartialView($"{HomePartialViewLocation}_Contact.cshtml");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitForm(ContactModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var to = EmailSenderAddress.GetSenderEmail("ContactUs");
                    base.SendEmail(GetMessageBody(model), to, $"Enquiry from {model.FirstName} {model.LastName} - {model.EmailAddress}");
                    var genericEmailModel = new GenericEmailModel()
                    {
                        Context = model.ContactType
                    };
                    base.SendEmail(AutoReplyToUser(genericEmailModel), model.EmailAddress, "NO REPLY-FIA");
                    TempData["SuccessMsg"] = "Thank you for submitting us your query. We will get back to you shortly.";
                    return RedirectToCurrentUmbracoPage();
                }

                return CurrentUmbracoPage();
            }
            catch (Exception ex)
            {
                Logger.Error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "ContactSurfaceCOntroller.SubmitForm", ex);
                TempData["SuccessMsg"] = "Error while submitting your concern. Please try again later.";
                return RedirectToCurrentUmbracoPage();

            }
        }

        private string AutoReplyToUser(GenericEmailModel model)
        {
            return RenderPartialView("~/Views/Partials/EmailTemplates/_GenericEmailResponse.cshtml", model);
        }

        private string GetMessageBody(ContactModel model)
        {
            var messagebody = "<table>";
            if (!string.IsNullOrWhiteSpace(model.ContactType))
            {
                messagebody += string.Format("<tr><td>Suggestion</td><td>{0}</td></tr>", model.ContactType);
            }
            if (!string.IsNullOrWhiteSpace(model.FirstName))
            {
                messagebody += string.Format("<tr><td>First name</td><td>{0}</td></tr>", model.FirstName);
            }
            if (!string.IsNullOrWhiteSpace(model.LastName))
            {
                messagebody += string.Format("<tr><td>Last name</td><td>{0}</td></tr>", model.LastName);
            }
            if (!string.IsNullOrWhiteSpace(model.EmailAddress))
            {
                messagebody += string.Format("<tr><td>Email</td><td>{0}</td></tr>", model.EmailAddress);
            }
            if (!string.IsNullOrWhiteSpace(model.Nationality))
            {
                messagebody += string.Format("<tr><td>Nationality</td><td>{0}</td></tr>", model.Nationality);
            }
            if (!string.IsNullOrWhiteSpace(model.Message))
            {
                messagebody += string.Format("<tr><td>Message</td><td>{0}</td></tr>", model.Message);
            }
            if (model.ServiceList.Count() > 0)
            {
                var serviceString = string.Join(", ", model.ServiceList.Distinct().ToArray());
                messagebody += string.Format("<tr><td>Services </td><td>{0}</td></tr>", serviceString);
                if (model.ServiceList.Any(x => x == "Others") && !string.IsNullOrWhiteSpace(model.ServiceOtherText))
                {
                    messagebody += string.Format("<tr><td>Other services </td><td>{0}</td></tr>", model.ServiceOtherText);
                }
            }
            messagebody += "</table>";
            return messagebody;
        }
    }
}