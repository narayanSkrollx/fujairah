using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class AdditionalItemsForOHLSubmittalConfiguration : EntityTypeConfiguration<AdditionalItemsForOHLSubmittal>
    {
        public AdditionalItemsForOHLSubmittalConfiguration()
        {
            ToTable("AdditionalItemsForOHLSubmittal");
            HasKey(x => x.AdditionalItemsForOHLSubmittalId);

            Property(x => x.AdditionalItemsForOHLSubmittalId).IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.UserServiceRequestMasterId).IsRequired();
            Property(x => x.AdditionalItemsForOHLSubmittalLookUpId).IsRequired();
            Property(x => x.OtherDescription).IsOptional().HasMaxLength(225);

            HasRequired(x => x.BuildingHeightRequest).WithMany(x => x.AdditionalItemsForOHLSubmittals)
                .HasForeignKey(x => x.UserServiceRequestMasterId).WillCascadeOnDelete(false);
            HasRequired(x => x.AdditionalItemsForOHLSubmittalLookUp).WithMany(x => x.AdditionalItemsForOHLSubmittals)
                .HasForeignKey(x => x.AdditionalItemsForOHLSubmittalLookUpId).WillCascadeOnDelete(false);
        }
    }
}