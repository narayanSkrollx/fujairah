﻿$(function () {
    fia.CreateNamespace('applicableFeesLookup');
    (function (context) {

        context.Title = 'Applicable Fees lookup';

        context.ViewModel = {}
        context.Mapping = {
            create: function (options) {
                var vm = ko.mapping.fromJS(options.data);

                vm.EditMode = ko.observable(false);

                vm.EditRecords = ko.mapping.fromJS(ko.toJS(vm.Records));

                vm.AddNewClick = function () {
                    var addNew = ko.toJS(vm.AddNewModel);
                    vm.EditRecords.push(ko.mapping.fromJS(addNew));
                }

                vm.EditClick = function () {
                    vm.EditMode(true);
                    console.log('edit click');
                }

                vm.CancelClick = function () {
                    vm.EditMode(false);
                }

                vm.SaveClick = function () {
                    var model = ko.toJS(vm.EditRecords);
                    context.Save(model);
                }

                return vm;
            }
        }
        context.Initialize = function () {
            ajaxRequest('/Admin/ApplicableFees/Initialize', 'GET', {}, function (response) {
                if (response.IsSuccess) {
                    if (!ko.dataFor($('#mainContent')[0])) {
                        context.ViewModel = ko.mapping.fromJS(response.Data, context.Mapping);
                        ko.applyBindings(context.ViewModel, $('#mainContent')[0]);
                    } else {
                        ko.mapping.fromJS(response.Data, {}, context.ViewModel);
                        context.ViewModel.EditMode(false);
                    }
                } else {
                    showMessage(context.Title, response.Message, 'error');
                }
            });
        }

        context.Save = function (model) {
            ajaxRequest('/Admin/ApplicableFees/Index', 'POST', { data: { model: model } }, function (response) {
                if (response.IsSuccess) {
                    context.Initialize();
                    showMessage(context.Title, 'Applicable Fees record saved successfully.', 'success');
                } else {
                    showMessage(context.Title, response.Message, 'error');
                }
            });
        }

    })(fia.applicableFeesLookup);
})