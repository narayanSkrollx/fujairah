using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using EService.Service.Model;

namespace EService.Service.Logic
{
    public class ServiceInfoService : BusinessAccessBase<Data.Entity.Entity.Service>
    {
        public ServiceInfoService(ServiceContext context) : base(context)
        {
        }

        public IEnumerable<ServiceInfoServiceModel> SearchServiceRecords()
        {
            return (from service in FindAllAsNoTracking(o => o.IsActive)
                    select new
                    {
                        service.ServiceId,
                        service.ServiceCode,
                        service.ServiceName,
                        service.Description,
                        service.DefaultRate,
                        service.IsDefault,
                        service.VATPercentage
                    })
                     .AsEnumerable()
                     .Select(o => new ServiceInfoServiceModel()
                     {
                         IsActive = true,
                         ServiceName = o.ServiceName,
                         Description = o.Description,
                         DefaultRate = o.DefaultRate,
                         ServiceCode = o.ServiceCode,
                         ServiceId = o.ServiceId,
                         IsDefault = o.IsDefault,
                         VATPercentage = o.VATPercentage
                     });
        }

        public Tuple<bool, String> Save(Data.Entity.Entity.Service model)
        {
            var result = false;
            var message = "";
            if (model.ServiceId > 0)
            {
                var existingService = Find(o => o.IsActive && o.ServiceId == model.ServiceId);
                if (existingService == null)
                {
                    result = false;
                    message = "Invalid service information provided to update.";
                }
                else
                {
                    existingService.ServiceName = model.ServiceName;
                    existingService.ServiceCode = model.ServiceCode;
                    existingService.DefaultRate = model.DefaultRate;
                    existingService.VATPercentage = model.VATPercentage;
                    existingService.Description = model.Description;
                    existingService.ModifiedByUserId = model.ModifiedByUserId;
                    existingService.ModifiedDate = model.ModifiedDate;

                    Context.Commit();
                    result = true;
                    message = "Service Information updated successfully.";
                }
            }
            else
            {
                var maxId = FindAllAsNoTracking(o => true).Max(o => o.ServiceId);
                var saveModel = new Data.Entity.Entity.Service()
                {
                    ServiceId = maxId + 1,
                    ServiceName = model.ServiceName,
                    ServiceCode = model.ServiceCode,
                    DefaultRate = model.DefaultRate,
                    Description = model.Description,
                    IsActive = true,
                    IsDeleted = false,
                    CreatedDate = model.CreatedDate,
                    CreatedByUserId = model.CreatedByUserId,
                    ModifiedDate = model.ModifiedDate,
                    ModifiedByUserId = model.ModifiedByUserId
                };
                Add(saveModel);
                Context.Commit();
                result = true;
                message = "Service information added succesfully.";
            }
            return new Tuple<bool, string>(result, message);
        }
    }
}