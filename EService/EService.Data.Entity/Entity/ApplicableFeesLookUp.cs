using System.Collections.Generic;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class ApplicableFeesLookUp : ILookupItem
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public string Notes { get; set; }
        public string Fees { get; set; }
        public decimal? Fee { get; set; }

        [JsonIgnore]
        public virtual ICollection<BuildingHeightApplicableFee> BuildingHeightApplicableFees { get; set; }
    }
}