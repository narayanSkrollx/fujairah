using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class BuildingCoordinateAndHeightConfiguration : EntityTypeConfiguration<BuildingCoordinateAndHeight>
    {
        public BuildingCoordinateAndHeightConfiguration()
        {
            ToTable("BuildingCoordinateAndHeight");
            HasKey(x => new {x.UserServiceRequestMasterId, x.BuildingCoordinateAndHeightLookUpId});

            Property(x => x.UserServiceRequestMasterId).IsRequired();
            Property(x => x.BuildingCoordinateAndHeightLookUpId).IsRequired();
            Property(x => x.Easting).IsOptional().HasMaxLength(225);
            ;
            Property(x => x.Northing).IsOptional().HasMaxLength(225);
            ;
            Property(x => x.GroundHeight).IsOptional();
            Property(x => x.StructureHeight).IsOptional();

            HasRequired(x => x.BuildingHeightRequest).WithMany(x => x.BuildingCoordinateAndHeights)
                .HasForeignKey(x => x.UserServiceRequestMasterId).WillCascadeOnDelete(false);
            HasRequired(x => x.BuildingCoordinateAndHeightLookUp).WithMany(x => x.BuildingCoordinateAndHeights)
                .HasForeignKey(x => x.BuildingCoordinateAndHeightLookUpId).WillCascadeOnDelete(false);
        }
    }
}