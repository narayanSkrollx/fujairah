using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Service.Logic.Enum;
using EService.Service.Model;
using EService.Web.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using EService.Web.Controllers.Base;

namespace EService.Web.Controllers
{
    public class LandingPermissionRequestController : ServiceRequestBaseController
    {
        private ServiceContext context;
        private LandingPermissionRequestService landingPermissionRequestService;
        private LookupService lookupService;
        private ServiceInfoService serviceInfoService;

        public LandingPermissionRequestController()
        {
            context = new ServiceContext();
            serviceInfoService = new ServiceInfoService(context);
            landingPermissionRequestService = new LandingPermissionRequestService(context);
            lookupService = new LookupService();
        }

        public override ServiceEnum Service => ServiceEnum.Other;

        public ActionResult Create()
        {
            return View();
        }

        public JsonResult GetNewLandingPermission()
        {
            var result = new JsonResponseObject();
            try
            {
                var model = new LandingPermissionRequestCreateViewModel();
                result.IsSuccess = true;
                result.Data = model;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "Error while obtaining Land Permission Request Info.";
            }
            return Json(result);
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Initalize()
        {
            var result = new ResponseObject();
            try
            {
                var model = new LandingPermissionRequestViewModel()
                {
                    SearchModel = new LandingPermissionRequestSearchViewModel()
                    {
                        AirportLandingList = lookupService.GetList<AirportLanding>().ToList(),
                    }
                };
                result.Data = model;
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "Error while obtaining model.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}