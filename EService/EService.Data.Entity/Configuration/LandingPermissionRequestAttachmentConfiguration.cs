using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class LandingPermissionRequestAttachmentConfiguration :
        EntityTypeConfiguration<LandingPermissionRequestAttachment>
    {
        public LandingPermissionRequestAttachmentConfiguration()
        {
            ToTable("LandingPermissionRequestAttachment");

            HasKey(o => o.LandingPermissionRequestAttachmentId);

            Property(o => o.LandingPermissionRequestAttachmentId).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.LandingPermissionRequestId).IsRequired();
            Property(o => o.UserAttachmentId).IsRequired();

            HasRequired(o => o.LandingPermissionRequest)
                .WithMany(o => o.LandingPermissionRequestAttachments)
                .HasForeignKey(o => o.LandingPermissionRequestId).WillCascadeOnDelete(false);
            HasRequired(o => o.UserAttachment)
                .WithMany(o => o.LandingPermissionRequestAttachments)
                .HasForeignKey(o => o.UserAttachmentId).WillCascadeOnDelete(false);
        }
    }
}