using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class AdditionalItemsForBuildingSubmittalLookUpConfiguration : EntityTypeConfiguration<
        AdditionalItemsForBuildingSubmittalLookUp>
    {
        public AdditionalItemsForBuildingSubmittalLookUpConfiguration()
        {
            ToTable("AdditionalItemsForBuildingSubmittalLookUp");
            HasKey(x => x.Id);

            Property(x => x.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Description).IsRequired().HasMaxLength(225);
            Property(x => x.Active).IsRequired();
        }
    }
}