using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class InfringementTypeConfiguration : EntityTypeConfiguration<InfringementType>
    {
        public InfringementTypeConfiguration()
        {
            ToTable("InfringementType");

            HasKey(o => o.Id);
            Property(o => o.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(o => o.Description).IsRequired().HasMaxLength(255);
            Property(o => o.IsOther).IsRequired();

        }
    }
}