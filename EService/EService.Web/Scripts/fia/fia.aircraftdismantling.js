﻿$(function () {
    fia.CreateNamespace("aircraftdismantling");

    (function (context) {
        context.Title = 'Aircarft Dismantle';
        context.createForm = '#frmAircraftDismantlingRequest';
        context.ServiceId = fia.ServiceEnum.AircraftDismantlingRequest;

        context.InitializeCreateUrl = ApplicationRootUrl('InitializeCreate', 'AircraftDismantlingRequest');
        context.CreateUrl = ApplicationRootUrl('Create', 'AircraftDismantlingRequest');
        context.CreateSuccessUrl = ApplicationRootUrl('Index', 'Dashboard');
        context.InitializeIndexUrl = ApplicationRootUrl('Initialize', 'AircraftDismantlingRequest');
        context.DetailsUrl = ApplicationRootUrl('Detail', 'AircraftDismantlingRequest');
        context.EditUrl = ApplicationRootUrl('Edit', 'AircraftDismantlingRequest');
        context.InitializeDetailUrl = ApplicationRootUrl('InitializeDetail', 'AircraftDismantlingRequest');

        context.SaveRequest = function (isDraft) {
            if (!$(context.createForm).valid()) {
                return false;
            }
            var model = { model: ko.mapping.toJS(context.ViewModel.AddNewModel), isDraft: isDraft };
            var lightModel = JSON.stringify(model);
            var savecontrol = isDraft ? 'draft' : 'save';
            var ajaxOption = {
                dataType: "JSON",
                contentType: "application/json; charset=UTF-8",
                data: lightModel,
                enableLadda: true,
                targetLaddaElement: `[data-role=${savecontrol}]`,
            };
            ajaxRequest(context.CreateUrl, 'POST', ajaxOption, function (resp) {
                if (resp.IsSuccess) {
                    showMessage(context.Title, resp.Message, 'success', function () {
                        //
                        window.location = '/ServiceRequest/Index?serviceId=' + context.ServiceId;
                    });
                } else {
                    showMessage(context.Title, resp.Message, 'error');
                }
            });
        }

        context.ViewModel = {
            SaveRequest: function () {
                context.SaveRequest(false)
            },
            SaveDraftRequest: function () {
                context.SaveRequest(true)
            },
            ViewDetails: function (item) {
                window.location = context.DetailsUrl + '?id=' + item.UserServiceRequestMasterId();
            },
            Edit: function (item) {
                window.location = context.DetailsUrl + '?id=' + item.UserServiceRequestMasterId();
            },
            SetUserInfo: function () {
                context.SetUserInfo();
            },
            CreateRenderComplete: function () {
                $(context.createForm).validate({
                    rules: {
                        DateOfApplication: { required: true,  },
                        NameOfApplicant: { required: true, maxlength: 1000 },
                        Address: { required: true, maxlength: 255 },
                        PhoneNumber: { required: true, maxlength: 20, regex: phoneNoRegex },
                        Email: { required: true, email: true, maxlength: 225 },
                        ApplicantCompanyName: { required: true, maxlength: 225 },

                        HasGCAAApproval: { required: true },
                        AircraftTypeAndModelToDismantle: { maxlength: 225 },
                        AircraftMSNAndRegistrationNumber: { maxlength: 225 },
                        PreviousOwnerOfAircraft: { maxlength: 225 },
                        CurrentOwnerOfAircraft: { maxlength: 225 },
                        OperatorOrAgentWhoBroughtTheAircraft: { maxlength: 225 },
                        GCAAApprovalRefNumber: { maxlength: 225 },
                        CertificateOfDeRegistrationRefNumber: { maxlength: 225 },
                        TradeLicenseRefNumber: { maxlength: 225 },
                        FANRLicenseRefNumber: { maxlength: 225 },
                        FujairahEPDLicenseRefNumber: { maxlength: 225 },
                        MethodOfDismantlingAndDestruction: { maxlength: 225 },

                    },
                    messages: {
                        DateOfApplication: { required: 'Date of Aplication is required', date: 'Date of Application must be a valid date.', },
                        NameOfApplicant: { required: 'Name of Applicant is required', maxlength: 'Applicant Name length cannot be greater than 1000.' },
                        Address: { required: 'Applicant Address is required', maxlength: 'Applicant Address length cannot be greater than 255.' },
                        PhoneNumber: { required: 'Applicant Phone Number is required', maxlength: 'Phone number length cannot be greater than 20.' },
                        Email: { required: 'Applicant Email is required.', email: 'Applicant Email must be a valid Email Address,', maxlength: 'Email Max Length cannot be greater than 225' },
                        ApplicantCompanyName: { required: 'Applicant Company Name is required.', maxlength: 'Applicant Compnay Name length cannot be greater than 225.' },

                        HasGCAAApproval: { required: 'Required.' },
                        AircraftTypeAndModelToDismantle: { maxlength: 'Aircraft Type and Model to Dismantle length cannot be greater than 225' },
                        AircraftMSNAndRegistrationNumber: { maxlength: 'Aircraft MSN &Registration Number length cannot be greater than 225' },
                        PreviousOwnerOfAircraft: { maxlength: 'Previous Owner of the Aircraft length cannot be greater than 225' },
                        CurrentOwnerOfAircraft: { maxlength: 'Current Owner of the Aircraft length cannot be greater than 225' },
                        OperatorOrAgentWhoBroughtTheAircraft: { maxlength: 'Operator/Agent who brought the Aircraft in FIA length cannot be greater than 225' },
                        GCAAApprovalRefNumber: { maxlength: 'GCAA approval Ref No length cannot be greater than 225' },
                        CertificateOfDeRegistrationRefNumber: { maxlength: 'Certificate of De-Registration Ref. No. length cannot be greater than 225' },
                        TradeLicenseRefNumber: { maxlength: 'Trade License Ref. No. of Dismantling Company length cannot be greater than 225' },
                        FANRLicenseRefNumber: { maxlength: 'FANR License Ref. No. of Dismantling Company length cannot be greater than 225' },
                        FujairahEPDLicenseRefNumber: { maxlength: 'Fujairah EPD License Ref. No. of Dismantling Company length cannot be greater than 225' },
                        MethodOfDismantlingAndDestruction: { maxlength: 'Method of Dismantling and/or Destruction length cannot be greater than 225' },
                    }
                });
            }
        };

        context.SetUserInfo = function () {
            var addNewModel = context.ViewModel.AddNewModel;
            var userInfo = context.ViewModel.AddNewModel.UserInfo;
            if (addNewModel != null && userInfo != null) {
                addNewModel.NameOfApplicant(userInfo.FirstName() + ' ' + userInfo.LastName());
                addNewModel.Address(userInfo.Address());
                addNewModel.PhoneNumber(userInfo.Phone());
                addNewModel.Email(userInfo.Email());
                addNewModel.ApplicantCompanyName(userInfo.CompanyName());
            }
        }

        context.InitializeCreate = function () {
            var masterId = parseInt($("#CurrentUserServiceRequestMasterId.forKnockout").val());
            context.ViewModel.IsCreate = Number.isInteger(masterId);
            var id = context.ViewModel.IsCreate ? parseInt(masterId) : null;

            ajaxRequest(context.InitializeCreateUrl, 'GET', { data: { id: id } }, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#createContent')[0])) {
                        context.ViewModel.AddNewModel = ko.mapping.fromJS(resp.Data);
                        context.ViewModel.RemoveUploadedFile = function (item) {
                            item.ObjectState(2);
                        }
                        ko.applyBindings(context.ViewModel, $('#createContent')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel.AddNewModel);
                    }
                    //validation

                    //init file uploader
                    fia.fileinputhelper.InitializeFileUpload('#kv-explorer', fia.ServiceEnum.AircraftDismantlingRequest);
                    fia.fileinputhelper.UploadCompleteCallback = function (response) {
                        context.ViewModel.AddNewModel.UploadedFiles.push(response.Data);
                    }
                    fia.fileinputhelper.DeleteCompleteCallback = function (removedData) {
                        context.ViewModel.AddNewModel.UploadedFiles.remove(function (data) { return data == removedData });
                    }
                } else {
                    console.log(resp.Message);
                }
            });
        };

        context.InitializeIndex = function () {
            ajaxRequest(context.InitializeIndexUrl, 'GET', {}, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#mainContent')[0])) {
                        context.ViewModel.Records = ko.mapping.fromJS(resp.Data);
                        ko.applyBindings(context.ViewModel, $('#mainContent')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel.Records);
                    }
                } else {
                    showMessage('Service Request Detail', resp.Message, 'error');
                }
            });
        };

        context.InitializeDetail = function () {
            context.CurrentUserServiceRequestMasterId = $("#CurrentUserServiceRequestMasterId.forKnockout").val();

            ajaxRequest(context.InitializeDetailUrl + "?id=" + context.CurrentUserServiceRequestMasterId, 'GET', {}, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#aircraftDismantleDetailsDiv')[0])) {
                        context.ViewModel.Details = ko.mapping.fromJS(resp.Data);
                        ko.applyBindings(context.ViewModel, $('#aircraftDismantleDetailsDiv')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel.Details);
                    }
                    fia.fileinputhelper.InitializeUploadedList('#uploadedFiles', context.CurrentUserServiceRequestMasterId);
                } else {
                    showMessage('Service Request Detail', resp.Message, 'error');
                }
            });
        };

    })(fia.aircraftdismantling);
});