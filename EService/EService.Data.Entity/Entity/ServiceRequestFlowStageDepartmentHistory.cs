using System;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class ServiceRequestFlowStageDepartmentHistory
    {
        public int ServiceRequestFlowStageDepartmentHistoryId { get; set; }

        public int ServiceRequestFlowStageDepartmentId { get; set; }
        public int ServiceRequestFlowStageId { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int? DepartmentUserId { get; set; }
        public string DepartmentUserName { get; set; }
        public int Status { get; set; }
        public DateTime? StatusChangedDateTime { get; set; }
        public bool? IsApproved { get; set; }
        public string Remarks { get; set; }

        [JsonIgnore]
        public virtual ServiceRequestFlowStageDepartment ServiceRequestFlowStageDepartment { get; set; }
    }
}