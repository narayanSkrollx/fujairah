using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class CraneTypeConfiguration : EntityTypeConfiguration<CraneType>
    {
        public CraneTypeConfiguration()
        {
            ToTable("CraneType");
            HasKey(o => o.CraneTypeId);
            Property(o => o.CraneTypeId).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(o => o.CraneTypeName).IsRequired().HasMaxLength(255);
            Property(o => o.IsOther).IsRequired();

        }
    }
}