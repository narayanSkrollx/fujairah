using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FIA.CMS.Model;
using Umbraco.Core.Models;
using Umbraco.Web;
using Archetype;
using Archetype.Models;
using FIA.CMS.Helper;

namespace FIA.CMS.Controller
{

    public class DefaultContentController : BaseController
    {
        public ActionResult RenderArcheTypeContent()
        {
            var model = new List<DefaultContentArcheType>();
            //IPublishedContent homePage = CurrentPage
            //        .AncestorOrSelf(1)
            //        .DescendantsOrSelf()
            //        .FirstOrDefault(o => o.DocumentTypeAlias == "home");
            ArchetypeModel contentItems = CurrentPage.GetPropertyValue<ArchetypeModel>("contentGrid");

            foreach (ArchetypeFieldsetModel fieldSet in contentItems.Fieldsets)
            {
                var image = fieldSet.GetValue<IPublishedContent>("image");

                bool revert = false;
                bool convertSuccess = bool.TryParse(fieldSet.GetValue("revertImageContent"), out revert);
                model.Add(new DefaultContentArcheType()
                {
                    ImageUrl = image.Url,
                    MainContent = fieldSet.GetValue("mainContent"),
                    Title = fieldSet.GetValue("title"),
                    RevertPlacement = convertSuccess ? revert : (bool?)null,
                });
            }
            return PartialView("~/Views/Partials/Shared/_DefaultContentTemplate.cshtml", model);
        }

        public ActionResult RenderArcheTypeContentByArcheType(ArcheTypeEnum archeTypeEnum, int pageId)
        {
            var currentPage = Umbraco.Content(pageId);

            if (archeTypeEnum == ArcheTypeEnum.Normal)
            {
                var model = new List<DefaultContentArcheType>();
                ArchetypeModel contentItems = currentPage.GetPropertyValue<ArchetypeModel>("contentGrid");

                foreach (ArchetypeFieldsetModel fieldSet in contentItems.Fieldsets)
                {
                    if (fieldSet != null)
                    {
                        var image = fieldSet.GetValue<IPublishedContent>("image");

                        bool revert = false;
                        bool convertSuccess = bool.TryParse(fieldSet.GetValue("revertImageContent"), out revert);
                        model.Add(new DefaultContentArcheType()
                        {
                            ImageUrl = image?.Url,
                            MainContent = fieldSet.GetValue("mainContent"),
                            Title = fieldSet.GetValue("title"),
                            RevertPlacement = convertSuccess ? revert : (bool?)null,
                        });
                    }
                }
                return PartialView("~/Views/Partials/Shared/_DefaultContentTemplate.cshtml", model);
            }
            else
            {
                var model = new List<DefaultContentArcheType>();

                ArchetypeModel contentItems = currentPage.GetPropertyValue<ArchetypeModel>("contentGrid");

                foreach (ArchetypeFieldsetModel fieldSet in contentItems.Fieldsets)
                {
                    var image = fieldSet.GetValue<IPublishedContent>("image");

                    bool revert = false;
                    bool convertSuccess = bool.TryParse(fieldSet.GetValue("revertImageContent"), out revert);
                    model.Add(new DefaultContentArcheType()
                    {
                        ImageUrl = image.Url,
                        MainContent = fieldSet.GetValue("mainContent"),
                        Title = fieldSet.GetValue("title"),
                        RevertPlacement = convertSuccess ? revert : (bool?)null,
                    });
                }
                return PartialView("~/Views/Partials/Shared/_DefaultContentTemplateTwoInARow.cshtml", model);
            }
        }

    }
}