using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class AdditionalItemsForBuildingSubmittal
    {
        public int AdditionalItemsForBuildingSubmittalId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public int AdditionalItemsForBuildingSubmittalLookUpId { get; set; }
        public string OtherDescription { get; set; }

        // navigation properties
        [JsonIgnore]
        public virtual BuildingHeightRequest BuildingHeightRequest { get; set; }
        [JsonIgnore]
        public virtual AdditionalItemsForBuildingSubmittalLookUp AdditionalItemsForBuildingSubmittalLookUp { get; set; }
    }
}