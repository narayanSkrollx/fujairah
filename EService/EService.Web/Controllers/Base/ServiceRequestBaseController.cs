using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Mvc;
using EService.Service.Logic;
using EService.Service.Logic.Enum;
using EService.Service.Model;
using EService.Web.Helper;
using EService.Web.Helper.EmailSender;
using EService.Web.Models;
using EService.Web.Models.ServiceNotification;
using Nelibur.ObjectMapper;
using NLog;

namespace EService.Web.Controllers.Base
{
    public abstract class ServiceRequestBaseController : ExternalUserBaseController
    {
        protected ServiceApprovalStageService ServiceApprovalStageService;
        private UserAttachmentService userAttachmentService;
        private UserServiceRequestMasterService userServiceRequestMasterService;
        private SMTPEmailSettingService emailSettingService;
        private UserService userService;
        private EmailService emailService;

        protected ServiceRequestBaseController()
        {
            ServiceApprovalStageService = new ServiceApprovalStageService(serviceContext);
            userAttachmentService = new UserAttachmentService(serviceContext);
            userServiceRequestMasterService = new UserServiceRequestMasterService(serviceContext);
            emailSettingService = new SMTPEmailSettingService(serviceContext);
            userService = new UserService(serviceContext);
            emailService = new EmailService(serviceContext);
        }

        public abstract ServiceEnum Service { get; }
        public int ServiceId => (int)Service;

        //protected void TriggerEmail(FIAEmailType emailType, int userServiceRequestMasterId)
        //{
        //    //var userServiceRequest = userServiceRequestMasterService.GetServiceRequest(userServiceRequestMasterId);
        //    var smtpSetting = emailSettingService.Find(o => true);
        //    //var reciepients =
        //    //    emailService.GetApproveRejectEmailRecepientsByUserServiceRequestId(userServiceRequestMasterId);
        //    var userInfo = emailService.GetUserInfo(userServiceRequestMasterId);
        //    var userServiceRequestmaster =
        //        userServiceRequestMasterService.Find(o => o.UserServiceRequestMasterId == userServiceRequestMasterId);

        //    var newEmailModel = new NewServiceRequestNotificationViewModel()
        //    {
        //        ServiceId = userServiceRequestmaster.ServiceId,
        //        ServiceName = userServiceRequestmaster.Service.ServiceName,
        //        RequestedByUserName = userInfo.UserName,
        //        RequestRegistrationNo = userServiceRequestmaster.RegistrationNo,
        //        RequestedByUserEmail = userInfo.Email,
        //        RequestedByUserId = userServiceRequestmaster.UserId,
        //        RequestedDateTime = userServiceRequestmaster.RequestDateTime
        //    };

        //    var emailContent = RenderPartialView("_NewServiceRequestEmailTemplate", newEmailModel);
        //    EmailHelper.SendEmail(smtpSetting.From, new string[] { userInfo.Email }, new string[] { }, new string[] { }, "New Service Request", emailContent, smtpSetting);
        //}

        protected List<FileUploadedViewModel> GetUploadedFiles(int id)
        {
            return userAttachmentService.GetUploadedFileInfoServiceModels(id).Select(o => new FileUploadedViewModel()
            {
                ContentType = o.ContentType,
                FileName = o.FileName,
                Id = o.UserAttachmentId,
                ObjectState = ObjectState.Unchanged
            }).ToList();
        }

        public JsonResult UploadedFiles(int userServiceRequestmasterId)
        {
            var result = new JsonResponseObject();

            try
            {
                result.Data = userAttachmentService.GetUploadedFileInfoServiceModels(userServiceRequestmasterId);

                result.IsSuccess = true;
            }
            catch (Exception)
            {
                result.IsSuccess = false;
                result.Message = "Error while ";
            }
            return Json(result);
        }
    }
}