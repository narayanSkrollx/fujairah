using System.Collections.Generic;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class UserType : AuditableEntity
    {
        public int UserTypeId { get; set; }
        public string UserTypeName { get; set; }
        
        [JsonIgnore]
        public virtual ICollection<User> Users { get; set; }

    }
}