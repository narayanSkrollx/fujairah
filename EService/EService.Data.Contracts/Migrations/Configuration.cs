using System.Collections.Generic;
using EService.Core.Security;
using EService.Data.Entity.Entity;

namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using EService.Core;
    internal sealed class Configuration : DbMigrationsConfiguration<EServiceDb>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EServiceDb context)
        {
            //SetAircraftTypes(context);
            //SeedSMTP(context);
            //SetUserTypes(context);
            //SetCompanyCategory(context);
            //SetSecurityQuestion(context);
            //SetServices(context);
            //SetDepartments(context);
            //SetDefaultSystemUser(context);
            //SetAirportOfLanding(context);
            //SetCraneTypes(context);
            //SetServiceFlowState(context);
            //SetInfringementTypes(context);
            //SetCurrency(context);
            //SetStaffStrength(context);
            //SetProjectTypes(context);
            //SetApplicantOwnerRelations(context);

            //SetWeatherTypes(context);
            //SetWeatherElements(context);

            //SetBuildingCoordinateAndHeightLookUp(context);
            //SetAviationObstacleLookUp(context);
            //SetAdditionalItemsForBuildingSubmittalLookUp(context);
            //SetAdditionalItemsForOHLSubmittalLookUp(context);
            //SetApplicableFeesLookUps(context);

            //SetServiceFlowStates(context);

            //SetPaymentKeys(context);
        }

        private void SetAircraftTypes(EServiceDb context)
        {
            context.AircraftTypes.AddOrUpdate(o => o.Id, new AircraftType
            {
                Id = 1,
                Description = "Turn around",
                Active = true,
                IsOther = false
            }, new AircraftType {
                Id = 2,
                Description = "Ferry",
                Active = true,
                IsOther = false
            }, new AircraftType
            {
                Id = 3,
                Description = "Night Shop",
                Active = true,
                IsOther = false
            }, new AircraftType {
                Id = 4,
                Description = "Other",
                Active = true,
                IsOther = true
            });
            context.SaveChanges();
        }
        private void SetPaymentKeys(EServiceDb eServiceDb)
        {
            eServiceDb.PaymentKeys.AddOrUpdate(o => o.Id, new PaymentKey { EncryptionKey = "3CqhgrUt+sdTvI7kczKyoHPvcEA82P/KsjFC8+13jQU=", MerchantId = "201802251000001", Url = "https://uat-NeO.network.ae/direcpay/secure/PaymentTxnServlet" });
            eServiceDb.SaveChanges();
        }

        private void SetServiceFlowStates(EServiceDb context)
        {
            context.ServiceApprovalStages.AddOrUpdate(o => o.ServiceApprovalStageId,
                //Crane Permit
                new ServiceApprovalStage()
                {
                    ServiceId = 2,
                    Index = 1,
                    ServiceApprovalStageId = 1,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 1}
                    }
                },
                new ServiceApprovalStage()
                {
                    ServiceId = 2,
                    Index = 2,
                    ServiceApprovalStageId = 2,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 2}
                    }
                },
                 new ServiceApprovalStage()
                 {
                     ServiceId = 2,
                     Index = 3,
                     ServiceApprovalStageId = 3,
                     ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 3}
                    }
                 },
                //Aircraft Warning 
                new ServiceApprovalStage()
                {
                    ServiceId = 3,
                    Index = 1,
                    ServiceApprovalStageId = 4,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 1}
                    }
                }, new ServiceApprovalStage()
                {
                    ServiceId = 3,
                    Index = 2,
                    ServiceApprovalStageId = 5,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 5}
                    }
                }, new ServiceApprovalStage()
                {
                    ServiceId = 3,
                    Index = 3,
                    ServiceApprovalStageId = 6,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 6}
                    }
                },
                //Aircraft Dismantling
                new ServiceApprovalStage()
                {
                    ServiceId = 4,
                    Index = 1,
                    ServiceApprovalStageId = 7,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 5}
                    }
                },
                new ServiceApprovalStage()
                {
                    ServiceId = 4,
                    Index = 2,
                    ServiceApprovalStageId = 8,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 6}
                    }
                },
                new ServiceApprovalStage()
                {
                    ServiceId = 4,
                    Index = 3,
                    ServiceApprovalStageId = 9,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 7}
                    }
                },
                      new ServiceApprovalStage()
                      {
                          ServiceId = 4,
                          Index = 4,
                          ServiceApprovalStageId = 10,
                          ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 8}
                    }
                      },
                //Aircraft Maintenance
                new ServiceApprovalStage()
                {
                    ServiceId = 5,
                    Index = 1,
                    ServiceApprovalStageId = 11,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 5}
                    }
                },
                new ServiceApprovalStage()
                {
                    ServiceId = 5,
                    Index = 2,
                    ServiceApprovalStageId = 12,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 7}
                    }
                },
                new ServiceApprovalStage()
                {
                    ServiceId = 5,
                    Index = 3,
                    ServiceApprovalStageId = 13,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 5}
                    }
                },
                //Trade License
                new ServiceApprovalStage()
                {
                    ServiceId = 6,
                    Index = 1,
                    ServiceApprovalStageId = 14,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 5}
                    }
                },
                //Exit of Aircraft Spare Parts
                new ServiceApprovalStage()
                {
                    ServiceId = 7,
                    Index = 1,
                    ServiceApprovalStageId = 15,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 5}
                    }
                },
                new ServiceApprovalStage()
                {
                    ServiceId = 7,
                    Index = 2,
                    ServiceApprovalStageId = 16,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 6}
                    }
                },

                //Building Height
                new ServiceApprovalStage()
                {
                    ServiceId = 9,
                    Index = 1,
                    ServiceApprovalStageId = 17,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 1}
                    }
                },
                new ServiceApprovalStage()
                {
                    ServiceId = 9,
                    Index = 2,
                    ServiceApprovalStageId = 18,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 5}
                    }
                },
                new ServiceApprovalStage()
                {
                    ServiceId = 9,
                    Index = 3,
                    ServiceApprovalStageId = 19,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 4}
                    }
                },
                  new ServiceApprovalStage()
                  {
                      ServiceId = 9,
                      Index = 4,
                      ServiceApprovalStageId = 20,
                      ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 3}
                    }
                  },
                //Ground Handling
                new ServiceApprovalStage()
                {
                    ServiceId = 1,
                    Index = 1,
                    ServiceApprovalStageId = 21,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>()
                    {
                        new ServiceApprovalStageDepartment() {DepartmentId = 6}
                    }
                },
                //Meteorological Data Request
                new ServiceApprovalStage
                {
                    ServiceId = 8,
                    Index = 1,
                    ServiceApprovalStageId = 22,
                    ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>
                    {
                        new ServiceApprovalStageDepartment{ DepartmentId = 9}
                    }
                },
                 new ServiceApprovalStage
                 {
                     ServiceId = 8,
                     Index = 2,
                     ServiceApprovalStageId = 23,
                     ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>
                    {
                        new ServiceApprovalStageDepartment{ DepartmentId = 3}
                    }
                 },
                     new ServiceApprovalStage
                     {
                         ServiceId = 8,
                         Index = 3,
                         ServiceApprovalStageId = 24,
                         ServiceApprovalStageDepartments = new List<ServiceApprovalStageDepartment>
                    {
                        new ServiceApprovalStageDepartment{ DepartmentId = 5}
                    }
                     }
                );

            context.SaveChanges();
        }

        private void SeedSMTP(EServiceDb context)
        {
            context.SMTPEmailSettings.AddOrUpdate(o => o.Id, new SMTPEmailSetting()
            {
                From = "noreply@fiademo.com",
                Host = "mail1.fujairah-airport.com",
                Port = 25,
                UserName = "admin",
                Password = "fujfia6579",
                EnableSsl = false,
                Id = 1
            });

            context.SaveChanges();
        }

        private void SetUserTypes(EServiceDb context)
        {
            context.UserTypes.AddOrUpdate(o => o.UserTypeId,
                new UserType() { UserTypeId = 1, UserTypeName = "SystemAdmin" },
                new UserType() { UserTypeId = 2, UserTypeName = "DepartmentUser" },
                new UserType() { UserTypeId = 3, UserTypeName = "ExternalUser" },
                new UserType() { UserTypeId = 4, UserTypeName = "FinanceUser" }
                );
            context.SaveChanges();
        }

        private void SetDefaultSystemUser(EServiceDb context)
        {
            context.Users.AddOrUpdate(o => o.UserName,
                new User()
                {
                    UserId = 1,
                    FirstName = "System",
                    LastName = "Admin",
                    UserName = "systemadmin",
                    Email = "test@test.com",
                    Password = "Password".ToMd5(),
                    UserTypeId = 1
                },
                 new User()
                 {
                     UserId = 2,
                     FirstName = "Finance",
                     LastName = "Admin",
                     UserName = "finance",
                     Email = "finance@default.com",
                     Password = "F!nanc3".ToMd5(),
                     DepartmentId = 3,
                     UserTypeId = 4
                 });
            context.SaveChanges();
        }

        private void SetCompanyCategory(EServiceDb context)
        {
            context.CompanyCategories.AddOrUpdate(o => o.Id,
                new CompanyCategory() { Id = 1, Description = "Airline", Active = true },
                new CompanyCategory() { Id = 2, Description = "Individual", Active = true },
                new CompanyCategory() { Id = 3, Description = "Handling Agents", Active = true },
                new CompanyCategory() { Id = 4, Description = "Supplier", Active = true },
                new CompanyCategory() { Id = 5, Description = "Consultant", Active = true },
                new CompanyCategory() { Id = 6, Description = "Contractor", Active = true },
                new CompanyCategory() { Id = 7, Description = "Other", Active = true });
            context.SaveChanges();
        }

        private void SetSecurityQuestion(EServiceDb context)
        {
            context.SecurityQuestions.AddOrUpdate(o => o.Id,
                new SecurityQuestion() { Id = 1, Description = "What is the name of your best friend?", Active = true },
                new SecurityQuestion() { Id = 2, Description = "What is the name of your pet?", Active = true },
                new SecurityQuestion() { Id = 3, Description = "What is your nick name?", Active = true }
            );
            context.SaveChanges();
        }

        private void SetServices(EServiceDb context)
        {
            context.Services.AddOrUpdate(o => o.ServiceId,
                new Service()
                {
                    ServiceId = 1,
                    ServiceName = "Ground Handling Service Request",
                    IsActive = true,
                    ServiceCode = "GH",
                    DefaultRate = 100,
                    CreatedByUserId = 1,
                    CreatedDate = DateTime.Now.DubaiTime(),
                    ModifiedByUserId = 1,
                    ModifiedDate = DateTime.Now.DubaiTime(),
                    VATPercentage = 5,
                    IsDeleted = false
                },
                new Service()
                {
                    ServiceId = 2,
                    ServiceName = "Crane Permit Request",
                    IsActive = true,
                    IsDefault = true,
                    ServiceCode = "CO",
                    DefaultRate = (decimal)0.01,
                    CreatedByUserId = 1,
                    CreatedDate = DateTime.Now.DubaiTime(),
                    ModifiedByUserId = 1,
                    ModifiedDate = DateTime.Now.DubaiTime(),
                    VATPercentage = 5,
                    IsDeleted = false
                },
                new Service()
                {
                    ServiceId = 3,
                    ServiceName = "Aircraft Warning Light",
                    IsActive = true,
                    ServiceCode = "AW",
                    DefaultRate = 100,
                    CreatedByUserId = 1,
                    CreatedDate = DateTime.Now.DubaiTime(),
                    ModifiedByUserId = 1,
                    ModifiedDate = DateTime.Now.DubaiTime(),
                    VATPercentage = 5,
                    IsDeleted = false
                },
                new Service()
                {
                    ServiceId = 4,
                    ServiceName = "Aircraft Dismantling Request",
                    IsActive = true,
                    ServiceCode = "AD",
                    DefaultRate = 100,
                    CreatedByUserId = 1,
                    CreatedDate = DateTime.Now.DubaiTime(),
                    ModifiedByUserId = 1,
                    ModifiedDate = DateTime.Now.DubaiTime(),
                    VATPercentage = 5,
                    IsDeleted = false
                },
                new Service()
                {
                    ServiceId = 5,
                    ServiceName = "Aircraft Maintenance Request",
                    IsActive = true,
                    ServiceCode = "AM",
                    DefaultRate = 100,
                    CreatedByUserId = 1,
                    CreatedDate = DateTime.Now.DubaiTime(),
                    ModifiedByUserId = 1,
                    ModifiedDate = DateTime.Now.DubaiTime(),
                    VATPercentage = 5,
                    IsDeleted = false
                },
                new Service()
                {
                    ServiceId = 6,
                    ServiceName = "Trade License Request",
                    IsActive = true,
                    ServiceCode = "TL",
                    DefaultRate = 100,
                    CreatedByUserId = 1,
                    CreatedDate = DateTime.Now.DubaiTime(),
                    ModifiedByUserId = 1,
                    ModifiedDate = DateTime.Now.DubaiTime(),
                    VATPercentage = 5,
                    IsDeleted = false
                },
                new Service()
                {
                    ServiceId = 7,
                    ServiceName = "Exit of Aircraft Spare Parts Request",
                    IsActive = true,
                    ServiceCode = "ES",
                    DefaultRate = 100,
                    CreatedByUserId = 1,
                    CreatedDate = DateTime.Now.DubaiTime(),
                    ModifiedByUserId = 1,
                    ModifiedDate = DateTime.Now.DubaiTime(),
                    VATPercentage = 5,
                    IsDeleted = false
                },
                new Service()
                {
                    ServiceId = 8,
                    ServiceName = "Meteorological Data Request",
                    IsActive = true,
                    ServiceCode = "MD",
                    DefaultRate = (decimal)0.01,
                    CreatedByUserId = 1,
                    CreatedDate = DateTime.Now.DubaiTime(),
                    ModifiedByUserId = 1,
                    VATPercentage = 5,
                    ModifiedDate = DateTime.Now.DubaiTime(),
                    IsDeleted = false
                },
                new Service()
                {
                    ServiceId = 9,
                    ServiceName = "Building Height Request",
                    IsActive = true,
                    ServiceCode = "BH",
                    DefaultRate = 100,
                    CreatedByUserId = 1,
                    CreatedDate = DateTime.Now.DubaiTime(),
                    ModifiedByUserId = 1,
                    ModifiedDate = DateTime.Now.DubaiTime(),
                    VATPercentage = 5,
                    IsDeleted = false
                }
            );
            context.SaveChanges();
        }

        private void SetAirportOfLanding(EServiceDb context)
        {
            context.AirportLandings.AddOrUpdate(o => o.Id,
                new AirportLanding() { Id = 1, Active = true, Description = "DXB/OMDB" },
                new AirportLanding() { Id = 2, Active = true, Description = "DWC/OMDW" },
                new AirportLanding() { Id = 3, Active = true, Description = "DXB/DWC" },
                new AirportLanding() { Id = 4, Active = true, Description = "DWC/DXB" }
            );
            context.SaveChanges();
        }

        private void SetDepartments(EServiceDb context)
        {
            context.Departments.AddOrUpdate(o => o.DepartmentId,
                new Department() { IsDefault = true, DepartmentId = 1, DepartmentName = "Planning Dept.", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 2, DepartmentName = "Safety Dept.", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 3, DepartmentName = "Finance Dept.", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 4, DepartmentName = "ATS", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 5, DepartmentName = "GM", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 6, DepartmentName = "NOC", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 7, DepartmentName = "DCA.", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 8, DepartmentName = "OAGM.", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 9, DepartmentName = "MET", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 10, DepartmentName = "Chairman", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 11, DepartmentName = "Accounts", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 12, DepartmentName = "GSD", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 13, DepartmentName = "Cargo", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 14, DepartmentName = "ATC", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 15, DepartmentName = "OPS", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 16, DepartmentName = "ASM", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 17, DepartmentName = "HSE", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 18, DepartmentName = "Quality", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 19, DepartmentName = "HR", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 20, DepartmentName = "Personal", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 21, DepartmentName = "CFO", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 22, DepartmentName = "PRO", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 23, DepartmentName = "Audit", IsDeleted = false },
                new Department() { IsDefault = true, DepartmentId = 24, DepartmentName = "TSD", IsDeleted = false }
            );
        }

        private void SetCraneTypes(EServiceDb context)
        {
            context.CraneTypes.AddOrUpdate(o => o.CraneTypeId
                , new CraneType() { CraneTypeId = 1, CraneTypeName = "Mobile Crane", IsDeleted = false, IsOther = false }
                , new CraneType() { CraneTypeId = 2, CraneTypeName = "Tower Crane", IsDeleted = false, IsOther = false }
                , new CraneType()
                {
                    CraneTypeId = 3,
                    CraneTypeName = "Concrete Pump",
                    IsDeleted = false,
                    IsOther = false
                }
                , new CraneType() { CraneTypeId = 4, CraneTypeName = "Cherry Picker", IsDeleted = false, IsOther = false }
                , new CraneType() { CraneTypeId = 5, CraneTypeName = "Others", IsDeleted = false, IsOther = true }
            );
            context.SaveChanges();
        }

        public void SetServiceFlowState(EServiceDb context)
        {
            context.ServiceFlowStatuses.AddOrUpdate(o => o.Id,
                new ServiceFlowStatus() { Description = "New", Id = 1, Active = true },
                new ServiceFlowStatus() { Description = "Active", Id = 2, Active = true },
                new ServiceFlowStatus() { Description = "Approved", Id = 3, Active = true },
                new ServiceFlowStatus() { Description = "Rejected", Id = 4, Active = true },
                new ServiceFlowStatus() { Description = "PaymentPending", Id = 5, Active = true },
                new ServiceFlowStatus() { Description = "PaymentCompleted", Id = 6, Active = true }
                );
            context.SaveChanges();
        }

        private void SetInfringementTypes(EServiceDb context)
        {
            context.InfringementTypes.AddOrUpdate(o => o.Id
                , new InfringementType() { Description = "Transitional Surface", Id = 1, Active = true, IsOther = false }
                , new InfringementType() { Description = "Inner Horizontal", Id = 2, Active = true, IsOther = false }
                , new InfringementType() { Description = "Conical", Id = 3, Active = true, IsOther = false }
                , new InfringementType() { Description = "Outer Horizontal", Id = 4, Active = true, IsOther = false }
                , new InfringementType() { Description = "Apps", Id = 5, Active = true, IsOther = false }
                , new InfringementType() { Description = "TOCS", Id = 6, Active = true, IsOther = false }
                , new InfringementType() { Description = "Others", Id = 7, Active = true, IsOther = true }
            );
            context.SaveChanges();
        }

        private void SetCurrency(EServiceDb context)
        {
            context.Currencies.AddOrUpdate(x => x.CurrencyName
                , new Currency() { CurrencyName = "UAE Dhs", IsDeleted = false }
                , new Currency() { CurrencyName = "US", IsDeleted = false }
                , new Currency() { CurrencyName = "Euro", IsDeleted = false }
            );
            context.SaveChanges();
        }

        private void SetStaffStrength(EServiceDb context)
        {
            context.StaffStrengths.AddOrUpdate(x => x.StaffStrengthName
                , new StaffStrength { StaffStrengthName = "Managerial", IsDeleted = false }
                , new StaffStrength { StaffStrengthName = "Supervisors", IsDeleted = false }
                , new StaffStrength { StaffStrengthName = "Skilled", IsDeleted = false }
                , new StaffStrength { StaffStrengthName = "Unskilled", IsDeleted = false }
            );
            context.SaveChanges();
        }

        #region "Aircraft Warning"

        public void SetProjectTypes(EServiceDb context)
        {
            context.ProjectTypes.AddOrUpdate(o => o.Id
            , new ProjectType() { Id = 1, Description = "Building", IsOther = false, Active = true }
            , new ProjectType() { Id = 2, Description = "OHL/Communication Tower", IsOther = false, Active = true }
            , new ProjectType() { Id = 3, Description = "Other", IsOther = true, Active = true }
                );
            context.SaveChanges();
        }

        public void SetApplicantOwnerRelations(EServiceDb context)
        {
            context.ApplicantOwnerRelationships.AddOrUpdate(o => o.Id
            , new ApplicantOwnerRelationship() { Id = 1, Description = "Owner's Representatives", IsOther = false, Active = true }
            , new ApplicantOwnerRelationship() { Id = 2, Description = "Construction Contractor", IsOther = false, Active = true }
            , new ApplicantOwnerRelationship() { Id = 3, Description = "Architect", IsOther = false, Active = true }
            , new ApplicantOwnerRelationship() { Id = 4, Description = "Engineer", IsOther = false, Active = true }
            , new ApplicantOwnerRelationship() { Id = 5, Description = "Other", IsOther = true, Active = true }
                );
            context.SaveChanges();
        }
        #endregion

        #region "Meteorological Data Request"

        private void SetWeatherTypes(EServiceDb context)
        {
            context.WeatherTypeReports.AddOrUpdate(o => o.Id
            , new WeatherTypeReport() { Id = 1, Description = "Weather Report", Active = true }
            , new WeatherTypeReport() { Id = 2, Description = "Advisory Weather Forecast for Fujairah Area", Active = true }
            , new WeatherTypeReport() { Id = 3, Description = "Adverse Weather Information Notification ", Active = true }
            );
            context.SaveChanges();
        }

        private void SetWeatherElements(EServiceDb context)
        {
            context.WeatherElements.AddOrUpdate(o => o.Id
            , new WeatherElement() { Id = 1, Active = true, Description = "Rainfall Figures", IsOther = false }
            , new WeatherElement() { Id = 2, Active = true, Description = "Surface Wind Direction / Speed and Gust", IsOther = false }
            , new WeatherElement() { Id = 3, Active = true, Description = "Wind Frequency %", IsOther = false }
            , new WeatherElement() { Id = 4, Active = true, Description = "Surface Wind Roses", IsOther = false }
            , new WeatherElement() { Id = 5, Active = true, Description = "Visibility", IsOther = false }
            , new WeatherElement() { Id = 6, Active = true, Description = "Temperature", IsOther = false }
            , new WeatherElement() { Id = 7, Active = true, Description = "Dew Point Temperature", IsOther = false }
            , new WeatherElement() { Id = 8, Active = true, Description = "Relative Humidity", IsOther = false }
            , new WeatherElement() { Id = 9, Active = true, Description = "Pressure (MSL-Altimeter)", IsOther = false }
            , new WeatherElement() { Id = 10, Active = true, Description = "Vapor Pressure", IsOther = false }
            , new WeatherElement() { Id = 11, Active = true, Description = "Weather Day (Phenomena Occurrence : TS, HZ, DS, SS, FG, BR etc.)", IsOther = false }
            , new WeatherElement() { Id = 12, Active = true, Description = "Global Radiation", IsOther = false }
            , new WeatherElement() { Id = 13, Active = true, Description = "Other", IsOther = true }
            );
            context.SaveChanges();
        }
        #endregion

        #region "building Heights"
        private void SetBuildingCoordinateAndHeightLookUp(EServiceDb context)
        {
            context.BuildingCoordinateAndHeightLookUps.AddOrUpdate(x => x.Id
                , new BuildingCoordinateAndHeightLookUp { Id = 1, Description = "A", Active = true }
                , new BuildingCoordinateAndHeightLookUp { Id = 2, Description = "B", Active = true }
                , new BuildingCoordinateAndHeightLookUp { Id = 3, Description = "C", Active = true }
                , new BuildingCoordinateAndHeightLookUp { Id = 4, Description = "D", Active = true }
                , new BuildingCoordinateAndHeightLookUp { Id = 5, Description = "E(if necessary)", Active = true }
                , new BuildingCoordinateAndHeightLookUp { Id = 6, Description = "F(if necessary)", Active = true }
                , new BuildingCoordinateAndHeightLookUp { Id = 7, Description = "G(if necessary)", Active = true }
                , new BuildingCoordinateAndHeightLookUp { Id = 8, Description = "point of tallest height", Active = true }
                , new BuildingCoordinateAndHeightLookUp { Id = 9, Description = "point of second tallest height", Active = true }
                , new BuildingCoordinateAndHeightLookUp { Id = 10, Description = "point of third tallest height", Active = true }
            );
            context.SaveChanges();
        }

        private void SetAviationObstacleLookUp(EServiceDb context)
        {
            context.AviationObstacleLookUps.AddOrUpdate(x => x.Id
                , new AviationObstacleLookUp { Id = 1, Description = "none", Active = true }
                , new AviationObstacleLookUp { Id = 2, Description = "aeronautical spheres", Active = true }
                , new AviationObstacleLookUp { Id = 3, Description = "obstruction light", Active = true }
                , new AviationObstacleLookUp { Id = 4, Description = "red and white paint markings", Active = true }
            );
            context.SaveChanges();
        }

        private void SetAdditionalItemsForBuildingSubmittalLookUp(EServiceDb context)
        {
            context.AdditionalItemsForBuildingSubmittalLookUps.AddOrUpdate(x => x.Id
                , new AdditionalItemsForBuildingSubmittalLookUp { Id = 1, Description = "Location map", Active = true, IsOther = false }
                , new AdditionalItemsForBuildingSubmittalLookUp { Id = 2, Description = "Site plan *", Active = true, IsOther = false }
                , new AdditionalItemsForBuildingSubmittalLookUp { Id = 3, Description = "Floor plan", Active = true, IsOther = false }
                , new AdditionalItemsForBuildingSubmittalLookUp { Id = 4, Description = "Elevations *", Active = true, IsOther = false }
                , new AdditionalItemsForBuildingSubmittalLookUp { Id = 5, Description = "Section(s) *", Active = true, IsOther = false }
                , new AdditionalItemsForBuildingSubmittalLookUp { Id = 6, Description = "Municipality permit", Active = true, IsOther = false }
                , new AdditionalItemsForBuildingSubmittalLookUp { Id = 7, Description = "Specification sheets of obstacle lighting", Active = true, IsOther = false }
                , new AdditionalItemsForBuildingSubmittalLookUp { Id = 8, Description = "Trade License Copy", Active = true }
                , new AdditionalItemsForBuildingSubmittalLookUp { Id = 9, Description = "Soft copy of building points in MS Excel file format", Active = true, IsOther = false }
                , new AdditionalItemsForBuildingSubmittalLookUp { Id = 10, Description = "Other", Active = true, IsOther = true }

            );
            context.SaveChanges();
        }

        private void SetAdditionalItemsForOHLSubmittalLookUp(EServiceDb context)
        {
            context.AdditionalItemsForOHLSubmittalLookUps.AddOrUpdate(x => x.Id
                , new AdditionalItemsForOHLSubmittalLookUp { Id = 1, Description = "Location map", Active = true, IsOther = false }
                , new AdditionalItemsForOHLSubmittalLookUp { Id = 2, Description = "Site plan *", Active = true, IsOther = false }
                , new AdditionalItemsForOHLSubmittalLookUp { Id = 3, Description = "Elevations *", Active = true, IsOther = false }
                , new AdditionalItemsForOHLSubmittalLookUp { Id = 4, Description = "Municipality permit/ contract authorization", Active = true, IsOther = false }
                , new AdditionalItemsForOHLSubmittalLookUp { Id = 5, Description = "Specifications for items to be used for aviation obstacle mitigation", Active = true, IsOther = false }
                , new AdditionalItemsForOHLSubmittalLookUp { Id = 6, Description = "Trade License Copy", Active = true }
                , new AdditionalItemsForOHLSubmittalLookUp { Id = 7, Description = "Soft copy in MS Excel file format", Active = true, IsOther = false }
                , new AdditionalItemsForOHLSubmittalLookUp { Id = 8, Description = "Other", Active = true, IsOther = true }

            );
            context.SaveChanges();
        }

        private void SetApplicableFeesLookUps(EServiceDb context)
        {
            context.ApplicableFeesLookUps.AddOrUpdate(x => x.Id
                , new ApplicableFeesLookUp() { Id = 1, Description = "Buildings", Notes = "per 1-2 buildings on same site", Fees = "5,000", Fee = 5000, Active = true }
                , new ApplicableFeesLookUp() { Id = 2, Description = "Buildings", Notes = "per each additional structure on same site", Fees = "2,500", Fee = 2500, Active = true }
                , new ApplicableFeesLookUp() { Id = 3, Description = "OHL", Notes = "application fee only", Fees = "5,000", Fee = 5000, Active = true }
                , new ApplicableFeesLookUp() { Id = 4, Description = "OHL", Notes = "per each structure", Fees = "200", Fee = 200, Active = true }
                , new ApplicableFeesLookUp() { Id = 5, Description = "Communications Tower", Notes = "per each structure", Fee = 5000, Fees = "5,000", Active = true }
                , new ApplicableFeesLookUp() { Id = 6, Description = "Other", Notes = "application fee per site", Fees = "5,000", Fee = 5000, Active = true }
                , new ApplicableFeesLookUp() { Id = 7, Description = "Other", Notes = "additional fee for project complexity", Fees = "TBD on case to case basi", Fee = null, Active = true }
                , new ApplicableFeesLookUp() { Id = 8, Description = "Crane Permit", Notes = "included with project application fee", Fees = "0", Fee = 0, Active = true }
                , new ApplicableFeesLookUp() { Id = 9, Description = "Expedited Services", Notes = "For 3-5 business day turn-around on NOC application & analysis.Not applicable to Aeronautical safety studies", Fees = "3,000", Fee = 3000, Active = true }
            );
        }

        #endregion
    }
}