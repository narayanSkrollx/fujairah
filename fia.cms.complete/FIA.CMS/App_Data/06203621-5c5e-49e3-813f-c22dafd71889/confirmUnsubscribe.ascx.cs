﻿using System;
using System.Collections;
using umbraco.cms.businesslogic.member;


namespace Designit.Umb.Newsletter.Custom.usercontrols.designit.newsletter
{
    public partial class confirmUnsubscribe : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var guid = Request["guid"];
            var member = Designit.Umb.Newsletter.Bll.ConfirmUnsubscribe.Confirm(guid);
            ltlSucces.Text = member.Email + " has succesfully unsubscribed";
        }

        
    }
}