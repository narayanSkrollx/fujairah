using System;
using System.Linq;
using System.Web.Mvc;
using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Service.Logic.Enum;
using EService.Web.Controllers.Base;
using EService.Web.Helper;
using EService.Web.Models;
using EService.Web.Models.ServiceRequestOperation;
using Nelibur.ObjectMapper;
using EService.Core;
using EService.Web.Helper.EmailSender;

namespace EService.Web.Controllers
{
    public class AircraftWarningLightController : ServiceRequestBaseController
    {
        private AircraftWarningLightService aircraftWarningLightService;
        private UserServiceRequestMasterService userServiceRequestMasterService;
        private LookupService lookupService;
        private ProjectTypeService projectTypeService;
        private ApplicantOwnerRelationshipService applicantOwnerRelationshipService;
        private UserServiceRequestAttachmentService userServiceRequestAttachmentService;
        private ServiceInfoService serviceInfoService;
        private UserService userService;

        public AircraftWarningLightController()
        {
            this.projectTypeService = new ProjectTypeService(serviceContext);
            aircraftWarningLightService = new AircraftWarningLightService(serviceContext);
            userServiceRequestMasterService = new UserServiceRequestMasterService(serviceContext);
            lookupService = new LookupService();
            applicantOwnerRelationshipService = new ApplicantOwnerRelationshipService(serviceContext);
            userServiceRequestAttachmentService = new UserServiceRequestAttachmentService(serviceContext);
            serviceInfoService = new ServiceInfoService(serviceContext);
            userService = new UserService(serviceContext);
        }

        public override ServiceEnum Service => ServiceEnum.AircraftWarningLight;

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(AircraftWarningLightViewModel model, bool isDraft = false)
        {
            var result = new JsonResponseObject();
            try
            {
                if (ModelState.IsValid)
                {
                    var serviceFlowStages = ServiceApprovalStageService.FindAllAsNoTracking(o => o.ServiceId == ServiceId).ToList();
                    var serviceModal = userServiceRequestMasterService.GetNewServiceCode(ServiceId);
                    var serviceInfo = serviceInfoService.Find(o => o.ServiceId == ServiceId);
                    if (model.UserServiceRequestMasterId > 0)
                    {
                        var existingRequest = userServiceRequestMasterService.Find(o => o.IsDraft
                        && !o.IsDeleted
                        && o.UserServiceRequestMasterId == model.UserServiceRequestMasterId
                        && o.UserId == UserProfile.UserId
                        && o.AircraftWarningLight != null
                        );
                        if (existingRequest != null)
                        {
                            existingRequest.IsDraft = isDraft;

                            existingRequest.AircraftWarningLight.ApplicantAddress = model.ApplicantAddress;
                            existingRequest.AircraftWarningLight.ApplicantAddress = model.ApplicantAddress;
                            existingRequest.AircraftWarningLight.ApplicantEmail = model.ApplicantEmail;
                            existingRequest.AircraftWarningLight.ApplicantCellNo = model.ApplicantCellNo;
                            existingRequest.AircraftWarningLight.ApplicantCompanyName = model.ApplicantCompanyName;
                            existingRequest.AircraftWarningLight.ApplicantContactInfo = model.ApplicantContactInfo;
                            existingRequest.AircraftWarningLight.ApplicantJobTitle = model.ApplicantJobTitle;
                            existingRequest.AircraftWarningLight.ApplicantName = model.ApplicantName;
                            existingRequest.AircraftWarningLight.ApplicantTelNo = model.ApplicantTelNo;
                            existingRequest.AircraftWarningLight.ApplicantWebsite = model.ApplicantWebsite;
                            existingRequest.AircraftWarningLight.ApplicationDate = model.ApplicationDate;
                            existingRequest.AircraftWarningLight.NoOfObstacleLights = model.NoOfObstacleLights ?? 0;
                            existingRequest.AircraftWarningLight.NoOfStructures = model.NoOfStructures ?? 0;
                            existingRequest.AircraftWarningLight.OwnerAddress = model.OwnerAddress;
                            existingRequest.AircraftWarningLight.OwnerCellNo = model.OwnerCellNo;
                            existingRequest.AircraftWarningLight.OwnerCompanyName = model.OwnerCompanyName;
                            existingRequest.AircraftWarningLight.OwnerContactInfo = model.OwnerContactInfo;
                            existingRequest.AircraftWarningLight.OwnerEmail = model.OwnerEmail;
                            existingRequest.AircraftWarningLight.OwnerName = model.OwnerName;
                            existingRequest.AircraftWarningLight.OwnerTelNo = model.OwnerTelNo;
                            existingRequest.AircraftWarningLight.OwnerWebsite = model.OwnerWebsite;
                            existingRequest.AircraftWarningLight.ProjectDescription = model.ProjectDescription;
                            existingRequest.AircraftWarningLight.ProjectName = model.ProjectName;
                            existingRequest.AircraftWarningLight.ProjectLocation = model.ProjectLocation;
                            existingRequest.AircraftWarningLight.TypesOfObstacleLights = model.TypesOfObstacleLights;
                            existingRequest.AircraftWarningLight.ApplicantOwnerRelationShipId = model.ApplicantOwnerRelationShipId;
                            existingRequest.AircraftWarningLight.ProjectTypeId = model.ProjectTypeId;
                            existingRequest.AircraftWarningLight.ProjectTypeOtherName = model.ProjectTypeOtherName;
                            existingRequest.AircraftWarningLight.ApplicantOwnerRelationshipOtherName = model.ApplicantOwnerRelationshipOtherName;

                            existingRequest.AircraftWarningLight.ModifiedByUserId = UserProfile.UserId;
                            existingRequest.AircraftWarningLight.ModifiedDate = DateTime.Now.DubaiTime();

                            #region "Uploaded Files Edit"
                            if (model.EditUploadedFiles != null && model.EditUploadedFiles.Any())
                            {
                                var deletedFiles = model.EditUploadedFiles.Where(o => o.ObjectState == ObjectState.Deleted).Select(o => o.Id);
                                var removeList = existingRequest.UserServiceRequestAttachments.Where(o => deletedFiles.Contains(o.UserAttachmentId)).ToList();
                                removeList.ForEach(o => userServiceRequestAttachmentService.Remove(o));
                            }
                            if (model.UploadedFiles != null && model.UploadedFiles.Any())
                            {
                                model.UploadedFiles.ForEach(o =>
                                {
                                    var item = new UserServiceRequestAttachment
                                    {
                                        UserAttachmentId = o.Id
                                    };
                                    existingRequest.UserServiceRequestAttachments.Add(item);
                                });
                            }

                            #endregion

                            serviceContext.Commit();
                            result.IsSuccess = true;
                            result.Message = "Aircraft warning light request updated successfully.";

                            if (!isDraft)
                            {
                                //TriggerEmail(FIAEmailType.New, model.UserServiceRequestMasterId);
                                EmailSenderHelper.SendNewRequestEmail(ControllerContext, model.UserServiceRequestMasterId);
                            }
                        }
                        else
                        {
                            result.IsSuccess = false;
                            result.Message = "Invalid Aircraft Warning Light information provided to update.";
                        }
                    }
                    else
                    {
                        var userServiceRequest = new UserServiceRequestMaster()
                        {
                            RequestDateTime = DateTime.Now.DubaiTime(),
                            ServiceId = ServiceId,
                            Status = (int)ServiceFlowStateEnum.New,
                            UserId = UserProfile.UserId,
                            IsDeleted = false,
                            ParentUserServiceRequestMasterId = model.ParentUserServiceRequestMasterid,
                            IsDraft = isDraft,
                            VAT = serviceInfo.VATPercentage,
                            ServiceRequestFlowStages = serviceFlowStages
                            .Select(o => new ServiceRequestFlowStage()
                            {
                                Index = o.Index,
                                IsCurrentStage = o.Index == 1,
                                ServiceRequestFlowStageDepartments = o.ServiceApprovalStageDepartments.Select(p =>
                                new ServiceRequestFlowStageDepartment()
                                {
                                    DepartmentId = p.DepartmentId,
                                    Status = (int)ServiceFlowStateEnum.New,
                                    IsApproved = false,
                                }).ToList()
                            }).ToList(),
                            RegistrationNoYear = serviceModal.Data.Year,
                            RegistrationNoMonth = serviceModal.Data.Month,
                            RegistrationNoIndex = serviceModal.Data.RegistrationNoIndex,
                            RegistrationNoServiceCode = serviceModal.Data.ServiceCode,
                            RegistrationNo = serviceModal.Data.RegistrationNo,
                            UserServiceRequestAttachments = model.UploadedFiles.Select(o => new UserServiceRequestAttachment()
                            {
                                UserAttachmentId = o.Id
                            }).ToList(),
                            ServieBill = new UserServiceRequestBill()
                            {
                                ChargedAmount = (double)serviceInfo.DefaultRate
                            }
                        };


                        var aircraftWarningLight = new AircraftWarningLightServiceRequest()
                        {
                            UserServiceRequestMaster = userServiceRequest,
                            ApplicantAddress = model.ApplicantAddress,
                            ApplicantEmail = model.ApplicantEmail,
                            ApplicantCellNo = model.ApplicantCellNo,
                            ApplicantCompanyName = model.ApplicantCompanyName,
                            ApplicantContactInfo = model.ApplicantContactInfo,
                            ApplicantJobTitle = model.ApplicantJobTitle,
                            ApplicantName = model.ApplicantName,
                            ApplicantTelNo = model.ApplicantTelNo,
                            ApplicantWebsite = model.ApplicantWebsite,
                            ApplicationDate = model.ApplicationDate,
                            NoOfObstacleLights = model.NoOfObstacleLights ?? 0,
                            NoOfStructures = model.NoOfStructures ?? 0,
                            OwnerAddress = model.OwnerAddress,
                            OwnerCellNo = model.OwnerCellNo,
                            OwnerCompanyName = model.OwnerCompanyName,
                            OwnerContactInfo = model.OwnerContactInfo,
                            OwnerEmail = model.OwnerEmail,
                            OwnerName = model.OwnerName,
                            OwnerTelNo = model.OwnerTelNo,
                            OwnerWebsite = model.OwnerWebsite,
                            ProjectDescription = model.ProjectDescription,
                            ProjectName = model.ProjectName,
                            ProjectLocation = model.ProjectLocation,
                            TypesOfObstacleLights = model.TypesOfObstacleLights,
                            CreatedByUserId = UserProfile.UserId,
                            CreatedDate = DateTime.Now.DubaiTime(),
                            IsDeleted = false,
                            ApplicantOwnerRelationShipId = model.ApplicantOwnerRelationShipId,
                            ProjectTypeId = model.ProjectTypeId,
                            ProjectTypeOtherName = model.ProjectTypeOtherName,
                            ApplicantOwnerRelationshipOtherName = model.ApplicantOwnerRelationshipOtherName,
                            //AircraftWarningLightOwnerApplicantRelations = model.ApplicantOwnerRelations.Where(o => o.IsSelected).Select(o => new AircraftWarningLightOwnerApplicantRelation()
                            //{
                            //    ApplicantOwnerRelationShipId = o.ApplicantOwnerRelationShipId,
                            //    OtherName = o.IsOther ? o.OtherName : "",

                            //}).ToList(),
                            //AircraftWarningLightProjectTypes = model.ProjectTypes.Where(o => o.IsSelected).Select(o => new AircraftWarningLightProjectType()
                            //{
                            //    ProjectTypeId = o.ProjectTypeId,
                            //    OtherName = o.IsOther ? o.OtherName : ""
                            //}).ToList()

                        };
                        //cranePermitRequestService.Add(cranePermitRequest);
                        aircraftWarningLightService.Add(aircraftWarningLight);
                        serviceContext.Commit();
                        if (!isDraft)
                        {
                            //TriggerEmail(FIAEmailType.New, aircraftWarningLight.UserServiceRequestMasterId);
                            EmailSenderHelper.SendNewRequestEmail(ControllerContext, aircraftWarningLight.UserServiceRequestMasterId);
                        }
                        result.IsSuccess = true;
                        result.Message = "";
                    }
                }
                else
                {
                    result.Message = "Server Validation Error: " + ModelState.GetModelStateError();
                    result.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving Aircraft Warning Light Request.";
            }
            return Json(result);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.UserServiceRequestMasterId = id;
            return View("Create");
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Initialize()
        {
            var result = new JsonResponseObject();
            try
            {
                var data = userServiceRequestMasterService.GetServiceRequests(UserProfile.UserId, ServiceId);
                result.Data = data;
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while obtaining list";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InitializeCreate(int? id)
        {
            try
            {
                var model = new AircraftWarningLightViewModel()
                {
                    ApplicationDate = DateTime.Now.DubaiTime(),
                    ProjectTypes = projectTypeService.FindAllAsNoTracking(o => o.Active).AsEnumerable().Select(o => new ProjectType()
                    {
                        Id = o.Id,
                        Description = o.Description,
                        IsOther = o.IsOther
                    }).ToList(),
                    Relationships = applicantOwnerRelationshipService.FindAllAsNoTracking(o => o.Active).AsEnumerable().Select(o => new ApplicantOwnerRelationship()
                    {
                        Id = o.Id,
                        IsOther = o.IsOther,
                        Description = o.Description
                    }).ToList()
                };
                var reapplyData = TempData[Constants.ServiceRequestReapplyContentTempDataKey] as ServiceReapplyTempDataViewModel;
                if (reapplyData != null || id.HasValue)
                {
                    var editId = reapplyData != null ? reapplyData.UserServiceRequestMasterId : id;
                    var userServiceRequest =
                        userServiceRequestMasterService.Find(
                            o => o.UserServiceRequestMasterId == editId &&
                                 !o.IsDeleted && o.UserId == UserProfile.UserId && o.ServiceId == ServiceId && (!id.HasValue || o.IsDraft));

                    if (userServiceRequest == null || userServiceRequest.AircraftWarningLight == null)
                    {
                        return Json(new JsonResponseObject() { IsSuccess = false, Message = $"Invalid User Service Request for {(reapplyData != null ? @"Reapply" : "Edit")}." });
                    }
                    else
                    {
                        var aircraftWarning = userServiceRequest.AircraftWarningLight;

                        TinyMapper.Bind<AircraftWarningLightServiceRequest, AircraftWarningLightViewModel>();

                        var aircraftWarningViewModel = TinyMapper.Map<AircraftWarningLightViewModel>(aircraftWarning);
                        aircraftWarningViewModel.ApplicationDate = model.ApplicationDate;
                        aircraftWarningViewModel.ProjectTypes = model.ProjectTypes;
                        aircraftWarningViewModel.Relationships = model.Relationships;

                        model = aircraftWarningViewModel;
                        model.UserServiceRequestMasterId = reapplyData != null ? 0 : (id ?? 0);
                        model.ParentUserServiceRequestMasterid = reapplyData?.UserServiceRequestMasterId;

                    }
                }

                model.UserInfo = userService.GetUserBasicInfo(UserProfile.UserId).Data;
                //model.UploadedFiles = userServiceRequestMasterService.
                model.EditUploadedFiles = GetUploadedFiles(reapplyData?.UserServiceRequestMasterId ?? (id ?? 0));
                return Json(new JsonResponseObject() { IsSuccess = true, Data = new { AddNewModel = model } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return Json(new JsonResponseObject() { IsSuccess = false, Message = "Error" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Reapply()
        {
            return View("Create");
        }
    }
}