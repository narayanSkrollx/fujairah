﻿using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class UserServiceRequestReceiptConfiguration : EntityTypeConfiguration<UserServiceRequestReceipt>
    {
        public UserServiceRequestReceiptConfiguration(string schema = "dbo")
        {
            ToTable("UserServiceRequestReceipt", schema);

            HasKey(o => o.UserServiceRequestReceiptId);

            Property(o => o.UserServiceRequestReceiptId).IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(o => o.UserServiceRequestMasterId).IsRequired();
            Property(o => o.ReceiptNo).IsOptional().HasMaxLength(255);
            Property(o => o.Year).IsOptional();
            Property(o => o.Index).IsOptional();
            Property(o => o.PaymentStage).IsRequired();
            Property(o => o.IsPaid).IsRequired();
            Property(o => o.AdminFee).IsOptional();
            Property(o => o.ServiceDevelopmentCharge).IsOptional();
            Property(o => o.VATAmount).IsOptional();

            HasRequired(o => o.UserServiceRequestMaster).WithMany(o => o.UserServiceRequestReceipts).HasForeignKey(o => o.UserServiceRequestMasterId).WillCascadeOnDelete(false);

        }
    }

    public class UserServiceRequestRecieptDetailConfiguration : EntityTypeConfiguration<UserServiceRequestReceiptDetail>
    {
        public UserServiceRequestRecieptDetailConfiguration(string schema = "dbo")
        {
            ToTable("UserServiceRequestReceiptDetail", schema);

            HasKey(o => o.UserServiceRequestReceiptDetailId);

            Property(o => o.UserServiceRequestReceiptDetailId).IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(o => o.UserServiceRequestReceiptId).IsRequired();


            Property(o => o.Description).IsRequired().HasMaxLength(1024);
            Property(o => o.Quantity).IsRequired();
            Property(o => o.Rate).IsRequired();

            HasRequired(o => o.Receipt).WithMany(o => o.ReceiptDetails).HasForeignKey(o => o.UserServiceRequestReceiptId).WillCascadeOnDelete(false);

            
        }
    }
}