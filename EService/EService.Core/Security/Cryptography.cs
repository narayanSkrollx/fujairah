﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace EService.Core.Security
{
    public static class Cryptography
    {
        /// <summary>
        ///     Converts clear string to MD5 encrypted string
        /// </summary>
        /// <param name="clearText">Clear text</param>
        /// <returns>Returns MD5 encrypted text</returns>
        public static String ToMd5(this String clearText)
        {
            string result;
            const string salt = "!@#$%^&*()";
            var saltedString = String.Concat(clearText, salt);

            // Encrypt this user's password information.
            using (MD5 md5EncryptionObject = new MD5CryptoServiceProvider())
            {
                var originalStringBytes = Encoding.Default.GetBytes(saltedString);
                var encodedStringBytes = md5EncryptionObject.ComputeHash(originalStringBytes);

                var sb = new StringBuilder();
                foreach (var b in encodedStringBytes)
                {
                    sb.Append(b.ToString("x2").ToLower());
                }
                result = sb.ToString();
            }
            return result;
        }
    }
}
