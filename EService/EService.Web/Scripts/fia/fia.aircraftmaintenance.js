﻿$(function () {
    fia.CreateNamespace("aircraftmaintenance");

    (function (context) {
        context.Title = 'Aircraft Maintenance';
        context.createForm = '#frmAircraftMaintenanceRequest';
        context.ServiceId = fia.ServiceEnum.AircraftMaintenanceRequest;

        context.InitializeCreateUrl = ApplicationRootUrl('InitializeCreate', 'AircraftMaintenanceRequest');
        context.CreateUrl = ApplicationRootUrl('Create', 'AircraftMaintenanceRequest');
        context.CreateSuccessUrl = ApplicationRootUrl('Index', 'Dashboard');
        context.InitializeIndexUrl = ApplicationRootUrl('Initialize', 'AircraftMaintenanceRequest');
        context.DetailsUrl = ApplicationRootUrl('Detail', 'AircraftMaintenanceRequest');
        context.InitializeDetailUrl = ApplicationRootUrl('InitializeDetail', 'AircraftMaintenanceRequest');

        context.SaveRequest = function (isDraft) {
            if (!$(context.createForm).valid()) {
                return false;
            }
            var model = { model: ko.mapping.toJS(context.ViewModel.AddNewModel), isDraft: isDraft };
            var submitControl = isDraft ? 'draft' : 'save';
            var lightModel = JSON.stringify(model);
            var ajaxOption = {
                dataType: "JSON",
                contentType: "application/json; charset=UTF-8",
                data: lightModel,
                enableLadda: true,
                targetLaddaElement: `[data-role=${submitControl}]`,
            };
            ajaxRequest(context.CreateUrl, 'POST', ajaxOption, function (resp) {
                if (resp.IsSuccess) {
                    showMessage(context.Title, resp.Message, 'success', function () {
                        //
                        window.location = '/ServiceRequest/Index?serviceId=' + context.ServiceId;
                    });
                } else {
                    showMessage(context.Title, resp.Message, 'error');
                }
            });
        }

        context.ViewModel = {
            SaveRequest: function () {
                context.SaveRequest(false);
            },
            SaveDraftRequest: function () {
                context.SaveRequest(true);
            },
            ViewDetails: function (item) {
                window.location = context.DetailsUrl + '?id=' + item.UserServiceRequestMasterId();
            },
            SetUserInfo: function () {
                context.SetUserInfo();
            },
            AfterRenderCreate: function () {
                $(context.createForm).validate({
                    rules: {
                        DateOfApplication: { required: true,  },
                        NameOfApplicant: { required: true, maxlength: 225 },
                        Designation: { required: true, maxlength: 225 },
                        PhoneNumber: { required: true, maxlength: 20, regex: phoneNoRegex },
                        EmailId: { required: true, email: true }
                    },
                    messages: {
                        DateOfApplication: { required: 'Application Date is required.', date: 'Application Date must be a valid date.' },
                        NameOfApplicant: { required: 'Applicant Name is required.', maxlength: 'Applicant Name length cannot be greater than 225.' },
                        Designation: { required: 'Applicant Designation is required.', maxlength: 'Applicant Designation length cannot be greater than 225.'},
                        PhoneNumber: { required: 'Applicant Phone number is required.', maxlength: 'Applicant Phone number length cannot be greater than 20.'},
                        EmailId: { required: 'Applicant Email is required.', email: 'Applicant Email must be valid Email.' }
                    }
                });
            }
        };

        context.SetUserInfo = function () {
            var addNewModel = context.ViewModel.AddNewModel;
            var userInfo = context.ViewModel.AddNewModel.UserInfo;
            if (addNewModel != null && userInfo != null) {
                addNewModel.NameOfApplicant(userInfo.FirstName() + ' ' + userInfo.LastName());
                addNewModel.PhoneNumber(userInfo.Phone());
                addNewModel.EmailId(userInfo.Email());
            }
        }
        context.InitializeCreate = function () {
            var masterId = parseInt($("#CurrentUserServiceRequestMasterId.forKnockout").val());
            context.ViewModel.IsCreate = Number.isInteger(masterId);
            var id = context.ViewModel.IsCreate ? parseInt(masterId) : null;

            ajaxRequest(context.InitializeCreateUrl, 'GET', { data: { id: id } }, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#mainContent')[0])) {
                        context.ViewModel.AddNewModel = ko.mapping.fromJS(resp.Data);
                        context.ViewModel.RemoveUploadedFile = function (item) {
                            item.ObjectState(2);
                        }
                        ko.applyBindings(context.ViewModel, $('#mainContent')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel.AddNewModel);
                    }
                    fia.fileinputhelper.InitializeFileUpload('#kv-explorer', fia.ServiceEnum.AircraftMaintenanceRequest);
                    fia.fileinputhelper.UploadCompleteCallback= function(response) {
                        context.ViewModel.AddNewModel.UploadedFiles.push(response.Data);

                    }
                } else {
                    console.log(resp.Message);
                }
            });
        };

        context.InitializeIndex = function () {
            ajaxRequest(context.InitializeIndexUrl, 'GET', {}, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#mainContent')[0])) {
                        context.ViewModel.Records = ko.mapping.fromJS(resp.Data);
                        ko.applyBindings(context.ViewModel, $('#mainContent')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel.Records);
                    }
                } else {
                    showMessage('Service Request Detail', resp.Message, 'error');
                }
            });
        };

        context.InitializeDetail = function () {
            context.CurrentUserServiceRequestMasterId = $("#CurrentUserServiceRequestMasterId.forKnockout").val();

            ajaxRequest(context.InitializeDetailUrl + "?id=" + context.CurrentUserServiceRequestMasterId, 'GET', {}, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#aircraftMaintenanceDetailsDiv')[0])) {
                        context.ViewModel.Details = ko.mapping.fromJS(resp.Data);
                        ko.applyBindings(context.ViewModel, $('#aircraftMaintenanceDetailsDiv')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel.Details);
                    }

                    fia.fileinputhelper.InitializeUploadedList('#uploadedFiles', context.CurrentUserServiceRequestMasterId);
                } else {
                    showMessage('Service Request Detail', resp.Message, 'error');
                }
            });
        };

    })(fia.aircraftmaintenance);
});