﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EService.Data.Contracts;

namespace EService.Service.Logic
{
    public class ServiceContext : IDisposable
    {
        bool disposed;

        public ServiceContext()
        {
            Database = new EServiceDb();
        }

        protected internal EServiceDb Database { get; private set; }

        public int Commit()
        {
            return Database.SaveChanges();
        }

        #region Dispose Members

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (Database != null)
                    {
                        Database.Dispose();
                        Database = null;
                    }
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ServiceContext()
        {
            Dispose(false);
        }

        #endregion
    }
}
