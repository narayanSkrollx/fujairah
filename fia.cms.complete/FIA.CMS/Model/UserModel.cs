﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FIA.CMS.Model
{
    public class UserModel
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        public string ResumePath { get; set; }

        public string FullName
        {
            get { return FirstName + " " + LastName; }
           
        }
        [Required]
        public string UserName { get; set; }
        public string YearOfExperience { get; set; }
        public string Overview { get; set; }
        public string Contact { get; set; }
        public HttpPostedFileBase File { get; set; }

    }
}