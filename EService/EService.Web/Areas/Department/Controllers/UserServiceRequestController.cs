﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Service.Logic.Enum;
using EService.Service.Model;
using EService.Web.Areas.Department.Models;
using EService.Web.Controllers;
using EService.Web.Controllers.Base;
using EService.Web.Helper;
using EService.Web.Models;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;

namespace EService.Web.Areas.Department.Controllers
{
    public class UserServiceRequestController : DepartmentUserBaseController
    {
        private UserServiceRequestMasterService userServiceRequestService;
        private CraneTypeService craneTypeService;
        private CranePermitRequestService cranePermitRequestService;
        private ExitOfAircraftSparePartsRequestService exitOfSparePartsService;
        private MeteorologicalDataRequestService metRequestService;
        private BuildingHeightRequestService buildingHeightRequestService;
        private UserAttachmentService userAttachmentService;

        public UserServiceRequestController()
        {
            userServiceRequestService = new UserServiceRequestMasterService(serviceContext);
            craneTypeService = new CraneTypeService(serviceContext);
            cranePermitRequestService = new CranePermitRequestService(serviceContext);
            exitOfSparePartsService = new ExitOfAircraftSparePartsRequestService(serviceContext);
            metRequestService = new MeteorologicalDataRequestService(serviceContext);
            buildingHeightRequestService = new BuildingHeightRequestService(serviceContext);
            userAttachmentService = new UserAttachmentService(serviceContext);
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Initialize(int pageSize = 50, int page = 1)
        {
            var result = new JsonResponseObject();
            try
            {
                var skip = (page - 1) * pageSize;
                result.Data =
                    userServiceRequestService.GetServiceRequests(DepartmentId, new ServiceFlowStateEnum[] { ServiceFlowStateEnum.New, ServiceFlowStateEnum.Active }, pageSize, skip).ToList();
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while obtaining service requests.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail(int id)
        {
            TempData["CurrentUserServiceRequestId"] = id;
            return View();
        }

        [HttpGet]
        public JsonResult InitializeDetailView()
        {
            var result = new JsonResponseObject();
            try
            {
                var id = Convert.ToInt32(TempData["CurrentUserServiceRequestId"]);
                TempData["CurrentUserServiceRequestId"] = id;
                var data = userServiceRequestService.Find(o => !o.IsDeleted && o.UserServiceRequestMasterId == id);
                var html = "";
                switch (data.ServiceId)
                {
                    case (int)ServiceEnum.CranePermitRequest:
                        var serviceData = userServiceRequestService.GetCranePermitRequest(id);
                        html = RenderPartialView("_CranePermitRequestDetail", serviceData);
                        break;
                    case (int)ServiceEnum.AircraftMaintenanceRequest:
                        html = RenderPartialView("_AircraftMaintenanceRequestDetail", new AircraftMaintenanceRequestServiceModal());
                        break;
                    case (int)ServiceEnum.AircraftDismantlingRequest:
                        html = RenderPartialView("_AircraftDsimantlingRequestDetail", new AircraftDismantlingRequestServiceModal());
                        break;
                    case (int)ServiceEnum.AircraftWarningLight:
                        html = RenderPartialView("_AircraftWarningLightDetail", new AircraftWarningLightViewModel());
                        break;
                    case (int)ServiceEnum.TradeLicenseRequest:
                        html = RenderPartialView("_TradeLicenseRequestDetail", new TradeLicenseRequestServiceModel());
                        break;
                    case (int)ServiceEnum.MeteorologicalDataRequest:
                        html = RenderPartialView("_MeteorologicalDataRequest", new MeteorologicalDataRequestServiceModal());
                        break;
                    case (int)ServiceEnum.ExitOfAircraftSparePartsRequest:
                        html = RenderPartialView("_ExitOfAircraftSpareParts", new ExitOfAircraftSparePartsRequestViewModel());
                        break;
                    case (int)ServiceEnum.BuildingHeightRequest:
                        html = RenderPartialView("_BuildingHeightRequesetDetail", new BuildingHeightRequestServiceModel());
                        break;
                    case (int)ServiceEnum.GroundHandlingRequest:
                        html = RenderPartialView("_GroundHandlingDetail", new GroundHandlingRequestServiceModel());
                        break;
                    default:
                        html = "Service Not Implemented Yet";
                        break;
                }
                result.Data = new
                {
                    Html = html,
                    UserServiceRequestId = id,
                    Comments = ""
                };
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while obtaining Service info.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult InitializeDetailData()
        {
            var result = new JsonResponseObject();
            try
            {
                var id = Convert.ToInt32(TempData["CurrentUserServiceRequestId"]);
                TempData["CurrentUserServiceRequestId"] = id;
                var data = userServiceRequestService.Find(o => !o.IsDeleted && o.UserServiceRequestMasterId == id);
                var currentStage = data.ServiceRequestFlowStages.FirstOrDefault(o => o.IsCurrentStage);
                if (currentStage != null)
                {
                    var previousStages = data.ServiceRequestFlowStages.
                        Where(o => o.Index < currentStage.Index)
                        .Select(o => new
                        {
                            o.Index,
                            Departments =
                                        o.ServiceRequestFlowStageDepartments.Select(p => p.Department.DepartmentName),
                            o.ServiceRequestFlowStageId
                        })
                        .Select(o => new
                        {
                            Id = o.ServiceRequestFlowStageId,
                            Description =
                                        string.Format("{0} - {1}", "Stage " + o.Index, string.Join(",", o.Departments))
                        })
                        .ToList();
                    object viewData = null;
                    var history = userServiceRequestService.GetRequestFlowStageDepartmentHistories(id).ToList();
                    switch (data.ServiceId)
                    {
                        case (int)ServiceEnum.CranePermitRequest:
                            viewData = userServiceRequestService.GetCranePermitRequest(id);
                            break;
                        case (int)ServiceEnum.AircraftMaintenanceRequest:
                            viewData = userServiceRequestService.GetAircraftMaintenanceRequest(id);
                            break;
                        case (int)ServiceEnum.AircraftDismantlingRequest:
                            viewData = userServiceRequestService.GetAircraftDismantlingRequest(id);
                            break;
                        case (int)ServiceEnum.AircraftWarningLight:
                            viewData = userServiceRequestService.GetAircraftWarningRequest(id);
                            break;
                        case (int)ServiceEnum.TradeLicenseRequest:
                            viewData = userServiceRequestService.GetTradeLicenseRequest(id);
                            break;
                        case (int)ServiceEnum.MeteorologicalDataRequest:
                            viewData = userServiceRequestService.GetMeteorologicalDataRequest(id);
                            break;
                        case (int)ServiceEnum.ExitOfAircraftSparePartsRequest:
                            viewData = userServiceRequestService.GetExitOfSparePartsRequest(id);
                            break;
                        case (int)ServiceEnum.BuildingHeightRequest:
                            viewData = userServiceRequestService.GetBuildingHeightRequest(id);
                            break;
                        case (int)ServiceEnum.GroundHandlingRequest:
                            viewData = userServiceRequestService.GetGroundHandlingRequset(id);
                            break;
                        default:
                            break;
                    }
                    result.Data = new
                    {
                        Data = viewData,
                        History = history,
                        UserServiceRequestId = id,
                        Comments = "",
                        PreviousStages = previousStages,
                        IsUserSelectedForReject = false,
                        SelectedStageId = (int?)null
                    };
                    result.IsSuccess = true;
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "Current Stage is null hence user must re-apply this to get in progress.";
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                LogError(ex);
                result.Message = "Error while obtaining Service info.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DepartmentStageEntry()
        {
            var result = new JsonResponseObject();
            try
            {
                var id = Convert.ToInt32(TempData["CurrentUserServiceRequestId"]);
                TempData["CurrentUserServiceRequestId"] = id;
                var data = userServiceRequestService.Find(o => !o.IsDeleted && o.UserServiceRequestMasterId == id);
                var html = "";
                switch (data.ServiceId)
                {
                    case 13:
                        var serviceData = userServiceRequestService.GetCranePermitRequestPlanning(id);
                        //var 
                        html = RenderPartialView("_CranePermitRequestPlanning", serviceData);

                        break;
                    default:
                        html = "Service Not Implemented Yet";
                        break;
                }
                result.Data = new
                {
                    Html = html,
                    //History = history,
                    UserServiceRequestId = id,
                    Comments = ""
                };
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while obtaining Service info.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult CraneRequestStageModel()
        {
            var result = new JsonResponseObject();
            try
            {
                var id = Convert.ToInt32(TempData["CurrentUserServiceRequestId"]);
                TempData["CurrentUserServiceRequestId"] = id;
                var cranePermtRequest = userServiceRequestService.GetCranePermitRequest(id);

                //var serviceData = userServiceRequestService.GetCranePermitRequestPlanning(id);

                result.Data = cranePermtRequest;
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while obtaining Service info.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Approve(int id)
        {
            var result = new JsonResponseObject();
            try
            {
                var approveResult = userServiceRequestService.Approve(id, DepartmentId, UserProfile.UserId);
                result.IsSuccess = approveResult.Item1;
                result.Message = approveResult.Item2;
                if (approveResult.Item1)
                {
                    //
                    TriggerEmail(FIAEmailType.Approved, approveResult.Item3.ServiceRequestFlowStageDepartmentId);
                }
                result.IsSuccess = approveResult.Item1;
                result.Message = approveResult.Item2;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while approving selected request.";
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult Reject(int id, string comments, bool isUserSelectedForReject, int? selectedStageId)
        {
            var result = new JsonResponseObject();
            try
            {
                var rejectResult = userServiceRequestService.Reject(id, DepartmentId, UserProfile.UserId, comments,
                    isUserSelectedForReject, selectedStageId);
                result.IsSuccess = rejectResult.Item1;
                result.Message = rejectResult.Item2;
                if (rejectResult.Item1)
                {
                    TriggerEmail(FIAEmailType.Rejected, rejectResult.Item3.ServiceRequestFlowStageDepartmentId);
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while rejecting the service request.";
            }

            return Json(result);
        }

        public ActionResult Edit(int id)
        {
            try
            {

                TempData["CurrentUserServiceRequestId"] = id;
                var data = userServiceRequestService.Find(o => !o.IsDeleted && o.UserServiceRequestMasterId == id);

                switch (data.ServiceId)
                {
                    case 13:
                        var serviceData = userServiceRequestService.GetCranePermitRequest(id);
                        return View("EditCranePermitRequest", serviceData);
                        break;
                    default:
                        break;
                }
                return View();
            }
            catch (Exception ex)
            {
                LogError(ex);

            }
            return ErrorResult();
        }

        public JsonResult InitializeEdit()
        {
            var result = new JsonResponseObject();
            try
            {
                var id = Convert.ToInt32(TempData["CurrentUserServiceRequestId"]);
                var serviceData = userServiceRequestService.GetCranePermitRequest(id);

                var model = new CranePermitRequestViewModel()
                {
                    ApplicationDate = serviceData.ApplicationDate,
                    UserServiceRequestMasterId = serviceData.UserServiceRequestMasterId,
                    ApplicantEmail = serviceData.ApplicantEmail,
                    CompanyName = serviceData.CompanyName,
                    ApplicantName = serviceData.ApplicantName,
                    CraneLocation = serviceData.CraneLocation,
                    OperationStartTime = serviceData.OperationStartTime,
                    OperationEndTime = serviceData.OperationEndTime,
                    Position = serviceData.Position,
                    PreviousPermitNumbers = serviceData.PreviousPermitNumbers,
                    PurposeOfUser = serviceData.PurposeOfUser,
                    OperatorName = serviceData.OperatorName,
                    NorthingsEastings = serviceData.NorthingsEastings,
                    CraneOperatingCompany = serviceData.CraneOperatingCompany,
                    ContactNumber = serviceData.ContactNumber,
                    OperationStartDate = serviceData.OperationStartDate,
                    OperationEndDate = serviceData.OperationEndDate,
                    IsNewApplication = serviceData.IsNewApplication,
                    OperatorContactNumber = serviceData.OperatorContactNumber,
                    MaxOperatingHeight = serviceData.MaxOperatingHeight,
                    //CraneTypes = craneTypeService.FindAllAsNoTracking(o => !o.IsDeleted).ToList(),
                    CraneTypeSelectionViewModels =
                        craneTypeService.FindAllAsNoTracking(o => !o.IsDeleted)
                            .Select(o => new CraneTypeSelectionViewModel()
                            {
                                CraneTypeId = o.CraneTypeId,
                                IsOther = o.IsOther,
                                CraneTypeName = o.CraneTypeName,
                                IsSelected = false
                            }).ToList()
                };
                result.Data = model;
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "Error while loading edit data.";
                LogError(ex);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadAttachment(int id)
        {
            try
            {
                var userAttachment = userAttachmentService.Find(o => o.UserAttachmentId == id && o.UserAttachmentData != null);
                if (userAttachment != null)
                {
                    var data = userAttachment.UserAttachmentData;
                    var contents = data.FileContents;
                    return File(contents, System.Net.Mime.MediaTypeNames.Application.Octet, userAttachment.FileName);
                }
                else
                {
                    return ErrorResult("");
                }

            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return ErrorResult("");
        }

        #region "Crane Permit Request Specific Actions"


        //[HttpPost]
        //public JsonResult UpdateCranePermitRequest(CranePermitRequestViewModel model)
        //{
        //    var result = new JsonResponseObject();

        //    try
        //    {
        //        var cranePermitRequest = new CranePermitRequest
        //        {
        //            ApplicantEmail = model.ApplicantEmail,
        //            ApplicantName = model.ApplicantName,
        //            ApplicationDate = model.ApplicationDate.Value,
        //            CompanyName = model.CompanyName,
        //            ContactNumber = model.ContactNumber,
        //            CraneLocation = model.CraneLocation,
        //            CraneOperatingCompany = model.CraneOperatingCompany,
        //            IsNewApplication = model.IsNewApplication,
        //            PurposeOfUser = model.PurposeOfUser,
        //            PreviousPermitNumbers = model.PreviousPermitNumbers,
        //            MaxOperatingHeight = model.MaxOperatingHeight.Value,
        //            NorthingsEastings = model.NorthingsEastings,
        //            OperationStartDate = model.OperationStartDate.Value,
        //            OperationEndDate = model.OperationEndDate.Value,
        //            OperationStartTime = model.OperationStartTime,
        //            OperationEndTime = model.OperationEndTime,
        //            OperatorContactNumber = model.OperatorContactNumber,
        //            OperatorName = model.OperatorName,
        //            Position = model.Position,
        //            CranePermitRequestSelectedCranes =
        //                model.CraneTypeSelectionViewModels.Where(o => o.IsSelected)
        //                    .Select(o => new CranePermitRequestSelectedCrane()
        //                    {
        //                        CraneTypeId = o.CraneTypeId,
        //                        OtherCraneType = o.OtherCraneTypeName,
        //                    }).ToList(),
        //            UserServiceRequestMasterId = model.UserServiceRequestMasterId
        //        };
        //        cranePermitRequestService.UpdateCranePemitRequest(cranePermitRequest);
        //        result.IsSuccess = true;
        //        result.Message = "Crane Permit request updated successfully.";
        //    }
        //    catch (Exception ex)
        //    {
        //        result.IsSuccess = false;
        //        result.Message = "Error while updating crane permit request.";
        //    }

        //    return Json(result);
        //}


        [HttpPost]
        public JsonResult SaveCranePlanningInfo(CraneRequestPlanningViewModel model, int id, string InfringementTypes)
        {
            var result = new JsonResponseObject();

            TempData["CurrentUserServiceRequestId"] = id;
            try
            {
                if (!ModelState.IsValid)
                {
                    result.IsSuccess = false;
                    result.Message = ModelState.GetModelStateError();
                }
                else
                {
                    model.PlanningModel.InfringementTypes = JsonConvert.DeserializeObject<List<InfringementTypeModel>>(InfringementTypes);
                    if (model.FileUpload != null && model.FileUpload.Length > 0)
                    {
                        model.PlanningModel.UploadedFiles = model.FileUpload.Where(o => o != null).Select(o => new UploadedFile
                        {
                            Content = new BinaryReader(o.InputStream).ReadBytes(o.ContentLength),
                            ContentType = o.ContentType,
                            FileName = o.FileName
                        }).ToList();
                    }
                    var saveResult = cranePermitRequestService.SaveCraneRequestPlanningInfo(model.PlanningModel, id,
                        UserProfile.UserId);
                    result.IsSuccess = saveResult.IsSuccess;
                    result.Message = saveResult.Message;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving Crane Request Planning Info";
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult SaveCraneQASafetyInfo(CraneRequestSafetyViewModel model, int id)
        {
            var result = new JsonResponseObject();

            TempData["CurrentUserServiceRequestId"] = id;
            try
            {
                if (!ModelState.IsValid)
                {
                    result.IsSuccess = false;
                    result.Message = ModelState.GetModelStateError();
                }
                else
                {
                    if (model.FileUpload != null && model.FileUpload.Length > 0)
                    {
                        model.QASafetyModel.UploadedFiles = model.FileUpload.Where(o => o != null).Select(o => new UploadedFile
                        {
                            Content = new BinaryReader(o.InputStream).ReadBytes(o.ContentLength),
                            ContentType = o.ContentType,
                            FileName = o.FileName
                        }).ToList();
                    }

                    var saveResult = cranePermitRequestService.SaveCraneRequestQASafetyInfo(model.QASafetyModel, id,
                        UserProfile.UserId);
                    result.IsSuccess = saveResult.IsSuccess;
                    result.Message = saveResult.Message;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving Crane Permit Request QA Safety Approval";
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult SaveCraneFinanceInfo(CraneRequestFinanceModel model, int id)
        {
            var result = new JsonResponseObject();

            TempData["CurrentUserServiceRequestId"] = id;
            try
            {
                if (!ModelState.IsValid)
                {
                    result.IsSuccess = false;
                    result.Message = ModelState.GetModelStateError();
                }
                else
                {
                    var saveResult = cranePermitRequestService.SaveCraneRequestFinanceInfo(model, id,
                        UserProfile.UserId);
                    result.IsSuccess = saveResult.IsSuccess;
                    result.Message = saveResult.Message;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving Crane Permit Request Finance Info";
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult SaveMetRequestChargeAmount(int id, decimal chargedAmount, string remarks)
        {
            var result = new JsonResponseObject();

            TempData["CurrentUserServiceRequestId"] = id;
            try
            {
                if (!ModelState.IsValid)
                {
                    result.IsSuccess = false;
                    result.Message = ModelState.GetModelStateError();
                }
                else
                {
                    var saveResult = metRequestService.UpdateChargeAmount(id, chargedAmount, remarks);
                    result.IsSuccess = saveResult.IsSuccess;
                    result.Message = saveResult.Message;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving Meteorological Data Request charge Amount";
            }
            return Json(result);
        }
        [HttpPost]
        public JsonResult SaveBuildingHeightRequestChargeAmount(int id, decimal chargedAmount)
        {
            var result = new JsonResponseObject();

            TempData["CurrentUserServiceRequestId"] = id;
            try
            {
                if (!ModelState.IsValid)
                {
                    result.IsSuccess = false;
                    result.Message = ModelState.GetModelStateError();
                }
                else
                {
                    var saveResult = buildingHeightRequestService.UpdateChargeAmount(id, chargedAmount);
                    result.IsSuccess = saveResult.IsSuccess;
                    result.Message = saveResult.Message;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving Building Height Request Charge Amount";
            }
            return Json(result);
        }
        [HttpPost]
        public JsonResult SaveBuildingHeightRequestPlanningRemarks(int id, string remarks, HttpPostedFileBase[] FileUpload)
        {
            var result = new JsonResponseObject();

            try
            {
                if (!ModelState.IsValid)
                {
                    result.IsSuccess = false;
                    result.Message = ModelState.GetModelStateError();
                }
                else
                {
                    var files = (FileUpload ?? new HttpPostedFileBase[] { }).Select(o => new UploadedFile
                    {
                        ContentType = o.ContentType,
                        FileName = o.FileName,
                        Content = new BinaryReader(o.InputStream).ReadBytes(o.ContentLength),
                    }).ToArray();
                    var saveResult = buildingHeightRequestService.AddPlanningRemarks(id, remarks, files, UserProfile.UserId);
                    result.IsSuccess = saveResult.IsSuccess;
                    result.Message = saveResult.Message;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving Meteorological Data Request charge Amount";
            }
            return Json(result);
        }
        #endregion

    }
}