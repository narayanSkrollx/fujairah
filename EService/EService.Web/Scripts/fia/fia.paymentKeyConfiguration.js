﻿$(function () {
    fia.CreateNamespace("paymentKeyConfiguration");

    (function (context) {

        context.Title = 'Payment Key Configuration';
        context.ViewModel = {

        };
        context.ViewModel.Save = function () {
            if (!$('#form').valid()) {
                return false;
            }

            var model = ko.toJS(context.ViewModel.PaymentKeyConfiguration);

            ajaxRequest('/Admin/PaymentKeyConfiguration/Index', 'POST', { data: { model: model } }, function (response) {
                if (response.IsSuccess) {
                    showMessage(context.Title, 'Save Success', 'success', function () {
                        window.location = window.location;
                    });
                } else {
                    showMessage(context.Title, response.Message, 'error');
                }
            });
        }

        context.Initialize = function () {
            ajaxRequest('/Admin/PaymentKeyConfiguration/Initialize', 'GET', { data: {} }, function (response) {
                if (response.IsSuccess) {
                    if (!ko.dataFor($('#mainContent')[0])) {
                        context.ViewModel.PaymentKeyConfiguration = ko.mapping.fromJS(response.Data, {});

                        ko.applyBindings(context.ViewModel, $('#mainContent')[0]);
                    } else {
                        ko.mapping.fromJS(response.Data, {}, context.ViewModel.PaymentKeyConfiguration);
                    }
                    context.ApplyValidation();
                } else {
                    showMessage(context.Title, response.Message, 'error');
                }
            });
        }

        context.ApplyValidation = function () {
            $('#form').validate({
                rules: {
                    MerchantId: { required: true },
                    EncryptionKey: { required: true },
                    Url: { required: true },
                },
                messages: {
                    MerchantId: { required: 'Merchant Id is required' },
                    EncryptionKey: { required: 'Encryption Key is required' },
                    Url: { required: 'Url is required' },
                }
            });
        }

    })(fia.paymentKeyConfiguration);
});