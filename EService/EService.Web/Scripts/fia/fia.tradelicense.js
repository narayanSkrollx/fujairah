﻿$(function () {
    fia.CreateNamespace("tradelicense");

    (function (context) {
        context.Title = 'Trade License Request';
        context.ServiceId = fia.ServiceEnum.TradeLicenseRequest;

        context.InitializeCreateUrl = ApplicationRootUrl('InitializeCreate', 'TradeLicenseRequest');
        context.CreateUrl = ApplicationRootUrl('Create', 'TradeLicenseRequest');
        context.CreateSuccessUrl = ApplicationRootUrl('Index', 'Dashboard');
        context.InitializeIndexUrl = ApplicationRootUrl('Initialize', 'TradeLicenseRequest');
        context.DetailsUrl = ApplicationRootUrl('Detail', 'TradeLicenseRequest');
        context.InitializeDetailUrl = ApplicationRootUrl('InitializeDetail', 'TradeLicenseRequest');

        context.SaveRequest = function (isDraft) {
            if (!$('#frmTradeLicenseRequest').valid()) {
                return false;
            }
            var model = ko.mapping.toJS(context.ViewModel.AddNewModel);
            //delete model.Currencies;
            var submitControl = isDraft ? 'draft' : 'save';
            var lightModel = JSON.stringify({ model: model, isDraft: isDraft });
            var ajaxOption = {
                dataType: "JSON",
                contentType: "application/json; charset=UTF-8",
                data: lightModel,
                enableLadda: true,
                targetLaddaElement: `[data-role=${submitControl}]`,
            };
            ajaxRequest(context.CreateUrl, 'POST', ajaxOption, function (resp) {
                if (resp.IsSuccess) {
                    showMessage('Trade License Request', resp.Message, 'success', function () {
                        //
                        window.location = context.CreateSuccessUrl;
                    });
                } else {
                    showMessage('Trade License Request', resp.Message, 'error');
                }
            });
        }

        context.ViewModel = {
            SaveRequest: function () {
                context.SaveRequest(false);
            },
            SaveDraftRequest: function () {
                context.SaveRequest(true);
            },
            SetUserInfo: function () {
                context.SetUserInfo();
            },
            ViewDetails: function (item) {
                window.location = context.DetailsUrl + '?id=' + item.UserServiceRequestMasterId();
            }
        };

        context.Mapping = {
            create: function(options) {
                var vm = ko.mapping.fromJS(options.data);

                vm.AfterRenderComplete = function() {
                    context.ApplyValidation();
                }

                return vm;
            }
        }

        context.ApplyValidation = function() {
            console.log('AfterRenderComplete');
            $('#frmTradeLicenseRequest').validate({
                rules: {
                    ApplicantName:{required: true,},
                    PresentSponsorInUAE: { required: true },
                    AddressOfSponser: { required: true },
                    ApplicantOfficeNumber: { required: true },
                    ApplicantResidenceNumber: {},
                    ApplicantMobileNumber: { required: true, regex: phoneNoRegex },
                    ApplicantEmailNumber: { required: true, email: true },
                    ApplicantCountry: { required: true },
                    ApplicationDate:{required: true},


                    CompanyPreferredName:{required: true,},
                    BusinessActivities1: { required: true, },
                    ExpectedAnnualBusinessTrunoverAmount: { required: true, number: true},
                    TotalStaffStrengthRequired: { required: true, },
                },
                messages: {
                    ApplicantName: { required: 'Applicant Name is required.', },
                    PresentSponsorInUAE: { required: 'Present Sponsor in UAE is required.' },
                    AddressOfSponser: { required: 'Address of Sponsor is required' },
                    ApplicantOfficeNumber: { required: 'Applicant Office Number is required' },
                    ApplicantResidenceNumber: {},
                    ApplicantMobileNumber: { required: 'Applicant Mobile Number is required.' },
                    ApplicantEmailNumber: { required: 'Applicant Email is required', email: 'Applicant Email must be a valid email address.' },
                    ApplicantCountry: { required: 'Applicant Country is required.' },
                    ApplicationDate: { required: 'Application Date is required.', date: 'Application Date must be a valid date,' },


                    CompanyPreferredName: { required: 'Company Preferred Name is required', },
                    BusinessActivities1: { required: 'Business Activities 1 is requried.', },
                    ExpectedAnnualBusinessTrunoverAmount: { required: 'Expected Annual Turnover amount is required.', number: 'Expected Annual Turnover number must be a valid number.' },
                    TotalStaffStrengthRequired: { required: 'Total Staff strength is required.', },
                }
            });
        }

        context.SetUserInfo = function () {
            var addNewModel = context.ViewModel.AddNewModel;
            var userInfo = context.ViewModel.AddNewModel.UserInfo;
            if (addNewModel != null && userInfo != null) {
                addNewModel.ApplicantName(userInfo.FirstName()+' '+userInfo.LastName());
                addNewModel.ApplicantMobileNumber(userInfo.Phone());
                addNewModel.ApplicantEmailNumber(userInfo.Email());
                addNewModel.ApplicantFaxNumber(userInfo.Fax());
            }
        }


        context.InitializeCreate = function () {
            var masterId = parseInt($("#CurrentUserServiceRequestMasterId.forKnockout").val());
            context.ViewModel.IsCreate = Number.isInteger(masterId);
            var id = context.ViewModel.IsCreate ? parseInt(masterId) : null;

            ajaxRequest(context.InitializeCreateUrl, 'GET', { data: { id: id } }, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#createContent')[0])) {
                        context.ViewModel.AddNewModel = ko.mapping.fromJS(resp.Data, context.Mapping);
                        context.ViewModel.RemoveUploadedFile = function (item) {
                            item.ObjectState(2);
                        }
                        ko.applyBindings(context.ViewModel, $('#createContent')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel.AddNewModel);
                    }
                    //
                    fia.fileinputhelper.InitializeFileUpload('#kv-explorer', fia.ServiceEnum.TradeLicenseRequest);
                    fia.fileinputhelper.UploadCompleteCallback = function (response) {
                        context.ViewModel.AddNewModel.UploadedFiles.push(response.Data);
                    }
                } else {
                    showMessage('Trade License Service Request', resp.Message, 'error');
                }
            });
        };

        context.InitializeIndex = function () {
            ajaxRequest(context.InitializeIndexUrl, 'GET', {}, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#mainContent')[0])) {
                        context.ViewModel.Records = ko.mapping.fromJS(resp.Data);
                        ko.applyBindings(context.ViewModel, $('#mainContent')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel.Records);
                    }
                } else {
                    showMessage('Service Request Detail', resp.Message, 'error');
                }
            });
        };

        context.InitializeDetail = function () {
            context.CurrentUserServiceRequestMasterId = $("#CurrentUserServiceRequestMasterId.forKnockout").val();

            ajaxRequest(context.InitializeDetailUrl + "?id=" + context.CurrentUserServiceRequestMasterId, 'GET', {}, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#tradeLicenseDetailsDiv')[0])) {
                        context.ViewModel.Details = ko.mapping.fromJS(resp.Data);
                        ko.applyBindings(context.ViewModel, $('#tradeLicenseDetailsDiv')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel.Details);
                    }
                    //
                    fia.fileinputhelper.InitializeUploadedList('#uploadedFiles', context.CurrentUserServiceRequestMasterId);
                } else {
                    showMessage('Trade License Detail', resp.Message, 'error');
                }
            });
        };

    })(fia.tradelicense);
});