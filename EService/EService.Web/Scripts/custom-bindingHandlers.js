ko.bindingHandlers.datePicker = {
    init: function (element, valueAccessor, allBindingsAccessor) {

        //initialize datepicker with date only formatting
        var options = allBindingsAccessor().dateTimePickerOptions || { format: "DD/MM/YYYY", useCurrent: false };
        $(element).datetimepicker(options);

        //when a user changes the date, update the view model
        ko.utils.registerEventHandler(element,
            "dp.change",
            function (event) {
                var value = valueAccessor();
                if (ko.isObservable(value)) {
                    if (event.date != null) {
                        value(event.date);
                    } else {
                        value(null);
                    }
                }
            });

        ko.utils.domNodeDisposal.addDisposeCallback(element,
            function () {
                var picker = $(element).data("DateTimePicker");
                if (picker) {
                    picker.destroy();
                }
            });
    },
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var picker = $(element).data("DateTimePicker");

        //when the view model is updated, update the widget
        if (picker) {
            var koDate = ko.utils.unwrapObservable(valueAccessor());
            if (koDate === false || koDate === null) {
                picker.date(null);
            } else {
                if (koDate !== undefined) {
                    koDate = koDate._isAMomentObject ? koDate : new Date(parseFloat(koDate.replace(/[^0-9]/g, "")));
                    picker.date(koDate);
                }
            }
        }
    }
};


ko.bindingHandlers.dateAndTimePicker = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        //initialize datepicker with date time formatting
        var options = allBindingsAccessor().dateTimePickerOptions || { format: "DD/MM/YYYY hh:mm a", useCurrent: false };
        $(element).datetimepicker(options);

        //when a user changes the date, update the view model
        ko.utils.registerEventHandler(element,
            "dp.change",
            function (event) {
                var value = valueAccessor();
                if (ko.isObservable(value)) {
                    if (event.date != null) {
                        value(event.date);
                    } else {
                        value(null);
                    }
                }
            });

        ko.utils.domNodeDisposal.addDisposeCallback(element,
            function () {
                var picker = $(element).data("DateTimePicker");
                if (picker) {
                    picker.destroy();
                }
            });
    },
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var picker = $(element).data("DateTimePicker");

        //when the view model is updated, update the widget
        if (picker) {
            var koDate = ko.utils.unwrapObservable(valueAccessor());
            if (koDate === false || koDate === null) {
                picker.date(null);
            } else {
                if (koDate !== undefined) {
                    koDate = koDate._isAMomentObject ? koDate : new Date(parseFloat(koDate.replace(/[^0-9]/g, "")));
                    picker.date(koDate);
                }
            }
        }
    }
};
