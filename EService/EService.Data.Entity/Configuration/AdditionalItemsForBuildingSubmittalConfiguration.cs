using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class AdditionalItemsForBuildingSubmittalConfiguration : EntityTypeConfiguration<AdditionalItemsForBuildingSubmittal>
    {
        public AdditionalItemsForBuildingSubmittalConfiguration()
        {
            ToTable("AdditionalItemsForBuildingSubmittal");
            HasKey(x => x.AdditionalItemsForBuildingSubmittalId);

            Property(x => x.AdditionalItemsForBuildingSubmittalId).IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.UserServiceRequestMasterId).IsRequired();
            Property(x => x.AdditionalItemsForBuildingSubmittalLookUpId).IsRequired();
            Property(x => x.OtherDescription).IsOptional().HasMaxLength(225);

            HasRequired(x => x.BuildingHeightRequest).WithMany(x => x.AdditionalItemsForBuildingSubmittals)
                .HasForeignKey(x => x.UserServiceRequestMasterId);
            HasRequired(x => x.AdditionalItemsForBuildingSubmittalLookUp)
                .WithMany(x => x.AdditionalItemsForBuildingSubmittals)
                .HasForeignKey(x => x.AdditionalItemsForBuildingSubmittalLookUpId).WillCascadeOnDelete(false);

        }
    }
}