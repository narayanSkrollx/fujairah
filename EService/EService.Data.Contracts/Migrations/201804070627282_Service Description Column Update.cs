namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ServiceDescriptionColumnUpdate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Service", "Description", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Service", "Description", c => c.String(maxLength: 1000));
        }
    }
}
