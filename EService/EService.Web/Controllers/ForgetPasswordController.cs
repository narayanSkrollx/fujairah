using System;
using System.IO;
using System.Web.Mvc;
using EService.Service.Logic;
using EService.Service.Model;
using EService.Web.Helper;
using EService.Web.Models;

namespace EService.Web.Controllers
{
    public class ForgotPasswordController : Controller
    {
        private ForgotPasswordRequestService forgotPasswordRequestService;
        private ServiceContext serviceContext;
        private EmailService emailService;
        private UserService userService;

        public ForgotPasswordController()
        {
            serviceContext = new ServiceContext();
            forgotPasswordRequestService = new ForgotPasswordRequestService(serviceContext);
            emailService = new EmailService(serviceContext);
            userService = new UserService(serviceContext);
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string email)
        {
            var result = new JsonResponseObject();
            try
            {
                var passwordResetRequest = forgotPasswordRequestService.GetNewForgotPasswordToken(email);
                if (passwordResetRequest != null && passwordResetRequest.Item1)
                {
                    var model = new ForgotPasswordUserEmailModel()
                    {
                        UserName = passwordResetRequest.Item3 + " " + passwordResetRequest.Item4,
                        Token = passwordResetRequest.Item2.Token.ToString("N")
                    };
                    var smtpSetting = emailService.Find(o => true);
                    var content = RenderPartialView("~/Views/Shared/EmailTemplates/_ForgotPasswordUserEmailTemplate.cshtml", model);
                    EmailHelper.SendEmail(smtpSetting.From, new string[] { passwordResetRequest.Item2.Email }, new string[] { }, new string[] { }, "Request for password reset", content, smtpSetting);
                }
                result.IsSuccess = passwordResetRequest.Item1;
                result.Data = null;
                result.Message = passwordResetRequest.Item1 ? "Please check your email." : "Error while processing password reset request.";
            }
            catch (Exception)
            {
                //LogError(ex);
                ViewBag.Response = new JsonResponseObject
                {
                    IsSuccess = false,
                    Message = "Error while processing password reset request."
                };
                result.IsSuccess = false;
                result.Message = "Error while configuring for password reset";
            }

            return Json(result);
        }

        public ActionResult Reset(string token)
        {
            var result = new ResponseObject<ResetPasswordViewModel>();
            try
            {
                var guidToken = Guid.Parse(token);
                var forgotPassword = forgotPasswordRequestService.IsTokenValid(guidToken);
                result.IsSuccess = forgotPassword;
                result.Message = forgotPassword ? "" : "The link is already expired. Please try again.";
                result.Data = forgotPassword ? new ResetPasswordViewModel()
                {
                    Token = token
                } : null;
            }
            catch (Exception)
            {
                result.IsSuccess = false;
                result.Message = "Error while trying to validate the link.";
            }
            return View(result);
        }

        [HttpPost]
        public ActionResult Reset(ResetPasswordViewModel model)
        {
            var result = new ResponseObject<ResetPasswordViewModel>();
            try
            {
                if (!ModelState.IsValid)
                {
                    result.IsSuccess = false;
                    result.Message = ModelState.GetModelStateError();
                }
                else
                {
                    var guidToken = Guid.Parse(model.Token);
                    var forgotPassword = forgotPasswordRequestService.IsTokenValid(guidToken);
                    if (forgotPassword)
                    {
                        //valid
                        var resetPasswordResult = userService.ResetPassword(model.Password, guidToken);
                        if (resetPasswordResult.IsSuccess)
                        {
                            result.IsSuccess = true;
                            result.Message = "Password reset successfully. Proceed to login";
                        }
                        else
                        {
                            result.IsSuccess = false;
                            result.Message = resetPasswordResult.Message;
                            result.Data = new ResetPasswordViewModel() { Token = model.Token };
                        }
                    }
                    else
                    {
                        result.IsSuccess = false;
                        result.Message = "Invalid Token or already expired. Try again.";
                    }
                }

            }
            catch (Exception)
            {
                //LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while resetting password.";
            }

            return View(result);
        }

        protected virtual string RenderPartialView(string partialViewName, object model = null)
        {
            if (ControllerContext == null)
                return string.Empty;

            if (string.IsNullOrEmpty(partialViewName))
                throw new ArgumentNullException("partialViewName");

            ModelState.Clear();//Remove possible model binding error.

            ViewData.Model = model;//Set the model to the partial view

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, partialViewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}