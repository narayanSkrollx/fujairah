﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EService.Service.Model.Email
{
    public class ApprovedEmailListModel
    {
        public string[] NextStageDepartmentEmails { get; set; }

        public string ApprovedByUserFullName { get; set; }
        public string ApprovedByUserEmail { get; set; }
    }
}
