namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HourlyDatarequestformet : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MeteorologicalDataRequestService", "DataRangeHourly", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MeteorologicalDataRequestService", "DataRangeHourly");
        }
    }
}
