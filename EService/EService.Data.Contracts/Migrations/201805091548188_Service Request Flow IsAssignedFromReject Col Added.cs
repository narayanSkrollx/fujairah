namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ServiceRequestFlowIsAssignedFromRejectColAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ServiceRequestFlowStageDepartment", "IsAssignedAfterReject", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ServiceRequestFlowStageDepartment", "IsAssignedAfterReject");
        }
    }
}
