﻿using System.Collections.Generic;

namespace EService.Data.Entity.Entity
{
    public class StaffStrength: AuditableEntity
    {
        public int StaffStrengthId { get; set; }
        public string StaffStrengthName { get; set; }

        public virtual ICollection<TradeLicenseSelectedStaffStrength> TradeLicenseSelectedStaffStrengths { get; set; }
    }
}