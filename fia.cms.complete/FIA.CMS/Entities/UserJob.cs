﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace FIA.CMS.Entities
{
    [TableName("UserJob")]
    [PrimaryKey("Id", autoIncrement = true)]
    [ExplicitColumns]
    public class UserJob
    {
        [Column("Id")]
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }
        [Column("UserId")]
        public int UserId { get; set; }
        [Column("JobId")]
        public int JobId { get; set; }
        [Column("IsAccepted")]
        public bool? IsAccepted { get; set; }
        [Column("ApplyDate")]
        public DateTime ApplyDate { get; set; }
        [Column("AcceptRejectDate")]
        public DateTime? AcceptRejectDate { get; set; }
    }
}