using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class UserAttachmentConfiguration : EntityTypeConfiguration<UserAttachment>
    {
        public UserAttachmentConfiguration()
        {
            ToTable("UserAttachment");

            HasKey(o => o.UserAttachmentId);

            Property(o => o.UserAttachmentId).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.Timestamp).IsRequired();
            Property(o => o.UserId).IsRequired();
            Property(o => o.ServiceId).IsRequired();
            Property(o => o.FileName).IsRequired().HasMaxLength(255);
            Property(o => o.ContentType).IsRequired().HasMaxLength(255);
            //Property(o => o.FileContents).IsRequired().HasColumnType("varbinary(MAX)");

            HasRequired(o => o.User).WithMany(o => o.UserAttachments).HasForeignKey(o => o.UserId).WillCascadeOnDelete(false);
            HasRequired(o => o.Service).WithMany(o => o.UserAttachments).HasForeignKey(o => o.ServiceId).WillCascadeOnDelete(false);
        }
    }
}