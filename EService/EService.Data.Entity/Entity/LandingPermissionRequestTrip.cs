using System;

namespace EService.Data.Entity.Entity
{
    public class LandingPermissionRequestTrip
    {
        public int LandingPermissionRequestTripId { get; set; }
        public int LandingPermissionRequestId { get; set; }

        #region "Trip Details"
        /// <summary>
        /// PAX/Crew/Cargo Details
        /// Required
        /// </summary>
        public string CrewDetail { get; set; }//required
        /// <summary>
        /// No of Passengers
        /// Required
        /// </summary>
        public int NoOfPassengers { get; set; }
        /// <summary>
        /// ETD, Origin DateTime
        /// Required
        /// </summary>
        public DateTime ETDOrigin { get; set; }
        /// <summary>
        /// ETA(Destination after outbound Dubai) 
        /// Required
        /// </summary>
        public DateTime ETADestination { get; set; }

        public bool IsManualEntry { get; set; }
        /// <summary>
        /// Action Code
        /// Requried
        /// e.g. N
        /// </summary>
        public string TripActionCode { get; set; }
        /// <summary>
        /// Arrival Flight Destination Number
        /// Required
        /// e.g. XY023
        /// </summary>
        public string TripArrivalDestinationCode { get; set; }
        /// <summary>
        /// Departure flight Destinator number
        /// required
        /// e.g. XY024
        /// </summary>
        public string TripDepartureDestinationCode { get; set; }
        /// <summary>
        /// Start Of Period or Single Day
        /// Required
        /// e.g. 03AUG
        /// </summary>
        public string StartOfPeriod { get; set; }
        /// <summary>
        /// End of period or single day
        /// e.g. 30SEP
        /// </summary>
        public string EndOfPeriod { get; set; }
        /// <summary>
        /// Weekdays of operation
        /// e.g. 1234500
        /// </summary>
        public int WeekdaysOfOperation { get; set; }

        /// <summary>
        /// No of Seats fitted (3 digits)
        /// e.g. 120
        /// required
        /// </summary>
        public int NoOfSeats { get; set; }
        /// <summary>
        /// IARA Aircraft type code (3 alphanumeric)
        /// e.g. 319
        /// </summary>
        public string AircraftTypeCode { get; set; }
        /// <summary>
        /// Origin/Previous Station (Arriving from )
        /// e.g. BKK
        /// </summary>

        public string OriginStationCode { get; set; }
        /// <summary>
        /// Required Arrival Time in UTC
        /// e.g. 0700
        /// </summary>
        public string ArrivalTimeUTC { get; set; }
        /// <summary>
        /// Required Departure time in UTC
        /// e.g. 0750
        /// </summary>
        public string DepartureTimeUTC { get; set; }
        /// <summary>
        /// Next/Destination Station (departure To)
        /// e.g. BDJ
        /// </summary>
        public string DestinationStationCode { get; set; }
        /// <summary>
        /// Arrival Service Type
        /// e.g. J
        /// </summary>
        public string ArrivalServiceType { get; set; }
        /// <summary>
        /// Departure Service Type
        /// e.g. J
        /// </summary>
        public string DepartureServiceType { get; set; }
        public string CompleteRouting { get; set; }
        #endregion
        public LandingPermissionRequest LandingPermissionRequest { get; set; }

    }
}