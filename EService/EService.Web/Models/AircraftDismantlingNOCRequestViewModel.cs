﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EService.Web.Models
{
    public class AircraftDismantlingNOCRequestViewModel : EServiceCreateBaseViewModel, IValidatableObject
    {
        public int UserServiceRequestMasterId { get; set; }

        [Display(Name = "Date of Application")]
        [Required(ErrorMessage = "Date of Application is required")]
        public DateTime? DateOfApplication { get; set; }
        [Display(Name = "Name of the Applicant")]
        [Required(ErrorMessage = "Name of Applicant is required")]
        public string NameOfApplicant { get; set; }
        [Display(Name = "Address")]
        [Required(ErrorMessage = "Applicant Address is required")]
        public string Address { get; set; }
        [Display(Name = "Phone / Mobile No")]
        [Required(ErrorMessage = "Applicant Phone Number is required")]
        public string PhoneNumber { get; set; }
        [Display(Name = "E-mail id")]
        [EmailAddress(ErrorMessage = "Please enter a valid e-mail address")]
        [Required(ErrorMessage = "Applicant Email is required")]
        public string Email { get; set; }
        [Display(Name = "Applicant Company name (Dismantling company)")]
        [Required(ErrorMessage = "Applicant Company Name is required")]
        public string ApplicantCompanyName { get; set; }


        [Display(Name = "Aircraft Type and Model to dismantle")]
        [StringLength(225, ErrorMessage = "Aircraft Type and Model to dismantle length cannot be greater than 225")]
        public string AircraftTypeAndModelToDismantle { get; set; }
        [Display(Name = "Aircraft MSN & Registration Number")]
        [StringLength(225, ErrorMessage = "Aircraft MSN & Registration Number length cannot be greater than 225")]
        public string AircraftMSNAndRegistrationNumber { get; set; }
        [Display(Name = "Old Registration Number")]
        [StringLength(225, ErrorMessage = "Old Registration Number length cannot be greater than 225")]
        public string OldRegistrationNo { get; set; }
        [Display(Name = "Previous Owner of the Aircraft")]
        [StringLength(225, ErrorMessage = "Previous Owner of the Aircraft length cannot be greater than 225")]
        public string PreviousOwnerOfAircraft { get; set; }
        [Display(Name = "Current Owner of the Aircraft ")]
        [StringLength(225, ErrorMessage = "Current Owner of the Aircraft length cannot be greater than 225")]
        public string CurrentOwnerOfAircraft { get; set; }

        [Display(Name = "Operator/Agent who brought the Aircraft in FIA")]
        [StringLength(225, ErrorMessage = "Operator/Agent who brought the Aircraft in FIA length cannot be greater than 225")]
        public string OperatorOrAgentWhoBroughtTheAircraft { get; set; }

        [Display(Name = "With “No Objection from the Aircraft Owner” for the proposed dismantling of the aircraft")]
        public bool? WithNoObjectionFromTheAircraftOwner { get; set; }

        [Display(Name = "Has General Civil Aviation Authority (GCAA) approval attached for Aircraft dismantling")]
        [Required(ErrorMessage = "Has GCA Approval is required")]
        public bool? HasGCAAApproval { get; set; }

        [Display(Name = "GCAA approval  Ref No")]
        [StringLength(225, ErrorMessage = "GCAA approval  Ref No length cannot be greater than 225")]
        public string GCAAApprovalRefNumber { get; set; }

        [Display(Name = "Certificate of De-Registration Ref. No.")]
        [StringLength(225, ErrorMessage = "Certificate of De-Registration Ref. No. length cannot be greater than 225")]
        public string CertificateOfDeRegistrationRefNumber { get; set; }

        [Display(Name = "Trade License Ref. No. of Dismantling Company")]
        [StringLength(225, ErrorMessage = "Trade License Ref. No. of Dismantling Company length cannot be greater than 225")]
        public string TradeLicenseRefNumber { get; set; }

        [Display(Name = "Date of Issue of Trade License")]
        public DateTime? DateOfIssueOfTradeLicense { get; set; }

        [Display(Name = "Date of Expiry of Trade License")]
        public DateTime? DateOfExpiryOfTradeLicense { get; set; }

        [Display(Name = "FANR License Ref. No. of Dismantling Company")]
        [StringLength(225, ErrorMessage = "FANR License Ref. No. of Dismantling Company length cannot be greater than 225")]
        public string FANRLicenseRefNumber { get; set; }

        [Display(Name = "Date of Issue of FANR License")]
        public DateTime? DateOfIssueOfFANRLicense { get; set; }

        [Display(Name = "Date of Expiry of FANR License")]
        public DateTime? DateOfExpiryOfFANRLicense { get; set; }

        [Display(Name = "Fujairah EPD License Ref. No. of Dismantling Company")]
        [StringLength(225, ErrorMessage = "Fujairah EPD License Ref. No. of Dismantling Company length cannot be greater than 225")]
        public string FujairahEPDLicenseRefNumber { get; set; }

        [Display(Name = "Date of Issue of Fujairah EPD License")]
        public DateTime? DateOfIssueOfFujairahEPDLicense { get; set; }

        [Display(Name = "Date of Expiry of Fujairah EPD License")]
        public DateTime? DateOfExpiryOfFujairahEPDLicense { get; set; }

        [Display(Name = "Method of Dismantling and/or Destruction")]
        [StringLength(225, ErrorMessage = "Method of Dismantling and/or Destruction length cannot be greater than 225")]
        public string MethodOfDismantlingAndDestruction { get; set; }


        #region "Display properties"
        public string WithNoObjectionFromTheAircraftOwnerDisplay { get { return WithNoObjectionFromTheAircraftOwner.HasValue ? (WithNoObjectionFromTheAircraftOwner.Value ? "Yes" : "No") : ""; } }
        public string HasGCAAApprovalDisplay { get { return HasGCAAApproval.HasValue ? (HasGCAAApproval.Value ? "Yes" : "No") : ""; } }


        #endregion

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var result = new List<ValidationResult>();
            if (DateOfIssueOfFANRLicense > DateOfExpiryOfFANRLicense)
            {
                result.Add(new ValidationResult("Date of Issue of FANR License cannot be great than Date of Expiry of FANR License"));
            }
            if (DateOfIssueOfTradeLicense > DateOfExpiryOfTradeLicense)
            {
                result.Add(new ValidationResult("Date of Issue of Trade License cannot be great than Date of Expiry of Trade License"));
            }
            if (DateOfIssueOfFujairahEPDLicense > DateOfExpiryOfFujairahEPDLicense)
            {
                result.Add(new ValidationResult("Date of Issue of Fujairah EPD License cannot be greater than Date of Expiry of Fujairah EPD License"));
            }
            return result;
        }
    }
}