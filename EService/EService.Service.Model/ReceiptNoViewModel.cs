﻿namespace EService.Service.Model
{
    public class ReceiptNoViewModel
    {
        public int Year { get; set; }
        public int Index { get; set; }

        public string ReceiptNo { get; set; }
    }
}