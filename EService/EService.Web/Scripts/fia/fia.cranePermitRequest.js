﻿$(function () {
    fia.CreateNamespace("cranePermitRequest");

    (function (context) {
        context.Title = 'Crane Permit Request';
        context.CreateFormId = '#frm';
        context.ServiceId = fia.ServiceEnum.CranePermitRequest;

        context.SaveRequest = function (isDraft) {
            if (!$(context.CreateFormId).valid()) {
                return false;
            }
            var model = ko.toJS(context.ViewModel.AddNewModel);
            model.OperationStartDate = moment(model.OperationStartDate).isValid() ? moment(model.OperationStartDate).format('YYYY/MM/DD') : null;
            model.OperationEndDate = moment(model.OperationEndDate).isValid() ? moment(model.OperationEndDate).format('YYYY/MM/DD') : null;
            model.ApplicationDate = moment(model.ApplicationDate).isValid() ? moment(model.ApplicationDate).format('YYYY/MM/DD') : null;
            var saveModel = { model: model, isDraft: isDraft };
            var submitControl = isDraft ? 'draft' : 'save';
            var lightModel = JSON.stringify(saveModel);
            var ajaxOption = {
                dataType: "JSON",
                contentType: "application/json; charset=UTF-8",
                data: lightModel,
                enableLadda: true,
                targetLaddaElement: `[data-role=${submitControl}]`,
            };
            ajaxRequest('/CranePermitRequest/Create', 'POST', ajaxOption, function (response) {
                if (response.IsSuccess) {
                    showMessage('Crane Permit Requeset', 'Crane Permit Request saved successfully.', 'success', function () {
                        window.location = '/ServiceRequest/Index?ServiceId=' + context.ServiceId;
                    });
                } else {
                    showMessage('Crane Permit Request', response.Message, 'error', function () {
                    });
                }
            });
        }

        context.ViewModel = {

            ViewDetails: function (item) {
                window.location = context.DetailsUrl + '?id=' + item.UserServiceRequestMasterId();
            }
        };

        context.IndexMapping = {
            create: function (options) {
                var vm = ko.mapping.fromJS(options.data);

                vm.ViewDetail = function (item) {
                    window.location = '/CranePermitRequest/Detail/' + item.UserServiceRequestMasterId();
                    console.log(ko.toJSON(item));
                }

                return vm;
            }
        }

        context.CreateMapping = {
            create: function (options) {
                var vm = ko.mapping.fromJS(options.data);
                vm.Save = function () {
                    context.SaveRequest(false)
                }
                vm.SaveDraft = function () {
                    context.SaveRequest(true)
                }

                vm.SetUserInfo = function () {
                    context.SetUserInfo();
                }

                vm.AfterRenderComplete = function () {
                    context.ApplyValidation();
                }

                return vm;
            }
        }

        context.SetUserInfo = function () {
            var addNewModel = context.ViewModel.AddNewModel;
            var userInfo = context.ViewModel.AddNewModel.UserInfo;
            if (addNewModel != null && userInfo != null) {
                addNewModel.ApplicantName(userInfo.FirstName() + ' ' + userInfo.LastName());
                addNewModel.ContactNumber(userInfo.Phone());
                addNewModel.ApplicantEmail(userInfo.Email());
                addNewModel.CompanyName(userInfo.CompanyName());
            }
        }

        context.ApplyValidation = function () {
            $(context.CreateFormId).validate({
                rules: {
                    ApplicationDate: { required: true },
                    ApplicantName: { required: true, maxlength: 255 },
                    CompanyName: { required: true, maxlength: 1024 },
                    Position: { required: true, maxlength: 255 },
                    ContactNumber: { required: true, maxlength: 20, regex: phoneNoRegex },
                    ApplicantEmail: { required: true, maxlength: 255, email: true },

                    PreviousPermitNumbers: { maxlength: 1024 },


                    CraneLocation: { required: true, maxlength: 1024 },
                    NorthingsEastings: { required: true, maxlength: 1024 },
                    OperationStartDate: { required: true, },
                    OperationEndDate: { required: true, },
                    OperationStartTime: { required: true, maxlength: 255, },
                    OperationEndTime: { required: true, maxlength: 255, },
                    MaxOperatingHeight: { required: true, number: true },
                    PurposeOfUser: { required: true, maxlength: 255, },

                    CraneOperatingCompany: { required: true, maxlength: 255, },
                    OperatorName: { required: true, maxlength: 255, },
                    OperatorContactNumber: { required: true, maxlength: 20, regex: phoneNoRegex },
                    CraneType: { required: true }
                },
                messages: {
                    ApplicationDate: {
                        required: 'Application Date is required.',
                        date: 'Application Date must be a valid Date.'
                    },
                    ApplicantName: {
                        required: 'Applicant Name is required.',
                        maxlength: 'Applicant Name length cannot be greater than 255'
                    },
                    CompanyName: {
                        required: 'Applicant Company Name is required.',
                        maxlength: 'Applicant Company name length cannot be greater than 1024'
                    },
                    Position: {
                        required: 'Applicant Position is required.',
                        maxlength: 'Applicant Name position lengfth cannot be greater than 255'
                    },
                    ContactNumber: {
                        required: 'Applicant Contact No is required',
                        maxlength: 'Applicant Contact No length cannot be greater than 20'
                    },
                    ApplicantEmail: {
                        required: 'Applicant Email is required',
                        maxlength: 'Applicant email length cannot be greater than 255',
                        email: 'Applicant email must be a valid email address.'
                    },

                    PreviousPermitNumbers: {
                        maxlength: 'Previous Permit Numbers length cannot be greater than 1024'
                    },


                    CraneLocation: {
                        required: 'Location of Crane is required',
                        maxlength: 'Location of Crane length cannot be greater than 1024'
                    },
                    NorthingsEastings: {
                        required: 'Northings/Eastings is required',
                        maxlength: 'Northings/Eastings length cannot be greater than 1024'
                    },
                    OperationStartDate: {
                        required: 'Operation Start Date is required.',
                        date: 'Operation Start Date must be a valid date.'
                    },
                    OperationEndDate: {
                        required: 'Operation End Date is required.',
                        date: 'Operation end date must be a valid date.'
                    },
                    OperationStartTime: {
                        required: 'Operation Start Time is required',
                        maxlength: 'Operation Start Time length cannot be greater than 255',
                    },
                    OperationEndTime: {
                        required: 'Operation End time is required.',
                        maxlength: 'Operation End time length cannot be greater than 255',
                    },
                    MaxOperatingHeight: {
                        required: 'Max Operation Height is required',
                        number: 'Max Operation Height must be a valid Number',
                        min: 'Max Opearation height must be greater than 1'
                    },
                    PurposeOfUser: {
                        required: 'Purpose of User is required',
                        maxlength: 'Purpose of User length cannot be greater than 255',
                    },

                    CraneOperatingCompany: {
                        required: 'Crane Operating Company is required.',
                        maxlength: 'Crane Operating Company length cannot be greater than 255.',
                    },
                    OperatorName: {
                        required: 'Operator name is required.',
                        maxlength: 'Operator name length cannot be greater than 255',
                    },
                    OperatorContactNumber: { required: 'Operator Contact No is required', maxlength: 'Operator Contact length cannot be greater than 20', },
                    CraneType: { required: "Crane Type must be selected" }
                }
            });
        }

        context.InitializeCreate = function () {
            var masterId = parseInt($("#CurrentUserServiceRequestMasterId.forKnockout").val());
            context.ViewModel.IsCreate = Number.isInteger(masterId);
            var id = context.ViewModel.IsCreate ? parseInt(masterId) : null;
            ajaxRequest('/CranePermitRequest/InitializeCreate', 'GET', { data: { id: id } }, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#createContent')[0])) {
                        context.ViewModel = ko.mapping.fromJS(resp.Data, context.CreateMapping);
                        context.ViewModel.RemoveUploadedFile = function (item) {
                            item.ObjectState(2);
                        }
                        ko.applyBindings(context.ViewModel, $('#createContent')[0]);

                        fia.fileinputhelper.InitializeFileUpload('#kv-explorer', fia.ServiceEnum.CranePermitRequest, resp.Data.InitialPreview, resp.Data.PreviewConfig);
                        fia.fileinputhelper.UploadCompleteCallback = function (response) {
                            context.ViewModel.AddNewModel.UploadedFiles.push(response.Data);
                        }

                        context.ApplyValidation();
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel);
                    }
                } else {
                    console.log(resp.Message);
                }
            });
        };

        context.InitializeIndex = function () {
            ajaxRequest('/CranePermitRequest/Initialize', 'GET', {}, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#mainContent')[0])) {
                        context.ViewModel.Records = ko.mapping.fromJS(resp.Data, context.IndexMapping);
                        ko.applyBindings(context.ViewModel, $('#mainContent')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, context.IndexMapping, context.ViewModel.Records);
                    }
                } else {
                    showMessage('Service Request Detail', resp.Message, 'error');
                }
            });
        };

        context.InitializeDetail = function () {
            context.CurrentUserServiceRequestMasterId = $("#CurrentUserServiceRequestMasterId.forKnockout").val();

            ajaxRequest(context.InitializeDetailUrl + "?id=" + context.CurrentUserServiceRequestMasterId, 'GET', {}, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#aircraftDismantleDetailsDiv')[0])) {
                        context.ViewModel.Details = ko.mapping.fromJS(resp.Data);
                        ko.applyBindings(context.ViewModel, $('#aircraftDismantleDetailsDiv')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel.Details);
                    }
                } else {
                    showMessage('Service Request Detail', resp.Message, 'error');
                }
            });
        };

    })(fia.cranePermitRequest);
});