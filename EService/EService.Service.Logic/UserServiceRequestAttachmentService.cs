using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class UserServiceRequestAttachmentService : BusinessAccessBase<UserServiceRequestAttachment>
    {
        public UserServiceRequestAttachmentService(ServiceContext context) : base(context)
        {
        }
    }
}