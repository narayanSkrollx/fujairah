﻿$(function () {
    fia.CreateNamespace("aircraftWarningLightRequest");

    (function (context) {

        context.ServiceId = fia.ServiceEnum.AircraftWarningLight;

        context.SaveRequest = function (isDraft) {
            //
            if (!$('#frmAircraftWarningLight').valid()) {
                return false;
            }
            var model = ko.toJS(context.ViewModel.AddNewModel);

            model.ApplicationDate = moment(model.ApplicationDate).isValid() ? moment(model.ApplicationDate).format('YYYY-MM-DD') : null;
            var lightModel = JSON.stringify({ model: model, isDraft: isDraft })
            var submitControl = isDraft ? 'draft' : 'save';
            ajaxRequest('/AircraftWarningLight/Create/', 'POST', {
                dataType: "JSON",
                contentType: "application/json; charset=UTF-8",
                enableLadda: true,
                data: lightModel,
                targetLaddaElement: `[data-role=${submitControl}]`,
            }, function (response) {
                if (response.IsSuccess) {
                    showMessage('Aircraft Warning Light', 'Aricraft Warning Light Service Request saved successfully.', 'success', function () {
                        window.location = '/ServiceRequest/Index?serviceId=' + context.ServiceId;
                    });
                } else {
                    showMessage('Aircraft Warning Light', response.Message, 'error', function () {
                    });
                }
            });
        }

        context.ViewModel = {
            ViewDetails: function (item) {
                window.location = context.DetailsUrl + '?id=' + item.UserServiceRequestMasterId();
            }
        };

        context.IndexMapping = {
            create: function (options) {
                var vm = ko.mapping.fromJS(options.data);

                vm.ViewDetail = function (item) {
                    console.log(ko.toJSON(item));
                }

                return vm;
            }
        }

        context.CreateMapping = {
            create: function (options) {
                var vm = ko.mapping.fromJS(options.data);

                vm.Save = function () {
                    context.SaveRequest(false)
                }

                vm.SaveDraftRequest = function () {
                    context.SaveRequest(true);
                }

                vm.SetUserInfo = function () {
                    context.SetUserInfo();
                }

                vm.RemoveUploadedFile = function (item) {
                    item.ObjectState(2);
                }

                return vm;
            }
        }

        context.SetUserInfo = function () {
            var addNewModel = context.ViewModel.AddNewModel;
            var userInfo = context.ViewModel.AddNewModel.UserInfo;
            if (addNewModel != null && userInfo != null) {
                addNewModel.ApplicantName(userInfo.FirstName() + ' ' + userInfo.LastName());
                addNewModel.ApplicantCompanyName(userInfo.CompanyName());
                addNewModel.ApplicantAddress(userInfo.Address());
                addNewModel.ApplicantCellNo(userInfo.Phone());
                addNewModel.ApplicantEmail(userInfo.Email());
            }
        }

        context.InitializeCreate = function () {
            var masterId = parseInt($("#CurrentUserServiceRequestMasterId.forKnockout").val());
            context.ViewModel.IsCreate = Number.isInteger(masterId);
            var id = context.ViewModel.IsCreate ? parseInt(masterId) : null;

            ajaxRequest('/AircraftWarningLight/InitializeCreate', 'GET', {
                data: { id: id }
            }, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#createContent')[0])) {
                        context.ViewModel = ko.mapping.fromJS(resp.Data, context.CreateMapping);

                        ko.applyBindings(context.ViewModel, $('#createContent')[0]);

                        fia.fileinputhelper.InitializeFileUpload('#kv-explorer', fia.ServiceEnum.AircraftWarningLight);
                        fia.fileinputhelper.UploadCompleteCallback = function (response) {
                            context.ViewModel.AddNewModel.UploadedFiles.push(response.Data);
                        }

                        context.ApplyValidation();
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel);
                    }
                } else {
                    showMessage('Aircraft Warning Light', resp.Message, 'error');
                    console.log(resp.Message);
                }
            });
        };

        context.ApplyValidation = function () {
            $('#frmAircraftWarningLight').validate({
                rules: {
                    ApplicantName: { required: true, maxlength: 255 },
                    ApplicantJobTitle: { required: true, maxlength: 255 },
                    ApplicantCompanyName: { required: true, maxlength: 255 },
                    ApplicantAddress: { required: true, maxlength: 255 },
                    ApplicantContactInfo: { required: true, maxlength: 255 },
                    ApplicantCellNo: { required: true, maxlength: 255, regex: phoneNoRegex },
                    ApplicantTelNo: { required: true, maxlength: 20, regex: phoneNoRegex },
                    ApplicantEmail: { required: true, maxlength: 255, email: true },
                    ApplicantWebsite: { maxlength: 255 },

                    OwnerName: { required: true, maxlength: 255 },
                    OwnerCompanyName: { required: true, maxlength: 255 },
                    OwnerAddress: { required: true, maxlength: 255 },
                    OwnerContactInfo: { required: true, maxlength: 255 },
                    OwnerCellNo: { required: true, maxlength: 255, regex: phoneNoRegex },
                    OwnerTelNo: { required: true, maxlength: 255, regex: phoneNoRegex },
                    OwnerEmail: { required: true, maxlength: 255, email: true },
                    OwnerWebsite: { maxlength: 255 },

                    ProjectName: { required: true, maxlength: 1024 },
                    NoOfStructures: { required: true, max: 10000, min: 1 },
                    NoOfObstacleLights: { required: true, max: 10000, min: 1 },
                    TypesOfObstacleLights: { required: true, maxlength: 1024 },

                    ApplicantOwnerRelationship: { required: true },
                    ProjectTypes: { required: true }
                },
                messages: {
                    ApplicantName: { required: 'Applicant Name is required', maxlength: 'Applicant Name length cannot be greater than 255' },
                    ApplicantJobTitle: { required: 'Applicant Job Title is required', maxlength: 'Applicant Job Title length cannot be greater than 255' },
                    ApplicantCompanyName: { required: 'Applicant Company Name is required.', maxlength: 'Applican Company Name length cannot be greater than 255' },
                    ApplicantAddress: { required: 'Applicant Address is required.', maxlength: 'Applicant Address length cannot be greater than 255' },
                    ApplicantContactInfo: { required: 'Applicant Contact Info is required.', maxlength: 'Applicant Contact info length cannot be greater than 255.' },
                    ApplicantCellNo: { required: 'Applicant Cell No is required.', maxlength: 'Applicant Cell No length cannot be greater than 255' },
                    ApplicantTelNo: { required: 'Applicant Tel No is required.', maxlength: 'Applicant Tel No Length cannot be greater than 20.' },
                    ApplicantEmail: { required: 'Applicant Email is required', maxlength: 'Email length cannot be greater than 255', email: 'Applicant Email must be a valid Email' },
                    ApplicantWebsite: { required: 'Applicant Website is required', maxlength: 'Applicant Website length cannot be greater than 255' },

                    OwnerName: { required: 'Owner Name is required', maxlength: 'Owner Name length cannot be greater than 255' },
                    OwnerCompanyName: { required: 'Owner Company Name is required.', maxlength: 'Applican Company Name length cannot be greater than 255' },
                    OwnerAddress: { required: 'Owner Address is required.', maxlength: 'Owner Address length cannot be greater than 255' },
                    OwnerContactInfo: { required: 'Owner Contact Info is required.', maxlength: 'Owner Contact info length cannot be greater than 255.' },
                    OwnerCellNo: { required: 'Owner Cell No is required.', maxlength: 'Owner Cell No length cannot be greater than 255' },
                    OwnerTelNo: { required: 'Owner Tel No is required.', maxlength: 'Owner Tel No Length cannot be greater than 20.' },
                    OwnerEmail: { required: 'Owner Email is required', maxlength: 'Email length cannot be greater than 255', email: 'Owner Email must be a valid Email' },
                    OwnerWebsite: { required: 'Owner Website is required', maxlength: 'Owner Website length cannot be greater than 255' },

                    ProjectName: { required: 'Project Name is required', maxlength: 'Project Name max length cannot be greater than ' },
                    NoOfStructures: { required: 'No of structure is required', max: 'No of Structures cannot be greater than 10000', min: 'No of structure cannot be less than 1' },
                    NoOfObstacleLights: { required: 'No of Obstacles light is required.', max: 'No of obstacles light cannot be greater than 10000', min: 'No of obstacles light cannot be less than 1' },
                    TypesOfObstacleLights: { required: 'Types of Obstacles light is required.', maxlength: 'Types of obstacle light length cannot be greater than 1024' },

                    ApplicantOwnerRelationship: { required: "Applicant Owner Relationship must be selected" },
                    ProjectTypes: { required: "Project type must be selected" }
                }
            });
        }

        context.InitializeIndex = function () {
            ajaxRequest('/AircraftWarningLight/Initialize', 'GET', {}, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#mainContent')[0])) {
                        context.ViewModel.Records = ko.mapping.fromJS(resp.Data, context.IndexMapping);
                        ko.applyBindings(context.ViewModel, $('#mainContent')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, context.IndexMapping, context.ViewModel.Records);
                    }
                } else {
                    showMessage('Service Request Detail', resp.Message, 'error');
                }
            });
        };

        context.InitializeDetail = function () {
            context.CurrentUserServiceRequestMasterId = $("#CurrentUserServiceRequestMasterId.forKnockout").val();

            ajaxRequest(context.InitializeDetailUrl + "?id=" + context.CurrentUserServiceRequestMasterId, 'GET', {}, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#aircraftDismantleDetailsDiv')[0])) {
                        context.ViewModel.Details = ko.mapping.fromJS(resp.Data);
                        ko.applyBindings(context.ViewModel, $('#aircraftDismantleDetailsDiv')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel.Details);
                    }
                } else {
                    showMessage('Service Request Detail', resp.Message, 'error');
                }
            });
        };

    })(fia.aircraftWarningLightRequest);
});