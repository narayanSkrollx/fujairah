using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class SMTPEmailSettingService : BusinessAccessBase<SMTPEmailSetting>
    {
        public SMTPEmailSettingService(ServiceContext context) : base(context)
        {
        }
    }
}