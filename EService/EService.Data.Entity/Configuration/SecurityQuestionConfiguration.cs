using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class SecurityQuestionConfiguration : EntityTypeConfiguration<SecurityQuestion>
    {
        public SecurityQuestionConfiguration()
        {
            ToTable("SecurityQuestion");

            HasKey(o => o.Id);

            Property(o => o.Id).HasColumnName("SecurityQuestionId")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(o => o.Description).HasColumnName("Question").IsRequired().HasMaxLength(255);
        }
    }
}