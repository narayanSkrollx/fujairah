using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class ApplicableFeesLookUpConfiguration : EntityTypeConfiguration<ApplicableFeesLookUp>
    {
        public ApplicableFeesLookUpConfiguration()
        {
            ToTable("ApplicableFeesLookUp");
            HasKey(x => x.Id);

            Property(x => x.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Description).IsRequired().HasMaxLength(225);
            Property(x => x.Active).IsRequired();
            Property(x => x.Notes).IsRequired().HasMaxLength(225);
            Property(x => x.Fees).IsRequired().HasMaxLength(225);
            Property(x => x.Fee).IsOptional();
        }
    }
}