using System;
using System.Linq;
using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class EmailService : BusinessAccessBase<SMTPEmailSetting>
    {
        public EmailService(ServiceContext context) : base(context)
        {
        }

        public string[] GetNextStageEmailRecepients(int serviceRequestFlowStageDepartmentId)
        {

            var serviceRequestFlowStageDepartment = Context.Database.ServiceRequestFlowStageDepartments.FirstOrDefault(
                 o => o.ServiceRequestFlowStageDepartmentId == serviceRequestFlowStageDepartmentId);

            var nextStage = Context.Database.ServiceRequestFlowStages.FirstOrDefault(
                  o => o.UserServiceRequestMasterId == serviceRequestFlowStageDepartment.ServiceRequestFlowStage.UserServiceRequestMasterId && o.Index == serviceRequestFlowStageDepartment.ServiceRequestFlowStage.Index + 1);
            if (nextStage != null)
            {
                var departments =
                 nextStage.ServiceRequestFlowStageDepartments.Select(
                     o => o.DepartmentId);
                var users = (from department in departments
                             join user in Context.Database.Users
                             on department equals user.DepartmentId
                             select user.Email
                    ).ToList();
                return users.ToArray();
            }
            return new string[] { };
        }

        public Tuple<string[], bool> GetPreviousStageEmailRecepients(int serviceRequestFlowStageDepartmentId)
        {

            var serviceRequestFlowStageDepartment = Context.Database.ServiceRequestFlowStageDepartments.FirstOrDefault(
                 o => o.ServiceRequestFlowStageDepartmentId == serviceRequestFlowStageDepartmentId);

            var previousStage = Context.Database.ServiceRequestFlowStages.FirstOrDefault(
                  o => o.UserServiceRequestMasterId == serviceRequestFlowStageDepartment.ServiceRequestFlowStage.UserServiceRequestMasterId && o.Index == serviceRequestFlowStageDepartment.ServiceRequestFlowStage.Index - 1);
            if (previousStage != null && previousStage.IsCurrentStage)
            {
                var departments =
                 previousStage.ServiceRequestFlowStageDepartments.Select(
                     o => o.DepartmentId);
                var users = (from department in departments
                             join user in Context.Database.Users
                             on department equals user.DepartmentId
                             select user.Email
                    ).ToList();
                return new Tuple<string[], bool>(users.ToArray(), false);
            }
            else if (previousStage == null || (previousStage != null && !previousStage.IsCurrentStage))
            {
                return new Tuple<string[], bool>(new string[] { serviceRequestFlowStageDepartment.ServiceRequestFlowStage.UserServiceRequestMaster.User.Email }, true);
            }
            return new Tuple<string[], bool>(new string[] { }, false);
        }

        public string[] GetApproveRejectEmailRecepients(int serviceRequestFlowStageDepartmentId)
        {
            var serviceRequestFlowStageDepartment =
                Context.Database.ServiceRequestFlowStageDepartments.FirstOrDefault(
                    o => o.ServiceRequestFlowStageDepartmentId == serviceRequestFlowStageDepartmentId);

            var departments =
                serviceRequestFlowStageDepartment.ServiceRequestFlowStage.ServiceRequestFlowStageDepartments.Select(
                    o => o.DepartmentId);
            var users = (from department in departments
                         join user in Context.Database.Users
                         on department equals user.DepartmentId
                         select user.Email
                ).ToList();
            //users.Add(serviceRequestFlowStageDepartment.ServiceRequestFlowStage.UserServiceRequestMaster.User.Email);
            return users.ToArray();
        }

        public string[] GetApproveRejectEmailRecepientsByUserServiceRequestId(int userServiceRequestMasterId)
        {
            var userServiceRequestMaster =
                Context.Database.UserServiceRequests.FirstOrDefault(
                    o => o.UserServiceRequestMasterId == userServiceRequestMasterId);

            var departments =
                userServiceRequestMaster.ServiceRequestFlowStages.SelectMany(
                    o => o.ServiceRequestFlowStageDepartments.Select(d => d.DepartmentId));
            var users = (from department in departments
                         join user in Context.Database.Users
                         on department equals user.DepartmentId
                         select user.Email
                ).ToList();
            //users.Add(userServiceRequestMaster.User.Email);
            return users.ToArray();
        }

        public User GetUserInfo(int userServiceRequestMasterId)
        {
            return (from request in
                 Context.Database.UserServiceRequests
                    join user in Context.Database.Users on request.UserId equals user.UserId
                    where request.UserServiceRequestMasterId == userServiceRequestMasterId
                    select user).FirstOrDefault();

            //.FirstOrDefault(
            //    o => o.UserServiceRequestMasterId == userServiceRequestMasterId);

            //return userServiceRequestMaster.User.Email;
        }
    }
}