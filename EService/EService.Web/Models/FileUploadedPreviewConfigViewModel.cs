﻿namespace EService.Web.Models
{
    public class FileUploadedPreviewConfigViewModel
    {
        public string caption { get; set; }
        public string width { get; set; }
        public string url { get; set; }
        public int key { get; set; }
    }
}