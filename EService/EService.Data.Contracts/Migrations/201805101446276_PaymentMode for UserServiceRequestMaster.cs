namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PaymentModeforUserServiceRequestMaster : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserServiceRequestMaster", "PaymentMode", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserServiceRequestMaster", "PaymentMode");
        }
    }
}
