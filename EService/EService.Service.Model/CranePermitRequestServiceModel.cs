﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EService.Service.Model
{
    public class CranePermitRequestServiceModel : EServiceBaseModal
    {
        public CranePermitRequestServiceModel()
        {
            SelectedCraneTypes = new List<CraneTypeSelectedModel>();
        }
        public int CurrentIndex { get; set; }
        #region "Service Specific  Properties"
        public string ApplicantName { get; set; }
        public DateTime ApplicationDate { get; set; }
        public string CompanyName { get; set; }
        public string Position { get; set; }
        public string ContactNumber { get; set; }
        public string ApplicantEmail { get; set; }
        public bool IsNewApplication { get; set; }
        public string PreviousPermitNumbers { get; set; }
        public string NorthingsEastings { get; set; }
        public string CraneLocation { get; set; }
        public string OperationStartTime { get; set; }
        public string OperationEndTime { get; set; }
        public DateTime OperationStartDate { get; set; }
        public DateTime OperationEndDate { get; set; }
        public int MaxOperatingHeight { get; set; }
        public string PurposeOfUser { get; set; }
        public string CraneOperatingCompany { get; set; }
        public string OperatorName { get; set; }
        public string OperatorContactNumber { get; set; }
        #endregion
        public CraneRequestPlanningModel PlanningModel { get; set; }
        public CraneRequestPlanningModel PlanningNewModel { get; set; }
        public CraneRequestFinanceModel FinanceInfoModel { get; set; }
        public CraneRequestFinanceModel FinanceInfoNewModel { get; set; }
        public bool IsPlanningInfoSaved { get; set; }
        public bool IsQASafetyInfoSaved { get; set; }
        public bool IsFinanceInfoSaved { get; set; }
        public CraneRequestQASafetyModel QASafetyModel { get; set; }
        public CraneRequestQASafetyModel QASafetyNewModel { get; set; }
        public List<CraneTypeSelectedModel> SelectedCraneTypes { get; set; }
    }

    public class CraneTypeSelectedModel
    {
        public int CraneTypeId { get; set; }
        public string CraneTypeName { get; set; }
        public bool IsOther { get; set; }
        public bool IsSelected { get; set; }
        public string OtherCraneTypeName { get; set; }
    }

    public class CraneRequestPlanningModel
    {
        public int SavedByUserId { get; set; }
        public bool HasPentrationOfSurface { get; set; }
        public decimal? PenetrationInMeter { get; set; }
        public bool RequireAviationLight { get; set; }
        public string Comments { get; set; }
        public decimal? MaxCraneOperatingHeight { get; set; }
        public decimal? CraneOperatingDistanceFromRunway { get; set; }
        public string Recommendations { get; set; }
        public DateTime SavedDateTime { get; set; }
        public string AssessedBy { get; set; }
        public string AirportPlanner { get; set; }
        public DateTime? AssessedDate { get; set; }
        public List<InfringementTypeModel> InfringementTypes { get; set; }

        public List<UploadedFile> UploadedFiles { get; set; }

    }

    public class UploadedFile
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] Content { get; set; }
    }

    public class CraneRequestFinanceModel
    {
        public bool? IsInternal { get; set; }
        public double ServiceChargeAmount { get; set; }
    }

    public class CraneRequestQASafetyModel
    {
        public bool HasSafetyAssessmentBeenDone { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedByPosition { get; set; }
        public DateTime? ApprovedDateTime { get; set; }
        public string OperatingRestrictions { get; set; }
        [Required(ErrorMessage = "Additional Comments is required.")]
        public string AdditionalComments { get; set; }
        public List<UploadedFile> UploadedFiles { get; set; }
    }

    public class InfringementTypeModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }

        public bool IsOther { get; set; }
        public string OtherName { get; set; }
    }


}