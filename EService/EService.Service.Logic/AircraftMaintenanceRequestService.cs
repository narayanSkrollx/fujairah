﻿using EService.Data.Entity.Entity;
using EService.Service.Logic.Enum;
using EService.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EService.Service.Logic
{
    public class AircraftMaintenanceRequestService : BaseEService<AircraftMaintenanceRequest>
    {
        public AircraftMaintenanceRequestService(ServiceContext context) : base(context)
        {
        }

        public override ServiceEnum Service => ServiceEnum.AircraftMaintenanceRequest;

        public ResponseObject UpdateAircraftMaintenanceRequest(AircraftMaintenanceRequest model, bool isDraft, List<UserServiceRequestAttachment> attachments)
        {
            var result = new ResponseObject();
            var contract = Find(x => x.UserServiceRequestMasterId == model.UserServiceRequestMasterId && !x.IsDeleted && x.UserServiceRequestMaster.IsDraft);
            if (contract == null)
            {
                result.IsSuccess = false;
                result.Message = "No request matched to edit.";
            }
            else
            {
                var removeDocList = contract.UserServiceRequestMaster.UserServiceRequestAttachments.ToList();
                Context.Database.UserServiceRequestAttachment.RemoveRange(removeDocList);
                attachments.ForEach(o => contract.UserServiceRequestMaster.UserServiceRequestAttachments.Add(o));
                contract.UserServiceRequestMaster.IsDraft = isDraft;
                contract.UserServiceRequestMasterId = model.UserServiceRequestMasterId;
                contract.DateOfApplication = model.DateOfApplication;
                contract.NameOfApplicant = model.NameOfApplicant;
                contract.Designation = model.Designation;
                contract.PhoneNumber = model.PhoneNumber;
                contract.EmailId = model.EmailId;
                contract.ApplicantRegisteredMaintenanceCompany = model.ApplicantRegisteredMaintenanceCompany;
                contract.AddressOfMaintenanceCompany = model.AddressOfMaintenanceCompany;
                contract.PhoneNumberOfRefisteredmaintenanceCompany = model.PhoneNumberOfRefisteredmaintenanceCompany;
                contract.MaintenanceCompanyTradeLicenseNumber = model.MaintenanceCompanyTradeLicenseNumber;
                contract.MaintenanceCompanyTradeLicenseExpiryDate = model.MaintenanceCompanyTradeLicenseExpiryDate;
                contract.Part145RegistrationNumber = model.Part145RegistrationNumber;
                contract.Part145RegistrationExpiryDate = model.Part145RegistrationExpiryDate;
                contract.AircraftType = model.AircraftType;
                contract.AircraftModel = model.AircraftModel;
                contract.AircraftRegistrationNumber = model.AircraftRegistrationNumber;
                contract.TypeOfMaintenance = model.TypeOfMaintenance;
                contract.DetailsOfMaintenance = model.DetailsOfMaintenance;
                contract.ScheduleDateOfAircraftArrival = model.ScheduleDateOfAircraftArrival;
                contract.ScheduleDateOfAircraftDeparture = model.ScheduleDateOfAircraftDeparture;
                Context.Commit();
                result.IsSuccess = true;
                result.Message = "Aircraft Maintenance Request updated successfully";
            }
            return result;
        }
    }
}
