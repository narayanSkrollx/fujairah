namespace EService.Data.Entity
{
    public interface ILookupItem
    {
        int Id { get; set; }
        string Description { get; set; }
        bool Active { get; set; }
    }
    
}