﻿using System.Collections;
using umbraco.cms.businesslogic.member;

namespace Designit.Umb.Newsletter.Bll
{
    public class Subscribe
    {
        public static void AssignMemberToGroup(Member member, string memberGroupName)
        {

            var memberGroup = MemberGroup.GetByName(memberGroupName);
            if (!member.Groups.Contains(memberGroup.Id))
            {
                member.AddGroup(memberGroup.Id);
            }

        }
    }
}
