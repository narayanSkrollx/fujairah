﻿using Umbraco.Core;
using Umbraco.Core.Persistence;


namespace FIA.CMS.Entities
{
    public class RegisterEvents : ApplicationEventHandler
    {
        //This happens everytime the Umbraco Application starts
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication,
            ApplicationContext applicationContext)
        {
            //Get the Umbraco Database context
            var db = applicationContext.DatabaseContext.Database;

            //Check if the DB table does NOT exist
            if (!db.TableExist("UserLogin"))
            {
                //Create DB table - and set overwrite to false
                db.CreateTable<UserLogin>(false);
            }
            if (!db.TableExist("UserJob"))
            {
                //Create DB table - and set overwrite to false
                db.CreateTable<UserJob>(false);
            }
            if (!db.TableExist("UserSubscription"))
            {
                //Create DB table - and set overwrite to false
                db.CreateTable<UserSubscription>(false);
            }
            if (!db.TableExist("NewsletterTemplate"))
            {
                //Create DB table - and set overwrite to false
                db.CreateTable<NewsletterTemplate>(false);
            }


        }

    }

}