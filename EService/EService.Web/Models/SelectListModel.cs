﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EService.Web.Models
{
    public class SelectListModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool IsSelected { get; set; }
    }
}