﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EService.Data.Entity;
using EService.Data.Entity.Configuration;
using EService.Data.Entity.Entity;
using EService.Core;

namespace EService.Data.Contracts
{
    public class EServiceDb : DbContext
    {
        private int _currentUserId;

        public EServiceDb() : base("app")
        {
            _currentUserId = 0;
        }
        public EServiceDb(int currentUserId = 0, string connectionString = "app")
            : base(connectionString)
        {
            _currentUserId = currentUserId;
        }

        #region "DbSets"
        public DbSet<User> Users { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<UserType> UserTypes { get; set; }
        public DbSet<UserContactPerson> UserContactPersons { get; set; }
        public DbSet<UserCompanyProfile> UserCompanyProfiles { get; set; }
        public DbSet<UserSecruityQuestion> UserSecruityQuestions { get; set; }
        public DbSet<CompanyCategory> CompanyCategories { get; set; }
        public DbSet<SecurityQuestion> SecurityQuestions { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<ServiceApprovalStage> ServiceApprovalStages { get; set; }
        public DbSet<ServiceApprovalStageDepartment> ServiceApprovalStageDepartments { get; set; }
        public DbSet<ServiceFlowStatus> ServiceFlowStatuses { get; set; }
        public DbSet<UserServiceRequestMaster> UserServiceRequests { get; set; }

        #region "Landing Permission Service Related Tables"
        public DbSet<AirportLanding> AirportLandings { get; set; }
        public DbSet<FlightNatureOfOperation> FlightNatureOfOperations { get; set; }
        public DbSet<LandingPermissionRequestAttachment> LandingPermissionRequestAttachments { get; set; }
        public DbSet<LandingPermissionRequest> LandingPermissionRequests { get; set; }
        public DbSet<LandingPermissionRequestTrip> LandingPermissionRequestTrips { get; set; }

        #endregion
        public DbSet<ErrorLog> ErrorLogs { get; set; }
        public DbSet<SMTPEmailSetting> SMTPEmailSettings { get; set; }
        public DbSet<UserAttachment> UserAttachments { get; set; }
        public DbSet<UserAttachmentData> UserAttachmentDatas { get; set; }
        public DbSet<UserServiceRequestBill> UserServiceRequestBills { get; set; }
        public DbSet<UserServiceRequestReceipt> UserServiceRequestReceipts { get; set; }
        public DbSet<UserServiceRequestReceiptDetail> UserServiceRequestReceiptDetails { get; set; }
        public DbSet<UserServiceRequestOtherPayment> UserServiceRequestOtherPayments { get; set; }

        public DbSet<ForgotPasswordRequest> ForgotPasswordRequests { get; set; }

        #endregion

        #region "Crane Permnit Related table"
        public DbSet<CraneType> CraneTypes { get; set; }
        public DbSet<CranePermitRequest> CranePermitRequests { get; set; }
        public DbSet<CranePermitRequestSelectedCrane> CranePermitRequestSelectedCranes { get; set; }
        public DbSet<CranePermitRequestUserAttachment> CranePermitRequestUserAttachments { get; set; }
        public DbSet<CranePermitRequestFirstStageEntry> CranePermitRequestFirstStageEntries { get; set; }
        public DbSet<CranePermitRequestFirstStageInfringementType> CranePermitRequestFirstStageInfringementTypes
        {
            get;
            set;
        }
        public DbSet<InfringementType> InfringementTypes { get; set; }
        public DbSet<CranePermitSafetyQAApproval> CranePermitSafetyQAApprovals { get; set; }
        public DbSet<CranePermitPlanningAttachment> CranePermitPlanningAttachments { get; set; }
        public DbSet<CranePermitSafetyFileAttachment> CranePermitSafetyFileAttachments { get; set; }
        #endregion

        #region "Aircraft Dismantle Related Tables"
        public DbSet<AircraftDismantlingNOCRequest> AircraftDismantlingNOCRequests { get; set; }
        #endregion

        #region "Aircraft Maintenance Related Tables"
        public DbSet<AircraftMaintenanceRequest> AircraftMaintenanceRequest { get; set; }
        #endregion

        #region"Service Request Flow Stages"
        public DbSet<ServiceRequestFlowStage> ServiceRequestFlowStages { get; set; }
        public DbSet<ServiceRequestFlowStageDepartment> ServiceRequestFlowStageDepartments { get; set; }
        public DbSet<ServiceRequestFlowStageDepartmentHistory> ServiceRequestFlowStageDepartmentHistories { get; set; }
        #endregion

        #region "Aircraft Warning Light Related Tables"
        public DbSet<ProjectType> ProjectTypes { get; set; }
        public DbSet<ApplicantOwnerRelationship> ApplicantOwnerRelationships { get; set; }
        public DbSet<AircraftWarningLightServiceRequest> AircraftWarningLightServiceRequests { get; set; }
        //public DbSet<AircraftWarningLightProjectType> AircraftWarningLightProjectTypes { get; set; }
        //public DbSet<AircraftWarningLightOwnerApplicantRelation> AircraftWarningLightOwnerApplicantRelations
        //{
        //    get; set;
        //}
        public DbSet<UserServiceRequestAttachment> UserServiceRequestAttachment { get; set; }
        #endregion

        #region "Trade License Request Tables"

        public DbSet<TradeLicenseRequest> TradeLicenseRequests { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<StaffStrength> StaffStrengths { get; set; }
        public DbSet<TradeLicenseSelectedStaffStrength> TradeLicenseSelectedStaffStrengths { get; set; }

        #endregion

        #region "Exit Spare Parts Request related tables"

        public DbSet<ExitOfAircraftSparePartsRequest> ExitOfAircraftSparePartsRequests { get; set; }
        public DbSet<ExitSparePartsPassChecklist> ExitSparePartsPassChecklists { get; set; }

        #endregion

        #region "Building heights related tables"

        public DbSet<AdditionalItemsForBuildingSubmittalLookUp> AdditionalItemsForBuildingSubmittalLookUps { get; set; }
        public DbSet<AdditionalItemsForOHLSubmittal> AdditionalItemsForOHLSubmittals { get; set; }
        public DbSet<AdditionalItemsForOHLSubmittalLookUp> AdditionalItemsForOHLSubmittalLookUps { get; set; }
        public DbSet<AdditionalItemsForBuildingSubmittal> AdditionalItemsForBuildingSubmittals { get; set; }
        public DbSet<ApplicableFeesLookUp> ApplicableFeesLookUps { get; set; }
        public DbSet<AviationObstacleLookUp> AviationObstacleLookUps { get; set; }
        public DbSet<BuildingCoordinateAndHeight> BuildingCoordinateAndHeights { get; set; }
        public DbSet<BuildingCoordinateAndHeightLookUp> BuildingCoordinateAndHeightLookUps { get; set; }
        public DbSet<BuildingHeightApplicableFee> BuildingHeightApplicableFees { get; set; }
        public DbSet<BuildingHeightAviationObstacle> BuildingHeightAviationObstacles { get; set; }
        public DbSet<BuildingHeightRequest> BuildingHeightRequests { get; set; }
        public DbSet<OHLStructureCoordinateAndHeight> OHLStructureCoordinateAndHeight { get; set; }
        public DbSet<BuildingHeightPlanningApproval> BuildingHeightPlanningApprovals { get; set; }
        public DbSet<BuildingHeightPlanningApprovalAttachment> BuildingHeightPlanningApprovalAttachments { get; set; }
        #endregion

        #region "Meteorological Data Request"
        public DbSet<WeatherElement> WeatherElements { get; set; }
        public DbSet<WeatherTypeReport> WeatherTypeReports { get; set; }
        public DbSet<MeteorologicalDataRequestWeatherElement> MeteorologicalDataRequestWeatherElements { get; set; }
        public DbSet<MeteorologicalDataRequestWeatherType> MeteorologicalDataRequestWeatherTypes { get; set; }
        public DbSet<MeteorologicalDataRequest> MeteorologicalDataRequests { get; set; }
        #endregion

        #region "Ground Handling Request"
        public DbSet<GroundHandlingRequest> GroundHandlingRequests { get; set; }
        public DbSet<AircraftInteriorCleaning> AircraftInteriorCleanings { get; set; }
        public DbSet<GroundHandlingRequestAircraftType> GroundHandlingRequestAircraftTypes { get; set; }
        public DbSet<AircraftType> AircraftTypes { get; set; }

        #endregion


        #region "Payment Related"
        public DbSet<PaymentKey> PaymentKeys { get; set; }
        public DbSet<UserServiceRequestPaymentResponse> PaymentResponses { get; set; }
        #endregion
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //Database.Log = (query) => Debug.Write(query);

            //modelBuilder.Types<IObjectWithState>().Configure(c => c.Ignore(p => p.ObjectState));

            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new DepartmentConfiguration());
            modelBuilder.Configurations.Add(new UserTypeConfiguration());
            modelBuilder.Configurations.Add(new UserContactPersonConfiguration());
            modelBuilder.Configurations.Add(new UserCompanyProfileConfiguration());
            modelBuilder.Configurations.Add(new SecurityQuestionConfiguration());
            modelBuilder.Configurations.Add(new UserSecurityQuestionConfiguration());
            modelBuilder.Configurations.Add(new CompanyCategoryConfiguration());
            modelBuilder.Configurations.Add(new ServiceConfiguration());
            modelBuilder.Configurations.Add(new ServiceApprovalStageConfiguration());
            modelBuilder.Configurations.Add(new ServiceApprovalStageDepartmentConfiguration());

            modelBuilder.Configurations.Add(new UserServiceRequestMasterConfiguration());

            #region "Landing permission Related Tables Configurations"

            modelBuilder.Configurations.Add(new AirportLandingConfiguration());
            modelBuilder.Configurations.Add(new FlightNaureOfOperationConfiguration());
            modelBuilder.Configurations.Add(new LandingPermissionRequestAttachmentConfiguration());
            modelBuilder.Configurations.Add(new LandingPermissionRequestConfiguration());
            modelBuilder.Configurations.Add(new LandingPermissionRequestTripConfiguration());


            #endregion

            modelBuilder.Configurations.Add(new ErrorLogConfiguration());
            modelBuilder.Configurations.Add(new SMTPEmailSettingConfiguration());
            modelBuilder.Configurations.Add(new UserAttachmentConfiguration());
            modelBuilder.Configurations.Add(new UserAttachmentDataConfiguration());

            #region "Crane Permit Related Tables"

            modelBuilder.Configurations.Add(new CraneTypeConfiguration());
            modelBuilder.Configurations.Add(new CranePermitRequestConfiguration());
            modelBuilder.Configurations.Add(new CranePermitRequestSelectedCraneConfiguration());
            modelBuilder.Configurations.Add(new CranePermitRequestUserAttachmentConfiguration());

            modelBuilder.Configurations.Add(new InfringementTypeConfiguration());
            modelBuilder.Configurations.Add(new CranePermitRequestFirstStageEntryConfiguration());
            modelBuilder.Configurations.Add(new CranePermitRequestFirstStageInfringementTypeConfiguration());
            modelBuilder.Configurations.Add(new CranePermitSafetyQAApprovalConfiguration());
            modelBuilder.Configurations.Add(new CranePermitPlanningAttachmentConfiguration());
            modelBuilder.Configurations.Add(new CranePermitSafetyFileAttachmentConfiguration());
            #endregion

            #region "Aircraft Warning Light Related TAbles"

            modelBuilder.Configurations.Add(new ProjectTypeConfiguration());
            modelBuilder.Configurations.Add(new ApplicantOwnerRelationshipConfiguration());
            //modelBuilder.Configurations.Add(new AircraftWarningLightProjectTypesConfiguration());
            //modelBuilder.Configurations.Add(new AircraftWarningLightOwnerApplicantRelationConfiguration());
            modelBuilder.Configurations.Add(new AircraftWarningLightConfiguration());
            modelBuilder.Configurations.Add(new UserServiceRequestAttachmentConfiguration());
            #endregion

            #region "Aircraft Dismantle Related Tables"
            modelBuilder.Configurations.Add(new AircraftDismantlingNOCRequestConfiguration());
            #endregion

            #region "Aircraft Maintenance Related Tables"
            modelBuilder.Configurations.Add(new AircraftMaintenanceRequestConfiguration());
            #endregion

            #region "Trade License Related Tables"

            modelBuilder.Configurations.Add(new TradeLicenseRequestContiguration());
            modelBuilder.Configurations.Add(new CurrencyConfiguration());
            modelBuilder.Configurations.Add(new StaffStrengthConfiguration());
            modelBuilder.Configurations.Add(new TradeLicenseSelectedStaffStrengthConfiguration());
            #endregion

            #region "Exit Spare Parts Request Related Tables"


            modelBuilder.Configurations.Add(new ExitOfAircraftSparePartsRequestConfiguration());
            modelBuilder.Configurations.Add(new ExitSparePartsPassChecklistConfiguration());
            #endregion

            #region "Meteorological Data Request Related Tables"
            modelBuilder.Configurations.Add(new WeatherElementConfiguration());
            modelBuilder.Configurations.Add(new WeatherTypeReportConfiguration());
            modelBuilder.Configurations.Add(new MeteorologicalDataRequestWeatherElementConfiguration());
            modelBuilder.Configurations.Add(new MeteorologicalDataRequestWeatherTypeConfiguration());
            modelBuilder.Configurations.Add(new MeteorologicalDataRequestServiceConfiguration());

            #endregion

            #region "Building heights related tables"
            modelBuilder.Configurations.Add(new AdditionalItemsForBuildingSubmittalLookUpConfiguration());
            modelBuilder.Configurations.Add(new AdditionalItemsForOHLSubmittalConfiguration());
            modelBuilder.Configurations.Add(new AdditionalItemsForOHLSubmittalLookUpConfiguration());
            modelBuilder.Configurations.Add(new AdditionalItemsForBuildingSubmittalConfiguration());
            modelBuilder.Configurations.Add(new ApplicableFeesLookUpConfiguration());
            modelBuilder.Configurations.Add(new AviationObstacleLookUpConfiguration());
            modelBuilder.Configurations.Add(new BuildingCoordinateAndHeightConfiguration());
            modelBuilder.Configurations.Add(new BuildingCoordinateAndHeightLookUpConfiguration());
            modelBuilder.Configurations.Add(new BuildingHeightApplicableFeeConfiguration());
            modelBuilder.Configurations.Add(new BuildingHeightAviationObstacleConfiguration());
            modelBuilder.Configurations.Add(new BuildingHeightRequestConfiguration());
            modelBuilder.Configurations.Add(new OHLStructureCoordinateAndHeightConfiguration());
            modelBuilder.Configurations.Add(new BuildingHeightRequestPlanningApprovalConfiguration());
            modelBuilder.Configurations.Add(new BuildinHeightRequestPlanningApprovalAttachmentConfiguration());

            #endregion


            modelBuilder.Configurations.Add(new ServiceRequestFlowStageConfiguration());
            modelBuilder.Configurations.Add(new ServiceRequestFlowStageDepartmentConfiguration());
            modelBuilder.Configurations.Add(new ServiceRequestFlowStageDepartmentHistoryConfiguration());
            modelBuilder.Configurations.Add(new ServiceFlowStatusConfiguration());
            #region "Ground handling Request"

            modelBuilder.Configurations.Add(new AircraftTypeConfiguration());
            modelBuilder.Configurations.Add(new GroundHandlingRequestAircraftTypeConfiguration());
            modelBuilder.Configurations.Add(new AircraftInteriorCleaningConfiguation());
            modelBuilder.Configurations.Add(new GroundHandlingRequestConfiguration());
            #endregion
            modelBuilder.Configurations.Add(new UserServiceRequestBillConfiguration());
            modelBuilder.Configurations.Add(new UserServiceRequestReceiptConfiguration());
            modelBuilder.Configurations.Add(new UserServiceRequestRecieptDetailConfiguration());
            modelBuilder.Configurations.Add(new UserServiceRequestOtherPaymentConfiguration());
            modelBuilder.Configurations.Add(new ForgotPasswordRequestConfguration());

            #region "Payment Related"
            modelBuilder.Configurations.Add(new PaymentKeyConfiguration());
            modelBuilder.Configurations.Add(new UserServiceRequestPaymentResponseConfiguration());
            #endregion
        }

        /// <summary>
        ///     Overrides the SaveChanges method to add the current user id to the UpdatedBy field.
        /// </summary>
        /// <returns></returns>
        public override int SaveChanges()
        {

            if (_currentUserId < 0)
            {
                throw new DataException("User not found!");
            }
            foreach (var o in GetChangedEntries())
            {
                if (o.Entity is AuditableEntity)
                {
                    var now = DateTime.Now.DubaiTime();
                    var entity = o.Entity as AuditableEntity;

                    // make sure created date and user are filled in
                    entity.CreatedDate = entity.CreatedDate == DateTime.MinValue ? now : entity.CreatedDate;
                    entity.CreatedByUserId = entity.CreatedByUserId > 0 ? entity.CreatedByUserId : _currentUserId;

                    entity.ModifiedByUserId = _currentUserId;
                    entity.ModifiedDate = now;
                }
            }
            return base.SaveChanges();
        }

        private IEnumerable<DbEntityEntry> GetChangedEntries()
        {
            return new List<DbEntityEntry>(
                from e in ChangeTracker.Entries()
                where e.State != EntityState.Unchanged
                select e);
        }
    }
}
