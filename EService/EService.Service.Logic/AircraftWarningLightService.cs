using EService.Data.Entity.Entity;
using EService.Service.Logic.Enum;

namespace EService.Service.Logic
{
    public class AircraftWarningLightService : BaseEService<AircraftWarningLightServiceRequest>
    {
        public AircraftWarningLightService(ServiceContext context) : base(context)
        {
        }

        public override ServiceEnum Service => ServiceEnum.AircraftWarningLight;
    }
}