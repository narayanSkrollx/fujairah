﻿$(function () {
    fia.CreateNamespace("serviceRequestDepartment");

    (function (context) {
        context.Title = 'Service Request';
        context.ViewModel = {}

        context.Mapping = {
            create: function (options) {
                var vm = ko.mapping.fromJS(options.data);

                vm.SearchClick = function () {
                    context.Search();
                }

                vm.ViewDetails = function(item) {
                    var jsItem = ko.toJS(item);
                    window.location = '/ServiceRequest/Detail?id=' + jsItem.UserServiceRequestMasterId;
                }

                return vm;
            }
        }

        context.Search = function () {
            var searchModel = ko.toJS(context.ViewModel.ServiceInfoVM.SearchModel);
            
            searchModel.DateFrom = moment(searchModel.DateFrom).isValid() ? moment(searchModel.DateFrom).format('YYYY-MM-DD') : null;
            searchModel.DateTo = moment(searchModel.DateTo).isValid() ? moment(searchModel.DateTo).format('YYYY-MM-DD') : null;
            delete searchModel.ServiceStatusList;
            
            ajaxRequest('/ServiceRequest/Search/', 'POST',
                {
                    data: { searchModel: searchModel },
                    enableLadda: true,
                    targetLaddaElement: '[data-role=search]'
                }, function (response) {
                    if (response.IsSuccess) {
                        ko.mapping.fromJS(response.Data, {}, context.ViewModel.ServiceInfoVM.Records);
                    } else {
                        showMessage(context.Title, response.Message, 'error');
                    }
                });
        }

        context.Initialize = function () {
            ajaxRequest('/ServiceRequest/Initialize/', 'GET', {}, function (response) {
                if (response.IsSuccess) {
                    if (!ko.dataFor($('#mainContent')[0])) {
                        context.ViewModel.ServiceInfoVM = ko.mapping.fromJS(response.Data, context.Mapping);

                        ko.applyBindings(context.ViewModel.ServiceInfoVM, $('#mainContent')[0]);
                    } else {
                        ko.mapping.fromJS(response.Data, {}, context.ViewModel.ServiceInfoVM);
                    }
                } else {
                    showMessage(context.Title, response.Message, 'error');
                }
            });
        }

        context.InitializeDetail = function () {
            ajaxRequest('/ServiceRequest/InitializeDetail/', 'GET', {}, function (response) {
                if (response.IsSuccess) {
                    $('#detailContent').html(response.Data.Html);
                    if (context.ViewModel.Detail == null) {
                        context.ViewModel.Detail = ko.mapping.fromJS(response.Data.Data);

                        ko.applyBindings(context.ViewModel.Detail, $('[data-role=navigation-up]')[0]);
                        ko.applyBindings(context.ViewModel.Detail, $('[data-role=navigation-down]')[0]);
                    } else {
                        ko.mapping.fromJS(response.Data.Data, {}, context.ViewModel.Detail);
                    }
                } else {
                    showMessage(context.Title, response.Message, 'error');
                }
            });
        }

    })(fia.serviceRequestDepartment);
});