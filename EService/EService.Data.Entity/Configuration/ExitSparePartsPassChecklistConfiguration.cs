using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class ExitSparePartsPassChecklistConfiguration : EntityTypeConfiguration<ExitSparePartsPassChecklist>
    {
        public ExitSparePartsPassChecklistConfiguration()
        {
            ToTable("ExitSparePartsPassChecklist");
            HasKey(x => x.ExitSparePartsPassChecklistId);

            Property(x => x.ExitSparePartsPassChecklistId).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.UserServiceRequestMasterId).IsRequired();
            Property(x => x.Description).IsRequired().HasMaxLength(225);
            Property(x => x.PartNumber).IsOptional().HasMaxLength(225);
            Property(x => x.NumberOfPieces).IsOptional();
            Property(x => x.Origin).IsOptional().HasMaxLength(225);
            Property(x => x.ExitToWhere).IsOptional().HasMaxLength(225);

            HasRequired(x => x.ExitOfAircraftSparePartsRequest).WithMany(x => x.ExitAndReturnPassChecklists).HasForeignKey(x => x.UserServiceRequestMasterId).WillCascadeOnDelete(false);
        }
    }
}