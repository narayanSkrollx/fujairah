﻿using System.Linq;
using EService.Data.Entity.Entity;
using EService.Service.Model;

namespace EService.Service.Logic
{
    public class ServiceApprovalStageService : BusinessAccessBase<ServiceApprovalStage>
    {
        public ServiceApprovalStageService(ServiceContext context) : base(context)
        {
        }

        public ServiceApproveModal GetServiceApprovalStage(int serviceId)
        {
            var service = Context.Database.Services.First(o => o.ServiceId == serviceId);
            var result = new ServiceApproveModal()
            {
                ServiceId = service.ServiceId,
                ServiceName = service.ServiceName,
                ServiceApprovalStages = service.ServiceApprovalStages.Where(o => !o.IsDeleted).Select(o => new ServiceApprovalStageModal()
                {
                    Index = o.Index,
                    ServiceApprovalStageDepartments = o.ServiceApprovalStageDepartments.Select(p => new ServiceApprovalStageDepartmentModal()
                    {
                        DepartmentId = p.DepartmentId,
                        DepartmentName = p.Department.DepartmentName
                    }).ToList()
                }).ToList()
            };
            return result;
        }

        public ResponseObject<Data.Entity.Entity.Service> Save(ServiceApproveModal saveModel)
        {
            var result = new ResponseObject<Data.Entity.Entity.Service>();
            var service = Context.Database.Services.FirstOrDefault(o => o.ServiceId == saveModel.ServiceId);
            if (service == null)
            {
                result.IsSuccess = false;
                result.Message = "Invalid Service information provided.";
            }
            else
            {
                //validate
                #region "Validate"

                if (saveModel.ServiceApprovalStages == null || saveModel.ServiceApprovalStages.Count < 1)
                {
                    result.IsSuccess = false;
                    result.Message = "There are no stages defined for approval.";
                    return result;
                }
                var invalidServiceStages =
                    saveModel.ServiceApprovalStages.Where(
                        o => o.ServiceApprovalStageDepartments == null || o.ServiceApprovalStageDepartments.Count < 1).ToList();
                if (invalidServiceStages.Any())
                {
                    result.IsSuccess = false;
                    result.Message = "Following Stages have no departments involved: " + string.Join(",", invalidServiceStages.Select(o => o.Index));
                    return result;
                }
                var flatDepartments = saveModel.ServiceApprovalStages.SelectMany(o => o.ServiceApprovalStageDepartments).ToList();
                var validDepartments = (from department in Context.Database.Departments.Where(o => !o.IsDeleted).AsEnumerable()
                                        join flatDepartment in flatDepartments
                                            on department.DepartmentId equals flatDepartment.DepartmentId
                                        select department
                    ).ToList();
                if (flatDepartments.Count != validDepartments.Count)
                {
                    result.IsSuccess = false;
                    var invalidDepartments = flatDepartments.Where(o => !validDepartments.Select(p => p.DepartmentId).Contains(o.DepartmentId));
                    result.Message = "There are one or more invalid departments selected to save: " + string.Join(",", invalidDepartments.Select(o => o.DepartmentName));
                    return result;
                }
                #endregion
                var serviceStageRemoveList = service.ServiceApprovalStages.ToList();
                foreach (var serviceApprovalStage in serviceStageRemoveList)
                {
                    Context.Database.ServiceApprovalStageDepartments.RemoveRange(
                        serviceApprovalStage.ServiceApprovalStageDepartments);
                    Context.Database.ServiceApprovalStages.Remove(serviceApprovalStage);
                }
                service.ServiceApprovalStages =
                    saveModel.ServiceApprovalStages.Select(o => new ServiceApprovalStage()
                    {
                        Index = o.Index,
                        ServiceId = service.ServiceId,
                        ServiceApprovalStageDepartments = o.ServiceApprovalStageDepartments.Select(p => new ServiceApprovalStageDepartment()
                        {
                            DepartmentId = p.DepartmentId
                        }).ToList()
                    }).ToList();
                Context.Commit();
                result.IsSuccess = true;
                result.Data = service;
                result.Message = "Service Approval Flow saved successfully for: " + service.ServiceName;
            }
            return result;
        }
    }
}