namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RollUp : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdditionalItemsForBuildingSubmittalLookUp",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 225),
                        Active = c.Boolean(nullable: false),
                        IsOther = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AdditionalItemsForBuildingSubmittal",
                c => new
                    {
                        AdditionalItemsForBuildingSubmittalId = c.Int(nullable: false, identity: true),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        AdditionalItemsForBuildingSubmittalLookUpId = c.Int(nullable: false),
                        OtherDescription = c.String(maxLength: 225),
                    })
                .PrimaryKey(t => t.AdditionalItemsForBuildingSubmittalId)
                .ForeignKey("dbo.AdditionalItemsForBuildingSubmittalLookUp", t => t.AdditionalItemsForBuildingSubmittalLookUpId)
                .ForeignKey("dbo.BuildingHeightRequest", t => t.UserServiceRequestMasterId, cascadeDelete: true)
                .Index(t => t.UserServiceRequestMasterId)
                .Index(t => t.AdditionalItemsForBuildingSubmittalLookUpId);
            
            CreateTable(
                "dbo.BuildingHeightRequest",
                c => new
                    {
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        ProjectName = c.String(nullable: false, maxLength: 225),
                        ApplicationDate = c.DateTime(),
                        ExpectedConstructionStartDate = c.DateTime(),
                        ExpectedConstructionEndDate = c.DateTime(),
                        Location = c.String(maxLength: 225),
                        ProjectDescription = c.String(maxLength: 225),
                        ApplicantSignature = c.String(maxLength: 225),
                        ApplicantName = c.String(maxLength: 225),
                        ApplicantJobTitle = c.String(maxLength: 225),
                        ApplicantCompanyName = c.String(maxLength: 225),
                        ApplicantAddress = c.String(maxLength: 225),
                        ApplicantCell = c.String(maxLength: 225),
                        ApplicantTel = c.String(maxLength: 225),
                        ApplicantEmail = c.String(maxLength: 225),
                        ApplicantWebsite = c.String(maxLength: 225),
                        OwnerName = c.String(maxLength: 225),
                        OwnerCompanyName = c.String(maxLength: 225),
                        OwnerAddress = c.String(maxLength: 225),
                        OwnerCell = c.String(maxLength: 225),
                        OwnerTel = c.String(maxLength: 225),
                        OwnerEmail = c.String(maxLength: 225),
                        OwnerWebsite = c.String(maxLength: 225),
                        IsBuilding = c.Boolean(),
                        IsOHLCommunicationTower = c.Boolean(),
                        IsOtherType = c.Boolean(),
                        OtherTypeDescription = c.String(maxLength: 225),
                        BuildingType = c.Boolean(),
                        NumberOfBuildings = c.Int(),
                        NumberOfFloors = c.Int(),
                        TallestHeight = c.Double(),
                        HasObstacleLight = c.Boolean(),
                        DetailsOfLightinDrawing = c.String(maxLength: 225),
                        OHLOrCommunicationType = c.Boolean(),
                        NumberOfStructure = c.Int(),
                        TypeOfOHLCommTower = c.String(maxLength: 225),
                        AreThereMountains = c.Boolean(),
                        ApplicantOwnerRelationshipId = c.Int(nullable: false),
                        ApplicantOwnerRelationshipOtherName = c.String(maxLength: 255),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.ApplicantOwnerRelationship", t => t.ApplicantOwnerRelationshipId)
                .ForeignKey("dbo.UserServiceRequestMaster", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId)
                .Index(t => t.ApplicantOwnerRelationshipId);
            
            CreateTable(
                "dbo.AdditionalItemsForOHLSubmittal",
                c => new
                    {
                        AdditionalItemsForOHLSubmittalId = c.Int(nullable: false, identity: true),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        AdditionalItemsForOHLSubmittalLookUpId = c.Int(nullable: false),
                        OtherDescription = c.String(maxLength: 225),
                    })
                .PrimaryKey(t => t.AdditionalItemsForOHLSubmittalId)
                .ForeignKey("dbo.AdditionalItemsForOHLSubmittalLookUp", t => t.AdditionalItemsForOHLSubmittalLookUpId)
                .ForeignKey("dbo.BuildingHeightRequest", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId)
                .Index(t => t.AdditionalItemsForOHLSubmittalLookUpId);
            
            CreateTable(
                "dbo.AdditionalItemsForOHLSubmittalLookUp",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 225),
                        Active = c.Boolean(nullable: false),
                        IsOther = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ApplicantOwnerRelationship",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 255),
                        Active = c.Boolean(nullable: false),
                        IsOther = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BuildingCoordinateAndHeight",
                c => new
                    {
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        BuildingCoordinateAndHeightLookUpId = c.Int(nullable: false),
                        Easting = c.String(maxLength: 225),
                        Northing = c.String(maxLength: 225),
                        GroundHeight = c.Double(),
                        StructureHeight = c.Double(),
                    })
                .PrimaryKey(t => new { t.UserServiceRequestMasterId, t.BuildingCoordinateAndHeightLookUpId })
                .ForeignKey("dbo.BuildingCoordinateAndHeightLookUp", t => t.BuildingCoordinateAndHeightLookUpId)
                .ForeignKey("dbo.BuildingHeightRequest", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId)
                .Index(t => t.BuildingCoordinateAndHeightLookUpId);
            
            CreateTable(
                "dbo.BuildingCoordinateAndHeightLookUp",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Description = c.String(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BuildingHeightApplicableFee",
                c => new
                    {
                        ApplicableFeesLookUpId = c.Int(nullable: false),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        IsSelected = c.Boolean(nullable: false),
                        Quantity = c.Double(),
                        TotalFee = c.Double(),
                    })
                .PrimaryKey(t => new { t.ApplicableFeesLookUpId, t.UserServiceRequestMasterId })
                .ForeignKey("dbo.ApplicableFeesLookUp", t => t.ApplicableFeesLookUpId)
                .ForeignKey("dbo.BuildingHeightRequest", t => t.UserServiceRequestMasterId)
                .Index(t => t.ApplicableFeesLookUpId)
                .Index(t => t.UserServiceRequestMasterId);
            
            CreateTable(
                "dbo.ApplicableFeesLookUp",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 225),
                        Active = c.Boolean(nullable: false),
                        Notes = c.String(nullable: false, maxLength: 225),
                        Fees = c.String(nullable: false, maxLength: 225),
                        Fee = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BuildingHeightAviationObstacle",
                c => new
                    {
                        BuildingHeightAviationObstacleId = c.Int(nullable: false, identity: true),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        AviationObstacleLookUpId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BuildingHeightAviationObstacleId)
                .ForeignKey("dbo.AviationObstacleLookUp", t => t.AviationObstacleLookUpId)
                .ForeignKey("dbo.BuildingHeightRequest", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId)
                .Index(t => t.AviationObstacleLookUpId);
            
            CreateTable(
                "dbo.AviationObstacleLookUp",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 225),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OHLStructureCoordinateAndHeight",
                c => new
                    {
                        OHLStructureCoordinateAndHeightId = c.Int(nullable: false, identity: true),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        StructureReferenceNumber = c.String(nullable: false, maxLength: 225),
                        Easting = c.String(nullable: false, maxLength: 225),
                        Northing = c.String(nullable: false, maxLength: 225),
                        GroundHeight = c.Double(nullable: false),
                        StructureHeight = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.OHLStructureCoordinateAndHeightId)
                .ForeignKey("dbo.BuildingHeightRequest", t => t.UserServiceRequestMasterId, cascadeDelete: true)
                .Index(t => t.UserServiceRequestMasterId);
            
            CreateTable(
                "dbo.UserServiceRequestMaster",
                c => new
                    {
                        UserServiceRequestMasterId = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        RequestDateTime = c.DateTime(nullable: false),
                        ServiceId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        RegistrationNoServiceCode = c.String(nullable: false, maxLength: 50),
                        RegistrationNoYear = c.Int(nullable: false),
                        RegistrationNoMonth = c.Int(nullable: false),
                        RegistrationNoIndex = c.Int(nullable: false),
                        ParentUserServiceRequestMasterId = c.Int(),
                        RegistrationNo = c.String(nullable: false, maxLength: 1024),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.UserServiceRequestMaster", t => t.ParentUserServiceRequestMasterId)
                .ForeignKey("dbo.Service", t => t.ServiceId)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ServiceId)
                .Index(t => t.ParentUserServiceRequestMasterId);
            
            CreateTable(
                "dbo.AircraftDismantlingNOCRequest",
                c => new
                    {
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        DateOfApplication = c.DateTime(nullable: false),
                        NameOfApplicant = c.String(nullable: false),
                        Address = c.String(),
                        PhoneNumber = c.String(maxLength: 225),
                        Email = c.String(maxLength: 225),
                        ApplicantCompanyName = c.String(maxLength: 225),
                        AircraftTypeAndModelToDismantle = c.String(maxLength: 225),
                        AircraftMSNAndRegistrationNumber = c.String(maxLength: 225),
                        PreviousOwnerOfAircraft = c.String(maxLength: 225),
                        CurrentOwnerOfAircraft = c.String(maxLength: 225),
                        OperatorOrAgentWhoBroughtTheAircraft = c.String(maxLength: 225),
                        WithNoObjectionFromTheAircraftOwner = c.Boolean(),
                        HasGCAAApproval = c.Boolean(nullable: false),
                        GCAAApprovalRefNumber = c.String(maxLength: 225),
                        CertificateOfDeRegistrationRefNumber = c.String(maxLength: 225),
                        TradeLicenseRefNumber = c.String(maxLength: 225),
                        DateOfIssueOfTradeLicense = c.DateTime(),
                        DateOfExpiryOfTradeLicense = c.DateTime(),
                        FANRLicenseRefNumber = c.String(maxLength: 225),
                        DateOfIssueOfFANRLicense = c.DateTime(),
                        DateOfExpiryOfFANRLicense = c.DateTime(),
                        FujairahEPDLicenseRefNumber = c.String(maxLength: 225),
                        DateOfIssueOfFujairahEPDLicense = c.DateTime(),
                        DateOfExpiryOfFujairahEPDLicense = c.DateTime(),
                        MethodOfDismantlingAndDestruction = c.String(maxLength: 225),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.UserServiceRequestMaster", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId);
            
            CreateTable(
                "dbo.AircraftMaintenanceRequest",
                c => new
                    {
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        DateOfApplication = c.DateTime(nullable: false),
                        NameOfApplicant = c.String(nullable: false, maxLength: 225),
                        Designation = c.String(maxLength: 225),
                        PhoneNumber = c.String(maxLength: 225),
                        EmailId = c.String(maxLength: 225),
                        ApplicantRegisteredMaintenanceCompany = c.String(maxLength: 225),
                        AddressOfMaintenanceCompany = c.String(maxLength: 225),
                        PhoneNumberOfRefisteredmaintenanceCompany = c.String(maxLength: 225),
                        MaintenanceCompanyTradeLicenseNumber = c.String(maxLength: 225),
                        MaintenanceCompanyTradeLicenseExpiryDate = c.DateTime(),
                        Part145RegistrationNumber = c.String(maxLength: 225),
                        Part145RegistrationExpiryDate = c.DateTime(),
                        AircraftType = c.String(maxLength: 225),
                        AircraftModel = c.String(maxLength: 225),
                        AircraftRegistrationNumber = c.String(maxLength: 225),
                        TypeOfMaintenance = c.Boolean(),
                        DetailsOfMaintenance = c.String(unicode: false),
                        ScheduleDateOfAircraftArrival = c.DateTime(),
                        ScheduleDateOfAircraftDeparture = c.DateTime(),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.UserServiceRequestMaster", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId);
            
            CreateTable(
                "dbo.AircraftWarningLight",
                c => new
                    {
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        ApplicationDate = c.DateTime(nullable: false),
                        ProjectName = c.String(nullable: false, maxLength: 1024),
                        ProjectLocation = c.String(),
                        ProjectDescription = c.String(),
                        ApplicantName = c.String(nullable: false, maxLength: 255),
                        ApplicantJobTitle = c.String(nullable: false, maxLength: 255),
                        ApplicantCompanyName = c.String(nullable: false, maxLength: 255),
                        ApplicantAddress = c.String(maxLength: 255),
                        ApplicantContactInfo = c.String(maxLength: 255),
                        ApplicantCellNo = c.String(maxLength: 255),
                        ApplicantTelNo = c.String(maxLength: 255),
                        ApplicantEmail = c.String(maxLength: 255),
                        ApplicantWebsite = c.String(maxLength: 255),
                        OwnerName = c.String(nullable: false, maxLength: 255),
                        OwnerCompanyName = c.String(nullable: false, maxLength: 255),
                        OwnerAddress = c.String(maxLength: 255),
                        OwnerContactInfo = c.String(maxLength: 255),
                        OwnerCellNo = c.String(maxLength: 255),
                        OwnerTelNo = c.String(maxLength: 255),
                        OwnerEmail = c.String(maxLength: 255),
                        OwnerWebsite = c.String(maxLength: 255),
                        NoOfStructures = c.Int(nullable: false),
                        NoOfObstacleLights = c.Int(nullable: false),
                        TypesOfObstacleLights = c.String(maxLength: 1024),
                        ProjectTypeId = c.Int(nullable: false),
                        ProjectTypeOtherName = c.String(maxLength: 1024),
                        ApplicantOwnerRelationShipId = c.Int(nullable: false),
                        ApplicantOwnerRelationshipOtherName = c.String(maxLength: 1024),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.ApplicantOwnerRelationship", t => t.ApplicantOwnerRelationShipId)
                .ForeignKey("dbo.ProjectType", t => t.ProjectTypeId)
                .ForeignKey("dbo.UserServiceRequestMaster", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId)
                .Index(t => t.ProjectTypeId)
                .Index(t => t.ApplicantOwnerRelationShipId);
            
            CreateTable(
                "dbo.ProjectType",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 255),
                        Active = c.Boolean(nullable: false),
                        IsOther = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CranePermitRequest",
                c => new
                    {
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        ApplicantName = c.String(nullable: false),
                        ApplicationDate = c.DateTime(nullable: false),
                        CompanyName = c.String(nullable: false, maxLength: 1024),
                        Position = c.String(maxLength: 255),
                        ContactNumber = c.String(nullable: false, maxLength: 50),
                        ApplicantEmail = c.String(nullable: false, maxLength: 255),
                        IsNewApplication = c.Boolean(nullable: false),
                        PreviousPermitNumbers = c.String(maxLength: 1024),
                        NorthingsEastings = c.String(maxLength: 1024),
                        CraneLocation = c.String(maxLength: 1024),
                        OperationStartDate = c.DateTime(nullable: false),
                        OperationEndDate = c.DateTime(nullable: false),
                        OperationStartTime = c.String(nullable: false, maxLength: 255),
                        OperationEndTime = c.String(nullable: false, maxLength: 255),
                        MaxOperatingHeight = c.Int(nullable: false),
                        PurposeOfUser = c.String(maxLength: 255),
                        CraneOperatingCompany = c.String(nullable: false, maxLength: 255),
                        OperatorName = c.String(nullable: false, maxLength: 255),
                        OperatorContactNumber = c.String(nullable: false, maxLength: 255),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.UserServiceRequestMaster", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId);
            
            CreateTable(
                "dbo.CranePermitRequestFirstStageEntry",
                c => new
                    {
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        SavedByUserId = c.Int(nullable: false),
                        HasPentrationOfSurface = c.Boolean(nullable: false),
                        PenetrationInMeter = c.Decimal(precision: 18, scale: 2),
                        RequireAviationLight = c.Boolean(nullable: false),
                        Comments = c.String(maxLength: 1024),
                        MaxCraneOperatingHeight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CraneOperatingDistanceFromRunway = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Recommendations = c.String(maxLength: 1024),
                        SavedDateTime = c.DateTime(nullable: false),
                        AssessedBy = c.String(maxLength: 1024),
                        AirportPlanner = c.String(maxLength: 1024),
                        AssessedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.CranePermitRequest", t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.User", t => t.SavedByUserId)
                .Index(t => t.UserServiceRequestMasterId)
                .Index(t => t.SavedByUserId);
            
            CreateTable(
                "dbo.CranePermitRequestFirstStageInfringementType",
                c => new
                    {
                        CranePermitRequestFirstStageInfringementTypeId = c.Int(nullable: false, identity: true),
                        InfringementTypeId = c.Int(nullable: false),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        OtherName = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.CranePermitRequestFirstStageInfringementTypeId)
                .ForeignKey("dbo.CranePermitRequestFirstStageEntry", t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.InfringementType", t => t.InfringementTypeId)
                .Index(t => t.InfringementTypeId)
                .Index(t => t.UserServiceRequestMasterId);
            
            CreateTable(
                "dbo.InfringementType",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 255),
                        Active = c.Boolean(nullable: false),
                        IsOther = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false, maxLength: 100),
                        FirstName = c.String(nullable: false, maxLength: 1024),
                        LastName = c.String(nullable: false, maxLength: 1024),
                        Password = c.String(nullable: false, maxLength: 100),
                        Email = c.String(nullable: false, maxLength: 1024),
                        UserTypeId = c.Int(nullable: false),
                        DepartmentId = c.Int(),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Department", t => t.DepartmentId)
                .ForeignKey("dbo.UserType", t => t.UserTypeId, cascadeDelete: true)
                .Index(t => t.UserName, unique: true)
                .Index(t => t.UserTypeId)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.Department",
                c => new
                    {
                        DepartmentId = c.Int(nullable: false, identity: true),
                        DepartmentName = c.String(nullable: false, maxLength: 100),
                        IsDefault = c.Boolean(nullable: false),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.DepartmentId);
            
            CreateTable(
                "dbo.ServiceApprovalStageDepartment",
                c => new
                    {
                        ServiceApprovalStageDepartmentId = c.Int(nullable: false, identity: true),
                        ServiceApprovalStageId = c.Int(nullable: false),
                        DepartmentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ServiceApprovalStageDepartmentId)
                .ForeignKey("dbo.Department", t => t.DepartmentId, cascadeDelete: true)
                .ForeignKey("dbo.ServiceApprovalStage", t => t.ServiceApprovalStageId, cascadeDelete: true)
                .Index(t => t.ServiceApprovalStageId)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.ServiceApprovalStage",
                c => new
                    {
                        ServiceApprovalStageId = c.Int(nullable: false, identity: true),
                        ServiceId = c.Int(nullable: false),
                        Index = c.Int(nullable: false),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ServiceApprovalStageId)
                .ForeignKey("dbo.Service", t => t.ServiceId, cascadeDelete: true)
                .Index(t => new { t.ServiceId, t.Index }, unique: true, name: "IX_ServiceApprovalStageUnique");
            
            CreateTable(
                "dbo.Service",
                c => new
                    {
                        ServiceId = c.Int(nullable: false),
                        ServiceName = c.String(nullable: false, maxLength: 255),
                        IsActive = c.Boolean(nullable: false),
                        Description = c.String(maxLength: 1000),
                        IsDefault = c.Boolean(nullable: false),
                        ServiceCode = c.String(nullable: false, maxLength: 10),
                        DefaultRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ServiceId)
                .Index(t => t.ServiceName, unique: true, name: "IX_ServiceNameUnique");
            
            CreateTable(
                "dbo.UserAttachment",
                c => new
                    {
                        UserAttachmentId = c.Int(nullable: false, identity: true),
                        Timestamp = c.DateTime(nullable: false),
                        UserId = c.Int(nullable: false),
                        ServiceId = c.Int(nullable: false),
                        FileName = c.String(nullable: false, maxLength: 255),
                        ContentType = c.String(nullable: false, maxLength: 255),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserAttachmentId)
                .ForeignKey("dbo.Service", t => t.ServiceId)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ServiceId);
            
            CreateTable(
                "dbo.CranePermitRequestUserAttachmentConfiguration",
                c => new
                    {
                        CranePermitRequestUserAttachmentId = c.Int(nullable: false, identity: true),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        UserAttachmentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CranePermitRequestUserAttachmentId)
                .ForeignKey("dbo.CranePermitRequest", t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.UserAttachment", t => t.UserAttachmentId)
                .Index(t => t.UserServiceRequestMasterId)
                .Index(t => t.UserAttachmentId);
            
            CreateTable(
                "dbo.LandingPermissionRequestAttachment",
                c => new
                    {
                        LandingPermissionRequestAttachmentId = c.Int(nullable: false, identity: true),
                        LandingPermissionRequestId = c.Int(nullable: false),
                        UserAttachmentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.LandingPermissionRequestAttachmentId)
                .ForeignKey("dbo.LandingPermissionRequest", t => t.LandingPermissionRequestId)
                .ForeignKey("dbo.UserAttachment", t => t.UserAttachmentId)
                .Index(t => t.LandingPermissionRequestId)
                .Index(t => t.UserAttachmentId);
            
            CreateTable(
                "dbo.LandingPermissionRequest",
                c => new
                    {
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        AirlineName = c.String(nullable: false, maxLength: 1024),
                        Email = c.String(nullable: false, maxLength: 1024),
                        Telephone = c.String(nullable: false, maxLength: 1024),
                        Fac = c.String(),
                        Address = c.String(maxLength: 1024),
                        OperatorName = c.String(nullable: false, maxLength: 1024),
                        OperatorEmail = c.String(nullable: false, maxLength: 1024),
                        OperatorTelephone = c.String(nullable: false, maxLength: 1024),
                        Fax = c.String(maxLength: 1024),
                        OperatorAddress = c.String(maxLength: 1024),
                        FlightNatureOfOperationId = c.Int(nullable: false),
                        AircraftRegistration = c.String(nullable: false, maxLength: 1024),
                        TypeOfAircraft = c.String(nullable: false, maxLength: 1024),
                        AirportLandingId = c.Int(nullable: false),
                        MTOW = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remarks = c.String(),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.AirportLanding", t => t.AirportLandingId)
                .ForeignKey("dbo.FlightNatureOfOperation", t => t.FlightNatureOfOperationId)
                .ForeignKey("dbo.UserServiceRequestMaster", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId)
                .Index(t => t.FlightNatureOfOperationId)
                .Index(t => t.AirportLandingId);
            
            CreateTable(
                "dbo.AirportLanding",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 1024),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FlightNatureOfOperation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 1024),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LandingPermissionRequestTrip",
                c => new
                    {
                        LandingPermissionRequestTripId = c.Int(nullable: false, identity: true),
                        LandingPermissionRequestId = c.Int(nullable: false),
                        CrewDetail = c.String(nullable: false),
                        NoOfPassengers = c.Int(nullable: false),
                        ETDOrigin = c.DateTime(nullable: false),
                        ETADestination = c.DateTime(nullable: false),
                        IsManualEntry = c.Boolean(nullable: false),
                        TripActionCode = c.String(nullable: false, maxLength: 100),
                        TripArrivalDestinationCode = c.String(nullable: false, maxLength: 100),
                        TripDepartureDestinationCode = c.String(nullable: false, maxLength: 100),
                        StartOfPeriod = c.String(nullable: false, maxLength: 100),
                        EndOfPeriod = c.String(nullable: false, maxLength: 100),
                        WeekdaysOfOperation = c.Int(nullable: false),
                        NoOfSeats = c.Int(nullable: false),
                        AircraftTypeCode = c.String(nullable: false, maxLength: 100),
                        OriginStationCode = c.String(nullable: false, maxLength: 100),
                        ArrivalTimeUTC = c.String(nullable: false, maxLength: 100),
                        DepartureTimeUTC = c.String(nullable: false, maxLength: 100),
                        DestinationStationCode = c.String(nullable: false, maxLength: 100),
                        ArrivalServiceType = c.String(nullable: false, maxLength: 100),
                        DepartureServiceType = c.String(nullable: false, maxLength: 100),
                        CompleteRouting = c.String(nullable: false, maxLength: 1024),
                    })
                .PrimaryKey(t => t.LandingPermissionRequestTripId)
                .ForeignKey("dbo.LandingPermissionRequest", t => t.LandingPermissionRequestId)
                .Index(t => t.LandingPermissionRequestId);
            
            CreateTable(
                "dbo.UserAttachmentData",
                c => new
                    {
                        UserAttachmentId = c.Int(nullable: false),
                        FileContents = c.Binary(nullable: false),
                    })
                .PrimaryKey(t => t.UserAttachmentId)
                .ForeignKey("dbo.UserAttachment", t => t.UserAttachmentId)
                .Index(t => t.UserAttachmentId);
            
            CreateTable(
                "dbo.UserServiceRequestAttachment",
                c => new
                    {
                        UserServiceRequestAttachmentId = c.Int(nullable: false, identity: true),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        UserAttachmentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserServiceRequestAttachmentId)
                .ForeignKey("dbo.UserAttachment", t => t.UserAttachmentId, cascadeDelete: true)
                .ForeignKey("dbo.UserServiceRequestMaster", t => t.UserServiceRequestMasterId, cascadeDelete: true)
                .Index(t => t.UserServiceRequestMasterId)
                .Index(t => t.UserAttachmentId);
            
            CreateTable(
                "dbo.ServiceRequestFlowStageDepartment",
                c => new
                    {
                        ServiceRequestFlowStageDepartmentId = c.Int(nullable: false, identity: true),
                        ServiceRequestFlowStageId = c.Int(nullable: false),
                        DepartmentId = c.Int(nullable: false),
                        DepartmentUserId = c.Int(),
                        Status = c.Int(nullable: false),
                        StatusChangedDateTime = c.DateTime(),
                        IsApproved = c.Boolean(),
                        IsOverriddenFromAdmin = c.Boolean(nullable: false),
                        Remarks = c.String(),
                    })
                .PrimaryKey(t => t.ServiceRequestFlowStageDepartmentId)
                .ForeignKey("dbo.Department", t => t.DepartmentId, cascadeDelete: true)
                .ForeignKey("dbo.ServiceRequestFlowStage", t => t.ServiceRequestFlowStageId, cascadeDelete: true)
                .Index(t => t.ServiceRequestFlowStageId)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.ServiceRequestFlowStage",
                c => new
                    {
                        ServiceRequestFlowStageId = c.Int(nullable: false, identity: true),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        Index = c.Int(nullable: false),
                        IsCurrentStage = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ServiceRequestFlowStageId)
                .ForeignKey("dbo.UserServiceRequestMaster", t => t.UserServiceRequestMasterId, cascadeDelete: true)
                .Index(t => t.UserServiceRequestMasterId);
            
            CreateTable(
                "dbo.ServiceRequestFlowStageDepartmentHistory",
                c => new
                    {
                        ServiceRequestFlowStageDepartmentHistoryId = c.Int(nullable: false, identity: true),
                        ServiceRequestFlowStageDepartmentId = c.Int(nullable: false),
                        ServiceRequestFlowStageId = c.Int(nullable: false),
                        DepartmentId = c.Int(nullable: false),
                        DepartmentName = c.String(nullable: false, maxLength: 255),
                        DepartmentUserId = c.Int(),
                        DepartmentUserName = c.String(maxLength: 255),
                        Status = c.Int(nullable: false),
                        StatusChangedDateTime = c.DateTime(),
                        IsApproved = c.Boolean(),
                        Remarks = c.String(),
                    })
                .PrimaryKey(t => t.ServiceRequestFlowStageDepartmentHistoryId)
                .ForeignKey("dbo.ServiceRequestFlowStageDepartment", t => t.ServiceRequestFlowStageDepartmentId, cascadeDelete: true)
                .Index(t => t.ServiceRequestFlowStageDepartmentId);
            
            CreateTable(
                "dbo.UserCompanyProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        CompanyCategoryId = c.Int(nullable: false),
                        CompanyName = c.String(maxLength: 255),
                        CompanyAddress = c.String(maxLength: 255),
                        Phone = c.String(maxLength: 100),
                        Fax = c.String(maxLength: 100),
                        POBox = c.String(maxLength: 100),
                        City = c.String(maxLength: 255),
                        Country = c.String(maxLength: 255),
                        VATRegistrationNumber = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.CompanyCategory", t => t.CompanyCategoryId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.CompanyCategoryId);
            
            CreateTable(
                "dbo.CompanyCategory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserContactPerson",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        ContactPersonName = c.String(maxLength: 255),
                        ContactPersonMobile = c.Long(),
                        EmiratesId = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserSecurityQuestion",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        SecurityQuestionId = c.Int(nullable: false),
                        Answer = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.SecurityQuestion", t => t.SecurityQuestionId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.SecurityQuestionId);
            
            CreateTable(
                "dbo.SecurityQuestion",
                c => new
                    {
                        SecurityQuestionId = c.Int(nullable: false, identity: true),
                        Question = c.String(nullable: false, maxLength: 255),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SecurityQuestionId);
            
            CreateTable(
                "dbo.UserType",
                c => new
                    {
                        UserTypeId = c.Int(nullable: false),
                        UserTypeName = c.String(nullable: false, maxLength: 100),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserTypeId);
            
            CreateTable(
                "dbo.CranePermitRequestSelectedCrane",
                c => new
                    {
                        CranePermitRequestSelectedCraneId = c.Int(nullable: false, identity: true),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        CraneTypeId = c.Int(nullable: false),
                        OtherCraneType = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.CranePermitRequestSelectedCraneId)
                .ForeignKey("dbo.CranePermitRequest", t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.CraneType", t => t.CraneTypeId)
                .Index(t => t.UserServiceRequestMasterId)
                .Index(t => t.CraneTypeId);
            
            CreateTable(
                "dbo.CraneType",
                c => new
                    {
                        CraneTypeId = c.Int(nullable: false),
                        CraneTypeName = c.String(nullable: false, maxLength: 255),
                        IsOther = c.Boolean(nullable: false),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CraneTypeId);
            
            CreateTable(
                "dbo.CranePermitSafetyQAApproval",
                c => new
                    {
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        HasSafetyAssessmentBeenDone = c.Boolean(nullable: false),
                        ApprovedBy = c.String(nullable: false, maxLength: 255),
                        ApprovedByPosition = c.String(nullable: false, maxLength: 255),
                        ApprovedDateTime = c.DateTime(nullable: false),
                        OperatingRestrictions = c.String(nullable: false, maxLength: 1024),
                        AdditionalComments = c.String(nullable: false, maxLength: 1024),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.CranePermitRequest", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId);
            
            CreateTable(
                "dbo.ExitOfAircraftSparePartsRequest",
                c => new
                    {
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        DateOfApplication = c.DateTime(nullable: false),
                        ApplicantCompany = c.String(maxLength: 225),
                        AircraftType = c.String(maxLength: 225),
                        AircraftRegistrationNumber = c.String(maxLength: 225),
                        OwnerOfAircraft = c.String(maxLength: 225),
                        NoObjectionLetterOfTheOwner = c.String(maxLength: 225),
                        AircraftMaintenanceOrganisationOrCar145GCAARegisteredCo = c.Boolean(),
                        ApplicantMailIdOrContactDetails = c.String(maxLength: 225),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.UserServiceRequestMaster", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId);
            
            CreateTable(
                "dbo.ExitSparePartsPassChecklist",
                c => new
                    {
                        ExitSparePartsPassChecklistId = c.Int(nullable: false, identity: true),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 225),
                        PartNumber = c.String(maxLength: 225),
                        NumberOfPieces = c.Int(),
                        Origin = c.String(maxLength: 225),
                        ExitToWhere = c.String(maxLength: 225),
                    })
                .PrimaryKey(t => t.ExitSparePartsPassChecklistId)
                .ForeignKey("dbo.ExitOfAircraftSparePartsRequest", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId);
            
            CreateTable(
                "dbo.GroundHandlingRequest",
                c => new
                    {
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        InboundDateTimeUTC = c.DateTime(nullable: false),
                        From = c.String(nullable: false, maxLength: 1024),
                        ETAUTC = c.DateTime(nullable: false),
                        OutboundDateTimeUTC = c.DateTime(nullable: false),
                        To = c.String(),
                        ETDUTC = c.DateTime(nullable: false),
                        Weight = c.Double(),
                        AircraftRegistration = c.String(),
                        RequestWBCalculation = c.Boolean(),
                        LoadingInstruction = c.Boolean(),
                        METOrATSDelivery = c.Boolean(),
                        NoOfDepartingPassengers = c.Int(),
                        NoOfArrivingPassengers = c.Int(),
                        PermittedToAddGood = c.Boolean(),
                        HasSpecialPermission = c.Boolean(),
                        CargoOffLoadingType = c.Boolean(),
                        CoLNoOfULD = c.Int(),
                        CoLULDWeight = c.Double(),
                        CoLTypeOfDG = c.String(maxLength: 1024),
                        CoLQuantityOfDG = c.Double(),
                        CoLOtherSpecialCargo = c.String(maxLength: 1024),
                        CargoLoadingType = c.Boolean(),
                        CLNoOfULD = c.Int(),
                        CLULDWeight = c.Int(),
                        CLTypeOfDG = c.String(maxLength: 1024),
                        CLQuantityDG = c.Double(),
                        CLDGWeight = c.Double(),
                        CLOtherSpecialCargo = c.String(maxLength: 1024),
                        CompanyName = c.String(nullable: false, maxLength: 1024),
                        FirstName = c.String(nullable: false, maxLength: 1024),
                        LastName = c.String(nullable: false, maxLength: 1024),
                        Address = c.String(nullable: false, maxLength: 1024),
                        Email = c.String(nullable: false, maxLength: 1024),
                        Phone = c.String(nullable: false, maxLength: 1024),
                        Fax = c.String(maxLength: 1024),
                        SITATEX = c.String(maxLength: 1024),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.UserServiceRequestMaster", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId);
            
            CreateTable(
                "dbo.AircraftInteriorCleaning",
                c => new
                    {
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        FumigationRequired = c.Boolean(nullable: false),
                        WasingRequired = c.Boolean(nullable: false),
                        PowerType = c.String(nullable: false, maxLength: 255),
                        Pushback = c.Boolean(nullable: false),
                        PassengerStairs = c.Boolean(nullable: false),
                        ToiletService = c.Boolean(nullable: false),
                        PortableWater = c.Boolean(nullable: false),
                        ASUHours = c.String(),
                        AdditionalPlatformCRJ = c.Boolean(nullable: false),
                        SecuritySurveillanceForRAmpHours = c.Boolean(nullable: false),
                        HotelBooking = c.Boolean(nullable: false),
                        StandbyFireFighting = c.Boolean(nullable: false),
                        AdditionalManpower = c.Boolean(nullable: false),
                        AdditionalRequirements = c.String(maxLength: 1024),
                    })
                .PrimaryKey(t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.GroundHandlingRequest", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId);
            
            CreateTable(
                "dbo.GroundHandlingRequestAircraftType",
                c => new
                    {
                        GroundHandlingRequestAircraftTypeId = c.Int(nullable: false, identity: true),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        AircraftTypeId = c.Int(nullable: false),
                        GroundHandlingRequest_UserServiceRequestMasterId = c.Int(),
                    })
                .PrimaryKey(t => t.GroundHandlingRequestAircraftTypeId)
                .ForeignKey("dbo.AircraftType", t => t.AircraftTypeId)
                .ForeignKey("dbo.GroundHandlingRequest", t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.GroundHandlingRequest", t => t.GroundHandlingRequest_UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId)
                .Index(t => t.AircraftTypeId)
                .Index(t => t.GroundHandlingRequest_UserServiceRequestMasterId);
            
            CreateTable(
                "dbo.AircraftType",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 255),
                        Active = c.Boolean(nullable: false),
                        IsOther = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MeteorologicalDataRequestService",
                c => new
                    {
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        Email = c.String(nullable: false, maxLength: 1024),
                        Department = c.String(maxLength: 1024),
                        Phone = c.String(nullable: false, maxLength: 50),
                        Fax = c.String(),
                        Address = c.String(maxLength: 1024),
                        DataRangeDaily = c.Boolean(nullable: false),
                        DataRangeMonthly = c.Boolean(nullable: false),
                        DataRangeYearly = c.Boolean(nullable: false),
                        DataRangeSelectPeriod = c.Boolean(nullable: false),
                        CustomRangeStartDate = c.DateTime(),
                        CustomRangeEndDate = c.DateTime(),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.UserServiceRequestMaster", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId);
            
            CreateTable(
                "dbo.MeteorologicalDataRequestWeatherElement",
                c => new
                    {
                        MeteorologicalDataRequestWeatherElementId = c.Int(nullable: false, identity: true),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        WeatherElementId = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        OtherName = c.String(maxLength: 1024),
                    })
                .PrimaryKey(t => t.MeteorologicalDataRequestWeatherElementId)
                .ForeignKey("dbo.MeteorologicalDataRequestService", t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.WeatherElement", t => t.WeatherElementId)
                .Index(t => t.UserServiceRequestMasterId)
                .Index(t => t.WeatherElementId);
            
            CreateTable(
                "dbo.WeatherElement",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 1024),
                        Active = c.Boolean(nullable: false),
                        IsOther = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MeteorologicalDataRequestWeatherType",
                c => new
                    {
                        MeteorologicalDataRequestWeatherTypeid = c.Int(nullable: false, identity: true),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        WeatherTypeReportId = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.MeteorologicalDataRequestWeatherTypeid)
                .ForeignKey("dbo.MeteorologicalDataRequestService", t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.WeatherTypeReport", t => t.WeatherTypeReportId)
                .Index(t => t.UserServiceRequestMasterId)
                .Index(t => t.WeatherTypeReportId);
            
            CreateTable(
                "dbo.WeatherTypeReport",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 1024),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserServiceRequestBill",
                c => new
                    {
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        ChargedAmount = c.Double(nullable: false),
                        DiscountAmount = c.Double(),
                        PaymentMode = c.Int(),
                        PaidAmount = c.Double(),
                        PaidByUserId = c.Int(),
                        PaidDateTime = c.DateTime(),
                        PaidByUserName = c.String(),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.UserServiceRequestMaster", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId);
            
            CreateTable(
                "dbo.TradeLicenseRequest",
                c => new
                    {
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        ApplicantName = c.String(nullable: false),
                        PresentSponsorInUAE = c.String(maxLength: 225),
                        AddressOfSponser = c.String(maxLength: 225),
                        ApplicantOfficeNumber = c.String(maxLength: 225),
                        ApplicantResidenceNumber = c.String(maxLength: 225),
                        ApplicantMobileNumber = c.String(maxLength: 225),
                        ApplicantFaxNumber = c.String(maxLength: 225),
                        ApplicantEmailNumber = c.String(maxLength: 225),
                        ApplicantCountry = c.String(maxLength: 225),
                        MailingAddress = c.String(maxLength: 225),
                        ApplicationDate = c.DateTime(),
                        CompanyPreferredName = c.String(maxLength: 225),
                        CompanyNameOption1 = c.String(maxLength: 225),
                        CompanyNameOption2 = c.String(maxLength: 225),
                        CompanyNameOption3 = c.String(maxLength: 225),
                        BusinessActivities1 = c.String(maxLength: 225),
                        BusinessActivities2 = c.String(maxLength: 225),
                        BusinessActivities3 = c.String(maxLength: 225),
                        Remarks = c.String(maxLength: 225),
                        ExpectedAnnualBusinessTrunoverAmount = c.Double(),
                        ExpectedAnnualBusinessTrunoverCurrencyId = c.Int(),
                        TotalStaffStrengthRequired = c.String(maxLength: 225),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.Currency", t => t.ExpectedAnnualBusinessTrunoverCurrencyId)
                .ForeignKey("dbo.UserServiceRequestMaster", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId)
                .Index(t => t.ExpectedAnnualBusinessTrunoverCurrencyId);
            
            CreateTable(
                "dbo.Currency",
                c => new
                    {
                        CurrencyId = c.Int(nullable: false, identity: true),
                        CurrencyName = c.String(nullable: false, maxLength: 225),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CurrencyId);
            
            CreateTable(
                "dbo.TradeLicenseSelectedStaffStrength",
                c => new
                    {
                        TradeLicenseSelectedStaffStrengthId = c.Int(nullable: false, identity: true),
                        TradeLicenseRequestId = c.Int(nullable: false),
                        StaffStrengthId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TradeLicenseSelectedStaffStrengthId)
                .ForeignKey("dbo.StaffStrength", t => t.StaffStrengthId)
                .ForeignKey("dbo.TradeLicenseRequest", t => t.TradeLicenseRequestId)
                .Index(t => t.TradeLicenseRequestId)
                .Index(t => t.StaffStrengthId);
            
            CreateTable(
                "dbo.StaffStrength",
                c => new
                    {
                        StaffStrengthId = c.Int(nullable: false, identity: true),
                        StaffStrengthName = c.String(nullable: false, maxLength: 225),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.StaffStrengthId);
            
            CreateTable(
                "dbo.ErrorLog",
                c => new
                    {
                        ErrorLogId = c.Int(nullable: false, identity: true),
                        Timestamp = c.DateTime(nullable: false),
                        File = c.String(),
                        Method = c.String(),
                        Exception = c.String(),
                        InnerException = c.String(),
                        StackTrace = c.String(),
                        Source = c.String(),
                        Data = c.String(),
                        ClientIp = c.String(),
                        ClientBrowserAgent = c.String(),
                        UserId = c.Int(),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.ErrorLogId);
            
            CreateTable(
                "dbo.ForgotPasswordRequest",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        Email = c.String(nullable: false, maxLength: 1024),
                        Token = c.Guid(nullable: false),
                        ExpiryTime = c.DateTime(nullable: false),
                        IsUsed = c.Boolean(nullable: false),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ServiceFlowStatus",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Description = c.String(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SMTPEmailSetting",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Host = c.String(nullable: false),
                        From = c.String(),
                        Port = c.Int(nullable: false),
                        UserName = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ForgotPasswordRequest", "UserId", "dbo.User");
            DropForeignKey("dbo.AdditionalItemsForBuildingSubmittal", "UserServiceRequestMasterId", "dbo.BuildingHeightRequest");
            DropForeignKey("dbo.BuildingHeightRequest", "UserServiceRequestMasterId", "dbo.UserServiceRequestMaster");
            DropForeignKey("dbo.UserServiceRequestMaster", "UserId", "dbo.User");
            DropForeignKey("dbo.TradeLicenseRequest", "UserServiceRequestMasterId", "dbo.UserServiceRequestMaster");
            DropForeignKey("dbo.TradeLicenseSelectedStaffStrength", "TradeLicenseRequestId", "dbo.TradeLicenseRequest");
            DropForeignKey("dbo.TradeLicenseSelectedStaffStrength", "StaffStrengthId", "dbo.StaffStrength");
            DropForeignKey("dbo.TradeLicenseRequest", "ExpectedAnnualBusinessTrunoverCurrencyId", "dbo.Currency");
            DropForeignKey("dbo.UserServiceRequestBill", "UserServiceRequestMasterId", "dbo.UserServiceRequestMaster");
            DropForeignKey("dbo.UserServiceRequestMaster", "ServiceId", "dbo.Service");
            DropForeignKey("dbo.UserServiceRequestMaster", "ParentUserServiceRequestMasterId", "dbo.UserServiceRequestMaster");
            DropForeignKey("dbo.MeteorologicalDataRequestService", "UserServiceRequestMasterId", "dbo.UserServiceRequestMaster");
            DropForeignKey("dbo.MeteorologicalDataRequestWeatherType", "WeatherTypeReportId", "dbo.WeatherTypeReport");
            DropForeignKey("dbo.MeteorologicalDataRequestWeatherType", "UserServiceRequestMasterId", "dbo.MeteorologicalDataRequestService");
            DropForeignKey("dbo.MeteorologicalDataRequestWeatherElement", "WeatherElementId", "dbo.WeatherElement");
            DropForeignKey("dbo.MeteorologicalDataRequestWeatherElement", "UserServiceRequestMasterId", "dbo.MeteorologicalDataRequestService");
            DropForeignKey("dbo.GroundHandlingRequest", "UserServiceRequestMasterId", "dbo.UserServiceRequestMaster");
            DropForeignKey("dbo.GroundHandlingRequestAircraftType", "GroundHandlingRequest_UserServiceRequestMasterId", "dbo.GroundHandlingRequest");
            DropForeignKey("dbo.GroundHandlingRequestAircraftType", "UserServiceRequestMasterId", "dbo.GroundHandlingRequest");
            DropForeignKey("dbo.GroundHandlingRequestAircraftType", "AircraftTypeId", "dbo.AircraftType");
            DropForeignKey("dbo.AircraftInteriorCleaning", "UserServiceRequestMasterId", "dbo.GroundHandlingRequest");
            DropForeignKey("dbo.ExitOfAircraftSparePartsRequest", "UserServiceRequestMasterId", "dbo.UserServiceRequestMaster");
            DropForeignKey("dbo.ExitSparePartsPassChecklist", "UserServiceRequestMasterId", "dbo.ExitOfAircraftSparePartsRequest");
            DropForeignKey("dbo.CranePermitRequest", "UserServiceRequestMasterId", "dbo.UserServiceRequestMaster");
            DropForeignKey("dbo.CranePermitSafetyQAApproval", "UserServiceRequestMasterId", "dbo.CranePermitRequest");
            DropForeignKey("dbo.CranePermitRequestSelectedCrane", "CraneTypeId", "dbo.CraneType");
            DropForeignKey("dbo.CranePermitRequestSelectedCrane", "UserServiceRequestMasterId", "dbo.CranePermitRequest");
            DropForeignKey("dbo.CranePermitRequestFirstStageEntry", "SavedByUserId", "dbo.User");
            DropForeignKey("dbo.User", "UserTypeId", "dbo.UserType");
            DropForeignKey("dbo.UserSecurityQuestion", "UserId", "dbo.User");
            DropForeignKey("dbo.UserSecurityQuestion", "SecurityQuestionId", "dbo.SecurityQuestion");
            DropForeignKey("dbo.UserContactPerson", "UserId", "dbo.User");
            DropForeignKey("dbo.UserCompanyProfile", "UserId", "dbo.User");
            DropForeignKey("dbo.UserCompanyProfile", "CompanyCategoryId", "dbo.CompanyCategory");
            DropForeignKey("dbo.User", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.ServiceRequestFlowStageDepartmentHistory", "ServiceRequestFlowStageDepartmentId", "dbo.ServiceRequestFlowStageDepartment");
            DropForeignKey("dbo.ServiceRequestFlowStageDepartment", "ServiceRequestFlowStageId", "dbo.ServiceRequestFlowStage");
            DropForeignKey("dbo.ServiceRequestFlowStage", "UserServiceRequestMasterId", "dbo.UserServiceRequestMaster");
            DropForeignKey("dbo.ServiceRequestFlowStageDepartment", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.ServiceApprovalStageDepartment", "ServiceApprovalStageId", "dbo.ServiceApprovalStage");
            DropForeignKey("dbo.ServiceApprovalStage", "ServiceId", "dbo.Service");
            DropForeignKey("dbo.UserServiceRequestAttachment", "UserServiceRequestMasterId", "dbo.UserServiceRequestMaster");
            DropForeignKey("dbo.UserServiceRequestAttachment", "UserAttachmentId", "dbo.UserAttachment");
            DropForeignKey("dbo.UserAttachmentData", "UserAttachmentId", "dbo.UserAttachment");
            DropForeignKey("dbo.UserAttachment", "UserId", "dbo.User");
            DropForeignKey("dbo.UserAttachment", "ServiceId", "dbo.Service");
            DropForeignKey("dbo.LandingPermissionRequestAttachment", "UserAttachmentId", "dbo.UserAttachment");
            DropForeignKey("dbo.LandingPermissionRequestAttachment", "LandingPermissionRequestId", "dbo.LandingPermissionRequest");
            DropForeignKey("dbo.LandingPermissionRequest", "UserServiceRequestMasterId", "dbo.UserServiceRequestMaster");
            DropForeignKey("dbo.LandingPermissionRequestTrip", "LandingPermissionRequestId", "dbo.LandingPermissionRequest");
            DropForeignKey("dbo.LandingPermissionRequest", "FlightNatureOfOperationId", "dbo.FlightNatureOfOperation");
            DropForeignKey("dbo.LandingPermissionRequest", "AirportLandingId", "dbo.AirportLanding");
            DropForeignKey("dbo.CranePermitRequestUserAttachmentConfiguration", "UserAttachmentId", "dbo.UserAttachment");
            DropForeignKey("dbo.CranePermitRequestUserAttachmentConfiguration", "UserServiceRequestMasterId", "dbo.CranePermitRequest");
            DropForeignKey("dbo.ServiceApprovalStageDepartment", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.CranePermitRequestFirstStageInfringementType", "InfringementTypeId", "dbo.InfringementType");
            DropForeignKey("dbo.CranePermitRequestFirstStageInfringementType", "UserServiceRequestMasterId", "dbo.CranePermitRequestFirstStageEntry");
            DropForeignKey("dbo.CranePermitRequestFirstStageEntry", "UserServiceRequestMasterId", "dbo.CranePermitRequest");
            DropForeignKey("dbo.AircraftWarningLight", "UserServiceRequestMasterId", "dbo.UserServiceRequestMaster");
            DropForeignKey("dbo.AircraftWarningLight", "ProjectTypeId", "dbo.ProjectType");
            DropForeignKey("dbo.AircraftWarningLight", "ApplicantOwnerRelationShipId", "dbo.ApplicantOwnerRelationship");
            DropForeignKey("dbo.AircraftMaintenanceRequest", "UserServiceRequestMasterId", "dbo.UserServiceRequestMaster");
            DropForeignKey("dbo.AircraftDismantlingNOCRequest", "UserServiceRequestMasterId", "dbo.UserServiceRequestMaster");
            DropForeignKey("dbo.OHLStructureCoordinateAndHeight", "UserServiceRequestMasterId", "dbo.BuildingHeightRequest");
            DropForeignKey("dbo.BuildingHeightAviationObstacle", "UserServiceRequestMasterId", "dbo.BuildingHeightRequest");
            DropForeignKey("dbo.BuildingHeightAviationObstacle", "AviationObstacleLookUpId", "dbo.AviationObstacleLookUp");
            DropForeignKey("dbo.BuildingHeightApplicableFee", "UserServiceRequestMasterId", "dbo.BuildingHeightRequest");
            DropForeignKey("dbo.BuildingHeightApplicableFee", "ApplicableFeesLookUpId", "dbo.ApplicableFeesLookUp");
            DropForeignKey("dbo.BuildingCoordinateAndHeight", "UserServiceRequestMasterId", "dbo.BuildingHeightRequest");
            DropForeignKey("dbo.BuildingCoordinateAndHeight", "BuildingCoordinateAndHeightLookUpId", "dbo.BuildingCoordinateAndHeightLookUp");
            DropForeignKey("dbo.BuildingHeightRequest", "ApplicantOwnerRelationshipId", "dbo.ApplicantOwnerRelationship");
            DropForeignKey("dbo.AdditionalItemsForOHLSubmittal", "UserServiceRequestMasterId", "dbo.BuildingHeightRequest");
            DropForeignKey("dbo.AdditionalItemsForOHLSubmittal", "AdditionalItemsForOHLSubmittalLookUpId", "dbo.AdditionalItemsForOHLSubmittalLookUp");
            DropForeignKey("dbo.AdditionalItemsForBuildingSubmittal", "AdditionalItemsForBuildingSubmittalLookUpId", "dbo.AdditionalItemsForBuildingSubmittalLookUp");
            DropIndex("dbo.ForgotPasswordRequest", new[] { "UserId" });
            DropIndex("dbo.TradeLicenseSelectedStaffStrength", new[] { "StaffStrengthId" });
            DropIndex("dbo.TradeLicenseSelectedStaffStrength", new[] { "TradeLicenseRequestId" });
            DropIndex("dbo.TradeLicenseRequest", new[] { "ExpectedAnnualBusinessTrunoverCurrencyId" });
            DropIndex("dbo.TradeLicenseRequest", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.UserServiceRequestBill", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.MeteorologicalDataRequestWeatherType", new[] { "WeatherTypeReportId" });
            DropIndex("dbo.MeteorologicalDataRequestWeatherType", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.MeteorologicalDataRequestWeatherElement", new[] { "WeatherElementId" });
            DropIndex("dbo.MeteorologicalDataRequestWeatherElement", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.MeteorologicalDataRequestService", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.GroundHandlingRequestAircraftType", new[] { "GroundHandlingRequest_UserServiceRequestMasterId" });
            DropIndex("dbo.GroundHandlingRequestAircraftType", new[] { "AircraftTypeId" });
            DropIndex("dbo.GroundHandlingRequestAircraftType", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.AircraftInteriorCleaning", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.GroundHandlingRequest", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.ExitSparePartsPassChecklist", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.ExitOfAircraftSparePartsRequest", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.CranePermitSafetyQAApproval", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.CranePermitRequestSelectedCrane", new[] { "CraneTypeId" });
            DropIndex("dbo.CranePermitRequestSelectedCrane", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.UserSecurityQuestion", new[] { "SecurityQuestionId" });
            DropIndex("dbo.UserSecurityQuestion", new[] { "UserId" });
            DropIndex("dbo.UserContactPerson", new[] { "UserId" });
            DropIndex("dbo.UserCompanyProfile", new[] { "CompanyCategoryId" });
            DropIndex("dbo.UserCompanyProfile", new[] { "UserId" });
            DropIndex("dbo.ServiceRequestFlowStageDepartmentHistory", new[] { "ServiceRequestFlowStageDepartmentId" });
            DropIndex("dbo.ServiceRequestFlowStage", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.ServiceRequestFlowStageDepartment", new[] { "DepartmentId" });
            DropIndex("dbo.ServiceRequestFlowStageDepartment", new[] { "ServiceRequestFlowStageId" });
            DropIndex("dbo.UserServiceRequestAttachment", new[] { "UserAttachmentId" });
            DropIndex("dbo.UserServiceRequestAttachment", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.UserAttachmentData", new[] { "UserAttachmentId" });
            DropIndex("dbo.LandingPermissionRequestTrip", new[] { "LandingPermissionRequestId" });
            DropIndex("dbo.LandingPermissionRequest", new[] { "AirportLandingId" });
            DropIndex("dbo.LandingPermissionRequest", new[] { "FlightNatureOfOperationId" });
            DropIndex("dbo.LandingPermissionRequest", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.LandingPermissionRequestAttachment", new[] { "UserAttachmentId" });
            DropIndex("dbo.LandingPermissionRequestAttachment", new[] { "LandingPermissionRequestId" });
            DropIndex("dbo.CranePermitRequestUserAttachmentConfiguration", new[] { "UserAttachmentId" });
            DropIndex("dbo.CranePermitRequestUserAttachmentConfiguration", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.UserAttachment", new[] { "ServiceId" });
            DropIndex("dbo.UserAttachment", new[] { "UserId" });
            DropIndex("dbo.Service", "IX_ServiceNameUnique");
            DropIndex("dbo.ServiceApprovalStage", "IX_ServiceApprovalStageUnique");
            DropIndex("dbo.ServiceApprovalStageDepartment", new[] { "DepartmentId" });
            DropIndex("dbo.ServiceApprovalStageDepartment", new[] { "ServiceApprovalStageId" });
            DropIndex("dbo.User", new[] { "DepartmentId" });
            DropIndex("dbo.User", new[] { "UserTypeId" });
            DropIndex("dbo.User", new[] { "UserName" });
            DropIndex("dbo.CranePermitRequestFirstStageInfringementType", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.CranePermitRequestFirstStageInfringementType", new[] { "InfringementTypeId" });
            DropIndex("dbo.CranePermitRequestFirstStageEntry", new[] { "SavedByUserId" });
            DropIndex("dbo.CranePermitRequestFirstStageEntry", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.CranePermitRequest", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.AircraftWarningLight", new[] { "ApplicantOwnerRelationShipId" });
            DropIndex("dbo.AircraftWarningLight", new[] { "ProjectTypeId" });
            DropIndex("dbo.AircraftWarningLight", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.AircraftMaintenanceRequest", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.AircraftDismantlingNOCRequest", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.UserServiceRequestMaster", new[] { "ParentUserServiceRequestMasterId" });
            DropIndex("dbo.UserServiceRequestMaster", new[] { "ServiceId" });
            DropIndex("dbo.UserServiceRequestMaster", new[] { "UserId" });
            DropIndex("dbo.OHLStructureCoordinateAndHeight", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.BuildingHeightAviationObstacle", new[] { "AviationObstacleLookUpId" });
            DropIndex("dbo.BuildingHeightAviationObstacle", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.BuildingHeightApplicableFee", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.BuildingHeightApplicableFee", new[] { "ApplicableFeesLookUpId" });
            DropIndex("dbo.BuildingCoordinateAndHeight", new[] { "BuildingCoordinateAndHeightLookUpId" });
            DropIndex("dbo.BuildingCoordinateAndHeight", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.AdditionalItemsForOHLSubmittal", new[] { "AdditionalItemsForOHLSubmittalLookUpId" });
            DropIndex("dbo.AdditionalItemsForOHLSubmittal", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.BuildingHeightRequest", new[] { "ApplicantOwnerRelationshipId" });
            DropIndex("dbo.BuildingHeightRequest", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.AdditionalItemsForBuildingSubmittal", new[] { "AdditionalItemsForBuildingSubmittalLookUpId" });
            DropIndex("dbo.AdditionalItemsForBuildingSubmittal", new[] { "UserServiceRequestMasterId" });
            DropTable("dbo.SMTPEmailSetting");
            DropTable("dbo.ServiceFlowStatus");
            DropTable("dbo.ForgotPasswordRequest");
            DropTable("dbo.ErrorLog");
            DropTable("dbo.StaffStrength");
            DropTable("dbo.TradeLicenseSelectedStaffStrength");
            DropTable("dbo.Currency");
            DropTable("dbo.TradeLicenseRequest");
            DropTable("dbo.UserServiceRequestBill");
            DropTable("dbo.WeatherTypeReport");
            DropTable("dbo.MeteorologicalDataRequestWeatherType");
            DropTable("dbo.WeatherElement");
            DropTable("dbo.MeteorologicalDataRequestWeatherElement");
            DropTable("dbo.MeteorologicalDataRequestService");
            DropTable("dbo.AircraftType");
            DropTable("dbo.GroundHandlingRequestAircraftType");
            DropTable("dbo.AircraftInteriorCleaning");
            DropTable("dbo.GroundHandlingRequest");
            DropTable("dbo.ExitSparePartsPassChecklist");
            DropTable("dbo.ExitOfAircraftSparePartsRequest");
            DropTable("dbo.CranePermitSafetyQAApproval");
            DropTable("dbo.CraneType");
            DropTable("dbo.CranePermitRequestSelectedCrane");
            DropTable("dbo.UserType");
            DropTable("dbo.SecurityQuestion");
            DropTable("dbo.UserSecurityQuestion");
            DropTable("dbo.UserContactPerson");
            DropTable("dbo.CompanyCategory");
            DropTable("dbo.UserCompanyProfile");
            DropTable("dbo.ServiceRequestFlowStageDepartmentHistory");
            DropTable("dbo.ServiceRequestFlowStage");
            DropTable("dbo.ServiceRequestFlowStageDepartment");
            DropTable("dbo.UserServiceRequestAttachment");
            DropTable("dbo.UserAttachmentData");
            DropTable("dbo.LandingPermissionRequestTrip");
            DropTable("dbo.FlightNatureOfOperation");
            DropTable("dbo.AirportLanding");
            DropTable("dbo.LandingPermissionRequest");
            DropTable("dbo.LandingPermissionRequestAttachment");
            DropTable("dbo.CranePermitRequestUserAttachmentConfiguration");
            DropTable("dbo.UserAttachment");
            DropTable("dbo.Service");
            DropTable("dbo.ServiceApprovalStage");
            DropTable("dbo.ServiceApprovalStageDepartment");
            DropTable("dbo.Department");
            DropTable("dbo.User");
            DropTable("dbo.InfringementType");
            DropTable("dbo.CranePermitRequestFirstStageInfringementType");
            DropTable("dbo.CranePermitRequestFirstStageEntry");
            DropTable("dbo.CranePermitRequest");
            DropTable("dbo.ProjectType");
            DropTable("dbo.AircraftWarningLight");
            DropTable("dbo.AircraftMaintenanceRequest");
            DropTable("dbo.AircraftDismantlingNOCRequest");
            DropTable("dbo.UserServiceRequestMaster");
            DropTable("dbo.OHLStructureCoordinateAndHeight");
            DropTable("dbo.AviationObstacleLookUp");
            DropTable("dbo.BuildingHeightAviationObstacle");
            DropTable("dbo.ApplicableFeesLookUp");
            DropTable("dbo.BuildingHeightApplicableFee");
            DropTable("dbo.BuildingCoordinateAndHeightLookUp");
            DropTable("dbo.BuildingCoordinateAndHeight");
            DropTable("dbo.ApplicantOwnerRelationship");
            DropTable("dbo.AdditionalItemsForOHLSubmittalLookUp");
            DropTable("dbo.AdditionalItemsForOHLSubmittal");
            DropTable("dbo.BuildingHeightRequest");
            DropTable("dbo.AdditionalItemsForBuildingSubmittal");
            DropTable("dbo.AdditionalItemsForBuildingSubmittalLookUp");
        }
    }
}
