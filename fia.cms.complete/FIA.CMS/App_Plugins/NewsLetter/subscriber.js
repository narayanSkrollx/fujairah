﻿angular.module("umbraco").controller("newsletter.subscriber",
    function ($scope, $http, notificationsService) {
        $scope.list = [];
       

        $scope.GetSubscriber = function () {
            $http.get("/umbraco/surface/Newsletter/GetSubscriber").then(function(result) {
                if (result && result.data) {
                    $scope.list = result.data;
                 
                }
            });
        }
        $scope.GetSubscriber();
    });