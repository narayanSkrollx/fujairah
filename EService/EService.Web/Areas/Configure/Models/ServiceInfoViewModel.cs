﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using EService.Service.Model;

namespace EService.Web.Areas.Configure.Models
{
    public class ServiceInfoAddViewModel : ServiceInfoServiceModel
    {
        public int? ServiceId { get; set; }
        [Required(ErrorMessage = "Service Name is required.")]
        [StringLength(255, ErrorMessage = "Service Name length cannot be greater than 255.")]
        public string ServiceName { get; set; }
        [StringLength(1000, ErrorMessage = "Description Length cannot be greater than 1000.")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Service Code is required,")]
        [StringLength(10, ErrorMessage = "Service Code Length cannot be greater than 10.")]
        public string ServiceCode { get; set; }
        [Required(ErrorMessage = "Default Rate is required.")]
        public decimal? DefaultRate { get; set; }
        [Required(ErrorMessage = "VAT Percentage is required.")]
        public decimal? VATPercentage { get; set; }
    }
}