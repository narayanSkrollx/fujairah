using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Service.Logic.Enum;
using EService.Web.Controllers.Base;
using EService.Web.Helper;
using EService.Web.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using EService.Web.Models.ServiceRequestOperation;
using Nelibur.ObjectMapper;
using EService.Service.Model;
using System.Collections.Generic;
using EService.Core;
using EService.Web.Helper.EmailSender;

namespace EService.Web.Controllers
{
    public class MeteorologicalDataController : ServiceRequestBaseController
    {
        private LookupService lookupService;
        private MeteorologicalDataRequestService meteorologicalDataRequestService;
        private UserServiceRequestMasterService userServiceRequestMasterService;
        private WeatherElementService weatherElementService;
        private ServiceInfoService serviceInfoService;
        private UserService userService;

        public MeteorologicalDataController()
        {
            lookupService = new LookupService();
            userServiceRequestMasterService = new UserServiceRequestMasterService(serviceContext);
            meteorologicalDataRequestService = new MeteorologicalDataRequestService(serviceContext);
            weatherElementService = new WeatherElementService(serviceContext);
            serviceInfoService = new ServiceInfoService(serviceContext);
            userService = new UserService(serviceContext);
        }

        public override ServiceEnum Service => ServiceEnum.MeteorologicalDataRequest;

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(MeteorologicalDataRequestViewModel model, bool isDraft = false)
        {
            var result = new JsonResponseObject();
            try
            {
                if (!ModelState.IsValid)
                {
                    result.IsSuccess = false;
                    result.Message = ModelState.GetModelStateError();
                }
                else
                {
                    if (model.UserServiceRequestMasterId > 0)
                    {
                        var contract = MapToMeteorologicalDataRequest(model);
                        var attachmentList = new List<UserServiceRequestAttachment>();
                        #region "Uploaded Files Edit"
                        if (model.EditUploadedFiles != null && model.EditUploadedFiles.Any())
                        {
                            attachmentList.AddRange(model.EditUploadedFiles.Where(o => o.ObjectState != ObjectState.Deleted).Select(o => new UserServiceRequestAttachment { UserAttachmentId = o.Id }));

                        }
                        if (model.UploadedFiles != null && model.UploadedFiles.Any())
                        {
                            attachmentList.AddRange(model.UploadedFiles.Select(o => new UserServiceRequestAttachment { UserAttachmentId = o.Id }));

                        }

                        #endregion
                        ResponseObject updateResult = meteorologicalDataRequestService.Update(contract, UserProfile.UserId, isDraft, attachmentList);

                        result.IsSuccess = updateResult.IsSuccess;
                        result.Message = updateResult.Message;

                        if (!isDraft && updateResult.IsSuccess)
                        {
                            //TriggerEmail(FIAEmailType.New, model.UserServiceRequestMasterId);
                            EmailSenderHelper.SendNewRequestEmail(ControllerContext, model.UserServiceRequestMasterId);
                        }
                    }
                    else
                    {
                        var serviceFlowStages =
                           ServiceApprovalStageService.FindAllAsNoTracking(o => o.ServiceId == ServiceId).ToList();
                        var serviceModal = userServiceRequestMasterService.GetNewServiceCode(ServiceId);
                        var serviceInfo = serviceInfoService.Find(o => o.ServiceId == ServiceId);
                        var userServiceRequest = new UserServiceRequestMaster()
                        {
                            RequestDateTime = DateTime.Now.DubaiTime(),
                            ServiceId = ServiceId,
                            Status = (int)ServiceFlowStateEnum.New,
                            UserId = UserProfile.UserId,
                            IsDeleted = false,
                            CreatedByUserId = UserProfile.UserId,
                            CreatedDate = DateTime.Now.DubaiTime(),
                            IsDraft = isDraft,
                            VAT = serviceInfo.VATPercentage,
                            ServiceRequestFlowStages = serviceFlowStages
                            .Select(o => new ServiceRequestFlowStage()
                            {
                                Index = o.Index,
                                IsCurrentStage = o.Index == 1,
                                ServiceRequestFlowStageDepartments = o.ServiceApprovalStageDepartments.Select(p =>
                                new ServiceRequestFlowStageDepartment()
                                {
                                    DepartmentId = p.DepartmentId,
                                    Status = (int)ServiceFlowStateEnum.New,
                                    IsApproved = false,
                                }).ToList()
                            }).ToList(),
                            UserServiceRequestAttachments = model.UploadedFiles.Select(o => new UserServiceRequestAttachment()
                            {
                                UserAttachmentId = o.Id
                            }).ToList(),
                            RegistrationNoYear = serviceModal.Data.Year,
                            RegistrationNoMonth = serviceModal.Data.Month,
                            RegistrationNoIndex = serviceModal.Data.RegistrationNoIndex,
                            RegistrationNoServiceCode = serviceModal.Data.ServiceCode,
                            RegistrationNo = serviceModal.Data.RegistrationNo,
                            ModifiedByUserId = UserProfile.UserId,
                            ModifiedDate = DateTime.Now.DubaiTime(),
                            ParentUserServiceRequestMasterId = model.ParentUserServiceRequestMasterid,
                            ServieBill = new UserServiceRequestBill()
                            {
                                ChargedAmount = (double)serviceInfo.DefaultRate
                            }
                        };

                        var metRequest = new MeteorologicalDataRequest()
                        {
                            UserServiceRequestMaster = userServiceRequest,
                            Address = model.Address,
                            CreatedByUserId = UserProfile.UserId,
                            CreatedDate = DateTime.Now.DubaiTime(),
                            CustomRangeEndDate = model.CustomRangeEndDate,
                            CustomRangeStartDate = model.CustomRangeStartDate,
                            DataRangeDaily = model.DataRangeDaily,
                            DataRangeMonthly = model.DataRangeMonthly,
                            DataRangeHourly = model.DataRangeHourly,
                            DataRangeSelectPeriod = model.DataRangeSelectPeriod,
                            DataRangeYearly = model.DataRangeDaily,
                            Department = model.Department,
                            Email = model.Email,
                            Fax = model.Fax,
                            IsDeleted = false,
                            Phone = model.Phone,
                            MeteorologicalDataRequestWeatherElements = model.WeatherElements.Select(o => new MeteorologicalDataRequestWeatherElement()
                            {
                                IsActive = o.IsActive,
                                OtherName = o.OtherName,
                                WeatherElementId = o.WeatherElementId
                            }).ToList(),
                            MeteorologicalDataRequestWeatherTypes = model.WeatherTypes.Select(o => new MeteorologicalDataRequestWeatherType()
                            {
                                IsActive = o.IsActive,
                                WeatherTypeReportId = o.WeatherTypeReportId
                            }).ToList()
                        };

                        meteorologicalDataRequestService.Add(metRequest);
                        //cranePermitRequestService.Add(cranePermitRequest);
                        //aircraftWarningLightService.Add(aircraftWarningLight);
                        serviceContext.Commit();
                        if (!isDraft)
                        {
                            ////TriggerEmail(FIAEmailType.New, userServiceRequest.UserServiceRequestMasterId);
                            EmailSenderHelper.SendNewRequestEmail(ControllerContext, userServiceRequest.UserServiceRequestMasterId);
                        }
                        result.IsSuccess = true;
                        result.Message = "";
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving meteorological data request.";
            }
            return Json(result);
        }

        public ActionResult Detail(int id)
        {
            ViewBag.UserServiceRequestId = id;
            return View();
        }

        public ActionResult Edit(int id)
        {
            ViewBag.UserServiceRequestMasterId = id;
            return View("Create");
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Initialize()
        {
            var result = new JsonResponseObject();
            try
            {
                var data = userServiceRequestMasterService.GetServiceRequests(UserProfile.UserId, ServiceId);
                result.Data = new { Records = data };
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while obtaining list";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InitializeCreate(int? id)
        {
            var result = new JsonResponseObject();

            try
            {
                var weatherTypes = lookupService.GetList<WeatherTypeReport>().Select(o => new MetWeatherTypeReportViewModel()
                {
                    IsActive = false,
                    WeatherTypeReportId = o.Id,
                    WeatherTypeDescription = o.Description
                }).ToList();
                var weatherElements = weatherElementService.FindAllAsNoTracking(o => o.Active)
                    .AsEnumerable()
                    .Select(o => new MetWeatherElementReportViewModel()
                    {
                        IsOther = o.IsOther,
                        OtherName = "",
                        IsActive = false,
                        WeatherElementId = o.Id,
                        WeatherElementDescription = o.Description
                    }).ToList();
                var model = new MeteorologicalDataRequestViewModel()
                {
                    ApplicationDate = DateTime.Now.DubaiTime(),
                    WeatherElements = weatherElements.ToList(),
                    WeatherTypes = weatherTypes.ToList()
                };

                var reapplyData = TempData[Constants.ServiceRequestReapplyContentTempDataKey] as ServiceReapplyTempDataViewModel;
                if (reapplyData != null || id.HasValue)
                {
                    var editId = reapplyData?.UserServiceRequestMasterId ?? id.Value;
                    var userServiceRequest =
                        userServiceRequestMasterService.Find(
                            o => o.UserServiceRequestMasterId == editId &&
                                 !o.IsDeleted && o.UserId == UserProfile.UserId && o.ServiceId == ServiceId);

                    if (userServiceRequest == null || userServiceRequest.MeteorologicalDataRequest == null)
                    {
                        return Json(new JsonResponseObject() { IsSuccess = false, Message = "Invalid User Service Request for reapply." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var metRequest = userServiceRequest.MeteorologicalDataRequest;


                        TinyMapper.Bind<MeteorologicalDataRequest, MeteorologicalDataRequestViewModel>();

                        var metRequestViewModel = TinyMapper.Map<MeteorologicalDataRequestViewModel>(metRequest);

                        metRequestViewModel.ParentUserServiceRequestMasterid = reapplyData?.UserServiceRequestMasterId;
                        metRequestViewModel.UserServiceRequestMasterId = reapplyData != null ? 0 : id ?? 0;

                        metRequestViewModel.WeatherTypes = (from weatherType in weatherTypes
                                                            join savedMetType in metRequest.MeteorologicalDataRequestWeatherTypes
                                                            on weatherType.WeatherTypeReportId equals savedMetType.WeatherTypeReportId
                                                            into leftJoinWeatherType
                                                            from defaultWeatherType in leftJoinWeatherType.DefaultIfEmpty()
                                                            select new MetWeatherTypeReportViewModel
                                                            {
                                                                WeatherTypeReportId = weatherType.WeatherTypeReportId,
                                                                WeatherTypeDescription = weatherType.WeatherTypeDescription,
                                                                IsActive = defaultWeatherType != null && defaultWeatherType.IsActive

                                                            }).ToList();

                        metRequestViewModel.WeatherElements = (from weatherElement in weatherElements
                                                               join savedWeatherElement in metRequest.MeteorologicalDataRequestWeatherElements
                                                               on weatherElement.WeatherElementId equals savedWeatherElement.WeatherElementId
                                                               into leftJoinWeatherElement
                                                               from defaultWeatherElement in leftJoinWeatherElement.DefaultIfEmpty()
                                                               select new MetWeatherElementReportViewModel
                                                               {
                                                                   WeatherElementId = weatherElement.WeatherElementId,
                                                                   IsActive = defaultWeatherElement != null && defaultWeatherElement.IsActive,
                                                                   OtherName = defaultWeatherElement?.OtherName ?? "",
                                                                   IsOther = weatherElement.IsOther,
                                                                   WeatherElementDescription = weatherElement.WeatherElementDescription
                                                               }).ToList();

                        model = metRequestViewModel;

                    }
                }
                model.UserInfo = userService.GetUserBasicInfo(UserProfile.UserId).Data;
                model.EditUploadedFiles = GetUploadedFiles(reapplyData?.UserServiceRequestMasterId ?? (id ?? 0));

                result.IsSuccess = true;
                result.Data = new { AddNewModel = model };
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while obtaining List.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InitializeDetail(int id)
        {
            var result = new JsonResponseObject();

            try
            {
                var data = meteorologicalDataRequestService.Find(o => o.UserServiceRequestMasterId == id);
                var model = new MeteorologicalDataRequestViewModel()
                {
                    ApplicationDate = data.UserServiceRequestMaster.RequestDateTime,
                    Address = data.Address,
                    CustomRangeEndDate = data.CustomRangeEndDate,
                    CustomRangeStartDate = data.CustomRangeStartDate,
                    DataRangeDaily = data.DataRangeDaily,
                    DataRangeMonthly = data.DataRangeMonthly,
                    DataRangeYearly = data.DataRangeYearly,
                    DataRangeSelectPeriod = data.DataRangeSelectPeriod,
                    Department = data.Department,
                    Email = data.Email,
                    Phone = data.Phone,
                    Fax = data.Fax,
                    UserServiceRequestMasterId = data.UserServiceRequestMasterId,
                    WeatherElements = data.MeteorologicalDataRequestWeatherElements.Select(o => new MetWeatherElementReportViewModel()
                    {
                        IsOther = o.WeatherElement.IsOther,
                        OtherName = o.OtherName,
                        IsActive = o.IsActive,
                        WeatherElementId = o.WeatherElementId,
                        WeatherElementDescription = o.WeatherElement.Description
                    }).ToList(),
                    WeatherTypes = data.MeteorologicalDataRequestWeatherTypes.Select(o => new MetWeatherTypeReportViewModel()
                    {
                        IsActive = o.IsActive,
                        WeatherTypeReportId = o.WeatherTypeReportId,
                        WeatherTypeDescription = o.WeatherTypeReport.Description
                    }).ToList()
                };
                result.IsSuccess = true;
                result.Data = new { Detail = model };
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while obtaining List.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Reapply()
        {
            return View("Create");
        }

        public MeteorologicalDataRequest MapToMeteorologicalDataRequest(MeteorologicalDataRequestViewModel model)
        {
            var metRequest = new MeteorologicalDataRequest()
            {
                UserServiceRequestMasterId = model.UserServiceRequestMasterId,
                Address = model.Address,
                CreatedByUserId = UserProfile.UserId,
                CreatedDate = DateTime.Now.DubaiTime(),
                CustomRangeEndDate = model.CustomRangeEndDate,
                CustomRangeStartDate = model.CustomRangeStartDate,
                DataRangeDaily = model.DataRangeDaily,
                DataRangeMonthly = model.DataRangeMonthly,
                DataRangeHourly= model.DataRangeHourly,
                DataRangeSelectPeriod = model.DataRangeSelectPeriod,
                DataRangeYearly = model.DataRangeDaily,
                Department = model.Department,
                Email = model.Email,
                Fax = model.Fax,
                IsDeleted = false,
                Phone = model.Phone,
                MeteorologicalDataRequestWeatherElements = model.WeatherElements.Select(o => new MeteorologicalDataRequestWeatherElement()
                {
                    IsActive = o.IsActive,
                    OtherName = o.OtherName,
                    WeatherElementId = o.WeatherElementId,

                }).ToList(),
                MeteorologicalDataRequestWeatherTypes = model.WeatherTypes.Select(o => new MeteorologicalDataRequestWeatherType()
                {
                    IsActive = o.IsActive,
                    WeatherTypeReportId = o.WeatherTypeReportId
                }).ToList()
            };

            return metRequest;
        }
    }
}