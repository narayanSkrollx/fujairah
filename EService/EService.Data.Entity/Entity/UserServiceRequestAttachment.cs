﻿namespace EService.Data.Entity.Entity
{
    public class UserServiceRequestAttachment
    {
        public int UserServiceRequestAttachmentId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public int UserAttachmentId { get; set; }

        public virtual UserServiceRequestMaster UserServiceRequestMaster { get; set; }
        public virtual UserAttachment UserAttachment { get; set; }
    }
}