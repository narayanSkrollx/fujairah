﻿using System.Collections.Generic;
using System.Runtime;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class User : AuditableEntity
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        public int UserTypeId { get; set; }
        public int? DepartmentId { get; set; }

        [JsonIgnore]
        public virtual UserType UserType { get; set; }
        [JsonIgnore]
        public virtual Department Department { get; set; }

        [JsonIgnore]
        public virtual UserContactPerson UserContactPerson { get; set; }
        [JsonIgnore]
        public virtual UserCompanyProfile UserCompanyProfile { get; set; }
        [JsonIgnore]
        public virtual UserSecruityQuestion UserSecruityQuestion { get; set; }

        [JsonIgnore]
        public virtual ICollection<UserServiceRequestMaster> UserServiceRequests { get; set; }
        [JsonIgnore]
        public virtual ICollection<UserAttachment> UserAttachments { get; set; }
        [JsonIgnore]
        public virtual ICollection<CranePermitRequestFirstStageEntry> CranePermitRequestFirstStageEntries { get; set; }
    }
}
