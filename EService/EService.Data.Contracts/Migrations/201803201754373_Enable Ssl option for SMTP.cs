namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnableSsloptionforSMTP : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SMTPEmailSetting", "EnableSsl", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SMTPEmailSetting", "EnableSsl");
        }
    }
}
