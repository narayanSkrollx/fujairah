using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class WeatherElementService : BusinessAccessBase<WeatherElement>
    {
        public WeatherElementService(ServiceContext context) : base(context)
        {
        }
    }
    
}