namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PaymentKeyRelatedTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PaymentKey",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MerchantId = c.String(nullable: false, maxLength: 1024),
                        EncryptionKey = c.String(nullable: false, maxLength: 1024),
                        Url = c.String(nullable: false, maxLength: 1024),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PaymentKey");
        }
    }
}
