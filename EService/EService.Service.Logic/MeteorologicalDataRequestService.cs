using System;
using System.Collections.Generic;
using System.Linq;
using EService.Data.Entity.Entity;
using EService.Service.Logic.Enum;
using EService.Service.Model;
using EService.Core;
namespace EService.Service.Logic
{
    public class MeteorologicalDataRequestService : BaseEService<MeteorologicalDataRequest>
    {
        public MeteorologicalDataRequestService(ServiceContext context) : base(context)
        {
        }

        public override ServiceEnum Service => ServiceEnum.MeteorologicalDataRequest;

        public override ResponseObject IsValidForApproveReject(int userServiceRequestMasterId, int serviceRequestFlowStageDepartmentId,
         int departmentId)
        {

            var result = new ResponseObject();
            var serviceRequestFlowStageDepartment =
                Context.Database.ServiceRequestFlowStageDepartments.FirstOrDefault(
                    o => o.ServiceRequestFlowStageDepartmentId == serviceRequestFlowStageDepartmentId && o.ServiceRequestFlowStage.IsCurrentStage && o.DepartmentId == departmentId);
            if (serviceRequestFlowStageDepartment == null)
            {
                result.IsSuccess = false;
                result.Message =
                    "Invalid Service Flow Request. Either the department is invalid or is not current stage.";
            }
            else
            {
                var userServiceRequestMaster =
                    Context.Database.UserServiceRequests.FirstOrDefault(
                        o =>
                            !o.IsDeleted && o.UserServiceRequestMasterId == userServiceRequestMasterId);
                if (userServiceRequestMaster == null || userServiceRequestMaster.MeteorologicalDataRequest == null)
                {
                    result.IsSuccess = false;
                    result.Message = "Provided Service Request info is not a valid Meteorological Data Request";
                }
                else
                {
                    var serviceFlowConfiguration =
                        Context.Database.ServiceApprovalStageDepartments.FirstOrDefault(
                            o => o.ServiceApprovalStage.ServiceId == (int)Service && departmentId == o.DepartmentId);
                    if (serviceFlowConfiguration == null)
                    {
                        result.IsSuccess = false;
                        result.Message = "Either Service Flow Configuration is not configured or does not exists.";
                    }
                    else
                    {
                        if (serviceFlowConfiguration.ServiceApprovalStage.Index == 1)
                        {
                            result.IsSuccess = !(Math.Abs(userServiceRequestMaster.ServieBill.ChargedAmount - (double)userServiceRequestMaster.Service.DefaultRate) < 0.01);
                            result.Message = !result.IsSuccess ? "MET Department needs to add Charge Amount for this request.." : "";
                        }
                        else
                        {
                            result.IsSuccess = true;
                        }
                    }
                }
            }
            return result;
        }

        public ResponseObject Update(MeteorologicalDataRequest model, int userId, bool isDraft, List<UserServiceRequestAttachment> addAttachments)
        {
            var request = Find(o => o.UserServiceRequestMasterId == model.UserServiceRequestMasterId && o.UserServiceRequestMaster.IsDraft && !o.UserServiceRequestMaster.IsDeleted);
            var list = request.MeteorologicalDataRequestWeatherElements.ToList();
            Context.Database.MeteorologicalDataRequestWeatherElements.RemoveRange(list);
            var typeList = request.MeteorologicalDataRequestWeatherTypes.ToList();
            Context.Database.MeteorologicalDataRequestWeatherTypes.RemoveRange(typeList);

            var removeDocList = request.UserServiceRequestMaster.UserServiceRequestAttachments.ToList();
            Context.Database.UserServiceRequestAttachment.RemoveRange(removeDocList);
            addAttachments.ForEach(o => request.UserServiceRequestMaster.UserServiceRequestAttachments.Add(o));

            request.Address = model.Address;
            request.Email = model.Email;
            request.Department = model.Department;
            request.Phone = model.Phone;
            request.Fax = model.Fax;
            request.ModifiedByUserId = userId;
            request.ModifiedDate = DateTime.Now.DubaiTime();
            request.DataRangeDaily = model.DataRangeDaily;
            request.DataRangeHourly = model.DataRangeHourly;
            request.DataRangeMonthly = model.DataRangeMonthly;
            request.DataRangeYearly = model.DataRangeYearly;
            request.DataRangeSelectPeriod = model.DataRangeSelectPeriod;
            request.CustomRangeStartDate = model.CustomRangeStartDate;
            request.CustomRangeEndDate = model.CustomRangeEndDate;

            request.UserServiceRequestMaster.IsDraft = isDraft;
            request.MeteorologicalDataRequestWeatherElements = new List<MeteorologicalDataRequestWeatherElement>();

            foreach (var type in model.MeteorologicalDataRequestWeatherElements)
            {
                request.MeteorologicalDataRequestWeatherElements.Add(type);
            }
            request.MeteorologicalDataRequestWeatherTypes = new List<MeteorologicalDataRequestWeatherType>();

            foreach (var type in model.MeteorologicalDataRequestWeatherTypes)
            {
                request.MeteorologicalDataRequestWeatherTypes.Add(type);
            }
            Context.Commit();
            return new ResponseObject { IsSuccess = true, Message = "Meteorological Data Request information updated successfully." };
        }

        public override bool HasExtraFormForApprovalReject()
        {
            return true;
        }

        public ResponseObject UpdateChargeAmount(int id, decimal chargedAmount, string remarks)
        {
            var result = new ResponseObject();
            var userServiceRequestMaster =
             Context.Database.UserServiceRequests.FirstOrDefault(o => o.UserServiceRequestMasterId == id);
            if (userServiceRequestMaster == null || userServiceRequestMaster.MeteorologicalDataRequest == null ||
                Math.Abs(userServiceRequestMaster.ServieBill.ChargedAmount - (double)userServiceRequestMaster.Service.DefaultRate) > 0.01)
            {
                result.IsSuccess = false;
                result.Message = "Either user Service Request is not a valid meteorological data request or has already saved Charged Amount.";
            }
            else
            {
                userServiceRequestMaster.ServieBill.ChargedAmount = (double)chargedAmount;
                userServiceRequestMaster.MeteorologicalDataRequest.METDepartmentRemrks = remarks;
                Context.Commit();
                result.IsSuccess = true;
                result.Message = "Meteorological Data Request Chrage Amount saved successfully.";
            }
            return result;
        }

    }
}