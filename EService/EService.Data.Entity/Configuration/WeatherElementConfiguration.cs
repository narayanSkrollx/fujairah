using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class WeatherElementConfiguration : EntityTypeConfiguration<WeatherElement>
    {
        public WeatherElementConfiguration()
        {
            ToTable("WeatherElement");

            HasKey(o => o.Id);
            Property(o => o.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(o => o.Description).IsRequired().HasMaxLength(1024);

        }
    }

    public class WeatherTypeReportConfiguration : EntityTypeConfiguration<WeatherTypeReport>
    {
        public WeatherTypeReportConfiguration()
        {
            ToTable("WeatherTypeReport");

            HasKey(o => o.Id);
            Property(o => o.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(o => o.Description).IsRequired().HasMaxLength(1024);

        }
    }

    public class MeteorologicalDataRequestWeatherElementConfiguration :
        EntityTypeConfiguration<MeteorologicalDataRequestWeatherElement>
    {
        public MeteorologicalDataRequestWeatherElementConfiguration()
        {
            ToTable("MeteorologicalDataRequestWeatherElement");

            HasKey(o => o.MeteorologicalDataRequestWeatherElementId);
            Property(o => o.MeteorologicalDataRequestWeatherElementId)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.UserServiceRequestMasterId).IsRequired();
            Property(o => o.WeatherElementId).IsRequired();
            Property(o => o.IsActive).IsRequired();
            Property(o => o.OtherName).IsOptional().HasMaxLength(1024);

            HasRequired(o => o.WeatherElement)
                .WithMany(o => o.MeteorologicalDataRequestWeatherElements)
                .HasForeignKey(o => o.WeatherElementId).WillCascadeOnDelete(false);
            HasRequired(o => o.MeteorologicalDataRequest)
                .WithMany(o => o.MeteorologicalDataRequestWeatherElements)
                .HasForeignKey(o => o.UserServiceRequestMasterId).WillCascadeOnDelete(false);

        }
    }

    public class MeteorologicalDataRequestWeatherTypeConfiguration :
        EntityTypeConfiguration<MeteorologicalDataRequestWeatherType>
    {
        public MeteorologicalDataRequestWeatherTypeConfiguration()
        {
            ToTable("MeteorologicalDataRequestWeatherType");

            HasKey(o => o.MeteorologicalDataRequestWeatherTypeid);

            Property(o => o.MeteorologicalDataRequestWeatherTypeid)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.UserServiceRequestMasterId).IsRequired();
            Property(o => o.WeatherTypeReportId).IsRequired();
            Property(o => o.IsActive);

            HasRequired(o => o.MeteorologicalDataRequest)
                .WithMany(o => o.MeteorologicalDataRequestWeatherTypes)
                .HasForeignKey(o => o.UserServiceRequestMasterId).WillCascadeOnDelete(false);
            HasRequired(o => o.WeatherTypeReport)
                .WithMany(o => o.MeteorologicalDataRequestWeatherTypes)
                .HasForeignKey(o => o.WeatherTypeReportId).WillCascadeOnDelete(false);

        }
    }

    public class MeteorologicalDataRequestServiceConfiguration :
        EntityTypeConfiguration<MeteorologicalDataRequest>
    {
        public MeteorologicalDataRequestServiceConfiguration()
        {
            ToTable("MeteorologicalDataRequestService");

            HasKey(o => o.UserServiceRequestMasterId);

            Property(o => o.UserServiceRequestMasterId)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(o => o.Email).IsRequired().HasMaxLength(1024);
            Property(o => o.Department).IsOptional().HasMaxLength(1024);
            Property(o => o.Phone).IsRequired().HasMaxLength(50);
            Property(o => o.Address).IsOptional().HasMaxLength(1024);

            Property(o => o.DataRangeDaily);
            Property(o => o.DataRangeMonthly);
            Property(o => o.DataRangeYearly);
            Property(o => o.DataRangeSelectPeriod);
            Property(o => o.CustomRangeStartDate).IsOptional();
            Property(o => o.CustomRangeEndDate).IsOptional();
            Property(o => o.METDepartmentRemrks).IsOptional().HasMaxLength(255);

            HasRequired(o => o.UserServiceRequestMaster).WithRequiredDependent(o => o.MeteorologicalDataRequest).WillCascadeOnDelete(false);


        }
    }
}