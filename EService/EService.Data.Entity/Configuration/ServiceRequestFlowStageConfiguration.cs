using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class ServiceRequestFlowStageConfiguration : EntityTypeConfiguration<ServiceRequestFlowStage>
    {
        public ServiceRequestFlowStageConfiguration()
        {
            ToTable("ServiceRequestFlowStage");

            HasKey(o => o.ServiceRequestFlowStageId);
            Property(o => o.ServiceRequestFlowStageId)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.UserServiceRequestMasterId).IsRequired();
            Property(o => o.Index).IsRequired();
            Property(o => o.IsCurrentStage).IsRequired();

            HasRequired(o => o.UserServiceRequestMaster)
                .WithMany(o => o.ServiceRequestFlowStages)
                .HasForeignKey(o => o.UserServiceRequestMasterId);

        }
    }
}