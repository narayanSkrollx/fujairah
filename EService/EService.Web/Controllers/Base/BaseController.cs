﻿using System;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web.Mvc;
using EService.Core;
using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Web.Models;

namespace EService.Web.Controllers.Base
{
    [Authorize]
    public abstract class BaseController : Controller
    {
        protected ServiceContext serviceContext;
        private ErrorLogLogic errorLogLogic;

        protected BaseController()
        {
            serviceContext = new ServiceContext();
            errorLogLogic = new ErrorLogLogic(serviceContext);
        }

        //public void GetClaims()
        //{
        //    //Get the current claims principal
        //    var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;

        //    // Get the claims values
        //    var name = identity.Claims.Where(c => c.Type == ClaimTypes.Name)
        //                       .Select(c => c.Value).SingleOrDefault();
        //    var sid = identity.Claims.Where(c => c.Type == ClaimTypes.Sid)
        //                       .Select(c => c.Value).SingleOrDefault();
        //}

        protected UserProfile UserProfile
        {
            get
            {
                var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;

                // Get the claims values
                var userProfileData = identity.Claims.Where(c => c.Type == ClaimTypes.UserData)
                                   .Select(c => c.Value).SingleOrDefault();
                if (string.IsNullOrEmpty(userProfileData))
                {
                    throw new UnauthorizedAccessException("User Profile Could not be obtained.");
                }
                else
                {
                    return userProfileData.DeserializeObject<UserProfile>();
                }
            }
        }

        public ActionResult ErrorResult(string message = "")
        {
            if (Request.IsAjaxRequest())
            {
                return
                    Json(new JsonResponseObject()
                    {
                        IsRedirect = true,
                        IsSuccess = false,
                        Message = Url.Action("Index", "Error", new { Area = "" })
                    }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("Index", "Error");
            }
        }

        protected void LogError(Exception ex, bool isHandled = true)
        {
            var requestUrl = Request.Url.Segments.ToList();
            errorLogLogic.Add(new ErrorLog()
            {
                Exception = ex.Message,
                File = string.Format("{0}", string.Join("", requestUrl)),
                InnerException = ex.GetBaseException().Message,
                StackTrace = ex.StackTrace,
                Timestamp = DateTime.UtcNow,
                Method = ex.TargetSite.Name,
                Source = ex.Source,
                UserId = UserProfile.UserId,
                ClientBrowserAgent = Request.UserAgent,
                ClientIp = Request.UserHostAddress,
                Data = ex.Data.ToText(),
                UserName = UserProfile.UserName,
            });
            serviceContext.Commit();
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);

            try
            {
                LogError(filterContext.Exception, false);
            }
            catch
            {
            }
            if (!filterContext.ExceptionHandled)
            {
                filterContext.Result = filterContext.HttpContext.Request.IsAjaxRequest() ? new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError) : ErrorResult("An Unhandled Exception occurred. Your error has been logged.");
            }
        }

        protected virtual string RenderPartialView(string partialViewName, object model = null)
        {
            if (ControllerContext == null)
                return string.Empty;

            if (string.IsNullOrEmpty(partialViewName))
                throw new ArgumentNullException("partialViewName");

            ModelState.Clear();//Remove possible model binding error.

            ViewData.Model = model;//Set the model to the partial view

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, partialViewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }

        protected ActionResult UnAuthroizedResult()
        {
            if (Request.IsAjaxRequest())
            {
                return
                    Json(new JsonResponseObject()
                    {
                        IsRedirect = true,
                        Message = Url.Action("Index", "Login", new { Area = "" })
                    });
            }
            else
            {
                return RedirectToAction("Index", "Login", new { Area = "" });
            }
        }
    }
}