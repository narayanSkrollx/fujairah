﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.BusinessLogic;
using umbraco.cms.presentation.Trees;
using umbraco.DataLayer;

namespace Designit.Umb.Newsletter.Bll
{
    public class loadNewsletterMailSettings: BaseTree
    {
        public loadNewsletterMailSettings(string application) :
            base(application) { }


        protected override void CreateRootNode(ref XmlTreeNode rootNode)
        {
            rootNode.Icon = FolderIcon;
            rootNode.OpenIcon = FolderIconOpen;
            rootNode.NodeType = TreeAlias;
            rootNode.NodeID = "init";
            rootNode.Menu.Clear();
        }

        public override void RenderJS(ref System.Text.StringBuilder Javascript)
        {
            Javascript.Append(
                @"
                        function openNewsletterMailSettings()
                        {
                           parent.right.document.location.href = 'plugins/designit/newsletter/editNewsletterMailSettings.aspx';
                        }
        
                ");

            
        }


        public override void Render(ref XmlTree tree)
        {
            
                XmlTreeNode xNode = XmlTreeNode.Create(this);
                xNode.Text = "Mail settings";
                xNode.Icon = "developerMacro.gif";
                xNode.Menu.Clear();
                xNode.Action = "javascript:openNewsletterMailSettings()";
                tree.Add(xNode);

            
        }
    }
}
