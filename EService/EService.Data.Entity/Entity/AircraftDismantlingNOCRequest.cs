﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EService.Data.Entity.Entity
{
    public class AircraftDismantlingNOCRequest : AuditableEntity, IServiceEntity
    {
        public int UserServiceRequestMasterId { get; set; }

        public DateTime? DateOfApplication { get; set; }
        public string NameOfApplicant { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string ApplicantCompanyName { get; set; }
        public string AircraftTypeAndModelToDismantle { get; set; }
        public string AircraftMSNAndRegistrationNumber { get; set; }
        public string OldRegistrationNo { get; set; }
        public string PreviousOwnerOfAircraft { get; set; }
        public string CurrentOwnerOfAircraft { get; set; }
        public string OperatorOrAgentWhoBroughtTheAircraft { get; set; }
        public bool? WithNoObjectionFromTheAircraftOwner { get; set; }
        public bool HasGCAAApproval { get; set; }
        public string GCAAApprovalRefNumber { get; set; }
        public string CertificateOfDeRegistrationRefNumber { get; set; }
        public string TradeLicenseRefNumber { get; set; }
        public DateTime? DateOfIssueOfTradeLicense { get; set; }
        public DateTime? DateOfExpiryOfTradeLicense { get; set; }
        public string FANRLicenseRefNumber { get; set; }
        public DateTime? DateOfIssueOfFANRLicense { get; set; }
        public DateTime? DateOfExpiryOfFANRLicense { get; set; }
        public string FujairahEPDLicenseRefNumber { get; set; }
        public DateTime? DateOfIssueOfFujairahEPDLicense { get; set; }
        public DateTime? DateOfExpiryOfFujairahEPDLicense { get; set; }
        public string MethodOfDismantlingAndDestruction { get; set; }


        [JsonIgnore]
        public virtual UserServiceRequestMaster UserServiceRequestMaster { get; set; }
    }
}
