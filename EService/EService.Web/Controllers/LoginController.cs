﻿using System;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using EService.Core;
using EService.Core.Security;
using EService.Service.Logic;
using EService.Service.Model;
using EService.Web.Helper;
using EService.Web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace EService.Web.Controllers
{
    public class LoginController : Controller
    {
        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        private void SignIn(string userId, string userName, UserProfile userProfile, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, userName), //user.Name from my database
                new Claim(ClaimTypes.NameIdentifier, userId), //user.Id from my database
                new Claim(ClaimTypes.UserData, userProfile.ToJson()), //userdata from my database
            };
            Session["UserName"] = userName;
            Session["UserProfile"] = userProfile;

            var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
            var claimsPrincipal = new ClaimsPrincipal(identity);
            // Set current principal
            Thread.CurrentPrincipal = claimsPrincipal;
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, identity);
        }


        private ServiceContext serviceContext;
        private UserService userService;

        public LoginController()
        {
            serviceContext = new ServiceContext();
            userService = new UserService(serviceContext);
        }

        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Logout");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginViewModel model, string returnURL = "")
        {
            var currentdate = DateTime.Now;
            var currentDAteUtc = DateTime.UtcNow;
            var currentDubaiDate = DateTime.Now.DubaiTime();

            var responseObject = new JsonResponseObject();
            try
            {
                if (ModelState.IsValid)
                {
                    var authenticateResult = userService.Authenticate(model.UserName, model.Password.ToMd5());
                    if (authenticateResult.IsSuccess && authenticateResult.Data != null)
                    {
                        var userProfile = new UserProfile()
                        {
                            UserName = authenticateResult.Data.UserName,
                            UserId = authenticateResult.Data.UserId,
                            UserTypeId = authenticateResult.Data.UserTypeId,
                            UserTypeName = authenticateResult.Data.UserType.UserTypeName,
                            DepartmentId = authenticateResult.Data.DepartmentId
                        };
                        SignIn(userProfile.UserId.ToString(), userProfile.UserName, userProfile, false);
                        responseObject.IsSuccess = true;
                        responseObject.Message = "User authenticated successfully.";
                    }
                    else
                    {
                        responseObject.IsSuccess = false;
                        responseObject.Message = "Provided User name and Password could not be authenticated";
                    }
                }
                else
                {
                    responseObject.IsSuccess = false;
                    responseObject.Message = "There are errors in model: " + ModelState.GetModelStateError();
                }
            }
            catch (Exception ex)
            {
                //LogError(ex);
                responseObject.IsSuccess = false;
                responseObject.Message = "Error while trying to login.";
            }
            return Json(responseObject);
        }

        public ActionResult Logout()
        {
            LogOut();
            return RedirectToAction("Index");
        }
        private bool LogOut()
        {
            try
            {
                if (Request.IsAuthenticated)
                {
                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                }
                Session.Clear();
                Session.Abandon();
                AuthenticationManager.SignOut();
                Session.Clear();
                Session.Abandon(); // Session Expire but cookie do exist
                Response.Cookies.Clear();
                HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                authCookie.Expires = DateTime.Now.DubaiTime().AddYears(-1);
                Request.Cookies.Clear();
                Response.Cookies.Add(authCookie);
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", string.Empty));//.AspNet.ApplicationCookie
                Response.Cookies.Add(new HttpCookie(".AspNet.ApplicationCookie", string.Empty));//.AspNet.ApplicationCookie
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}