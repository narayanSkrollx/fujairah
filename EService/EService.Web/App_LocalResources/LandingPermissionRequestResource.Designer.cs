﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EService.Web.App_LocalResources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class LandingPermissionRequestResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal LandingPermissionRequestResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("EService.Web.App_LocalResources.LandingPermissionRequestResource", typeof(LandingPermissionRequestResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Address.
        /// </summary>
        public static string Address {
            get {
                return ResourceManager.GetString("Address", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Aircraft Registration.
        /// </summary>
        public static string AircraftRegistration {
            get {
                return ResourceManager.GetString("AircraftRegistration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Airline Name.
        /// </summary>
        public static string AirlineName {
            get {
                return ResourceManager.GetString("AirlineName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Airport of Landing.
        /// </summary>
        public static string AirportLanding {
            get {
                return ResourceManager.GetString("AirportLanding", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Airport Of Operation.
        /// </summary>
        public static string AirportOfOperation {
            get {
                return ResourceManager.GetString("AirportOfOperation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Complete Routing.
        /// </summary>
        public static string CompleteRouting {
            get {
                return ResourceManager.GetString("CompleteRouting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PAX / Crew/ Cargo Details.
        /// </summary>
        public static string CrewDetail {
            get {
                return ResourceManager.GetString("CrewDetail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date From.
        /// </summary>
        public static string DateFrom {
            get {
                return ResourceManager.GetString("DateFrom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date To.
        /// </summary>
        public static string DateTo {
            get {
                return ResourceManager.GetString("DateTo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to E-mail.
        /// </summary>
        public static string Email {
            get {
                return ResourceManager.GetString("Email", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter Trip Details.
        /// </summary>
        public static string EnterTripDetails {
            get {
                return ResourceManager.GetString("EnterTripDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ETA (Destination after outboud Dubai).
        /// </summary>
        public static string ETADestination {
            get {
                return ResourceManager.GetString("ETADestination", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ETD(Origin).
        /// </summary>
        public static string ETDOrigin {
            get {
                return ResourceManager.GetString("ETDOrigin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fax.
        /// </summary>
        public static string Fax {
            get {
                return ResourceManager.GetString("Fax", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nature of Operation.
        /// </summary>
        public static string FlightNatureOfOperations {
            get {
                return ResourceManager.GetString("FlightNatureOfOperations", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Flight Number.
        /// </summary>
        public static string FlightNumber {
            get {
                return ResourceManager.GetString("FlightNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manual Entry.
        /// </summary>
        public static string IsManualEntry {
            get {
                return ResourceManager.GetString("IsManualEntry", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to MTOW (unit : in Kgs).
        /// </summary>
        public static string MTOW {
            get {
                return ResourceManager.GetString("MTOW", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No of Passengers.
        /// </summary>
        public static string NoOfPassengers {
            get {
                return ResourceManager.GetString("NoOfPassengers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Permit Number.
        /// </summary>
        public static string PermitNumber {
            get {
                return ResourceManager.GetString("PermitNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ref No.
        /// </summary>
        public static string RefNo {
            get {
                return ResourceManager.GetString("RefNo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Telephone.
        /// </summary>
        public static string Telephone {
            get {
                return ResourceManager.GetString("Telephone", resourceCulture);
            }
        }
    }
}
