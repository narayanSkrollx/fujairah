﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace FIA.CMS.Entities
{
    [TableName("NewsletterTemplate")]
    [PrimaryKey("Id", autoIncrement = true)]
    [ExplicitColumns]
    public class NewsletterTemplate
    {
        [Column("Id")]
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }
        [Column("Subject")]
        [Required]
        public string Subject { get; set; }
        [Column("Body")]
        [Required]
        public string Body { get; set; }
    }
}