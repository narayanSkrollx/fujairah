﻿using System;
using System.Collections.Generic;

namespace EService.Service.Model
{
    public class ReceiptPrintModel
    {
        public string RequestRegistrationNo { get; set; }
        public string UserName { get; set; }
        public string Address { get; set; }
        public string ReceiptNo { get; set; }
        public int ReceiptId { get; set; }
        public List<ReceiptItem> Items { get; set; }

        public DateTime PaidDateTime { get; set; }
        public double AdminFee { get; set; }
        public double VAT { get; set; }
        public double SocialDevelopmentFee { get; set; }
        public double Total { get; set; }
        public class ReceiptItem
        {
            public string Description { get; set; }
            public int Quantity { get; set; }
            public double Rate { get; set; }
        }
    }
}