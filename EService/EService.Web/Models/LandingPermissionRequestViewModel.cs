using System.Collections.Generic;

namespace EService.Web.Models
{
    public class LandingPermissionRequestViewModel
    {
        public LandingPermissionRequestSearchViewModel SearchModel { get; set; }
        public List<object> Records { get; set; }
    }
}