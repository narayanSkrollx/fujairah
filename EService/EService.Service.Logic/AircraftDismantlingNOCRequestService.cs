﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EService.Data.Entity;
using EService.Data.Entity.Entity;
using EService.Service.Logic.Enum;
using EService.Service.Model;

namespace EService.Service.Logic
{
    public class AircraftDismantlingNOCRequestService : BaseEService<AircraftDismantlingNOCRequest>
    {
        public AircraftDismantlingNOCRequestService(ServiceContext context) : base(context)
        {
        }

        public override ServiceEnum Service => ServiceEnum.AircraftDismantlingRequest;

        public ResponseObject UpdateAircraftDismantlingNOCRequestService(AircraftDismantlingNOCRequest model, bool isDraft, List<UserServiceRequestAttachment> addAttachments)
        {
            var result = new ResponseObject();
            var contract = Find(x => x.UserServiceRequestMasterId == model.UserServiceRequestMasterId && !x.IsDeleted && x.UserServiceRequestMaster.IsDraft);
            if (contract == null)
            {
                result.IsSuccess = false;
                result.Message = "No request matched to edit.";
            }
            else
            {
                var removeDocList = contract.UserServiceRequestMaster.UserServiceRequestAttachments.ToList();
                Context.Database.UserServiceRequestAttachment.RemoveRange(removeDocList);

                addAttachments.ForEach(o => contract.UserServiceRequestMaster.UserServiceRequestAttachments.Add(o));
                 
                contract.UserServiceRequestMaster.IsDraft = isDraft;
                contract.Address = model.Address;
                contract.OldRegistrationNo = model.OldRegistrationNo;
                contract.AircraftMSNAndRegistrationNumber = model.AircraftMSNAndRegistrationNumber;
                contract.AircraftTypeAndModelToDismantle = model.AircraftTypeAndModelToDismantle;
                contract.ApplicantCompanyName = model.ApplicantCompanyName;
                contract.CertificateOfDeRegistrationRefNumber = model.CertificateOfDeRegistrationRefNumber;
                contract.CurrentOwnerOfAircraft = model.CurrentOwnerOfAircraft;
                contract.DateOfApplication = model.DateOfApplication;
                contract.DateOfExpiryOfFANRLicense = model.DateOfExpiryOfFANRLicense;
                contract.DateOfExpiryOfFujairahEPDLicense = model.DateOfExpiryOfFujairahEPDLicense;
                contract.DateOfExpiryOfTradeLicense = model.DateOfExpiryOfTradeLicense;
                contract.DateOfIssueOfFANRLicense = model.DateOfIssueOfFANRLicense;
                contract.DateOfIssueOfFujairahEPDLicense = model.DateOfIssueOfFujairahEPDLicense;
                contract.DateOfIssueOfTradeLicense = model.DateOfIssueOfTradeLicense;
                contract.Email = model.Email;
                contract.FANRLicenseRefNumber = model.FANRLicenseRefNumber;
                contract.FujairahEPDLicenseRefNumber = model.FujairahEPDLicenseRefNumber;
                contract.GCAAApprovalRefNumber = model.GCAAApprovalRefNumber;
                contract.HasGCAAApproval = model.HasGCAAApproval;
                contract.MethodOfDismantlingAndDestruction = model.MethodOfDismantlingAndDestruction;
                contract.NameOfApplicant = model.NameOfApplicant;
                contract.OperatorOrAgentWhoBroughtTheAircraft = model.OperatorOrAgentWhoBroughtTheAircraft;
                contract.PhoneNumber = model.PhoneNumber;
                contract.PreviousOwnerOfAircraft = model.PreviousOwnerOfAircraft;
                contract.TradeLicenseRefNumber = model.TradeLicenseRefNumber;
                contract.UserServiceRequestMasterId = model.UserServiceRequestMasterId;
                contract.WithNoObjectionFromTheAircraftOwner = model.WithNoObjectionFromTheAircraftOwner;
                


                Context.Commit();
                result.IsSuccess = true;
                result.Message = "Aircraft Dismantle NOC request updated successfully";
            }
            return result;
        }
    }
}
