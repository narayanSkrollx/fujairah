using System.Collections.Generic;
using EService.Data.Entity;
using EService.Data.Entity.Entity;
using EService.Service.Model;
using Quartz;

namespace EService.Web.Models
{
    public class UserViewModel
    {
        public List<UserModel> UserList { get; set; }
        public List<LookupItem> UserTypes { get; set; }
        public List<LookupItem> Departments { get; set; }
        public DepartmentUserCreateViewModel NewDepartmentUserModel { get; set; }
    }
}