﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace FIA.CMS.Helper
{
    public class EmailSenderAddress
    {
        public static string GetSenderEmail(string modelName)
        {
            return ConfigurationManager.AppSettings[modelName + "_emailTo"];
        }
    }
}