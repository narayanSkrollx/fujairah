using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class FlightNaureOfOperationConfiguration : EntityTypeConfiguration<FlightNatureOfOperation>
    {
        public FlightNaureOfOperationConfiguration()
        {
            ToTable("FlightNatureOfOperation");

            HasKey(o => o.Id);
            Property(o => o.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.Description).IsRequired().HasMaxLength(1024);
            Property(o => o.Active).IsRequired();
        }
    }
}