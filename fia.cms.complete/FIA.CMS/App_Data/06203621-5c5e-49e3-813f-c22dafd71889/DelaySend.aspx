﻿<%@ Page ValidateRequest="false" Language="C#" MasterPageFile="/umbraco/masterpages/umbracoPage.Master"
    AutoEventWireup="true" CodeBehind="DelaySend.aspx.cs" Inherits="Designit.Umb.Newsletter.Custom.plugins.designit.newsletter.DelaySend" %>
<%@ Register TagPrefix="cc1" Namespace="umbraco.uicontrols" Assembly="controls" %>
<%@ Register TagPrefix="umb" Namespace="ClientDependency.Core.Controls" Assembly="ClientDependency.Core" %>
<%@ Register TagPrefix="cc1" Namespace="umbraco.uicontrols.DatePicker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <umb:JsInclude ID="JsInclude1" runat="server" FilePath="js/umbracoCheckKeys.js" PathNameAlias="UmbracoRoot" />
    <umb:JsInclude ID="JsInclude2" runat="server" FilePath="ui/jquery.js" PathNameAlias="UmbracoClient" Priority="0" />
    <umb:JsInclude ID="JsInclude3" runat="server" FilePath="ui/jqueryui.js" PathNameAlias="UmbracoClient" Priority="1" />

    <cc1:UmbracoPanel ID="Panel1" runat="server" Width="608px" Height="550px" hasMenu="false">
        <cc1:Pane ID="Pane1" runat="server" Height="44px" Width="500" Text="Send campaign now">
            <div style="text-align: left; float: left;">
                <cc1:PropertyPanel ID="PropertyPanel1" runat="server" Text="Send campaign" />
            </div>
            <div style="text-align: right; float: right;">
                <cc1:PropertyPanel ID="pp_Send" runat="server" Text="">
                    <asp:Button ID="btnSend" runat="server" Text="Send campaign" OnClick="btnSend_Click" />
                </cc1:PropertyPanel>
            </div>
        </cc1:Pane>
        <cc1:Pane ID="Pane2" runat="server" Height="44px" Width="500" Text="Or schedule when to send campaign">
            <cc1:PropertyPanel ID="pp_Timezone" runat="server" Text="Select timezone">
                <asp:DropDownList ID="TimezoneList" runat="server">
                </asp:DropDownList>
            </cc1:PropertyPanel>
            <cc1:PropertyPanel ID="pp_Date" runat="server" Text="Select date">
                <div class="propertyItemContent">
                    <asp:Panel ID="panReleaseDate" runat="server" />
                </div>
            </cc1:PropertyPanel>
            <div style="text-align: right; float: right;">
                <cc1:PropertyPanel ID="pp_buttons" runat="server">
                    <asp:Button ID="btnDelay" runat="server" Text="Schedule campaign" OnClick="btnDelay_Click" />
                </cc1:PropertyPanel>
            </div>
        </cc1:Pane>
    </cc1:UmbracoPanel>

    <script type="text/javascript">


        function Redir() {
            <%= umbraco.BasePages.ClientTools.Scripts.ChangeContentFrameUrl("/umbraco/plugins/designit/newsletter/editNewsletterCampaign.aspx?id=1") %>
            //parent.right.document.location.href = 'editNewsletterCampaign.aspx?id=1';
        }
        function Close() {
            <% =umbraco.BasePages.ClientTools.Scripts.CloseModalWindow() %>

        }
   
    </script>

</asp:Content>
