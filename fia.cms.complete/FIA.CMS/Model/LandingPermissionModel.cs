﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FIA.CMS.Model
{
    public class LandingPermissionModel
    {
        [Required]
        public string Operator { get; set; }
        [Required]
        public string TypeOfAC { get; set; }
        [Required]
        public string RegistrationMTOW { get; set; }
        [Required]
        public string IcoCallSign { get; set; }
        [Required]
        public DateTime DateOfOps { get; set; }
        [Required]
        public string Crew { get; set; }
        [Required]
        public string PurposeOfFlt { get; set; }
        [Required]
        public string Route { get; set; }
        [Required]
        public DateTime Schedule { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Mobile { get; set; }
        [Required]
        [EmailAddress]
        public string EmailAddress { get; set; }
        [Required]
        public string AdditionalServies { get; set; }
        [Required]
        public string Message { get; set; }
        [Required]
        public List<LandingPermissionDocumentAttachment> Attachments { get; set; }
      

        //[Required]
        //public string emiratedId { get; set; }
        //[Required]
        //public string catagory { get; set; }
        //[Required]
        //public string cname { get; set; }
        //[Required]
        //public string caddress { get; set; }
        //[Required]
        //public string phone { get; set; }
        //[Required]
        //public string fax { get; set; }
        //[Required]
        //public string pobox { get; set; }
        //[Required]
        //public string city { get; set; }
    }

    public class LandingPermissionDocumentAttachment
    {
        [Required]
        public string Document { get; set; }
      public HttpPostedFileBase File { get; set; }
    }
}