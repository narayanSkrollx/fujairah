﻿using Archetype.Models;
using FIA.CMS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace FIA.CMS.Controller
{
    public class TeamConnectionController : BaseController
    {
        // GET: TeamConnection
        public ActionResult RenderArcheTypeContent()
        {
            var model = new List<DefaultContentArcheType>();
            //IPublishedContent homePage = CurrentPage
            //        .AncestorOrSelf(1)
            //        .DescendantsOrSelf()
            //        .FirstOrDefault(o => o.DocumentTypeAlias == "home");
            ArchetypeModel contentItems = CurrentPage.GetPropertyValue<ArchetypeModel>("contentGrid");

            foreach (ArchetypeFieldsetModel fieldSet in contentItems.Fieldsets)
            {
                var image = fieldSet.GetValue<IPublishedContent>("image");

                bool revert = false;
                bool convertSuccess = bool.TryParse(fieldSet.GetValue("revertImageContent"), out revert);
                model.Add(new DefaultContentArcheType()
                {
                    ImageUrl = image.Url,
                    MainContent = fieldSet.GetValue("mainContent"),
                    Title = fieldSet.GetValue("title"),
                    RevertPlacement = convertSuccess ? revert : (bool?)null,
                });
            }
            return PartialView("~/Views/Partials/Shared/_TeamConnectionContentTemplate.cshtml", model);
        }
    }
}