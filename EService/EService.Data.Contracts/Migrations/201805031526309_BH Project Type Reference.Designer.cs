// <auto-generated />
namespace EService.Data.Contracts.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class BHProjectTypeReference : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(BHProjectTypeReference));
        
        string IMigrationMetadata.Id
        {
            get { return "201805031526309_BH Project Type Reference"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
