﻿using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;
using System.ComponentModel.DataAnnotations.Schema;
namespace EService.Data.Entity.Configuration
{
    public class PaymentKeyConfiguration : EntityTypeConfiguration<PaymentKey>
    {
        public PaymentKeyConfiguration()
        {
            ToTable("PaymentKey");

            HasKey(o => o.Id);

            Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.MerchantId).IsRequired().HasMaxLength(1024);
            Property(o => o.EncryptionKey).IsRequired().HasMaxLength(1024);
            Property(o => o.Url).IsRequired().HasMaxLength(1024);


        }
    }
}