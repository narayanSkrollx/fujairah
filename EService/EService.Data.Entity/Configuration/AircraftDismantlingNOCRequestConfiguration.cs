﻿using EService.Data.Entity.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EService.Data.Entity.Configuration
{
    public class AircraftDismantlingNOCRequestConfiguration : EntityTypeConfiguration<AircraftDismantlingNOCRequest>
    {
        public AircraftDismantlingNOCRequestConfiguration()
        {
            ToTable("AircraftDismantlingNOCRequest");
            HasKey(x => x.UserServiceRequestMasterId);

            Property(x => x.UserServiceRequestMasterId).IsRequired();
            Property(x => x.DateOfApplication).IsRequired();
            Property(x => x.NameOfApplicant).IsRequired();
            Property(x => x.Address).IsOptional();
            Property(x => x.PhoneNumber).IsOptional().HasMaxLength(225);
            Property(x => x.Email).IsOptional().HasMaxLength(225);
            Property(x => x.ApplicantCompanyName).IsOptional().HasMaxLength(225);
            Property(x => x.AircraftTypeAndModelToDismantle).IsOptional().HasMaxLength(225);
            Property(x => x.AircraftMSNAndRegistrationNumber).IsOptional().HasMaxLength(225);
            Property(x => x.OldRegistrationNo).IsOptional().HasMaxLength(225);
            Property(x => x.PreviousOwnerOfAircraft).IsOptional().HasMaxLength(225);
            Property(x => x.CurrentOwnerOfAircraft).IsOptional().HasMaxLength(225);
            Property(x => x.OperatorOrAgentWhoBroughtTheAircraft).IsOptional().HasMaxLength(225);
            Property(x => x.WithNoObjectionFromTheAircraftOwner).IsOptional();
            Property(x => x.HasGCAAApproval).IsRequired();
            Property(x => x.GCAAApprovalRefNumber).IsOptional().HasMaxLength(225);
            Property(x => x.CertificateOfDeRegistrationRefNumber).IsOptional().HasMaxLength(225);
            Property(x => x.TradeLicenseRefNumber).IsOptional().HasMaxLength(225);
            Property(x => x.DateOfIssueOfTradeLicense).IsOptional();
            Property(x => x.DateOfExpiryOfTradeLicense).IsOptional();
            Property(x => x.FANRLicenseRefNumber).IsOptional().HasMaxLength(225);
            Property(x => x.DateOfIssueOfFANRLicense).IsOptional();
            Property(x => x.DateOfExpiryOfFANRLicense).IsOptional();
            Property(x => x.FujairahEPDLicenseRefNumber).IsOptional().HasMaxLength(225);
            Property(x => x.DateOfIssueOfFujairahEPDLicense).IsOptional();
            Property(x => x.DateOfExpiryOfFujairahEPDLicense).IsOptional();
            Property(x => x.MethodOfDismantlingAndDestruction).IsOptional().HasMaxLength(225);


            HasRequired(x => x.UserServiceRequestMaster)
                .WithRequiredDependent(x => x.AircraftDismantlingNOCRequest).WillCascadeOnDelete(false);
        }
    }
}
