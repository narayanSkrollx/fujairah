﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using umbraco.BusinessLogic;

namespace Designit.Umb.Newsletter.Bll
{
    public class newsletterTemplateTasks : umbraco.interfaces.ITaskReturnUrl
    {


        #region ITaskReturnUrl Members

        private string returnUrl;

        public string ReturnUrl
        {
            get { return returnUrl; }
        }

        #endregion

        #region ITask Members

        private string alias;
        private int parentID;
        private int typeID;
        private int userID;
        
        public string Alias
        {
            get
            {
                return alias;
            }
            set
            {
                alias = value;
            }
        }

        public int ParentID
        {
            get
            {
                return parentID;
            }
            set
            {
                parentID = value;
            }
        }


        public int TypeID
        {
            get
            {
                return typeID;
            }
            set
            {
                typeID = value;
            }
        }

        public int UserId
        {
            set { userID = value; }
        }

        public bool Delete()
        {

            alias = Application.SqlHelper.ExecuteScalar<string>
                            ("select alias from newsletterTemplate where id = @id",
                    Application.SqlHelper.CreateParameter("@id", ParentID));

            var path = Bll.Helper.GetTemplateDir();
            path = Path.Combine(path, string.Format("{0}.master", alias));
            if (File.Exists(path))
            {
                File.Delete(path);
            }

            Application.SqlHelper.ExecuteNonQuery("delete from newsletterTemplate where id = @id",
                            Application.SqlHelper.CreateParameter("@id", ParentID));
            
         

            return true;


        }


        public bool Save()
        {

            

            int IdReturned = Application.SqlHelper.ExecuteScalar<int>
                            ("insert into newsletterTemplate(alias, design) values(@alias, @design);select @@Identity",
                    Application.SqlHelper.CreateParameter("@alias", Alias),
                    Application.SqlHelper.CreateParameter("@design", @"<html><head></head><body></body></html>"));

            returnUrl = "plugins/designit/newsletter/editNewsletterTemplate.aspx?id=" + IdReturned;

            return true;

        }

      

        #endregion

    }
}
