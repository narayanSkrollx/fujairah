﻿namespace EService.Data.Entity.Entity
{
    public class  PaymentKey : AuditableEntity
    {
        public int Id { get; set; }
        public string MerchantId { get; set; }
        public string EncryptionKey { get; set; }
        public string Url { get; set; }
    }
}