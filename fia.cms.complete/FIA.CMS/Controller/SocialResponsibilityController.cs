﻿using FIA.CMS.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using FIA.CMS.Helper;

namespace FIA.CMS.Controller
{
    public class SocialResponsibilityController : BaseController
    {
        private void SendEmail(SocialResponsibilityModel model)
        {
            var server = ConfigurationManager.AppSettings["smtpServer"];
            var port = Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]);
            var username = ConfigurationManager.AppSettings["smtpEmail"];
            var password = ConfigurationManager.AppSettings["smtpPassword"];
            var to = ConfigurationManager.AppSettings["emailTo"];
            var message = new MailMessage(username, to);
            message.Subject = $"Social Responsibility for {model.FirstName} {model.LastName}";
            message.Body = GetMessageBody(model);
            message.IsBodyHtml = true;
            var client = new SmtpClient(server, port);
            client.Credentials = new NetworkCredential(username, password);
            //client.EnableSsl = true;
            client.Send(message);
        }

        //public ActionResult RenderForm()
        //{
        //    return PartialView($"{HomePartialViewLocation}_Contact.cshtml");
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitForm(SocialResponsibilityModel model)
        {
            if (ModelState.IsValid)
            {
                var to = model.ServiceType.Equals("Air Traffic Control")
                    ? EmailSenderAddress.GetSenderEmail("AirTrafficControlTraining")
                    : (model.ServiceType.Equals("Fire Protection")
                        ? EmailSenderAddress.GetSenderEmail("FireService")
                        : EmailSenderAddress.GetSenderEmail("SafetyTraining"));
                TempData["SuccessMsg"] = "Thank you for submitting us your query. We will get back to you shortly.";
                var genericEmailModel = new GenericEmailModel()
                {
                    Context = model.ServiceType
                };
                base.SendEmail(AutoReplyToUser(genericEmailModel), model.Email, "NO REPLY-FIA");
                base.SendEmail(GetMessageBody(model), to, $"Social Responsibility for {model.FirstName} {model.LastName}");
                return RedirectToCurrentUmbracoPage();
            }

            return CurrentUmbracoPage();
        }

        private string AutoReplyToUser(GenericEmailModel model)
        {
            return RenderPartialView("~/Views/Partials/EmailTemplates/_GenericEmailResponse.cshtml", model);
        }
        private string GetMessageBody(SocialResponsibilityModel model)
        {
            var messagebody = "<table>";
            if (!string.IsNullOrWhiteSpace(model.ServiceType))
            {
                messagebody += string.Format("<tr><td>ServiceType</td><td>{0}</td></tr>", model.ServiceType);
            }
            if (!string.IsNullOrWhiteSpace(model.Email))
            {
                messagebody += string.Format("<tr><td>Email</td><td>{0}</td></tr>", model.Email);
            }
            if (!string.IsNullOrWhiteSpace(model.Message))
            {
                messagebody += string.Format("<tr><td>Message</td><td>{0}</td></tr>", model.Message);
            }

            messagebody += "</table>";
            return messagebody;
        }
    }
}