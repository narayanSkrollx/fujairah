﻿
var ajaxRequest = function (url, method, options, callback) {
    var defaults = {
        disableCache: true,
        //dataType: 'JSON',
        //contentType: 'application/json; charset=UTF-8',
        enableLadda: false,
        targetLaddaElement: null,
        data: null
    }
    var opts = $.extend({}, defaults, options);
    var laddaElements = [];
    $.ajax({
        url: url,
        method: method,
        dataType: opts.dataType,
        contentType: opts.contentType,
        data: opts.data,
        cache: !opts.disableCache,
        async: true,
        beforeSend: function (n, t) {
            if (opts.enableLadda && opts.targetLaddaElement != null) {
                $(opts.targetLaddaElement).each(function (index, item) {
                    var laddaElement = Ladda.create(item);
                    laddaElement.start();
                    laddaElements.push(laddaElement);
                });

            }
        },
        complete: function (n, t) {
            if (opts.enableLadda && opts.targetLaddaElement != null) {
                $(laddaElements).each(function (index, item) {
                    item.stop();
                });
            }
        },
        success: function (response) {
            if ($.isFunction(callback)) {
                callback(response);
            } else {
                console.log('Invalid callback function set.');
            }
        },
        error: function (xhrResponse) {
            if ($.isFunction(callback)) {
                callback({ IsSuccess: false, IsRedirect: false, Message: xhrResponse.error, IsServerError: true });
            } else {
                console.log('Invalid callback function set.');
            }
        }
    });
};

(function () {
    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);