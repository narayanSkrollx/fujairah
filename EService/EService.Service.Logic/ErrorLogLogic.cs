using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class ErrorLogLogic : BusinessAccessBase<ErrorLog>
    {
        public ErrorLogLogic(ServiceContext context) : base(context)
        {
        }
    }
}