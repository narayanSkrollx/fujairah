using System;
using System.Collections.Generic;
using System.Linq;
using EService.Data.Entity.Entity;
using EService.Service.Logic.Enum;
using EService.Service.Model;
using EService.Core;
namespace EService.Service.Logic
{
    public class CranePermitRequestService : BaseEService<CranePermitRequest>
    {
        public CranePermitRequestService(ServiceContext context) : base(context)
        {
        }

        public override ServiceEnum Service => ServiceEnum.CranePermitRequest;
        public override bool HasExtraFormForApprovalReject()
        {
            return true;
        }

        public override ResponseObject IsValidForApproveReject(int userServiceRequestMasterId, int serviceRequestFlowStageDepartmentId,
            int departmentId)
        {

            var result = new ResponseObject();
            var serviceRequestFlowStageDepartment =
                Context.Database.ServiceRequestFlowStageDepartments.FirstOrDefault(
                    o => o.ServiceRequestFlowStageDepartmentId == serviceRequestFlowStageDepartmentId && o.ServiceRequestFlowStage.IsCurrentStage && o.DepartmentId == departmentId);
            if (serviceRequestFlowStageDepartment == null)
            {
                result.IsSuccess = false;
                result.Message =
                    "Invalid Service Flow Request. Either the department is invalid or is not current stage.";
            }
            else
            {
                var userServiceRequestMaster =
                    Context.Database.UserServiceRequests.FirstOrDefault(
                        o =>
                            !o.IsDeleted && o.UserServiceRequestMasterId == userServiceRequestMasterId);
                if (userServiceRequestMaster == null || userServiceRequestMaster.CranePermitRequest == null)
                {
                    result.IsSuccess = false;
                    result.Message = "Provided Service Request info is not a valid Crane Permit Request";
                }
                else
                {
                    var serviceFlowConfiguration =
                        Context.Database.ServiceApprovalStageDepartments.FirstOrDefault(
                            o => o.ServiceApprovalStage.ServiceId == (int)Service && departmentId == o.DepartmentId);
                    if (serviceFlowConfiguration == null)
                    {
                        result.IsSuccess = false;
                        result.Message = "Either Service Flow Configuration is not configured or does not exists.";
                    }
                    else
                    {
                        if (serviceFlowConfiguration.ServiceApprovalStage.Index == 1)
                        {
                            result.IsSuccess = userServiceRequestMaster.CranePermitRequest.CranePermitRequestFirstStageEntry != null;
                            result.Message = !result.IsSuccess ? "Planning Section is yet to save Penetration Assessment info." : "";
                        }
                        else if (serviceFlowConfiguration.ServiceApprovalStage.Index == 2)
                        {
                            result.IsSuccess =
                                userServiceRequestMaster.CranePermitRequest.CranePermitSafetyQAApproval != null;
                            result.Message = !result.IsSuccess
                                ? "QA and Safety Assessment info is yet to be saved."
                                : "";
                        }
                        else if (serviceFlowConfiguration.ServiceApprovalStage.Index == 3)
                        {
                            result.IsSuccess =
                                userServiceRequestMaster.ServieBill.ChargedAmount != 0.01 && userServiceRequestMaster.CranePermitRequest.IsInternal != null;
                            result.Message = !result.IsSuccess
                                ? "Finance User is yet to set amount and request type ."
                                : "";
                        }
                        else
                        {
                            result.IsSuccess = true;
                        }
                    }
                }
            }
            return result;
        }

        public void UpdateCranePemitRequest(CranePermitRequest model, bool isDraft, List<UserServiceRequestAttachment> addAttachments)
        {
            var request = Find(o => o.UserServiceRequestMasterId == model.UserServiceRequestMasterId);
            var list = request.CranePermitRequestSelectedCranes.ToList();
            Context.Database.CranePermitRequestSelectedCranes.RemoveRange(list);

            var removeDocList = request.UserServiceRequestMaster.UserServiceRequestAttachments.ToList();
            Context.Database.UserServiceRequestAttachment.RemoveRange(removeDocList);
            addAttachments.ForEach(o => request.UserServiceRequestMaster.UserServiceRequestAttachments.Add(o));


            request.UserServiceRequestMaster.IsDraft = isDraft;
            request.ApplicantEmail = model.ApplicantEmail;
            request.ApplicantName = model.ApplicantName;
            request.ApplicationDate = model.ApplicationDate;
            request.CompanyName = model.CompanyName;
            request.ContactNumber = model.ContactNumber;
            request.CraneLocation = model.CraneLocation;
            request.CraneOperatingCompany = model.CraneOperatingCompany;
            request.IsNewApplication = model.IsNewApplication;
            request.PurposeOfUser = model.PurposeOfUser;
            request.PreviousPermitNumbers = model.PreviousPermitNumbers;
            request.MaxOperatingHeight = model.MaxOperatingHeight;
            request.NorthingsEastings = model.NorthingsEastings;
            request.OperationStartDate = model.OperationStartDate;
            request.OperationEndDate = model.OperationEndDate;
            request.OperationStartTime = model.OperationStartTime;
            request.OperationEndTime = model.OperationEndTime;
            request.OperatorContactNumber = model.OperatorContactNumber;
            request.OperatorName = model.OperatorName;
            request.Position = model.Position;
            foreach (var craneType in model.CranePermitRequestSelectedCranes)
            {
                request.CranePermitRequestSelectedCranes.Add(craneType);
            }
            Context.Commit();
        }

        public ResponseObject SaveCraneRequestPlanningInfo(CraneRequestPlanningModel model, int id, int userId)
        {
            var result = new ResponseObject();

            var userServiceRequestMaster =
                Context.Database.UserServiceRequests.FirstOrDefault(o => o.UserServiceRequestMasterId == id);
            if (userServiceRequestMaster == null || userServiceRequestMaster.CranePermitRequest == null ||
                userServiceRequestMaster.CranePermitRequest.CranePermitRequestFirstStageEntry != null)
            {
                result.IsSuccess = false;
                result.Message = "Either user Service Request is invalid or is not a valid crane permit request.";
            }
            else
            {
                var attachments = (model.UploadedFiles ?? new List<UploadedFile>()).Select(o => new CranePermitPlanningAttachment
                {
                    Attachment = new UserAttachment
                    {
                        UserId = userId,
                        ServiceId = userServiceRequestMaster.ServiceId,
                        Timestamp = DateTime.Now.DubaiTime(),
                        ContentType = o.ContentType,
                        CreatedByUserId = userId,
                        CreatedDate = DateTime.Now.DubaiTime(),
                        FileName = o.FileName,
                        IsDeleted = false,
                        UserAttachmentData = new UserAttachmentData
                        {
                            FileContents = o.Content
                        },
                        ModifiedDate = DateTime.Now.DubaiTime(),
                        ModifiedByUserId = userId
                    },
                    UserAttachmentId = id,
                    UserServiceRequestMasterId = id
                }).ToList();
                userServiceRequestMaster.CranePermitRequest.CranePermitRequestFirstStageEntry = new CranePermitRequestFirstStageEntry()
                {
                    AirportPlanner = "",
                    AssessedBy = model.AssessedBy,
                    AssessedDate = model.AssessedDate,
                    Comments = model.Comments,
                    CraneOperatingDistanceFromRunway = model.CraneOperatingDistanceFromRunway,
                    HasPentrationOfSurface = model.HasPentrationOfSurface,
                    UserServiceRequestMasterId = id,
                    SavedByUserId = userId,
                    SavedDateTime = DateTime.Now.DubaiTime(),
                    RequireAviationLight = model.RequireAviationLight,
                    MaxCraneOperatingHeight = model.MaxCraneOperatingHeight,
                    PenetrationInMeter = model.PenetrationInMeter,
                    Recommendations = model.Recommendations,
                    CranePermitRequestFirstStageInfringementTypes = model.InfringementTypes.Where(o => o.Active).Select(p => new CranePermitRequestFirstStageInfringementType()
                    {
                        UserServiceRequestMasterId = id,
                        InfringementTypeId = p.Id,
                        OtherName = p.OtherName
                    }).ToList(),

                };
                Context.Database.CranePermitPlanningAttachments.AddRange(attachments);
                Context.Commit();
                result.IsSuccess = true;
                result.Message = "Crane Permit Request Planning info saved successfully.";
            }
            return result;
        }

        public ResponseObject SaveCraneRequestQASafetyInfo(CraneRequestQASafetyModel model, int id, int userId)
        {
            var result = new ResponseObject();

            var userServiceRequestMaster =
                Context.Database.UserServiceRequests.FirstOrDefault(o => o.UserServiceRequestMasterId == id);
            if (userServiceRequestMaster == null || userServiceRequestMaster.CranePermitRequest == null ||
                userServiceRequestMaster.CranePermitRequest.CranePermitSafetyQAApproval != null)
            {
                result.IsSuccess = false;
                result.Message = "Either user Service Request is invalid or is not a valid crane permit request.";
            }
            else
            {
                var attachments = (model.UploadedFiles ?? new List<UploadedFile>()).Select(o => new CranePermitSafetyFileAttachment
                {
                    Attachment = new UserAttachment
                    {
                        UserId = userId,
                        ServiceId = userServiceRequestMaster.ServiceId,
                        Timestamp = DateTime.Now.DubaiTime(),
                        ContentType = o.ContentType,
                        CreatedByUserId = userId,
                        CreatedDate = DateTime.Now.DubaiTime(),
                        FileName = o.FileName,
                        IsDeleted = false,
                        UserAttachmentData = new UserAttachmentData
                        {
                            FileContents = o.Content
                        },
                        ModifiedDate = DateTime.Now.DubaiTime(),
                        ModifiedByUserId = userId
                    },
                    UserAttachmentId = id,
                    UserServiceRequestMasterId = id
                }).ToList();
                userServiceRequestMaster.CranePermitRequest.CranePermitSafetyQAApproval = new CranePermitSafetyQAApproval()
                {

                    UserServiceRequestMasterId = id,
                    AdditionalComments = model.AdditionalComments,
                    ApprovedBy = model.ApprovedBy,
                    ApprovedByPosition = model.ApprovedByPosition,
                    ApprovedDateTime = model.ApprovedDateTime.Value,
                    CreatedByUserId = userId,
                    CreatedDate = DateTime.Now.DubaiTime(),
                    HasSafetyAssessmentBeenDone = model.HasSafetyAssessmentBeenDone,
                    IsDeleted = false,
                    OperatingRestrictions = model.OperatingRestrictions,
                    ModifiedByUserId = userId,
                    ModifiedDate = DateTime.Now.DubaiTime(),
                   Attachments = attachments.ToList()
                };
                Context.Commit();
                result.IsSuccess = true;
                result.Message = "Crane Permit Request QA Safety info saved successfully.";
            }
            return result;
        }

        public ResponseObject SaveCraneRequestFinanceInfo(CraneRequestFinanceModel model, int id, int userId)
        {
            var result = new ResponseObject();

            var userServiceRequestMaster =
                Context.Database.UserServiceRequests.FirstOrDefault(o => o.UserServiceRequestMasterId == id);
            if (userServiceRequestMaster == null || userServiceRequestMaster.CranePermitRequest == null ||
                userServiceRequestMaster.CranePermitRequest.IsInternal != null)
            {
                result.IsSuccess = false;
                result.Message = "Either user Service Request is invalid or is not a valid crane permit request.";
            }
            else
            {
                userServiceRequestMaster.CranePermitRequest.IsInternal = model.IsInternal;
                userServiceRequestMaster.ServieBill.ChargedAmount = model.ServiceChargeAmount;
                if (model.IsInternal == true && model.ServiceChargeAmount == 0)
                {
                    //set payment completed, since this is the last step of approval and is done by finance user

                }
                Context.Commit();
                result.IsSuccess = true;
                result.Message = "Crane Permit Request Finance info saved successfully.";
            }
            return result;
        }
    }
}