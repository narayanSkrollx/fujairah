using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class LandingPermissionRequestConfiguration : EntityTypeConfiguration<LandingPermissionRequest>
    {
        public LandingPermissionRequestConfiguration()
        {
            ToTable("LandingPermissionRequest");

            HasKey(o => o.UserServiceRequestMasterId);

            Property(o => o.UserServiceRequestMasterId).IsRequired();

            Property(o => o.AirlineName).IsRequired().HasMaxLength(1024);
            Property(o => o.Email).IsRequired().HasMaxLength(1024);
            Property(o => o.Telephone).IsRequired().HasMaxLength(1024);
            Property(o => o.Fax).IsOptional().HasMaxLength(1024);
            Property(o => o.Address).IsOptional().HasMaxLength(1024);
            Property(o => o.OperatorName).IsRequired().HasMaxLength(1024);
            Property(o => o.OperatorEmail).IsRequired().HasMaxLength(1024);
            Property(o => o.OperatorTelephone).IsRequired().HasMaxLength(1024);
            Property(o => o.Fax).IsOptional().HasMaxLength(1024);
            Property(o => o.OperatorAddress).IsOptional().HasMaxLength(1024);

            Property(o => o.FlightNatureOfOperationId).IsRequired();
            Property(o => o.AircraftRegistration).IsRequired().HasMaxLength(1024);
            Property(o => o.TypeOfAircraft).IsRequired().HasMaxLength(1024);
            Property(o => o.AirportLandingId).IsRequired();
            Property(o => o.MTOW).IsRequired();
            Property(o => o.Remarks).IsOptional().HasColumnType("nvarchar(max)");

            HasRequired(o => o.UserServiceRequestMaster).WithRequiredDependent(o => o.LandingPermissionRequest).WillCascadeOnDelete(false);

            HasRequired(o => o.FlightNatureOfOperation)
                .WithMany(o => o.LandingPermissionRequests)
                .HasForeignKey(o => o.FlightNatureOfOperationId).WillCascadeOnDelete(false);
            HasRequired(o => o.AirportLanding)
                .WithMany(o => o.LandingPermissionRequests)
                .HasForeignKey(o => o.AirportLandingId).WillCascadeOnDelete(false);
        }
    }
}