namespace EService.Web.Helper
{
    public enum FIAEmailType
    {
        New,
        Approved,
        Rejected,
        Completed
    }
}