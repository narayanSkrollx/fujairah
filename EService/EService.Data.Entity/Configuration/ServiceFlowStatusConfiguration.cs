using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class ServiceFlowStatusConfiguration : EntityTypeConfiguration<ServiceFlowStatus>
    {
        public ServiceFlowStatusConfiguration()
        {
            ToTable("ServiceFlowStatus");

            HasKey(o => o.Id);
            Property(o => o.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}