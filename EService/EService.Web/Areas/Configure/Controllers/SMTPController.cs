﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Web.Controllers.Base;
using EService.Web.Helper;
using EService.Web.Models;

namespace EService.Web.Areas.Configure.Controllers
{
    public class SMTPController : SystemAdminBaseController
    {
        private SMTPEmailSettingService smtpEmailSettingService;


        public SMTPController()
        {
            smtpEmailSettingService = new SMTPEmailSettingService(serviceContext);
        }

        // GET: Configure/SMTP
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(SMTPEmailSetting model)
        {
            var result = new JsonResponseObject();
            try
            {
                if (ModelState.IsValid)
                {
                    var existingModel = smtpEmailSettingService.Find(o => !o.IsDeleted);
                    if (existingModel != null)
                    {
                        existingModel.From = model.From;
                        existingModel.Host = model.Host;
                        existingModel.Port = model.Port;
                        existingModel.UserName = model.UserName;
                        existingModel.Password = model.Password;
                        existingModel.EnableSsl = model.EnableSsl;
                    }
                    else
                    {
                        smtpEmailSettingService.Add(model);
                    }
                    serviceContext.Commit();
                    result.IsSuccess = true;

                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = ModelState.GetModelStateError();
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving SMTP Setting.";
            }
            return Json(result);
        }

        public JsonResult Initialize()
        {
            var result = new JsonResponseObject();

            try
            {
                var model = smtpEmailSettingService.Find(o => !o.IsDeleted);
                result.Data = model ?? new SMTPEmailSetting();
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                LogError(ex);
                result.Message = "";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}