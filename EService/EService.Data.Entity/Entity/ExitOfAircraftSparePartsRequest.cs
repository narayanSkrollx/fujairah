﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class ExitOfAircraftSparePartsRequest : AuditableEntity, IServiceEntity
    {
        public int UserServiceRequestMasterId { get; set; }

        public DateTime DateOfApplication { get; set; }
        public string ApplicantCompany { get; set; }
        public string AircraftType { get; set; }
        public string AircraftRegistrationNumber { get; set; }
        public string OwnerOfAircraft { get; set; }
        public bool? HasNoObjectionLetter { get; set; }
        public string NoObjectionLetterOfTheOwner { get; set; }
        public bool? AircraftMaintenanceOrganisationOrCar145GCAARegisteredCo { get; set; }
        public string ApplicantMailIdOrContactDetails { get; set; }

        [JsonIgnore]
        public virtual UserServiceRequestMaster UserServiceRequestMaster { get; set; }
        [JsonIgnore]
        public virtual ICollection<ExitSparePartsPassChecklist> ExitAndReturnPassChecklists { get; set; }
    }
}
