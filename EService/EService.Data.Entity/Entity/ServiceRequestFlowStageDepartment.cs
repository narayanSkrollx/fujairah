using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class ServiceRequestFlowStageDepartment
    {
        public int ServiceRequestFlowStageDepartmentId { get; set; }
        public int ServiceRequestFlowStageId { get; set; }
        public int DepartmentId { get; set; }
        public int? DepartmentUserId { get; set; }
        public int Status { get; set; }
        public DateTime? StatusChangedDateTime { get; set; }
        public bool? IsApproved { get; set; }
        public bool IsOverriddenFromAdmin { get; set; }
        public string Remarks { get; set; }
        public bool IsAssignedAfterReject { get; set; }

        [JsonIgnore]
        public virtual ServiceRequestFlowStage ServiceRequestFlowStage { get; set; }

        [JsonIgnore]
        public virtual ICollection<ServiceRequestFlowStageDepartmentHistory> ServiceRequestFlowStageDepartmentHistories
        {
            get;
            set;
        }

        [JsonIgnore]
        public virtual Department Department { get; set; }

        [JsonIgnore]
        public virtual User DepartmentUser { get; set; }
        
    }
}