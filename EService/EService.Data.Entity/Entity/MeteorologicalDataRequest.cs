using System;
using System.Collections.Generic;

namespace EService.Data.Entity.Entity
{
    public class MeteorologicalDataRequest : AuditableEntity
    {
        public int UserServiceRequestMasterId { get; set; }

        #region "Contact Information"

        public string Email { get; set; }
        public string Department { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Address { get; set; }

        #endregion

        public bool DataRangeDaily { get; set; }
        public bool DataRangeHourly { get; set; }
        public bool DataRangeMonthly { get; set; }
        public bool DataRangeYearly { get; set; }
        public bool DataRangeSelectPeriod { get; set; }
        public DateTime? CustomRangeStartDate { get; set; }
        public DateTime? CustomRangeEndDate { get; set; }

        public string METDepartmentRemrks { get; set; }

        public virtual UserServiceRequestMaster UserServiceRequestMaster { get; set; }

        public virtual ICollection<MeteorologicalDataRequestWeatherType> MeteorologicalDataRequestWeatherTypes
        {
            get;
            set;
        }

        public virtual ICollection<MeteorologicalDataRequestWeatherElement> MeteorologicalDataRequestWeatherElements
        {
            get; set;
        }
    }
}