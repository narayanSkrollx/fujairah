using System;

namespace EService.Data.Entity.Entity
{
    public class ErrorLog
    {
        public int ErrorLogId { get; set; }

        public DateTime Timestamp { get; set; }
        public string File { get; set; }
        public string Method { get; set; }
        public string Exception { get; set; }
        public string InnerException { get; set; }
        public string StackTrace { get; set; }
        public String Source { get; set; }
        public string Data { get; set; }
        public string ClientIp { get; set; }
        public string ClientBrowserAgent { get; set; }

        public int UserId { get; set; }
        public string UserName { get; set; }
    }
}