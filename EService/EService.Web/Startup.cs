﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(EService.Web.Startup))]
namespace EService.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}