using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class ApplicantOwnerRelationshipService : BusinessAccessBase<ApplicantOwnerRelationship>
    {
        public ApplicantOwnerRelationshipService(ServiceContext context) : base(context)
        {
        }
    }
}