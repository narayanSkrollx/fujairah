using System;
using System.Linq;
using System.Web.Mvc;
using EService.Data.Entity;
using EService.Service.Logic;
using EService.Web.Helper;
using EService.Web.Models.ServiceNotification;

namespace EService.Web.Controllers.Base
{
    public class SystemAdminBaseController : BaseController
    {
        private UserServiceRequestMasterService userServiceRequestMasterService;
        private SMTPEmailSettingService emailSettingService;
        private UserService userService;
        private EmailService emailService;
        private ServiceRequestFlowStageDepartmentService serviceRequestFlowStageDepartmentService;


        public SystemAdminBaseController()
        {
            userServiceRequestMasterService = new UserServiceRequestMasterService(serviceContext);
            emailSettingService = new SMTPEmailSettingService(serviceContext);
            userService = new UserService(serviceContext);
            emailService = new EmailService(serviceContext);
            serviceRequestFlowStageDepartmentService = new ServiceRequestFlowStageDepartmentService(serviceContext);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            if (UserProfile.UserType != UserTypeEnum.SystemAdmin)
            {
                filterContext.Result = UnAuthroizedResult();
            }

        }

        protected void TriggerEmail(FIAEmailType emailType, int serviceFlowRequestDepartmentId)
        {
            //var userServiceRequest = userServiceRequestMasterService.GetServiceRequest(userServiceRequestMasterId);
            var smtpSetting = emailSettingService.Find(o => true);
            var allUsers = userService.FindAllAsNoTracking(o => !o.IsDeleted).ToList();
            var receipients = emailService.GetApproveRejectEmailRecepients(serviceFlowRequestDepartmentId);
            var serviceFlowRequest =
             serviceRequestFlowStageDepartmentService.Find(
                 o => o.ServiceRequestFlowStageDepartmentId == serviceFlowRequestDepartmentId);
            var htmlContent = "";
            switch (emailType)
            {
                case FIAEmailType.Approved:
                    //var serviceFlowRequest =
                    //    serviceRequestFlowStageDepartmentService.Find(
                    //        o => o.ServiceRequestFlowStageDepartmentId == serviceFlowRequestDepartmentId);
                    var amodel = new ApprovedServiceRequestNotificationViewModel()
                    {
                        ServiceId = serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster.ServiceId,
                        ServiceName = serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster.Service.ServiceName,
                        RequestedByUserName = serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster.User.UserName,
                        RequestedByUserEmail = serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster.User.Email,
                        RequestRegistrationNo = serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster.RegistrationNo,
                        RequestedDateTime = serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster.RequestDateTime,
                        RequestedByUserId = serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster.UserId,
                        ApprovedByDepartmentUserName = serviceFlowRequest.DepartmentUserId.ToString(),
                        ApprovedByDepartmentDateTime = serviceFlowRequest.StatusChangedDateTime ?? DateTime.MinValue

                    };
                    htmlContent = RenderPartialView("_AcceptedServiceRequestEmailTemplate", amodel);
                    break;
                case FIAEmailType.Rejected:


                    var model = new RejectedServiceRequestNotificationViewModel()
                    {
                        ServiceId = serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster.ServiceId,
                        ServiceName = serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster.Service.ServiceName,
                        RequestedByUserName = serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster.User.UserName,
                        RequestedByUserEmail = serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster.User.Email,
                        RequestRegistrationNo = serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster.RegistrationNo,
                        RequestedDateTime = serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster.RequestDateTime,
                        RequestedByUserId = serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster.UserId,
                        RejectedByDepartmentUserName = serviceFlowRequest.DepartmentUserId.ToString(),
                        IsPassedToUser = !serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster.ServiceRequestFlowStages.Any(o => o.IsCurrentStage),
                        RejectedByDepartmentUserId = serviceFlowRequest.DepartmentUserId ?? 0,
                        RejectedByDepartmentDateTime = serviceFlowRequest.StatusChangedDateTime ?? DateTime.MinValue

                    };
                    htmlContent = RenderPartialView("_RejectedServiceRequestEmailTemplate", model);
                    break;
                default:
                    break;
            }
            EmailHelper.SendEmail(smtpSetting.From, receipients, new string[] { }, new string[] { }, emailType.ToString(), htmlContent, smtpSetting);
        }
    }
}