﻿$(function () {
    fia.CreateNamespace("serviceRequestFinance");

    (function (context) {
        context.Title = 'Service Request';
        context.ViewModel = {}

        context.Mapping = {
            create: function (options) {
                var vm = ko.mapping.fromJS(options.data);

                vm.SearchClick = function () {
                    context.Search();
                }

                vm.ViewDetails = function (item) {
                    var jsItem = ko.toJS(item);
                    window.location = '/Finance/UserServiceRequest/Detail?id=' + jsItem.UserServiceRequestMasterId;
                }

                return vm;
            }
        }

        context.DetailMapping = {
            create: function (options) {
                var vm = ko.mapping.fromJS(options.data);

                vm.ApproveClick = function () {
                    context.Approve();
                }

                return vm;
            }
        }

        context.Search = function () {
            var searchModel = ko.toJS(context.ViewModel.ServiceInfoVM.SearchModel);

            searchModel.DateFrom = moment(searchModel.DateFrom).isValid() ? moment(searchModel.DateFrom).format('YYYY-MM-DD') : null;
            searchModel.DateTo = moment(searchModel.DateTo).isValid() ? moment(searchModel.DateTo).format('YYYY-MM-DD') : null;
            delete searchModel.ServiceStatusList;

            ajaxRequest('/Finance/UserServiceRequest/Search/', 'POST',
                {
                    data: { searchModel: searchModel },
                    enableLadda: true,
                    targetLaddaElement: '[data-role=search]'
                }, function (response) {
                    if (response.IsSuccess) {
                        ko.mapping.fromJS(response.Data, {}, context.ViewModel.ServiceInfoVM.Records);
                    } else {
                        showMessage(context.Title, response.Message, 'error');
                    }
                });
        }

        context.Initialize = function () {
            ajaxRequest('/Finance/UserServiceRequest/Initialize/', 'GET', {}, function (response) {
                if (response.IsSuccess) {
                    if (!ko.dataFor($('#mainContent')[0])) {
                        context.ViewModel.ServiceInfoVM = ko.mapping.fromJS(response.Data, context.Mapping);

                        ko.applyBindings(context.ViewModel.ServiceInfoVM, $('#mainContent')[0]);
                    } else {
                        ko.mapping.fromJS(response.Data, {}, context.ViewModel.ServiceInfoVM);
                    }
                } else {
                    showMessage(context.Title, response.Message, 'error');
                }
            });
        }

        context.InitializeDetail = function () {
            ajaxRequest('/Finance/UserServiceRequest/InitializeDetail/', 'GET', {}, function (response) {
                if (response.IsSuccess) {
                    $('#detailContent').html(response.Data.Html);
                    if (context.ViewModel.Detail == null) {
                        context.ViewModel.Detail = ko.mapping.fromJS(response.Data.Data, context.DetailMapping);
                        context.ViewModel.Detail.BillData = ko.mapping.fromJS(response.Data.BillData);

                        ko.applyBindings(context.ViewModel.Detail, $('#serviceStatus')[0]);//billContent
                        ko.applyBindings(context.ViewModel.Detail, $('#billContent')[0]);//billContent
                        ko.applyBindings(context.ViewModel.Detail, $('[data-role=navigation-up]')[0]);
                        ko.applyBindings(context.ViewModel.Detail, $('[data-role=navigation-down]')[0]);//uploadedFileContent
                        ko.applyBindings(context.ViewModel.Detail, $('#uploadedFileContent')[0]);//uploadedFileContent
                    } else {
                        ko.mapping.fromJS(response.Data.Data, {}, context.ViewModel.Detail);
                    }
                } else {
                    showMessage(context.Title, response.Message, 'error');
                }
            });
        }

        context.Approve = function () {
            var model = ko.toJS(context.ViewModel.Detail);
            var id = model.UserServiceRequestMasterId;
            ajaxRequest('/Finance/UserServiceRequest/Approve/', 'POST', {
                data: { id: id },
                enableLadda: true,
                targetLaddaElement: '[data-role=approve]'
            }, function (response) {
                if (response.IsSuccess) {
                    showMessage(context.Title, response.Message, 'success');
                    setTimeout(function() {
                        window.location = '/Finance/UserServiceRequest/Index';
                    }, 500);
                } else {
                    showMessage(context.Title, response.Message, 'error');
                }
            });
        }
    })(fia.serviceRequestFinance);
});