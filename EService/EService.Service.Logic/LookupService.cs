﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EService.Data.Entity;

namespace EService.Service.Logic
{
    public class LookupService
    {
        public IEnumerable<LookupItem> GetList<T>() where T : class, ILookupItem
        {
            var context = new ServiceContext();
            return context.Database.Set<T>()
                .Where(e => e.Active)
                .Select(o => new LookupItem()
                {
                    Id = o.Id,
                    Description = o.Description,
                    Active = o.Active
                })
                .ToList();
        }

        public IEnumerable<T> GetGenericList<T>() where T : class, ILookupItem
        {
            var context = new ServiceContext();
            return context.Database.Set<T>()
                .Where(e => e.Active).ToList();
        }
    }
}
