using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class CranePermitRequestUserAttachmentConfiguration :
        EntityTypeConfiguration<CranePermitRequestUserAttachment>
    {
        public CranePermitRequestUserAttachmentConfiguration()
        {
            ToTable("CranePermitRequestUserAttachmentConfiguration");

            HasKey(o => o.CranePermitRequestUserAttachmentId);
            Property(o => o.CranePermitRequestUserAttachmentId)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.UserAttachmentId).IsRequired();
            Property(o => o.UserServiceRequestMasterId).IsRequired();

            HasRequired(o => o.UserAttachment)
                .WithMany(o => o.CranePermitRequestUserAttachments)
                .HasForeignKey(o => o.UserAttachmentId).WillCascadeOnDelete(false);
            HasRequired(o => o.CranePermitRequest).WithMany(o => o.CranePermitRequestUserAttachments)
                .HasForeignKey(o => o.UserServiceRequestMasterId).WillCascadeOnDelete(false);
        }
    }
}