﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EService.Data.Entity.Entity;
using Newtonsoft.Json;

namespace EService.Data.Entity.Configuration
{
    public class BuildingHeightRequestConfiguration : EntityTypeConfiguration<BuildingHeightRequest>
    {
        public BuildingHeightRequestConfiguration()
        {
            ToTable("BuildingHeightRequest");
            HasKey(x => x.UserServiceRequestMasterId);

            Property(x => x.UserServiceRequestMasterId).IsRequired();


            Property(x => x.ProjectName).IsRequired().HasMaxLength(225);
            Property(x => x.ApplicationDate).IsOptional();
            Property(x => x.ExpectedConstructionStartDate).IsOptional();
            Property(x => x.ExpectedConstructionEndDate).IsOptional();
            Property(x => x.Location).IsOptional().HasMaxLength(225);
            Property(x => x.ProjectDescription).IsOptional().HasMaxLength(225);
            Property(x => x.ApplicantSignature).IsOptional().HasMaxLength(225);
            Property(x => x.ApplicantName).IsOptional().HasMaxLength(225);
            Property(x => x.ApplicantJobTitle).IsOptional().HasMaxLength(225);
            Property(x => x.ApplicantCompanyName).IsOptional().HasMaxLength(225);
            Property(x => x.ApplicantAddress).IsOptional().HasMaxLength(225);
            Property(x => x.ApplicantCell).IsOptional().HasMaxLength(225);
            Property(x => x.ApplicantTel).IsOptional().HasMaxLength(225);
            Property(x => x.ApplicantEmail).IsOptional().HasMaxLength(225);
            Property(x => x.ApplicantWebsite).IsOptional().HasMaxLength(225);
            Property(x => x.OwnerName).IsOptional().HasMaxLength(225);
            Property(x => x.OwnerCompanyName).IsOptional().HasMaxLength(225);
            Property(x => x.OwnerAddress).IsOptional().HasMaxLength(225);
            Property(x => x.OwnerCell).IsOptional().HasMaxLength(225);
            Property(x => x.OwnerTel).IsOptional().HasMaxLength(225);
            Property(x => x.OwnerEmail).IsOptional().HasMaxLength(225);
            Property(x => x.OwnerWebsite).IsOptional().HasMaxLength(225);
            Property(x => x.IsBuilding).IsOptional();
            Property(x => x.IsOHLCommunicationTower).IsOptional();
            Property(x => x.IsOtherType).IsOptional();
            Property(x => x.OtherTypeDescription).IsOptional().HasMaxLength(225);
            Property(x => x.BuildingType).IsOptional();
            Property(x => x.NumberOfBuildings).IsOptional();
            Property(x => x.NumberOfFloors).IsOptional();
            Property(x => x.TallestHeight).IsOptional();
            Property(x => x.HasObstacleLight).IsOptional();
            Property(x => x.DetailsOfLightinDrawing).IsOptional().HasMaxLength(225);
            Property(x => x.OHLOrCommunicationType).IsOptional();
            Property(x => x.NumberOfStructure).IsOptional();
            Property(x => x.TypeOfOHLCommTower).IsOptional().HasMaxLength(225);
            Property(x => x.AreThereMountains).IsOptional();

            Property(o => o.ApplicantOwnerRelationshipId).IsRequired();
            Property(o => o.ApplicantOwnerRelationshipOtherName).IsOptional().HasMaxLength(255);
            Property(o => o.OtherProjectTypeDescription).IsOptional().HasMaxLength(255);
            Property(o => o.ProjectTypeId).IsRequired();

            HasRequired(o => o.ProjectType).WithMany().HasForeignKey(o => o.ProjectTypeId).WillCascadeOnDelete(false);
            HasRequired(o => o.ApplicantOwnerRelationship).WithMany(o => o.BuildingHeightRequests).HasForeignKey(o => o.ApplicantOwnerRelationshipId).WillCascadeOnDelete(false);
            HasRequired(x => x.UserServiceRequestMaster).WithRequiredDependent(x => x.BuildingHeightRequest).WillCascadeOnDelete(false);
        }
    }

    public class BuildingHeightRequestPlanningApprovalConfiguration : EntityTypeConfiguration<BuildingHeightPlanningApproval>
    {
        public BuildingHeightRequestPlanningApprovalConfiguration(string schema = "dbo")
        {
            ToTable("BuildingHeightPlanningApproval", schema);

            HasKey(o => o.UserServiceRequestMasterId);

            Property(o => o.UserServiceRequestMasterId).IsRequired();
            Property(o => o.Remarks).IsOptional().HasMaxLength(1024);

            HasRequired(o => o.BuildingHeightRequest).WithRequiredDependent(o => o.BuildingHeightPlanningApproval).WillCascadeOnDelete(false);

        }
    }

    public class BuildinHeightRequestPlanningApprovalAttachmentConfiguration : EntityTypeConfiguration<BuildingHeightPlanningApprovalAttachment>
    {
        public BuildinHeightRequestPlanningApprovalAttachmentConfiguration(string schema = "dbo")
        {
            ToTable("BuildingHeightPlanningApprovalAttachment");

            HasKey(o => o.BuildingHeightPlanningApprovalAttachmentId);

            Property(o => o.BuildingHeightPlanningApprovalAttachmentId).IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(o => o.UserServiceRequestMasterId).IsRequired();
            Property(o => o.UserAttachmentId).IsRequired();

            HasRequired(o => o.PlanningApproval).WithMany(o => o.Attachments).HasForeignKey(o => o.UserServiceRequestMasterId).WillCascadeOnDelete(false);
            HasRequired(o => o.Attachment).WithMany().HasForeignKey(o => o.UserAttachmentId).WillCascadeOnDelete(false);

        }
    }
}
