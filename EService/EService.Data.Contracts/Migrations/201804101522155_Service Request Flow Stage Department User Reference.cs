namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ServiceRequestFlowStageDepartmentUserReference : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.ServiceRequestFlowStageDepartment", "DepartmentUserId");
            AddForeignKey("dbo.ServiceRequestFlowStageDepartment", "DepartmentUserId", "dbo.User", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ServiceRequestFlowStageDepartment", "DepartmentUserId", "dbo.User");
            DropIndex("dbo.ServiceRequestFlowStageDepartment", new[] { "DepartmentUserId" });
        }
    }
}
