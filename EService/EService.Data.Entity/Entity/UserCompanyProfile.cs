using System.Globalization;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class UserCompanyProfile
    {
        public int UserId { get; set; }

        public int CompanyCategoryId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string POBox { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string VATRegistrationNumber { get; set; }

        [JsonIgnore]
        public virtual User User { get; set; }
        [JsonIgnore]
        public CompanyCategory CompanyCategory { get; set; }
    }
}