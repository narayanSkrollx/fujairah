﻿using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Service.Logic.Enum;
using EService.Web.Helper;
using EService.Web.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using EService.Web.Controllers.Base;
using EService.Web.Models.ServiceRequestOperation;
using Nelibur.ObjectMapper;
using System.Collections.Generic;
using EService.Core;
using EService.Web.Helper.EmailSender;

namespace EService.Web.Controllers
{
    public class AircraftMaintenanceRequestController : ServiceRequestBaseController
    {
        private AircraftMaintenanceRequestService aircraftMaintenanceRequestService;
        private ServiceApprovalStageService serviceApprovalStageService;
        private UserServiceRequestMasterService userRequestService;
        private ServiceInfoService serviceInfoService;
        private UserService userService;

        public AircraftMaintenanceRequestController()
        {
            serviceApprovalStageService = new ServiceApprovalStageService(serviceContext);
            userRequestService = new UserServiceRequestMasterService(serviceContext);
            aircraftMaintenanceRequestService = new AircraftMaintenanceRequestService(serviceContext);
            serviceInfoService = new ServiceInfoService(serviceContext);
            userService = new UserService(serviceContext);
        }

        public override ServiceEnum Service => ServiceEnum.AircraftMaintenanceRequest;

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(AircraftMaintenanceRequestViewModel model, bool isDraft = false)
        {
            var result = new JsonResponseObject();
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.UserServiceRequestMasterId > 0)
                    {
                        var contract = MapViewModelToContract(model);
                        var attachmentList = new List<UserServiceRequestAttachment>();
                        #region "Uploaded Files Edit"
                        if (model.EditUploadedFiles != null && model.EditUploadedFiles.Any())
                        {
                            attachmentList.AddRange(model.EditUploadedFiles.Where(o => o.ObjectState != ObjectState.Deleted).Select(o => new UserServiceRequestAttachment { UserAttachmentId = o.Id }));

                        }
                        if (model.UploadedFiles != null && model.UploadedFiles.Any())
                        {
                            attachmentList.AddRange(model.UploadedFiles.Select(o => new UserServiceRequestAttachment { UserAttachmentId = o.Id }));

                        }

                        #endregion
                        var updateResult = aircraftMaintenanceRequestService.UpdateAircraftMaintenanceRequest(contract, isDraft, attachmentList);
                        result.IsSuccess = updateResult.IsSuccess;
                        result.Message = updateResult.Message;

                        if (!isDraft && updateResult.IsSuccess)
                        {
                            //TriggerEmail(FIAEmailType.New, model.UserServiceRequestMasterId);
                            EmailSenderHelper.SendNewRequestEmail(ControllerContext, model.UserServiceRequestMasterId);
                        }
                    }
                    else
                    {
                        var serviceRequest = SaveAircraftMaintenanceRequest(model, isDraft);
                        if (!isDraft)
                        {
                            //TriggerEmail(FIAEmailType.New, serviceRequest.UserServiceRequestMasterId);
                            EmailSenderHelper.SendNewRequestEmail(ControllerContext, serviceRequest.UserServiceRequestMasterId);
                        }
                        result.IsSuccess = true;
                        result.Message = "Aircraft Maintenance Request saved successfully";
                    }
                }
                else
                {
                    result.Message = "Server Validation Error: " + ModelState.GetModelStateError();
                    result.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving Aircraft Maintenance Request.";
            }
            return Json(result);
        }

        public ActionResult Detail(int id)
        {
            ViewBag.UserServiceRequestMasterId = id;
            return View();
        }

        public ActionResult Edit(int id)
        {
            ViewBag.UserServiceRequestMasterId = id;
            return View("Create");
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Initialize()
        {
            var result = new JsonResponseObject();
            try
            {
                var data = userRequestService.GetServiceRequests(UserProfile.UserId, ServiceId);
                result.Data = data;
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "Error while obtaining list";
                LogError(ex);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InitializeCreate(int? id)
        {
            try
            {
                var model = new AircraftMaintenanceRequestViewModel()
                {
                };
                var reapplyData = TempData[Constants.ServiceRequestReapplyContentTempDataKey] as ServiceReapplyTempDataViewModel;
                if (id.HasValue)
                {
                    model = GetAircraftMaintenanceRequest(id.Value);
                }
                else
                {
                    model.DateOfApplication = DateTime.Now.DubaiTime();
                    if (reapplyData != null)
                    {
                        var userServiceRequest = userRequestService.Find(o => o.UserServiceRequestMasterId == reapplyData.UserServiceRequestMasterId &&
                                      !o.IsDeleted && o.UserId == UserProfile.UserId && o.ServiceId == ServiceId);

                        if (userServiceRequest == null || userServiceRequest.AircraftMaintenanceRequest == null)
                        {
                            return Json(new JsonResponseObject() { IsSuccess = false, Message = "Invalid User Service Request for reapply." }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            var aircraftMaintenance = userServiceRequest.AircraftMaintenanceRequest;


                            TinyMapper.Bind<AircraftMaintenanceRequest, AircraftMaintenanceRequestViewModel>();

                            var aircraftMaintenanceViewModel = TinyMapper.Map<AircraftMaintenanceRequestViewModel>(aircraftMaintenance);

                            aircraftMaintenanceViewModel.ParentUserServiceRequestMasterid = reapplyData.UserServiceRequestMasterId;
                            aircraftMaintenanceViewModel.UserServiceRequestMasterId = 0;

                            model = aircraftMaintenanceViewModel;
                        }
                    }
                }

                model.UserInfo = userService.GetUserBasicInfo(UserProfile.UserId).Data;
                model.EditUploadedFiles = GetUploadedFiles(reapplyData?.UserServiceRequestMasterId ?? (id ?? 0));

                return Json(new JsonResponseObject() { IsSuccess = true, Data = model }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return Json(new JsonResponseObject() { IsSuccess = false, Message = "Error" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InitializeDetail(int id)
        {
            try
            {
                var model = GetAircraftMaintenanceRequest(id);
                return Json(new JsonResponseObject() { IsSuccess = true, Data = model }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return Json(new JsonResponseObject() { IsSuccess = false, Message = "Error" });
        }


        public ActionResult Reapply()
        {
            return View("Create");
        }

        #region "Private Methods"

        public AircraftMaintenanceRequestViewModel GetAircraftMaintenanceRequest(int id)
        {
            var contract = aircraftMaintenanceRequestService.Find(x => x.UserServiceRequestMasterId == id);
            var model = MapContractToViewModel(contract);
            return model;
        }

        public UserServiceRequestMaster SaveAircraftMaintenanceRequest(AircraftMaintenanceRequestViewModel model, bool isDraft = false)
        {
            var serviceFlowStages = serviceApprovalStageService.FindAllAsNoTracking(o => o.ServiceId == ServiceId).ToList();
            var serviceCode = userRequestService.GetNewServiceCode(ServiceId);
            var serviceInfo = serviceInfoService.Find(o => o.ServiceId == ServiceId);
            var userServiceRequest = new UserServiceRequestMaster()
            {
                RequestDateTime = DateTime.Now.DubaiTime(),
                ServiceId = ServiceId,
                Status = (int)ServiceFlowStateEnum.New,
                UserId = UserProfile.UserId,
                IsDeleted = false,
                RegistrationNoYear = serviceCode.Data.Year,
                RegistrationNoMonth = serviceCode.Data.Month,
                RegistrationNoIndex = serviceCode.Data.RegistrationNoIndex,
                RegistrationNoServiceCode = serviceCode.Data.ServiceCode,
                RegistrationNo = serviceCode.Data.RegistrationNo,
                ParentUserServiceRequestMasterId = model.ParentUserServiceRequestMasterid,
                IsDraft = isDraft,
                VAT = serviceInfo.VATPercentage,
                ServiceRequestFlowStages = serviceFlowStages
                .Select(o => new ServiceRequestFlowStage()
                {
                    Index = o.Index,
                    IsCurrentStage = o.Index == 1,
                    ServiceRequestFlowStageDepartments = o.ServiceApprovalStageDepartments.Select(p =>
                    new ServiceRequestFlowStageDepartment()
                    {
                        DepartmentId = p.DepartmentId,
                        Status = (int)ServiceFlowStateEnum.New,
                        IsApproved = false,
                    }).ToList()
                }).ToList(),
                UserServiceRequestAttachments = model.UploadedFiles.Select(o => new UserServiceRequestAttachment()
                {
                    UserAttachmentId = o.Id
                }).ToList(),
                ServieBill = new UserServiceRequestBill()
                {
                    ChargedAmount = (double)serviceInfo.DefaultRate
                }
            };
            var request = MapViewModelToContract(model);
            request.UserServiceRequestMaster = userServiceRequest;
            aircraftMaintenanceRequestService.Add(request);
            serviceContext.Commit();
            return userServiceRequest;
        }

        private AircraftMaintenanceRequestViewModel MapContractToViewModel(AircraftMaintenanceRequest contract)
        {
            var model = new AircraftMaintenanceRequestViewModel
            {
                UserServiceRequestMasterId = contract.UserServiceRequestMasterId,
                AddressOfMaintenanceCompany = contract.AddressOfMaintenanceCompany,
                AircraftModel = contract.AircraftModel,
                AircraftRegistrationNumber = contract.AircraftRegistrationNumber,
                AircraftType = contract.AircraftType,
                ApplicantRegisteredMaintenanceCompany = contract.ApplicantRegisteredMaintenanceCompany,
                DateOfApplication = contract.DateOfApplication,
                Designation = contract.Designation,
                DetailsOfMaintenance = contract.DetailsOfMaintenance,
                EmailId = contract.EmailId,
                MaintenanceCompanyTradeLicenseExpiryDate = contract.MaintenanceCompanyTradeLicenseExpiryDate,
                MaintenanceCompanyTradeLicenseNumber = contract.MaintenanceCompanyTradeLicenseNumber,
                NameOfApplicant = contract.NameOfApplicant,
                Part145RegistrationExpiryDate = contract.Part145RegistrationExpiryDate,
                Part145RegistrationNumber = contract.Part145RegistrationNumber,
                PhoneNumber = contract.PhoneNumber,
                PhoneNumberOfRefisteredmaintenanceCompany = contract.PhoneNumberOfRefisteredmaintenanceCompany,
                ScheduleDateOfAircraftArrival = contract.ScheduleDateOfAircraftArrival,
                ScheduleDateOfAircraftDeparture = contract.ScheduleDateOfAircraftDeparture,
                TypeOfMaintenance = contract.TypeOfMaintenance,
                //UploadedFiles = contract.UserServiceRequestMaster.UserServiceRequestAttachments.Select(o=> new FileUploadedViewModel { }).ToList()
            };
            return model;
        }

        private AircraftMaintenanceRequest MapViewModelToContract(AircraftMaintenanceRequestViewModel model)
        {
            var contract = new AircraftMaintenanceRequest
            {
                UserServiceRequestMasterId = model.UserServiceRequestMasterId,
                AddressOfMaintenanceCompany = model.AddressOfMaintenanceCompany,
                AircraftModel = model.AircraftModel,
                AircraftRegistrationNumber = model.AircraftRegistrationNumber,
                AircraftType = model.AircraftType,
                ApplicantRegisteredMaintenanceCompany = model.ApplicantRegisteredMaintenanceCompany,
                DateOfApplication = model.DateOfApplication,
                Designation = model.Designation,
                DetailsOfMaintenance = model.DetailsOfMaintenance,
                EmailId = model.EmailId,
                MaintenanceCompanyTradeLicenseExpiryDate = model.MaintenanceCompanyTradeLicenseExpiryDate,
                MaintenanceCompanyTradeLicenseNumber = model.MaintenanceCompanyTradeLicenseNumber,
                NameOfApplicant = model.NameOfApplicant,
                Part145RegistrationExpiryDate = model.Part145RegistrationExpiryDate,
                Part145RegistrationNumber = model.Part145RegistrationNumber,
                PhoneNumber = model.PhoneNumber,
                PhoneNumberOfRefisteredmaintenanceCompany = model.PhoneNumberOfRefisteredmaintenanceCompany,
                ScheduleDateOfAircraftArrival = model.ScheduleDateOfAircraftArrival,
                ScheduleDateOfAircraftDeparture = model.ScheduleDateOfAircraftDeparture,
                TypeOfMaintenance = model.TypeOfMaintenance
            };
            return contract;
        }

        #endregion "Private Methods"
    }
}