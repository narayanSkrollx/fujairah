﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using umbraco.BasePages;
using umbraco.uicontrols.DatePicker;

namespace Designit.Umb.Newsletter.Custom.plugins.designit.newsletter
{
    public partial class DelaySend : UmbracoEnsuredPage 
    {
        private int id;
        private DateTimePicker releaseDate = new DateTimePicker();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                id = int.Parse(Request["id"]);
                var timeZoneId = TimeZoneInfo.Local.Id;
                string path = Server.MapPath(@"/config/designit/newsletter/mailSettings.config");
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(path);
                XmlNode xnode = xmlDocument.SelectSingleNode("//mailSettings/mailSetting[@name='TimezoneId']");
                if (xnode.Attributes[1].Value.Length > 0)
                    timeZoneId = xnode.Attributes[1].Value;
                TimezoneList.Items.AddRange(Bll.Helper.GetTimezoneListItems(timeZoneId));
            }
            releaseDate.ID = "releaseDate";
            releaseDate.ShowTime = true;
            panReleaseDate.Controls.Add(releaseDate);
            
        }

        protected void btnDelay_Click(object sender, EventArgs e)
        {
            id = int.Parse(Request["id"]);
            /**
            string timezone = TimezoneList.SelectedItem.Value.Substring(4,6);
            if (timezone.StartsWith(")"))
            {
                timezone = "0";
            }
            else if (timezone.EndsWith("00"))
            {
                timezone = timezone.Substring(0, 3);
            }
            else if (timezone.EndsWith("30"))
            {
                timezone = timezone.Substring(0, 3) + ".5";
            }
            else
            {
                timezone = timezone.Substring(0, 3) + ".75";
            }
            **/
            var timezone = TimezoneList.SelectedItem.Value;
            if (releaseDate.DateTime <= new DateTime(1753, 1, 1) || releaseDate.DateTime >= new DateTime(9999, 12, 31))
            {
                ClientTools.ShowSpeechBubble(speechBubbleIcon.error, "No date or time selected", "");
            }
            else
            {
                //remove any possible delayed references
                umbraco.BusinessLogic.Application.SqlHelper.ExecuteNonQuery("delete from newsLetterCampaignDelayedSend where campaignId=@campaignId",
                    umbraco.BusinessLogic.Application.SqlHelper.CreateParameter("@campaignId", id));

                umbraco.BusinessLogic.Application.SqlHelper.ExecuteNonQuery(
                "insert into newsLetterCampaignDelayedSend(campaignId, timeStamp, timezone) values(@campaignId, @timeStamp, @timezone)",
                umbraco.BusinessLogic.Application.SqlHelper.CreateParameter("@campaignId", id),
                umbraco.BusinessLogic.Application.SqlHelper.CreateParameter("@timeStamp", releaseDate.DateTime),
                umbraco.BusinessLogic.Application.SqlHelper.CreateParameter("@timezone", timezone));

                umbraco.BusinessLogic.Application.SqlHelper.ExecuteNonQuery(
                    "update newsLetterCampaignNewsletter set campstatus='delayed' where id=@id",
                    umbraco.BusinessLogic.Application.SqlHelper.CreateParameter("@id", id));
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(@"<script type=""text/javascript"">");
                sb.AppendLine(@"Redir();");
                sb.AppendLine("Close();");
                sb.AppendLine("</script>");
                Page.ClientScript.RegisterStartupScript(this.GetType(), "close", sb.ToString());
            }



        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            id = int.Parse(Request["id"]);
            //remove any possible delayed references
            umbraco.BusinessLogic.Application.SqlHelper.ExecuteNonQuery("delete from newsLetterCampaignDelayedSend where campaignId=@campaignId",
                umbraco.BusinessLogic.Application.SqlHelper.CreateParameter("@campaignId", id));
           Designit.Umb.Newsletter.Bll.EmbedNewsletter.SendCampaign(id);
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(@"<script type=""text/javascript"">");
            sb.AppendLine(@"Redir();");
            sb.AppendLine("Close();");
            sb.AppendLine("</script>");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "close", sb.ToString());
           
        }


        
    }
}
