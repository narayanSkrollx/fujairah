﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EService.Web.App_LocalResources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class TradingLicenseRequestResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal TradingLicenseRequestResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("EService.Web.App_LocalResources.TradingLicenseRequestResource", typeof(TradingLicenseRequestResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Address of the Sponsor.
        /// </summary>
        public static string AddressOfSponsor {
            get {
                return ResourceManager.GetString("AddressOfSponsor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Applicant Country .
        /// </summary>
        public static string ApplicantCountry {
            get {
                return ResourceManager.GetString("ApplicantCountry", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Applicant Email.
        /// </summary>
        public static string ApplicantEmailNumber {
            get {
                return ResourceManager.GetString("ApplicantEmailNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fax.
        /// </summary>
        public static string ApplicantFaxNumber {
            get {
                return ResourceManager.GetString("ApplicantFaxNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Applicant Mobile.
        /// </summary>
        public static string ApplicantMobileNumber {
            get {
                return ResourceManager.GetString("ApplicantMobileNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Applicant’s Name.
        /// </summary>
        public static string ApplicantName {
            get {
                return ResourceManager.GetString("ApplicantName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Office.
        /// </summary>
        public static string ApplicantOfficeNumber {
            get {
                return ResourceManager.GetString("ApplicantOfficeNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Residence.
        /// </summary>
        public static string ApplicantResidenceNumber {
            get {
                return ResourceManager.GetString("ApplicantResidenceNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Application Date .
        /// </summary>
        public static string ApplicationDate {
            get {
                return ResourceManager.GetString("ApplicationDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Business Activities 1.
        /// </summary>
        public static string BusinessActivities1 {
            get {
                return ResourceManager.GetString("BusinessActivities1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Business Activities 2.
        /// </summary>
        public static string BusinessActivities2 {
            get {
                return ResourceManager.GetString("BusinessActivities2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Business Activities 3.
        /// </summary>
        public static string BusinessActivities3 {
            get {
                return ResourceManager.GetString("BusinessActivities3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Option 1.
        /// </summary>
        public static string CompanyNameOption1 {
            get {
                return ResourceManager.GetString("CompanyNameOption1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Option 2.
        /// </summary>
        public static string CompanyNameOption2 {
            get {
                return ResourceManager.GetString("CompanyNameOption2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Option 3 .
        /// </summary>
        public static string CompanyNameOption3 {
            get {
                return ResourceManager.GetString("CompanyNameOption3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company’s Preferred Name.
        /// </summary>
        public static string CompanyPreferredName {
            get {
                return ResourceManager.GetString("CompanyPreferredName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Expected Annual Business Turnover.
        /// </summary>
        public static string ExpectedAnnualBusinessTrunoverAmount {
            get {
                return ResourceManager.GetString("ExpectedAnnualBusinessTrunoverAmount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mailing Address.
        /// </summary>
        public static string MailingAddress {
            get {
                return ResourceManager.GetString("MailingAddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Present Sponsor in UAE(if any).
        /// </summary>
        public static string PresentSponsorInUAE {
            get {
                return ResourceManager.GetString("PresentSponsorInUAE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Remarks.
        /// </summary>
        public static string Remarks {
            get {
                return ResourceManager.GetString("Remarks", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Staff Strength Required.
        /// </summary>
        public static string TotalStaffStrengthRequired {
            get {
                return ResourceManager.GetString("TotalStaffStrengthRequired", resourceCulture);
            }
        }
    }
}
