﻿using System;

namespace EService.Service.Model
{
    public class UserServiceRequestModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime RequestDateTime { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public int RequestStatus { get; set; }
        public string RequestStatusName { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public string RegistrationNo { get; set; }
        public bool IsReapplied { get; set; }
        public bool IsDraft { get; set; }
    }
}