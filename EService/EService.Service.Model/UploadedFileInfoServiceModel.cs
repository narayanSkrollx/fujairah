﻿namespace EService.Service.Model
{
    public class UploadedFileInfoServiceModel
    {
        public int UserAttachmentId { get; set; }
        
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public int UploadedBy { get; set; }
        public string UploadedByName { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
    }
}