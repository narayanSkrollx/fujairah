using System;
using System.Collections.Generic;
using System.Runtime;

namespace EService.Data.Entity.Entity
{
    public class GroundHandlingRequest : AuditableEntity, IServiceEntity
    {
        public int UserServiceRequestMasterId { get; set; }
        #region "Project information"
        public DateTime InboundDateTimeUTC { get; set; }
        public string From { get; set; }
        public DateTime ETAUTC { get; set; }
        public DateTime OutboundDateTimeUTC { get; set; }
        public string To { get; set; }
        public DateTime ETDUTC { get; set; }
        #endregion
        public double? Weight { get; set; }
        public string AircraftRegistration { get; set; }
        public bool? RequestWBCalculation { get; set; }
        public bool? LoadingInstruction { get; set; }
        public bool? METOrATSDelivery { get; set; }
        public int? NoOfDepartingPassengers { get; set; }
        public int? NoOfArrivingPassengers { get; set; }
        public bool? PermittedToAddGood { get; set; }
        public bool? HasSpecialPermission { get; set; }
        #region "Cargo Off Loading"
        /// <summary>
        /// ULR Or Bulk
        /// </summary>
        public bool? CargoOffLoadingType { get; set; }
        public int? CoLNoOfULD { get; set; }
        public double? CoLULDWeight { get; set; }
        public string CoLTypeOfDG { get; set; }
        public double? CoLQuantityOfDG { get; set; }
        public string CoLOtherSpecialCargo { get; set; }
        #endregion

        #region "Cargo Loading"
        public bool? CargoLoadingType { get; set; }
        public int? CLNoOfULD { get; set; }
        public int? CLULDWeight { get; set; }
        public string CLTypeOfDG { get; set; }
        public double? CLQuantityDG { get; set; }
        public double? CLDGWeight { get; set; }
        public string CLOtherSpecialCargo { get; set; }

        #endregion

        #region "Applicant Info"

        public string CompanyName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string SITATEX { get; set; }

        #endregion
        public virtual AircraftInteriorCleaning AircraftCleaning { get; set; }
        public virtual UserServiceRequestMaster UserServiceRequestMaster { get; set; }
        public virtual ICollection<GroundHandlingRequestAircraftType> AircraftTypes { get; set; }
    }

    public class AircraftInteriorCleaning
    {
        public int UserServiceRequestMasterId { get; set; }
        public bool FumigationRequired { get; set; }
        public bool WasingRequired { get; set; }
        public string PowerType { get; set; }
        public bool Pushback { get; set; }
        public bool PassengerStairs { get; set; }
        public bool ToiletService { get; set; }
        public bool PortableWater { get; set; }
        public string ASUHours { get; set; }
        public bool AdditionalPlatformCRJ { get; set; }
        public bool SecuritySurveillanceForRAmpHours { get; set; }
        public bool HotelBooking { get; set; }
        public bool StandbyFireFighting { get; set; }
        public bool AdditionalManpower { get; set; }
        public string AdditionalRequirements { get; set; }

        public virtual GroundHandlingRequest GroundHandlingRequest { get; set; }

    }

    public class GroundHandlingRequestAircraftType
    {
        public int GroundHandlingRequestAircraftTypeId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public int AircraftTypeId { get; set; }


        public virtual AircraftType AircraftType { get; set; }
        public virtual GroundHandlingRequest GroundHandlingRequest { get; set; }
    }

    public class AircraftType : ILookupItem
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public bool IsOther { get; set; }

    }
}