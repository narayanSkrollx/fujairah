﻿using EService.Core;
using EService.Data.Entity.Entity;
using EService.Service.Model;
using System;
using System.Linq;

namespace EService.Service.Logic
{
    public class UserServiceRequestReceiptService : BusinessAccessBase<UserServiceRequestReceipt>
    {
        public UserServiceRequestReceiptService(ServiceContext context) : base(context)
        {
        }

        public ReceiptNoViewModel GetNewReceiptNo()
        {
            var date = DateTime.Now.DubaiTime();
            int year = date.Year;
            var maxIndex = FindAllAsNoTracking(o => o.Year == year && o.IsPaid).OrderByDescending(o => o.Index).FirstOrDefault();
            int index = maxIndex != null ? (maxIndex.Index ?? 0) + 1 : 1;
            string no = string.Format("{0}-{1}", year, index.ToString().PadLeft(8, '0'));
            return new ReceiptNoViewModel()
            {
                Index = index,
                Year = year,
                ReceiptNo = no,
            };
        }
    }
}
