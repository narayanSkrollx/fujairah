﻿using EService.Data.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EService.Data.Entity.Configuration
{
    public class AircraftMaintenanceRequestConfiguration: EntityTypeConfiguration<AircraftMaintenanceRequest>
    {
        public AircraftMaintenanceRequestConfiguration()
        {
            ToTable("AircraftMaintenanceRequest");
            HasKey(x => x.UserServiceRequestMasterId);

            Property(x => x.UserServiceRequestMasterId).IsRequired();
            Property(x => x.DateOfApplication).IsRequired();
            Property(x => x.NameOfApplicant).IsRequired().HasMaxLength(225);
            Property(x => x.Designation).IsOptional().HasMaxLength(225);
            Property(x => x.PhoneNumber).IsOptional().HasMaxLength(225);
            Property(x => x.EmailId).IsOptional().HasMaxLength(225);
            Property(x => x.ApplicantRegisteredMaintenanceCompany).IsOptional().HasMaxLength(225);
            Property(x => x.AddressOfMaintenanceCompany).IsOptional().HasMaxLength(225);
            Property(x => x.PhoneNumberOfRefisteredmaintenanceCompany).IsOptional().HasMaxLength(225);
            Property(x => x.MaintenanceCompanyTradeLicenseNumber).IsOptional().HasMaxLength(225);
            Property(x => x.MaintenanceCompanyTradeLicenseExpiryDate).IsOptional();
            Property(x => x.Part145RegistrationNumber).IsOptional().HasMaxLength(225);
            Property(x => x.Part145RegistrationExpiryDate).IsOptional();
            Property(x => x.AircraftType).IsOptional().HasMaxLength(225);
            Property(x => x.AircraftModel).IsOptional().HasMaxLength(225);
            Property(x => x.AircraftRegistrationNumber).IsOptional().HasMaxLength(225);
            Property(x => x.TypeOfMaintenance).IsOptional();
            Property(x => x.DetailsOfMaintenance).IsOptional().HasColumnType("varchar(max)");
            Property(x => x.ScheduleDateOfAircraftArrival).IsOptional();
            Property(x => x.ScheduleDateOfAircraftDeparture).IsOptional();

            HasRequired(x => x.UserServiceRequestMaster)
                .WithRequiredDependent(x => x.AircraftMaintenanceRequest).WillCascadeOnDelete(false);
        }
    }
}
