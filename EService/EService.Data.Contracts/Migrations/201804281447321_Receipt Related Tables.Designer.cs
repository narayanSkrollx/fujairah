// <auto-generated />
namespace EService.Data.Contracts.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ReceiptRelatedTables : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ReceiptRelatedTables));
        
        string IMigrationMetadata.Id
        {
            get { return "201804281447321_Receipt Related Tables"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
