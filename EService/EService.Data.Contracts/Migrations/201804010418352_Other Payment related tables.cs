namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OtherPaymentrelatedtables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserServiceRequestOtherPayment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        PaymentMode = c.Int(),
                        PaidAmount = c.Double(),
                        PaidByUserId = c.Int(),
                        PaidRequestDateTime = c.DateTime(),
                        PaidRequestedByUserName = c.String(),
                        OrderNumber = c.String(nullable: false),
                        IsReceivedOnSuccess = c.Boolean(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Currency = c.String(nullable: false, maxLength: 255),
                        StatusFlag = c.String(nullable: false, maxLength: 255),
                        ErrorCode = c.String(),
                        AllResponseJson = c.String(),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserServiceRequestMaster", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserServiceRequestOtherPayment", "UserServiceRequestMasterId", "dbo.UserServiceRequestMaster");
            DropIndex("dbo.UserServiceRequestOtherPayment", new[] { "UserServiceRequestMasterId" });
            DropTable("dbo.UserServiceRequestOtherPayment");
        }
    }
}
