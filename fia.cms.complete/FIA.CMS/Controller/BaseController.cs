﻿using System.Configuration;
using System.Net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;
using FIA.CMS.Entities;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.SqlSyntax;
using Umbraco.Web.Mvc;

namespace FIA.CMS.Controller
{
    public abstract class BaseController : SurfaceController
    {
        protected const string SiteLayoutPartialViewLocation = "~/Views/Partials/SiteLayout/";
        protected const string HomePartialViewLocation = "~/Views/Partials/Home/";

        #region "Current Context"
        protected ApplicationContext AppContext => ApplicationContext.Current;
        protected DatabaseContext DbContext => AppContext.DatabaseContext;
        protected Database Db => DbContext.Database;
        protected ISqlSyntaxProvider SqlSyntaxProvider => DbContext.SqlSyntax;
        #endregion  

        public void SendEmail(string message, string to, string subject, List<Attachment> attachments = null)
        {
            var smtpHost = ConfigurationManager.AppSettings["smtpServer"];
            var smtpPort = ConfigurationManager.AppSettings["smtpPort"];
            var smtpEmail = ConfigurationManager.AppSettings["smtpEmail"];
            var smtpUser = ConfigurationManager.AppSettings["smtpUser"];
            var smtpPassword = ConfigurationManager.AppSettings["smtpPassword"];
            var smtpEnableSsl = ConfigurationManager.AppSettings["smtpEnableSsl"];
            var mail = new MailMessage(smtpEmail, to)
            {
                Subject = subject,
                Body = message,
                IsBodyHtml = true
            };
            var sendEmail = new SmtpClient(smtpHost, int.Parse(smtpPort))
            {
                Credentials =  new NetworkCredential(smtpUser, smtpPassword),
            //new Constants.System.Net.NetworkCredential(smtpUser, smtpPassword)
            };
            if (attachments != null)
            {
                foreach (var attachment in attachments)
                {
                    mail.Attachments.Add(attachment);
                }
            }
            if (bool.Parse(smtpEnableSsl))
            {
                sendEmail.EnableSsl = true;
            }
            sendEmail.Send(mail);
        }

        protected virtual string RenderPartialView(string partialViewName, object model = null)
        {
            if (ControllerContext == null)
                return string.Empty;

            if (string.IsNullOrEmpty(partialViewName))
                throw new ArgumentNullException("partialViewName");

            ModelState.Clear();//Remove possible model binding error.

            ViewData.Model = model;//Set the model to the partial view

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, partialViewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }


        public int GetCurrentUserId()
        {
            var db = ApplicationContext.DatabaseContext.Database;
            var sql = "select * from UserLogin Where UserName='" + User.Identity.Name + "'";
            var users = db.Query<UserLogin>(sql);
            var userLogins = users as UserLogin[] ?? users.ToArray();
            if (userLogins.Any())
            {
                return userLogins[0].Id;
            }
            return 0;

        }
    }
}