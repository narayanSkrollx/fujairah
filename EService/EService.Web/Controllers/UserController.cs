using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using EService.Core.Security;
using EService.Data.Entity;
using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Service.Model;
using EService.Web.Controllers.Base;
using EService.Web.Helper;
using EService.Web.Models;

namespace EService.Web.Controllers
{
    /// <summary>
    /// Controller for system admin to create department users and so on
    /// </summary>
    public class UserController : SystemAdminBaseController
    {
        private ServiceContext context;
        private UserService userService;
        private UserTypeService userTypeService;
        private DepartmentService departmentService;

        public UserController()
        {
            context = new ServiceContext();
            userService = new UserService(context);
            userTypeService = new UserTypeService(context);
            departmentService = new DepartmentService(context);
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Initialize()
        {
            var result = new ResponseObject();
            try
            {
                var users = userService.GetUsers().ToList();
                var userTypes = userTypeService.FindAllAsNoTracking(o => !o.IsDeleted).AsEnumerable().Select(o => new LookupItem() { Active = true, Id = o.UserTypeId, Description = o.UserTypeName }).ToList();
                var departments = departmentService.FindAllAsNoTracking(o => !o.IsDeleted).AsEnumerable().Select(o => new LookupItem() { Active = true, Id = o.DepartmentId, Description = o.DepartmentName }).ToList();

                var newDepartmentUser = new DepartmentUserCreateViewModel();

                result.Data = new UserViewModel() { UserList = users, UserTypes = userTypes, Departments = departments, NewDepartmentUserModel = newDepartmentUser };
                result.IsSuccess = true;
            }
            catch (Exception)
            {
                result.IsSuccess = false;
                result.Message = "Error while obtaining users list.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveDepartmentUser(DepartmentUserCreateViewModel model)
        {
            var result = new ResponseObject();
            try
            {
                if (ModelState.IsValid)
                {
                    var user = new User()
                    {
                        DepartmentId = model.DepartmentId,
                        UserName = model.UserName,
                        Email = model.Email,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        UserTypeId = model.UserTypeId ?? (int)UserTypeEnum.DepartmentUser,
                        Password = model.Password.ToMd5()
                    };
                    var saveResult = userService.SaveDepartmentUser(user);
                    if (saveResult.IsSuccess)
                    {
                        return Initialize();
                    }
                    else
                    {
                        result.IsSuccess = false;
                        result.Message = saveResult.Message;
                    }
                }
                else
                {
                    result.Message = ModelState.GetModelStateError();
                    result.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                result.Message = "Error while saving department  user.";
                result.IsSuccess = false;
                LogError(ex);
            }
            return Json(result);
        }

        //public ActionResult ChangePassword()
        //{
        //    var model = new ChangePasswordViewModel();
        //    try
        //    {
        //        model.UserName = UserProfile.UserName;
        //        return View(model);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return ErrorResult();
        //    //UserProfile.UserName
        //}

        //[HttpPost]
        //public JsonResult ChangePassword(ChangePasswordViewModel model)
        //{
        //    var result = new JsonResponseObject();
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            ResponseObject saveResult = userService.ChangePassword(UserProfile.UserId, model.CurrentPassword,
        //                model.NewPassword);
        //            result.IsSuccess = saveResult.IsSuccess;
        //            result.Message = saveResult.Message;
        //        }
        //        else
        //        {
        //            result.IsSuccess = false;
        //            result.Message = ModelState.GetModelStateError();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result.IsSuccess = false;
        //        result.Message = "Error while changing password.";

        //    }
        //    return Json(result);
        //}

    }
}