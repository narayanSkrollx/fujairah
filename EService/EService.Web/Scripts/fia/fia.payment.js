﻿$(function () {
    fia.CreateNamespace("payment");

    (function (context) {
        context.Title = 'Payment';
        context.createForm = '#frmAircraftDismantlingRequest';
        context.ServiceId = fia.ServiceEnum.AircraftDismantlingRequest;

        context.ViewModel = {
            PaymentMode: ko.observable(0),
            SetPaymentMode: function () {
                ajaxRequest('/Payment/SetCashPayment', 'POST', { data: {} }, function (response) {
                    if (response.IsSuccess) {
                        showMessage(context.Title, 'Your Payment mode has been set to Manual / Cash payment.', 'success', function () {
                            window.location = response.Data.Url;

                        })
                    }
                    else {
                        showMessage(context.Title, response.Message, 'error')
                    }
                })
            },
            ConfirmPayment: function () {
                ajaxRequest('/Payment/Confirm', 'POST', { data: {} }, function (response) {
                    if (response.IsSuccess) {
                        $('#requestParameter').val(response.Data.RequestParameter);
                        $('#paymentPostForm')[0].action = response.Data.PostUrl;
                        $('#paymentPostForm')[0].submit();
                    }
                    else {
                        showMessage(context.Title, response.Message, 'error')
                    }
                })
            }
        }

        context.Initialize = function () {
            ko.applyBindings(context.ViewModel, $('#mainContent')[0]);
        }

    })(fia.payment);
});