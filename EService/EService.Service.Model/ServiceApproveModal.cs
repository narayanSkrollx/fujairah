﻿using System.Collections.Generic;

namespace EService.Service.Model
{
    public class ServiceApproveModal
    {
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public List<ServiceApprovalStageModal> ServiceApprovalStages { get; set; }
    }
}