namespace EService.Web.Helper
{
    public class Constants
    {
        public static string ServiceRequestReapplyContentTempDataKey = "ServiceReapplyContentKey";
        public static string ServiceRequestDetailTempDataKey = "ServiceRequestDetailViewKey";
        public static string PaymentRequestMasterIdKey = "paymentRequestMasterId";
        public static string PaymentRequestMasterContentKey = "paymentRequestMasterContentKey";
    }
}