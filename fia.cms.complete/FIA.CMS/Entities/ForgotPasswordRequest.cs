using System;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace FIA.CMS.Entities
{
    [TableName("ForgotPasswordRequest")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class ForgotPasswordRequest
    {
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }

        public int UserId { get; set; }
        public string Email { get; set; }
        public Guid Token { get; set; }
        public DateTime ExpiryTime { get; set; }
        public bool IsUsed { get; set; }

    }
}