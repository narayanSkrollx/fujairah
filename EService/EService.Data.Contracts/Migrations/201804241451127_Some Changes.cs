namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SomeChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserServiceRequestBill", "ReceiptNo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserServiceRequestBill", "ReceiptNo");
        }
    }
}
