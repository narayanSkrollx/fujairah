using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class UserAttachment : AuditableEntity
    {
        public int UserAttachmentId { get; set; }
        public DateTime Timestamp { get; set; }
        public int UserId { get; set; }
        public int ServiceId { get; set; }
        public string FileName { get; set; }
        //public byte[] FileContents { get; set; }
        public string ContentType { get; set; }

        [JsonIgnore]
        public virtual User User { get; set; }
        [JsonIgnore]
        public virtual Service Service { get; set; }

        [JsonIgnore]
        public virtual ICollection<LandingPermissionRequestAttachment> LandingPermissionRequestAttachments { get; set; }
        [JsonIgnore]
        public virtual ICollection<CranePermitRequestUserAttachment> CranePermitRequestUserAttachments { get; set; }
        [JsonIgnore]
        public virtual ICollection<UserServiceRequestAttachment> UserServiceRequestAttachments
        { get; set; }
        public virtual UserAttachmentData UserAttachmentData { get; set; }
    }

    public class UserAttachmentData
    {
        public int UserAttachmentId { get; set; }
        public byte[] FileContents { get; set; }
        public virtual UserAttachment UserAttachment { get; set; }
    }
}