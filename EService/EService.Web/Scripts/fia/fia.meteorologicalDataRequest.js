﻿$(function () {
    fia.CreateNamespace("meteorologicalDataRequest");

    (function (context) {

        context.ServiceId = fia.ServiceEnum.MeteorologicalDataRequest;
        context.CreateSuccessUrl = ApplicationRootUrl('Index', 'Dashboard');

        context.ViewModel = {
            Records: ko.observable({}),

        }

        context.Initialize = function () {
            context.LoadData();
        }

        context.InitializeCreate = function () {
            context.LoadCreateModel();
        }


        context.InitializeDetail = function () {
            context.LoadDetailModel();
        }

        context.IndexMapping = {
            create: function (options) {
                var vm = ko.mapping.fromJS(options.data);

                vm.ViewDetail = function (item) {
                    window.location = '/MeteorologicalData/Detail/' + item.UserServiceRequestMasterId();
                }

                return vm;
            }
        }

        context.SaveMetRequest = function (isDraft) {
            if (!$('#metRequestCreateForm').valid()) {
                return false;
            }
            var model = ko.toJS(context.ViewModel.AddNewModel);
            model.ApplicationDate = moment(model.ApplicationDate).isValid() ? moment(model.ApplicationDate).format('YYYY-MM-DD') : null;
            model.CustomRangeEndDate = moment(model.CustomRangeEndDate).isValid() ? moment(model.CustomRangeEndDate).format('YYYY-MM-DD') : null;
            model.CustomRangeStartDate = moment(model.CustomRangeStartDate).isValid() ? moment(model.CustomRangeStartDate).format('YYYY-MM-DD') : null;
            delete model.Save;

            var submitControl = isDraft ? 'draft' : 'save';
            var modelString = JSON.stringify({ model: (model), isDraft: isDraft });
            var ajaxOption = {
                dataType: "JSON",
                contentType: "application/json; charset=UTF-8",
                data: modelString,
                enableLadda: true,
                targetLaddaElement: `[data-role=${submitControl}]`,
            };
            ajaxRequest('/MeteorologicalData/Create', 'POST', ajaxOption, function (resp) {
                if (resp.IsSuccess) {
                    showMessage('Meteorological Data Request', 'Meteorological Data Request saved successfully.', 'success', function () {
                        window.location = context.CreateSuccessUrl;
                    });
                } else {
                    showMessage('Meteorological Data Request', resp.Message, 'error', function () {
                    });
                }
            });
        }

        context.CreateMapping = {
            create: function (options) {
                var vm = ko.mapping.fromJS(options.data);

                vm.Save = function () {
                    context.SaveMetRequest(false);
                }
                vm.SaveDraft = function () {
                    context.SaveMetRequest(true);
                }
                vm.SetUserInfo = function () {
                    context.SetUserInfo();
                }
                return vm;
            }
        }

        context.LoadData = function () {
            ajaxRequest('/MeteorologicalData/Initialize', 'GET', {}, function (resp) {
                if (resp.IsSuccess) {
                    context.ViewModel = ko.mapping.fromJS(resp.Data, context.IndexMapping);

                    ko.applyBindings(context.ViewModel, $('#mainContent')[0]);
                } else {
                    showMessage('Meteorological Data Request', resp.Message, 'error');
                }
            });
        }

        context.SetUserInfo = function () {
            var addNewModel = context.ViewModel.AddNewModel;
            var userInfo = context.ViewModel.AddNewModel.UserInfo;
            if (addNewModel != null && userInfo != null) {
                addNewModel.Phone(userInfo.Phone());
                addNewModel.Email(userInfo.Email());
                addNewModel.Address(userInfo.Address());

                addNewModel.Fax(userInfo.Fax());
            }
        }


        context.LoadDetailModel = function () {
            var id = $('#UserServiceRequestId').val();
            ajaxRequest('/MeteorologicalData/InitializeDetail', 'GET', { data: { id: id } }, function (resp) {
                if (resp.IsSuccess) {
                    context.ViewModel = ko.mapping.fromJS(resp.Data);

                    ko.applyBindings(context.ViewModel, $('#mainContent')[0]);

                    fia.fileinputhelper.InitializeUploadedList('#uploadedFiles', id);
                } else {
                    showMessage('Meteorological Data Request', resp.Message, 'error');
                }
            });
        }

        context.LoadCreateModel = function () {
            var masterId = parseInt($("#CurrentUserServiceRequestMasterId.forKnockout").val());
            context.ViewModel.IsCreate = Number.isInteger(masterId);
            var id = context.ViewModel.IsCreate ? parseInt(masterId) : null;
            ajaxRequest('/MeteorologicalData/InitializeCreate', 'GET', {
                data: { id: id }
            }, function (resp) {
                if (resp.IsSuccess) {
                    context.ViewModel = ko.mapping.fromJS(resp.Data, context.CreateMapping);

                    context.ViewModel.RemoveUploadedFile = function (item) {
                        item.ObjectState(2);
                    }
                    ko.applyBindings(context.ViewModel, $('#mainContent')[0]);
                    //
                    fia.fileinputhelper.InitializeFileUpload('#kv-explorer', fia.ServiceEnum.MeteorologicalDataRequest);
                    fia.fileinputhelper.UploadCompleteCallback = function (response) {
                        context.ViewModel.AddNewModel.UploadedFiles.push(response.Data);
                    }

                    //apply validation
                    context.ApplyValidation();
                } else {
                    showMessage('Meteorological Data Request', resp.Message, 'error');
                }
            });
        }


        context.ApplyValidation = function () {
            $('#metRequestCreateForm').validate({
                rules: {
                    ApplicationDate: { required: true, },
                    Email: { required: true },
                    Department: { required: true },
                    Phone: { required: true, regex: phoneNoRegex },
                    Fax: { required: true },
                    Address: { required: true },
                    WeatherType: { required: true },
                    CustomRangeStartDate: { required: true },
                    CustomRangeEndDate: { required: true },
                    RequisiteWeatherElement: { required: true },
                    DataRange: { required: true }
                },
                messages: {
                    ApplicationDate: { required: "Application Date is required", },
                    Email: { required: "Email is required" },
                    Department: { required: "Company Name is required" },
                    Phone: { required: "Phone is required" },
                    Fax: { required: "Fax is required" },
                    Address: { required: "Address is required" },
                    WeatherType: { required: "Weather Type must be selected." },
                    CustomRangeStartDate: { required: "Custom Range Start Date is required" },
                    CustomRangeEndDate: { required: "Custom Range End Date is required" },
                    RequisiteWeatherElement: { required: "Weather Element must be selected." },
                    DataRange: { required: "Data Range must be selected" }
                }

            });
        }


    })(fia.meteorologicalDataRequest);;
});