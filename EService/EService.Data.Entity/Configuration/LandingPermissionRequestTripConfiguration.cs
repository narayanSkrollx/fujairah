using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Security.Cryptography.X509Certificates;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class LandingPermissionRequestTripConfiguration : EntityTypeConfiguration<LandingPermissionRequestTrip>
    {
        public LandingPermissionRequestTripConfiguration()
        {
            ToTable("LandingPermissionRequestTrip");

            HasKey(o => o.LandingPermissionRequestTripId);

            Property(o => o.LandingPermissionRequestTripId)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(o => o.LandingPermissionRequestId).IsRequired();
            Property(o => o.CrewDetail).IsRequired();
            Property(o => o.NoOfPassengers).IsRequired();
            Property(o => o.ETDOrigin).IsRequired();
            Property(o => o.ETADestination).IsRequired();
            Property(o => o.IsManualEntry);
            Property(o => o.TripActionCode).IsRequired().HasMaxLength(100);
            Property(o => o.TripArrivalDestinationCode).IsRequired().HasMaxLength(100);
            Property(o => o.TripDepartureDestinationCode).IsRequired().HasMaxLength(100);
            Property(o => o.StartOfPeriod).IsRequired().HasMaxLength(100);
            Property(o => o.EndOfPeriod).IsRequired().HasMaxLength(100);
            Property(o => o.WeekdaysOfOperation).IsRequired();
            Property(o => o.NoOfSeats).IsRequired();
            Property(o => o.AircraftTypeCode).IsRequired().HasMaxLength(100);
            Property(o => o.OriginStationCode).IsRequired().HasMaxLength(100);
            Property(o => o.ArrivalTimeUTC).IsRequired().HasMaxLength(100);
            Property(o => o.DepartureTimeUTC).IsRequired().HasMaxLength(100);
            Property(o => o.DestinationStationCode).IsRequired().HasMaxLength(100);
            Property(o => o.ArrivalServiceType).IsRequired().HasMaxLength(100);
            Property(o => o.DepartureServiceType).IsRequired().HasMaxLength(100);
            Property(o => o.CompleteRouting).IsRequired().HasMaxLength(1024);

            HasRequired(o => o.LandingPermissionRequest)
                .WithMany(o => o.LandingPermissionRequestTrips)
                .HasForeignKey(o => o.LandingPermissionRequestId).WillCascadeOnDelete(false);

        }
    }
}