﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            ToTable("User");

            HasKey(o => o.UserId);

            Property(o => o.UserId).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.UserName).IsRequired().HasMaxLength(100).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_UserName", 1) { IsUnique = true }));
            Property(o => o.FirstName).IsRequired().HasMaxLength(1024);
            Property(o => o.LastName).IsRequired().HasMaxLength(1024);
            Property(o => o.Password).IsRequired().HasMaxLength(100);
            Property(o => o.Email).IsRequired().HasMaxLength(1024);
            Property(o => o.UserTypeId).IsRequired();
            Property(o => o.DepartmentId).IsOptional();

            HasRequired(o => o.UserType).WithMany(o => o.Users).HasForeignKey(o => o.UserTypeId);
            HasOptional(o => o.Department).WithMany(o => o.Users).HasForeignKey(o => o.DepartmentId);
            HasOptional(a => a.UserContactPerson).WithRequired(b => b.User).WillCascadeOnDelete(true);
            HasOptional(a => a.UserCompanyProfile).WithRequired(b => b.User).WillCascadeOnDelete(true);
            HasOptional(a => a.UserSecruityQuestion).WithRequired(b => b.User).WillCascadeOnDelete(true);
        }
    }
}
