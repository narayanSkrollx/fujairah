using EService.Service.Logic.Enum;
using EService.Service.Model;

namespace EService.Service.Logic
{
    public abstract class BaseEService<T> : BusinessAccessBase<T>, IServiceLogic where T : class
    {
        public BaseEService(ServiceContext context) : base(context)
        {
        }
        public abstract ServiceEnum Service { get; }
        public virtual ResponseObject IsValidForApproveReject(int userServiceRequestMasterId, int serviceRequestFlowStageDepartmentId, int departmentId)
        {
            return new ResponseObject() { IsSuccess = true };
        }

        public virtual bool HasExtraFormForApprovalReject()
        {
            return false;
        }
    }

    public interface IServiceLogic
    {
        ResponseObject IsValidForApproveReject(int userServiceRequestMasterId, int serviceRequestFlowStageDepartmentId,
            int departmentId);

        bool HasExtraFormForApprovalReject();
    }
}