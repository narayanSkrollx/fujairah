﻿namespace EService.Data.Entity.Entity
{
    public class CranePermitPlanningAttachment
    {
        public int CranePermitPlanningAttachmentId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public int UserAttachmentId { get; set; }

        public virtual CranePermitRequestFirstStageEntry PlanningUserEntry { get; set; }
        public virtual UserAttachment Attachment { get; set; }
    }
}