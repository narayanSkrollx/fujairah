using System;
using System.Linq;
using System.Web.Mvc;
using EService.Data.Entity;
using EService.Service.Logic;
using EService.Web.Helper;
using EService.Web.Helper.EmailSender;
using EService.Web.Models.ServiceNotification;

namespace EService.Web.Controllers.Base
{
    public class DepartmentUserBaseController : BaseController
    {
        private UserServiceRequestMasterService userServiceRequestMasterService;
        private SMTPEmailSettingService emailSettingService;
        private UserService userService;
        private EmailService emailService;
        private ServiceRequestFlowStageDepartmentService serviceRequestFlowStageDepartmentService;


        public DepartmentUserBaseController()
        {
            userServiceRequestMasterService = new UserServiceRequestMasterService(serviceContext);
            emailSettingService = new SMTPEmailSettingService(serviceContext);
            userService = new UserService(serviceContext);
            emailService = new EmailService(serviceContext);
            serviceRequestFlowStageDepartmentService = new ServiceRequestFlowStageDepartmentService(serviceContext);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            if (UserProfile.UserType != UserTypeEnum.DepartmentUser && UserProfile.UserType != UserTypeEnum.FinanceUser)
            {
                filterContext.Result = UnAuthroizedResult();
            }

        }

        protected void TriggerEmail(FIAEmailType emailType, int serviceFlowRequestDepartmentId)
        {
            //var userServiceRequest = userServiceRequestMasterService.GetServiceRequest(userServiceRequestMasterId);
            var smtpSetting = emailSettingService.Find(o => true);
            var allUsers = userService.FindAllAsNoTracking(o => !o.IsDeleted).ToList();
            String[] receipients = new string[] { };
            //var receipients = emailService.GetApproveRejectEmailRecepients(serviceFlowRequestDepartmentId);
            var serviceFlowRequest =
             serviceRequestFlowStageDepartmentService.Find(
                 o => o.ServiceRequestFlowStageDepartmentId == serviceFlowRequestDepartmentId);
            var htmlContent = "";
            var currentStageIndex = serviceFlowRequest.ServiceRequestFlowStage.Index;
            var request = serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster;
            switch (emailType)
            {
                case FIAEmailType.Approved:
                    receipients = emailService.GetNextStageEmailRecepients(serviceFlowRequestDepartmentId);
                    if (receipients != null && receipients.Count() > 0)
                    {
                        var nextStage = serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster.ServiceRequestFlowStages.FirstOrDefault(o => o.Index == currentStageIndex + 1);
                        var amodel = new ApprovedServiceRequestNotificationViewModel()
                        {
                            ServiceId = serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster.ServiceId,
                            ServiceName = request.Service.ServiceName,
                            RequestedByUserFullName = request.User.FirstName + " " + request.User.LastName,
                            RequestedByUserName = request.User.UserName,
                            RequestedByUserEmail = request.User.Email,
                            RequestRegistrationNo = request.RegistrationNo,
                            RequestedDateTime = request.RequestDateTime,
                            RequestedByUserId = request.UserId,
                            ApprovedByDepartmentUserName = serviceFlowRequest.DepartmentUser != null ? serviceFlowRequest.DepartmentUser.FirstName + " " + serviceFlowRequest.DepartmentUser.LastName : "",
                            ApprovedByDepartmentDateTime = serviceFlowRequest.StatusChangedDateTime ?? DateTime.MinValue,
                            NextDepartmentName = nextStage != null ? string.Join(",", nextStage.ServiceRequestFlowStageDepartments.Select(o => o.Department.DepartmentName)) : ""

                        };

                        htmlContent = RenderPartialView("_AcceptedServiceRequestEmailTemplate", amodel);
                    }

                    break;
                case FIAEmailType.Rejected:
                    var rejectReceipients = emailService.GetPreviousStageEmailRecepients(serviceFlowRequestDepartmentId);
                    if (rejectReceipients != null && rejectReceipients.Item1.Length > 0)
                    {
                        receipients = rejectReceipients.Item1;
                        var previousStage = serviceFlowRequest.ServiceRequestFlowStage.UserServiceRequestMaster.ServiceRequestFlowStages.FirstOrDefault(o => o.Index == currentStageIndex - 1);

                        var model = new RejectedServiceRequestNotificationViewModel()
                        {
                            ServiceId = request.ServiceId,
                            ServiceName = request.Service.ServiceName,
                            RequestedByUserName = request.User.UserName,
                            RequestedByUserFullName = request.User.FirstName + " " + request.User.LastName,
                            RequestedByUserEmail = request.User.Email,
                            RequestRegistrationNo = request.RegistrationNo,
                            RequestedDateTime = request.RequestDateTime,
                            RequestedByUserId = request.UserId,
                            RejectedByDepartmentUserName = serviceFlowRequest.DepartmentUser.FirstName + " " + serviceFlowRequest.DepartmentUser.FirstName.ToString(),
                            IsPassedToUser = !request.ServiceRequestFlowStages.Any(o => o.IsCurrentStage),
                            RejectedByDepartmentUserId = serviceFlowRequest.DepartmentUserId ?? 0,
                            RejectedByDepartmentDateTime = serviceFlowRequest.StatusChangedDateTime ?? DateTime.MinValue,
                            RejectedToDepartmentName = previousStage != null ? string.Join(",", previousStage.ServiceRequestFlowStageDepartments.Select(o=>o.Department.DepartmentName)) : ""
                        };
                        htmlContent = RenderPartialView("_RejectedServiceRequestEmailTemplate", model);
                    }
                    break;
                case FIAEmailType.Completed:
                    break;
                default:
                    break;
            }
            EmailHelper.SendEmail(smtpSetting.From, receipients, new string[] { }, new string[] { }, emailType.ToString(), htmlContent, smtpSetting);
        }

        protected int DepartmentId => UserProfile.DepartmentId.Value;
    }
}