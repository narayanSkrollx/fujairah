$(function () {
    fia.CreateNamespace('fileinputhelper');


    (function (context) {

        context.Selector = '#fileUploader';
        context.CurrentServiceId = fia.ServiceEnum.Other;

        context.UploadCompleteCallback = function (response) {

        }
        context.InitializeFileUpload = function (selector, serviceId, initialPreview, initialPreviewConfig) {
            context.Selector = selector;
            context.CurrentServiceId = serviceId;

            $(context.Selector).fileinput({
                'theme': 'explorer',
                'uploadUrl': '/FileUpload/upload/',
                overwriteInitial: false,
                uploadExtraData: { serviceId: context.CurrentServiceId },
                initialPreview: initialPreview,
                initialPreviewConfig: initialPreviewConfig,
                maxFileSize: 10240,
            });

            $(context.Selector).on('fileuploaded', function (event, data, previewId, index) {
                var form = data.form, files = data.files, extra = data.extra,
                    response = data.response, reader = data.reader;
                context.UploadCompleteCallback(response);
                console.log('File uploaded triggered');
            });
            $(context.Selector).on('filebeforedelete', function () {
                console.log(arguments);
                var aborted = !window.confirm('Are you sure you want to delete this file?');
                if (aborted) {

                };
                return aborted;
                console.log('before file delete');
            });
            $(context.Selector).on('filedeleted', function () {

                console.log('file deleted');
            });
        }

        context.InitializeUploadedList = function (selector, userServiceRequestMasterId) {
            ajaxRequest('/UserServiceRequestMaster/UploadedFiles/', 'GET', { data: { userServiceRequestmasterId: userServiceRequestMasterId } }, function (response) {
                if (response.IsSuccess) {
                    //
                    if (!ko.dataFor($(selector)[0])) {
                        context.UploadedFiles = ko.mapping.fromJS(response.Data);
                        context.DownloadAttachment = function (item) {
                            ajaxRequest('/Attachment/SetAttachment/', 'POST', { data: { id: item.UserAttachmentId() } }, function (response) {
                                if (response.IsSuccess) {
                                    window.open("/Attachment/Index/", "_blank");
                                } else {
                                    showMessage('User Attachment', response.Message, 'error');
                                }
                            });
                        }
                        ko.applyBindings(context, $(selector)[0]);
                    } else {
                        ko.mapping.fromJS(response.Data, {}, context.UploadedFiles);
                    }
                }
            });
        }

    })(fia.fileinputhelper);
});