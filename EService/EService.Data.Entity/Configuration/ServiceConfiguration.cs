using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class ServiceConfiguration : EntityTypeConfiguration<Service>
    {
        public ServiceConfiguration()
        {
            ToTable("Service");

            HasKey(o => o.ServiceId);

            Property(o => o.ServiceId).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(o => o.ServiceName)
                .IsRequired()
                .HasMaxLength(255)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("IX_ServiceNameUnique", 1) { IsUnique = true }));
            Property(o => o.ServiceCode).IsRequired().HasMaxLength(10);
            Property(o => o.DefaultRate).IsRequired();

            Property(o => o.IsActive).IsRequired();
            Property(o => o.Description).IsOptional().HasColumnType("nvarchar(max)");


        }
    }
}