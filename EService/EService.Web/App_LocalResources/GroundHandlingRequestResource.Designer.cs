﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EService.Web.App_LocalResources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class GroundHandlingRequestResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal GroundHandlingRequestResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("EService.Web.App_LocalResources.GroundHandlingRequestResource", typeof(GroundHandlingRequestResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Additional Manpower.
        /// </summary>
        public static string AdditionalManpower {
            get {
                return ResourceManager.GetString("AdditionalManpower", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Additional Platform CRJ.
        /// </summary>
        public static string AdditionalPlatformCRJ {
            get {
                return ResourceManager.GetString("AdditionalPlatformCRJ", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Additional Requirements.
        /// </summary>
        public static string AdditionalRequirements {
            get {
                return ResourceManager.GetString("AdditionalRequirements", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Address.
        /// </summary>
        public static string Address {
            get {
                return ResourceManager.GetString("Address", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Aircraft Registration.
        /// </summary>
        public static string AircraftRegistration {
            get {
                return ResourceManager.GetString("AircraftRegistration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email Address.
        /// </summary>
        public static string ApplicantEmail {
            get {
                return ResourceManager.GetString("ApplicantEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Applicant Email is required..
        /// </summary>
        public static string ApplicantEmailRequired {
            get {
                return ResourceManager.GetString("ApplicantEmailRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Applicant&apos;s Email must be a valid Email..
        /// </summary>
        public static string ApplicantEmailValidEmail {
            get {
                return ResourceManager.GetString("ApplicantEmailValidEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Application Date.
        /// </summary>
        public static string ApplicationDate {
            get {
                return ResourceManager.GetString("ApplicationDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Application Date must be selected..
        /// </summary>
        public static string ApplicationDateRequired {
            get {
                return ResourceManager.GetString("ApplicationDateRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ASU Hours.
        /// </summary>
        public static string ASUHours {
            get {
                return ResourceManager.GetString("ASUHours", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cargo Loading Type.
        /// </summary>
        public static string CargoLoadingType {
            get {
                return ResourceManager.GetString("CargoLoadingType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CLDG Weight.
        /// </summary>
        public static string CLDGWeight {
            get {
                return ResourceManager.GetString("CLDGWeight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CL No Of ULD.
        /// </summary>
        public static string CLNoOfULD {
            get {
                return ResourceManager.GetString("CLNoOfULD", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CL Other Special Cargo.
        /// </summary>
        public static string CLOtherSpecialCargo {
            get {
                return ResourceManager.GetString("CLOtherSpecialCargo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CL Quantity DG.
        /// </summary>
        public static string CLQuantityDG {
            get {
                return ResourceManager.GetString("CLQuantityDG", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CL Type Of DG.
        /// </summary>
        public static string CLTypeOfDG {
            get {
                return ResourceManager.GetString("CLTypeOfDG", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Co L No Of ULD.
        /// </summary>
        public static string CoLNoOfULD {
            get {
                return ResourceManager.GetString("CoLNoOfULD", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Co L Other Special Cargo.
        /// </summary>
        public static string CoLOtherSpecialCargo {
            get {
                return ResourceManager.GetString("CoLOtherSpecialCargo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Co L Quantity Of DG.
        /// </summary>
        public static string CoLQuantityOfDG {
            get {
                return ResourceManager.GetString("CoLQuantityOfDG", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Co L Type Of DG.
        /// </summary>
        public static string CoLTypeOfDG {
            get {
                return ResourceManager.GetString("CoLTypeOfDG", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Co LULD Weight.
        /// </summary>
        public static string CoLULDWeight {
            get {
                return ResourceManager.GetString("CoLULDWeight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company Name.
        /// </summary>
        public static string CompanyName {
            get {
                return ResourceManager.GetString("CompanyName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company Name is required..
        /// </summary>
        public static string CompanyNameRequired {
            get {
                return ResourceManager.GetString("CompanyNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Crane Operating Company.
        /// </summary>
        public static string CraneOperatingCompany {
            get {
                return ResourceManager.GetString("CraneOperatingCompany", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Crane Operating Company is required..
        /// </summary>
        public static string CraneOperatingCompnayRequired {
            get {
                return ResourceManager.GetString("CraneOperatingCompnayRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Estimated time of Arrival UTC.
        /// </summary>
        public static string ETAUTC {
            get {
                return ResourceManager.GetString("ETAUTC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Estimated Time of Departure UTC.
        /// </summary>
        public static string ETDUTC {
            get {
                return ResourceManager.GetString("ETDUTC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fax.
        /// </summary>
        public static string Fax {
            get {
                return ResourceManager.GetString("Fax", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First Name.
        /// </summary>
        public static string FirstName {
            get {
                return ResourceManager.GetString("FirstName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to From.
        /// </summary>
        public static string From {
            get {
                return ResourceManager.GetString("From", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fumigation Required.
        /// </summary>
        public static string FumigationRequired {
            get {
                return ResourceManager.GetString("FumigationRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Has Special Permission?.
        /// </summary>
        public static string HasSpecialPermission {
            get {
                return ResourceManager.GetString("HasSpecialPermission", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hotel Booking.
        /// </summary>
        public static string HotelBooking {
            get {
                return ResourceManager.GetString("HotelBooking", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Inbound Date Time UTC.
        /// </summary>
        public static string InboundDateTimeUTC {
            get {
                return ResourceManager.GetString("InboundDateTimeUTC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Last Name.
        /// </summary>
        public static string LastName {
            get {
                return ResourceManager.GetString("LastName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Loading Instruction.
        /// </summary>
        public static string LoadingInstruction {
            get {
                return ResourceManager.GetString("LoadingInstruction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to MET Or ATS Delivery.
        /// </summary>
        public static string METOrATSDelivery {
            get {
                return ResourceManager.GetString("METOrATSDelivery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Of Arriving Passengers.
        /// </summary>
        public static string NoOfArrivingPassengers {
            get {
                return ResourceManager.GetString("NoOfArrivingPassengers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Of Departing Passengers.
        /// </summary>
        public static string NoOfDepartingPassengers {
            get {
                return ResourceManager.GetString("NoOfDepartingPassengers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Outbound Date and Time UTC.
        /// </summary>
        public static string OutboundDateTimeUTC {
            get {
                return ResourceManager.GetString("OutboundDateTimeUTC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Passenger Stairs.
        /// </summary>
        public static string PassengerStairs {
            get {
                return ResourceManager.GetString("PassengerStairs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Permitted To Add Good?.
        /// </summary>
        public static string PermittedToAddGood {
            get {
                return ResourceManager.GetString("PermittedToAddGood", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Phone No.
        /// </summary>
        public static string Phone {
            get {
                return ResourceManager.GetString("Phone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Power Type.
        /// </summary>
        public static string PowerType {
            get {
                return ResourceManager.GetString("PowerType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pushback.
        /// </summary>
        public static string Pushback {
            get {
                return ResourceManager.GetString("Pushback", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request WB Calculation.
        /// </summary>
        public static string RequestWBCalculation {
            get {
                return ResourceManager.GetString("RequestWBCalculation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Security Surveillance For RAmp Hours.
        /// </summary>
        public static string SecuritySurveillanceForRAmpHours {
            get {
                return ResourceManager.GetString("SecuritySurveillanceForRAmpHours", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Standby Fire Fighting.
        /// </summary>
        public static string StandbyFireFighting {
            get {
                return ResourceManager.GetString("StandbyFireFighting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to To.
        /// </summary>
        public static string To {
            get {
                return ResourceManager.GetString("To", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Toilet Service.
        /// </summary>
        public static string ToiletService {
            get {
                return ResourceManager.GetString("ToiletService", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Washing Required.
        /// </summary>
        public static string WasingRequired {
            get {
                return ResourceManager.GetString("WasingRequired", resourceCulture);
            }
        }
    }
}
