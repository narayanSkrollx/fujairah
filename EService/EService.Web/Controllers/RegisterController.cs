﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EService.Core.Security;
using EService.Data.Entity;
using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Service.Model;
using EService.Web.Helper;
using EService.Web.Models;

namespace EService.Web.Controllers
{
    public class RegisterController : Controller
    {
        private LookupService lookupService;
        private UserService userService;
        private ServiceContext serviceContext;
        private EmailService emailService;
        public RegisterController()
        {
            serviceContext = new ServiceContext();
            lookupService = new LookupService();
            userService = new UserService(context: serviceContext);
            emailService = new EmailService(serviceContext);
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Create(UserRegisterViewModel model)
        {
            var result = new JsonResponseObject();
            try
            {
                if (ModelState.IsValid)
                {
                    var password = model.PasswordHash;
                    model.PasswordHash = model.PasswordHash.ToMd5();
                    User user;
                    string message = "";
                    var saveResult = userService.RegisterUser(model, out user, out message);
                    if (saveResult)
                    {
                        result.IsSuccess = true;
                        result.Message = "User Registered Successfully.";
                        //send email

                        var emailModel = new NewRegistrationEmailModel()
                        {
                            UserName = model.UserName,
                            FullName = model.FirstName + " "+ model.LastName,
                            Password = password
                        };
                        var smtpSetting = emailService.Find(o => true);
                        var content = RenderPartialView("~/Views/Shared/EmailTemplates/_NewRegistrationEmailTemplate.cshtml", emailModel);
                        EmailHelper.SendEmail(smtpSetting.From, new string[] { model.Email }, new string[] { }, new string[] { }, "Successful Registration", content, smtpSetting);
                    }
                    else
                    {
                        result.IsSuccess = false;
                        result.Message = message;
                    }
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = ModelState.GetModelStateError();
                }
            }
            catch (Exception ex)
            {
                //LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while registering for the user.";
            }
            return Json(result);
        }

        public JsonResult Initialize()
        {
            var result = new JsonResponseObject();
            try
            {
                var model = new UserRegisterViewModel()
                {
                    SecurityQuestions = lookupService.GetList<SecurityQuestion>().ToList(),
                    CompanyCategories = lookupService.GetList<CompanyCategory>().ToList(),
                    Countries = CountryHelper.GetListOfCountries().Select(o=> new  LookupItem{ Description= o }).ToList()
                };

                result.Data = model;
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "Error while obtaining model: " + ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        protected virtual string RenderPartialView(string partialViewName, object model = null)
        {
            if (ControllerContext == null)
                return string.Empty;

            if (string.IsNullOrEmpty(partialViewName))
                throw new ArgumentNullException("partialViewName");

            ModelState.Clear();//Remove possible model binding error.

            ViewData.Model = model;//Set the model to the partial view

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, partialViewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}