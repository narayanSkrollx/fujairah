﻿using System;
using System.Linq;
using System.Web.Mvc;
using EService.Data.Entity;
using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Service.Model;
using EService.Web.Controllers.Base;
using EService.Web.Models;

namespace EService.Web.Controllers
{
    public class ServiceApprovalFlowConfigurationController : SystemAdminBaseController
    {
        private ServiceApprovalStageService serviceApprovalStageService;
        private LookupService lookupService;
        private DepartmentService departmentService;

        public ServiceApprovalFlowConfigurationController()
        {
            serviceApprovalStageService = new ServiceApprovalStageService(serviceContext);
            lookupService = new LookupService();
            departmentService = new DepartmentService(serviceContext);
        }

        public ActionResult Index()
        {
            return View();
        }


        public JsonResult GetServiceApprovalStageModal(int id)
        {
            var result = new JsonResponseObject();
            try
            {
                var modal = serviceApprovalStageService.GetServiceApprovalStage(id);
                var vm = new ServiceApprovalStageViewModel()
                {
                    ServiceId = modal.ServiceId,
                    ServiceName = modal.ServiceName,
                    ServiceApprovalStages = modal.ServiceApprovalStages,
                    Departments = departmentService.FindAllAsNoTracking(o => !o.IsDeleted).Select(o => new LookupItem()
                    {
                        Description = o.DepartmentName,
                        Id = o.DepartmentId
                    }).ToList()
                };
                result.IsSuccess = true;
                result.Data = vm;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while getting service approval stage: " + ex.Message;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(ServiceApprovalStageViewModel modal)
        {
            var result = new JsonResponseObject();
            try
            {
                var saveModel = (ServiceApproveModal)modal;
                var savedResponse = serviceApprovalStageService.Save(saveModel);
                result.IsSuccess = savedResponse.IsSuccess;
                result.Data = null;
                result.Message = savedResponse.Message;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving service approval flow configuration: " + ex.Message;
            }
            return Json(result);
        }

    }
}