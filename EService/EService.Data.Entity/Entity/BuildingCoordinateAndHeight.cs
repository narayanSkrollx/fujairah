﻿using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class BuildingCoordinateAndHeight
    {
        public int UserServiceRequestMasterId { get; set; }
        public int BuildingCoordinateAndHeightLookUpId { get; set; }
        public string Easting { get; set; }
        public string Northing { get; set; }
        public double? GroundHeight { get; set; }
        public double? StructureHeight { get; set; }

        // navigation properties
        [JsonIgnore]
        public virtual BuildingHeightRequest BuildingHeightRequest { get; set; }
        [JsonIgnore]
        public BuildingCoordinateAndHeightLookUp BuildingCoordinateAndHeightLookUp { get; set; }
    }
}