using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class ServiceApprovalStageDepartment
    {
        public int ServiceApprovalStageDepartmentId { get; set; }
        public int ServiceApprovalStageId { get; set; }
        public int DepartmentId { get; set; }


        [JsonIgnore]
        public virtual ServiceApprovalStage ServiceApprovalStage { get; set; }
        [JsonIgnore]
        public virtual Department Department { get; set; }
    }
}