using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class DepartmentService : BusinessAccessBase<Department>
    {
        public DepartmentService(ServiceContext context) : base(context)
        {
        }
    }
}