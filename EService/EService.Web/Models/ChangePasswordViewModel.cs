using System.ComponentModel.DataAnnotations;

namespace EService.Web.Models
{
    public class ChangePasswordViewModel
    {
        [Display(Name = "User Name")]
        public string UserName { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name="Current Password")]
        [Required(ErrorMessage = "Current Password is required.")]
        public string CurrentPassword { get; set; }
        [Display(Name="New Password")]
        [Required(ErrorMessage = "New Password is required.")]
        public string NewPassword { get; set; }
        [Display(Name="Confirm New Password")]
        [Compare("NewPassword", ErrorMessage = "Confirm Password must match to new password")]
        public string ConfirmPassword { get; set; }
    }
}