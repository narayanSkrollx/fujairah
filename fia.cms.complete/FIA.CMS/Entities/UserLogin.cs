﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace FIA.CMS.Entities
{
    [TableName("UserLogin")]
    [PrimaryKey("Id", autoIncrement = true)]
    [ExplicitColumns]
    public class UserLogin
    {

        [Column("Id")]
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }
        [Required]
        [Column("UserName")]
        public string UserName { get; set; }
        [Column("Password")]
        [Required]
        public string Password { get; set; }
        [Column("FirstName")]
        [Required]
        public string FirstName { get; set; }
        [Column("LastName")]
        [Required]
        public string LastName { get; set; }
        [Column("Email")]
        [Required]
        public string Email { get; set; }
        [Column("ResumePath")]
        public string ResumePath { get; set; }
        [Column("YearOfExperience")]
        public string YearOfExperience { get; set; }
        [Column("Overview")]
        public string Overview { get; set; }
        [Column("Contact")]
        public string Contact { get; set; }
       


    }
}