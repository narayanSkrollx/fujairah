﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;

namespace FIA.CMS.Entities
{
    [TableName("UserSubscription")]
    [ExplicitColumns]
    public class UserSubscription
    {
        [Column("Email")]
        [Required]
        public string Email { get; set; }
      //  public bool? IsSubscribe { get; set; }
    }
}