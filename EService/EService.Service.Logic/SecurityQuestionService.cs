﻿using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class SecurityQuestionService : BusinessAccessBase<SecurityQuestion>
    {
        public SecurityQuestionService(ServiceContext context) : base(context)
        {
        }
    }
}