﻿using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class CurrencyService : BusinessAccessBase<Currency>
    {
        public CurrencyService(ServiceContext context) : base(context)
        {
        }
    }
}
