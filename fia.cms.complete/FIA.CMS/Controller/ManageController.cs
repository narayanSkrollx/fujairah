﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using FIA.CMS.Entities;
using FIA.CMS.Model;
using umbraco.presentation.plugins.tinymce3;
using System.Net.Mail;
using FIA.CMS.Helper;

namespace FIA.CMS.Controller
{
    public class ManageController : BaseController
    {

        [HttpPost]
        public ActionResult Login(ManageLoginModel model)
        {
            var result = new JsonResponseObject();
            try
            {
                if (ModelState.IsValid)
                {
                    var db = ApplicationContext.DatabaseContext.Database;
                    var sql = "select * from UserLogin Where UserName= @0 and password= @1";
                    var users = db.Query<UserLogin>(sql, model.UserName, model.Password);
                    var userLogins = users as UserLogin[] ?? users.ToArray();
                    if (userLogins.Any())
                    {
                        FormsAuthentication.SetAuthCookie(model.UserName, false);
                        result.IsSuccess = true;
                        result.Data = userLogins[0];
                    }
                    else
                    {
                        result.IsSuccess = false;
                        result.Message = "Invalid User name or password.";
                    }
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "Please provide both Username and password properly.";
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "Error while trying to log in. Please try again later.";
                Logger.Error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "ManageController.Login", ex);
            }
            return Json(result);
        }

        #region "Register"
        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            var result = new JsonResponseObject();
            try
            {
                var db = ApplicationContext.DatabaseContext.Database;
                var sql = "select * from UserLogin Where UserName='" + model.UserName.Trim() + "'";
                var users = db.Query<UserLogin>(sql);
                if (users.Any())
                {
                    result.IsSuccess = false;
                    result.Message = "User Name already exist. Try another user name.";
                }
                else
                {
                    var emailCheckQuery = "select * from UserLogin Where Email=@0";
                    var duplicateEmailRecords = db.Query<UserLogin>(emailCheckQuery, model.Email);

                    if (duplicateEmailRecords.Any())
                    {
                        result.IsSuccess = false;
                        result.Message = "Provided email address is already registered in the system. Please use login or forgot password option.";
                    }
                    else
                    {
                        var path = string.Empty;
                        var guid = Guid.NewGuid();
                        var attachments = new List<Attachment>();
                        if (model.File != null)
                        {
                            var pathString =
                                System.IO.Path.Combine(
                                    new DirectoryInfo(string.Format("{0}Media\\{1}", Server.MapPath(@"\"), guid))
                                        .ToString());

                            bool isExists = System.IO.Directory.Exists(pathString);
                            if (!isExists)
                                System.IO.Directory.CreateDirectory(pathString);

                            model.File.SaveAs(pathString + "/" + model.File.FileName);
                            path = string.Format("/Media/{0}/{1}", guid, model.File.FileName);

                            if (model.File != null)
                            {
                                var attachment = new Attachment(model.File.InputStream, model.File.FileName);
                                attachments.Add(attachment);
                            }
                        }

                        var userLogin = new UserLogin()
                        {
                            UserName = model.UserName,
                            Password = model.Password,
                            FirstName = model.FirstName,
                            LastName = model.LastName,
                            Email = model.Email,
                            Contact = model.Contact,
                            Overview = model.Overview,
                            YearOfExperience = model.YearOfExperience,
                            ResumePath = path
                        };
                        var data = db.Insert(userLogin);
                        db.CompleteTransaction();
                        var to = EmailSenderAddress.GetSenderEmail("Registration");
                        this.SendEmail(GetRegisterMessageBody(userLogin), to,
                            "New Registration -" + userLogin.FirstName + " " + userLogin.LastName, attachments);
                        SendRegsitrationSuccessEmail(userLogin);
                        FormsAuthentication.SetAuthCookie(userLogin.UserName, false);
                        result.IsSuccess = true;
                        result.Message = "User registration successful.";
                        result.Data = data;
                    }
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "Error while trying to register. Please try again later.";
                Logger.Error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "ManageController.Register", ex);
            }

            return Json(result);
        }


        private void SendRegsitrationSuccessEmail(UserLogin model)
        {
            if (HttpContext.Request.UrlReferrer.ToString().Contains("/ar/"))
            {
                //arabic
                this.SendEmail(RenderPartialView("_SuccessfulRegistrationEmailTemplateAr", model), model.Email,
                 "NO REPLY-FIA HR");
            }
            else
            {
                this.SendEmail(RenderPartialView("_SuccessfulRegistrationEmailTemplate", model), model.Email,
                    "NO REPLY-FIA HR");
            }
        }
        #endregion

        public ActionResult GetUsers()
        {
            var db = ApplicationContext.DatabaseContext.Database;
            var users = db.Query<UserModel>("select  * from UserLogin");

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChangePassword()
        {
            var model = new ChangePaswordModal();
            return View($"{HomePartialViewLocation}_ChangePassword.cshtml", model);
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePaswordModal model)
        {
            try
            {
                if (ModelState.IsValid && model.Id != 0)
                {
                    var db = ApplicationContext.DatabaseContext.Database;
                    var sql = "update  UserLogin set Password='" + model.Password + "' Where Id=" + model.Id;
                    db.Execute(sql);
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "ManageController.ChangePassword", ex);
                return Json(false, JsonRequestBehavior.AllowGet);

            }
        }

        public string GetRegisterMessageBody(UserLogin model)
        {
            var messagebody = "<table>";
            if (!string.IsNullOrWhiteSpace(model.FirstName) && !string.IsNullOrWhiteSpace(model.LastName))
            {
                messagebody += string.Format("<tr><td>Full Name</td><td>{0}</td></tr>", model.FirstName + " " + model.LastName);
            }
            if (!string.IsNullOrWhiteSpace(model.Email))
            {
                messagebody += string.Format("<tr><td>Email Address</td><td>{0}</td></tr>", model.Email);
            }
            if (!string.IsNullOrWhiteSpace(model.UserName))
            {
                messagebody += string.Format("<tr><td>User Name</td><td>{0}</td></tr>", model.UserName);
            }
            if (!string.IsNullOrWhiteSpace(model.Password))
            {
                messagebody += string.Format("<tr><td>Password</td><td>{0}</td></tr>", model.Password);
            }
            if (!string.IsNullOrWhiteSpace(model.Contact))
            {
                messagebody += string.Format("<tr><td>Contact</td><td>{0}</td></tr>", model.Contact);
            }
            if (!string.IsNullOrWhiteSpace(model.Overview))
            {
                messagebody += string.Format("<tr><td>Overview</td><td>{0}</td></tr>", model.Overview);
            }
            if (!string.IsNullOrWhiteSpace(model.YearOfExperience))
            {
                messagebody += string.Format("<tr><td>Year Of Experience</td><td>{0}</td></tr>", model.YearOfExperience);
            }
            return messagebody;
        }

        public ActionResult CheckUserName(string UserName, string Id = null)
        {
            try
            {
                var db = ApplicationContext.DatabaseContext.Database;

                var a = db.Query<UserLogin>("select * from UserLogin Where UserName=@0 ", UserName.Trim());
                if (a.Any())
                {

                    return Json(true, "User Name already exist. Try another");
                }
                return Json(true);
            }
            catch (Exception ex)
            {
                Logger.Error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "ManageController.CheckUserName", ex);
                return Json(true);

            }
        }

        public ActionResult GetManageProfile()
        {
            var userId = GetCurrentUserId();
            var db = ApplicationContext.DatabaseContext.Database;
            var users = db.Query<UserModel>("select  * from UserLogin Where Id=@0 ", userId);
            return PartialView($"{HomePartialViewLocation}_ManageProfile.cshtml", users.FirstOrDefault());

        }

        public ActionResult GetProfile(int userId)
        {

            var db = ApplicationContext.DatabaseContext.Database;
            var users = db.Query<UserModel>("select  * from UserLogin Where Id=@0 ", userId);
            return View($"{HomePartialViewLocation}_Profile.cshtml", users.FirstOrDefault());

        }

        public FileResult DownoadResume(int userId)
        {
            var db = ApplicationContext.DatabaseContext.Database;
            var users = db.Query<UserModel>("select  * from UserLogin Where Id=@0 ", userId);
            var path = users.FirstOrDefault().ResumePath;
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath(path));
            string fileName = System.IO.Path.GetFileName(path);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpPost]
        public ActionResult UserUpdate(UserModel model)
        {
            var result = new JsonResponseObject();
            try
            {
                if (ModelState.IsValid)
                {
                    var db = ApplicationContext.DatabaseContext.Database;
                    var userExist = db.Query<UserLogin>("select * from UserLogin Where UserName=@0 and Id !=@1",
                        model.UserName.Trim(), model.Id);
                    if (userExist.Any())
                    {
                        result.IsSuccess = false;
                        result.Message = "User Name which you are trying to update already exists. Please try another or leave your user name as it is.";
                    }
                    else
                    {
                        var emailCheckQuery = "select * from UserLogin Where Email=@0 and Id != @1";
                        var duplicateEmailRecords = db.Query<UserLogin>(emailCheckQuery, model.Email, model.Id);

                        if (duplicateEmailRecords.Any())
                        {
                            result.IsSuccess = false;
                            result.Message = "The email which you are trying to update already exists. Pleasy try another or leave your email as it is.";
                        }
                        else
                        {
                            var users = db.Query<UserLogin>("select * from UserLogin Where Id=@0 ", model.Id);
                            var user = users.ToList()[0];
                            var path = user.ResumePath;
                            if (model.File != null)
                            {
                                var guid = Guid.NewGuid();
                                var pathString = Path.Combine(new DirectoryInfo(string.Format("{0}Media\\{1}", Server.MapPath(@"\"), guid)).ToString());

                                bool isExists = Directory.Exists(pathString);
                                if (!isExists)
                                {
                                    Directory.CreateDirectory(pathString);
                                }

                                model.File.SaveAs(string.Format(pathString + "/{0}", model.File.FileName));
                                path = string.Format("/Media/{0}/{1}", guid, model.File.FileName);

                            }

                            var userLogin = new UserLogin()
                            {
                                Id = model.Id,
                                UserName = model.UserName,
                                FirstName = model.FirstName,
                                LastName = model.LastName,
                                Email = model.Email,
                                Contact = model.Contact,
                                Overview = model.Overview,
                                ResumePath = path,
                                YearOfExperience = model.YearOfExperience,
                                Password = user.Password
                            };
                            db.Save(userLogin);
                            result.IsSuccess = true;
                            result.Message = "Profile information updated successfully.";
                        }
                    }
                }
                else
                {
                    var errors = ViewData.ModelState.Values.SelectMany(modelState => modelState.Errors).Aggregate("", (current, error) => current + (error.ErrorMessage + "</br>"));
                    result.IsSuccess = false;
                    result.Message = errors;
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "Error while updating profile information. Please try again later.";
                Logger.Error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "ManageController.UserUpdate", ex);
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult LogOut()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return Redirect("/");
        }

        public JsonResult IsAuthenticated()
        {
            return Json(User.Identity.IsAuthenticated, JsonRequestBehavior.AllowGet);
        }
    }
}