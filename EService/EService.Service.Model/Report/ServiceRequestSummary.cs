﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EService.Service.Model.Report
{
    public class ServiceRequestSummary
    {
        public int ServiceId { get; set; }
        public string ServiceCode { get; set; }
        public string ServiceName { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public int Count { get; set; }
        public decimal DefaultRate { get; set; }
        public double ChargedAmount { get; set; }
        public double PaidAmount { get; set; }
    }
}
