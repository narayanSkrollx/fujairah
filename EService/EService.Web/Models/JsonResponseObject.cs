﻿using EService.Service.Model;

namespace EService.Web.Models
{
    public class JsonResponseObject : ResponseObject
    {
        public bool IsRedirect { get; set; }
    }
}