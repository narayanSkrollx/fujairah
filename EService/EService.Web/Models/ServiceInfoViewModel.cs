﻿using System.Collections.Generic;
using EService.Service.Model;
using EService.Web.Areas.Configure.Models;

namespace EService.Web.Models
{
    public class ServiceInfoViewModel
    {
        public ServiceInfoViewModel()
        {
            Records = new HashSet<ServiceInfoServiceModel>();
            AddNewModel = new ServiceInfoAddViewModel();
        }

        public HashSet<ServiceInfoServiceModel> Records { get; set; }
        public ServiceInfoAddViewModel AddNewModel { get; set; }
    }
}