using System.Collections.Generic;
using System.Security.Policy;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class ServiceApprovalStage : AuditableEntity
    {
        public int ServiceApprovalStageId { get; set; }
        public int ServiceId { get; set; }
        public int Index { get; set; }

        [JsonIgnore]
        public virtual Service Service { get; set; }
        [JsonIgnore]
        public virtual ICollection<ServiceApprovalStageDepartment> ServiceApprovalStageDepartments { get; set; }
    }
}