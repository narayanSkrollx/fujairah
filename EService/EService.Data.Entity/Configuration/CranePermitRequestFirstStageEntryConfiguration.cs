using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class CranePermitRequestFirstStageEntryConfiguration : EntityTypeConfiguration<CranePermitRequestFirstStageEntry>
    {
        public CranePermitRequestFirstStageEntryConfiguration()
        {
            ToTable("CranePermitRequestFirstStageEntry");

            HasKey(o => o.UserServiceRequestMasterId);
            Property(o => o.UserServiceRequestMasterId).IsRequired();
            Property(o => o.HasPentrationOfSurface);
            Property(o => o.PenetrationInMeter).IsOptional();
            Property(o => o.RequireAviationLight).IsRequired();
            Property(o => o.Comments).IsOptional().HasMaxLength(1024);
            Property(o => o.MaxCraneOperatingHeight).IsRequired();
            Property(o => o.CraneOperatingDistanceFromRunway).IsRequired();
            Property(o => o.Recommendations).IsOptional().HasMaxLength(1024);
            Property(o => o.SavedByUserId).IsRequired();
            Property(o => o.SavedDateTime).IsRequired();
            Property(o => o.AssessedBy).IsOptional().HasMaxLength(1024);
            Property(o => o.AirportPlanner).IsOptional().HasMaxLength(1024);
            Property(o => o.AssessedDate).IsOptional();

            HasRequired(o => o.User).WithMany(o => o.CranePermitRequestFirstStageEntries).HasForeignKey(o => o.SavedByUserId).WillCascadeOnDelete(false);
            HasRequired(o => o.CranePermitRequest).WithRequiredDependent(o => o.CranePermitRequestFirstStageEntry).WillCascadeOnDelete(false);
        }
    }

    public class CranePermitSafetyQAApprovalConfiguration : EntityTypeConfiguration<CranePermitSafetyQAApproval>
    {
        public CranePermitSafetyQAApprovalConfiguration()
        {
            ToTable("CranePermitSafetyQAApproval");

            HasKey(o => o.UserServiceRequestMasterId);
            Property(o => o.UserServiceRequestMasterId).IsRequired();
            Property(o => o.HasSafetyAssessmentBeenDone);
            Property(o => o.ApprovedBy).IsRequired().HasMaxLength(255);
            Property(o => o.ApprovedByPosition).IsRequired().HasMaxLength(255);
            Property(o => o.ApprovedDateTime).IsRequired();
            Property(o => o.OperatingRestrictions).IsRequired().HasMaxLength(1024);
            Property(o => o.AdditionalComments).IsRequired().HasMaxLength(1024);

            HasRequired(o => o.CranePermitRequest).WithRequiredDependent(o => o.CranePermitSafetyQAApproval).WillCascadeOnDelete(false);
        }
    }
}