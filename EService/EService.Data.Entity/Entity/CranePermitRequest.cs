using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class CranePermitRequest : AuditableEntity, IServiceEntity
    {
        public int UserServiceRequestMasterId { get; set; }

        public string ApplicantName { get; set; }
        public DateTime ApplicationDate { get; set; }
        public string CompanyName { get; set; }
        public string Position { get; set; }
        public string ContactNumber { get; set; }
        public string ApplicantEmail { get; set; }
        public bool IsNewApplication { get; set; }
        public string PreviousPermitNumbers { get; set; }
        public string NorthingsEastings { get; set; }
        public string CraneLocation { get; set; }
        public DateTime OperationStartDate { get; set; }
        public DateTime OperationEndDate { get; set; }
        public string OperationStartTime { get; set; }
        public string OperationEndTime { get; set; }
        public int MaxOperatingHeight { get; set; }
        public string PurposeOfUser { get; set; }
        public string CraneOperatingCompany { get; set; }
        public string OperatorName { get; set; }
        public string OperatorContactNumber { get; set; }
        public bool? IsInternal { get; set; }

        [JsonIgnore]
        public virtual UserServiceRequestMaster UserServiceRequestMaster { get; set; }

        [JsonIgnore]
        public virtual ICollection<CranePermitRequestUserAttachment> CranePermitRequestUserAttachments { get; set; }

        [JsonIgnore]
        public virtual ICollection<CranePermitRequestSelectedCrane> CranePermitRequestSelectedCranes { get; set; }

        [JsonIgnore]
        public virtual CranePermitRequestFirstStageEntry CranePermitRequestFirstStageEntry { get; set; }

        [JsonIgnore]
        public virtual CranePermitSafetyQAApproval CranePermitSafetyQAApproval { get; set; }

    }
}