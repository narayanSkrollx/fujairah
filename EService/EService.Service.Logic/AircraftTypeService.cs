﻿using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class AircraftTypeService : BusinessAccessBase<AircraftType>
    {
        public AircraftTypeService(ServiceContext context) : base(context)
        {
        }
    }
}