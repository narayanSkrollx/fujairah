using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class TradeLicenseRequestContiguration : EntityTypeConfiguration<TradeLicenseRequest>
    {
        public TradeLicenseRequestContiguration()
        {
            ToTable("TradeLicenseRequest");
            HasKey(x => x.UserServiceRequestMasterId);

            Property(x => x.UserServiceRequestMasterId).IsRequired();
            Property(x => x.ApplicantName).IsRequired();
            Property(x => x.PresentSponsorInUAE).IsOptional().HasMaxLength(225);
            Property(x => x.AddressOfSponser).IsOptional().HasMaxLength(225);
            Property(x => x.ApplicantOfficeNumber).IsOptional().HasMaxLength(225);
            Property(x => x.ApplicantResidenceNumber).IsOptional().HasMaxLength(225);
            Property(x => x.ApplicantMobileNumber).IsOptional().HasMaxLength(225);
            Property(x => x.ApplicantFaxNumber).IsOptional().HasMaxLength(225);
            Property(x => x.ApplicantEmailNumber).IsOptional().HasMaxLength(225);
            Property(x => x.ApplicantCountry).IsOptional().HasMaxLength(225);
            Property(x => x.MailingAddress).IsOptional().HasMaxLength(225);
            Property(x => x.ApplicationDate).IsOptional();
            Property(x => x.CompanyPreferredName).IsOptional().HasMaxLength(225);
            Property(x => x.CompanyNameOption1).IsOptional().HasMaxLength(225);
            Property(x => x.CompanyNameOption2).IsOptional().HasMaxLength(225);
            Property(x => x.CompanyNameOption3).IsOptional().HasMaxLength(225);
            Property(x => x.BusinessActivities1).IsOptional().HasMaxLength(225);
            Property(x => x.BusinessActivities2).IsOptional().HasMaxLength(225);
            Property(x => x.BusinessActivities3).IsOptional().HasMaxLength(225);
            Property(x => x.Remarks).IsOptional().HasMaxLength(225);
            Property(x => x.ExpectedAnnualBusinessTrunoverAmount).IsOptional();
            Property(x => x.ExpectedAnnualBusinessTrunoverCurrencyId).IsOptional();
            Property(x => x.TotalStaffStrengthRequired).IsOptional().HasMaxLength(225);

            HasRequired(x => x.UserServiceRequestMaster).WithRequiredDependent(x => x.TradeLicenseRequest).WillCascadeOnDelete(false);
            HasOptional(x => x.Currency).WithMany(x => x.TradeLicenseRequests).HasForeignKey(x => x.ExpectedAnnualBusinessTrunoverCurrencyId).WillCascadeOnDelete(false);
        }
    }
}