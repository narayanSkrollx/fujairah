using System.Collections.Generic;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class ProjectType : ILookupItem
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public bool IsOther { get; set; }

        //[JsonIgnore]
        //public virtual ICollection<AircraftWarningLightProjectType> AircraftWarningLightProjectTypes { get; set; }
    }
}