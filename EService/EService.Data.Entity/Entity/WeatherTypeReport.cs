using System.Collections.Generic;

namespace EService.Data.Entity.Entity
{
    public class WeatherTypeReport : ILookupItem
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }

        public virtual ICollection<MeteorologicalDataRequestWeatherType> MeteorologicalDataRequestWeatherTypes
        {
            get; set;
        }
    }
}