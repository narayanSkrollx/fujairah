using System.Web.Mvc;
using EService.Data.Entity;

namespace EService.Web.Controllers.Base
{
    public abstract class ExternalUserBaseController : BaseController
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            if (UserProfile.UserType != UserTypeEnum.ExternalUser)
            {
                filterContext.Result = UnAuthroizedResult();
            }

        }
    }
}