﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FIA.CMS.Model
{
    public class TeamMemberModel
    {
        public string Name { get; set; }
        public string Designation { get; set; }
        public string About { get; set; }
        public string ImageUrl { get; set; }
    }
}