﻿using EService.Service.Model;
using System.Data.Entity;

namespace EService.Web.Models
{
    public class UserServiceRequestUploadedFilesViewModel : UploadedFileInfoServiceModel
    {
        public UserServiceRequestUploadedFilesViewModel()
        {
            ObjectState = EntityState.Unchanged;
        }
        public EntityState ObjectState { get; set; }
    }
}