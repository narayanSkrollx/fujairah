using System.Collections.Generic;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class Currency : AuditableEntity
    {
        public int CurrencyId { get; set; }
        public string CurrencyName { get; set; }
        
        public virtual ICollection<TradeLicenseRequest> TradeLicenseRequests { get; set; }
    }
}