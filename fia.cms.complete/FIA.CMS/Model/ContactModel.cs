﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FIA.CMS.Model
{
    public class ContactModel
    {
        [Required]
        public string ContactType { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string EmailAddress { get; set; }

        [Required]
        public string Nationality { get; set; }

        [Required]
        public List<string> ServiceList { get; set; }
        public string ServiceOtherText { get; set; }

        [Required]
        public string Message { get; set; }

        //Captcha
    }
}