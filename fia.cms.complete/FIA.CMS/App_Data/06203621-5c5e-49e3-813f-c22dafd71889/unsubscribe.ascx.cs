﻿using System;
using System.Web.UI.WebControls;
using umbraco.cms.businesslogic.member;

namespace Designit.Umb.Newsletter.Custom.usercontrols.designit.newsletter
{
    public partial class unsubscribe : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GenerateCheckboxes();
            string email = Request["mail"];
            if (email != null)
            {
                txtEmail.Text = email;
            }

        }

        protected void btnUnsub_Click(object sender, EventArgs e)
        {
            if (Member.GetMemberFromEmail(txtEmail.Text) != null)
            {
                Member member = Member.GetMemberFromEmail(txtEmail.Text);
                Designit.Umb.Newsletter.Bll.Unsubscribe.UnsubscribeAll(member);
                Panel1.Visible = false;
                ltlConfirm.Visible = true;
                ltlConfirm.Text = "A confirmation mail has been sent to " + txtEmail.Text;
            }
            else
            {
                ltlConfirm.Visible = true;
                ltlConfirm.Text = txtEmail.Text + " is not a registered mail in database";
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (Member.GetMemberFromEmail(txtEmail.Text) != null)
            {
                Panel2.Visible = true;
                Panel3.Visible = true;
                foreach (var control in Panel2.Controls)
                {
                    if (control is CheckBox)
                        ((CheckBox)control).Checked = true;
                }
            }
            else
            {
                ltlConfirm.Visible = true;
                ltlConfirm.Text = txtEmail.Text + " is not a registered mail in database";
            }
        }

        private void GenerateCheckboxes()
        {
            if (!string.IsNullOrEmpty(txtEmail.Text))
            {
                Member member = Member.GetMemberFromEmail(txtEmail.Text);
                MemberGroup[] memberGroups = MemberGroup.GetAll;
                foreach (MemberGroup group in memberGroups)
                {
                    if (group.HasMember(member.Id))
                    {
                        CheckBox checkBox = new CheckBox();
                        checkBox.Text = group.Text;
                        checkBox.ID = "checkbox" + group.Id;
                        Panel2.Controls.Add(checkBox);
                    }
                }
            }
        }

        protected void btnUnsubSelected_Click(object sender, EventArgs e)
        {
            Member member = Member.GetMemberFromEmail(txtEmail.Text);
            Guid guid = Guid.NewGuid();
            bool changes = false;
            foreach (var control in Panel2.Controls)
            {
                if (control is CheckBox)
                {
                    var checkBox = (CheckBox)control;
                    if (!checkBox.Checked)
                    {
                        MemberGroup memberGroup = MemberGroup.GetByName(checkBox.Text);
                        Bll.Unsubscribe.UnsubscribeNotify(member, memberGroup, guid);
                        changes = true;
                    }
                }
            }
            if (changes)
            {
                Bll.Unsubscribe.SendConfirmMail(member, guid);
            }
            Panel1.Visible = false;
            ltlConfirm.Visible = true;
            ltlConfirm.Text = "A confirmation mail has been sent to " + txtEmail.Text;
        }




    }
}