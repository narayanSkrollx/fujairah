﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EService.Web.Helper
{
    public static class Extensions
    {
        public static string GetModelStateError(this ModelStateDictionary modelState)
        {
            return string.Join(Environment.NewLine,
                modelState.Keys.SelectMany(key => modelState[key].Errors.Select(x => key + ": " + x.ErrorMessage)));
        }

    }
}