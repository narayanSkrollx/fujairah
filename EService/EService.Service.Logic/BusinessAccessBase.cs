using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace EService.Service.Logic
{
    public abstract class BusinessAccessBase<T> : IDisposable where T : class
    {
        private bool disposed;

        #region .Ctor

        public BusinessAccessBase(ServiceContext context)
        {
            Context = context;
        }

        #endregion .Ctor

        #region Public Methods

        protected ServiceContext Context { get; set; }

        public virtual T Find(Expression<Func<T, bool>> predicate)
        {
            return Context.Database.Set<T>().FirstOrDefault(predicate);
        }

        public virtual IQueryable<T> FindAll(Expression<Func<T, bool>> predicate)
        {
            if (predicate != null)
            {
                return Context.Database.Set<T>().Where(predicate);
            }
            return Context.Database.Set<T>();
        }

        public virtual IQueryable<T> FindAllAsNoTracking(Expression<Func<T, bool>> predicate)
        {
            return FindAll(predicate).AsNoTracking();
        }

        public virtual IQueryable<TValue> FindBDTOAll<TValue>(Expression<Func<TValue, bool>> predicate) where TValue : class
        {
            if (predicate != null)
            {
                return Context.Database.Set<TValue>().Where(predicate);
            }
            return Context.Database.Set<TValue>().AsQueryable();
        }

        public virtual TBDTO FindBDTO<TBDTO>(Expression<Func<T, bool>> predicate, Func<T, TBDTO> mapping)
        {
            return Context.Database.Set<T>().Where(predicate).AsEnumerable().Select(mapping).FirstOrDefault();
        }

        public virtual T Add(T entity)
        {
            return Context.Database.Set<T>().Add(entity);
        }

        public virtual T Remove(T entity)
        {
            return Context.Database.Set<T>().Remove(entity);
        }

        //public bool CheckDuplicate(T table, Expression<Func<T, object>> field, object value)
        //{
        //    var sql = string.Format("select * from {0} where {1}={2}", table.ToString(), field.ToPropertyName(), value);
        //    var count = Context.Database.Database.ExecuteSqlCommand(sql);
        //    return count > 0;
        //    //return Context.Database.Set<T>().AsQueryable().Any( string.Format("{0}={1}", field, value));
        //}

        #endregion Public Methods

        #region Dispose Members

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~BusinessAccessBase()
        {
            Dispose(false);
        }

        #endregion Dispose Members
    }
}