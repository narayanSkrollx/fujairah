namespace EService.Service.Model
{
    public class ApplicableFeesLookupServiceModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public string Notes { get; set; }
        public decimal? Fee { get; set; }
    }
}