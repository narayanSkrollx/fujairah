﻿using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class AdditionalItemsForOHLSubmittal
    {
        public int AdditionalItemsForOHLSubmittalId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public int AdditionalItemsForOHLSubmittalLookUpId { get; set; }
        public string OtherDescription { get; set; }

        // navigation properties
        [JsonIgnore]
        public virtual BuildingHeightRequest BuildingHeightRequest { get; set; }
        [JsonIgnore]
        public virtual AdditionalItemsForOHLSubmittalLookUp AdditionalItemsForOHLSubmittalLookUp { get; set; }
    }
}