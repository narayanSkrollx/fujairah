﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EService.Data.Entity.Entity;
using EService.Service.Model;

namespace EService.Web.Models
{
    public class GroundHandlingRequestViewModel : EServiceCreateBaseViewModel
    {
        public GroundHandlingRequestViewModel()
        {
            GroundHandlingArcraftTypes = new List<GroundHandlingArcraftTypeViewModel>();
        }
        public int UserServiceRequestMasterId { get; set; }
        #region "Project information"
        [Display(Name = "Inboud Date")]
        [Required(ErrorMessage = "Inboun Date Time (UTC)")]
        public DateTime? InboundDateTimeUTC { get; set; }
        [Display(Name = "From")]
        [Required(ErrorMessage = "From is required.")]
        [StringLength(1024, ErrorMessage = "From Length cannot be greater than 1024")]
        public string From { get; set; }
        [Display(Name = "ETA (UTC)")]
        [Required(ErrorMessage = "ETA (UTC) is required.")]
        public DateTime? ETAUTC { get; set; }
        [Display(Name = "Outbound Date (UTC)")]
        [Required(ErrorMessage = "Outbound Date Time (UTC) is required")]
        public DateTime? OutboundDateTimeUTC { get; set; }
        [Required(ErrorMessage = "To is required.")]
        [StringLength(1024, ErrorMessage = "To Length canont be greater than 1024")]
        public string To { get; set; }
        [Display(Name = "ETD (UTC)")]
        [Required(ErrorMessage = "ETD (UTC) is required.")]
        public DateTime? ETDUTC { get; set; }
        #endregion
        public double? Weight { get; set; }
        [Display(Name = "Aircraft Registration")]
        public string AircraftRegistration { get; set; }
        [Display(Name = "Require Weight & Balance Calculation")]
        public bool? RequestWBCalculation { get; set; }
        [Display(Name = "Loading Instruction Report")]
        public bool? LoadingInstruction { get; set; }
        [Display(Name = "Delivery of MET and/or ATS")]
        public bool? METOrATSDelivery { get; set; }
        [Display(Name="No of Departing Passengers")]
        public int? NoOfDepartingPassengers { get; set; }
        [Display(Name="No of Arriving Pasengers")]
        public int? NoOfArrivingPassengers { get; set; }
        [Display(Name = "Airline Permitted to carry dangerous goods? (If Yes attach supporting files in file upload)")]
        public bool? PermittedToAddGood { get; set; }
        [Display(Name="Is Special Permissions available (MOU, MOD or Dip Clearance)(Supporting Documents to be attached below)")]
        public bool? HasSpecialPermission { get; set; }
        #region "Cargo Off Loading"
        /// <summary>
        /// ULR Or Bulk
        /// </summary>
        [Display(Name = "Cargo Off Loading type")]
        public bool? CargoOffLoadingType { get; set; }
        [Display(Name="No of ULDs")]
        public int? CoLNoOfULD { get; set; }
        [Display(Name="Weight (Kgs)")]
        public double? CoLULDWeight { get; set; }
        [Display(Name= "Type of DG (Supporting documents to be attached in file uploads)")]
        public string CoLTypeOfDG { get; set; }
        [Display(Name="Qty of DG")]
        public double? CoLQuantityOfDG { get; set; }
        [Display(Name="Other Special Cargo")]
        public string CoLOtherSpecialCargo { get; set; }
        #endregion

        #region "Cargo Loading"
        [Display(Name="Cargo Offloading Type")]
        public bool? CargoLoadingType { get; set; }
        [Display(Name="No of ULD")]
        public int? CLNoOfULD { get; set; }
        [Display(Name="ULD Weight (Kgs)")]
        public int? CLULDWeight { get; set; }
        [Display(Name="Type of DG (Supporting documents to be attached in file uploads)")]
        public string CLTypeOfDG { get; set; }
        [Display(Name="Qty of DG")]
        public double? CLQuantityDG { get; set; }
        [Display(Name="DG Weight (Kgs)")]
        public double? CLDGWeight { get; set; }
        [Display(Name="Other Special Cargo (Supporting documents to be attached in file uploads)")]
        public string CLOtherSpecialCargo { get; set; }

        #endregion


        #region "Applicant Info"
        [Display(Name ="Company Name")]
        [Required(ErrorMessage = "Company Name is required.")]
        [StringLength(1024, ErrorMessage = "Company Name length cannot be greater than 1024")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "First name is required.")]
        [StringLength(1024, ErrorMessage = "First Namelength cannot be greater than 1024")]
        [Display(Name="First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required.")]
        [StringLength(1024, ErrorMessage = "Last Name length cannot be greater than 1024")]
        [Display(Name="Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Address is required.")]
        [StringLength(1024, ErrorMessage = "Address length cannot be greater than 1024")]
        [Display(Name="Address")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Email is required.")]
        [StringLength(1024, ErrorMessage = "Email length cannot be greater than 1024")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Phone is required.")]
        [StringLength(1024, ErrorMessage = "Phone length cannot be greater than 1024")]
        public string Phone { get; set; }
        [StringLength(1024, ErrorMessage = "Fax length cannot be greater than 1024")]
        public string Fax { get; set; }
        [StringLength(1024, ErrorMessage = "SITATEX length cannot be greater than 1024")]
        public string SITATEX { get; set; }

        #endregion
        public AircraftInteriorCleaningViewModel AircraftCleaning { get; set; }

        public List<GroundHandlingArcraftTypeViewModel> GroundHandlingArcraftTypes { get; set; }
    }

    public class AircraftInteriorCleaningViewModel
    {
        public int UserServiceRequestMasterId { get; set; }
        [Display(Name="Aicraft Fumigation Required")]
        public bool FumigationRequired { get; set; }
        [Display(Name="Aicraft Washing Required")]
        public bool WasingRequired { get; set; }
        [StringLength(1024, ErrorMessage = "PowerType length cannot be greater than 1024")]
        public string PowerType { get; set; }
        public bool Pushback { get; set; }
        [Display(Name="Passenger Stairs")]
        public bool PassengerStairs { get; set; }
        [Display(Name="Toilet Service")]
        public bool ToiletService { get; set; }
        [Display(Name="Portable Water")]
        public bool PortableWater { get; set; }
        [StringLength(1024, ErrorMessage = "ASUHours length cannot be greater than 1024")]
        [Display(Name="ASU Hours")]
        public string ASUHours { get; set; }
        [Display(Name="Additional Platform for CRJ")]
        public bool AdditionalPlatformCRJ { get; set; }
        [Display(Name="Security Surveilance on the ramp hours")]
        public bool SecuritySurveillanceForRAmpHours { get; set; }
        [Display(Name="Hotel Booking")]
        public bool HotelBooking { get; set; }
        [Display(Name="Standby Fire fighting")]
        public bool StandbyFireFighting { get; set; }
        [Display(Name="Additional Manpower")]
        public bool AdditionalManpower { get; set; }
        [StringLength(1024, ErrorMessage = "AdditionalRequirements length cannot be greater than 1024")]
        [Display(Name="Additional Requirements")]
        public string AdditionalRequirements { get; set; }


    }

    public class GroundHandlingArcraftTypeViewModel
    {
        public bool IsSelected { get; set; }
        public int AircraftTypeId { get; set; }
        public string Description { get; set; }
        public bool IsOther { get; set; }
    }
}