using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class AdditionalItemsForOHLSubmittalLookUpConfiguration :
        EntityTypeConfiguration<AdditionalItemsForOHLSubmittalLookUp>
    {
        public AdditionalItemsForOHLSubmittalLookUpConfiguration()
        {
            ToTable("AdditionalItemsForOHLSubmittalLookUp");
            HasKey(x => x.Id);

            Property(x => x.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Description).IsRequired().HasMaxLength(225);
            Property(x => x.Active).IsRequired();
        }
    }
}