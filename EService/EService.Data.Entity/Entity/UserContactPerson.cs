using System;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class UserContactPerson
    {
        public int UserId { get; set; }
        public string ContactPersonName { get; set; }
        public Int64 ContactPersonMobile { get; set; }
        public string EmiratesId { get; set; }

        [JsonIgnore]
        public virtual User User { get; set; }
    }
}