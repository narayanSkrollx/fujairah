namespace FIA.CMS.Model
{
    public class ForgotPasswordEmailModel
    {
        public string Email { get; set; }
        public string Token { get; set; }   
    }
}