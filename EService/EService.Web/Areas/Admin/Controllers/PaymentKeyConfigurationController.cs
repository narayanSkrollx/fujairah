﻿using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Service.Model;
using EService.Web.Controllers.Base;
using System.Web.Mvc;

namespace EService.Web.Areas.Admin.Controllers
{
    public class PaymentKeyConfigurationController : SystemAdminBaseController
    {
        private PaymentKeysService paymentKeysService;

        public PaymentKeyConfigurationController()
        {
            paymentKeysService = new PaymentKeysService(serviceContext);

        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Index(PaymentKey model)
        {
            var result = new ResponseObject();

            try
            {
                var existingModel = paymentKeysService.Find(o => !o.IsDeleted);
                if(existingModel != null)
                {
                    existingModel.MerchantId = model.MerchantId;
                    existingModel.EncryptionKey = model.EncryptionKey;
                    existingModel.Url = model.Url;

                    serviceContext.Commit();
                    result.IsSuccess = true;
                    result.Message = "Payment Key configuration updated successfully.";
                       
                }
                else
                {
                    paymentKeysService.Add(model);
                    serviceContext.Commit();
                    result.IsSuccess = true;
                    result.Message = "Payment Key configuration added successfully.";
                }
            }
            catch (System.Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving Payment Key Configuration.";
            }

            return Json(result);
        }

        public JsonResult Initialize()
        {
            var result = new ResponseObject();
            try
            {
                var record = paymentKeysService.Find(o => !o.IsDeleted);
                result.IsSuccess = true;
                result.Data = record;

            }
            catch (System.Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while initializing payment key configuration.";
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}