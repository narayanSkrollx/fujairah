﻿using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class StaffStrengthService : BusinessAccessBase<StaffStrength>
    {
        public StaffStrengthService(ServiceContext context) : base(context)
        {
        }
    }
}