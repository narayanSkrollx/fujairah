using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class OHLStructureCoordinateAndHeightConfiguration
        : EntityTypeConfiguration<OHLStructureCoordinateAndHeight>
    {
        public OHLStructureCoordinateAndHeightConfiguration()
        {
            ToTable("OHLStructureCoordinateAndHeight");
            HasKey(x => x.OHLStructureCoordinateAndHeightId);

            Property(x => x.OHLStructureCoordinateAndHeightId).IsRequired();
            Property(x => x.UserServiceRequestMasterId).IsRequired();
            Property(x => x.StructureReferenceNumber).IsRequired().HasMaxLength(225);
            Property(x => x.Easting).IsRequired().HasMaxLength(225);
            Property(x => x.Northing).IsRequired().HasMaxLength(225);
            Property(x => x.GroundHeight).IsRequired();
            Property(x => x.StructureHeight).IsRequired();

            HasRequired(x => x.BuildingHeightRequest).WithMany(x => x.OHLStructureCoordinateAndHeights)
                .HasForeignKey(x => x.UserServiceRequestMasterId);
        }
    }
}