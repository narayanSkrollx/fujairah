using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class CranePermitRequestFirstStageInfringementTypeConfiguration :
        EntityTypeConfiguration<CranePermitRequestFirstStageInfringementType>
    {
        public CranePermitRequestFirstStageInfringementTypeConfiguration()
        {
            ToTable("CranePermitRequestFirstStageInfringementType");

            HasKey(o => o.CranePermitRequestFirstStageInfringementTypeId);

            Property(o => o.CranePermitRequestFirstStageInfringementTypeId)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.UserServiceRequestMasterId).IsRequired();
            Property(o => o.InfringementTypeId).IsRequired();
            Property(o => o.OtherName).IsOptional().HasMaxLength(255);

            HasRequired(o => o.InfringementType).WithMany(o => o.CranePermitRequestFirstStageInfringementTypes).HasForeignKey(o => o.InfringementTypeId).WillCascadeOnDelete(false);
            HasRequired(o => o.CranePermitRequestFirstStageEntry).WithMany(o => o.CranePermitRequestFirstStageInfringementTypes).HasForeignKey(o => o.UserServiceRequestMasterId).WillCascadeOnDelete(false);

        }
    }
}