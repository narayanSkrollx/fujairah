﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FIA.CMS.Entities;
using FIA.CMS.Model;
using Umbraco.Web.Security.Identity;

namespace FIA.CMS.Controller
{
    public class JobController : BaseController
    {
        // GET: Job
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UserJob(int userId, int jobId)
        {
            var result = new JsonResponseObject();
            try
            {
                var userJobApplyCheckQuery = "select * from UserJob Where UserId=@0 and JobId=@1";
                var duplicateuserJobRecord = Db.Query<UserLogin>(userJobApplyCheckQuery, userId, jobId);

                if (duplicateuserJobRecord.Any())
                {
                    result.IsSuccess = false;
                    result.Message = "You have already applied for this job.";
                }
                else
                {
                    var sql = "insert into UserJob (UserId, JobId,ApplyDate) values (@0,@1,@2)";
                    var affectedRows = Db.Execute(sql, userId, jobId, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    result.IsSuccess = true;
                    result.Message = "Your application for this job has been posted successfully.";
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "Error while applying for job. Please try again later.";
            }
            return Json(result);
        }

        public ActionResult GetUserJob()
        {
            var db = ApplicationContext.DatabaseContext.Database;
            var sql =
                "select uj.Id UserJobId ,u.Id UserId , u.FirstName , uj.IsAccepted,uj.AcceptRejectDate ,u.LastName,u.Email,u.ResumePath,uj.JobId,uj.ApplyDate from UserJob as uj join UserLogin as u on uj.UserId = u.Id";
            var userJobs = db.Query<UserJobModel>(sql);
            var userJobModels = userJobs as UserJobModel[] ?? userJobs.ToArray();
            for (var i = 0; i < userJobModels.Count(); i++)
            {
                var item = userJobModels[i];
                var job = Umbraco.Content(item.JobId);
                item.Url = job.Url;
                item.JobTitle = job.positionTitle;
            }
            return Json(userJobModels, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AcceptRejectUser(int userJobId, int status)
        {
            var db = ApplicationContext.DatabaseContext.Database;
            var users = db.Query<UserJob>("select  * from UserJob Where Id=@0", userJobId);
            var user = users.FirstOrDefault();
            user.IsAccepted = status == 1 ? true : false;
            user.AcceptRejectDate = DateTime.Now;
            db.Save(user);
            return null;
        }

        public ActionResult GetMyJob()
        {
            var db = ApplicationContext.DatabaseContext.Database;
            var sql =
                "select u.Id UserId , u.FirstName , u.LastName,u.Email,u.ResumePath,uj.JobId from UserJob as uj join UserLogin as u on uj.UserId = u.Id where u.Id=" + GetCurrentUserId();
            var userJobs = db.Query<UserJobModel>(sql);
            var userJobModels = userJobs.ToList();
            for (var i = 0; i < userJobModels.Count(); i++)
            {
                var item = userJobModels[i];
                var job = Umbraco.Content(item.JobId);
                item.Url = job.Url;
                item.JobPositingDate = job.postingDate;
                item.JobTitle = job.positionTitle;
                item.JobPostingId = job.jobId;
            }
            return PartialView($"{HomePartialViewLocation}_MyJobs.cshtml", userJobModels);
        }
    }
}