using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class CranePermitRequestConfiguration : EntityTypeConfiguration<CranePermitRequest>
    {
        public CranePermitRequestConfiguration()
        {
            ToTable("CranePermitRequest");

            HasKey(o => o.UserServiceRequestMasterId);

            Property(o => o.UserServiceRequestMasterId).IsRequired();
            Property(o => o.ApplicantName).IsRequired();
            Property(o => o.ApplicationDate).IsRequired();
            Property(o => o.CompanyName).IsRequired().HasMaxLength(1024);
            Property(o => o.Position).IsOptional().HasMaxLength(255);
            Property(o => o.ContactNumber).IsRequired().HasMaxLength(50);
            Property(o => o.ApplicantEmail).IsRequired().HasMaxLength(255);
            Property(o => o.IsNewApplication).IsRequired();
            Property(o => o.PreviousPermitNumbers).IsOptional().HasMaxLength(1024);
            Property(o => o.NorthingsEastings).IsOptional().HasMaxLength(1024);
            Property(o => o.CraneLocation).IsOptional().HasMaxLength(1024);
            Property(o => o.OperationStartDate).IsRequired();
            Property(o => o.OperationEndDate).IsRequired();
            Property(o => o.OperationStartTime).IsRequired().HasMaxLength(255);
            Property(o => o.OperationEndTime).IsRequired().HasMaxLength(255);
            Property(o => o.MaxOperatingHeight).IsRequired();
            Property(o => o.PurposeOfUser).IsOptional().HasMaxLength(255);
            Property(o => o.CraneOperatingCompany).IsRequired().HasMaxLength(255);
            Property(o => o.OperatorName).IsRequired().HasMaxLength(255);
            Property(o => o.OperatorContactNumber).IsRequired().HasMaxLength(255);

            HasRequired(o => o.UserServiceRequestMaster).WithRequiredDependent(o => o.CranePermitRequest).WillCascadeOnDelete(false);

        }
    }

    public class CranePermitPlanningAttachmentConfiguration : EntityTypeConfiguration<CranePermitPlanningAttachment>
    {
        public CranePermitPlanningAttachmentConfiguration()
        {
            ToTable("CranePermitPlanningAttachment");

            HasKey(o => o.CranePermitPlanningAttachmentId);

            Property(o => o.CranePermitPlanningAttachmentId).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.UserServiceRequestMasterId).IsRequired();
            Property(o => o.UserAttachmentId).IsRequired();

            HasRequired(o => o.PlanningUserEntry).WithMany(o => o.CranePermitPlanningAttachments).HasForeignKey(o => o.UserServiceRequestMasterId).WillCascadeOnDelete(false);
            HasRequired(o => o.Attachment).WithMany().HasForeignKey(o => o.UserAttachmentId).WillCascadeOnDelete(false);
        }
    }

    public class CranePermitSafetyFileAttachmentConfiguration : EntityTypeConfiguration<CranePermitSafetyFileAttachment>
    {
        public CranePermitSafetyFileAttachmentConfiguration()
        {
            ToTable("CranePermitSafetyFileAttachment");

            HasKey(o => o.CranePermitSafetyFileAttachmentId);
            Property(o => o.CranePermitSafetyFileAttachmentId).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.UserAttachmentId).IsRequired();
            Property(o => o.UserServiceRequestMasterId).IsRequired();

            HasRequired(o => o.Attachment).WithMany().HasForeignKey(o => o.UserAttachmentId).WillCascadeOnDelete(false);

            HasRequired(o => o.SafetyUserApproval).WithMany(o=>o.Attachments).HasForeignKey(o => o.UserServiceRequestMasterId).WillCascadeOnDelete(false);
        }
    }
}