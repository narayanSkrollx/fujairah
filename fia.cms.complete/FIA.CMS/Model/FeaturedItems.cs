﻿namespace FIA.CMS.Model
{
    public class DefaultContentArcheType
    {
        public string Title { get; set; }
        public string MainContent { get; set; }
        public string ImageUrl { get; set; }
        public bool? RevertPlacement { get; set; }
    }
}