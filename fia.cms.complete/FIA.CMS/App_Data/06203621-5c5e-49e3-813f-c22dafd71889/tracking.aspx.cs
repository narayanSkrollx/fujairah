﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Designit.Umb.Newsletter.Custom.plugins.designit.newsletter
{
    public partial class tracking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string url = Request["redirect"];

            int campaignId = Int32.Parse(Request["campaign"]);

            int numbersClicked =
                    umbraco.BusinessLogic.Application.SqlHelper.ExecuteScalar<int>("select clicks from newsletterTracking where campaignId=@campaignId and url=@url",
                    umbraco.BusinessLogic.Application.SqlHelper.CreateParameter("@campaignId", campaignId),
                    umbraco.BusinessLogic.Application.SqlHelper.CreateParameter("@url", url));

            numbersClicked++;

            umbraco.BusinessLogic.Application.SqlHelper.ExecuteNonQuery("update newsletterTracking set clicks=@clicks where campaignId=@campaignId and url=@url",
                umbraco.BusinessLogic.Application.SqlHelper.CreateParameter("@clicks", numbersClicked),
                umbraco.BusinessLogic.Application.SqlHelper.CreateParameter("@campaignId", campaignId),
                umbraco.BusinessLogic.Application.SqlHelper.CreateParameter("@url", url));

            Response.Redirect(url);
        }
    }
}
