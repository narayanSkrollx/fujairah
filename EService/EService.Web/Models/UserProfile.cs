using EService.Data.Entity;

namespace EService.Web.Models
{
    public class UserProfile
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int UserTypeId { get; set; }
        public string UserTypeName { get; set; }
        public int? DepartmentId { get; set; }
        public UserTypeEnum UserType => (UserTypeEnum)UserTypeId;
    }
}