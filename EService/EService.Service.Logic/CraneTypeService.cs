using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class CraneTypeService : BusinessAccessBase<CraneType>
    {
        public CraneTypeService(ServiceContext context) : base(context)
        {
        }
    }
}