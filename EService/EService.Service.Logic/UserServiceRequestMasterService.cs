﻿using EService.Data.Entity.Entity;
using EService.Service.Logic.Enum;
using EService.Service.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.SqlServer;
using System.Linq;
using EService.Core;
using EService.Service.Model.Report;
using Nelibur.ObjectMapper;

namespace EService.Service.Logic
{
    public class UserServiceRequestMasterService : BusinessAccessBase<UserServiceRequestMaster>
    {
        private ServiceResolver serviceResolver;

        public UserServiceRequestMasterService(ServiceContext context) : base(context)
        {
            serviceResolver = new ServiceResolver(context);
        }

        public Tuple<bool, string, ServiceRequestFlowStageDepartment> Approve(int userServiceRequestMasterId, int departmentId, int userId, bool isOverriddenFromAdmin = false)
        {
            var approvedStats = (int)ServiceFlowStateEnum.Approved;

            var acceptableStatus = new int[] { approvedStats };
            var userRequestService = Find(o => o.UserServiceRequestMasterId == userServiceRequestMasterId && !o.IsDeleted && !acceptableStatus.Contains(o.Status));
            if (userRequestService == null)
            {
                return new Tuple<bool, string, ServiceRequestFlowStageDepartment>(false, "Invalid Service Request info provided to approve.", null);
            }
            var serviceCurrentStage = userRequestService.ServiceRequestFlowStages.FirstOrDefault(
                o => o.IsCurrentStage);
            if (serviceCurrentStage == null)
            {
                return new Tuple<bool, string, ServiceRequestFlowStageDepartment>(false, "The request does not have currently active status. It is either in invalid stage or is already approved / rejected.", null);
            }

            var serviceRequestFlowStageDepartment =
                serviceCurrentStage.ServiceRequestFlowStageDepartments.FirstOrDefault(o => (isOverriddenFromAdmin || o.DepartmentId == departmentId) && (o.IsApproved != true || o.IsAssignedAfterReject));
            if (serviceRequestFlowStageDepartment == null)
            {
                return new Tuple<bool, string, ServiceRequestFlowStageDepartment>(false, "Either Current Department does not have any role in approving this service or is already approved.", null);
            }

            var service = serviceResolver.ResolveServiceFromUserServiceRequestMasterId(userServiceRequestMasterId);
            if (isOverriddenFromAdmin && service.HasExtraFormForApprovalReject())
            {
                return new Tuple<bool, string, ServiceRequestFlowStageDepartment>(false, "There are forms that specific user must enter values, hence admin cannot accept the request.", null);
            }
            var validReason = service.IsValidForApproveReject(userServiceRequestMasterId,
                serviceRequestFlowStageDepartment.ServiceRequestFlowStageDepartmentId, departmentId);
            if (!validReason.IsSuccess)
            {
                return new Tuple<bool, string, ServiceRequestFlowStageDepartment>(false, validReason.Message, null);
            }
            //valid
            SaveHistory(serviceRequestFlowStageDepartment, userId);
            serviceRequestFlowStageDepartment.DepartmentUserId = userId;

            serviceRequestFlowStageDepartment.IsApproved = true;
            serviceRequestFlowStageDepartment.IsAssignedAfterReject = false;
            serviceRequestFlowStageDepartment.Status = (int)ServiceFlowStateEnum.Approved;
            serviceRequestFlowStageDepartment.StatusChangedDateTime = DateTime.Now.DubaiTime();
            serviceRequestFlowStageDepartment.IsOverriddenFromAdmin = isOverriddenFromAdmin;

            if (serviceCurrentStage.ServiceRequestFlowStageDepartments.Where(o => o.DepartmentId != departmentId)
                    .All(o => o.IsApproved == true))
            {
                //proceed to next stage
                var upperStage = serviceCurrentStage.UserServiceRequestMaster.ServiceRequestFlowStages.FirstOrDefault(
                    o => o.Index == serviceCurrentStage.Index + 1);
                if (upperStage == null)
                {
                    //set request to complete
                    userRequestService.Status = (int)ServiceFlowStateEnum.PaymentPending;
                    serviceCurrentStage.IsCurrentStage = false;
                    //alert user
                }
                else
                {
                    //
                    //
                    upperStage.IsCurrentStage = true;
                    serviceCurrentStage.IsCurrentStage = false;
                    userRequestService.Status = (int)ServiceFlowStateEnum.Active;
                }
            }
            else
            {
                userRequestService.Status = (int)ServiceFlowStateEnum.Active;
            }

            Context.Commit();
            return new Tuple<bool, string, ServiceRequestFlowStageDepartment>(true, "Request approved successfully.", serviceRequestFlowStageDepartment);
        }

        //public Tuple<bool, string, UserServiceRequestBill> ApprovePayment(int userServiceRequestMasterId, int userId)
        //{
        //    var approvedStats = (int)ServiceFlowStateEnum.PaymentPending;

        //    var acceptableStatus = new int[] { approvedStats };
        //    var userRequestService = Find(o => o.UserServiceRequestMasterId == userServiceRequestMasterId && !o.IsDeleted);
        //    if (userRequestService == null)
        //    {
        //        return new Tuple<bool, string, UserServiceRequestBill>(false, "Invalid Service Request info provided to approve payment.", null);
        //    }
        //    else
        //    {
        //        double chargedAmount = 0;
        //        var receipts = new List<UserServiceRequestReceiptDetail>();
        //        if (acceptableStatus.Contains(userRequestService.Status))
        //        {
        //            userRequestService.Status = (int)ServiceFlowStateEnum.PaymentCompleted;
        //            userRequestService.ServieBill.DiscountAmount = 0;
        //            userRequestService.ServieBill.PaidAmount = userRequestService.ServieBill.ChargedAmount;
        //            userRequestService.ServieBill.PaidByUserId = userId;
        //            userRequestService.ServieBill.PaidByUserName = Context.Database.Users.FirstOrDefault(o => o.UserId == userId).UserName;
        //            userRequestService.ServieBill.PaidDateTime = DateTime.Now.DubaiTime();
        //            userRequestService.ServieBill.PaymentMode = (int)PaymentMode.Manual;
        //            chargedAmount = userRequestService.ServieBill.ChargedAmount;
        //        }
        //        else if (userRequestService.IsDraft)
        //        {
        //            switch (userRequestService.ServiceId)
        //            {
        //                case (int)ServiceEnum.BuildingHeightRequest:
        //                    chargedAmount = userRequestService.BuildingHeightRequest.BuildingHeightApplicableFees.Where(o => o.IsSelected).Sum(o => o.Quantity * (double)(o.ApplicableFeesLookUp.Fee ?? 0));
        //                    break;
        //                default:
        //                    break;

        //            }
        //            userRequestService.Status = (int)ServiceFlowStateEnum.New;
        //            userRequestService.IsDraft = false;
        //            userRequestService.RequestDateTime = DateTime.Now.DubaiTime();
        //        }

        //        var newNo = (new UserServiceRequestReceiptService(Context)).GetNewReceiptNo();
        //        var payViewModel = new PaymentResponseServiceViewModel
        //        {
        //            ChargedAmount = chargedAmount
        //        };
        //        userRequestService.UserServiceRequestReceipts.Add(new UserServiceRequestReceipt
        //        {
        //            IsPaid = true,
        //            Year = newNo.Year,
        //            Index = newNo.Index,
        //            ReceiptNo = newNo.ReceiptNo,
        //            AdminFee = payViewModel.AdminFee,
        //            PaymentStage = 2,
        //            ServiceDevelopmentCharge = payViewModel.SocialDevelopmentFee,
        //        });

        //    }

        //    Context.Commit();
        //    return new Tuple<bool, string, UserServiceRequestBill>(true, "Request approved successfully.", userRequestService.ServieBill);
        //}


        public Tuple<bool, string, UserServiceRequestBill> ApprovePayment(int userServiceRequestMasterId, int userId)
        {
            var approvedStats = (int)ServiceFlowStateEnum.PaymentPending;


            var acceptableStatus = new int[] { approvedStats };
            var userRequestService = Find(o => o.UserServiceRequestMasterId == userServiceRequestMasterId && !o.IsDeleted);
            if (userRequestService != null)
            {
                var fromPaymentPending = acceptableStatus.Contains(userRequestService.Status);
                double chargedAmount = 0;
                var receipts = new List<UserServiceRequestReceiptDetail>();
                if (fromPaymentPending && !userRequestService.IsDraft && userRequestService.ServieBill != null)
                {
                    userRequestService.ServieBill.DiscountAmount = 0;
                    userRequestService.ServieBill.PaidAmount = userRequestService.ServieBill.ChargedAmount;
                    userRequestService.ServieBill.PaidByUserId = userId;
                    userRequestService.ServieBill.PaidByUserName = Context.Database.Users.FirstOrDefault(o => o.UserId == userId).UserName;
                    userRequestService.ServieBill.PaidDateTime = DateTime.Now.DubaiTime();
                    userRequestService.ServieBill.PaymentMode = (int)PaymentMode.Manual;
                }
                else if (userRequestService.IsDraft)
                {
                    switch (userRequestService.ServiceId)
                    {
                        case (int)ServiceEnum.BuildingHeightRequest:
                            chargedAmount = userRequestService.BuildingHeightRequest.BuildingHeightApplicableFees.Where(o => o.IsSelected).Sum(o => o.Quantity * (double)(o.ApplicableFeesLookUp.Fee ?? 0));
                            receipts = userRequestService.BuildingHeightRequest.BuildingHeightApplicableFees.Select(o => new UserServiceRequestReceiptDetail
                            {
                                Description = o.ApplicableFeesLookUp.Description,
                                Quantity = (int)o.Quantity,
                                Rate = (double)(o.ApplicableFeesLookUp.Fee ?? 0),
                            }).ToList();
                            break;
                        default:
                            receipts = new List<UserServiceRequestReceiptDetail> { new UserServiceRequestReceiptDetail { Description = userRequestService.Service.ServiceName
                                , Quantity = 1, Rate = userRequestService.ServieBill?.ChargedAmount ??0 } };
                            break;

                    }
                    userRequestService.Status = (int)ServiceFlowStateEnum.New;
                    userRequestService.IsDraft = false;
                    userRequestService.RequestDateTime = DateTime.Now.DubaiTime();
                }

                var newNo = (new UserServiceRequestReceiptService(Context)).GetNewReceiptNo();
                var payViewModel = new PaymentResponseServiceViewModel
                {
                    ChargedAmount = chargedAmount
                };
                userRequestService.UserServiceRequestReceipts.Add(new UserServiceRequestReceipt
                {
                    IsPaid = true,
                    Year = newNo.Year,
                    Index = newNo.Index,
                    ReceiptNo = newNo.ReceiptNo,
                    AdminFee = payViewModel.AdminFee,
                    PaymentStage = 2,
                    ServiceDevelopmentCharge = payViewModel.SocialDevelopmentFee,
                    ReceiptDetails = receipts
                });

                Context.Commit();

                return new Tuple<bool, string, UserServiceRequestBill>(true, "Request approved successfully.", userRequestService.ServieBill);
            }
            else
            {
                return new Tuple<bool, string, UserServiceRequestBill>(false, "Invalid Service information provided to approve.", userRequestService.ServieBill);
            }
        }


        public IEnumerable<ServiceFlowStageDepartmentHistoryViewModel> GetRequestFlowStageDepartmentHistories(
            int userServiceRequestMasterId)
        {
            return (from serviceRequestFlow in Context.Database.ServiceRequestFlowStages

                    join serviceRequestFlowStageDepartment in Context.Database.ServiceRequestFlowStageDepartments
                        on serviceRequestFlow.ServiceRequestFlowStageId equals
                        serviceRequestFlowStageDepartment.ServiceRequestFlowStageId
                    join serviceRequestFlowStageDepartmentHistory in
                        Context.Database.ServiceRequestFlowStageDepartmentHistories
                        on serviceRequestFlowStageDepartment.ServiceRequestFlowStageDepartmentId
                        equals serviceRequestFlowStageDepartmentHistory.ServiceRequestFlowStageDepartmentId
                    join flowStage in Context.Database.ServiceFlowStatuses on serviceRequestFlowStageDepartment.Status equals flowStage.Id
                    where serviceRequestFlow.UserServiceRequestMasterId == userServiceRequestMasterId
                    select new
                    {
                        history = serviceRequestFlowStageDepartmentHistory,
                        stage = serviceRequestFlow,
                        departmentStage = serviceRequestFlowStageDepartment,
                        flowStage
                    }
                ).AsNoTracking().AsEnumerable().Select(o => new ServiceFlowStageDepartmentHistoryViewModel()
                {
                    DepartmentId = o.history.DepartmentId,
                    Status = o.history.Status,
                    DepartmentName = o.history.DepartmentName,
                    DepartmentUserId = o.history.DepartmentUserId,
                    IsApproved = o.history.IsApproved,
                    ServiceRequestFlowStageId = o.history.ServiceRequestFlowStageId,
                    ServiceRequestFlowStageDepartmentId = o.history.ServiceRequestFlowStageDepartmentId,
                    DepartmentUserName = o.history.DepartmentUserName,
                    ServiceRequestFlowStageDepartmentHistoryId = o.history.ServiceRequestFlowStageDepartmentHistoryId,
                    StatusChangedDateTime = o.history.StatusChangedDateTime,
                    Remarks = o.history.Remarks,
                    StageIndex = o.stage.Index,
                    StatusName = o.flowStage.Description
                });
        }

        public UserServiceRequestMaster GetServiceRequest(int userServiceRequestId)
        {
            return Find(o => !o.IsDeleted && o.UserServiceRequestMasterId == userServiceRequestId);
        }

        public IEnumerable<UserServiceRequestFlowStageModel> GetServiceRequestDetails(int userId, int? serviceid = null)
        {
            //FindAllAsNoTracking()
            //var intStatus = (status ?? new List<ServiceFlowStateEnum>().ToArray()).Select(o => (int)o).ToList();
            var a = (from serviceRequestStage in Context.Database.ServiceRequestFlowStages
                     join serviceRequestStageDepartment in Context.Database.ServiceRequestFlowStageDepartments
                     on serviceRequestStage.ServiceRequestFlowStageId equals serviceRequestStageDepartment.ServiceRequestFlowStageId
                     join userRequest in Context.Database.UserServiceRequests
                     on serviceRequestStage.UserServiceRequestMasterId equals userRequest.UserServiceRequestMasterId
                     join user in Context.Database.Users
                     on userRequest.UserId equals user.UserId
                     join service in Context.Database.Services
                     on userRequest.ServiceId equals service.ServiceId

                     join serviceFlowState in Context.Database.ServiceFlowStatuses
                     on userRequest.UserServiceRequestMasterId equals serviceFlowState.Id
                     join departmentServiceFlowState in Context.Database.ServiceFlowStatuses
                     on serviceRequestStageDepartment.Status equals departmentServiceFlowState.Id
                     where
                     //serviceRequestStage.IsCurrentStage
                     //&&
                     userRequest.UserId == userId
                     //&& serviceRequestStageDepartment.DepartmentId == departmentId
                     //&& (intStatus.Count == 0 || intStatus.Contains(serviceRequestStageDepartment.Status))
                     select new UserServiceRequestFlowStageModel
                     {
                         Index = serviceRequestStage.Index,
                         ServiceId = userRequest.ServiceId,
                         ServiceName = service.ServiceName,
                         UserId = userRequest.UserId,
                         DepartmentId = serviceRequestStageDepartment.DepartmentId,
                         UserName = user.UserName,
                         Email = user.Email,
                         DepartmentStatus = serviceRequestStageDepartment.Status,
                         UserServiceRequestMasterId = userRequest.UserServiceRequestMasterId,
                         DepartmentUserId = serviceRequestStageDepartment.DepartmentUserId,
                         RequestDateTime = userRequest.RequestDateTime,
                         IsApproved = serviceRequestStageDepartment.IsApproved,
                         IsCurrentStage = serviceRequestStage.IsCurrentStage,
                         RequestStatus = userRequest.Status,
                         ServiceRequestFlowStageDepartmentId = serviceRequestStageDepartment.DepartmentId,
                         ServiceRequestFlowStageId = serviceRequestStage.ServiceRequestFlowStageId,
                         StatusChangedDateTime = serviceRequestStageDepartment.StatusChangedDateTime,
                         ServiceRequestStatusName = serviceFlowState.Description,
                         DepartmentServiceRequestStatusName = departmentServiceFlowState.Description
                     }
                    )
                //.Take(top)
                ;
            return a;
        }

        public IEnumerable<UserServiceRequestFlowStageModel> GetServiceRequests(int departmentId, ServiceFlowStateEnum[] status, int top = 20, int skip = 0)
        {
            //FindAllAsNoTracking()
            var intStatus = (status ?? new List<ServiceFlowStateEnum>().ToArray()).Select(o => (int)o).ToList();
            var a = (from serviceRequestStage in Context.Database.ServiceRequestFlowStages
                     join serviceRequestStageDepartment in Context.Database.ServiceRequestFlowStageDepartments
                     on serviceRequestStage.ServiceRequestFlowStageId equals serviceRequestStageDepartment.ServiceRequestFlowStageId
                     join userRequest in Context.Database.UserServiceRequests
                     on serviceRequestStage.UserServiceRequestMasterId equals userRequest.UserServiceRequestMasterId
                     join user in Context.Database.Users
                     on userRequest.UserId equals user.UserId
                     join service in Context.Database.Services
                     on userRequest.ServiceId equals service.ServiceId

                     join serviceFlowState in Context.Database.ServiceFlowStatuses
                     on userRequest.Status equals serviceFlowState.Id
                     join departmentServiceFlowState in Context.Database.ServiceFlowStatuses
                     on serviceRequestStageDepartment.Status equals departmentServiceFlowState.Id
                     where serviceRequestStage.IsCurrentStage && serviceRequestStageDepartment.DepartmentId == departmentId
                     && (intStatus.Count == 0 || intStatus.Contains(userRequest.Status))

                     && !userRequest.IsDraft

                     orderby userRequest.UserServiceRequestMasterId descending
                     select new UserServiceRequestFlowStageModel
                     {
                         Index = serviceRequestStage.Index,
                         ServiceId = userRequest.ServiceId,
                         ServiceName = service.ServiceName,
                         RegistrationNo = userRequest.RegistrationNo,
                         UserId = userRequest.UserId,
                         DepartmentId = serviceRequestStageDepartment.DepartmentId,
                         UserName = user.UserName,
                         Email = user.Email,
                         DepartmentStatus = serviceRequestStageDepartment.Status,
                         UserServiceRequestMasterId = userRequest.UserServiceRequestMasterId,
                         DepartmentUserId = serviceRequestStageDepartment.DepartmentUserId,
                         RequestDateTime = userRequest.RequestDateTime,
                         IsApproved = serviceRequestStageDepartment.IsApproved,
                         IsCurrentStage = serviceRequestStage.IsCurrentStage,
                         RequestStatus = userRequest.Status,
                         ServiceRequestFlowStageDepartmentId = serviceRequestStageDepartment.DepartmentId,
                         ServiceRequestFlowStageId = serviceRequestStage.ServiceRequestFlowStageId,
                         StatusChangedDateTime = serviceRequestStageDepartment.StatusChangedDateTime,
                         ServiceRequestStatusName = serviceFlowState.Description,
                         DepartmentServiceRequestStatusName = departmentServiceFlowState.Description
                     }
                    ).Skip(skip).Take(top)
                ;
            return a;
        }

        public IEnumerable<UserServiceRequestModel> GetServiceRequests(int? userId = null, int? serviceid = null, DateTime? dateFrom = null, DateTime? dateTo = null,
            ServiceFlowStateEnum? serviceStatusEnum = null, string emailOrUserNameOrRegistrationNo = "", bool excludeDraft = false, int skip = 0, int pageSize = 50, PaymentMode? paymentMode = null)
        {
            var serviceStatus = serviceStatusEnum == null ? null : (int?)serviceStatusEnum;
            var isEmailEmpty = string.IsNullOrEmpty(emailOrUserNameOrRegistrationNo);
            var paymentModeInt = paymentMode == null ? 0 : (int)paymentMode;
            //FindAllAsNoTracking()
            //var intStatus = (status ?? new List<ServiceFlowStateEnum>().ToArray()).Select(o => (int)o).ToList();
            var a = (from userRequest in Context.Database.UserServiceRequests
                     join user in Context.Database.Users
                     on userRequest.UserId equals user.UserId
                     join service in Context.Database.Services
                     on userRequest.ServiceId equals service.ServiceId
                     join stat in Context.Database.ServiceFlowStatuses
                     on userRequest.Status equals stat.Id
                     where (userId == null || userRequest.UserId == userId)
                     && (serviceid == null || service.ServiceId == serviceid)
                     && (serviceStatus == null || userRequest.Status == serviceStatus)
                     && (dateFrom == null || userRequest.RequestDateTime >= dateFrom)
                     && (dateTo == null || userRequest.RequestDateTime <= dateTo)
                     && (isEmailEmpty || user.UserName.Contains(emailOrUserNameOrRegistrationNo) || user.Email.Contains(emailOrUserNameOrRegistrationNo) || userRequest.RegistrationNo.Contains(emailOrUserNameOrRegistrationNo))
                     && (!excludeDraft || !userRequest.IsDraft)
                     && (paymentModeInt == 0 || userRequest.PaymentMode == paymentModeInt)
                     orderby userRequest.UserServiceRequestMasterId descending
                     select new UserServiceRequestModel
                     {
                         ServiceId = userRequest.ServiceId,
                         ServiceName = service.ServiceName,
                         UserId = userRequest.UserId,
                         UserName = user.UserName,
                         Email = user.Email,
                         RequestDateTime = userRequest.RequestDateTime,
                         RequestStatus = userRequest.Status,
                         RequestStatusName = stat.Description,
                         UserServiceRequestMasterId = userRequest.UserServiceRequestMasterId,
                         RegistrationNo = userRequest.RegistrationNo,
                         IsReapplied = userRequest.ParentUserServiceRequestMasterId != null,
                         IsDraft = userRequest.IsDraft
                     })
                     .Skip(skip)
                     .Take(pageSize)
                ;
            return a;
        }

        public Tuple<bool, string, ServiceRequestFlowStageDepartment> Reject(int userServiceRequestMasterId, int departmentId, int userId, string comments, bool isSelectedForUser, int? selectedStageId)
        {
            var approvedStats = (int)ServiceFlowStateEnum.Approved;
            var rejectedStatus = (int)ServiceFlowStateEnum.Rejected;

            var acceptableStatus = new int[] { approvedStats };
            var userRequestService = Find(o => o.UserServiceRequestMasterId == userServiceRequestMasterId && !o.IsDeleted && !acceptableStatus.Contains(o.Status));
            if (userRequestService == null)
            {
                return new Tuple<bool, string, ServiceRequestFlowStageDepartment>(false, "Invalid Service Request info provided to reject.", null);
            }
            var serviceCurrentStage = userRequestService.ServiceRequestFlowStages.FirstOrDefault(
                o => o.IsCurrentStage);
            if (serviceCurrentStage == null)
            {
                return new Tuple<bool, string, ServiceRequestFlowStageDepartment>(false, "The request does not have currently active status. It is either in invalid stage or is already approved / rejected.", null);
            }
            var serviceRequestFlowStageDepartment =
                serviceCurrentStage.ServiceRequestFlowStageDepartments.FirstOrDefault(o => o.DepartmentId == departmentId);
            if (serviceRequestFlowStageDepartment == null)
            {
                return new Tuple<bool, string, ServiceRequestFlowStageDepartment>(false, "Current Department does not have any role in approving this service or is already approved / rejected.", null);
            }
            //valid

            var service = serviceResolver.ResolveServiceFromUserServiceRequestMasterId(userServiceRequestMasterId);
            var validReason = service.IsValidForApproveReject(userServiceRequestMasterId,
                serviceRequestFlowStageDepartment.ServiceRequestFlowStageDepartmentId, departmentId);
            if (!validReason.IsSuccess)
            {
                return new Tuple<bool, string, ServiceRequestFlowStageDepartment>(false, validReason.Message, null);
            }

            serviceRequestFlowStageDepartment.Remarks = comments;
            SaveHistory(serviceRequestFlowStageDepartment, userId);

            serviceRequestFlowStageDepartment.DepartmentUserId = userId;
            serviceRequestFlowStageDepartment.IsApproved = false;
            serviceRequestFlowStageDepartment.Status = (int)ServiceFlowStateEnum.Rejected;
            serviceRequestFlowStageDepartment.StatusChangedDateTime = DateTime.Now.DubaiTime();

            if (isSelectedForUser)
            {
                //change all is current to false;
                var allStages = userRequestService.ServiceRequestFlowStages.ToList();
                foreach (var stage in allStages)
                {
                    stage.IsCurrentStage = false;
                }
                //set request to reject
                userRequestService.Status = (int)ServiceFlowStateEnum.Rejected;
            }
            else
            {
                //proceed to prev stage
                var prevStage = serviceCurrentStage.UserServiceRequestMaster.ServiceRequestFlowStages.FirstOrDefault(
                    o => o.Index < serviceCurrentStage.Index && o.ServiceRequestFlowStageId == selectedStageId);
                if (prevStage == null)
                {
                    //set request to reject
                    userRequestService.Status = (int)ServiceFlowStateEnum.Rejected;
                    //alert user
                }
                else
                {
                    //
                    //
                    foreach (var item in prevStage.ServiceRequestFlowStageDepartments)
                    {
                        item.IsAssignedAfterReject = true;
                    }
                    prevStage.IsCurrentStage = true;
                    serviceCurrentStage.IsCurrentStage = false;
                    userRequestService.Status = (int)ServiceFlowStateEnum.Active;
                }
            }

            Context.Commit();
            return new Tuple<bool, string, ServiceRequestFlowStageDepartment>(true, "Request rejected successfully.", serviceRequestFlowStageDepartment);
        }

        private void SaveHistory(ServiceRequestFlowStageDepartment departmentServiceStage, int userId)
        {
            var departmentUser =
                Context.Database.Users.FirstOrDefault(o => o.UserId == userId);
            var a = new ServiceRequestFlowStageDepartmentHistory()
            {
                DepartmentId = departmentServiceStage.DepartmentId,
                Status = departmentServiceStage.Status,
                DepartmentName = departmentServiceStage.Department.DepartmentName,
                DepartmentUserId = userId,
                DepartmentUserName = departmentUser != null ? departmentUser.UserName : "",
                IsApproved = departmentServiceStage.IsApproved,
                Remarks = departmentServiceStage.Remarks,
                ServiceRequestFlowStageDepartmentId = departmentServiceStage.ServiceRequestFlowStageDepartmentId,
                ServiceRequestFlowStageId = departmentServiceStage.ServiceRequestFlowStageId,
                StatusChangedDateTime = DateTime.Now.DubaiTime(),
            };

            Context.Database.ServiceRequestFlowStageDepartmentHistories.Add(a);
        }

        #region "Service Specific Functions"

        #region "Crane Permit Related Functions"

        public CranePermitSafetyQAApproval GetCranePermitRequesSafetyQA(int userServiceRequestMasterId)
        {
            var userServiceRequest =
                Context.Database.UserServiceRequests.FirstOrDefault(
                    o => o.UserServiceRequestMasterId == userServiceRequestMasterId && o.CranePermitRequest != null && o.ServiceId == (int)ServiceEnum.CranePermitRequest);

            if (userServiceRequest == null)
            {
                throw new DataException("Invalid Service Request Master.");
            }
            return userServiceRequest.CranePermitRequest.CranePermitSafetyQAApproval ??
                   new CranePermitSafetyQAApproval()
                   {
                       UserServiceRequestMasterId = userServiceRequestMasterId
                   };
        }

        public CranePermitRequestServiceModel GetCranePermitRequest(int userServiceRequestMasterId, bool skipCurrentStage = false)
        {
            var userServiceRequest =
                Context.Database.UserServiceRequests.FirstOrDefault(
                    o => o.UserServiceRequestMasterId == userServiceRequestMasterId && o.CranePermitRequest != null && o.ServiceId == (int)ServiceEnum.CranePermitRequest);

            if (userServiceRequest == null)
            {
                throw new DataException("Invalid Service Request Master.");
            }
            var currentStage = userServiceRequest.ServiceRequestFlowStages.FirstOrDefault(o => o.IsCurrentStage);
            if (!skipCurrentStage)
            {
                if (currentStage == null)
                {
                    throw new DataException("There is no currently active stage.");
                }
            }

            var craneTypes = (from craneType in Context.Database.CraneTypes.Where(o => !o.IsDeleted)
                              join craneTypeRecord in Context.Database.CranePermitRequestSelectedCranes.Where(o => o.CranePermitRequest.UserServiceRequestMasterId == userServiceRequestMasterId)
                              on craneType.CraneTypeId equals craneTypeRecord.CraneTypeId
                              into craneTypeGroup
                              from craneRecord in craneTypeGroup.DefaultIfEmpty()
                              select new
                              {
                                  CraneTypeId = craneType.CraneTypeId,
                                  IsOther = craneType.IsOther,
                                  CraneTypeName = craneType.CraneTypeName,
                                  IsSelected = craneRecord != null,
                                  OtherCraneTypeName = craneRecord != null ? craneRecord.OtherCraneType : ""
                              })
                                  .AsEnumerable().Select(o => new CraneTypeSelectedModel
                                  {
                                      CraneTypeId = o.CraneTypeId,
                                      IsOther = o.IsOther,
                                      CraneTypeName = o.CraneTypeName,
                                      IsSelected = o.IsSelected,
                                      OtherCraneTypeName = o.OtherCraneTypeName
                                  })
                                  .ToList();

            #region "Planning"

            var planningInfo = userServiceRequest.CranePermitRequest.CranePermitRequestFirstStageEntry;
            var planningInfoNewModel = planningInfo ?? new CranePermitRequestFirstStageEntry() { UserServiceRequestMasterId = userServiceRequestMasterId };
            var savedInfringements = planningInfoNewModel.CranePermitRequestFirstStageInfringementTypes ??
                                     new List<CranePermitRequestFirstStageInfringementType>();
            var infringementTypes = (from infringementType in Context.Database.InfringementTypes.ToList()
                                     let savedInfringement = savedInfringements.FirstOrDefault(o => o.InfringementTypeId == infringementType.Id)
                                     select new InfringementTypeModel
                                     {
                                         Id = infringementType.Id,
                                         Description = infringementType.Description,
                                         IsOther = infringementType.IsOther,
                                         Active = savedInfringement != null,
                                         OtherName = savedInfringement != null ? savedInfringement.OtherName : ""
                                     }).ToList();

            var planningModel = planningInfo != null ? new CraneRequestPlanningModel()
            {
                SavedByUserId = planningInfo.SavedByUserId,
                AssessedDate = planningInfo.AssessedDate,
                AirportPlanner = planningInfo.AirportPlanner,
                RequireAviationLight = planningInfo.RequireAviationLight,
                HasPentrationOfSurface = planningInfo.HasPentrationOfSurface,
                CraneOperatingDistanceFromRunway = planningInfo.CraneOperatingDistanceFromRunway,
                AssessedBy = planningInfo.AssessedBy,
                Recommendations = planningInfo.Recommendations,
                MaxCraneOperatingHeight = planningInfo.MaxCraneOperatingHeight,
                PenetrationInMeter = planningInfo.PenetrationInMeter,
                SavedDateTime = planningInfo.SavedDateTime,
                Comments = planningInfo.Comments,
                InfringementTypes = infringementTypes,
                UploadedFiles = planningInfo.CranePermitPlanningAttachments.Select(o => new UploadedFile { Id = o.UserAttachmentId, ContentType = o.Attachment.ContentType, FileName = o.Attachment.FileName }).ToList()
            } : null;
            var planningNewModal = new CraneRequestPlanningModel()
            {
                InfringementTypes = infringementTypes,
            };

            #endregion "Planning"

            #region "QA Safety"

            var qaSafetyInfo = userServiceRequest.CranePermitRequest.CranePermitSafetyQAApproval;
            var qaSafetyDefaultInfo = qaSafetyInfo ?? new CranePermitSafetyQAApproval();
            var qaSafetyModal = new CraneRequestQASafetyModel()
            {
                AdditionalComments = qaSafetyDefaultInfo.AdditionalComments,
                OperatingRestrictions = qaSafetyDefaultInfo.OperatingRestrictions,
                ApprovedByPosition = qaSafetyDefaultInfo.ApprovedByPosition,
                HasSafetyAssessmentBeenDone = qaSafetyDefaultInfo.HasSafetyAssessmentBeenDone,
                ApprovedDateTime = qaSafetyDefaultInfo.ApprovedDateTime != DateTime.MinValue ? (DateTime?)qaSafetyDefaultInfo.ApprovedDateTime : null,
                ApprovedBy = qaSafetyDefaultInfo.ApprovedBy,
                UploadedFiles = (qaSafetyDefaultInfo.Attachments ?? new List<CranePermitSafetyFileAttachment>()).Select(o => new UploadedFile { Id = o.UserAttachmentId, ContentType = o.Attachment.ContentType, FileName = o.Attachment.FileName }).ToList()
            };
            var qaSafetyNewModel = new CraneRequestQASafetyModel()
            {
                AdditionalComments = @"1.	Crane to be lowered if operationally required by FIA and when not in use.
2.Airside Safety to monitor and coordinate with ATC.
3.Crane to be lowered during adverse weather(if inside aerodrome).
",
                OperatingRestrictions = @"1.	Distribution: MATS, ATS-Head of Operations, ATS-Head of Safety, Airport Planning and SQD.
2.	 This document also serves as the Crane Permit after processing and approval from SQD.
",
            };

            #endregion "QA Safety"

            return new CranePermitRequestServiceModel()
            {
                ServiceId = userServiceRequest.ServiceId,
                UserName = userServiceRequest.User.UserName,
                UserId = userServiceRequest.UserId,
                RegistrationNo = userServiceRequest.RegistrationNo,
                UserServiceRequestMasterId = userServiceRequest.UserServiceRequestMasterId,
                ServiceName = userServiceRequest.Service.ServiceName,
                ApplicantEmail = userServiceRequest.User.Email,
                ApplicantName = userServiceRequest.CranePermitRequest.ApplicantName,
                CraneLocation = userServiceRequest.CranePermitRequest.CraneLocation,
                OperationStartTime = userServiceRequest.CranePermitRequest.OperationStartTime,
                OperationEndTime = userServiceRequest.CranePermitRequest.OperationEndTime,
                CompanyName = userServiceRequest.CranePermitRequest.CompanyName,
                IsNewApplication = userServiceRequest.CranePermitRequest.IsNewApplication,
                ApplicationDate = userServiceRequest.CranePermitRequest.ApplicationDate,
                Position = userServiceRequest.CranePermitRequest.Position,
                PreviousPermitNumbers = userServiceRequest.CranePermitRequest.PreviousPermitNumbers,
                NorthingsEastings = userServiceRequest.CranePermitRequest.NorthingsEastings,
                CraneOperatingCompany = userServiceRequest.CranePermitRequest.CraneOperatingCompany,
                OperatorContactNumber = userServiceRequest.CranePermitRequest.OperatorContactNumber,
                ContactNumber = userServiceRequest.CranePermitRequest.ContactNumber,
                PurposeOfUser = userServiceRequest.CranePermitRequest.PurposeOfUser,
                OperatorName = userServiceRequest.CranePermitRequest.OperatorName,
                MaxOperatingHeight = userServiceRequest.CranePermitRequest.MaxOperatingHeight,
                OperationStartDate = userServiceRequest.CranePermitRequest.OperationStartDate,
                OperationEndDate = userServiceRequest.CranePermitRequest.OperationEndDate,
                RequestDateTime = userServiceRequest.RequestDateTime,
                Status = userServiceRequest.Status,
                CurrentIndex = currentStage?.Index ?? 0,
                StatusName = Context.Database.ServiceFlowStatuses.FirstOrDefault(o => o.Id == userServiceRequest.Status).Description,
                PlanningModel = planningInfo != null ? planningModel : null,
                IsPlanningInfoSaved = planningInfo != null,
                SelectedCraneTypes = craneTypes,
                QASafetyModel = qaSafetyInfo != null ? qaSafetyModal : null,
                PlanningNewModel = planningNewModal,
                QASafetyNewModel = qaSafetyNewModel,
                IsQASafetyInfoSaved = qaSafetyInfo != null,
                IsFinanceInfoSaved = userServiceRequest.CranePermitRequest.IsInternal != null && userServiceRequest.ServieBill.ChargedAmount != 0.01,
                FinanceInfoModel = new CraneRequestFinanceModel { ServiceChargeAmount = userServiceRequest.ServieBill.ChargedAmount, IsInternal = null },
                FinanceInfoNewModel = new CraneRequestFinanceModel { ServiceChargeAmount = 0.01, IsInternal = userServiceRequest.CranePermitRequest.IsInternal },
                UploadedFiles = userServiceRequest.UserServiceRequestAttachments.Select(o => new FileUploadedServiceModel() { ContentType = o.UserAttachment.ContentType, FileName = o.UserAttachment.FileName, Id = o.UserServiceRequestMasterId }).ToList()
            };
        }

        public CranePermitRequestServiceModel GetCranePermitRequestDetail(int userServiceRequestMasterId)
        {
            var userServiceRequest =
                Context.Database.UserServiceRequests.FirstOrDefault(
                    o => o.UserServiceRequestMasterId == userServiceRequestMasterId && o.CranePermitRequest != null && o.ServiceId == (int)ServiceEnum.CranePermitRequest);

            if (userServiceRequest == null)
            {
                throw new DataException("Invalid Service Request Master.");
            }
            var currentStage = userServiceRequest.ServiceRequestFlowStages.FirstOrDefault(o => o.IsCurrentStage);

            var craneTypes = (from craneType in Context.Database.CraneTypes.Where(o => !o.IsDeleted)
                              join craneTypeRecord in Context.Database.CranePermitRequestSelectedCranes.Where(o => o.CranePermitRequest.UserServiceRequestMasterId == userServiceRequestMasterId)
                              on craneType.CraneTypeId equals craneTypeRecord.CraneTypeId
                              into craneTypeGroup
                              from craneRecord in craneTypeGroup.DefaultIfEmpty()
                              select new
                              {
                                  CraneTypeId = craneType.CraneTypeId,
                                  IsOther = craneType.IsOther,
                                  CraneTypeName = craneType.CraneTypeName,
                                  IsSelected = craneRecord != null,
                                  OtherCraneTypeName = craneRecord != null ? craneRecord.OtherCraneType : ""
                              })
                              .AsEnumerable().Select(o => new CraneTypeSelectedModel
                              {
                                  CraneTypeId = o.CraneTypeId,
                                  IsOther = o.IsOther,
                                  CraneTypeName = o.CraneTypeName,
                                  IsSelected = o.IsSelected,
                                  OtherCraneTypeName = o.OtherCraneTypeName
                              })
                              .ToList();

            #region "Planning"

            var planningInfo = userServiceRequest.CranePermitRequest.CranePermitRequestFirstStageEntry;
            var planningInfoNewModel = planningInfo ?? new CranePermitRequestFirstStageEntry() { UserServiceRequestMasterId = userServiceRequestMasterId };
            var savedInfringements = planningInfoNewModel.CranePermitRequestFirstStageInfringementTypes ??
                                     new List<CranePermitRequestFirstStageInfringementType>();
            var infringementTypes = (from infringementType in Context.Database.InfringementTypes.ToList()
                                     let savedInfringement = savedInfringements.FirstOrDefault(o => o.InfringementTypeId == infringementType.Id)
                                     select new InfringementTypeModel
                                     {
                                         Id = infringementType.Id,
                                         Description = infringementType.Description,
                                         IsOther = infringementType.IsOther,
                                         Active = savedInfringement != null,
                                         OtherName = savedInfringement != null ? savedInfringement.OtherName : ""
                                     }).ToList();

            var planningModel = planningInfo != null ? new CraneRequestPlanningModel()
            {
                SavedByUserId = planningInfo.SavedByUserId,
                AssessedDate = planningInfo.AssessedDate,
                AirportPlanner = planningInfo.AirportPlanner,
                RequireAviationLight = planningInfo.RequireAviationLight,
                HasPentrationOfSurface = planningInfo.HasPentrationOfSurface,
                CraneOperatingDistanceFromRunway = planningInfo.CraneOperatingDistanceFromRunway,
                AssessedBy = planningInfo.AssessedBy,
                Recommendations = planningInfo.Recommendations,
                MaxCraneOperatingHeight = planningInfo.MaxCraneOperatingHeight,
                PenetrationInMeter = planningInfo.PenetrationInMeter,
                SavedDateTime = planningInfo.SavedDateTime,
                Comments = planningInfo.Comments,
                InfringementTypes = infringementTypes
            } : null;

            #endregion "Planning"

            #region "QA Safety"

            var qaSafetyInfo = userServiceRequest.CranePermitRequest.CranePermitSafetyQAApproval;
            var qaSafetyDefaultInfo = qaSafetyInfo ?? new CranePermitSafetyQAApproval();
            var qaSafetyModal = new CraneRequestQASafetyModel()
            {
                AdditionalComments = qaSafetyDefaultInfo.AdditionalComments,
                OperatingRestrictions = qaSafetyDefaultInfo.OperatingRestrictions,
                ApprovedByPosition = qaSafetyDefaultInfo.ApprovedByPosition,
                HasSafetyAssessmentBeenDone = qaSafetyDefaultInfo.HasSafetyAssessmentBeenDone,
                ApprovedDateTime = qaSafetyDefaultInfo.ApprovedDateTime != DateTime.MinValue ? (DateTime?)qaSafetyDefaultInfo.ApprovedDateTime : null,
                ApprovedBy = qaSafetyDefaultInfo.ApprovedBy
            };

            #endregion "QA Safety"

            return new CranePermitRequestServiceModel()
            {
                ServiceId = userServiceRequest.ServiceId,
                UserName = userServiceRequest.User.UserName,
                UserId = userServiceRequest.UserId,
                RegistrationNo = userServiceRequest.RegistrationNo,
                UserServiceRequestMasterId = userServiceRequest.UserServiceRequestMasterId,
                ServiceName = userServiceRequest.Service.ServiceName,
                ApplicantEmail = userServiceRequest.User.Email,
                ApplicantName = userServiceRequest.CranePermitRequest.ApplicantName,
                CraneLocation = userServiceRequest.CranePermitRequest.CraneLocation,
                OperationStartTime = userServiceRequest.CranePermitRequest.OperationStartTime,
                OperationEndTime = userServiceRequest.CranePermitRequest.OperationEndTime,
                CompanyName = userServiceRequest.CranePermitRequest.CompanyName,
                IsNewApplication = userServiceRequest.CranePermitRequest.IsNewApplication,
                ApplicationDate = userServiceRequest.CranePermitRequest.ApplicationDate,
                Position = userServiceRequest.CranePermitRequest.Position,
                PreviousPermitNumbers = userServiceRequest.CranePermitRequest.PreviousPermitNumbers,
                NorthingsEastings = userServiceRequest.CranePermitRequest.NorthingsEastings,
                CraneOperatingCompany = userServiceRequest.CranePermitRequest.CraneOperatingCompany,
                OperatorContactNumber = userServiceRequest.CranePermitRequest.OperatorContactNumber,
                ContactNumber = userServiceRequest.CranePermitRequest.ContactNumber,
                PurposeOfUser = userServiceRequest.CranePermitRequest.PurposeOfUser,
                OperatorName = userServiceRequest.CranePermitRequest.OperatorName,
                MaxOperatingHeight = userServiceRequest.CranePermitRequest.MaxOperatingHeight,
                OperationStartDate = userServiceRequest.CranePermitRequest.OperationStartDate,
                OperationEndDate = userServiceRequest.CranePermitRequest.OperationEndDate,
                RequestDateTime = userServiceRequest.RequestDateTime,
                Status = userServiceRequest.Status,
                CurrentIndex = currentStage?.Index ?? 0,
                StatusName = Context.Database.ServiceFlowStatuses.FirstOrDefault(o => o.Id == userServiceRequest.Status).Description,
                PlanningModel = planningInfo != null ? planningModel : null,
                IsPlanningInfoSaved = planningInfo != null,
                SelectedCraneTypes = craneTypes,
                QASafetyModel = qaSafetyInfo != null ? qaSafetyModal : null,
                IsQASafetyInfoSaved = qaSafetyInfo != null,
                UploadedFiles = userServiceRequest.UserServiceRequestAttachments.Select(o => new FileUploadedServiceModel() { ContentType = o.UserAttachment.ContentType, FileName = o.UserAttachment.FileName, Id = o.UserServiceRequestMasterId }).ToList()
            };
        }

        public CranePermitRequestFirstStageEntry GetCranePermitRequestPlanning(int userServiceRequestMasterId)
        {
            var userServiceRequest =
                Context.Database.UserServiceRequests.FirstOrDefault(
                    o => o.UserServiceRequestMasterId == userServiceRequestMasterId && o.CranePermitRequest != null && o.ServiceId == (int)ServiceEnum.CranePermitRequest);

            if (userServiceRequest == null)
            {
                throw new DataException("Invalid Service Request Master.");
            }
            return userServiceRequest.CranePermitRequest.CranePermitRequestFirstStageEntry ??
                   new CranePermitRequestFirstStageEntry()
                   {
                       UserServiceRequestMasterId = userServiceRequestMasterId
                   };
        }

        #endregion "Crane Permit Related Functions"

        #region "Aircraft Maintenance Related Functions"

        public AircraftMaintenanceRequestServiceModal GetAircraftMaintenanceRequest(int userServiceRequestMasterId, bool skipCheckActiveStage = false)
        {
            var userServiceRequest =
                Context.Database.UserServiceRequests.FirstOrDefault(
                    o => o.UserServiceRequestMasterId == userServiceRequestMasterId && o.AircraftMaintenanceRequest != null && o.ServiceId == (int)ServiceEnum.AircraftMaintenanceRequest);

            if (userServiceRequest == null)
            {
                throw new DataException("Invalid Service Request Master.");
            }
            var currentStage = userServiceRequest.ServiceRequestFlowStages.FirstOrDefault(o => o.IsCurrentStage);
            if (!skipCheckActiveStage)
            {
                if (currentStage == null)
                {
                    throw new DataException("There is no currently active stage.");
                }
            }

            var aircraftMaintenanceRequest = userServiceRequest.AircraftMaintenanceRequest;

            return new AircraftMaintenanceRequestServiceModal()
            {
                ServiceId = userServiceRequest.ServiceId,
                UserName = userServiceRequest.User.UserName,
                UserId = userServiceRequest.UserId,
                RegistrationNo = userServiceRequest.RegistrationNo,
                UserServiceRequestMasterId = userServiceRequest.UserServiceRequestMasterId,
                ServiceName = userServiceRequest.Service.ServiceName,
                RequestDateTime = userServiceRequest.RequestDateTime,
                Status = userServiceRequest.Status,
                CurrentIndex = currentStage?.Index ?? 0,
                StatusName = Context.Database.ServiceFlowStatuses.FirstOrDefault(o => o.Id == userServiceRequest.Status).Description,
                AddressOfMaintenanceCompany = aircraftMaintenanceRequest.AddressOfMaintenanceCompany,
                AircraftModel = aircraftMaintenanceRequest.AircraftModel,
                AircraftRegistrationNumber = aircraftMaintenanceRequest.AircraftRegistrationNumber,
                AircraftType = aircraftMaintenanceRequest.AircraftType,
                ApplicantRegisteredMaintenanceCompany = aircraftMaintenanceRequest.ApplicantRegisteredMaintenanceCompany,
                DateOfApplication = aircraftMaintenanceRequest.DateOfApplication,
                Designation = aircraftMaintenanceRequest.Designation,
                DetailsOfMaintenance = aircraftMaintenanceRequest.DetailsOfMaintenance,
                EmailId = aircraftMaintenanceRequest.EmailId,
                MaintenanceCompanyTradeLicenseExpiryDate = aircraftMaintenanceRequest.MaintenanceCompanyTradeLicenseExpiryDate,
                MaintenanceCompanyTradeLicenseNumber = aircraftMaintenanceRequest.MaintenanceCompanyTradeLicenseNumber,
                NameOfApplicant = aircraftMaintenanceRequest.NameOfApplicant,
                Part145RegistrationExpiryDate = aircraftMaintenanceRequest.Part145RegistrationExpiryDate,
                Part145RegistrationNumber = aircraftMaintenanceRequest.Part145RegistrationNumber,
                PhoneNumber = aircraftMaintenanceRequest.PhoneNumber,
                PhoneNumberOfRefisteredmaintenanceCompany = aircraftMaintenanceRequest.PhoneNumberOfRefisteredmaintenanceCompany,
                ScheduleDateOfAircraftArrival = aircraftMaintenanceRequest.ScheduleDateOfAircraftArrival,
                ScheduleDateOfAircraftDeparture = aircraftMaintenanceRequest.ScheduleDateOfAircraftDeparture,
                TypeOfMaintenance = aircraftMaintenanceRequest.TypeOfMaintenance,
                UploadedFiles = userServiceRequest.UserServiceRequestAttachments.Select(o => new FileUploadedServiceModel() { ContentType = o.UserAttachment.ContentType, FileName = o.UserAttachment.FileName, Id = o.UserServiceRequestMasterId }).ToList()

            };
        }

        #endregion "Aircraft Maintenance Related Functions"

        #region "Aircraft Dismantling Service Request"

        public AircraftDismantlingRequestServiceModal GetAircraftDismantlingRequest(int userServiceRequestMasterId, bool skipCheckActiveStage = false)
        {
            var userServiceRequest =
                Context.Database.UserServiceRequests.FirstOrDefault(
                    o => o.UserServiceRequestMasterId == userServiceRequestMasterId && o.AircraftDismantlingNOCRequest != null && o.ServiceId == (int)ServiceEnum.AircraftDismantlingRequest);

            if (userServiceRequest == null)
            {
                throw new DataException("Invalid Service Request Master.");
            }
            var currentStage = userServiceRequest.ServiceRequestFlowStages.FirstOrDefault(o => o.IsCurrentStage);
            if (!skipCheckActiveStage)
            {
                if (currentStage == null)
                {
                    throw new DataException("There is no currently active stage.");
                }
            }

            var aircraftDismantlingRequest = userServiceRequest.AircraftDismantlingNOCRequest;

            return new AircraftDismantlingRequestServiceModal()
            {
                ServiceId = userServiceRequest.ServiceId,
                UserName = userServiceRequest.User.UserName,
                UserId = userServiceRequest.UserId,
                RegistrationNo = userServiceRequest.RegistrationNo,
                UserServiceRequestMasterId = userServiceRequest.UserServiceRequestMasterId,
                ServiceName = userServiceRequest.Service.ServiceName,
                RequestDateTime = userServiceRequest.RequestDateTime,
                Status = userServiceRequest.Status,
                CurrentIndex = currentStage?.Index ?? 0,
                StatusName = Context.Database.ServiceFlowStatuses.FirstOrDefault(o => o.Id == userServiceRequest.Status).Description,
                DateOfApplication = aircraftDismantlingRequest.DateOfApplication,
                NameOfApplicant = aircraftDismantlingRequest.NameOfApplicant,
                PhoneNumber = aircraftDismantlingRequest.PhoneNumber,
                Email = aircraftDismantlingRequest.Email,
                Address = aircraftDismantlingRequest.Address,
                ApplicantCompanyName = aircraftDismantlingRequest.ApplicantCompanyName,
                AircraftMSNAndRegistrationNumber = aircraftDismantlingRequest.AircraftMSNAndRegistrationNumber,
                OldRegistrationNo = aircraftDismantlingRequest.OldRegistrationNo,
                AircraftTypeAndModelToDismantle = aircraftDismantlingRequest.AircraftTypeAndModelToDismantle,
                CertificateOfDeRegistrationRefNumber = aircraftDismantlingRequest.CertificateOfDeRegistrationRefNumber,
                CurrentOwnerOfAircraft = aircraftDismantlingRequest.CurrentOwnerOfAircraft,
                DateOfExpiryOfFANRLicense = aircraftDismantlingRequest.DateOfExpiryOfFANRLicense,
                DateOfExpiryOfFujairahEPDLicense = aircraftDismantlingRequest.DateOfExpiryOfFujairahEPDLicense,
                DateOfExpiryOfTradeLicense = aircraftDismantlingRequest.DateOfExpiryOfTradeLicense,
                DateOfIssueOfFANRLicense = aircraftDismantlingRequest.DateOfIssueOfFANRLicense,
                DateOfIssueOfFujairahEPDLicense = aircraftDismantlingRequest.DateOfIssueOfFujairahEPDLicense,
                DateOfIssueOfTradeLicense = aircraftDismantlingRequest.DateOfIssueOfTradeLicense,
                FANRLicenseRefNumber = aircraftDismantlingRequest.FANRLicenseRefNumber,
                FujairahEPDLicenseRefNumber = aircraftDismantlingRequest.FANRLicenseRefNumber,
                GCAAApprovalRefNumber = aircraftDismantlingRequest.GCAAApprovalRefNumber,
                HasGCAAApproval = aircraftDismantlingRequest.HasGCAAApproval,
                MethodOfDismantlingAndDestruction = aircraftDismantlingRequest.MethodOfDismantlingAndDestruction,
                OperatorOrAgentWhoBroughtTheAircraft = aircraftDismantlingRequest.OperatorOrAgentWhoBroughtTheAircraft,
                PreviousOwnerOfAircraft = aircraftDismantlingRequest.PreviousOwnerOfAircraft,
                TradeLicenseRefNumber = aircraftDismantlingRequest.TradeLicenseRefNumber,
                WithNoObjectionFromTheAircraftOwner = aircraftDismantlingRequest.WithNoObjectionFromTheAircraftOwner,
                UploadedFiles = userServiceRequest.UserServiceRequestAttachments.Select(o => new FileUploadedServiceModel() { ContentType = o.UserAttachment.ContentType, FileName = o.UserAttachment.FileName, Id = o.UserServiceRequestMasterId }).ToList()

            };
        }

        #endregion "Aircraft Dismantling Service Request"

        #region "Trade License Related Functions"

        public TradeLicenseRequestServiceModel GetTradeLicenseRequest(int userServiceRequestMasterId, bool skipCheckActiveStage = false)
        {
            var userServiceRequest =
                Context.Database.UserServiceRequests.FirstOrDefault(
                    o => o.UserServiceRequestMasterId == userServiceRequestMasterId && o.TradeLicenseRequest != null && o.ServiceId == (int)ServiceEnum.TradeLicenseRequest);

            if (userServiceRequest == null)
            {
                throw new DataException("Invalid Service Request Master.");
            }
            var currentStage = userServiceRequest.ServiceRequestFlowStages.FirstOrDefault(o => o.IsCurrentStage);
            if (!skipCheckActiveStage)
            {
                if (currentStage == null)
                {
                    throw new DataException("There is no currently active stage.");
                }
            }


            var tradeLicenseRequest = userServiceRequest.TradeLicenseRequest;

            return new TradeLicenseRequestServiceModel()
            {
                ServiceId = userServiceRequest.ServiceId,
                UserName = userServiceRequest.User.UserName,
                UserId = userServiceRequest.UserId,
                RegistrationNo = userServiceRequest.RegistrationNo,
                UserServiceRequestMasterId = userServiceRequest.UserServiceRequestMasterId,
                ServiceName = userServiceRequest.Service.ServiceName,
                RequestDateTime = userServiceRequest.RequestDateTime,
                Status = userServiceRequest.Status,
                CurrentIndex = currentStage?.Index ?? 0,
                StatusName = Context.Database.ServiceFlowStatuses.FirstOrDefault(o => o.Id == userServiceRequest.Status).Description,
                ExpectedAnnualBusinessTrunoverCurrencyId = tradeLicenseRequest.ExpectedAnnualBusinessTrunoverCurrencyId,
                AddressOfSponser = tradeLicenseRequest.AddressOfSponser,
                ApplicantCountry = tradeLicenseRequest.ApplicantCountry,
                ApplicantEmailNumber = tradeLicenseRequest.ApplicantEmailNumber,
                ApplicantFaxNumber = tradeLicenseRequest.ApplicantFaxNumber,
                ApplicantMobileNumber = tradeLicenseRequest.ApplicantMobileNumber,
                ApplicantName = tradeLicenseRequest.ApplicantName,
                ApplicantOfficeNumber = tradeLicenseRequest.ApplicantOfficeNumber,
                ApplicantResidenceNumber = tradeLicenseRequest.ApplicantResidenceNumber,
                ApplicationDate = tradeLicenseRequest.ApplicationDate,
                BusinessActivities1 = tradeLicenseRequest.BusinessActivities1,
                BusinessActivities2 = tradeLicenseRequest.BusinessActivities2,
                BusinessActivities3 = tradeLicenseRequest.BusinessActivities3,
                CompanyNameOption1 = tradeLicenseRequest.CompanyNameOption1,
                CompanyNameOption2 = tradeLicenseRequest.CompanyNameOption2,
                CompanyNameOption3 = tradeLicenseRequest.CompanyNameOption3,
                CompanyPreferredName = tradeLicenseRequest.CompanyPreferredName,
                ExpectedAnnualBusinessTrunoverAmount = tradeLicenseRequest.ExpectedAnnualBusinessTrunoverAmount,
                MailingAddress = tradeLicenseRequest.MailingAddress,
                PresentSponsorInUAE = tradeLicenseRequest.PresentSponsorInUAE,
                Remarks = tradeLicenseRequest.Remarks,
                TotalStaffStrengthRequired = tradeLicenseRequest.TotalStaffStrengthRequired,
                CurrencyName = tradeLicenseRequest?.Currency?.CurrencyName ?? "",
                SelectedStaffStrengths = tradeLicenseRequest.SelectedStaffStrengths.AsEnumerable().Select(o => new StaffStrengServiceModel()
                {
                    StaffStrengthId = o.StaffStrengthId,
                    StaffStrengthName = o.StaffStrength.StaffStrengthName
                }).ToList(),
                UploadedFiles = userServiceRequest.UserServiceRequestAttachments.Select(o => new FileUploadedServiceModel() { ContentType = o.UserAttachment.ContentType, FileName = o.UserAttachment.FileName, Id = o.UserServiceRequestMasterId }).ToList()

            };
        }

        #endregion "Trade License Related Functions"

        #region "Aircraft Warning Light Request"
        public AircraftWarningLightServiceModel GetAircraftWarningRequest(int userServiceRequestMasterId)
        {
            var userServiceRequest =
                Context.Database.UserServiceRequests.FirstOrDefault(
                    o => o.UserServiceRequestMasterId == userServiceRequestMasterId && o.AircraftWarningLight != null && o.ServiceId == (int)ServiceEnum.AircraftWarningLight);

            if (userServiceRequest == null)
            {
                throw new DataException("Invalid Service Request Master.");
            }
            var currentStage = userServiceRequest.ServiceRequestFlowStages.FirstOrDefault(o => o.IsCurrentStage);


            var request = userServiceRequest.AircraftWarningLight;


            return new AircraftWarningLightServiceModel()
            {
                ServiceId = userServiceRequest.ServiceId,
                UserName = userServiceRequest.User.UserName,
                UserId = userServiceRequest.UserId,
                RegistrationNo = userServiceRequest.RegistrationNo,
                UserServiceRequestMasterId = userServiceRequest.UserServiceRequestMasterId,
                ServiceName = userServiceRequest.Service.ServiceName,
                RequestDateTime = userServiceRequest.RequestDateTime,
                Status = userServiceRequest.Status,
                CurrentIndex = currentStage?.Index,
                StatusName = Context.Database.ServiceFlowStatuses.FirstOrDefault(o => o.Id == userServiceRequest.Status).Description,
                ApplicantName = request.ApplicantName,
                ApplicationDate = request.ApplicationDate,
                ApplicantEmail = request.ApplicantEmail,
                ApplicantAddress = request.ApplicantAddress,
                ApplicantCellNo = request.ApplicantCellNo,
                ApplicantCompanyName = request.ApplicantCompanyName,
                ApplicantContactInfo = request.ApplicantContactInfo,
                ApplicantJobTitle = request.ApplicantJobTitle,
                ApplicantTelNo = request.ApplicantTelNo,
                ApplicantWebsite = request.ApplicantWebsite,
                OwnerCompanyName = request.OwnerCompanyName,
                OwnerEmail = request.OwnerEmail,
                OwnerName = request.OwnerName,
                OwnerWebsite = request.OwnerWebsite,
                OwnerAddress = request.OwnerAddress,
                OwnerCellNo = request.OwnerCellNo,
                OwnerTelNo = request.OwnerTelNo,
                OwnerContactInfo = request.OwnerContactInfo,
                ProjectDescription = request.ProjectDescription,
                ProjectLocation = request.ProjectLocation,
                ProjectName = request.ProjectName,
                NoOfObstacleLights = request.NoOfObstacleLights,
                NoOfStructures = request.NoOfStructures,
                TypesOfObstacleLights = request.TypesOfObstacleLights,
                ApplicantOwnerRelationShipId = request.ApplicantOwnerRelationShipId,
                ProjectTypeId = request.ProjectTypeId,
                ProjectTypeName = request.ProjectType.Description,
                ApplicantOwnerRelationshipName = request.ApplicantOwnerRelationship.Description,
                ApplicantOwnerRelationshipOtherName = request.ApplicantOwnerRelationshipOtherName,
                ProjectTypeOtherName = request.ProjectTypeOtherName,
                UploadedFiles = userServiceRequest.UserServiceRequestAttachments.Select(o => new FileUploadedServiceModel() { ContentType = o.UserAttachment.ContentType, FileName = o.UserAttachment.FileName, Id = o.UserServiceRequestMasterId }).ToList()
            };
        }

        #endregion

        #endregion "Service Specific Functions"

        #region Service Request List

        #endregion

        public ResponseObject<ServiceRegistrationNoModel> GetNewServiceCode(int serviceId)
        {
            var currentYear = DateTime.Now.DubaiTime().Year;
            var currentMonth = DateTime.Now.DubaiTime().Month;
            var service = Context.Database.Services.FirstOrDefault(o => o.ServiceId == serviceId);
            var result = new ResponseObject<ServiceRegistrationNoModel>();
            if (service == null)
            {
                result.IsSuccess = false;
                result.Message = "Invalid Service Information provided.";
                result.Data = null;
            }
            else
            {
                var lastServiceCode =
                    FindAllAsNoTracking(
                        o => o.ServiceId == serviceId
                             && o.RegistrationNoServiceCode == service.ServiceCode
                             && o.RegistrationNoYear == currentYear
                             && o.RegistrationNoMonth == currentMonth
                        ).GetMax(o => o.RegistrationNoIndex);
                var newCode = lastServiceCode + 1;
                var model = new ServiceRegistrationNoModel()
                {
                    RegistrationNoIndex = newCode,
                    ServiceCode = service.ServiceCode,
                    Month = currentMonth,
                    Year = currentYear
                };
                result.IsSuccess = true;
                result.Message = "";
                result.Data = model;
            }
            return result;
        }

        public MeteorologicalDataRequestServiceModal GetMeteorologicalDataRequest(int id)
        {
            var userServiceRequest = Context.Database.UserServiceRequests.FirstOrDefault(
              o => o.UserServiceRequestMasterId == id && o.MeteorologicalDataRequest != null && o.ServiceId == (int)ServiceEnum.MeteorologicalDataRequest);

            if (userServiceRequest == null)
            {
                throw new DataException("Invalid Service Request Master.");
            }
            var currentStage = userServiceRequest.ServiceRequestFlowStages.FirstOrDefault(o => o.IsCurrentStage);


            var request = userServiceRequest.MeteorologicalDataRequest;


            return new MeteorologicalDataRequestServiceModal()
            {
                ServiceId = userServiceRequest.ServiceId,
                UserServiceRequestMasterId = userServiceRequest.UserServiceRequestMasterId,
                Department = request.Department,
                UserId = userServiceRequest.UserId,
                RegistrationNo = userServiceRequest.RegistrationNo,
                Status = userServiceRequest.Status,
                CurrentIndex = currentStage?.Index,
                StatusName = Context.Database.ServiceFlowStatuses.FirstOrDefault(o => o.Id == userServiceRequest.Status).Description,
                UserName = userServiceRequest.User.UserName,
                ServiceName = userServiceRequest.Service.ServiceName,
                RequestDateTime = userServiceRequest.RequestDateTime,
                Email = request.Email,
                Fax = request.Fax,
                Address = request.Address,
                CustomRangeEndDate = request.CustomRangeEndDate,
                CustomRangeStartDate = request.CustomRangeStartDate,
                DataRangeDaily = request.DataRangeDaily,
                DataRangeHourly = request.DataRangeHourly,
                DataRangeMonthly = request.DataRangeMonthly,
                DataRangeYearly = request.DataRangeYearly,
                DataRangeSelectPeriod = request.DataRangeSelectPeriod,
                Phone = request.Phone,
                WeatherTypes = request.MeteorologicalDataRequestWeatherTypes.Select(o => new MetWeatherTypeReportServiceModel()
                {
                    IsActive = o.IsActive,
                    WeatherTypeReportId = o.WeatherTypeReportId,
                    WeatherTypeDescription = o.WeatherTypeReport.Description
                }).ToList(),
                WeatherElements = request.MeteorologicalDataRequestWeatherElements.Select(o => new MetWeatherElementReportServiceModel()
                {
                    IsActive = o.IsActive,
                    IsOther = o.WeatherElement.IsOther,
                    OtherName = o.OtherName,
                    WeatherElementId = o.WeatherElementId,
                    WeatherElementDescription = o.WeatherElement.Description
                }).ToList(),
                UploadedFiles = userServiceRequest.UserServiceRequestAttachments.Select(o => new FileUploadedServiceModel() { ContentType = o.UserAttachment.ContentType, FileName = o.UserAttachment.FileName, Id = o.UserServiceRequestMasterId }).ToList(),
                ChargedAmount = request.UserServiceRequestMaster.ServieBill.ChargedAmount,
                METDepartmentRemrks = request.METDepartmentRemrks,
                IsChargeAmountUpdated = request.UserServiceRequestMaster.Service.DefaultRate != (decimal)request.UserServiceRequestMaster.ServieBill.ChargedAmount

            };
        }

        public object GetExitOfSparePartsRequest(int id)
        {
            var userServiceRequest = Context.Database.UserServiceRequests.FirstOrDefault(o => o.UserServiceRequestMasterId == id && o.ExitOfAircraftSparePartsRequest != null && o.ServiceId == (int)ServiceEnum.ExitOfAircraftSparePartsRequest);

            if (userServiceRequest == null)
            {
                throw new DataException("Invalid Service Request Master.");
            }
            var currentStage = userServiceRequest.ServiceRequestFlowStages.FirstOrDefault(o => o.IsCurrentStage);


            var request = userServiceRequest.ExitOfAircraftSparePartsRequest;


            return new ExitOfAircraftSparePartsRequestModel()
            {
                ServiceId = userServiceRequest.ServiceId,
                UserServiceRequestMasterId = userServiceRequest.UserServiceRequestMasterId,
                UserId = userServiceRequest.UserId,
                RegistrationNo = userServiceRequest.RegistrationNo,
                Status = userServiceRequest.Status,
                CurrentIndex = currentStage?.Index,
                StatusName = Context.Database.ServiceFlowStatuses.FirstOrDefault(o => o.Id == userServiceRequest.Status).Description,
                UserName = userServiceRequest.User.UserName,
                ServiceName = userServiceRequest.Service.ServiceName,
                RequestDateTime = userServiceRequest.RequestDateTime,
                AircraftType = request.AircraftType,
                AircraftRegistrationNumber = request.AircraftRegistrationNumber,
                DateOfApplication = request.DateOfApplication,
                AircraftMaintenanceOrganisationOrCar145GCAARegisteredCo = request.AircraftMaintenanceOrganisationOrCar145GCAARegisteredCo,
                ApplicantCompany = request.ApplicantCompany,
                ApplicantMailIdOrContactDetails = request.ApplicantMailIdOrContactDetails,
                NoObjectionLetterOfTheOwner = request.NoObjectionLetterOfTheOwner,
                HasNoObjectionLetter = request.HasNoObjectionLetter,
                OwnerOfAircraft = request.OwnerOfAircraft,
                ExitAndReturnPassChecklists = request.ExitAndReturnPassChecklists.Select(p => new ExitAndReturnPassChecklistServiceModel()
                {
                    UserServiceRequestMasterId = p.UserServiceRequestMasterId,
                    Description = p.Description,
                    ExitSparePartsPassChecklistId = p.ExitSparePartsPassChecklistId,
                    ExitToWhere = p.ExitToWhere,
                    NumberOfPieces = p.NumberOfPieces,
                    Origin = p.Origin,
                    PartNumber = p.PartNumber
                }).ToList(),
                UploadedFiles = userServiceRequest.UserServiceRequestAttachments.Select(o => new FileUploadedServiceModel() { ContentType = o.UserAttachment.ContentType, FileName = o.UserAttachment.FileName, Id = o.UserServiceRequestMasterId }).ToList()

            };
        }

        public object GetBuildingHeightRequest(int id)
        {

            var userServiceRequest = Context.Database.UserServiceRequests.FirstOrDefault(o => o.UserServiceRequestMasterId == id && o.BuildingHeightRequest != null && o.ServiceId == (int)ServiceEnum.BuildingHeightRequest);

            if (userServiceRequest == null)
            {
                throw new DataException("Invalid Service Request for Building Height Request.");
            }
            var currentStage = userServiceRequest.ServiceRequestFlowStages.FirstOrDefault(o => o.IsCurrentStage);


            var request = userServiceRequest.BuildingHeightRequest;
            TinyMapper.Bind<BuildingHeightRequest, BuildingHeightRequestServiceModel>();

            var buildingHeightRequestViewModel = TinyMapper.Map<BuildingHeightRequestServiceModel>(request);
            buildingHeightRequestViewModel.IsChargeAmountUpdated = request.UserServiceRequestMaster.Service.DefaultRate !=
                (decimal)request.UserServiceRequestMaster.ServieBill.ChargedAmount;
            buildingHeightRequestViewModel.CurrentIndex = currentStage?.Index;

            buildingHeightRequestViewModel.ApplicantOwnerRelationshipName =
                request.ApplicantOwnerRelationship.Description;

            buildingHeightRequestViewModel.ApplicantOwnerRelationshipOtherName =
                request.ApplicantOwnerRelationshipOtherName;

            buildingHeightRequestViewModel.BuildingCoordinateAndHeights = (from coordinate in Context.Database.BuildingCoordinateAndHeightLookUps.AsEnumerable()
                                                                           join savedCoordinate in request.BuildingCoordinateAndHeights on coordinate.Id equals savedCoordinate.BuildingCoordinateAndHeightLookUpId
                                                                                                       into leftJoinCoordinate
                                                                           from defaultCoordinate in leftJoinCoordinate.DefaultIfEmpty()
                                                                           select new BuildingCoordinateAndHeightServiceModel()
                                                                           {
                                                                               BuildingCoordinateAndHeightLookUpDescription = coordinate.Description,
                                                                               BuildingCoordinateAndHeightLookUpId = coordinate.Id,
                                                                               Easting = defaultCoordinate != null ? defaultCoordinate.Easting : "",
                                                                               GroundHeight = defaultCoordinate != null ? defaultCoordinate.GroundHeight : null,
                                                                               Northing = defaultCoordinate != null ? defaultCoordinate.Northing : "",
                                                                               StructureHeight = defaultCoordinate != null ? defaultCoordinate.StructureHeight : null,
                                                                           }).ToList();

            buildingHeightRequestViewModel.AdditionalItemsForBuildingSubmittals = (from item in Context.Database.AdditionalItemsForBuildingSubmittalLookUps.AsEnumerable()
                                                                                   join savedItem in request.AdditionalItemsForBuildingSubmittals on item.Id
                                                                                   equals savedItem.AdditionalItemsForBuildingSubmittalId
                                                                                                       into leftJoinItem
                                                                                   from defaultItem in leftJoinItem.DefaultIfEmpty()
                                                                                   select new AdditionalItemsForBuildingSubmittalServiceModel()
                                                                                   {
                                                                                       AdditionalItemsForBuildingSubmittalLookUpDescription = item.Description,
                                                                                       AdditionalItemsForBuildingSubmittalLookUpId = item.Id,
                                                                                       IsOther = item.IsOther,
                                                                                       IsSelected = defaultItem != null,
                                                                                       OtherDescription = defaultItem != null ? defaultItem.OtherDescription : ""
                                                                                   }).ToList();
            buildingHeightRequestViewModel.AviationObstacles = (from item in Context.Database.AviationObstacleLookUps.AsEnumerable()
                                                                join savedItem in request.BuildingHeightAviationObstacles on item.Id
                                                                equals savedItem.AviationObstacleLookUpId
                                                                                    into leftJoinItem
                                                                from defaultItem in leftJoinItem.DefaultIfEmpty()
                                                                select new AviationObstacleServiceModel()
                                                                {
                                                                    Description = item.Description,
                                                                    Id = item.Id,
                                                                    IsSelected = defaultItem != null,
                                                                }).ToList();

            buildingHeightRequestViewModel.AdditionalItemsForOHLSubmittals = (from item in Context.Database.AdditionalItemsForBuildingSubmittalLookUps.AsEnumerable()
                                                                              join savedItem in request.AdditionalItemsForOHLSubmittals on item.Id
                                                                                         equals savedItem.AdditionalItemsForOHLSubmittalLookUpId
                                                                                                             into leftJoinItem
                                                                              from defaultItem in leftJoinItem.DefaultIfEmpty()
                                                                              select new AdditionalItemsForOHLSubmittalServiceModel()
                                                                              {
                                                                                  IsOther = item.IsOther,
                                                                                  IsSelected = defaultItem != null,
                                                                                  OtherDescription = defaultItem != null ? defaultItem.OtherDescription : "",
                                                                                  AdditionalItemsForOHLSubmittalLookUpDescription = item.Description,
                                                                                  AdditionalItemsForOHLSubmittalLookUpId = item.Id
                                                                              }).ToList();

            buildingHeightRequestViewModel.BuildingHeightApplicableFees = (from item in Context.Database.ApplicableFeesLookUps.AsEnumerable()
                                                                           join savedItem in request.BuildingHeightApplicableFees on item.Id
                                                                                           equals savedItem.ApplicableFeesLookUpId
                                                                                                               into leftJoinItem
                                                                           from defaultItem in leftJoinItem.DefaultIfEmpty()
                                                                           select new BuildingHeightApplicableFeeServiceModel()
                                                                           {
                                                                               ApplicableFeesLookUpDescription = item.Description,
                                                                               ApplicableFeesLookUpId = item.Id,
                                                                               IsSelected = defaultItem != null,
                                                                               Fee = item.Fee,
                                                                               Fees = item.Fees,
                                                                               Quantity = defaultItem != null ? defaultItem.Quantity : 0,
                                                                               Notes = item.Notes,
                                                                               TotalFee = ((double)(item.Fee ?? 0)) * (defaultItem != null ? defaultItem.Quantity : 0)
                                                                           }).ToList();
            buildingHeightRequestViewModel.ServiceId = userServiceRequest.ServiceId;
            buildingHeightRequestViewModel.UserServiceRequestMasterId = userServiceRequest.UserServiceRequestMasterId;
            buildingHeightRequestViewModel.UserId = userServiceRequest.UserId;
            buildingHeightRequestViewModel.RegistrationNo = userServiceRequest.RegistrationNo;
            buildingHeightRequestViewModel.Status = userServiceRequest.Status;
            buildingHeightRequestViewModel.ProjectTypeId = request.ProjectTypeId;
            buildingHeightRequestViewModel.SelectedProjectDescription = request.ProjectType.Description;
            buildingHeightRequestViewModel.OtherProjectTypeDescription = request.OtherProjectTypeDescription;
            buildingHeightRequestViewModel.StatusName =
                Context.Database.ServiceFlowStatuses.FirstOrDefault(o => o.Id == userServiceRequest.Status).Description;
            buildingHeightRequestViewModel.UserName = userServiceRequest.User.UserName;
            buildingHeightRequestViewModel.ServiceName = userServiceRequest.Service.ServiceName;
            buildingHeightRequestViewModel.RequestDateTime = userServiceRequest.RequestDateTime;
            buildingHeightRequestViewModel.UploadedFiles =
                userServiceRequest.UserServiceRequestAttachments.Select(
                    o =>
                        new FileUploadedServiceModel()
                        {
                            ContentType = o.UserAttachment.ContentType,
                            FileName = o.UserAttachment.FileName,
                            Id = o.UserServiceRequestMasterId
                        }).ToList();
            if (userServiceRequest.BuildingHeightRequest.BuildingHeightPlanningApproval != null)
            {
                buildingHeightRequestViewModel.PlanningApprovalModel = new BuildingHeightRequestServiceModel.PlanningApprove
                {
                    Remarks = userServiceRequest.BuildingHeightRequest.BuildingHeightPlanningApproval.Remarks,
                    UploadedFiles = userServiceRequest.BuildingHeightRequest.BuildingHeightPlanningApproval.Attachments.Select(o => new UploadedFile { Id = o.UserAttachmentId, FileName = o.Attachment.FileName, ContentType = o.Attachment.ContentType }).ToList()
                };
            }
            return buildingHeightRequestViewModel;

        }

        public object GetGroundHandlingRequset(int id)
        {
            var userServiceRequest =
                Find(o => o.UserServiceRequestMasterId == id && o.GroundHandlingRequest != null)
                ;

            if (userServiceRequest == null)
            {
                throw new DataException("Invalid Service Request for Building Height Request.");
            }
            var currentStage = userServiceRequest.ServiceRequestFlowStages.FirstOrDefault(o => o.IsCurrentStage);


            var request = userServiceRequest.GroundHandlingRequest;
            TinyMapper.Bind<GroundHandlingRequest, GroundHandlingRequestServiceModel>();
            TinyMapper.Bind<AircraftInteriorCleaning, AircraftInteriorCleaningServiceModel>();
            var model = TinyMapper.Map<GroundHandlingRequest, GroundHandlingRequestServiceModel>(request);

            model.ServiceId = userServiceRequest.ServiceId;
            model.UserServiceRequestMasterId = userServiceRequest.UserServiceRequestMasterId;
            model.UserId = userServiceRequest.UserId;
            model.RegistrationNo = userServiceRequest.RegistrationNo;
            model.Status = userServiceRequest.Status;
            model.CurrentIndex = currentStage?.Index;
            model.StatusName =
                Context.Database.ServiceFlowStatuses.FirstOrDefault(o => o.Id == userServiceRequest.Status).Description;
            model.UserName = userServiceRequest.User.UserName;
            model.ServiceName = userServiceRequest.Service.ServiceName;
            model.RequestDateTime = userServiceRequest.RequestDateTime;
            model.AircraftCleaning = TinyMapper.Map<AircraftInteriorCleaning, AircraftInteriorCleaningServiceModel>(request.AircraftCleaning);
            model.GroundHandlingArcraftTypes =
                request.AircraftTypes
                    .Select(o => new GroundHandlingArcraftTypeServiceModel()
                    {
                        Description = o.AircraftType.Description,
                        IsOther = o.AircraftType.IsOther,
                        IsSelected = true,
                        AircraftTypeId = o.AircraftTypeId
                    })
                    .ToList();
            return model;
        }

        #region "Summary Reports"

        public List<ServiceRequestSummary> GetServiceStatusSummary(DateTime? dateFrom, DateTime? dateTo,
            int? serviceId = null, int? statusId = null)
        {
            var a = (from request in FindAllAsNoTracking(o => !o.IsDeleted)
                     join service in Context.Database.Services on request.ServiceId equals service.ServiceId
                     join serviceBill in Context.Database.UserServiceRequestBills on request.UserServiceRequestMasterId equals serviceBill.UserServiceRequestMasterId
                     join status in Context.Database.ServiceFlowStatuses on request.Status equals status.Id
                     where (dateFrom == null || request.RequestDateTime >= dateFrom.Value)
                        && (dateTo == null || request.RequestDateTime <= dateTo.Value)
                        && (serviceId == null || request.ServiceId == serviceId.Value)
                        && (statusId == null || request.Status == statusId.Value)
                     group new { request, serviceBill } by new
                     {
                         request.ServiceId,
                         service.ServiceName,
                         service.ServiceCode,
                         request.Status,
                         StatusName = status.Description
                     }
                        into reportGroup
                     select new ServiceRequestSummary
                     {
                         ServiceId = reportGroup.Key.ServiceId,
                         Status = reportGroup.Key.Status,
                         ServiceName = reportGroup.Key.ServiceName,
                         StatusName = reportGroup.Key.StatusName,
                         ServiceCode = reportGroup.Key.ServiceCode,
                         //DefaultRate = reportGroup.Key.DefaultRate,
                         PaidAmount = reportGroup.Sum(o => o.serviceBill.PaidAmount ?? 0),
                         ChargedAmount = reportGroup.Sum(o => o.serviceBill.ChargedAmount),
                         Count = reportGroup.Count()
                     }
                );

            return a.ToList();
        }

        //public List<ServiceRequestSummary> GetServiceStatusSummary(DateTime? dateFrom, DateTime? dateTo,
        //   int? serviceId = null, int? statusId = null)
        //{
        //    var a = (from request in FindAllAsNoTracking(o => !o.IsDeleted)
        //             join service in Context.Database.Services on request.ServiceId equals service.ServiceId
        //             join status in Context.Database.ServiceFlowStatuses on request.Status equals status.Id
        //             where (dateFrom == null || request.RequestDateTime >= dateFrom.Value)
        //                && (dateTo == null || request.RequestDateTime <= dateTo.Value)
        //                && (serviceId == null || request.ServiceId == serviceId.Value)
        //                && (statusId == null || request.Status == statusId.Value)
        //             group request by new
        //             {
        //                 request.ServiceId,
        //                 service.ServiceName,
        //                 service.ServiceCode,
        //                 request.Status,
        //                 StatusName = status.Description,

        //             }
        //                into reportGroup
        //             select new ServiceRequestSummary
        //             {
        //                 ServiceId = reportGroup.Key.ServiceId,
        //                 Status = reportGroup.Key.Status,
        //                 ServiceName = reportGroup.Key.ServiceName,
        //                 StatusName = reportGroup.Key.StatusName,
        //                 ServiceCode = reportGroup.Key.ServiceCode,
        //                 Count = reportGroup.Count()
        //             }
        //        );

        //    return a.ToList();
        //}
        #endregion

        public ResponseObject<PaymentResponseServiceViewModel> GetPaymentRelatedServiceInfo(int id, int userId, bool isPreStagepayment = false)
        {
            var result = new ResponseObject<PaymentResponseServiceViewModel>();
            var serviceRequest = Find(o => o.UserServiceRequestMasterId == id && !o.IsDeleted && (isPreStagepayment || !o.IsDraft) && o.UserId == userId);
            if (serviceRequest == null)
            {
                result.IsSuccess = false;
                result.Message = "Invalid service information provided to obtain information.";
            }
            else
            {
                var serviceRequestStages = serviceRequest.ServiceRequestFlowStages;
                if (!isPreStagepayment && serviceRequest.Status != (int)ServiceFlowStateEnum.PaymentPending)
                {
                    result.IsSuccess = false;
                    result.Message = "Service has either already payment completed or is not yet valid for payment.";
                }
                else
                {
                    TinyMapper.Bind<PaymentResponseServiceViewModel, UserServiceRequestMaster>();
                    result.Data = TinyMapper.Map<PaymentResponseServiceViewModel>(serviceRequest);
                    var user = Context.Database.Users.FirstOrDefault(o => o.UserId == userId);
                    result.Data.UserName = user.UserName;
                    result.Data.FirstName = user.FirstName;
                    result.Data.LastName = user.LastName;
                    result.Data.ServiceName = serviceRequest.Service.ServiceName;
                    result.Data.UserStreet1 = "";
                    result.Data.UserStreet2 = "";
                    result.Data.City = "City";
                    result.Data.State = "State";
                    result.Data.PostalCode = "";
                    result.Data.ChargedAmount = serviceRequest.ServieBill.ChargedAmount;
                    result.Data.UserEmail = user.Email;
                    result.Data.PhoneNumber = "";
                    result.Data.Country = "";
                    result.Data.VATPercentage = serviceRequest.VAT;
                    result.IsSuccess = true;
                }
            }
            return result;
        }


        public UserServiceRequestReceipt AddReceiptBeforePayment(PaymentResponseServiceViewModel model, int userId)
        {
            var userServiceRequest = Find(o => o.UserServiceRequestMasterId == model.UserServiceRequestMasterId);
            var receipt = new UserServiceRequestReceipt
            {
                AdminFee = model.AdminFee,
                VATAmount = model.VAT,
                CreatedDate = DateTime.Now,
                CreatedByUserId = userId,
                IsPaid = false,
                PaymentStage = 1,
                ServiceDevelopmentCharge = model.SocialDevelopmentFee,
                ReceiptDetails = new List<UserServiceRequestReceiptDetail> { new UserServiceRequestReceiptDetail
                {
                    Description = model.ServiceName,
                    Quantity = 1,
                    Rate = model.ChargedAmount,
                }
                }
            };
            userServiceRequest.UserServiceRequestReceipts.Add(receipt);
            Context.Commit();
            return receipt;
        }
        //public ResponseObject<ServiceRegistrationNoModel> GetNewReceipt()
        //{
        //    var currentYear = DateTime.Now.DubaiTime().Year;
        //    var currentMonth = DateTime.Now.DubaiTime().Month;
        //    var result = new ResponseObject<ServiceRegistrationNoModel>();
        //        var lastServiceCode =
        //            FindAllAsNoTracking(
        //                o => o.ServiceId == serviceId
        //                     && o.RegistrationNoServiceCode == service.ServiceCode
        //                     && o.RegistrationNoYear == currentYear
        //                     && o.RegistrationNoMonth == currentMonth
        //                ).GetMax(o => o.RegistrationNoIndex);
        //        var newCode = lastServiceCode + 1;
        //        var model = new ServiceRegistrationNoModel()
        //        {
        //            RegistrationNoIndex = newCode,
        //            ServiceCode = service.ServiceCode,
        //            Month = currentMonth,
        //            Year = currentYear
        //        };
        //        result.IsSuccess = true;
        //        result.Message = "";
        //        result.Data = model;
        //    }
        //    return result;
        //}

        public ResponseObject<List<ReceiptPrintModel>> GetReceiptModel(int userServiceRequestMasterId, int? forUserId = null)
        {

            var result = new ResponseObject<List<ReceiptPrintModel>>();

            var paymentCompletedStatus = (int)ServiceFlowStateEnum.PaymentCompleted;

            var userServiceRequest = Find(o => !o.IsDeleted && o.UserServiceRequestMasterId == userServiceRequestMasterId && (forUserId == null || o.UserId == forUserId));

            if (userServiceRequest == null)
            {

                result.IsSuccess = false;
                result.Message = "Service Request could not be found for requested information";
            }
            else
            {
                var paidReceipts = userServiceRequest.UserServiceRequestReceipts.Where(o => o.IsPaid);
                var userCompanyProfile = userServiceRequest.User.UserCompanyProfile;
                var model = paidReceipts.Select(o => new ReceiptPrintModel()
                {
                    Address = userCompanyProfile != null ? userCompanyProfile.CompanyAddress : "",
                    RequestRegistrationNo = userServiceRequest.RegistrationNo,
                    UserName = userServiceRequest.User.FirstName + " " + userServiceRequest.User.LastName,
                    AdminFee = o.AdminFee ?? 0,
                    ReceiptNo = o.ReceiptNo,
                    ReceiptId = o.UserServiceRequestReceiptId,
                    PaidDateTime = o.CreatedDate,
                    VAT = o.VATAmount ?? 0,
                    SocialDevelopmentFee = o.ServiceDevelopmentCharge ?? 0,
                    Total = o.AdminFee ?? 0,
                    Items = o.ReceiptDetails.Select(p => new ReceiptPrintModel.ReceiptItem
                    {
                        Description = p.Description,
                        Quantity = p.Quantity,
                        Rate = p.Rate
                    }).ToList()
                }).ToList();
                result.IsSuccess = true;
                result.Data = model;
            }
            return result;
        }

        public ResponseObject<ReceiptPrintModel> GetSingleReceipt(int receiptId, int? forUserId = null)
        {

            var result = new ResponseObject<ReceiptPrintModel>();

            var paymentCompletedStatus = (int)ServiceFlowStateEnum.PaymentCompleted;

            var receiptInfo = Context.Database.UserServiceRequestReceipts.FirstOrDefault(o => o.IsPaid && o.UserServiceRequestReceiptId == receiptId && (forUserId == null || o.UserServiceRequestMaster.UserId == forUserId));// Find(o => !o.IsDeleted && o.UserServiceRequestMasterId == o. && o.Status == paymentCompletedStatus && (forUserId == null || o.UserId == forUserId));

            if (receiptInfo == null)
            {

                result.IsSuccess = false;
                result.Message = "Receipt Info could not be found for requested information";
            }
            else
            {
                var userServiceRequest = receiptInfo.UserServiceRequestMaster;
                var model = new ReceiptPrintModel()
                {
                    //Address = userCompanyProfile != null ? userCompanyProfile.CompanyAddress : "",
                    RequestRegistrationNo = userServiceRequest.RegistrationNo,
                    UserName = userServiceRequest.User.FirstName + " " + userServiceRequest.User.LastName,
                    AdminFee = receiptInfo.AdminFee ?? 0,
                    ReceiptNo = receiptInfo.ReceiptNo,
                    PaidDateTime = receiptInfo.CreatedDate,
                    ReceiptId = receiptInfo.UserServiceRequestReceiptId,
                    VAT = receiptInfo.VATAmount ?? 0,
                    SocialDevelopmentFee = receiptInfo.ServiceDevelopmentCharge ?? 0,
                    Total = receiptInfo.AdminFee ?? 0,
                    Items = receiptInfo.ReceiptDetails.Select(p => new ReceiptPrintModel.ReceiptItem
                    {
                        Description = p.Description,
                        Quantity = p.Quantity,
                        Rate = p.Rate
                    }).ToList()
                };
                result.IsSuccess = true;
                result.Data = model;
            }
            return result;
        }
    }
}