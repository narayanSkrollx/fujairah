﻿namespace EService.Service.Logic.Enum
{
    public enum ServiceEnum
    {
        GroundHandlingRequest = 1,
        CranePermitRequest = 2,
        AircraftWarningLight = 3,
        AircraftDismantlingRequest = 4,
        AircraftMaintenanceRequest = 5,
        TradeLicenseRequest = 6,
        ExitOfAircraftSparePartsRequest = 7,
        MeteorologicalDataRequest = 8,
        BuildingHeightRequest = 9,

        Other = 1000
    }
}