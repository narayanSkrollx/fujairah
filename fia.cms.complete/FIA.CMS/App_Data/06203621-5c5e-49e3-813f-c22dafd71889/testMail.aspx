﻿<%@ Page Language="C#"  MasterPageFile="/umbraco/masterpages/umbracoPage.Master" AutoEventWireup="true" CodeBehind="testMail.aspx.cs" Inherits="Designit.Umb.Newsletter.Custom.plugins.designit.newsletter.TestMail" %>
<%@ Register Namespace="umbraco.uicontrols" Assembly="controls" TagPrefix="umb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">

<umb:UmbracoPanel ID="Panel1" runat="server" Width="608px" Height="336px" hasMenu="true">
      <umb:Pane ID="Pane1" runat="server" Height="44px" Width="528px">
        <umb:PropertyPanel ID="pp_TestMail" runat="server" Text="Recievers (seperate with ; or ,)">
            <asp:TextBox ID="txtModtager" Width="350px" runat="server"></asp:TextBox><br />
            <div style="float:right;">
                <asp:Button ID="btnSend" runat="server" Text="Send mail" OnClick="btnSend_Click" />
            </div>
        </umb:PropertyPanel>
      </umb:Pane>
    </umb:UmbracoPanel>
 
</asp:Content>

