using System.Collections.Generic;
using System.Linq;
using EService.Data.Entity.Entity;
using EService.Service.Model;

namespace EService.Service.Logic
{
    public class UserAttachmentService : BusinessAccessBase<UserAttachment>
    {
        public UserAttachmentService(ServiceContext context) : base(context)
        {
        }

        public bool SaveUserAttachment(UserAttachment attachment)
        {
            Add(attachment);
            Context.Commit();
            return true;
        }

        public IEnumerable<UploadedFileInfoServiceModel> GetUploadedFileInfoServiceModels(int userServiceMasterId)
        {
            return (from file in FindAllAsNoTracking(o => true)
                    join user in Context.Database.Users on file.UserId equals user.UserId
                    join service in Context.Database.Services on file.ServiceId equals service.ServiceId
                    join userServiceRequest in Context.Database.UserServiceRequestAttachment on file.UserAttachmentId equals userServiceRequest.UserAttachmentId
                    where userServiceRequest.UserServiceRequestMasterId == userServiceMasterId
                    select new UploadedFileInfoServiceModel
                    {
                        ContentType = file.ContentType,
                        UserAttachmentId = file.UserAttachmentId,
                        FileName = file.FileName,
                        ServiceId = file.ServiceId,
                        ServiceName = service.ServiceName,
                        UploadedBy = file.UserId,
                        UploadedByName = user.UserName
                    });
        }
    }
}