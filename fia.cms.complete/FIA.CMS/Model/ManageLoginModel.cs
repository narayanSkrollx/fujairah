﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FIA.CMS.Model
{
    public class ManageLoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}