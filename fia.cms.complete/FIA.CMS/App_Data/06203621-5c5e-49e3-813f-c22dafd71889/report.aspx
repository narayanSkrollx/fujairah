﻿<%@ Page Language="C#" MasterPageFile="/umbraco/masterpages/umbracoPage.Master" AutoEventWireup="true" CodeBehind="report.aspx.cs" Inherits="Designit.Umb.Newsletter.Custom.plugins.designit.newsletter.report" %>
<%@ Register Namespace="umbraco.uicontrols" Assembly="controls" TagPrefix="umb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">

<umb:UmbracoPanel ID="Panel1" runat="server" Width="608px" Height="500px" hasMenu="false">
      <umb:Pane ID="Pane1" runat="server" Height="44px" Width="550px">
        <umb:PropertyPanel ID="pp_Report" runat="server">
            <asp:Literal ID="litText" runat="server" />
            <asp:Table ID="tblClicks" runat="server" />
        </umb:PropertyPanel>
      </umb:Pane>
    </umb:UmbracoPanel>

</asp:Content>