﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Web.Mvc;
using FIA.CMS.Helper;
using FIA.CMS.Model;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace FIA.CMS.Controller
{
    public class SiteLayoutController : BaseController
    {

        public ActionResult RenderTopHeader()
        {
            return PartialView($"{SiteLayoutPartialViewLocation}_TopHeader.cshtml");
        }

        //public ActionResult RenderHeader()
        //{
        //    return PartialView($"{SiteLayoutPartialViewLocation}_Header.cshtml");
        //}

        public ActionResult RenderSocialSiteLink()
        {
            return PartialView($"{SiteLayoutPartialViewLocation}_SocialSiteLinks.cshtml");
        }

        public ActionResult RenderFooter()
        {
            return PartialView($"{SiteLayoutPartialViewLocation}_Footer.cshtml");
        }

        public ActionResult RenderSiteMenu()
        {
            if (Umbraco.AssignedContentItem.Url.Contains("/travellers/"))
            {
                return PartialView($"{SiteLayoutPartialViewLocation}_TravellerSiteMenu.cshtml");

            }
            else
            {
                return PartialView($"{SiteLayoutPartialViewLocation}_SiteMenu.cshtml");

            }

        }


        #region "Dynamic Menu"
        /// <summary>
        /// Renders the top navigation partial
        /// </summary>
        /// <returns>Partial view with a model</returns>
        public ActionResult RenderHeader()
        {
            List<NavigationListItem> nav = Caching.GetObjectFromCache<List<NavigationListItem>>("resultOfExpensiveQuery", 10, GetNavigationModelFromDatabase);

            //List<NavigationListItem> nav = GetNavigationModelFromDatabase();
            return PartialView(SiteLayoutPartialViewLocation + "_Header.cshtml", nav);
        }

        /// <summary>
        /// Finds the home page and gets the navigation structure based on it and it's children
        /// </summary>
        /// <returns>A List of NavigationListItems, representing the structure of the site.</returns>
        private List<NavigationListItem> GetNavigationModelFromDatabase()
        {

            //const int HOME_PAGE_POSITION_IN_PATH = 2;
            //int homePageId = int.Parse(CurrentPage.Path.Split(',')[HOME_PAGE_POSITION_IN_PATH]);
            IPublishedContent homePage = CurrentPage.AncestorOrSelf("home");
            //IPublishedContent homePage = Umbraco.Content(homePageId);
            List<NavigationListItem> nav = new List<NavigationListItem>();
            nav.Add(new NavigationListItem(new NavigationLink(homePage.Url, homePage.Name)));
            nav.AddRange(GetChildNavigationList(homePage));
            return nav;
        }

        /// <summary>
        /// Loops through the child pages of a given page and their children to get the structure of the site.
        /// </summary>
        /// <param name="page">The parent page which you want the child structure for</param>
        /// <returns>A List of NavigationListItems, representing the structure of the pages below a page.</returns>
        private List<NavigationListItem> GetChildNavigationList(IPublishedContent page)
        {
            List<NavigationListItem> listItems = null;
            List<IPublishedContent> childPages = page.Children.ToList();
            if (childPages != null && childPages.Any())
            {
                listItems = new List<NavigationListItem>();
                foreach (var childPage in childPages)
                {
                    NavigationListItem listItem = new NavigationListItem(new NavigationLink(childPage.Url, childPage.Name));
                    listItem.Items = GetChildNavigationList(childPage);
                    listItems.Add(listItem);
                }
            }
            return listItems;
        }

        #endregion

    }
}