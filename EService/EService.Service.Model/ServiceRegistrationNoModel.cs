﻿using System;

namespace EService.Service.Model
{
    public class ServiceRegistrationNoModel
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public string ServiceCode { get; set; }
        public int RegistrationNoIndex { get; set; }
        public string RegistrationNo => ServiceCode + "-" + Year.ToString().PadLeft(4, '0') + "-" + Month.ToString().PadLeft(2, '0') + "-" + RegistrationNoIndex.ToString().PadLeft(5, '0');
    }
}