﻿using System;
using umbraco.DataLayer;
using System.Text;
using System.Web.UI.WebControls;

namespace Designit.Umb.Newsletter.Custom.plugins.designit.newsletter
{
    public partial class report : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var id = Int32.Parse(Request["id"]);
            var numRecipients = Bll.Helper.GetMembersForCampaign(id).Count;

            var title =
                umbraco.BusinessLogic.Application.SqlHelper.ExecuteScalar<string>("select camptitle from newsLetterCampaignNewsletter where id=@id",
                    umbraco.BusinessLogic.Application.SqlHelper.CreateParameter("@id", id));

            var sbReport = new StringBuilder();
            sbReport.AppendLine("Report for campaign: " + title + "<br />");
            sbReport.AppendLine("<br />");
            sbReport.AppendLine("Number of recipients: " + numRecipients + "<br />");
            sbReport.AppendLine("<br />");
            sbReport.AppendLine("Link tracking");

            var google = umbraco.BusinessLogic.Application.SqlHelper.ExecuteScalar<string>("select campGoogle from newsLetterCampaignNewsletter where id=@id",
                       umbraco.BusinessLogic.Application.SqlHelper.CreateParameter("@id", id));
            if (google == "true")
            {
                sbReport.AppendLine(" (Local links are not showed here. They are tracked using Google Analytics)");
            }
            sbReport.AppendLine(":<br />");
            litText.Text = sbReport.ToString();


            using (var reader =
                umbraco.BusinessLogic.Application.SqlHelper.ExecuteReader("select * from newsletterTracking where campaignId=@campaignId",
                    umbraco.BusinessLogic.Application.SqlHelper.CreateParameter("@campaignId", id)))
            {
                if (reader.HasRecords)
                {
                    var headerRow = new TableHeaderRow();
                    var headerCell = new TableHeaderCell {Text = "Link"};
                    headerRow.Cells.Add(headerCell);
                    headerCell = new TableHeaderCell {Text = "Clicks"};
                    headerRow.Cells.Add(headerCell);
                    tblClicks.Rows.Add(headerRow);
                    while (reader.Read())
                    {
                        var row = new TableRow();
                        var cell = new TableCell {Text = reader.GetString("url")};
                        row.Cells.Add(cell);
                        cell = new TableCell {Text = reader.GetInt("clicks").ToString()};
                        row.Cells.Add(cell);
                        tblClicks.Rows.Add(row);
                    }
                }
                else
                    tblClicks.Visible = false;
            }
        }
    }
}
