﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EService.Web.Models
{
    public class AircraftMaintenanceRequestViewModel : EServiceCreateBaseViewModel
    {
        public int UserServiceRequestMasterId { get; set; }

        [Display(Name = "Date of Application")]
        [Required(ErrorMessage = "Date of Application is required")]
        public DateTime DateOfApplication { get; set; }
        [Display(Name = "Name of the Applicant ")]
        [Required(ErrorMessage = "Name of Applicant is required")]
        public string NameOfApplicant { get; set; }
        [Display(Name = "Designation")]
        public string Designation { get; set; }
        [Display(Name = "Phone / Mobile No")]
        public string PhoneNumber { get; set; }
        [Display(Name = "E-mail id")]
        [EmailAddress(ErrorMessage = "Please enter a valid e-mail address")]
        public string EmailId { get; set; }
        [Display(Name = "Applicant registered Maintenance Company")]
        public string ApplicantRegisteredMaintenanceCompany { get; set; }
        [Display(Name = "Address of the registered Maintenance company")]
        public string AddressOfMaintenanceCompany { get; set; }
        [Display(Name = "Phone No. of the registered Maintenance company")]
        public string PhoneNumberOfRefisteredmaintenanceCompany { get; set; }
        [Display(Name = "Maintenance company trade License No.")]
        public string MaintenanceCompanyTradeLicenseNumber { get; set; }
        [Display(Name = "Maintenance company trade License Expiry date")]
        public DateTime? MaintenanceCompanyTradeLicenseExpiryDate { get; set; }
        [Display(Name = "Part 145 Registration No.")]
        public string Part145RegistrationNumber { get; set; }
        [Display(Name = "Part 145 Registration Expiry date")]
        public DateTime? Part145RegistrationExpiryDate { get; set; }
        [Display(Name = "Aircraft Type")]
        public string AircraftType { get; set; }
        [Display(Name = "Aircraft Model")]
        public string AircraftModel { get; set; }
        [Display(Name = "Aircraft Registration No")]
        public string AircraftRegistrationNumber { get; set; }
        // true for scheduled and 
        // false for Non Scheduled
        [Display(Name = "Type of Maintenance")]
        public bool? TypeOfMaintenance { get; set; }
        [Display(Name = "State the details of Maintenance")]
        public string DetailsOfMaintenance { get; set; }
        [Display(Name = "Start date of Aircraft Maintanance")]
        public DateTime? ScheduleDateOfAircraftArrival { get; set; }
        [Display(Name = "End  date of Aircraft Maintanance")]
        public DateTime? ScheduleDateOfAircraftDeparture { get; set; }

        #region "Display Properties"
        public string TypeOfMaintenanceDisplay { get { return TypeOfMaintenance.HasValue ? (TypeOfMaintenance.Value ? "Scheduled" : "Non Scheduled") : ""; } }
        #endregion
    }
}