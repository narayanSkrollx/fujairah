namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CranePermitInternalRequestseparate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CranePermitRequest", "IsInternal", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CranePermitRequest", "IsInternal");
        }
    }
}
