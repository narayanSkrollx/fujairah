﻿<%@ Page ValidateRequest="false" Language="C#" MasterPageFile="/umbraco/masterpages/umbracoPage.Master" AutoEventWireup="true" CodeBehind="editNewsletterTemplate.aspx.cs" Inherits="Designit.Umb.Newsletter.Custom.plugins.designit.newsletter.editNewsletterTemplate" %>
<%@ Register Namespace="umbraco.uicontrols" Assembly="controls" TagPrefix="umb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">

<umb:UmbracoPanel ID="Panel1" runat="server" Width="608px" Height="336px" hasMenu="true">
      <umb:Pane ID="Pane1" runat="server" Height="44px" Width="528px">
        <umb:PropertyPanel ID="pp_name" runat="server" Text="Name">
            <asp:TextBox ID="txtName" Width="350px" runat="server"></asp:TextBox>
        </umb:PropertyPanel>
        <umb:PropertyPanel id="pp_alias" runat="server" Text="Alias">
            <asp:TextBox ID="txtAlias" Width="350px" runat="server"></asp:TextBox>
        </umb:PropertyPanel>
        <umb:PropertyPanel id="pp_source" runat="server">
            <umb:CodeArea ID="editorSource" runat="server" AutoResize="true" OffSetX="37" OffSetY="54"/>
        </umb:PropertyPanel>
      </umb:Pane>
    </umb:UmbracoPanel>
  
<script type="text/javascript">
    function designTag() {
        var cp = 'designit.Newsletter:Item field="bodytext"';
        UmbEditor.Insert('\n<' + cp + '/>\n', '', '<%= editorSource.ClientID %>');
    }

    function designUnsubscribeTag() {
        var cp = 'a href="@unsubscribe"';
        UmbEditor.Insert('<' + cp + '>Click here to unsubscribe<' + '/' + 'a>', '', '<%= editorSource.ClientID %>');
}
   
</script>
 
</asp:Content>

