﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EService.Web.App_LocalResources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class CranePermitLocationResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CranePermitLocationResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("EService.Web.App_LocalResources.CranePermitLocationResource", typeof(CranePermitLocationResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email Address.
        /// </summary>
        public static string ApplicantEmail {
            get {
                return ResourceManager.GetString("ApplicantEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Applicant Email is required..
        /// </summary>
        public static string ApplicantEmailRequired {
            get {
                return ResourceManager.GetString("ApplicantEmailRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Applicant&apos;s Email must be a valid Email..
        /// </summary>
        public static string ApplicantEmailValidEmail {
            get {
                return ResourceManager.GetString("ApplicantEmailValidEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Applicant Name.
        /// </summary>
        public static string ApplicantName {
            get {
                return ResourceManager.GetString("ApplicantName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Applicant Name is required.
        /// </summary>
        public static string ApplicantNameRequired {
            get {
                return ResourceManager.GetString("ApplicantNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Application Date.
        /// </summary>
        public static string ApplicationDate {
            get {
                return ResourceManager.GetString("ApplicationDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Application Date must be selected..
        /// </summary>
        public static string ApplicationDateRequired {
            get {
                return ResourceManager.GetString("ApplicationDateRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company Name.
        /// </summary>
        public static string CompanyName {
            get {
                return ResourceManager.GetString("CompanyName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company Name is required..
        /// </summary>
        public static string CompanyNameRequired {
            get {
                return ResourceManager.GetString("CompanyNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contact Number.
        /// </summary>
        public static string ContactNumber {
            get {
                return ResourceManager.GetString("ContactNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contact Number is required..
        /// </summary>
        public static string ContactNumberRequired {
            get {
                return ResourceManager.GetString("ContactNumberRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Crane Operating Company.
        /// </summary>
        public static string CraneOperatingCompany {
            get {
                return ResourceManager.GetString("CraneOperatingCompany", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Crane Operating Company is required..
        /// </summary>
        public static string CraneOperatingCompnayRequired {
            get {
                return ResourceManager.GetString("CraneOperatingCompnayRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Crane Type.
        /// </summary>
        public static string CraneType {
            get {
                return ResourceManager.GetString("CraneType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Current Stage.
        /// </summary>
        public static string CurrentIndex {
            get {
                return ResourceManager.GetString("CurrentIndex", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Is New Application.
        /// </summary>
        public static string IsNewApplication {
            get {
                return ResourceManager.GetString("IsNewApplication", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Location of Crane.
        /// </summary>
        public static string LocationOfCrane {
            get {
                return ResourceManager.GetString("LocationOfCrane", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Max. Operating Height Above ground (in meter).
        /// </summary>
        public static string MaxOperatingHeight {
            get {
                return ResourceManager.GetString("MaxOperatingHeight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Max Operating Height must be selected..
        /// </summary>
        public static string MaxOperatingHeightRequired {
            get {
                return ResourceManager.GetString("MaxOperatingHeightRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Northings and Eastings.
        /// </summary>
        public static string NorthingsEastings {
            get {
                return ResourceManager.GetString("NorthingsEastings", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Operation Date must be selected..
        /// </summary>
        public static string OperationDateRequired {
            get {
                return ResourceManager.GetString("OperationDateRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date of Operation (End).
        /// </summary>
        public static string OperationEndDate {
            get {
                return ResourceManager.GetString("OperationEndDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Timings of Operation (End).
        /// </summary>
        public static string OperationEndTime {
            get {
                return ResourceManager.GetString("OperationEndTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date of Operation (Start).
        /// </summary>
        public static string OperationStartDate {
            get {
                return ResourceManager.GetString("OperationStartDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Timings of Operation (Start).
        /// </summary>
        public static string OperationStartTime {
            get {
                return ResourceManager.GetString("OperationStartTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Operation Timing is required..
        /// </summary>
        public static string OperationTimingRequired {
            get {
                return ResourceManager.GetString("OperationTimingRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contact Number.
        /// </summary>
        public static string OperatorContactNumber {
            get {
                return ResourceManager.GetString("OperatorContactNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Operator Contact Number is required..
        /// </summary>
        public static string OperatorContactNumberRequired {
            get {
                return ResourceManager.GetString("OperatorContactNumberRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Crane Operator Name.
        /// </summary>
        public static string OperatorName {
            get {
                return ResourceManager.GetString("OperatorName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Operator Name is required..
        /// </summary>
        public static string OperatorNameRequired {
            get {
                return ResourceManager.GetString("OperatorNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Position.
        /// </summary>
        public static string Position {
            get {
                return ResourceManager.GetString("Position", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Previous permit numbers (if any).
        /// </summary>
        public static string PreviousPermitNumbers {
            get {
                return ResourceManager.GetString("PreviousPermitNumbers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Purpose of Use.
        /// </summary>
        public static string PurposeOfUser {
            get {
                return ResourceManager.GetString("PurposeOfUser", resourceCulture);
            }
        }
    }
}
