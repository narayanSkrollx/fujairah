﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EService.Service.Model
{
    public class GroundHandlingRequestServiceModel: EServiceBaseModal
    {
        public GroundHandlingRequestServiceModel()
        {
            GroundHandlingArcraftTypes = new List<GroundHandlingArcraftTypeServiceModel>();
        }
        public int UserServiceRequestMasterId { get; set; }
        public int? CurrentIndex { get; set; }
        #region "Project information"
        public DateTime? InboundDateTimeUTC { get; set; }
        public string From { get; set; }
        public DateTime? ETAUTC { get; set; }
        public DateTime? OutboundDateTimeUTC { get; set; }
        public string To { get; set; }
        public DateTime? ETDUTC { get; set; }
        #endregion
        public double? Weight { get; set; }
        public string AircraftRegistration { get; set; }
        public bool? RequestWBCalculation { get; set; }
        public bool? LoadingInstruction { get; set; }
        public bool? METOrATSDelivery { get; set; }
        public int? NoOfDepartingPassengers { get; set; }
        public int? NoOfArrivingPassengers { get; set; }
        public bool? PermittedToAddGood { get; set; }
        public bool? HasSpecialPermission { get; set; }
        #region "Cargo Off Loading"
        /// <summary>
        /// ULR Or Bulk
        /// </summary>
        public bool? CargoOffLoadingType { get; set; }
        public int? CoLNoOfULD { get; set; }
        public double? CoLULDWeight { get; set; }
        public string CoLTypeOfDG { get; set; }
        public double? CoLQuantityOfDG { get; set; }
        public string CoLOtherSpecialCargo { get; set; }
        #endregion

        #region "Cargo Loading"
        public bool? CargoLoadingType { get; set; }
        public int? CLNoOfULD { get; set; }
        public int? CLULDWeight { get; set; }
        public string CLTypeOfDG { get; set; }
        public double? CLQuantityDG { get; set; }
        public double? CLDGWeight { get; set; }
        public string CLOtherSpecialCargo { get; set; }

        #endregion


        #region "Applicant Info"
        public string CompanyName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }
        public string Fax { get; set; }
        public string SITATEX { get; set; }

        #endregion
        public AircraftInteriorCleaningServiceModel AircraftCleaning { get; set; }

        public List<GroundHandlingArcraftTypeServiceModel> GroundHandlingArcraftTypes { get; set; }
    }

    public class AircraftInteriorCleaningServiceModel
    {
        public int UserServiceRequestMasterId { get; set; }
        [Display(Name = "Aicraft Fumigation Required")]
        public bool FumigationRequired { get; set; }
        [Display(Name = "Aicraft Washing Required")]
        public bool WasingRequired { get; set; }
        [StringLength(1024, ErrorMessage = "PowerType length cannot be greater than 1024")]
        public string PowerType { get; set; }
        public bool Pushback { get; set; }
        [Display(Name = "Passenger Stairs")]
        public bool PassengerStairs { get; set; }
        [Display(Name = "Toilet Service")]
        public bool ToiletService { get; set; }
        [Display(Name = "Portable Water")]
        public bool PortableWater { get; set; }
        [StringLength(1024, ErrorMessage = "ASUHours length cannot be greater than 1024")]
        [Display(Name = "ASU Hours")]
        public string ASUHours { get; set; }
        [Display(Name = "Additional Platform for CRJ")]
        public bool AdditionalPlatformCRJ { get; set; }
        [Display(Name = "Security Surveilance on the ramp hours")]
        public bool SecuritySurveillanceForRAmpHours { get; set; }
        [Display(Name = "Hotel Booking")]
        public bool HotelBooking { get; set; }
        [Display(Name = "Standby Fire fighting")]
        public bool StandbyFireFighting { get; set; }
        [Display(Name = "Additional Manpower")]
        public bool AdditionalManpower { get; set; }
        [StringLength(1024, ErrorMessage = "AdditionalRequirements length cannot be greater than 1024")]
        [Display(Name = "Additional Requirements")]
        public string AdditionalRequirements { get; set; }


    }

    public class GroundHandlingArcraftTypeServiceModel
    {
        public bool IsSelected { get; set; }
        public int AircraftTypeId { get; set; }
        public string Description { get; set; }
        public bool IsOther { get; set; }
    }
}