namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EASPNoObjectionLetter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExitOfAircraftSparePartsRequest", "HasNoObjectionLetter", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ExitOfAircraftSparePartsRequest", "HasNoObjectionLetter");
        }
    }
}
