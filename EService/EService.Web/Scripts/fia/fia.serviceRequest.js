﻿$(function () {
    fia.CreateNamespace("serviceRequest");

    (function (context) {
        context.Title = 'Service Request';
        context.ViewModel = {}

        context.Mapping = {
            create: function (options) {
                var vm = ko.mapping.fromJS(options.data);

                vm.SearchClick = function () {
                    context.Search();
                }

                vm.ViewDetails = function (item) {
                    var jsItem = ko.toJS(item);
                    window.location = '/ServiceRequest/Detail?id=' + jsItem.UserServiceRequestMasterId;
                }

                vm.Edit = function (item) {
                    var jsItem = ko.toJS(item);
                    window.location = `/ServiceRequest/Edit?id=${jsItem.UserServiceRequestMasterId}&serviceId=${jsItem.ServiceId}`;
                }


                return vm;
            }
        }

        context.Search = function () {
            var searchModel = ko.mapping.toJS(context.ViewModel.ServiceInfoVM.SearchModel);

            searchModel.DateFrom = moment(searchModel.DateFrom).isValid() ? moment(searchModel.DateFrom).format('YYYY-MM-DD') : null;
            searchModel.DateTo = moment(searchModel.DateTo).isValid() ? moment(searchModel.DateTo).format('YYYY-MM-DD') : null;
            delete searchModel.ServiceStatusList;

            ajaxRequest('/ServiceRequest/Search/', 'POST',
                {
                    data: { searchModel: searchModel },
                    enableLadda: true,
                    targetLaddaElement: '[data-role=search]'
                }, function (response) {
                    if (response.IsSuccess) {
                        ko.mapping.fromJS(response.Data, {}, context.ViewModel.ServiceInfoVM.Records);
                    } else {
                        showMessage(context.Title, response.Message, 'error');
                    }
                });
        }

        context.Initialize = function () {
            ajaxRequest('/ServiceRequest/Initialize/', 'GET', {}, function (response) {
                if (response.IsSuccess) {
                    if (!ko.dataFor($('#mainContent')[0])) {
                        context.ViewModel.ServiceInfoVM = ko.mapping.fromJS(response.Data, context.Mapping);

                        ko.applyBindings(context.ViewModel.ServiceInfoVM, $('#mainContent')[0]);
                    } else {
                        ko.mapping.fromJS(response.Data, {}, context.ViewModel.ServiceInfoVM);
                    }
                } else {
                    showMessage(context.Title, response.Message, 'error');
                }
            });
        }

        context.ProceedToPayment = function (userServiceRequestId) {
            console.log(userServiceRequestId);
            ajaxRequest('/ServiceRequest/ProceedToPayment/', 'POST', {
                data: { id: userServiceRequestId }
            }, function (response) {
                if (response.IsSuccess) {
                    window.location = '/Payment/Index';
                }
                else {
                    showMessage(context.Title, response.Message, 'error');
                }
            });
        }

        context.InitializeDetail = function () {
            ajaxRequest('/ServiceRequest/InitializeDetail/', 'GET', {}, function (response) {
                if (response.IsSuccess) {
                    $('#detailContent').html(response.Data.Html);
                    if (context.ViewModel.Detail == null) {
                        context.ViewModel.Detail = ko.mapping.fromJS(response.Data.Data);
                        context.ViewModel.Detail.History = ko.mapping.fromJS(response.Data.History);
                        context.ViewModel.Detail.ServiceInfo = ko.mapping.fromJS(response.Data.ServiceInfo);//
                        context.ViewModel.Detail.PaymentInfo = ko.mapping.fromJS(response.Data.PaymentInfo);//PaymentInfo
                        context.ViewModel.Detail.ProceedToPaymentClick = function (id) {

                            context.ProceedToPayment(id);
                        }
                        context.ViewModel.Detail.GotoReceiptList = function (id) {

                            window.location = '/ServiceRequest/Receipt/' + id;
                        }
                        ko.applyBindings(context.ViewModel.Detail, $('#uploadedFileContent')[0]);//uploadedFileContent
                        ko.applyBindings(context.ViewModel.Detail, $('[data-role=navigation-up]')[0]);//uploadedFileContent
                        ko.applyBindings(context.ViewModel.Detail, $('[data-role=navigation-down]')[0]); //historyContent
                        ko.applyBindings(context.ViewModel.Detail, $('#historyContent')[0]); //historyContent
                        ko.applyBindings(context.ViewModel.Detail, $('#currentStatusContent')[0]); //Current Status Content
                        ko.applyBindings(context.ViewModel.Detail, $('#paymentInfoContent')[0]); //Current Status Content
                        ko.applyBindings(context.ViewModel.Detail, $('#receiptInfoContent')[0]); //Current Status Content
                    } else {
                        ko.mapping.fromJS(response.Data.Data, {}, context.ViewModel.Detail);
                    }
                } else {
                    showMessage(context.Title, response.Message, 'error');
                }
            });
        }

    })(fia.serviceRequest);
});