using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class AirportLandingConfiguration : EntityTypeConfiguration<AirportLanding>
    {
        public AirportLandingConfiguration()
        {
            ToTable("AirportLanding");

            HasKey(o => o.Id);
            Property(o => o.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(o => o.Description).IsRequired().HasMaxLength(1024);
            Property(o => o.Active).IsRequired();
        }
    }

    public class UserAttachmentDataConfiguration : EntityTypeConfiguration<UserAttachmentData>
    {
        public UserAttachmentDataConfiguration()
        {
            ToTable("UserAttachmentData");

            HasKey(o => o.UserAttachmentId);

            Property(o => o.UserAttachmentId).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(o => o.FileContents).IsRequired().HasColumnType("varbinary(max)");
            
            HasRequired(o=>o.UserAttachment).WithRequiredDependent(o=>o.UserAttachmentData).WillCascadeOnDelete(false);
        }
    }
}