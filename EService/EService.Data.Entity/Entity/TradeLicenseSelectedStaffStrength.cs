﻿using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class TradeLicenseSelectedStaffStrength
    {
        public int TradeLicenseSelectedStaffStrengthId { get; set; }
        public int TradeLicenseRequestId { get; set; }
        public int StaffStrengthId { get; set; }

        [JsonIgnore]
        public virtual StaffStrength StaffStrength { get; set; }
        [JsonIgnore]
        public virtual TradeLicenseRequest TradeLicenseRequest { get; set; }
    }
}