﻿using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Web.Controllers.Base;
using EService.Web.Helper;
using EService.Web.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace EService.Web.Controllers
{
    public class ProfileController : ExternalUserBaseController
    {

        private LookupService lookupService;
        private UserService userService;
        private ServiceContext serviceContext;
        private EmailService emailService;

        public ProfileController()
        {
            serviceContext = new ServiceContext();
            lookupService = new LookupService();
            userService = new UserService(context: serviceContext);
            emailService = new EmailService(serviceContext);
        }
        public ActionResult index()
        {
            return View();
        }

        public JsonResult Initialize()
        {
            var result = new JsonResponseObject();
            try
            {
                var userInfo = userService.Find(o => o.UserId == UserProfile.UserId);

                var model = new UserRegisterViewModel()
                {
                    SecurityQuestions = lookupService.GetList<SecurityQuestion>().ToList(),
                    CompanyCategories = lookupService.GetList<CompanyCategory>().ToList(),
                    Countries = CountryHelper.GetListOfCountries().Select(o=> new Data.Entity.LookupItem { Description=o}).ToList(),
                    Answer = userInfo.UserSecruityQuestion.Answer,
                    City = userInfo.UserCompanyProfile.City,
                    CompanyAddress = userInfo.UserCompanyProfile.CompanyAddress,
                    CompanyCategoryId = userInfo.UserCompanyProfile.CompanyCategoryId,
                    CompanyName = userInfo.UserCompanyProfile.CompanyName,
                    ContactPersonMobile = userInfo.UserContactPerson.ContactPersonMobile,
                    ContactPersonName = userInfo.UserContactPerson.ContactPersonName,
                    Country = userInfo.UserCompanyProfile.Country,
                    Email = userInfo.Email,
                    EmiratesId = userInfo.UserContactPerson.EmiratesId,
                    Fax = userInfo.UserCompanyProfile.Fax,
                    FirstName = userInfo.FirstName,
                    LastName = userInfo.LastName,
                    Phone = userInfo.UserCompanyProfile.Phone,
                    POBox = userInfo.UserCompanyProfile.POBox,
                    SecurityQuestionId = userInfo.UserSecruityQuestion.SecurityQuestionId,
                    VATRegistrationNo = userInfo.UserCompanyProfile.VATRegistrationNumber,


                };

                result.Data = model;
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "Error while obtaining model: " + ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Index(UserRegisterViewModel model)
        {

            var result = new JsonResponseObject();
            try
            {
                ModelState.Remove("model.ConfirmPassword");
                ModelState.Remove("model.PasswordHash");
                ModelState.Remove("model.UserName");
                if (ModelState.IsValid)
                {
                    var userInfo = userService.Find(o => o.UserId == UserProfile.UserId);// model, out user, out message);
                    if (userInfo == null)
                    {
                        result.IsSuccess = false;
                        result.Message = "Invalid User information.";
                        //send email
                    }
                    else
                    {
                        userInfo.UserSecruityQuestion.Answer = model.Answer;
                        userInfo.UserCompanyProfile.City = model.City;
                        userInfo.UserCompanyProfile.CompanyAddress = model.CompanyAddress;
                        userInfo.UserCompanyProfile.CompanyCategoryId = model.CompanyCategoryId;
                        userInfo.UserCompanyProfile.CompanyName = model.CompanyName;
                        userInfo.UserContactPerson.ContactPersonMobile = model.ContactPersonMobile ?? 0;
                        userInfo.UserContactPerson.ContactPersonName = model.ContactPersonName;
                        userInfo.UserCompanyProfile.Country = model.Country;
                        userInfo.Email = model.Email;
                        userInfo.UserContactPerson.EmiratesId = model.EmiratesId;
                        userInfo.UserCompanyProfile.Fax = model.Fax;
                        userInfo.FirstName = model.FirstName;
                        userInfo.LastName = model.LastName;
                        userInfo.UserCompanyProfile.Phone = model.Phone;
                        userInfo.UserCompanyProfile.POBox = model.POBox;
                        userInfo.UserSecruityQuestion.SecurityQuestionId = model.SecurityQuestionId;
                        userInfo.UserCompanyProfile.VATRegistrationNumber = model.VATRegistrationNo;

                        serviceContext.Commit();

                        result.IsSuccess = true;
                        result.Message = "Profile Updated successfully.";
                    }
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = ModelState.GetModelStateError();
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while registering for the user.";
            }
            return Json(result);
        }
    }
}