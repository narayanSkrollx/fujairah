namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BHPlanUserAttachmentTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BuildingHeightPlanningApproval",
                c => new
                    {
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        Remarks = c.String(maxLength: 1024),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByUserId = c.Int(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserServiceRequestMasterId)
                .ForeignKey("dbo.BuildingHeightRequest", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId);
            
            CreateTable(
                "dbo.BuildingHeightPlanningApprovalAttachment",
                c => new
                    {
                        BuildingHeightPlanningApprovalAttachmentId = c.Int(nullable: false, identity: true),
                        UserServiceRequestMasterId = c.Int(nullable: false),
                        UserAttachmentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BuildingHeightPlanningApprovalAttachmentId)
                .ForeignKey("dbo.UserAttachment", t => t.UserAttachmentId)
                .ForeignKey("dbo.BuildingHeightPlanningApproval", t => t.UserServiceRequestMasterId)
                .Index(t => t.UserServiceRequestMasterId)
                .Index(t => t.UserAttachmentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BuildingHeightPlanningApproval", "UserServiceRequestMasterId", "dbo.BuildingHeightRequest");
            DropForeignKey("dbo.BuildingHeightPlanningApprovalAttachment", "UserServiceRequestMasterId", "dbo.BuildingHeightPlanningApproval");
            DropForeignKey("dbo.BuildingHeightPlanningApprovalAttachment", "UserAttachmentId", "dbo.UserAttachment");
            DropIndex("dbo.BuildingHeightPlanningApprovalAttachment", new[] { "UserAttachmentId" });
            DropIndex("dbo.BuildingHeightPlanningApprovalAttachment", new[] { "UserServiceRequestMasterId" });
            DropIndex("dbo.BuildingHeightPlanningApproval", new[] { "UserServiceRequestMasterId" });
            DropTable("dbo.BuildingHeightPlanningApprovalAttachment");
            DropTable("dbo.BuildingHeightPlanningApproval");
        }
    }
}
