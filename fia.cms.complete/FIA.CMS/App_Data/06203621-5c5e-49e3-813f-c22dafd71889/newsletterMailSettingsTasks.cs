﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using umbraco.BusinessLogic;

namespace Designit.Umb.Newsletter.Bll
{
    public class newsletterMailSettingsTasks : umbraco.interfaces.ITaskReturnUrl
    {

        
        #region ITaskReturnUrl Members

        private string returnUrl;

        public string ReturnUrl
        {
            get { return returnUrl; }
            
        }

        #endregion

        #region ITask Members

        private string alias;
        private int parentID;
        private int typeID;
        private int userID;

        public string Alias
        {
            get
            {
                return alias;
            }
            set
            {
                alias = value;
            }
        }

        public int ParentID
        {
            get
            {
                return parentID;
            }
            set
            {
                parentID = value;
            }
        }


        public int TypeID
        {
            get
            {
                return typeID;
            }
            set
            {
                typeID = value;
            }
        }

        public int UserId
        {
            set { userID = value; }
        }

        public bool Delete()
        {

            return true;
        }


        public bool Save()
        {
            return true;

        }



        #endregion

    }
}

