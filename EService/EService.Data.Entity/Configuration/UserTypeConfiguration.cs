using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class UserTypeConfiguration : EntityTypeConfiguration<UserType>
    {
        public UserTypeConfiguration()
        {
            ToTable("UserType");

            HasKey(o => o.UserTypeId);

            Property(o => o.UserTypeId).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(o => o.UserTypeName).IsRequired().HasMaxLength(100);

        }
    }
}