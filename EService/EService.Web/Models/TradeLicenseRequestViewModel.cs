﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EService.Data.Entity.Entity;
using EService.Web.App_LocalResources;

namespace EService.Web.Models
{
    public class TradeLicenseRequestViewModel : EServiceCreateBaseViewModel
    {
        public int UserServiceRequestMasterId { get; set; }

        #region "Details of the applicant who wants to register the company under DCA"
        [Required(ErrorMessage = "Applicant Name is required")]
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "ApplicantName")]
        public string ApplicantName { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "PresentSponsorInUAE")]
        public string PresentSponsorInUAE { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "AddressOfSponsor")]
        public string AddressOfSponser { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "ApplicantOfficeNumber")]
        public string ApplicantOfficeNumber { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "ApplicantResidenceNumber")]
        public string ApplicantResidenceNumber { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "ApplicantMobileNumber")]
        public string ApplicantMobileNumber { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "ApplicantFaxNumber")]
        public string ApplicantFaxNumber { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "ApplicantEmailNumber")]
        public string ApplicantEmailNumber { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "ApplicantCountry")]
        public string ApplicantCountry { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "MailingAddress")]
        public string MailingAddress { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "ApplicationDate")]
        public DateTime? ApplicationDate { get; set; }

        #endregion

        #region "Details of the proposed Company to sponsored under DCA"

        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "CompanyPreferredName")]
        public string CompanyPreferredName { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "CompanyNameOption1")]
        public string CompanyNameOption1 { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "CompanyNameOption2")]
        public string CompanyNameOption2 { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "CompanyNameOption3")]
        public string CompanyNameOption3 { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "BusinessActivities1")]
        public string BusinessActivities1 { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "BusinessActivities2")]
        public string BusinessActivities2 { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "BusinessActivities3")]
        public string BusinessActivities3 { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "Remarks")]
        public string Remarks { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "ExpectedAnnualBusinessTrunoverAmount")]
        public double? ExpectedAnnualBusinessTrunoverAmount { get; set; }
        public int? ExpectedAnnualBusinessTrunoverCurrencyId { get; set; }
        [Display(ResourceType = typeof (TradingLicenseRequestResource), Name = "TotalStaffStrengthRequired")]
        public string TotalStaffStrengthRequired { get; set; }

        public List<SelectListModel> SelectedStaffStrengths { get; set; }

        #endregion
        public IEnumerable<SelectListModel> Currencies { get; set; }

        #region "Display Properties"

        public string CurrencyDisplay { get; set; }
        public string StaffStrengthDisplay { get; set; }

        #endregion
    }

    //public class SelectedStaffStrengthViewmodel
    //{
    //    public int TradeLicenseSelectedStaffStrengthId { get; set; }
    //    public int TradeLicenseRequestId { get; set; }
    //    public int StaffStrengthId { get; set; }
    //    public bool IsSelected { get; set; }
    //    public string StaffStrengthName { get; set; }
    //}
}