using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class OHLStructureCoordinateAndHeight
    {
        public int OHLStructureCoordinateAndHeightId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public string StructureReferenceNumber { get; set; }
        public string Easting { get; set; }
        public string Northing { get; set; }
        public double GroundHeight { get; set; }
        public double StructureHeight { get; set; }

        [JsonIgnore]
        public virtual BuildingHeightRequest BuildingHeightRequest { get; set; }
    }
}