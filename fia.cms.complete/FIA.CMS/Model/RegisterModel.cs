﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace FIA.CMS.Model
{
    public class RegisterModel
    {
        [Required(ErrorMessage="First Name is required")]
        [StringLength(255)]
        public string FirstName { get; set; }
        [Required(ErrorMessage="Last Name is required")]
        [StringLength(255)]
        public string LastName { get; set; }
        [Required(ErrorMessage="Email is required")]
        [StringLength(255)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [StringLength(100, ErrorMessage ="The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [Display(Name = "Confirm  password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Remote("CheckUserName", "Manage")]
        [Required(ErrorMessage = "User Name is required.")]
        [StringLength(255)]
        public string UserName { get; set; }
        public string YearOfExperience { get; set; }
        public string Overview { get; set; }
        public string Contact { get; set; }
        [Required(ErrorMessage = "CV must be attached")]
        public HttpPostedFileBase File { get; set; }
    }
}