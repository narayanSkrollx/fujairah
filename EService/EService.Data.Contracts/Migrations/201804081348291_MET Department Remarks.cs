namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class METDepartmentRemarks : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MeteorologicalDataRequestService", "METDepartmentRemrks", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MeteorologicalDataRequestService", "METDepartmentRemrks");
        }
    }
}
