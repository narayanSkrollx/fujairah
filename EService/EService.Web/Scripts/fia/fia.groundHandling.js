﻿$(function () {
    fia.CreateNamespace("groundHandling");

    (function (context) {
        context.Title = 'Ground Handling Request';
        context.createForm = '#groundHandlingRequestForm';
        context.ServiceId = fia.ServiceEnum.GroundHandlingRequest;
        context.CreateUrl = '/GroundHandlingRequest/Create/'

        context.SaveRequest = function (isDraft) {
            if (!$(context.createForm).valid()) {
                return false;
            }
            var model = { model: ko.mapping.toJS(context.ViewModel.AddNewModel), isDraft: isDraft };
            var lightModel = JSON.stringify(model);
            var submitcontrol = isDraft ? 'draft' : 'save';
            var ajaxOption = {
                dataType: "JSON",
                contentType: "application/json; charset=UTF-8",
                data: lightModel,
                enableLadda: true,
                targetLaddaElement: `[data-role=${submitcontrol}]`,
            };
            ajaxRequest(context.CreateUrl, 'POST', ajaxOption, function (resp) {
                if (resp.IsSuccess) {
                    showMessage(context.Title, resp.Message, 'success', function () {
                        //
                        window.location = '/ServiceRequest/Index?serviceId=' + context.ServiceId;
                    });
                } else {
                    showMessage(context.Title, resp.Message, 'error');
                }
            });
        }

        context.ViewModel = {

            SaveRequest: function () {
                context.SaveRequest(false)
            },
            SaveDraftRequest: function () {
                context.SaveRequest(true)
            },
            SetUserInfo: function () {
                context.SetUserInfo();
            },
            ViewDetails: function (item) {
                window.location = context.DetailsUrl + '?id=' + item.UserServiceRequestMasterId();
            },
            CreateRenderComplete: function () {
                console.log('Render Complete')
                $(context.createForm).validate({
                    rules: {
                        DateOfApplication: { required: true,  },
                        CompanyName: { required: true, maxlength: 1000 },
                        FirstName: { required: true, maxlength: 1000 },
                        LastName: { required: true, maxlength: 1000 },
                        Address: { required: true, maxlength: 255 },
                        Phone: { required: true, maxlength: 20, regex: phoneNoRegex },
                        Email: { required: true, email: true, maxlength: 225 },
                        Fax: { maxlength: 225 },
                        SITATEX: { maxlength: 225 },

                        InboundDateTimeUTC: { required: true, },
                        From: { required: true, maxlength: 1000 },
                        ETAUTC: { required: true,  },
                        OutboundDateTimeUTC: { required: true, },
                        To: { required: true, maxlength: 1000 },
                        ETDUTC: { required: true,  },

                    },
                    messages: {

                        DateOfApplication: { required: 'Application Date is required.', date: 'Application Date must be a valid date', },
                        FirstName: { required: 'First Name is required', maxlength: 'First Name length cannot be greater than 1000' },
                        CompanyName: { required: 'Company Name is required', maxlength: 'Company Name length cannot be greater than 1000' },
                        LastName: { required: 'Last Name is required', maxlength: 'Last Name length cannot be greater than 1000' },
                        Address: { required: 'Address is required', maxlength: 'Address Length cannot be greater than 255' },
                        Phone: { required: 'Phone is required', maxlength: 'Phone max length cannot be greater than 20' },
                        Email: { required: 'Email is required.', email: 'Email must be a valid email address.', maxlength: 'Email Address max Length cannot be greater than 255.' },
                        Fax: { maxlength: 'Fax max length cannot be greater than 255' },
                        SITATEX: { maxlength: 'SITATEX length cannot be greater than 255' },

                        InboundDateTimeUTC: { required: 'Inbound Date UTC is required.', date: 'Inbound Date must be a valid date' },
                        From: { required: 'From is required', maxlength: 'From length cannot be grreater than 100' },
                        ETAUTC: { required: 'ETA UTC is required', date: 'ETA UTC must be valid date.' },
                        OutboundDateTimeUTC: { required: 'Outbound date is required,', date: 'Outbound Date must be a valid date.' },
                        To: { required: 'To is required', maxlength: 'To length cannot be greater than 1000' },
                        ETDUTC: { required: 'ETD UTC is required.', date: 'ETD UTC must be a valid date.' },
                    }
                });
            }
        };

        context.SetUserInfo = function () {
            var addNewModel = context.ViewModel.AddNewModel;
            var userInfo = context.ViewModel.AddNewModel.UserInfo;
            if (addNewModel != null && userInfo != null) {
                addNewModel.FirstName(userInfo.FirstName());
                addNewModel.LastName(userInfo.LastName());
                addNewModel.Address(userInfo.Address());
                addNewModel.Phone(userInfo.Phone());
                addNewModel.Email(userInfo.Email());
                addNewModel.Fax(userInfo.Fax());
                addNewModel.CompanyName(userInfo.CompanyName());
            }
        }

        context.InitializeCreate = function () {
            var masterId = parseInt($("#CurrentUserServiceRequestMasterId.forKnockout").val());
            context.ViewModel.IsCreate = Number.isInteger(masterId);
            var id = context.ViewModel.IsCreate ? parseInt(masterId) : null;
            ajaxRequest('/GroundHandlingRequest/InitializeCreate', 'GET', {
                data: { id: id }
            }, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#createContent')[0])) {
                        context.ViewModel.AddNewModel = ko.mapping.fromJS(resp.Data);
                        context.ViewModel.RemoveUploadedFile = function (item) {
                            item.ObjectState(2);
                        }
                        ko.applyBindings(context.ViewModel, $('#createContent')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel.AddNewModel);
                    }
                    //validation

                    //init file uploader
                    fia.fileinputhelper.InitializeFileUpload('#kv-explorer', fia.ServiceEnum.AircraftDismantlingRequest);
                    fia.fileinputhelper.UploadCompleteCallback = function (response) {
                        context.ViewModel.AddNewModel.UploadedFiles.push(response.Data);
                    }
                } else {
                    console.log(resp.Message);
                }
            });
        };

    })(fia.groundHandling);
});