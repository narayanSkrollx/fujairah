using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EService.Data.Entity.Configuration;
using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Service.Logic.Enum;
using EService.Service.Model;
using EService.Web.Controllers.Base;
using EService.Web.Helper;
using EService.Web.Models;
using EService.Web.Models.ServiceRequestOperation;
using Nelibur.ObjectMapper;
using EService.Core;
using EService.Web.Helper.EmailSender;

namespace EService.Web.Controllers
{
    public class GroundHandlingRequestController : ServiceRequestBaseController
    {
        private AircraftMaintenanceRequestService aircraftMaintenanceRequestService;
        private ServiceApprovalStageService serviceApprovalStageService;
        private UserServiceRequestMasterService userRequestService;
        private GroundHandlingRequestService groundHandlingRequestService;
        private ServiceInfoService serviceInfoService;
        private AircraftTypeService aircraftTypeService;
        private UserService userService;

        public GroundHandlingRequestController()
        {
            serviceApprovalStageService = new ServiceApprovalStageService(serviceContext);
            userRequestService = new UserServiceRequestMasterService(serviceContext);
            aircraftMaintenanceRequestService = new AircraftMaintenanceRequestService(serviceContext);
            groundHandlingRequestService = new GroundHandlingRequestService(serviceContext);
            serviceInfoService = new ServiceInfoService(serviceContext);
            aircraftTypeService = new AircraftTypeService(serviceContext);
            userService = new UserService(serviceContext);
        }

        public override ServiceEnum Service => ServiceEnum.GroundHandlingRequest;

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(GroundHandlingRequestViewModel model, bool isDraft = false)
        {
            var result = new JsonResponseObject();
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.UserServiceRequestMasterId > 0)
                    {
                        var existingGroundHandlingRequest = groundHandlingRequestService.Find(o => !o.IsDeleted && o.UserServiceRequestMasterId == model.UserServiceRequestMasterId
                        && o.UserServiceRequestMaster.IsDraft
                        && o.UserServiceRequestMaster.UserId == UserProfile.UserId
                        );
                        if (existingGroundHandlingRequest != null)
                        {
                            var saveModel = MapViewModelToContract(model);
                            var attachmentList = new List<UserServiceRequestAttachment>();
                            #region "Uploaded Files Edit"
                            if (model.EditUploadedFiles != null && model.EditUploadedFiles.Any())
                            {
                                attachmentList.AddRange(model.EditUploadedFiles.Where(o => o.ObjectState != ObjectState.Deleted).Select(o => new UserServiceRequestAttachment { UserAttachmentId = o.Id }));

                            }
                            if (model.UploadedFiles != null && model.UploadedFiles.Any())
                            {
                                attachmentList.AddRange(model.UploadedFiles.Select(o => new UserServiceRequestAttachment { UserAttachmentId = o.Id }));

                            }

                            #endregion

                            ResponseObject updateResult = groundHandlingRequestService.Update(saveModel, UserProfile.UserId, isDraft, attachmentList);

                            result.IsSuccess = updateResult.IsSuccess;
                            result.Message = updateResult.Message;
                            if (!isDraft && updateResult.IsSuccess)
                            {
                                //TriggerEmail(FIAEmailType.New, model.UserServiceRequestMasterId);
                                EmailSenderHelper.SendNewRequestEmail(ControllerContext, model.UserServiceRequestMasterId);
                            }
                        }
                        else
                        {
                            result.IsSuccess = false;
                            result.Message = "Invalid User Service Request information provided for edit.";
                        }
                    }
                    else
                    {
                        var serviceRequest = SaveGroundHandlingRequest(model, isDraft);
                        if (!isDraft)
                        {
                            //TriggerEmail(FIAEmailType.New, serviceRequest.UserServiceRequestMasterId);
                            EmailSenderHelper.SendNewRequestEmail(ControllerContext, serviceRequest.UserServiceRequestMasterId);
                        }
                        result.IsSuccess = true;
                        result.Message = "Ground Handling Request saved successfully";
                    }
                }
                else
                {
                    result.Message = "Server Validation Error: " + ModelState.GetModelStateError();
                    result.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving Ground Handling Request.";
            }
            return Json(result);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.UserServiceRequestMasterId = id;
            return View("Create");
        }

        public JsonResult InitializeCreate(int? id)
        {
            try
            {
                var model = new GroundHandlingRequestViewModel();
                model.GroundHandlingArcraftTypes = aircraftTypeService.FindAllAsNoTracking(o => o.Active)
                    .Select(o => new GroundHandlingArcraftTypeViewModel()
                    {
                        IsOther = o.IsOther,
                        Description = o.Description,
                        IsSelected = false,
                        AircraftTypeId = o.Id
                    }).ToList();

                var reapplyData = TempData[Constants.ServiceRequestReapplyContentTempDataKey] as ServiceReapplyTempDataViewModel;
                if (reapplyData != null || id.HasValue)
                {
                    var existingId = reapplyData?.UserServiceRequestMasterId ?? id.Value;
                    var userServiceRequest = userRequestService.Find(o => o.UserServiceRequestMasterId == existingId &&
                                                                          !o.IsDeleted && o.UserId == UserProfile.UserId && o.ServiceId == ServiceId && (!id.HasValue || o.IsDraft));

                    if (userServiceRequest == null || userServiceRequest.GroundHandlingRequest == null)
                    {
                        return Json(new JsonResponseObject() { IsSuccess = false, Message = $"Invalid User Service Request for {(reapplyData != null ? "Reapply" : "Edit")}." });
                    }
                    else
                    {
                        var groundHandlingRequest = userServiceRequest.GroundHandlingRequest;


                        TinyMapper.Bind<GroundHandlingRequest, GroundHandlingRequestViewModel>();

                        var groundHandlingRequestViewModel = TinyMapper.Map<GroundHandlingRequestViewModel>(groundHandlingRequest);

                        groundHandlingRequestViewModel.ParentUserServiceRequestMasterid = reapplyData?.UserServiceRequestMasterId;
                        groundHandlingRequestViewModel.UserServiceRequestMasterId = reapplyData != null ? 0 : id ?? 0;
                        groundHandlingRequestViewModel.GroundHandlingArcraftTypes = (from item in model.GroundHandlingArcraftTypes
                                                                                     join type in groundHandlingRequest.AircraftTypes on item.AircraftTypeId equals type.AircraftTypeId
                                                                                    into leftJoinType
                                                                                     from defaultType in leftJoinType.DefaultIfEmpty()
                                                                                     select new GroundHandlingArcraftTypeViewModel
                                                                                     {
                                                                                         AircraftTypeId = item.AircraftTypeId,
                                                                                         Description = item.Description,
                                                                                         IsOther = item.IsOther,
                                                                                         IsSelected = defaultType != null
                                                                                     }).ToList();

                        model = groundHandlingRequestViewModel;
                    }
                }
                model.UserInfo = userService.GetUserBasicInfo(UserProfile.UserId).Data;
                model.EditUploadedFiles = GetUploadedFiles(reapplyData?.UserServiceRequestMasterId ?? (id ?? 0));

                return Json(new JsonResponseObject() { IsSuccess = true, Data = model }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return Json(new JsonResponseObject() { IsSuccess = false, Message = "Error" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Reapply()
        {
            return View("Create");
        }

        #region "Private Methods"

        //public AircraftMaintenanceRequestViewModel GetAircraftMaintenanceRequest(int id)
        //{
        //    var contract = aircraftMaintenanceRequestService.Find(x => x.UserServiceRequestMasterId == id);
        //    var model = MapContractToViewModel(contract);
        //    return model;
        //}

        public UserServiceRequestMaster SaveGroundHandlingRequest(GroundHandlingRequestViewModel model, bool isDraft = false)
        {
            var serviceFlowStages = serviceApprovalStageService.FindAllAsNoTracking(o => o.ServiceId == ServiceId).ToList();
            var serviceCode = userRequestService.GetNewServiceCode(ServiceId);
            var serviceInfo = serviceInfoService.Find(o => o.ServiceId == ServiceId);
            var userServiceRequest = new UserServiceRequestMaster()
            {
                RequestDateTime = DateTime.Now.DubaiTime(),
                ServiceId = ServiceId,
                Status = (int)ServiceFlowStateEnum.New,
                UserId = UserProfile.UserId,
                IsDeleted = false,
                RegistrationNoYear = serviceCode.Data.Year,
                IsDraft = isDraft,
                RegistrationNoMonth = serviceCode.Data.Month,
                RegistrationNoIndex = serviceCode.Data.RegistrationNoIndex,
                RegistrationNoServiceCode = serviceCode.Data.ServiceCode,
                RegistrationNo = serviceCode.Data.RegistrationNo,
                ParentUserServiceRequestMasterId = model.ParentUserServiceRequestMasterid,
                VAT = serviceInfo.VATPercentage,
                ServiceRequestFlowStages = serviceFlowStages
                    .Select(o => new ServiceRequestFlowStage()
                    {
                        Index = o.Index,
                        IsCurrentStage = o.Index == 1,
                        ServiceRequestFlowStageDepartments = o.ServiceApprovalStageDepartments.Select(p =>
                            new ServiceRequestFlowStageDepartment()
                            {
                                DepartmentId = p.DepartmentId,
                                Status = (int)ServiceFlowStateEnum.New,
                                IsApproved = false,
                            }).ToList()
                    }).ToList(),
                UserServiceRequestAttachments = model.UploadedFiles.Select(o => new UserServiceRequestAttachment()
                {
                    UserAttachmentId = o.Id
                }).ToList(),

                ServieBill = new UserServiceRequestBill()
                {
                    ChargedAmount = (double)serviceInfo.DefaultRate
                }
            };
            var request = MapViewModelToContract(model);
            request.UserServiceRequestMaster = userServiceRequest;
            groundHandlingRequestService.Add(request);
            serviceContext.Commit();
            return userServiceRequest;
        }

        private GroundHandlingRequestViewModel MapContractToViewModel(GroundHandlingRequest contract)
        {
            TinyMapper.Bind<GroundHandlingRequest, GroundHandlingRequestViewModel>();
            TinyMapper.Bind<AircraftInteriorCleaning, AircraftInteriorCleaningViewModel>();
            var model = TinyMapper.Map<GroundHandlingRequest, GroundHandlingRequestViewModel>(contract);

            model.AircraftCleaning = TinyMapper.Map<AircraftInteriorCleaning, AircraftInteriorCleaningViewModel>(contract.AircraftCleaning);
            model.GroundHandlingArcraftTypes =
                contract.AircraftTypes
                    .Select(o => new GroundHandlingArcraftTypeViewModel()
                    {
                        Description = o.AircraftType.Description,
                        IsOther = o.AircraftType.IsOther,
                        IsSelected = true,
                        AircraftTypeId = o.AircraftTypeId
                    })
                    .ToList();
            return model;
        }

        private GroundHandlingRequest MapViewModelToContract(GroundHandlingRequestViewModel model)
        {
            TinyMapper.Bind<GroundHandlingRequestViewModel, GroundHandlingRequest>();
            TinyMapper.Bind<AircraftInteriorCleaningViewModel, AircraftInteriorCleaning>();
            var contract = TinyMapper.Map<GroundHandlingRequestViewModel, GroundHandlingRequest>(model);

            contract.AircraftCleaning = TinyMapper.Map<AircraftInteriorCleaningViewModel, AircraftInteriorCleaning>(model.AircraftCleaning);
            contract.AircraftTypes =
                model.GroundHandlingArcraftTypes.Where(o => o.IsSelected)
                    .Select(o => new GroundHandlingRequestAircraftType()
                    {
                        AircraftTypeId = o.AircraftTypeId
                    })
                    .ToList();
            return contract;
        }

        #endregion "Private Methods"
    }
}