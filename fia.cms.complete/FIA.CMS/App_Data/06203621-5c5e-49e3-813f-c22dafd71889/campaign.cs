﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using umbraco.DataLayer;

namespace Designit.Umb.Newsletter.Bll
{
    public class Campaign
    {
        public string Title { get; private set; }

        public string Subject { get; private set; }

        public DateTime DateTime { get; private set; }

        public string Content { get; private set; }

        public string Template { get; private set; }

        public Campaign GetCampaign(int id)
        {
            var campaign = new Campaign();
            using (var reader =
                umbraco.BusinessLogic.Application.SqlHelper.ExecuteReader("select * from newsLetterCampaignNewsletter where id=@id",
                    umbraco.BusinessLogic.Application.SqlHelper.CreateParameter("@id", id)))
            {
                while (reader.Read())
                {
                    campaign.Title = reader.GetString("camptitle");
                    campaign.Subject = reader.GetString("campsubject");
                    campaign.DateTime = DateTime.Parse(reader.GetString("campSentTime"));
                    campaign.Content = reader.GetString("campcontent");
                    campaign.Template = reader.GetString("campTemplatecontent");
                }
            }
            return campaign;
        }

        public static List<Campaign> GetAllCampaigns()
        {
            var campaigns = new List<Campaign>();
            using (var reader =
                umbraco.BusinessLogic.Application.SqlHelper.ExecuteReader("select * from newsLetterCampaignNewsletter where campstatus='sent'ORDER BY campSentTime DESC "))
            {
                while (reader.Read())
                {
                    Campaign campaign = new Campaign();
                    campaign.Title = reader.GetString("camptitle");
                    campaign.Subject = reader.GetString("campsubject");
                    campaign.DateTime = DateTime.Parse(reader.GetString("campSentTime"));
                    campaign.Content = reader.GetString("campcontent");
                    campaign.Template = reader.GetString("campTemplatecontent");
                    campaigns.Add(campaign);
                }
            }
            return campaigns;
        }

        public static string EmbedNewsletter(string template, string content)
        {
            return Bll.EmbedNewsletter.EmbContentAndTemplate(template, content);
        }
    }
}
