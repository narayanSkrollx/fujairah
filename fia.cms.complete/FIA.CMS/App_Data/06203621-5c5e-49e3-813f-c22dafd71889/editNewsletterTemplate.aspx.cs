﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.AccessControl;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using umbraco;
using umbraco.BasePages;
using umbraco.DataLayer;

namespace Designit.Umb.Newsletter.Custom.plugins.designit.newsletter
{
    public partial class editNewsletterTemplate : UmbracoEnsuredPage

    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CodeAreaJavaScript", "<script language='javascript' src='/umbraco_client/CodeArea/javascript.js'></script>");
            //Page.ClientScript.RegisterClientScriptInclude("CodeAreaJavaScript", "/umbraco_client/CodeArea/javascript.js");
            //umbraco.library.RegisterJavaScriptFile("CodeAreaJavaScript", "/umbraco_client/CodeArea/javascript.js");
            editorSource.CodeBase = umbraco.uicontrols.CodeArea.EditorType.HTML;

            if (!IsPostBack)
            {
                int id = int.Parse(Request["id"]);

                using (var reader =
                    umbraco.BusinessLogic.Application.SqlHelper.ExecuteReader(
                        "select * from newsletterTemplate where id = @id",
                        umbraco.BusinessLogic.Application.SqlHelper.CreateParameter("@id", id)))
                {

                    while (reader.Read())
                    {
                        txtName.Text = reader.GetString("name");
                        txtAlias.Text = reader.GetString("alias");
                        if (txtName.Text.Length == 0)
                        {
                            txtName.Text = txtAlias.Text;
                        }
                        

                        var source = string.Empty;

                        var path = Bll.Helper.GetTemplateDir();
                        path = Path.Combine(path, String.Format("{0}.master", (object)txtName.Text));
                        if (File.Exists(path))
                        {
                            source = File.ReadAllText(path);
                        }
                        else
                        {
                            source = reader.GetString("design");
                            File.WriteAllText(path, source, Encoding.GetEncoding(1252));
                        }
                        editorSource.Text = source;
                    }
                }
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            ImageButton save = Panel1.Menu.NewImageButton();
            save.ImageUrl = GlobalSettings.Path + "/images/editor/save.gif";
            save.ToolTip = "Save";
            save.Click += new ImageClickEventHandler(save_Click);
                
           
            ImageButton bodyText = Panel1.Menu.NewImageButton();
            bodyText.ImageUrl = GlobalSettings.Path + "/images/editor/insField.gif";
            bodyText.ToolTip = "Insert body text field";
            bodyText.OnClientClick = "designTag();return false;";

            ImageButton unsubscribe = Panel1.Menu.NewImageButton();
            unsubscribe.ImageUrl = GlobalSettings.Path + "/images/editor/insMemberItem.gif";
            unsubscribe.ToolTip = "Insert unsubscribe field";
            unsubscribe.OnClientClick = "designUnsubscribeTag();return false;";
                
                
        }

        private void save_Click(object sender, ImageClickEventArgs e)
        {
            string updateStatement =
                @"update newsletterTemplate set 
                    name = @name,
                    alias = @alias,
                    design = @design 
                    where id = @id;";

            global::umbraco.BusinessLogic.Application.SqlHelper.ExecuteNonQuery(
                updateStatement,
                global::umbraco.BusinessLogic.Application.SqlHelper.CreateParameter(
                    "@name", txtName.Text),
                global::umbraco.BusinessLogic.Application.SqlHelper.CreateParameter(
                    "@alias", txtAlias.Text),
                global::umbraco.BusinessLogic.Application.SqlHelper.CreateParameter(
                    "@design", editorSource.Text),
                global::umbraco.BusinessLogic.Application.SqlHelper.CreateParameter(
                    "@id", int.Parse(Request["id"])));


            var path = Bll.Helper.GetTemplateDir();
            path = Path.Combine(path, String.Format("{0}.master", (object) txtAlias.Text));
            if (File.Exists(path))
            {
                File.WriteAllText(path, editorSource.Text, Encoding.GetEncoding(1252));
            }

            speechBubble(speechBubbleIcon.success, "Template saved","");
        }
    }
}