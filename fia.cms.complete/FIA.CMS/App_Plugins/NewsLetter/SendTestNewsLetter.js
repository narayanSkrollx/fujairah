﻿angular.module("umbraco").controller("newsletter.sendTestNewsLetter",
    function ($scope, $http, notificationsService) {
        $scope.model = {
            testEmail: ''
        };

        $scope.sendTestNewsLetterEmail = function () {
            var data = {
                toEmail: $scope.model.testEmail
            }
            $http.post("/umbraco/surface/Newsletter/SendTestNewsLetterEmail", angular.toJson(data)).then(function (response) {
                console.log(response);
                notificationsService.success("Success", "Test Email sent successfully");
            });
        }
        
    });