﻿$(function () {
    fia.CreateNamespace("exitspareparts");

    (function (context) {

        context.ObjectState = {
            Added: 0,
            Edited: 1,
            Deleted: 2,
            Unchanged: 3
        }
        context.Title = 'Exit of Spare Parts';
        context.ServiceId = fia.ServiceEnum.ExitOfAircraftSparePartsRequest;


        context.InitializeCreateUrl = ApplicationRootUrl('InitializeCreate', 'ExitSparePartsRequest');
        context.CreateUrl = ApplicationRootUrl('Create', 'ExitSparePartsRequest');
        context.CreateSuccessUrl = ApplicationRootUrl('Index', 'Dashboard');
        context.InitializeIndexUrl = ApplicationRootUrl('Initialize', 'ExitSparePartsRequest');
        context.DetailsUrl = ApplicationRootUrl('Detail', 'ExitSparePartsRequest');
        context.InitializeDetailUrl = ApplicationRootUrl('InitializeDetail', 'ExitSparePartsRequest');

        context.SaveRequest = function (isDraft) {
            if (!$('#frmExitOfSparePartsRequest').valid()) {
                return false;
            }
            var model = ko.mapping.toJS(context.ViewModel.AddNewModel);
            delete model.ExitAndReturnPassChecklistAddModel;
            var amodel = { model: model, isDraft: isDraft };
            var submitControl = isDraft ? 'draft' : 'save'
            var lightModel = JSON.stringify(amodel);
            var ajaxOption = {
                dataType: "JSON",
                contentType: "application/json; charset=UTF-8",
                data: lightModel,
                enableLadda: true,
                targetLaddaElement: `[data-role=${submitControl}]`,
            };
            ajaxRequest(context.CreateUrl, 'POST', ajaxOption, function (resp) {
                if (resp.IsSuccess) {
                    showMessage('Exit of Aircraft Spare Parts Request', resp.Message, 'success', function () {
                        //
                        window.location = '/ServiceRequest/Index?ServiceId=' + context.ServiceId;
                    });
                } else {
                    showMessage('Exit of Aircraft Spare Parts Request', resp.Message, 'error');
                }
            });
        }

        context.ViewModel = {
            SaveRequest: function () {
                context.SaveRequest(false);
            },
            SaveDraftRequest: function () {
                context.SaveRequest(true);
            },
            SetUserInfo: function () {
                context.SetUserInfo();
            },
            ViewDetails: function (item) {
                window.location = context.DetailsUrl + '?id=' + item.UserServiceRequestMasterId();
            }
        };

        context.CreateMapping = {
            create: function (options) {
                var vm = ko.mapping.fromJS(options.data);

                vm.AddedChecklist = ko.computed(function () {
                    if (!vm.ExitAndReturnPassChecklists()) {
                        return vm.ExitAndReturnPassChecklists();
                    } else {
                        return ko.utils.arrayFilter(vm.ExitAndReturnPassChecklists(), function (item) {
                            return item.ObjectState !== context.ObjectState.Deleted;
                        });
                    }
                });

                vm.AddCheckListClick = function () {
                    var addItem = ko.toJS(vm.ExitAndReturnPassChecklistAddModel);
                    addItem.ObjectState = context.ObjectState.Added;
                    vm.ExitAndReturnPassChecklists.push(ko.mapping.fromJS(addItem));
                };

                vm.RemoveCheckListClick = function (item) {
                    //var addItem = ko.toJS(vm.ExitAndReturnPassChecklistAddModel);
                    //addItem.ObjectState = context.ObjectState.Added;
                    //vm.ExitAndReturnPassChecklists.push(ko.mapping.fromJS(addItem));
                    vm.ExitAndReturnPassChecklists.remove(item);
                };
             

                vm.RevalidateForm = function () {
                    context.FormValidateSetting = $("#frmExitOfSparePartsRequest").validate({
                        rules: {
                            DateOfApplication: { required: true,  },
                            ApplicantCompany: { required: true, },
                            ApplicantMailIdOrContactDetails: { required: true, },
                            AircraftType: { required: true, },
                            AircraftRegistrationNumber: { required: true, },
                            OwnerOfAircraft: { required: true, },
                            NoObjectionLetterOfTheOwner: { required: true, },
                            HasNoObjectionLetter: { required: true, },
                            AircraftMaintenanceOrganisationOrCar145GCAARegisteredCo: { required: true, },
                        },
                        messages: {
                            DateOfApplication: { required: 'Application Date is required', date: 'Application Date must be a valid Date.' },
                            ApplicantCompany: { required: 'Applicant COmpnay is required.', },
                            ApplicantMailIdOrContactDetails: { required: 'Applicant Mail is required', },
                            AircraftType: { required: 'Aircraft Type is required', },
                            AircraftRegistrationNumber: { required: 'Aircraft Registration No is required', },
                            OwnerOfAircraft: { required: 'Owner of Aircraft is required', },
                            NoObjectionLetterOfTheOwner: { required: 'No object Letter or owner is required', },
                            HasNoObjectionLetter: { required: 'No Objection Letter? is required', },
                            AircraftMaintenanceOrganisationOrCar145GCAARegisteredCo: { required: 'Aicraft Maintenance Org is required', },
                        }
                    });

                };



                return vm;
            }
        }


        context.SetUserInfo = function () {
            var addNewModel = context.ViewModel.AddNewModel;
            var userInfo = context.ViewModel.AddNewModel.UserInfo;
            if (addNewModel != null && userInfo != null) {
                addNewModel.ApplicantMailIdOrContactDetails(userInfo.Email());
                addNewModel.ApplicantCompany(userInfo.CompanyName());
            }
        }

        context.InitializeCreate = function () {
            var masterId = parseInt($("#CurrentUserServiceRequestMasterId.forKnockout").val());
            context.ViewModel.IsCreate = Number.isInteger(masterId);
            var id = context.ViewModel.IsCreate ? parseInt(masterId) : null;

            context.FormValidateSetting = $("#frmExitOfSparePartsRequest").validate();

            ajaxRequest(context.InitializeCreateUrl, 'GET', { data: { id: id } }, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#createContent')[0])) {
                        context.ViewModel.AddNewModel = ko.mapping.fromJS(resp.Data, context.CreateMapping);
                        context.ViewModel.RemoveUploadedFile = function (item) {
                            item.ObjectState(2);
                        }
                        ko.applyBindings(context.ViewModel, $('#createContent')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, context.CreateMapping, context.ViewModel.AddNewModel);
                    }
                    //
                    fia.fileinputhelper.InitializeFileUpload('#kv-explorer', fia.ServiceEnum.ExitOfAircraftSparePartsRequest);
                    fia.fileinputhelper.UploadCompleteCallback = function (response) {
                        context.ViewModel.AddNewModel.UploadedFiles.push(response.Data);
                    }
                } else {
                    console.log(resp.Message);
                }
            });
        };

        context.InitializeIndex = function () {
            ajaxRequest(context.InitializeIndexUrl, 'GET', {}, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#mainContent')[0])) {
                        context.ViewModel.Records = ko.mapping.fromJS(resp.Data);
                        ko.applyBindings(context.ViewModel, $('#mainContent')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel.Records);
                    }
                } else {
                    showMessage('Service Request Detail', resp.Message, 'error');
                }
            });
        };

        context.InitializeDetail = function () {
            context.CurrentUserServiceRequestMasterId = $("#CurrentUserServiceRequestMasterId.forKnockout").val();

            ajaxRequest(context.InitializeDetailUrl + "?id=" + context.CurrentUserServiceRequestMasterId, 'GET', {}, function (resp) {
                if (resp.IsSuccess) {
                    if (!ko.dataFor($('#exitSparePartsDetailsDiv')[0])) {
                        context.ViewModel.Details = ko.mapping.fromJS(resp.Data);
                        ko.applyBindings(context.ViewModel, $('#exitSparePartsDetailsDiv')[0]);
                    } else {
                        ko.mapping.fromJS(resp.Data, {}, context.ViewModel.Details);
                    }

                    fia.fileinputhelper.InitializeUploadedList('#uploadedFiles', context.CurrentUserServiceRequestMasterId);
                } else {
                    showMessage('Service Request Detail', resp.Message, 'error');
                }
            });
        };

    })(fia.exitspareparts);
});