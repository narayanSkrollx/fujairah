﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class ExitAndReturnPassChecklistService : BusinessAccessBase<ExitOfAircraftSparePartsRequest>
    {
        public ExitAndReturnPassChecklistService(ServiceContext context) : base(context)
        {
        }
    }
}
