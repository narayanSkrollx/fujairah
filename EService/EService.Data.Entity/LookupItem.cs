namespace EService.Data.Entity
{
    public class LookupItem : ILookupItem
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
    }
}