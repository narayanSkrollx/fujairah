using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class ServiceApprovalStageDepartmentConfiguration : EntityTypeConfiguration<ServiceApprovalStageDepartment>
    {
        public ServiceApprovalStageDepartmentConfiguration()
        {
            ToTable("ServiceApprovalStageDepartment");

            HasKey(o => o.ServiceApprovalStageDepartmentId);

            Property(o => o.ServiceApprovalStageDepartmentId)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(o => o.DepartmentId).IsRequired();

            HasRequired(o => o.Department)
                .WithMany(o => o.ServiceApprovalStageDepartments)
                .HasForeignKey(o => o.DepartmentId);
            HasRequired(o => o.ServiceApprovalStage)
                .WithMany(o => o.ServiceApprovalStageDepartments)
                .HasForeignKey(o => o.ServiceApprovalStageId);
        }
    }
}