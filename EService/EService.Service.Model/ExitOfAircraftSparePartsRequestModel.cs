﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EService.Service.Model
{
    public class ExitOfAircraftSparePartsRequestModel : EServiceBaseModal
    {

        #region "User Service Request Master Columns"

        public int? CurrentIndex { get; set; }

        #endregion "User Service Request Master Columns"
        public DateTime DateOfApplication { get; set; }
        public string ApplicantCompany { get; set; }
        public string AircraftType { get; set; }
        public string AircraftRegistrationNumber { get; set; }
        public string OwnerOfAircraft { get; set; }
        public bool? HasNoObjectionLetter { get; set; }
        public string NoObjectionLetterOfTheOwner { get; set; }
        public bool? AircraftMaintenanceOrganisationOrCar145GCAARegisteredCo { get; set; }
        public string ApplicantMailIdOrContactDetails { get; set; }
        public List<ExitAndReturnPassChecklistServiceModel> ExitAndReturnPassChecklists { get; set; } 
    }


        public class ExitAndReturnPassChecklistServiceModel
        {
            public int ExitSparePartsPassChecklistId { get; set; }
            public int UserServiceRequestMasterId { get; set; }

            public string Description { get; set; }
            public string PartNumber { get; set; }
            public int? NumberOfPieces { get; set; }
            public string Origin { get; set; }
            public string ExitToWhere { get; set; }

        }
}
