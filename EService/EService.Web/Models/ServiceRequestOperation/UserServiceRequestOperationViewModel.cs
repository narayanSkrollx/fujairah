﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EService.Service.Model;

namespace EService.Web.Models.ServiceRequestOperation
{
    public class UserServiceRequestOperationViewModel: ServiceRequestSearchViewModel
    {
        public UserServiceRequestOperationViewModel()
        {
            Services = new List<SelectListModel>();
            Statuses = new List<SelectListModel>();
        }

        public int? Status { get; set; }
        public string ApplicantEmail { get; set; }

        public List<SelectListModel> Services { get; set; }
        public List<SelectListModel> Statuses { get; set; }
    }
}