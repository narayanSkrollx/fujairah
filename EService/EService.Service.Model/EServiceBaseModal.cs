using System;
using System.Collections.Generic;

namespace EService.Service.Model
{
    public abstract class EServiceBaseModal
    {
        public EServiceBaseModal()
        {
            UploadedFiles = new List<FileUploadedServiceModel>();
        }

        public int UserServiceRequestMasterId { get; set; }
        public DateTime RequestDateTime { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string RegistrationNo { get; set; }

        public List<FileUploadedServiceModel> UploadedFiles { get; set; } 
    }
}