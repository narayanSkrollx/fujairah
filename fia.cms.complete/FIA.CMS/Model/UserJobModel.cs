﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web;
using Umbraco.Web.Mvc;
namespace FIA.CMS.Model
{
    public class UserJobModel
    {
        public int UserJobId { get; set; }
        public int UserId { get; set; }
        public int JobId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ResumePath { get; set; }
        public string Email { get; set; }
        public string Url { get; set; }
        public DateTime ApplyDate { get; set; }
        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }

        public string JobTitle { get; set; }
        public DateTime JobPositingDate { get; set; }
        public int JobPostingId { get;  set; }

        public string ApplyDateText
        {
            get { return ApplyDate.ToString("d"); }
        }

        public DateTime? AcceptRejectDate { get; set; }
        public bool? IsAccepted { get; set; }

        public string Status {
            get
            {
                if (IsAccepted.HasValue)
                {
                    return IsAccepted.Value ? "Accepted" : "Rejected";
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string AcceptRejectDateText
        {
            get
            {
                if (AcceptRejectDate.HasValue)
                {
                    return AcceptRejectDate.Value.ToString("d");
                }
                else
                {
                   return  string.Empty;
                }
            }
        }
    }
}