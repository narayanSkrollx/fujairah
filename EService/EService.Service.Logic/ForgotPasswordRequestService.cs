﻿using System;
using System.Linq;
using EService.Data.Entity;
using EService.Data.Entity.Entity;
using EService.Core;
namespace EService.Service.Logic
{
    public class ForgotPasswordRequestService : BusinessAccessBase<ForgotPasswordRequest>
    {
        public ForgotPasswordRequestService(ServiceContext context) : base(context)
        {
        }

        public Tuple<bool, ForgotPasswordRequest, string, string> GetNewForgotPasswordToken(string email)
        {
            var publicUser = (int)UserTypeEnum.ExternalUser;
            var user = Context.Database.Users.FirstOrDefault(o => o.UserTypeId == publicUser && !o.IsDeleted && o.Email == email);

            if (user == null)
            {
                return new Tuple<bool, ForgotPasswordRequest, string, string>(false, null, null, null);
            }
            else
            {
                var token = Guid.NewGuid();
                var model = new ForgotPasswordRequest()
                {
                    UserId = user.UserId,
                    Email = email,
                    ExpiryTime = DateTime.Now.DubaiTime().AddHours(24),
                    IsDeleted = false,
                    IsUsed = false,
                    Token = token,
                    CreatedByUserId = user.UserId,
                    CreatedDate = DateTime.Now.DubaiTime(),
                    ModifiedByUserId = user.UserId,
                    ModifiedDate = DateTime.Now.DubaiTime()
                };

                Context.Database.ForgotPasswordRequests.Add(model);
                Context.Database.SaveChanges();
                return new Tuple<bool, ForgotPasswordRequest, string, string>(true, model, user.FirstName, user.LastName);
            }
        }

        public bool IsTokenValid(Guid token)
        {
            var forgotPassword = Context.Database.ForgotPasswordRequests.FirstOrDefault(
                    o => !o.IsUsed && !o.IsDeleted && o.Token == token && o.ExpiryTime >= DateTime.Now.DubaiTime());
            return forgotPassword != null;
        }
    }
}