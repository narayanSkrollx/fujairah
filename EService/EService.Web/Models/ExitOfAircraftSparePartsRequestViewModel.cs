﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using EService.Web.App_LocalResources;

namespace EService.Web.Models
{
    public class ExitOfAircraftSparePartsRequestViewModel : EServiceCreateBaseViewModel
    {
        public int UserServiceRequestMasterId { get; set; }

        [Display(ResourceType = typeof (ExitOfSparePartsResource), Name = "ApplicationDate")]
        public DateTime DateOfApplication { get; set; }
        [Display(ResourceType = typeof (ExitOfSparePartsResource), Name = "ApplicantCompany")]
        public string ApplicantCompany { get; set; }
        [Display(ResourceType = typeof (ExitOfSparePartsResource), Name = "AircraftType")]
        public string AircraftType { get; set; }
        [Display(ResourceType = typeof (ExitOfSparePartsResource), Name = "AircraftRegNo")]
        public string AircraftRegistrationNumber { get; set; }
        [Display(ResourceType = typeof (ExitOfSparePartsResource), Name = "OwnerOfAircraft")]
        public string OwnerOfAircraft { get; set; }
        [Display(ResourceType = typeof (ExitOfSparePartsResource), Name = "OwnerNOC")]
        public string NoObjectionLetterOfTheOwner { get; set; }
        [Display(ResourceType = typeof(ExitOfSparePartsResource), Name = "HasNoObjectionLetter")]
        public bool? HasNoObjectionLetter { get; set; }
        [Display(ResourceType = typeof (ExitOfSparePartsResource), Name = "AircraftMaintenanceOrg")]
        public bool? AircraftMaintenanceOrganisationOrCar145GCAARegisteredCo { get; set; }
        [Display(ResourceType = typeof (ExitOfSparePartsResource), Name = "ApplicantContactDetail")]
        public string ApplicantMailIdOrContactDetails { get; set; }

        public List<ExitAndReturnPassChecklistViewModel> ExitAndReturnPassChecklists { get; set; }
        public ExitAndReturnPassChecklistViewModel ExitAndReturnPassChecklistAddModel { get; set; }

        public ExitOfAircraftSparePartsRequestViewModel()
        {
            ExitAndReturnPassChecklists = new List<ExitAndReturnPassChecklistViewModel>();
            ExitAndReturnPassChecklistAddModel = new ExitAndReturnPassChecklistViewModel();
        }
    }
}