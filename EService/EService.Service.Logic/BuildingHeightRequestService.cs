﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EService.Core;
using EService.Data.Entity.Entity;
using EService.Service.Logic.Enum;
using EService.Service.Model;

namespace EService.Service.Logic
{
    public class BuildingHeightRequestService : BaseEService<BuildingHeightRequest>
    {
        public BuildingHeightRequestService(ServiceContext context) : base(context)
        {
        }

        public override ServiceEnum Service => ServiceEnum.BuildingHeightRequest;

        public ResponseObject UpdateBuildingHeightRequest(BuildingHeightRequest model, bool isDraft, List<UserServiceRequestAttachment> attachmentList)
        {
            var result = new ResponseObject();
            var contract = Find(x => x.UserServiceRequestMasterId == model.UserServiceRequestMasterId);
            if (contract == null)
            {
                result.IsSuccess = false;
                result.Message = "No request matched to edit.";
            }
            else
            {
                contract.UserServiceRequestMaster.IsDraft = isDraft;

                var removeDocList = contract.UserServiceRequestMaster.UserServiceRequestAttachments.ToList();
                Context.Database.UserServiceRequestAttachment.RemoveRange(removeDocList);

                attachmentList.ForEach(o => contract.UserServiceRequestMaster.UserServiceRequestAttachments.Add(o));

                contract.ProjectName = model.ProjectName;
                contract.ApplicationDate = model.ApplicationDate;
                contract.ExpectedConstructionStartDate = model.ExpectedConstructionStartDate;
                contract.ExpectedConstructionEndDate = model.ExpectedConstructionEndDate;
                contract.Location = model.Location;
                contract.ProjectDescription = model.ProjectDescription;
                contract.ApplicantSignature = model.ApplicantSignature;
                contract.ApplicantName = model.ApplicantName;
                contract.ApplicantJobTitle = model.ApplicantJobTitle;
                contract.ApplicantCompanyName = model.ApplicantCompanyName;
                contract.ApplicantAddress = model.ApplicantAddress;
                contract.ApplicantCell = model.ApplicantCell;
                contract.ApplicantTel = model.ApplicantTel;
                contract.ApplicantEmail = model.ApplicantEmail;
                contract.ApplicantWebsite = model.ApplicantWebsite;
                contract.OwnerName = model.OwnerName;
                contract.OwnerCompanyName = model.OwnerCompanyName;
                contract.OwnerAddress = model.OwnerAddress;
                contract.OwnerCell = model.OwnerCell;
                contract.OwnerTel = model.OwnerTel;
                contract.OwnerEmail = model.OwnerEmail;
                contract.OwnerWebsite = model.OwnerWebsite;
                contract.IsBuilding = model.IsBuilding;
                contract.IsOHLCommunicationTower = model.IsOHLCommunicationTower;
                contract.IsOtherType = model.IsOtherType;
                contract.OtherTypeDescription = model.OtherTypeDescription;
                contract.BuildingType = model.BuildingType;
                contract.NumberOfBuildings = model.NumberOfBuildings;
                contract.NumberOfFloors = model.NumberOfFloors;
                contract.TallestHeight = model.TallestHeight;
                contract.HasObstacleLight = model.HasObstacleLight;
                contract.DetailsOfLightinDrawing = model.DetailsOfLightinDrawing;
                contract.OHLOrCommunicationType = model.OHLOrCommunicationType;
                contract.NumberOfStructure = model.NumberOfStructure;
                contract.TypeOfOHLCommTower = model.TypeOfOHLCommTower;
                contract.AreThereMountains = model.AreThereMountains;
                contract.ProjectTypeId = model.ProjectTypeId;
                contract.OtherProjectTypeDescription = model.OtherProjectTypeDescription;

                //todo: other reference tables
                var buildingCordinateHeights = contract.BuildingCoordinateAndHeights.ToList();
                Context.Database.BuildingCoordinateAndHeights.RemoveRange(buildingCordinateHeights);

                foreach (var item in model.BuildingCoordinateAndHeights)
                {
                    contract.BuildingCoordinateAndHeights.Add(item);
                }
                var AdditionalItemsForBuildingSubmittals = contract.AdditionalItemsForBuildingSubmittals.ToList();
                Context.Database.AdditionalItemsForBuildingSubmittals.RemoveRange(AdditionalItemsForBuildingSubmittals);
                foreach (var item in model.AdditionalItemsForBuildingSubmittals)
                {
                    contract.AdditionalItemsForBuildingSubmittals.Add(item);
                }
                var BuildingHeightApplicableFees = contract.BuildingHeightApplicableFees.ToList();
                Context.Database.BuildingHeightApplicableFees.RemoveRange(BuildingHeightApplicableFees);
                foreach (var item in model.BuildingHeightApplicableFees)
                {
                    contract.BuildingHeightApplicableFees.Add(item);
                }
                var OHLStructureCoordinateAndHeights = contract.OHLStructureCoordinateAndHeights.ToList();
                Context.Database.OHLStructureCoordinateAndHeight.RemoveRange(OHLStructureCoordinateAndHeights);
                foreach (var item in model.OHLStructureCoordinateAndHeights)
                {
                    contract.OHLStructureCoordinateAndHeights.Add(item);
                }

                var AdditionalItemsForOHLSubmittals = contract.AdditionalItemsForOHLSubmittals.ToList();
                Context.Database.AdditionalItemsForOHLSubmittals.RemoveRange(AdditionalItemsForOHLSubmittals);
                foreach (var item in model.AdditionalItemsForOHLSubmittals)
                {
                    contract.AdditionalItemsForOHLSubmittals.Add(item);
                }

                var BuildingHeightAviationObstacles = contract.BuildingHeightAviationObstacles.ToList();
                Context.Database.BuildingHeightAviationObstacles.RemoveRange(BuildingHeightAviationObstacles);
                foreach (var item in model.BuildingHeightAviationObstacles)
                {
                    contract.BuildingHeightAviationObstacles.Add(item);
                }

                Context.Commit();
                result.IsSuccess = true;
                result.Message = "Building Height Request updated successfully";
            }
            return result;
        }

        public override ResponseObject IsValidForApproveReject(int userServiceRequestMasterId, int serviceRequestFlowStageDepartmentId,
            int departmentId)
        {
            var result = new ResponseObject();
            var serviceRequestFlowStageDepartment =
                Context.Database.ServiceRequestFlowStageDepartments.FirstOrDefault(
                    o => o.ServiceRequestFlowStageDepartmentId == serviceRequestFlowStageDepartmentId && o.ServiceRequestFlowStage.IsCurrentStage && o.DepartmentId == departmentId);
            if (serviceRequestFlowStageDepartment == null)
            {
                result.IsSuccess = false;
                result.Message =
                    "Invalid Service Flow Request. Either the department is invalid or is not current stage.";
            }
            else
            {
                var userServiceRequestMaster =
                    Context.Database.UserServiceRequests.FirstOrDefault(
                        o =>
                            !o.IsDeleted && o.UserServiceRequestMasterId == userServiceRequestMasterId);
                if (userServiceRequestMaster == null || userServiceRequestMaster.BuildingHeightRequest == null)
                {
                    result.IsSuccess = false;
                    result.Message = "Provided Service Request info is not a valid Building Height Data Request";
                }
                else
                {
                    var serviceFlowConfiguration =
                        Context.Database.ServiceApprovalStageDepartments.FirstOrDefault(
                            o => o.ServiceApprovalStage.ServiceId == (int)Service && departmentId == o.DepartmentId);
                    if (serviceFlowConfiguration == null)
                    {
                        result.IsSuccess = false;
                        result.Message = "Either Service Flow Configuration is not configured or does not exists.";
                    }
                    else
                    {
                        if (serviceFlowConfiguration.ServiceApprovalStage.Index == 1)
                        {
                            result.IsSuccess = userServiceRequestMaster.BuildingHeightRequest.BuildingHeightPlanningApproval != null;
                            result.Message = !result.IsSuccess ? "Planning Department needs to add remarks before it can be approved or rejected." : "";
                        }
                        else if (serviceFlowConfiguration.ServiceApprovalStage.Index == 4)
                        {
                            result.IsSuccess = !(Math.Abs(userServiceRequestMaster.ServieBill.ChargedAmount - (double)userServiceRequestMaster.Service.DefaultRate) < 0.01);
                            result.Message = !result.IsSuccess ? "Finance Department needs to add Charge Amount for this request.." : "";
                        }
                        else
                        {
                            result.IsSuccess = true;
                        }
                    }
                }
            }
            return result;
        }

        public ResponseObject UpdateChargeAmount(int id, decimal chargedAmount)
        {
            var result = new ResponseObject();
            var userServiceRequestMaster =
             Context.Database.UserServiceRequests.FirstOrDefault(o => o.UserServiceRequestMasterId == id);
            if (userServiceRequestMaster == null || userServiceRequestMaster.BuildingHeightRequest == null ||
                Math.Abs(userServiceRequestMaster.ServieBill.ChargedAmount - (double)userServiceRequestMaster.Service.DefaultRate) > 0.01)
            {
                result.IsSuccess = false;
                result.Message = "Either user Service Request is not a valid Building Height request or has already saved Charged Amount.";
            }
            else
            {
                userServiceRequestMaster.ServieBill.ChargedAmount = (double)chargedAmount;
                Context.Commit();
                result.IsSuccess = true;
                result.Message = "Building Height Request Chrage Amount saved successfully.";
            }
            return result;
        }
        public ResponseObject AddPlanningRemarks(int id, string remarks, UploadedFile[] fileUploads, int userId)
        {
            var result = new ResponseObject();
            var userServiceRequestMaster =
             Context.Database.UserServiceRequests.FirstOrDefault(o => o.UserServiceRequestMasterId == id);
            if (userServiceRequestMaster == null || userServiceRequestMaster.BuildingHeightRequest == null || userServiceRequestMaster.BuildingHeightRequest.BuildingHeightPlanningApproval != null)
            {
                result.IsSuccess = false;
                result.Message = "Either user Service Request is not a valid Building Height request or has already saved Planning Remarks.";
            }
            else
            {
                userServiceRequestMaster.BuildingHeightRequest.BuildingHeightPlanningApproval = new BuildingHeightPlanningApproval
                {
                    Remarks = remarks,
                    CreatedByUserId = userId,
                    CreatedDate = DateTime.Now.DubaiTime(),
                    IsDeleted = false,
                    ModifiedByUserId = userId,
                    ModifiedDate = DateTime.Now.DubaiTime(),
                    Attachments = (fileUploads ?? new UploadedFile[] { }).Select(o => new BuildingHeightPlanningApprovalAttachment
                    {
                        Attachment = new UserAttachment
                        {
                            ContentType = o.ContentType,
                            CreatedByUserId = userId,
                            CreatedDate = DateTime.Now.DubaiTime(),
                            FileName = o.FileName,
                            IsDeleted = false,
                            ModifiedByUserId = userId,
                            ModifiedDate = DateTime.Now.DubaiTime(),
                            ServiceId = userServiceRequestMaster.ServiceId,
                            Timestamp = DateTime.Now.DubaiTime(),
                            UserAttachmentData = new UserAttachmentData
                            {
                                FileContents = o.Content,
                            },
                            UserId = userId,

                        }
                    }).ToList()

                };
                Context.Commit();
                result.IsSuccess = true;
                result.Message = "Building Height Request Planning Remarks saved successfully.";
            }
            return result;
        }

        public override bool HasExtraFormForApprovalReject()
        {
            return true;
        }
    }
}
