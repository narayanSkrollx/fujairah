﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class BuildingHeightRequest : AuditableEntity, IServiceEntity
    {
        public int UserServiceRequestMasterId { get; set; }

        #region "General Project Information"
        public string ProjectName { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public DateTime? ExpectedConstructionStartDate { get; set; }
        public DateTime? ExpectedConstructionEndDate { get; set; }
        public string Location { get; set; }
        public string ProjectDescription { get; set; }
        public string ApplicantSignature { get; set; }

        public string ApplicantName { get; set; }
        public string ApplicantJobTitle { get; set; }
        public string ApplicantCompanyName { get; set; }
        public string ApplicantAddress { get; set; }
        public string ApplicantCell { get; set; }
        public string ApplicantTel { get; set; }
        public string ApplicantEmail { get; set; }
        public string ApplicantWebsite { get; set; }

        public string OwnerName { get; set; }
        public string OwnerCompanyName { get; set; }
        public string OwnerAddress { get; set; }
        public string OwnerCell { get; set; }
        public string OwnerTel { get; set; }
        public string OwnerEmail { get; set; }
        public string OwnerWebsite { get; set; }

        public bool IsBuilding { get; set; }
        public bool IsOHLCommunicationTower { get; set; }
        public bool IsOtherType { get; set; }
        public string OtherTypeDescription { get; set; }
        #endregion

        #region "Buildings Details Only"
        /// <summary>
        /// true for singular building and false for multiple buildings
        /// </summary>
        public bool? BuildingType { get; set; }
        public int? NumberOfBuildings { get; set; }

        public int? NumberOfFloors { get; set; }
        public double? TallestHeight { get; set; }

        public bool? HasObstacleLight { get; set; }
        public string DetailsOfLightinDrawing { get; set; }

        #endregion

        #region "OHL/Communication tower projet details only"

        public bool? OHLOrCommunicationType { get; set; }
        public int? NumberOfStructure { get; set; }
        public string TypeOfOHLCommTower { get; set; }
        public bool? AreThereMountains { get; set; }

        #endregion

        #region "Applicant Owner Relation'
        public int ApplicantOwnerRelationshipId { get; set; }
        public string ApplicantOwnerRelationshipOtherName { get; set; }
        #endregion

        public int ProjectTypeId { get; set; }
        public string OtherProjectTypeDescription { get; set; }

        #region "Navigation Properties"

        [JsonIgnore]
        public virtual ApplicantOwnerRelationship ApplicantOwnerRelationship { get; set; }

        [JsonIgnore]
        public virtual UserServiceRequestMaster UserServiceRequestMaster { get; set; }

        [JsonIgnore]
        public virtual ICollection<BuildingCoordinateAndHeight> BuildingCoordinateAndHeights { get; set; }
        [JsonIgnore]
        public virtual ICollection<AdditionalItemsForBuildingSubmittal> AdditionalItemsForBuildingSubmittals { get; set; }
        [JsonIgnore]
        public virtual ICollection<BuildingHeightApplicableFee> BuildingHeightApplicableFees { get; set; }
        [JsonIgnore]
        public virtual ICollection<OHLStructureCoordinateAndHeight> OHLStructureCoordinateAndHeights { get; set; }
        [JsonIgnore]
        public virtual ICollection<AdditionalItemsForOHLSubmittal> AdditionalItemsForOHLSubmittals { get; set; }
        [JsonIgnore]
        public virtual ICollection<BuildingHeightAviationObstacle> BuildingHeightAviationObstacles { get; set; }
        [JsonIgnore]
        public virtual ProjectType ProjectType { get; set; }

        public virtual BuildingHeightPlanningApproval BuildingHeightPlanningApproval { get; set; }
        #endregion

    }
    public class BuildingHeightPlanningApproval : AuditableEntity
    {
        public int UserServiceRequestMasterId { get; set; }
        public string Remarks { get; set; }

        public virtual BuildingHeightRequest BuildingHeightRequest { get; set; }
        public virtual ICollection<BuildingHeightPlanningApprovalAttachment> Attachments { get; set; }
    }

    public class BuildingHeightPlanningApprovalAttachment
    {
        public int BuildingHeightPlanningApprovalAttachmentId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public int UserAttachmentId { get; set; }

        public virtual BuildingHeightPlanningApproval PlanningApproval { get; set; }
        public virtual UserAttachment Attachment { get; set; }
    }
}
