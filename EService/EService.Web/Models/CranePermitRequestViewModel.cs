using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EService.Data.Entity.Entity;
using EService.Web.App_LocalResources;

namespace EService.Web.Models
{
    public class CranePermitRequestViewModel : EServiceCreateBaseViewModel, IValidatableObject
    {
        public int UserServiceRequestMasterId { get; set; }

        [Required(ErrorMessageResourceType = typeof(CranePermitLocationResource), ErrorMessageResourceName = "ApplicantNameRequired")]
        public string ApplicantName { get; set; }
        [Required(ErrorMessageResourceType = typeof(CranePermitLocationResource), ErrorMessageResourceName = "ApplicationDateRequired")]
        [DataType(DataType.Date)]
        public DateTime? ApplicationDate { get; set; }
        [Required(ErrorMessageResourceType = typeof(CranePermitLocationResource), ErrorMessageResourceName = "CompanyNameRequired")]
        public string CompanyName { get; set; }
        [Required(ErrorMessage = "Applicant Position is required")]
        public string Position { get; set; }
        [Required(ErrorMessageResourceType = typeof(CranePermitLocationResource), ErrorMessageResourceName = "ContactNumberRequired")]
        public string ContactNumber { get; set; }
        [DataType(DataType.EmailAddress, ErrorMessageResourceType = typeof(CranePermitLocationResource), ErrorMessageResourceName = "ApplicantEmailValidEmail")]
        [Required(ErrorMessageResourceType = typeof(CranePermitLocationResource), ErrorMessageResourceName = "ApplicantEmailRequired")]
        public string ApplicantEmail { get; set; }
        public bool IsNewApplication { get; set; }
        public string PreviousPermitNumbers { get; set; }
        [Required(ErrorMessage = "Northings/Eastings is required")]
        public string NorthingsEastings { get; set; }
        [Required(ErrorMessage = "Location of Crane is required")]
        public string CraneLocation { get; set; }
        [Required(ErrorMessageResourceType = typeof(CranePermitLocationResource), ErrorMessageResourceName = "OperationTimingRequired")]
        public string OperationStartTime { get; set; }

        [Required(ErrorMessageResourceType = typeof(CranePermitLocationResource), ErrorMessageResourceName = "OperationTimingRequired")]
        public string OperationEndTime { get; set; }
        [Required(ErrorMessageResourceType = typeof(CranePermitLocationResource), ErrorMessageResourceName = "OperationDateRequired")]
        public DateTime? OperationStartDate { get; set; }
        [Required(ErrorMessageResourceType = typeof(CranePermitLocationResource), ErrorMessageResourceName = "OperationDateRequired")]
        public DateTime? OperationEndDate { get; set; }
        [Required(ErrorMessageResourceType = typeof(CranePermitLocationResource), ErrorMessageResourceName = "MaxOperatingHeightRequired")]
        public int? MaxOperatingHeight { get; set; }
        public string PurposeOfUser { get; set; }
        [Required(ErrorMessageResourceType = typeof(CranePermitLocationResource), ErrorMessageResourceName = "CraneOperatingCompnayRequired")]
        public string CraneOperatingCompany { get; set; }
        [Required(ErrorMessageResourceType = typeof(CranePermitLocationResource), ErrorMessageResourceName = "OperatorNameRequired")]
        public string OperatorName { get; set; }
        [Required(ErrorMessageResourceType = typeof(CranePermitLocationResource), ErrorMessageResourceName = "OperatorContactNumberRequired")]
        public string OperatorContactNumber { get; set; }
        //public List<CraneType> CraneTypes { get; set; }
        public List<CraneTypeSelectionViewModel> CraneTypeSelectionViewModels { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var result = new List<ValidationResult>();
            if (!OperationStartDate.HasValue)
            {
                result.Add(new ValidationResult("Operation Start Date is Required") { });
            }
            if (!OperationEndDate.HasValue)
            {
                result.Add(new ValidationResult("Operation End Date is Required") { });
            }
            if (OperationStartDate > OperationEndDate)
            {
                result.Add(new ValidationResult("Operation Start Date cannot be greater than End Date") { });
            }
            return result;
        }
    }

    public class CraneTypeSelectionViewModel
    {
        public int CraneTypeId { get; set; }
        public string CraneTypeName { get; set; }
        public bool IsOther { get; set; }
        public bool IsSelected { get; set; }
        public string OtherCraneTypeName { get; set; }
    }
}