using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class BuildingHeightApplicableFee
    {
        public int ApplicableFeesLookUpId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public bool IsSelected { get; set; }
        public double Quantity { get; set; }
        public double TotalFee { get; set; }

        [JsonIgnore]
        public virtual ApplicableFeesLookUp ApplicableFeesLookUp { get; set; }

        [JsonIgnore]
        public virtual BuildingHeightRequest BuildingHeightRequest { get; set; }
    }
}