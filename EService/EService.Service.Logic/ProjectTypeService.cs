using EService.Data.Entity.Entity;

namespace EService.Service.Logic
{
    public class ProjectTypeService : BusinessAccessBase<ProjectType>
    {
        public ProjectTypeService(ServiceContext context) : base(context)
        {
        }
    }
}