using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EService.Service.Logic;
using EService.Service.Model;
using EService.Web.Areas.Configure.Models;
using EService.Web.Controllers.Base;
using EService.Web.Helper;
using EService.Web.Models;
using EService.Core;
using EService.Data.Entity.Entity;

namespace EService.Web.Areas.Configure.Controllers
{
    public class ServiceController : SystemAdminBaseController
    {
        private ServiceInfoService serviceInfoService;

        public ServiceController()
        {
            serviceInfoService = new ServiceInfoService(serviceContext);
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Initialize()
        {
            var result = new ResponseObject();
            try
            {
                var model = new ServiceInfoViewModel()
                {
                    Records = new HashSet<ServiceInfoServiceModel>(serviceInfoService.SearchServiceRecords()),
                    AddNewModel = new ServiceInfoAddViewModel()
                };
                result.Data = model;
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                LogError(ex);
                result.Message = "Error while initializing service information.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [ValidateInput(false)]
        public JsonResult Create(ServiceInfoAddViewModel model)
        {
            var result = new JsonResponseObject();
            try
            {
                if (!ModelState.IsValid)
                {
                    result.IsSuccess = false;
                    result.Message = $"Server Validation Error: {ModelState.GetModelStateError()}";
                }
                else
                {
                    var saveModel = new Data.Entity.Entity.Service()
                    {
                        ServiceId = model.ServiceId ?? 0,
                        ServiceName = model.ServiceName,
                        ServiceCode = model.ServiceCode,
                        DefaultRate = model.DefaultRate ?? 0,
                        VATPercentage = model.VATPercentage ?? 0,
                        CreatedByUserId = UserProfile.UserId,
                        ModifiedByUserId = UserProfile.UserId,
                        CreatedDate = DateTime.Now.DubaiTime(),
                        ModifiedDate = DateTime.Now.DubaiTime()
                    };
                    var saveResult = serviceInfoService.Save(saveModel);
                    result.IsSuccess = saveResult.Item1;
                    result.Message = saveResult.Item2;
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "Error while saving Service Info.";
            }
            return Json(result);
        }

        public ActionResult Description(int id)
        {
            try
            {
                var serviceInfo = serviceInfoService.Find(o => o.ServiceId == id);
                if (serviceInfo != null)
                {
                    return View(serviceInfo);
                }
                else
                {
                    return ErrorResult("Valid Service Info not found");
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                return ErrorResult("Error while obtaining service information.");
            }
        }


        [HttpPost]
        [ValidateInput(false)]
        public JsonResult Description(Data.Entity.Entity.Service model)
        {
            var result = new ResponseObject();

            try
            {
                var serviceInfo = serviceInfoService.Find(o => o.ServiceId == model.ServiceId);
                if (serviceInfo != null)
                {
                    serviceInfo.Description = model.Description;

                    serviceContext.Commit();
                    result.IsSuccess = true;
                    result.Message = "Service Description information updated successfully.";

                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "Invalid Service Information provided to update service description.";
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving service Description.";
            }

            return Json(result);
        }
    }
}