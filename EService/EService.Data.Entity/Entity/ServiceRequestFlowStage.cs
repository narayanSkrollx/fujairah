using System.Collections.Generic;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class ServiceRequestFlowStage
    {
        public int ServiceRequestFlowStageId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public int Index { get; set; }
        public bool IsCurrentStage { get; set; }

        [JsonIgnore]
        public UserServiceRequestMaster UserServiceRequestMaster { get; set; }

        [JsonIgnore]
        public virtual ICollection<ServiceRequestFlowStageDepartment> ServiceRequestFlowStageDepartments { get; set; }
    }
}