using System.Collections.Generic;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class Department : AuditableEntity
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public bool IsDefault { get; set; }

        [JsonIgnore]
        public virtual ICollection<User> Users { get; set; }
        [JsonIgnore]
        public virtual ICollection<ServiceApprovalStageDepartment> ServiceApprovalStageDepartments { get; set; }
        [JsonIgnore]
        public virtual  ICollection<ServiceRequestFlowStageDepartment> ServiceRequestFlowStageDepartments { get; set; } 
    }
}