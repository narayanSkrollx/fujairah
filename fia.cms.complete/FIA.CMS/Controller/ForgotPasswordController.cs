﻿using System;
using System.Linq;
using System.Web.Mvc;
using FIA.CMS.Entities;
using FIA.CMS.Model;
using umbraco.BusinessLogic;
using Umbraco.Core.Persistence;

namespace FIA.CMS.Controller
{
    public class ForgotPasswordController : BaseController
    {
        [HttpPost]
        public JsonResult Verify(string email)
        {
            var result = new JsonResponseObject();
            try
            {
                var query = new Sql().Select("*").From<UserLogin>(SqlSyntaxProvider).Where<UserLogin>(x => x.Email == email, SqlSyntaxProvider);
                var user = Db.Fetch<UserLogin>(query).FirstOrDefault();
                if (user != null)
                {
                    var token = Guid.NewGuid();
                    var forgotPassword = new ForgotPasswordRequest()
                    {
                        Email = email,
                        ExpiryTime = DateTime.Now.AddHours(24),
                        IsUsed = false,
                        Token = token,
                        UserId = user.Id,

                    };
                    Db.Insert(forgotPassword);
                    Db.CompleteTransaction();
                    var model = new ForgotPasswordEmailModel()
                    {
                        Email = email,
                        Token = token.ToString("N")
                    };
                    var content = RenderPartialView("~/Views/EmailTemplates/_ForgotPasswordLinkEmailTemplate.cshtml", model);
                    this.SendEmail(content, email, "Forgot Password Reset");
                }
                result.IsSuccess = true;
                result.Message = "An email with reset link has been sent to your email. Please check your email.";
            }
            catch (Exception ex)
            {
                //Log.Add(LogRy);
                result.IsSuccess = false;
                result.Message = "Error whlie processing reset password request.";
            }
            return Json(result);
        }

        public ActionResult Reset(string token)
        {
            var model = new JsonResponseObject();
            try
            {
                Guid guidToken = Guid.Parse(token);
                var forgotPasswordQuery = "select * from ForgotPasswordRequest Where Token=@0 and IsUsed = '0' and ExpiryTime >= @1";
                var forgotPasswordRequest = Db.Query<ForgotPasswordRequest>(forgotPasswordQuery, guidToken, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")).FirstOrDefault();
                if (forgotPasswordRequest == null)
                {
                    model.IsSuccess = false;
                    model.Message = "Invalid Token or expired.";
                }
                else
                {
                    model.IsSuccess = true;
                    model.Data = new ResetPasswordViewModel
                    {
                        Token = forgotPasswordRequest.Token.ToString("N"),
                    };
                }
            }
            catch (Exception ex)
            {
                model.IsSuccess = false;
                model.Message = "Error while processing your forgot password request.";
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Reset(ResetPasswordViewModel model)
        {
            var result = new JsonResponseObject();
            try
            {
                Guid guidToken = Guid.Parse(model.Token);
                var forgotPasswordQuery = "select * from ForgotPasswordRequest Where Token=@0 and IsUsed = '0' and ExpiryTime >= @1";
                var forgotPasswordRequest = Db.Query<ForgotPasswordRequest>(forgotPasswordQuery, guidToken, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")).FirstOrDefault();
                if (forgotPasswordRequest == null)
                {
                    result.IsSuccess = false;
                    result.Message = "Invalid Token or expired.";
                }
                else
                {
                    var userlogin = Db.Query<UserLogin>("Select * from UserLogin where Id = @0", forgotPasswordRequest.UserId).FirstOrDefault();
                    userlogin.Password = model.Password;
                    Db.Update(userlogin);
                    forgotPasswordRequest.IsUsed = true;
                    Db.Update(forgotPasswordRequest);
                    result.IsSuccess = true;
                    result.Message = "Password changed successfully.";
                    result.Data = new ResetPasswordViewModel
                    {
                        Token = forgotPasswordRequest.Token.ToString("N"),
                    };
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "Error whlie processing password request form.";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}