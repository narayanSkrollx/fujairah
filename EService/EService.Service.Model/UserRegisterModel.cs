﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace EService.Service.Model
{
    public class UserRegisterModel
    {
        [Required(ErrorMessage = "Username is required.")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Password is required.")]
        [MinLength(6, ErrorMessage = "Password must be of at least 6 characters.")]
        public string PasswordHash { get; set; }
        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Email Address is invalid.")]
        public string Email { get; set; }
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "First Name is required.")]
        [StringLength(1024, ErrorMessage = "First name Length cannot be greater than 1024")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last Name is required.")]
        [StringLength(1024, ErrorMessage = "Last name Length cannot be greater than 1024")]
        public string LastName { get; set; }
        #region "User Contact Person"
        public string ContactPersonName { get; set; }
        public Int64? ContactPersonMobile { get; set; }
        public string EmiratesId { get; set; }
        #endregion
        #region "User Company Category"
        [Required(ErrorMessage = "Category must be selected.")]
        public int CompanyCategoryId { get; set; }
        [Required(ErrorMessage = "Company Name is required.")]
        public string CompanyName { get; set; }
        [Required(ErrorMessage = "Company Address is required.")]
        public string CompanyAddress { get; set; }
        [Required(ErrorMessage = "Company Phone is required.")]
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string POBox { get; set; }
        [Required(ErrorMessage = "Company City is required.")]
        public string City { get; set; }

        [Required(ErrorMessage = "Company Country is required.")]
        public string Country { get; set; }

        public string VATRegistrationNo { get; set; }
        #endregion

        #region ""
        [Required(ErrorMessage = "Security Question must be selected.")]
        public int SecurityQuestionId { get; set; }
        [Required(ErrorMessage = "Answer is required.")]
        public string Answer { get; set; }
        #endregion
    }
}
