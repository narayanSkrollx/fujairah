using System.ComponentModel.DataAnnotations;

namespace FIA.CMS.Model
{
    public class ResetPasswordViewModel
    {
        public string Token { get; set; }
        [Required]
        public string Password { get; set; }
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}