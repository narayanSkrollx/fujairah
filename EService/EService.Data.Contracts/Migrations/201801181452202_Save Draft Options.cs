namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SaveDraftOptions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserServiceRequestMaster", "IsDraft", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserServiceRequestMaster", "IsDraft");
        }
    }
}
