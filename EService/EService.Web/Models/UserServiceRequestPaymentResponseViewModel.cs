﻿using System.Collections.Generic;

namespace EService.Web.Models
{
    public class UserServiceRequestPaymentResponseViewModel
    {
        public string OrderNumber { get; set; }
        public bool IsReceivedOnSuccess { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string StatusFlag { get; set; }
        public string ErrorCode { get; set; }
        public string AllResponseJson { get; set; }
        public List<KeyValue> AllKeyValuePairs { get; set; }
    }
}