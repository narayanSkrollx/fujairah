﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EService.Service.Model
{
    public class BuildingHeightRequestServiceModel : EServiceBaseModal
    {

        public int UserServiceRequestMasterId { get; set; }

        public int? CurrentIndex { get; set; }
        #region "General Project Information"

        [Display(Name = "Project Name")]
        public string ProjectName { get; set; }
        [Display(Name = "Application Date")]
        public DateTime? ApplicationDate { get; set; }
        [Display(Name = "Expected Construction Start Date")]
        public DateTime? ExpectedConstructionStartDate { get; set; }
        [Display(Name = "Expected Construction End Date")]
        public DateTime? ExpectedConstructionEndDate { get; set; }
        [Display(Name = "Project Location")]
        public string Location { get; set; }
        [Display(Name = "Project Description")]
        public string ProjectDescription { get; set; }

        public bool IsChargeAmountUpdated { get; set; }
        public double ChargedAmount { get; set; }
        #region "Applicant"
        [Display(Name = "Signature")]
        public string ApplicantSignature { get; set; }

        [Display(Name = "Applicant's Name")]
        public string ApplicantName { get; set; }
        [Display(Name = "Applicant Job Title")]
        public string ApplicantJobTitle { get; set; }
        [Display(Name = "Applicant’s Company Name")]
        public string ApplicantCompanyName { get; set; }
        [Display(Name = "Applicant Address")]
        public string ApplicantAddress { get; set; }
        [Display(Name = "Cell")]
        public string ApplicantCell { get; set; }
        [Display(Name = "Email")]
        public string ApplicantEmail { get; set; }
        [Display(Name = "Website")]
        public string ApplicantWebsite { get; set; }
        [Display(Name = "Tel")]
        public string ApplicantTel { get; set; }
        #endregion

        #region "Owner Name"
        [Display(Name = "Owner’s Name")]
        public string OwnerName { get; set; }
        [Display(Name = "Owner’s Company Name")]
        public string OwnerCompanyName { get; set; }
        [Display(Name = "Owner's Address")]
        public string OwnerAddress { get; set; }
        [Display(Name = "Cell")]
        public string OwnerCell { get; set; }
        [Display(Name = "Tel")]
        public string OwnerTel { get; set; }
        [Display(Name = "Email")]
        public string OwnerEmail { get; set; }
        [Display(Name = "Website")]
        public string OwnerWebsite { get; set; }
        #endregion

        [Display(Name = "Building")]
        public bool IsBuilding { get; set; }
        [Display(Name = "OHL/Communication Tower")]
        public bool IsOHLCommunicationTower { get; set; }
        [Display(Name = "Other")]
        public bool IsOtherType { get; set; }
        public string OtherTypeDescription { get; set; }

        public int ProjectTypeId { get; set; }
        public string SelectedProjectDescription { get; set; }
        public string OtherProjectTypeDescription { get; set; }
        #endregion

        #region "Buildings Details Only"
        /// <summary>
        /// true for singular building and false for multiple buildings
        /// </summary>
        [Display(Name = "Development Type?")]
        public bool? BuildingType { get; set; }
        [Display(Name = "How many?")]
        public int? NumberOfBuildings { get; set; }
        [Display(Name = "No. of floors")]
        public int? NumberOfFloors { get; set; }
        [Display(Name = "Tallest Height (in meter)")]
        public double? TallestHeight { get; set; }

        [Display(Name = "Do you plan to use any obstacle lights on your building(s)?")]
        public bool? HasObstacleLight { get; set; }
        [Display(Name = "If yes, provide details & locations in your drawings")]
        public string DetailsOfLightinDrawing { get; set; }

        #endregion

        #region "OHL/Communication tower projet details only"

        [Display(Name = "Type")]
        public bool? OHLOrCommunicationType { get; set; }
        [Display(Name = "No. of Structures")]
        public int? NumberOfStructure { get; set; }
        [Display(Name = "Type of OHLs/Comm. Tower ? ")]
        public string TypeOfOHLCommTower { get; set; }
        [Display(Name = "Are there mountains within 1000 meters of your structures ? ")]
        public bool? AreThereMountains { get; set; }

        #endregion

        #region"Applicant Owner Relationship"
        public int ApplicantOwnerRelationshipId { get; set; }
        public string ApplicantOwnerRelationshipName { get; set; }
        public string ApplicantOwnerRelationshipOtherName { get; set; }
        #endregion

        public OHLStructureCoordinateAndHeightServiceModel OHLStructureCoordinateAndHeightAddModel { get; set; }

        public List<BuildingCoordinateAndHeightServiceModel> BuildingCoordinateAndHeights { get; set; }
        public List<AdditionalItemsForBuildingSubmittalServiceModel> AdditionalItemsForBuildingSubmittals { get; set; }
        public List<AviationObstacleServiceModel> AviationObstacles { get; set; }
        public List<OHLStructureCoordinateAndHeightServiceModel> OHLStructureCoordinateAndHeights { get; set; }
        public List<AdditionalItemsForOHLSubmittalServiceModel> AdditionalItemsForOHLSubmittals { get; set; }
        public List<BuildingHeightApplicableFeeServiceModel> BuildingHeightApplicableFees { get; set; }

        public double TotalBuildingHeightApplicableFee => BuildingHeightApplicableFees.Sum(o => o.TotalFee);

        public BuildingHeightRequestServiceModel()
        {
            OHLStructureCoordinateAndHeights = new List<OHLStructureCoordinateAndHeightServiceModel>();
            OHLStructureCoordinateAndHeightAddModel = new OHLStructureCoordinateAndHeightServiceModel();
            PlanningApproveNewModel = new PlanningApprove();
        }

        public PlanningApprove PlanningApprovalModel { get; set; }
        public PlanningApprove PlanningApproveNewModel { get; set; }
        public bool IsPlanningApprovalModelSaved => PlanningApprovalModel != null;

        public class PlanningApprove
        {
            public string Remarks { get; set; }
            public List<UploadedFile> UploadedFiles { get; set; }
        }
    }

    public class ApplicantOwnerRelationshipServiceModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool IsSelected { get; set; }
        public bool IsOther { get; set; }
        public string OtherValue { get; set; }
    }

    public class BuildingCoordinateAndHeightServiceModel
    {
        public int UserServiceRequestMasterId { get; set; }
        public int BuildingCoordinateAndHeightLookUpId { get; set; }
        public string BuildingCoordinateAndHeightLookUpDescription { get; set; }
        public string Easting { get; set; }
        public string Northing { get; set; }
        public double? GroundHeight { get; set; }
        public double? StructureHeight { get; set; }
    }

    public class AdditionalItemsForBuildingSubmittalServiceModel
    {
        public int AdditionalItemsForBuildingSubmittalId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public int AdditionalItemsForBuildingSubmittalLookUpId { get; set; }
        public string AdditionalItemsForBuildingSubmittalLookUpDescription { get; set; }
        public bool IsOther { get; set; }
        public string OtherDescription { get; set; }
        public bool IsSelected { get; set; }
    }

    public class AviationObstacleServiceModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool IsSelected { get; set; }
    }

    public class OHLStructureCoordinateAndHeightServiceModel
    {
        public int OHLStructureCoordinateAndHeightId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public string StructureReferenceNumber { get; set; }
        public string Easting { get; set; }
        public string Northing { get; set; }
        public double GroundHeight { get; set; }
        public double StructureHeight { get; set; }
    }

    public class AdditionalItemsForOHLSubmittalServiceModel
    {
        public int AdditionalItemsForOHLSubmittalId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public int AdditionalItemsForOHLSubmittalLookUpId { get; set; }
        public string AdditionalItemsForOHLSubmittalLookUpDescription { get; set; }
        public bool IsOther { get; set; }
        public string OtherDescription { get; set; }
        public bool IsSelected { get; set; }
    }

    public class BuildingHeightApplicableFeeServiceModel
    {
        public int ApplicableFeesLookUpId { get; set; }
        public int UserServiceRequestMasterId { get; set; }
        public string ApplicableFeesLookUpDescription { get; set; }
        public string Notes { get; set; }
        public string Fees { get; set; }
        public decimal? Fee { get; set; }

        public bool IsSelected { get; set; }
        public double Quantity { get; set; }
        public double TotalFee { get; set; }
    }
}
