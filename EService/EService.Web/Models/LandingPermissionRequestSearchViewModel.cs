using System.Collections.Generic;
using System.Web.Mvc;
using EService.Data.Entity;

namespace EService.Web.Models
{
    public class LandingPermissionRequestSearchViewModel : DefaultSearchViewModel
    {
        public List<LookupItem> AirportLandingList { get; set; }
        public string AircraftRegistration { get; set; }
        public string RefNo { get; set; }
        public string PermitNumber { get; set; }
        public string FlightNumber { get; set; }
        public int? AirportOfOperation { get; set; }
    }
}