using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class AircraftTypeConfiguration : EntityTypeConfiguration<AircraftType>
    {
        public AircraftTypeConfiguration()
        {
            ToTable("AircraftType");

            HasKey(o => o.Id);

            Property(o => o.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(o => o.Description).IsRequired().HasMaxLength(255);
            Property(o => o.Active);
            Property(o => o.IsOther);
        }
    }

    public class AircraftInteriorCleaningConfiguation : EntityTypeConfiguration<AircraftInteriorCleaning>
    {
        public AircraftInteriorCleaningConfiguation()
        {
            ToTable("AircraftInteriorCleaning");

            HasKey(o => o.UserServiceRequestMasterId);

            Property(o => o.UserServiceRequestMasterId)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(o => o.PowerType).IsRequired().HasMaxLength(255);
            Property(o => o.AdditionalRequirements).IsOptional().HasMaxLength(1024);

            HasRequired(o => o.GroundHandlingRequest).WithRequiredDependent(o => o.AircraftCleaning).WillCascadeOnDelete(false);
        }
    }

    public class GroundHandlingRequestAircraftTypeConfiguration :
        EntityTypeConfiguration<GroundHandlingRequestAircraftType>
    {
        public GroundHandlingRequestAircraftTypeConfiguration()
        {
            ToTable("GroundHandlingRequestAircraftType");

            HasKey(o => o.GroundHandlingRequestAircraftTypeId);

            Property(o => o.GroundHandlingRequestAircraftTypeId)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(o => o.UserServiceRequestMasterId).IsRequired();
            Property(o => o.AircraftTypeId).IsRequired();

            HasRequired(o => o.AircraftType).WithMany().HasForeignKey(o => o.AircraftTypeId).WillCascadeOnDelete(false);
            HasRequired(o => o.GroundHandlingRequest).WithMany().HasForeignKey(o => o.UserServiceRequestMasterId).WillCascadeOnDelete(false);
        }
    }

    public class GroundHandlingRequestConfiguration : EntityTypeConfiguration<GroundHandlingRequest>
    {
        public GroundHandlingRequestConfiguration()
        {
            ToTable("GroundHandlingRequest");

            HasKey(o => o.UserServiceRequestMasterId);

            Property(o => o.UserServiceRequestMasterId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(o => o.InboundDateTimeUTC).IsRequired();
            Property(o => o.From).IsRequired().HasMaxLength(1024);
            Property(o => o.ETAUTC).IsRequired();
            Property(o => o.OutboundDateTimeUTC).IsRequired();
            Property(o => o.ETDUTC).IsRequired();

            Property(o => o.CompanyName).IsRequired().HasMaxLength(1024);
            Property(o => o.FirstName).IsRequired().HasMaxLength(1024);
            Property(o => o.LastName).IsRequired().HasMaxLength(1024);
            Property(o => o.Address).IsRequired().HasMaxLength(1024);
            Property(o => o.Email).IsRequired().HasMaxLength(1024);
            Property(o => o.Phone).IsRequired().HasMaxLength(1024);
            Property(o => o.Fax).IsOptional().HasMaxLength(1024);
            Property(o => o.SITATEX).IsOptional().HasMaxLength(1024);

            Property(o => o.CoLTypeOfDG).IsOptional().HasMaxLength(1024);
            Property(o => o.CoLOtherSpecialCargo).IsOptional().HasMaxLength(1024);

            Property(o => o.CLTypeOfDG).IsOptional().HasMaxLength(1024);
            Property(o => o.CLOtherSpecialCargo).IsOptional().HasMaxLength(1024);

            HasRequired(o=>o.UserServiceRequestMaster).WithRequiredDependent(o=>o.GroundHandlingRequest).WillCascadeOnDelete(false);
        }
    }
}