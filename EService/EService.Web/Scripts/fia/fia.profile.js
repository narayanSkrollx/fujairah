﻿$(function () {
    fia.CreateNamespace("profile");

    (function (context) {
        context.Title = 'Profile';
        context.ViewModel = {
            Save: function () {
                if ($('#userRegisterForm').valid()) {
                    var model = ko.mapping.toJS(context.ViewModel.AddModel);
                    console.log(model);
                    //post
                    ajaxRequest('/Profile/Index', 'POST', {
                        data: { model:  model},
                        enableLadda: true,
                        targetLaddaElement: '#btnRegister',
                    }, function (resp) {
                        if (resp.IsSuccess) {
                            showMessage('Your profile information has been updated successfully.', 'Profile', 'success', function () {
                            });
                        } else {
                            showMessage(resp.Message, 'Profile', 'error');
                        }
                    });
                } else {
                    //alert

                }
            },
            RenderComplete : function () {
                context.ApplyValidation();
                console.log('render complete');
            }
        };

        context.Mapping = {
            create: function (options) {
                var vm = ko.mapping.fromJS(options.data);

              

                return vm;
            }
        }

        context.Initialize = function () {
            ajaxRequest('/Profile/Initialize', 'GET', {}, function (resp) {
                if (resp.IsSuccess) {
                    var vm = resp.Data;

                    if (ko.dataFor($("#mainContent")[0]) == null) {
                        context.ViewModel.AddModel = ko.mapping.fromJS(vm, context.Mapping);

                        ko.applyBindings(context.ViewModel, $('#mainContent')[0]);
                    } else {
                        ko.mapping.fromJS(vm, {}, context.ViewModel.AddModel);
                    }
                } else {
                    alert(resp.Message);
                }
            });
        }


        context.ApplyValidation = function () {
            $('#userRegisterForm').validate({
                rules: {
                    FirstName: { required: true, maxlength: 1024 },
                    LastName: { required: true, maxlength: 1024 },
                    UserName: { required: true },
                    PasswordHash: { required: true, minlength: 6 },
                    ConfirmPassword: { required: true, equalTo: "#PasswordHash" },
                    Email: { required: true, email: true },
                    CompanyCategoryId: { required: true },
                    SecurityQuestionId: { required: true },
                    CompanyName: { required: true, maxlength: 255 },
                    CompanyAddress: { required: true, maxlength: 255 },
                    Phone: { required: true, maxlength: 25, regex: phoneNoRegex },
                    City: { required: true, maxlength: 255, },
                    Country: { required: true, maxlength: 255 },
                },
                messages: {
                    FirstName: { required: 'First Name is required.', maxlength: 'First Name length cannot be greater than 1024' },
                    LastName: { required: 'Last Name is required.', maxlength: 'Last Name length cannot be greater than 1024' },
                    UserName: { required: 'UserName is required.' },
                    PasswordHash: { required: 'Password is required', minlength: 'Password must be of at least 6 characters.' },
                    ConfirmPassword: { required: 'Confirm password is required.', equalTo: "Confirm Password must match to Password" },
                    Email: { required: 'Email is required.', email: 'Email must be a valid email address' },
                    CompanyCategoryId: { required: 'Category must be selected.' },
                    SecurityQuestionId: { required: 'Security Question must be selected.' },
                    Answer: { required: 'Answer for security question must be given.' },
                    CompanyName: { required: 'Company Name is required', maxlength: 'Company Name length cannot be greater than 255' },
                    CompanyAddress: { required: 'Company Address is required', maxlength: 'Company Address length cannot be greater than 255' },
                    Phone: { required: 'Company Phone is required', maxlength: 'Company Phone length cannot be greater than 25' },
                    City: { required: 'City is required', maxlength: 'City length cannot be greater than 255' },
                    Country: { required: 'Country is required.', maxlength: 'Country length cannot be greater than 255' },
                }
            });
        }

    })(fia.profile);
});