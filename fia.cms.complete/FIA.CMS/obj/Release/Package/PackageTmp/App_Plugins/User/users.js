﻿

angular.module("umbraco").controller("user.users",
    function ($scope, $http, notificationsService) {
        $scope.list = [];


        $scope.GetUsers = function () {
            $http.get("/umbraco/surface/Manage/GetUsers").then(function (result) {
                if (result && result.data) {
                    $scope.list = result.data;

                }
            });
        }
        $scope.changePassword = function(userId) {
            UmbClientMgr.openAngularModalWindow({
                template: '/app_plugins/user/changePassword.html',
                dialogData: { userId: userId },
               callback: function(data) {
                   
                }
            });
          
        }
        $scope.GetUsers();
    });