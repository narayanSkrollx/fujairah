﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsletterSettings.ascx.cs"
    Inherits="Designit.Umb.Newsletter.Custom.usercontrols.designit.newsletter.NewsletterSettings" %>
<%@ Register Namespace="umbraco.uicontrols" Assembly="controls" TagPrefix="umb" %>
<umb:UmbracoPanel ID="Panel1" runat="server" hasMenu="true">
    <umb:Pane ID="Pane1" runat="server" Height="44px" Text="Default settings">
        <umb:PropertyPanel ID="pp_name" runat="server" Text="Default sender name">
            <asp:TextBox ID="txtName" Width="350px" runat="server"></asp:TextBox>
        </umb:PropertyPanel>
        <umb:PropertyPanel ID="pp_mail" runat="server" Text="Default sender mail">
            <asp:TextBox ID="txtMail" Width="350px" runat="server"></asp:TextBox>
        </umb:PropertyPanel>
        <umb:PropertyPanel ID="pp_timezone" runat="server" Text="Default sender timezone">
            <asp:DropDownList ID="TimezoneList" runat="server">
            </asp:DropDownList>
        </umb:PropertyPanel>
        <umb:PropertyPanel ID="pp_testMail" runat="server" Text="Default Test reciever">
            <asp:TextBox ID="txttestMail" Width="350px" runat="server"></asp:TextBox>
        </umb:PropertyPanel>
    </umb:Pane>
    <umb:Pane ID="Pane2" runat="server" Height="44px" Text="Mail server settings">
        <umb:PropertyPanel ID="pp_pop3" runat="server" Text="POP3 server">
            <asp:TextBox ID="txtPOP3" Width="350px" runat="server"></asp:TextBox>
            <asp:Literal ID="LiteralPopPort" Text="Port" runat="server"></asp:Literal>
            <asp:TextBox ID="txtPopPort" runat="server"></asp:TextBox>
        </umb:PropertyPanel>
        <umb:PropertyPanel ID="pp_User" runat="server" Text="Pop3 username">
            <asp:TextBox ID="txtUserID" Width="350px" runat="server"></asp:TextBox>
        </umb:PropertyPanel>
        <umb:PropertyPanel ID="pp_Pass" runat="server" Text="Pop3 password">
            <asp:TextBox ID="txtPassword" Width="350px" runat="server" TextMode="Password"></asp:TextBox>
        </umb:PropertyPanel>
        <umb:PropertyPanel ID="pp_smtp" runat="server" Text="SMTP server">
            <asp:TextBox ID="txtSMTP" Width="350px" runat="server"></asp:TextBox>
            <asp:Literal ID="LiteralPort" Text="Port" runat="server"></asp:Literal>
            <asp:TextBox ID="txtPort" runat="server"></asp:TextBox>
        </umb:PropertyPanel>
        <umb:PropertyPanel ID="pp_smtpAuth" runat="server" Text="Smtp authentication required:">
            <asp:CheckBox ID="authcheck" runat="server" />
        </umb:PropertyPanel>
    </umb:Pane>
    <umb:Pane ID="Pane3" runat="server" Height="44px" Text="Bounce handler settings">
        <umb:PropertyPanel ID="pp_SoftBounce" runat="server" Text="Enable soft bounce">
            <asp:CheckBox ID="softBounceCheck" runat="server" />
        </umb:PropertyPanel>
        <umb:PropertyPanel ID="pp_Bounce" runat="server" Text="Check interval (hours)">
            <asp:TextBox ID="txtBounce" Width="350px" runat="server"></asp:TextBox>
        </umb:PropertyPanel>
    </umb:Pane>
    <umb:Pane ID="Pane4" runat="server" Height="44px" Text="Link settings">
        <umb:PropertyPanel ID="pp_server" runat="server" Text="Domain:">
            <asp:TextBox ID="txtServer" Width="350px" runat="server" Text="http://"></asp:TextBox>
        </umb:PropertyPanel>
        <umb:PropertyPanel ID="pp_unsub" runat="server" Text="Unsubscribe page">
            <asp:TextBox ID="txtunsub" Width="350px" runat="server"></asp:TextBox>
        </umb:PropertyPanel>
        <umb:PropertyPanel ID="pp_confirmUnsub" runat="server" Text="Confirm Unsubscribe page">
            <asp:TextBox ID="txtconfirmUnsub" Width="350px" runat="server"></asp:TextBox>
        </umb:PropertyPanel>
        <umb:PropertyPanel ID="pp_googleTrack" runat="server" Text="Google track sites(1 each line)">
            <asp:TextBox ID="txtLocalSites" Width="350px" runat="server" TextMode="MultiLine"></asp:TextBox>
        </umb:PropertyPanel>
    </umb:Pane>
</umb:UmbracoPanel>
<asp:Button ID="ButtonSave" runat="server" OnClick="ButtonSave_Click" Text="Save settings" Visible="false" />