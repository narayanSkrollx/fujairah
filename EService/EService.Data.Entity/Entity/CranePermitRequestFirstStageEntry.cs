using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class CranePermitRequestFirstStageEntry
    {
        public int UserServiceRequestMasterId { get; set; }
        public int SavedByUserId { get; set; }
        public bool HasPentrationOfSurface { get; set; }
        public decimal? PenetrationInMeter { get; set; }
        public bool RequireAviationLight { get; set; }
        public string Comments { get; set; }
        public decimal? MaxCraneOperatingHeight { get; set; }
        public decimal? CraneOperatingDistanceFromRunway { get; set; }
        public string Recommendations { get; set; }
        public DateTime SavedDateTime { get; set; }
        public string AssessedBy { get; set; }
        public string AirportPlanner { get; set; }
        public DateTime? AssessedDate { get; set; }

        [JsonIgnore]
        public virtual User User { get; set; }

        [JsonIgnore]
        public virtual CranePermitRequest CranePermitRequest { get; set; }

        [JsonIgnore]
        public virtual ICollection<CranePermitRequestFirstStageInfringementType> CranePermitRequestFirstStageInfringementTypes
        { get; set; }

        public virtual ICollection<CranePermitPlanningAttachment> CranePermitPlanningAttachments { get; set; }
    }
}