﻿using System;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class UserServiceRequestOtherPayment : AuditableEntity
    {
        public int Id { get; set; }
        public int UserServiceRequestMasterId { get; set; }

        public int? PaymentMode { get; set; }
        public double? PaidAmount { get; set; }
        public int? PaidByUserId { get; set; }
        public DateTime? PaidRequestDateTime { get; set; }
        public string PaidRequestedByUserName { get; set; }


        public string OrderNumber { get; set; }
        public bool IsReceivedOnSuccess { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string StatusFlag { get; set; }
        public string ErrorCode { get; set; }
        public string AllResponseJson { get; set; }

        [JsonIgnore]
        public virtual UserServiceRequestMaster UserServiceRequestMaster { get; set; }
        
    }
}