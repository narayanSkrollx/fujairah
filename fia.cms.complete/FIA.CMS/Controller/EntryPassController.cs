﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using FIA.CMS.Helper;
using FIA.CMS.Model;

namespace FIA.CMS.Controller
{
    public class EntryPassController : BaseController
    {
        public ActionResult RenderForm(string entryPassType)
        {
            if (entryPassType.ToLower().Equals("vehicle"))
            {
                var list = new List<EntryPassAttachments>();
                list.Add(new EntryPassAttachments() { Document = "Letter from the company" });
                list.Add(new EntryPassAttachments() { Document = "Registration card of vehicle" });
                list.Add(new EntryPassAttachments() { Document = "Driving license" });
                list.Add(new EntryPassAttachments() { Document = "Passport copy / Emirates ID" });
                list.Add(new EntryPassAttachments() { Document = "Photo" });
                list.Add(new EntryPassAttachments() { Document = "Self-introductory statement" });
                list.Add(new EntryPassAttachments() { Document = "Others" });
                var model = new EntryPassVehicleModel { Attachments = list };

                return PartialView($"{HomePartialViewLocation}_EntryPassVehicle.cshtml", model);
            }
            else
            {
                var list = new List<EntryPassAttachments>();
                list.Add(new EntryPassAttachments() { Document = "Letter from the company" });
                list.Add(new EntryPassAttachments() { Document = "Passport copy / Emirates ID" });
                list.Add(new EntryPassAttachments() { Document = "Visa copy" });
                list.Add(new EntryPassAttachments() { Document = "Photo" });
                list.Add(new EntryPassAttachments() { Document = "Self-introductory statement" });
                list.Add(new EntryPassAttachments() { Document = "Others" });

                var model = new EntryPassIndividualModel() { Attachments = list };

                return PartialView($"{HomePartialViewLocation}_EntryPassIndividual.cshtml", model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitFormVehicle(EntryPassVehicleModel model)
        {
            if (ModelState.IsValid)
            {
                var attachments = new List<Attachment>();
                var to = EmailSenderAddress.GetSenderEmail("EntryPass");
                if (model.Attachments != null && model.Attachments.Any())
                {
                    foreach (var item in model.Attachments.Where(x => x.File != null))
                    {
                        var attachment = new Attachment(item.File.InputStream, item.File.FileName);
                        attachments.Add(attachment);
                    }
                }
                var genericEmailModel = new GenericEmailModel()
                {
                    Context = "Vehicle Entry Pass"
                };
                base.SendEmail(AutoReplyToUser(genericEmailModel), model.Email, "NO REPLY-FIA");
                base.SendEmail(GetMessageBodyVehicle(model), to, $"Entry pass request from {model.DriverName}", attachments); TempData["SuccessMsg"] = "Thank you for submitting us your query. We will get back to you shortly.";
                return RedirectToCurrentUmbracoPage();
            }

            return CurrentUmbracoPage();
        }

        private string GetMessageBodyVehicle(EntryPassVehicleModel model)
        {
            var messagebody = "<table>";

            messagebody += $"<tr><td>Entry Type</td><td>Vehicle</td></tr>";
            if (!string.IsNullOrWhiteSpace(model.VehicleRegistrationNr))
            {
                messagebody += $"<tr><td>Vehicle’s Registration No.</td><td>{model.VehicleRegistrationNr}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.DriverName))
            {
                messagebody += $"<tr><td>Name of the Driver</td><td>{model.DriverName}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.PassportNr))
            {
                messagebody += $"<tr><td>Passport no.</td><td>{model.PassportNr}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.Religion))
            {
                messagebody += $"<tr><td>Religion</td><td>{model.Religion}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.Sect))
            {
                messagebody += $"<tr><td>Sect</td><td>{model.Sect}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.VisaType))
            {
                messagebody += $"<tr><td>Visa Type</td><td>{model.VisaType}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.EmiratesId))
            {
                messagebody += $"<tr><td>Emirates Id</td><td>{model.EmiratesId}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.LicenseNr))
            {
                messagebody += $"<tr><td>License No. of the Driver</td><td>{model.LicenseNr}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.VisitArea))
            {
                messagebody += $"<tr><td>Area of Visit</td><td>{model.VisitArea}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.VisitPurpose))
            {
                messagebody += $"<tr><td>Purpose of Visit</td><td>{model.VisitPurpose}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.EntryDate.ToString()))
            {
                messagebody += $"<tr><td>Date of Entry</td><td>{model.EntryDate}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.ExitDate.ToString()))
            {
                messagebody += $"<tr><td>Date of Exit</td><td>{model.ExitDate}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.Email))
            {
                messagebody += $"<tr><td>Email</td><td>{model.Email}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.OfficePhone))
            {
                messagebody += $"<tr><td>Telephone (Off.)</td><td>{model.OfficePhone}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.MobilePhone))
            {
                messagebody += $"<tr><td>Telephone (Mob.) </td><td>{model.MobilePhone}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.Fax))
            {
                messagebody += $"<tr><td>Fax</td><td>{model.Fax}</td></tr>";
            }

            messagebody += "</table>";
            return messagebody;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitFormIndividual(EntryPassIndividualModel model)
        {
            if (ModelState.IsValid)
            {
                var attachments = new List<Attachment>();
                var to = EmailSenderAddress.GetSenderEmail("EntryPass");
                if (model.Attachments != null && model.Attachments.Any())
                {
                    foreach (var item in model.Attachments.Where(x => x.File != null))
                    {
                        var attachment = new Attachment(item.File.InputStream, item.File.FileName);
                        attachments.Add(attachment);
                    }
                }
                var genericEmailModel = new GenericEmailModel()
                {
                    Context = "Individual Entry Pass"
                };
                base.SendEmail(AutoReplyToUser(genericEmailModel), model.Email, "NO REPLY-FIA ");
                base.SendEmail(GetMessageBodyIndividual(model), to, $"Entry pass request from {model.CompanyName}", attachments); TempData["SuccessMsg"] = "Thank you for submitting us your query. We will get back to you shortly.";
                return RedirectToCurrentUmbracoPage();
            }

            return CurrentUmbracoPage();
        }

        private string AutoReplyToUser(GenericEmailModel model)
        {
            return RenderPartialView("~/Views/Partials/EmailTemplates/_GenericEmailResponse.cshtml", model);
        }

        private string GetMessageBodyIndividual(EntryPassIndividualModel model)
        {
            var messagebody = "<table>";

            messagebody += $"<tr><td>Entry Type</td><td>Individual</td></tr>";
            if (!string.IsNullOrWhiteSpace(model.VisitorName))
            {
                messagebody += $"<tr><td>Name of vistor</td><td>{model.VisitorName}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.Nationality))
            {
                messagebody += $"<tr><td>Nationality</td><td>{model.Nationality}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.PassportNr))
            {
                messagebody += $"<tr><td>Passport no.</td><td>{model.PassportNr}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.VisaType))
            {
                messagebody += $"<tr><td>Visa Type</td><td>{model.VisaType}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.EmiratesId))
            {
                messagebody += $"<tr><td>Emirates Id</td><td>{model.EmiratesId}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.Religion))
            {
                messagebody += $"<tr><td>Religion</td><td>{model.Religion}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.Sect))
            {
                messagebody += $"<tr><td>Sect</td><td>{model.Sect}</td></tr>";
            }

            if (!string.IsNullOrWhiteSpace(model.TitleJob))
            {
                messagebody += $"<tr><td>Title/Job</td><td>{model.TitleJob}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.CompanyName))
            {
                messagebody += $"<tr><td>Name of company</td><td>{model.CompanyName}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.Address))
            {
                messagebody += $"<tr><td>Address</td><td>{model.Address}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.VisitPurpose))
            {
                messagebody += $"<tr><td>Purpose of Visit</td><td>{model.VisitPurpose}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.VisitArea))
            {
                messagebody += $"<tr><td>Area of Visit</td><td>{model.VisitArea}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.EntryDate.ToString()))
            {
                messagebody += $"<tr><td>Date of Entry</td><td>{model.EntryDate}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.ExitDate.ToString()))
            {
                messagebody += $"<tr><td>Date of Exit</td><td>{model.ExitDate}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.Email))
            {
                messagebody += $"<tr><td>Email</td><td>{model.Email}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.OfficePhone))
            {
                messagebody += $"<tr><td>Telephone (Off.)</td><td>{model.OfficePhone}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.MobilePhone))
            {
                messagebody += $"<tr><td>Telephone (Mob.) </td><td>{model.MobilePhone}</td></tr>";
            }
            if (!string.IsNullOrWhiteSpace(model.Fax))
            {
                messagebody += $"<tr><td>Fax</td><td>{model.Fax}</td></tr>";
            }

            messagebody += "</table>";
            return messagebody;
        }
    }
}