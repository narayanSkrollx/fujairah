using System;

namespace EService.Web.Models.ServiceNotification
{
    public class ApprovedServiceRequestNotificationViewModel : ServiceNotificationBaseViewModel
    {
        public int ApprovedByDepartmentUserId { get; set; }
        public string ApprovedByDepartmentUserName { get; set; }
        public DateTime ApprovedByDepartmentDateTime { get; set; }
        public string NextDepartmentName { get; set; }
    }
}