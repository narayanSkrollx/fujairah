using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EService.Web.App_LocalResources;

namespace EService.Web.Models
{
    public class MeteorologicalDataRequestViewModel : EServiceCreateBaseViewModel, IValidatableObject
    {
        public DateTime ApplicationDate { get; set; }
        public int UserServiceRequestMasterId { get; set; }

        #region "Contact Information"

        [Required(ErrorMessageResourceType = typeof(MeteorologicalDataRequestResource), ErrorMessageResourceName = "EmailRequired")]
        public string Email { get; set; }
        public string Department { get; set; }
        [Required(ErrorMessageResourceType = typeof(MeteorologicalDataRequestResource), ErrorMessageResourceName = "PhoneNoRequired")]
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Address { get; set; }

        #endregion

        public bool DataRangeDaily { get; set; }
        public bool DataRangeHourly { get; set; }
        public bool DataRangeMonthly { get; set; }
        public bool DataRangeYearly { get; set; }
        public bool DataRangeSelectPeriod { get; set; }
        public DateTime? CustomRangeStartDate { get; set; }
        public DateTime? CustomRangeEndDate { get; set; }

        public List<MetWeatherTypeReportViewModel> WeatherTypes { get; set; }
        public List<MetWeatherElementReportViewModel> WeatherElements { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var result = new List<ValidationResult>();
            if ((CustomRangeEndDate == null || CustomRangeStartDate == null))
            {
                result.Add(new ValidationResult("Custom Date Range has to be entered."));
            }
            else if (CustomRangeEndDate < CustomRangeStartDate)
            {
                result.Add(new ValidationResult("Custom Range End Date must be greater than Start Date."));
            }

            return result;
            throw new NotImplementedException();
        }
    }

    public class MetWeatherTypeReportViewModel
    {
        public int WeatherTypeReportId { get; set; }
        public string WeatherTypeDescription { get; set; }
        public bool IsActive { get; set; }
    }

    public class MetWeatherElementReportViewModel
    {
        public int WeatherElementId { get; set; }
        public bool IsActive { get; set; }
        public string OtherName { get; set; }

        public string WeatherElementDescription { get; set; }
        public bool IsOther { get; set; }
    }
}