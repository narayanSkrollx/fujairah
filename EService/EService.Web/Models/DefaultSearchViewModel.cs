using System;

namespace EService.Web.Models
{
    public class DefaultSearchViewModel
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int? Status { get; set; }
    }
}