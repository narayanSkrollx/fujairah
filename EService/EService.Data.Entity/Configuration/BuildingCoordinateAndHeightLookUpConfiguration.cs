using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class BuildingCoordinateAndHeightLookUpConfiguration : EntityTypeConfiguration<BuildingCoordinateAndHeightLookUp>
    {
        public BuildingCoordinateAndHeightLookUpConfiguration()
        {
            ToTable("BuildingCoordinateAndHeightLookUp");
            HasKey(x => x.Id);

            Property(x => x.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Description).IsRequired();
            Property(x => x.Active).IsRequired();
        }
    }
}