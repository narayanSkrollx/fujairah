using System;
using System.Web;
using System.Web.Mvc;
using EService.Core;
using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Web.Controllers.Base;
using EService.Web.Models;

namespace EService.Web.Controllers
{
    public class FileUploadController : BaseController
    {
        private UserAttachmentService userAttachmentService;

        public FileUploadController()
        {
            userAttachmentService = new UserAttachmentService(serviceContext);
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase fila, int serviceId)
        {
            var result = new JsonResponseObject();

            try
            {
                var attachment = new UserAttachment()
                {
                    ContentType = fila.ContentType,
                    CreatedByUserId = UserProfile.UserId,
                    CreatedDate = DateTime.Now.DubaiTime(),
                    //FileContents = fila.InputStream.ReadFully(),
                    FileName = fila.FileName,
                    IsDeleted = false,
                    ServiceId = serviceId,
                    Timestamp = DateTime.Now.DubaiTime(),
                    UserId = UserProfile.UserId,
                    UserAttachmentData =  new UserAttachmentData()
                    {
                        FileContents = fila.InputStream.ReadFully()
                    }
                };
                userAttachmentService.SaveUserAttachment(attachment);
                result.IsSuccess = true;
                result.Message = "File attachment saved successfully.";
                result.Data = new { Id = attachment.UserAttachmentId, FileName = attachment.FileName };
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving file attachment";
            }
            return Json(result);
        }
    }
}