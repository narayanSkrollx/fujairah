using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Script.Serialization;

namespace EService.Core
{
    public static class HtmlExtension
    {
        public static SelectList ToSelectList<T>(this IQueryable<T> query, string dataValueField, string dataTextField, object selectedValue)
        {
            return new SelectList(query, dataValueField, dataTextField, selectedValue);
        }

        public static SelectList ToSelectList<T>(this IEnumerable<T> query, Expression<Func<T, object>> value, Expression<Func<T, object>> text, object selectedValue = null)
        {
            return new SelectList(query, value.ToPropertyName(), text.ToPropertyName(), selectedValue);
        }

        public static SelectList ToSelectList<T>(this IQueryable<T> items, Expression<Func<T, object>> value, Expression<Func<T, object>> text, object selectedValue = null)
        {
            return new SelectList(items, value.ToPropertyName(), text.ToPropertyName(), selectedValue);
        }
        

        #region Image Helper

        public static MvcHtmlString ImageActionLink(
            this HtmlHelper helper,
            string actionName,
            string controllerName,
            string toolTip,
            object routeValues)
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext, helper.RouteCollection);
            string imageUrl = urlHelper.Content(string.Format("~/Images/Icons/{0}.png", toolTip));
            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", imageUrl);
            imgBuilder.MergeAttribute("alt", toolTip);
            imgBuilder.MergeAttribute("title", toolTip);
            var linkBuilder = new TagBuilder("a");
            linkBuilder.MergeAttribute("href", urlHelper.Action(actionName, controllerName, routeValues));
            var text = linkBuilder.ToString(TagRenderMode.StartTag);
            text += imgBuilder.ToString(TagRenderMode.SelfClosing);
            text += linkBuilder.ToString(TagRenderMode.EndTag);
            return MvcHtmlString.Create(text);
        }

        public static MvcHtmlString ImageActionLink(
            this HtmlHelper helper,
            string actionName,
            string controllerName,
            object routeValues)
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext, helper.RouteCollection);
            string imageUrl = urlHelper.Content(string.Format("~/Images/Icons/{0}.png", actionName));
            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", imageUrl);
            imgBuilder.MergeAttribute("alt", actionName);
            imgBuilder.MergeAttribute("title", actionName);
            var linkBuilder = new TagBuilder("a");
            linkBuilder.MergeAttribute("href", urlHelper.Action(actionName, controllerName, routeValues));
            var text = linkBuilder.ToString(TagRenderMode.StartTag);
            text += imgBuilder.ToString(TagRenderMode.SelfClosing);
            text += linkBuilder.ToString(TagRenderMode.EndTag);
            return MvcHtmlString.Create(text);
        }

        public static MvcHtmlString ImageActionLink(
            this HtmlHelper helper,
            string actionName,
            string controllerName,
            object routeValues,
            object linkHtmlAttributes)
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext, helper.RouteCollection);
            string imageUrl = urlHelper.Content(string.Format("~/Images/Icons/{0}.png", actionName));
            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", imageUrl);
            imgBuilder.MergeAttribute("alt", actionName);
            imgBuilder.MergeAttribute("title", actionName);

            var linkAttributes = AnonymousObjectToKeyValue(linkHtmlAttributes);
            var linkBuilder = new TagBuilder("a");
            linkBuilder.MergeAttribute("href", urlHelper.Action(actionName, controllerName, routeValues));
            linkBuilder.MergeAttributes(linkAttributes, true);
            var text = linkBuilder.ToString(TagRenderMode.StartTag);
            text += imgBuilder.ToString(TagRenderMode.SelfClosing);
            text += linkBuilder.ToString(TagRenderMode.EndTag);
            return MvcHtmlString.Create(text);
        }

        private static Dictionary<string, object> AnonymousObjectToKeyValue(object anonymousObject)
        {
            var dictionary = new Dictionary<string, object>();
            if (anonymousObject != null)
            {
                foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(anonymousObject))
                {
                    dictionary.Add(propertyDescriptor.Name, propertyDescriptor.GetValue(anonymousObject));
                }
            }
            return dictionary;
        }

        #endregion Image Helper

        public static MvcHtmlString ToJson(this HtmlHelper html, object obj)
        {
            var serializer = new JavaScriptSerializer();
            return MvcHtmlString.Create(serializer.Serialize(obj));
        }

        public static MvcHtmlString ToJson(this HtmlHelper html, object obj, int recursionDepth)
        {
            var serializer = new JavaScriptSerializer
            {
                RecursionLimit = recursionDepth
            };
            return MvcHtmlString.Create(serializer.Serialize(obj));
        }


        public static string ResolveServerUrl(string serverUrl, bool forceHttps = false)
        {
            if (serverUrl.IndexOf("://", StringComparison.Ordinal) > -1)
                return serverUrl;

            var newUrl = serverUrl;
            var originalUri = HttpContext.Current.Request.Url;
            newUrl = (forceHttps ? "https" : originalUri.Scheme) +
                     "://" + originalUri.Authority + newUrl;
            return newUrl;
        }

        public static void PartialActionView(this HtmlHelper helper, string viewName)
        {
            var context = helper.ViewContext;

            var viewResult = ViewEngines.Engines.FindPartialView(context.Controller.ControllerContext, viewName);
            if (viewResult.View != null)
            {
                helper.RenderPartial(viewName);
            }
        }
    }
}