using System;
using System.Collections.Generic;
using System.Linq;
using EService.Service.Model;

namespace EService.Web.Models
{
    public abstract class EServiceCreateBaseViewModel
    {
        protected EServiceCreateBaseViewModel()
        {
            UploadedFiles = new List<FileUploadedViewModel>();
            EditUploadedFiles = new List<FileUploadedViewModel>();
        }
        public int? ParentUserServiceRequestMasterid { get; set; }
        public UserBasicInfoServiceModal UserInfo { get; set; }
        public List<FileUploadedViewModel> UploadedFiles { get; set; }
        public List<FileUploadedViewModel> EditUploadedFiles { get; set; }
        //public List<string> InitialPreview
        //{
        //    get
        //    {
        //        return UploadedFiles.Select(o => $"<img class='kv-preview-data file-preview-image'  src='data:{o.ContentType}; base64, {Convert.ToBase64String(o.Content)}' alt='{o.FileName}' />").ToList();
        //    }
        //}
        //public List<FileUploadedPreviewConfigViewModel> PreviewConfig
        //{
        //    get
        //    {
        //        return UploadedFiles.Select(o => new FileUploadedPreviewConfigViewModel
        //        {
        //            caption = o.FileName,
        //            key = o.Id,
        //            url = "/FileUpload/Delete",
        //            width = "120px"
        //        }).ToList();
        //    }
        //}
    }
}