using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using EService.Data.Entity.Entity;

namespace EService.Data.Entity.Configuration
{
    public class UserServiceRequestBillConfiguration : EntityTypeConfiguration<UserServiceRequestBill>
    {
        public UserServiceRequestBillConfiguration()
        {
            ToTable("UserServiceRequestBill");

            HasKey(o => o.UserServiceRequestMasterId);

            Property(o => o.UserServiceRequestMasterId)
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(o => o.ChargedAmount).IsRequired();
            Property(o => o.DiscountAmount).IsOptional();
            Property(o => o.PaymentMode).IsOptional();
            Property(o => o.PaidAmount).IsOptional();
            Property(o => o.PaidByUserId).IsOptional();
            Property(o => o.PaidDateTime).IsOptional();
            Property(o => o.PaidByUserName).IsOptional();

            HasRequired(o => o.UserServiceRequestMaster).WithRequiredDependent(o => o.ServieBill).WillCascadeOnDelete(false);
        }
    }


    public class UserServiceRequestOtherPaymentConfiguration : EntityTypeConfiguration<UserServiceRequestOtherPayment>
    {
        public UserServiceRequestOtherPaymentConfiguration()
        {
            ToTable("UserServiceRequestOtherPayment");

            HasKey(o => o.Id);

            Property(o => o.UserServiceRequestMasterId);
            Property(o => o.PaymentMode).IsOptional();
            Property(o => o.PaidAmount).IsOptional();
            Property(o => o.PaidByUserId).IsOptional();
            Property(o => o.PaidRequestDateTime).IsOptional(); 
            Property(o => o.PaidRequestedByUserName).IsOptional();
            Property(o => o.OrderNumber).IsRequired();
            Property(o => o.IsReceivedOnSuccess).IsRequired();
            Property(o => o.Amount).IsRequired();
            Property(o => o.Currency).IsRequired().HasMaxLength(255);
            Property(o => o.StatusFlag).IsRequired().HasMaxLength(255);
            Property(o => o.AllResponseJson).IsOptional().HasColumnType("nvarchar(max)");

            HasRequired(o => o.UserServiceRequestMaster).WithMany(o => o.UserServiceRequestOtherPayments).HasForeignKey(o => o.UserServiceRequestMasterId).WillCascadeOnDelete(false);
        }
    }
}