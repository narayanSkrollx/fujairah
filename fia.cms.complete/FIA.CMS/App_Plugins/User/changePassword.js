﻿

angular.module("umbraco").controller("user.changePassword",
    function ($scope, $http, notificationsService) {
       $scope.model= {
           password: "",
           confirmPassword:""
       }
       $scope.submit = function (isValid) {
           if (isValid) {
               $scope.model.Id = $scope.dialogData.userId;
               $http.post("/umbraco/surface/Manage/ChangePassword", $scope.model).then(function (result) {
                   if (result && result.data) {
                       $scope.close();
                       notificationsService.success("Success", "Successfully change password!");
                   }
               });
              
           } 
       };
    });