using System;

namespace EService.Web.Models.ServiceNotification
{
    public abstract class ServiceNotificationBaseViewModel
    {
        public int RequestedByUserId { get; set; }
        public string RequestedByUserName { get; set; }
        public string RequestedByUserFullName { get; set; }
        public string RequestedByUserEmail { get; set; }
        public DateTime RequestedDateTime { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string RequestRegistrationNo { get; set; }
    }
}