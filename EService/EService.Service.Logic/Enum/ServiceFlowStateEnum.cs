﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EService.Service.Logic.Enum
{
    public enum ServiceFlowStateEnum
    {
        New = 1,
        Active = 2,
        Approved = 3,
        Rejected = 4,
        PaymentPending = 5,
        PaymentCompleted = 6
    }

    public enum PaymentMode
    {
        Manual = 1,
        EPayment = 2
    }
}
