﻿using EService.Service.Logic.Enum;

namespace EService.Web.Models.ServiceRequestOperation
{
    public class ServiceReapplyTempDataViewModel
    {
        public int UserServiceRequestMasterId { get; set; }
        public ServiceEnum Service { get; set; }
    }
}