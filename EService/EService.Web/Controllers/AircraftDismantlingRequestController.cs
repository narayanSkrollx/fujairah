﻿using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Service.Logic.Enum;
using EService.Web.Helper;
using EService.Web.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using EService.Web.Controllers.Base;
using EService.Web.Models.ServiceRequestOperation;
using Nelibur.ObjectMapper;
using System.Collections.Generic;
using EService.Core;
using EService.Web.Helper.EmailSender;

namespace EService.Web.Controllers
{
    public class AircraftDismantlingRequestController : ServiceRequestBaseController
    {
        private AircraftDismantlingNOCRequestService aircraftDismantlingNOCRequestService;
        private ServiceApprovalStageService serviceApprovalStageService;
        private UserServiceRequestMasterService userRequestService;
        private ServiceInfoService serviceInfoService;
        private UserService userService;


        public AircraftDismantlingRequestController()
        {
            aircraftDismantlingNOCRequestService = new AircraftDismantlingNOCRequestService(serviceContext);
            serviceApprovalStageService = new ServiceApprovalStageService(serviceContext);
            userRequestService = new UserServiceRequestMasterService(serviceContext);
            serviceInfoService = new ServiceInfoService(serviceContext);
            userService = new UserService(serviceContext);
        }

        public override ServiceEnum Service => ServiceEnum.AircraftDismantlingRequest;

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(AircraftDismantlingNOCRequestViewModel model, bool isDraft = false)
        {
            var result = new JsonResponseObject();
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.UserServiceRequestMasterId > 0)
                    {
                        var contract = MapViewModelToContract(model);
                        var attachmentList = new List<UserServiceRequestAttachment>();
                        #region "Uploaded Files Edit"
                        if (model.EditUploadedFiles != null && model.EditUploadedFiles.Any())
                        {
                            attachmentList.AddRange(model.EditUploadedFiles.Where(o => o.ObjectState != ObjectState.Deleted).Select(o => new UserServiceRequestAttachment { UserAttachmentId = o.Id }));

                        }
                        if (model.UploadedFiles != null && model.UploadedFiles.Any())
                        {
                            attachmentList.AddRange(model.UploadedFiles.Select(o => new UserServiceRequestAttachment { UserAttachmentId = o.Id }));

                        }

                        #endregion
                        var updateResult = aircraftDismantlingNOCRequestService.UpdateAircraftDismantlingNOCRequestService(contract, isDraft, attachmentList);
                        result.IsSuccess = updateResult.IsSuccess;
                        result.Message = updateResult.Message;

                        if (!isDraft && updateResult.IsSuccess)
                        {
                            EmailSenderHelper.SendNewRequestEmail(ControllerContext, model.UserServiceRequestMasterId);
                            //TriggerEmail(FIAEmailType.New, model.UserServiceRequestMasterId);
                        }
                    }
                    else
                    {
                        var userServiceRequestmaster = SaveAircraftDismantleNOCRequest(model, isDraft);
                        if (!isDraft)
                        {
                            //TriggerEmail(FIAEmailType.New, userServiceRequestmaster.UserServiceRequestMasterId);
                            EmailSenderHelper.SendNewRequestEmail(ControllerContext, userServiceRequestmaster.UserServiceRequestMasterId);
                        }
                        result.IsSuccess = true;
                        result.Message = "Aircraft Dismantle NOC request saved successfully";
                    }
                }
                else
                {
                    result.Message = "Server Validation Error: " + ModelState.GetModelStateError();
                    result.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving Aircraft Dismantle NOC request.";
            }
            return Json(result);
        }

        public ActionResult Detail(int id)
        {
            ViewBag.UserServiceRequestMasterId = id;
            return View();
        }

        public ActionResult Edit(int id)
        {
            ViewBag.UserServiceRequestMasterId = id;
            return View("Create");
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Initialize()
        {
            var result = new JsonResponseObject();
            try
            {
                var data = userRequestService.GetServiceRequests(UserProfile.UserId, ServiceId);
                result.Data = data;
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while obtaining list";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InitializeCreate(int? id)
        {
            try
            {
                var model = new AircraftDismantlingNOCRequestViewModel()
                {
                    DateOfApplication = DateTime.Now.DubaiTime(),

                };
                var reapplyData = TempData[Constants.ServiceRequestReapplyContentTempDataKey] as ServiceReapplyTempDataViewModel;
                if (reapplyData != null)
                {
                    var userServiceRequest =
                        userRequestService.Find(
                            o => o.UserServiceRequestMasterId == reapplyData.UserServiceRequestMasterId &&
                                 !o.IsDeleted && o.UserId == UserProfile.UserId && o.ServiceId == ServiceId);

                    if (userServiceRequest == null || userServiceRequest.AircraftDismantlingNOCRequest == null)
                    {
                        return Json(new JsonResponseObject() { IsSuccess = false, Message = "Invalid User Service Request for reapply." });
                    }
                    else
                    {
                        var dismantlingNOCRequest = userServiceRequest.AircraftDismantlingNOCRequest;


                        TinyMapper.Bind<AircraftDismantlingNOCRequest, AircraftDismantlingNOCRequestViewModel>();

                        var dismantlingNOCRequestViewModel = TinyMapper.Map<AircraftDismantlingNOCRequestViewModel>(dismantlingNOCRequest);

                        dismantlingNOCRequestViewModel.ParentUserServiceRequestMasterid = reapplyData.UserServiceRequestMasterId;
                        dismantlingNOCRequestViewModel.UserServiceRequestMasterId = 0;

                        model = dismantlingNOCRequestViewModel;

                    }
                }

                else if (id.HasValue)
                {
                    var userServiceRequest =
                      userRequestService.Find(
                          o => o.UserServiceRequestMasterId == id &&
                               !o.IsDeleted && o.IsDraft && o.UserId == UserProfile.UserId && o.ServiceId == ServiceId);

                    if (userServiceRequest == null || userServiceRequest.AircraftDismantlingNOCRequest == null)
                    {
                        return Json(new JsonResponseObject() { IsSuccess = false, Message = "Invalid User Service Request for reapply." });
                    }
                    else
                    {
                        var dismantlingNOCRequest = userServiceRequest.AircraftDismantlingNOCRequest;


                        TinyMapper.Bind<AircraftDismantlingNOCRequest, AircraftDismantlingNOCRequestViewModel>();

                        var dismantlingNOCRequestViewModel = TinyMapper.Map<AircraftDismantlingNOCRequestViewModel>(dismantlingNOCRequest);


                        model = dismantlingNOCRequestViewModel;

                    }
                }
                model.UserInfo = userService.GetUserBasicInfo(UserProfile.UserId).Data;
                model.EditUploadedFiles = GetUploadedFiles(reapplyData?.UserServiceRequestMasterId ?? (id ?? 0));
                return Json(new JsonResponseObject() { IsSuccess = true, Data = model }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return Json(new JsonResponseObject() { IsSuccess = false, Message = "Error" });
        }

        public JsonResult InitializeDetail(int id)
        {
            try
            {
                var model = GetAircraftDismantleNOCRequest(id);
                return Json(new JsonResponseObject() { IsSuccess = true, Data = model }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return Json(new JsonResponseObject() { IsSuccess = false, Message = "Error" });
        }

        public ActionResult Reapply()
        {
            return View("Create");
        }

        #region "Private Methods"

        public AircraftDismantlingNOCRequestViewModel GetAircraftDismantleNOCRequest(int id)
        {
            var contract = aircraftDismantlingNOCRequestService.Find(x => x.UserServiceRequestMasterId == id);
            var model = new AircraftDismantlingNOCRequestViewModel
            {
                Address = contract.Address,
                UserServiceRequestMasterId = contract.UserServiceRequestMasterId,
                AircraftMSNAndRegistrationNumber = contract.AircraftMSNAndRegistrationNumber,
                AircraftTypeAndModelToDismantle = contract.AircraftTypeAndModelToDismantle,
                ApplicantCompanyName = contract.ApplicantCompanyName,
                CertificateOfDeRegistrationRefNumber = contract.CertificateOfDeRegistrationRefNumber,
                CurrentOwnerOfAircraft = contract.CurrentOwnerOfAircraft,
                DateOfApplication = contract.DateOfApplication,
                DateOfExpiryOfFANRLicense = contract.DateOfExpiryOfFANRLicense,
                DateOfExpiryOfFujairahEPDLicense = contract.DateOfExpiryOfFujairahEPDLicense,
                DateOfExpiryOfTradeLicense = contract.DateOfExpiryOfTradeLicense,
                DateOfIssueOfFANRLicense = contract.DateOfIssueOfFANRLicense,
                DateOfIssueOfFujairahEPDLicense = contract.DateOfIssueOfFujairahEPDLicense,
                DateOfIssueOfTradeLicense = contract.DateOfIssueOfTradeLicense,
                Email = contract.Email,
                FANRLicenseRefNumber = contract.FANRLicenseRefNumber,
                FujairahEPDLicenseRefNumber = contract.FujairahEPDLicenseRefNumber,
                GCAAApprovalRefNumber = contract.GCAAApprovalRefNumber,
                HasGCAAApproval = contract.HasGCAAApproval,
                MethodOfDismantlingAndDestruction = contract.MethodOfDismantlingAndDestruction,
                NameOfApplicant = contract.NameOfApplicant,
                OperatorOrAgentWhoBroughtTheAircraft = contract.OperatorOrAgentWhoBroughtTheAircraft,
                PhoneNumber = contract.PhoneNumber,
                PreviousOwnerOfAircraft = contract.PreviousOwnerOfAircraft,
                TradeLicenseRefNumber = contract.TradeLicenseRefNumber,
                WithNoObjectionFromTheAircraftOwner = contract.WithNoObjectionFromTheAircraftOwner,
                //UploadedFiles = contract.UserServiceRequestMaster.UserServiceRequestAttachments.Select(o => new FileUploadedViewModel { ContentType = o.UserAttachment.ContentType, FileName = o.UserAttachment.FileName, Id = o.UserAttachment.UserAttachmentId }).ToList()
            };
            return model;
        }

        public UserServiceRequestMaster SaveAircraftDismantleNOCRequest(AircraftDismantlingNOCRequestViewModel model, bool isDraft = false)
        {
            var serviceFlowStages = serviceApprovalStageService.FindAllAsNoTracking(o => o.ServiceId == ServiceId).ToList();
            var serviceCodeModal = userRequestService.GetNewServiceCode(ServiceId);
            var serviceInfo = serviceInfoService.Find(o => o.ServiceId == ServiceId);
            var userServiceRequest = new UserServiceRequestMaster()
            {
                RequestDateTime = DateTime.Now.DubaiTime(),
                ServiceId = ServiceId,
                Status = (int)ServiceFlowStateEnum.New,
                UserId = UserProfile.UserId,
                IsDeleted = false,
                RegistrationNoYear = serviceCodeModal.Data.Year,
                RegistrationNoMonth = serviceCodeModal.Data.Month,
                RegistrationNoIndex = serviceCodeModal.Data.RegistrationNoIndex,
                RegistrationNoServiceCode = serviceCodeModal.Data.ServiceCode,
                RegistrationNo = serviceCodeModal.Data.RegistrationNo,
                ParentUserServiceRequestMasterId = model.ParentUserServiceRequestMasterid,
                IsDraft = isDraft,
                VAT = serviceInfo.VATPercentage,
                ServiceRequestFlowStages = serviceFlowStages
                .Select(o => new ServiceRequestFlowStage()
                {
                    Index = o.Index,
                    IsCurrentStage = o.Index == 1,
                    ServiceRequestFlowStageDepartments = o.ServiceApprovalStageDepartments.Select(p =>
                    new ServiceRequestFlowStageDepartment()
                    {
                        DepartmentId = p.DepartmentId,
                        Status = (int)ServiceFlowStateEnum.New,
                        IsApproved = false,
                    }).ToList()
                }).ToList(),
                UserServiceRequestAttachments = model.UploadedFiles.Select(o => new UserServiceRequestAttachment()
                {
                    UserAttachmentId = o.Id
                }).ToList(),
                ServieBill = new UserServiceRequestBill()
                {
                    ChargedAmount = (double)serviceInfo.DefaultRate
                }
            };
            var request = MapViewModelToContract(model);
            request.UserServiceRequestMaster = userServiceRequest;
            aircraftDismantlingNOCRequestService.Add(request);
            serviceContext.Commit();
            return userServiceRequest;
        }

        private AircraftDismantlingNOCRequest MapViewModelToContract(AircraftDismantlingNOCRequestViewModel model)
        {
            var contract = new AircraftDismantlingNOCRequest
            {
                Address = model.Address,
                AircraftMSNAndRegistrationNumber = model.AircraftMSNAndRegistrationNumber,
                OldRegistrationNo = model.OldRegistrationNo,
                AircraftTypeAndModelToDismantle = model.AircraftTypeAndModelToDismantle,
                ApplicantCompanyName = model.ApplicantCompanyName,
                CertificateOfDeRegistrationRefNumber = model.CertificateOfDeRegistrationRefNumber,
                CurrentOwnerOfAircraft = model.CurrentOwnerOfAircraft,
                DateOfApplication = model.DateOfApplication,
                DateOfExpiryOfFANRLicense = model.DateOfExpiryOfFANRLicense,
                DateOfExpiryOfFujairahEPDLicense = model.DateOfExpiryOfFujairahEPDLicense,
                DateOfExpiryOfTradeLicense = model.DateOfExpiryOfTradeLicense,
                DateOfIssueOfFANRLicense = model.DateOfIssueOfFANRLicense,
                DateOfIssueOfFujairahEPDLicense = model.DateOfIssueOfFujairahEPDLicense,
                DateOfIssueOfTradeLicense = model.DateOfIssueOfTradeLicense,
                Email = model.Email,
                FANRLicenseRefNumber = model.FANRLicenseRefNumber,
                FujairahEPDLicenseRefNumber = model.FujairahEPDLicenseRefNumber,
                GCAAApprovalRefNumber = model.GCAAApprovalRefNumber,
                HasGCAAApproval = model.HasGCAAApproval.Value,
                MethodOfDismantlingAndDestruction = model.MethodOfDismantlingAndDestruction,
                NameOfApplicant = model.NameOfApplicant,
                OperatorOrAgentWhoBroughtTheAircraft = model.OperatorOrAgentWhoBroughtTheAircraft,
                PhoneNumber = model.PhoneNumber,
                PreviousOwnerOfAircraft = model.PreviousOwnerOfAircraft,
                TradeLicenseRefNumber = model.TradeLicenseRefNumber,
                UserServiceRequestMasterId = model.UserServiceRequestMasterId,
                WithNoObjectionFromTheAircraftOwner = model.WithNoObjectionFromTheAircraftOwner,

                //UserServiceRequestMaster = userServiceRequest
            };

            return contract;
        }

        #endregion "Private Methods"
    }
}