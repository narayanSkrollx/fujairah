﻿namespace EService.Service.Model
{
    public class UserBasicInfoServiceModal
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string CompanyCategory { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string POBox { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
    }
}