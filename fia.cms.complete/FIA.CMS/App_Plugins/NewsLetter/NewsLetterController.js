﻿angular.module("umbraco").controller("newsletter.template",
    function ($scope, $http, notificationsService) {
        $scope.model = {};
        $scope.body = {
            label: 'bodyText',
            description: 'Load some stuff here',
            view: 'rte',
            config: {
                editor: {
                    toolbar: ["code", "undo", "redo", "cut", "styleselect", "bold", "italic", "alignleft", "aligncenter", "alignright", "bullist", "numlist", "link", "umbmediapicker", "umbmacro", "table", "umbembeddialog"],
                    stylesheets: [],
                    dimensions: { height: 400 }
                }
            }
        };

        $scope.save = function () {
            var data = {
                subject: $scope.model.subject,
                body:$scope.body.value
            }
            $http.post("/umbraco/surface/Newsletter/SaveNewsletterTemplate", angular.toJson(data)).then(function () {
                notificationsService.success("Success", data.subject);
            });
        }
        $scope.sendNewsLetterEmail = function () {
            $http.post("/umbraco/surface/Newsletter/SendNewsLetterEmail", null).then(function () {
                notificationsService.success("Success", "Successfully send email!");
            });
        }
        
        $scope.getNewsletterTemplate = function() {
            $http.get("/umbraco/surface/Newsletter/GetNewsletterTemplate").then(function(result) {
                if (result && result.data) {
                    $scope.body.value = result.data.Body;
                    $scope.model.subject = result.data.Subject;
                }
            });
        }
        $scope.getNewsletterTemplate();
    });