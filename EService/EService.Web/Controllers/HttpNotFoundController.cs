﻿using System.Web.Mvc;
using EService.Web.Controllers.Base;

namespace EService.Web.Controllers
{
    public class HttpNotFoundController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}