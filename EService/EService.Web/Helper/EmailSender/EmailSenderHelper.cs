﻿using EService.Service.Logic;
using EService.Web.Models.ServiceNotification;
using System;
using System.IO;
using System.Web.Mvc;

namespace EService.Web.Helper.EmailSender
{
    public class EmailSenderHelper
    {
        private static EmailService emailService;
        private static ServiceContext serviceContext;
        private static SMTPEmailSettingService emailSettingService;
        private static UserServiceRequestMasterService userServiceRequestMasterService;

        private static string completedEmailTemplate = "";

        static EmailSenderHelper()
        {
            serviceContext = new ServiceContext();
            emailService = new EmailService(serviceContext);
            emailSettingService = new SMTPEmailSettingService(serviceContext);
            userServiceRequestMasterService = new UserServiceRequestMasterService(serviceContext);
        }

        public static void SendCompletedEmail(ControllerContext controllerContext, string regNo)
        {
            //var userServiceRequest = userServiceRequestMasterService.GetServiceRequest(userServiceRequestMasterId);
            var smtpSetting = emailSettingService.Find(o => true);
            //var receipients = emailService.GetApproveRejectEmailRecepients(serviceFlowRequestDepartmentId);
            var request =
             userServiceRequestMasterService.Find(
                 o => o.RegistrationNo == regNo);
            var htmlContent = "";

            var aModel = new NOCCollectionNotificationViewModel()
            {
                RequestRegistrationNo = request.RegistrationNo
            };
            String[] receipients = new string[] { request.User.Email };

            htmlContent = RenderPartialView(controllerContext, "_NoticeForNOCCollectionEmailTemplate", aModel);

            EmailHelper.SendEmail(smtpSetting.From, receipients, new string[] { }, new string[] { }, "NOC Collection", htmlContent, smtpSetting);
        }

        public static void SendNewRequestEmail(ControllerContext controllerContext, int userServiceRequestMasterId)
        {
            try
            {
                var smtpSetting = emailSettingService.Find(o => true);
                //var reciepients =
                //    emailService.GetApproveRejectEmailRecepientsByUserServiceRequestId(userServiceRequestMasterId);
                var userInfo = emailService.GetUserInfo(userServiceRequestMasterId);
                var userServiceRequestmaster =
                    userServiceRequestMasterService.Find(o => o.UserServiceRequestMasterId == userServiceRequestMasterId);

                var newEmailModel = new NewServiceRequestNotificationViewModel()
                {
                    ServiceId = userServiceRequestmaster.ServiceId,
                    ServiceName = userServiceRequestmaster.Service.ServiceName,
                    RequestedByUserName = userInfo.UserName,
                    RequestRegistrationNo = userServiceRequestmaster.RegistrationNo,
                    RequestedByUserEmail = userInfo.Email,
                    RequestedByUserId = userServiceRequestmaster.UserId,
                    RequestedDateTime = userServiceRequestmaster.RequestDateTime
                };

                var emailContent = RenderPartialView(controllerContext, "_NewServiceRequestEmailTemplate", newEmailModel);
                EmailHelper.SendEmail(smtpSetting.From, new string[] { userInfo.Email }, new string[] { }, new string[] { }, "New Service Request", emailContent, smtpSetting);
            }
            catch (Exception ex)
            {

            }
        }

        protected static string RenderPartialView(ControllerContext controllerContext, string partialViewName, object model = null)
        {
            if (controllerContext == null)
                return string.Empty;

            if (string.IsNullOrEmpty(partialViewName))
                throw new ArgumentNullException("partialViewName");
            //ModelState.Clear();//Remove possible model binding error.

            controllerContext.Controller.ViewData.Model = model;//Set the model to the partial view

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controllerContext, partialViewName);
                var viewContext = new ViewContext(controllerContext, viewResult.View, controllerContext.Controller.ViewData, controllerContext.Controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }

        public static void SendApprovedEmail(int serviceRequestMasterid)
        {

        }

        public static void SendRejectedEmail(int serviceRequestMasterId)
        {

        }

    }
}