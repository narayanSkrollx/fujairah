using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class LandingPermissionRequestAttachment
    {
        public int LandingPermissionRequestAttachmentId { get; set; }
        public int LandingPermissionRequestId { get; set; }
        public int UserAttachmentId { get; set; }

        [JsonIgnore]
        public LandingPermissionRequest LandingPermissionRequest { get; set; }

        [JsonIgnore]
        public UserAttachment UserAttachment { get; set; }
    }
}