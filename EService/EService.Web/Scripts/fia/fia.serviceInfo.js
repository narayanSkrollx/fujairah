﻿$(function () {
    fia.CreateNamespace("serviceInfo");

    (function (context) {
        context.Title = 'Service Info';
        context.ViewModel = {}

        context.Mapping = {
            create:function(options) {
                var vm = ko.mapping.fromJS(options.data);

                vm.CurrentServiceVM = ko.mapping.fromJS(ko.toJS(vm.AddNewModel));

                vm.AddNewClick = function() {
                    ko.mapping.fromJS(ko.toJS(vm.AddNewModel), {}, vm.CurrentServiceVM);
                    $('#addServiceInfoModal').modal({ backdrop: 'static', keyboard: false, show: true });
                }

                vm.EditClick = function (item) {
                    ko.mapping.fromJS(item, {}, vm.CurrentServiceVM);
                    $('#addServiceInfoModal').modal({ backdrop: 'static', keyboard: false, show: true });
                }
                vm.EditDescriptionClick = function (item) {
                    window.location = '/Configure/Service/Description/' + item.ServiceId();;
                }

                vm.Save = function() {
                    context.Save();
                }

                return vm;
            }
        }

        context.Save = function() {
            if (!$('#addServiceInfoForm').valid()) {
                return false;
            }
            var model = ko.toJS(context.ViewModel.ServiceInfoVM.CurrentServiceVM);
            ajaxRequest('/Configure/Service/Create', 'POST', {
                data: { model: model },
                enableLadda: true,
                targetLaddaElement: '#btnSave',
            }, function (resp) {
                if (resp.IsSuccess) {
                    showMessage(resp.Message, context.Title, 'success', function () {
                        $('#addServiceInfoModal').modal('hide');
                    });
                    context.Initialize();
                } else {
                    showMessage(resp.Message, context.Title, 'error');
                }
            });
        }

        context.ApplyValidation = function() {
            $('#addServiceInfoForm').validate({
                rules: {
                    ServiceName: {
                        required: true,
                        maxlength: 255
                    },
                    ServiceCode: {
                        required: true,
                        maxlength: 10
                    },
                    DefaultRate: {
                        required: true
                    },
                    VATPercentage: {
                        required: true
                    },
                    Description: {
                        maxlength: 1000
                    }
                },
                messages: {
                    ServiceName: {
                        required: 'Service Name isrequired',
                        maxlength: 'Service Name legnth cannot be greater than 255'
                    },
                    ServiceCode: {
                        required: 'Service Code is required',
                        maxlength: 'Service Code length cannot be greater than 10'
                    },
                    DefaultRate: {
                        required: 'Default Rate is required'
                    },
                    VATPercentage: {
                        required: 'Default Rate is required'
                    },
                    Description: {
                        maxlength: 'Description length cannot be greater than 1000.'
                    }
                }
            });
        }

        context.Initialize = function () {
            ajaxRequest('/Service/Initialize/', 'GET', {}, function (response) {
                if (response.IsSuccess) {
                    if (!ko.dataFor($('#mainContent')[0])) {
                        context.ViewModel.ServiceInfoVM = ko.mapping.fromJS(response.Data, context.Mapping);

                        ko.applyBindings(context.ViewModel.ServiceInfoVM, $('#mainContent')[0]);
                        context.ApplyValidation();
                    } else {
                        ko.mapping.fromJS(response.Data, {}, context.ViewModel.ServiceInfoVM);
                    }
                } else {
                    showMessage(context.Title, response.Message, 'error');
                }
            });
        }

    })(fia.serviceInfo);
});