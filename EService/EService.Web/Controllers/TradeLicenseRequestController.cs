﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EService.Data.Entity.Entity;
using EService.Service.Logic;
using EService.Service.Logic.Enum;
using EService.Web.Controllers.Base;
using EService.Web.Helper;
using EService.Web.Models;
using EService.Web.Models.ServiceRequestOperation;
using Nelibur.ObjectMapper;
using EService.Core;
using EService.Web.Helper.EmailSender;

namespace EService.Web.Controllers
{
    public class TradeLicenseRequestController : ServiceRequestBaseController
    {
        private TradeLicenseRequestService tradeLicenseRequestService;
        private ServiceApprovalStageService serviceApprovalStageService;
        private UserServiceRequestMasterService userRequestService;
        private StaffStrengthService staffStrengthService;
        private CurrencyService currencyService;
        private ServiceInfoService serviceInfoService;
        private UserService userService;

        public TradeLicenseRequestController()
        {
            staffStrengthService = new StaffStrengthService(serviceContext);
            serviceApprovalStageService = new ServiceApprovalStageService(serviceContext);
            userRequestService = new UserServiceRequestMasterService(serviceContext);
            tradeLicenseRequestService = new TradeLicenseRequestService(serviceContext);
            currencyService = new CurrencyService(serviceContext);
            serviceInfoService = new ServiceInfoService(serviceContext);
            userService = new UserService(serviceContext);
        }

        public override ServiceEnum Service => ServiceEnum.TradeLicenseRequest;

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(TradeLicenseRequestViewModel model, bool isDraft = false)
        {
            var result = new JsonResponseObject();
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.UserServiceRequestMasterId > 0)
                    {
                        var contract = MapViewModelToContract(model);
                        var attachmentList = new List<UserServiceRequestAttachment>();
                        #region "Uploaded Files Edit"
                        if (model.EditUploadedFiles != null && model.EditUploadedFiles.Any())
                        {
                            attachmentList.AddRange(model.EditUploadedFiles.Where(o => o.ObjectState != ObjectState.Deleted).Select(o => new UserServiceRequestAttachment { UserAttachmentId = o.Id }));

                        }
                        if (model.UploadedFiles != null && model.UploadedFiles.Any())
                        {
                            attachmentList.AddRange(model.UploadedFiles.Select(o => new UserServiceRequestAttachment { UserAttachmentId = o.Id }));

                        }

                        #endregion
                        var updateResult = tradeLicenseRequestService.UpdateTradeLicenseRequest(contract, isDraft, attachmentList);
                        result.IsSuccess = updateResult.IsSuccess;
                        result.Message = updateResult.Message;

                        if (!isDraft && updateResult.IsSuccess)
                        {
                            //TriggerEmail(FIAEmailType.New, model.UserServiceRequestMasterId);
                            EmailSenderHelper.SendNewRequestEmail(ControllerContext, model.UserServiceRequestMasterId);
                        }
                    }
                    else
                    {
                        var userServiceRequest = SaveTradeLicenseRequest(model, isDraft);
                        if (!isDraft)
                        {
                            //TriggerEmail(FIAEmailType.New, userServiceRequest.UserServiceRequestMasterId);
                            EmailSenderHelper.SendNewRequestEmail(ControllerContext, userServiceRequest.UserServiceRequestMasterId);
                        }
                        result.IsSuccess = true;
                        result.Message = "Trade LicenseS Request saved successfully";
                    }
                }
                else
                {
                    result.Message = "Server Validation Error: " + ModelState.GetModelStateError();
                    result.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while saving Trade License Request.";
            }
            return Json(result);
        }

        public ActionResult Detail(int id)
        {
            ViewBag.UserServiceRequestMasterId = id;
            return View();
        }

        public ActionResult Edit(int id)
        {
            ViewBag.UserServiceRequestMasterId = id;
            return View("Create");
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Initialize()
        {
            var result = new JsonResponseObject();
            try
            {
                var data = userRequestService.GetServiceRequests(UserProfile.UserId, ServiceId);
                result.Data = data;
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                LogError(ex);
                result.IsSuccess = false;
                result.Message = "Error while obtaining list";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InitializeCreate(int? id)
        {
            try
            {
                var model = new TradeLicenseRequestViewModel();
                if (id.HasValue)
                {
                    model = GetTradeLicenseRequest(id.Value);
                    model.SelectedStaffStrengths = (from a in staffStrengthService.FindAll(x => !x.IsDeleted).ToList()
                                                    join b in model.SelectedStaffStrengths on a.StaffStrengthId equals b.Id into ab
                                                    from b in ab.DefaultIfEmpty()
                                                    select new SelectListModel
                                                    {
                                                        Id = a.StaffStrengthId,
                                                        IsSelected = b != null,
                                                        Description = a.StaffStrengthName
                                                    }
                    ).ToList();
                    model.UserInfo = userService.GetUserBasicInfo(UserProfile.UserId).Data;
                    model.EditUploadedFiles = GetUploadedFiles((id ?? 0));
                }
                else
                {
                    var reapplyData =
                        TempData[Constants.ServiceRequestReapplyContentTempDataKey] as
                            ServiceReapplyTempDataViewModel;
                    if (reapplyData != null)
                    {
                        var userServiceRequest =
                            userRequestService.Find(
                                o => o.UserServiceRequestMasterId == reapplyData.UserServiceRequestMasterId &&
                                     !o.IsDeleted && o.UserId == UserProfile.UserId && o.ServiceId == ServiceId);

                        if (userServiceRequest == null || userServiceRequest.TradeLicenseRequest == null)
                        {
                            return Json(new JsonResponseObject()
                            {
                                IsSuccess = false,
                                Message = "Invalid User Service Request for reapply."
                            }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            var tradeLicenseRequest = userServiceRequest.TradeLicenseRequest;


                            TinyMapper.Bind<TradeLicenseRequest, TradeLicenseRequestViewModel>();

                            var tradeLicenseRequestViewModel =
                                TinyMapper.Map<TradeLicenseRequestViewModel>(tradeLicenseRequest);

                            tradeLicenseRequestViewModel.ParentUserServiceRequestMasterid = reapplyData.UserServiceRequestMasterId;
                            tradeLicenseRequestViewModel.UserServiceRequestMasterId = 0;

                            tradeLicenseRequestViewModel.SelectedStaffStrengths = staffStrengthService.FindAll(x => !x.IsDeleted).Select(
                                x => new SelectListModel()
                                {
                                    Id = x.StaffStrengthId,
                                    IsSelected = false,
                                    Description = x.StaffStrengthName
                                }).ToList();

                            tradeLicenseRequestViewModel.Currencies = currencyService.FindAllAsNoTracking(x => !x.IsDeleted).Select(
                                x => new SelectListModel()
                                {
                                    Id = x.CurrencyId,
                                    Description = x.CurrencyName
                                }).ToList();

                            model = tradeLicenseRequestViewModel;
                        }
                    }
                    model.UserInfo = userService.GetUserBasicInfo(UserProfile.UserId).Data;
                    model.EditUploadedFiles = GetUploadedFiles(reapplyData?.UserServiceRequestMasterId ?? (id ?? 0));

                }
                model.ApplicationDate = DateTime.Now.DubaiTime();
                return Json(new JsonResponseObject() { IsSuccess = true, Data = model },
                     JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogError(ex);
                return Json(new JsonResponseObject() { IsSuccess = false, Message = "Error while initializing model." }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult InitializeDetail(int id)
        {
            try
            {
                var model = GetTradeLicenseRequest(id);
                return Json(new JsonResponseObject() { IsSuccess = true, Data = model }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return Json(new JsonResponseObject() { IsSuccess = false, Message = "Error" });
        }

        public ActionResult Reapply()
        {
            return View("Create");
        }

        #region "Private Methods"

        private TradeLicenseRequestViewModel GetTradeLicenseRequest(int id)
        {
            var contract = tradeLicenseRequestService.Find(x => x.UserServiceRequestMasterId == id);
            var model = MapContractToViewModel(contract);
            return model;
        }

        private UserServiceRequestMaster SaveTradeLicenseRequest(TradeLicenseRequestViewModel model, bool isDraft = false)
        {
            var serviceFlowStages = serviceApprovalStageService.FindAllAsNoTracking(o => o.ServiceId == ServiceId).ToList();
            var serviceModal = userRequestService.GetNewServiceCode(ServiceId);
            var serviceInfo = serviceInfoService.Find(o => o.ServiceId == ServiceId);
            var userServiceRequest = new UserServiceRequestMaster()
            {
                RequestDateTime = DateTime.Now.DubaiTime(),
                ServiceId = ServiceId,
                Status = (int)ServiceFlowStateEnum.New,
                UserId = UserProfile.UserId,
                IsDeleted = false,
                IsDraft = isDraft,
                VAT = serviceInfo.VATPercentage,
                ServiceRequestFlowStages = serviceFlowStages
                    .Select(o => new ServiceRequestFlowStage()
                    {
                        Index = o.Index,
                        IsCurrentStage = o.Index == 1,
                        ServiceRequestFlowStageDepartments = o.ServiceApprovalStageDepartments.Select(p =>
                            new ServiceRequestFlowStageDepartment()
                            {
                                DepartmentId = p.DepartmentId,
                                Status = (int)ServiceFlowStateEnum.New,
                                IsApproved = false,
                            }).ToList()
                    }).ToList(),
                RegistrationNoYear = serviceModal.Data.Year,
                RegistrationNoMonth = serviceModal.Data.Month,
                RegistrationNoIndex = serviceModal.Data.RegistrationNoIndex,
                RegistrationNoServiceCode = serviceModal.Data.ServiceCode,
                ParentUserServiceRequestMasterId = model.ParentUserServiceRequestMasterid,
                RegistrationNo = serviceModal.Data.RegistrationNo,
                UserServiceRequestAttachments = model.UploadedFiles.Select(o => new UserServiceRequestAttachment()
                {
                    UserAttachmentId = o.Id
                }).ToList(),
                ServieBill = new UserServiceRequestBill()
                {
                    ChargedAmount = (double)serviceInfo.DefaultRate
                }
            };
            var request = MapViewModelToContract(model);
            request.UserServiceRequestMaster = userServiceRequest;
            tradeLicenseRequestService.Add(request);
            serviceContext.Commit();
            return userServiceRequest;
        }

        private TradeLicenseRequestViewModel MapContractToViewModel(TradeLicenseRequest contract)
        {
            var model = new TradeLicenseRequestViewModel
            {
                UserServiceRequestMasterId = contract.UserServiceRequestMasterId,
                ApplicantName = contract.ApplicantName,
                PresentSponsorInUAE = contract.PresentSponsorInUAE,
                AddressOfSponser = contract.AddressOfSponser,
                ApplicantOfficeNumber = contract.ApplicantOfficeNumber,
                ApplicantResidenceNumber = contract.ApplicantResidenceNumber,
                ApplicantMobileNumber = contract.ApplicantMobileNumber,
                ApplicantFaxNumber = contract.ApplicantFaxNumber,
                ApplicantEmailNumber = contract.ApplicantEmailNumber,
                ApplicantCountry = contract.ApplicantCountry,
                MailingAddress = contract.MailingAddress,
                ApplicationDate = contract.ApplicationDate,
                CompanyPreferredName = contract.CompanyPreferredName,
                CompanyNameOption1 = contract.CompanyNameOption1,
                CompanyNameOption2 = contract.CompanyNameOption2,
                CompanyNameOption3 = contract.CompanyNameOption3,
                BusinessActivities1 = contract.BusinessActivities1,
                BusinessActivities2 = contract.BusinessActivities2,
                BusinessActivities3 = contract.BusinessActivities3,
                Remarks = contract.Remarks,
                ExpectedAnnualBusinessTrunoverAmount = contract.ExpectedAnnualBusinessTrunoverAmount,
                ExpectedAnnualBusinessTrunoverCurrencyId = contract.ExpectedAnnualBusinessTrunoverCurrencyId,
                CurrencyDisplay = contract.Currency?.CurrencyName,
                TotalStaffStrengthRequired = contract.TotalStaffStrengthRequired,
                SelectedStaffStrengths = contract.SelectedStaffStrengths
                    .Select(x => new SelectListModel()
                    {
                        Id = x.StaffStrengthId,
                        Description = x.StaffStrength.StaffStrengthName,
                        IsSelected = true
                    }).ToList()
            };
            return model;
        }

        private TradeLicenseRequest MapViewModelToContract(TradeLicenseRequestViewModel model)
        {
            var contract = new TradeLicenseRequest
            {
                UserServiceRequestMasterId = model.UserServiceRequestMasterId,
                ApplicantName = model.ApplicantName,
                PresentSponsorInUAE = model.PresentSponsorInUAE,
                AddressOfSponser = model.AddressOfSponser,
                ApplicantOfficeNumber = model.ApplicantOfficeNumber,
                ApplicantResidenceNumber = model.ApplicantResidenceNumber,
                ApplicantMobileNumber = model.ApplicantMobileNumber,
                ApplicantFaxNumber = model.ApplicantFaxNumber,
                ApplicantEmailNumber = model.ApplicantEmailNumber,
                ApplicantCountry = model.ApplicantCountry,
                MailingAddress = model.MailingAddress,
                ApplicationDate = model.ApplicationDate,
                CompanyPreferredName = model.CompanyPreferredName,
                CompanyNameOption1 = model.CompanyNameOption1,
                CompanyNameOption2 = model.CompanyNameOption2,
                CompanyNameOption3 = model.CompanyNameOption3,
                BusinessActivities1 = model.BusinessActivities1,
                BusinessActivities2 = model.BusinessActivities2,
                BusinessActivities3 = model.BusinessActivities3,
                Remarks = model.Remarks,
                ExpectedAnnualBusinessTrunoverAmount = model.ExpectedAnnualBusinessTrunoverAmount,
                ExpectedAnnualBusinessTrunoverCurrencyId = model.ExpectedAnnualBusinessTrunoverCurrencyId,
                TotalStaffStrengthRequired = model.TotalStaffStrengthRequired,
                SelectedStaffStrengths = model.SelectedStaffStrengths.Where(x => x.IsSelected)
                .Select(x => new TradeLicenseSelectedStaffStrength()
                {
                    StaffStrengthId = x.Id,
                    //TradeLicenseRequestId = x.TradeLicenseRequestId
                }).ToList()

            };
            return contract;
        }

        #endregion "Private Methods"
    }
}