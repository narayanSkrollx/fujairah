namespace EService.Data.Contracts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BHProjectTypeReference : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BuildingHeightRequest", "ProjectTypeId", c => c.Int(nullable: false, defaultValue: 1));
            AddColumn("dbo.BuildingHeightRequest", "OtherProjectTypeDescription", c => c.String(maxLength: 255));
            CreateIndex("dbo.BuildingHeightRequest", "ProjectTypeId");
            AddForeignKey("dbo.BuildingHeightRequest", "ProjectTypeId", "dbo.ProjectType", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BuildingHeightRequest", "ProjectTypeId", "dbo.ProjectType");
            DropIndex("dbo.BuildingHeightRequest", new[] { "ProjectTypeId" });
            DropColumn("dbo.BuildingHeightRequest", "OtherProjectTypeDescription");
            DropColumn("dbo.BuildingHeightRequest", "ProjectTypeId");
        }
    }
}
