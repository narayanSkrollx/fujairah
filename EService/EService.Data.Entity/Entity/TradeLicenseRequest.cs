﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class TradeLicenseRequest : AuditableEntity, IServiceEntity
    {
        public int UserServiceRequestMasterId { get; set; }

        #region "Details of the applicant who wants to register the company under DCA"

        public string ApplicantName { get; set; }
        public string PresentSponsorInUAE { get; set; }
        public string AddressOfSponser { get; set; }
        public string ApplicantOfficeNumber { get; set; }
        public string ApplicantResidenceNumber { get; set; }
        public string ApplicantMobileNumber { get; set; }
        public string ApplicantFaxNumber { get; set; }
        public string ApplicantEmailNumber { get; set; }
        public string ApplicantCountry { get; set; }
        public string MailingAddress { get; set; }
        public DateTime? ApplicationDate { get; set; }

        #endregion

        #region "Details of the proposed Company to sponsored under DCA"

        public string CompanyPreferredName { get; set; }
        public string CompanyNameOption1 { get; set; }
        public string CompanyNameOption2 { get; set; }
        public string CompanyNameOption3 { get; set; }
        public string BusinessActivities1 { get; set; }
        public string BusinessActivities2 { get; set; }
        public string BusinessActivities3 { get; set; }
        public string Remarks { get; set; }
        public double? ExpectedAnnualBusinessTrunoverAmount { get; set; }
        public int? ExpectedAnnualBusinessTrunoverCurrencyId { get; set; }
        public string TotalStaffStrengthRequired { get; set; }

        #endregion

        #region "Navigational Properties"

        [JsonIgnore]
        public virtual UserServiceRequestMaster UserServiceRequestMaster { get; set; }

        [JsonIgnore]
        public virtual Currency Currency { get; set; }

        [JsonIgnore]
        public virtual ICollection<TradeLicenseSelectedStaffStrength> SelectedStaffStrengths { get; set; }

        #endregion
    }
}
