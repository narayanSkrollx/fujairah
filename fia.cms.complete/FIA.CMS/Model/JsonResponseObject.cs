﻿namespace FIA.CMS.Model
{
    public class JsonResponseObject
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}