namespace EService.Data.Entity.Entity
{
    public class ServiceFlowStatus : ILookupItem
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
    }
}