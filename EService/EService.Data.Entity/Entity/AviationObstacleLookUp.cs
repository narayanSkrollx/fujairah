using System.Collections.Generic;
using Newtonsoft.Json;

namespace EService.Data.Entity.Entity
{
    public class AviationObstacleLookUp : ILookupItem
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }

        [JsonIgnore]
        public virtual ICollection<BuildingHeightAviationObstacle> BuildingHeightAviationObstacles { get; set; }
    }
}