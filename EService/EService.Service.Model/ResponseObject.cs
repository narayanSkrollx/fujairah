namespace EService.Service.Model
{
    public class ResponseObject<T>
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
    }

    public class ResponseObject : ResponseObject<object>
    {

    }
}