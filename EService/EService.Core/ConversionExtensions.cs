﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace EService.Core
{
    public static class ConversionExtensions
    {
        public static bool IsNull(this object obj)
        {
            return (obj == null || obj == DBNull.Value);
        }

        public static DateTime DubaiTime(this DateTime dateTime)
        {
            var timeZone = TimeZoneInfo.FindSystemTimeZoneById("Arabian Standard Time");
            DateTime userTime = TimeZoneInfo.ConvertTimeFromUtc(dateTime.ToUniversalTime(), timeZone);
            //return dateTime.ToUniversalTime().AddHours(4);
            return userTime;
        }

        public static bool ToBoolean(this object obj)
        {
            try
            {
                if (obj.IsNull())
                    return false;
                return Convert.ToBoolean(obj);
            }
            catch
            {
                return false;
            }
        }

        public static Decimal ToDecimal(this object obj)
        {
            if (obj.IsNull())
                return 0;
            decimal result = 0;
            return (decimal.TryParse(obj.ToString(), out result)) ? result : 0;
        }

        public static Dictionary<int, string> ToDictionary(this Enum @enum)
        {
            var type = @enum.GetType();
            return Enum.GetValues(type).Cast<int>().ToDictionary(e => e, e => Enum.GetName(type, e));
        }

        public static Double ToDouble(this object obj)
        {
            try
            {
                if (obj.IsNull())
                    return 0;
                return Convert.ToDouble(obj);
            }
            catch
            {
                return 0;
            }
        }

        public static int ToInt32(this object obj)
        {
            if (obj.IsNull())
                return 0;
            int result = 0;
            return (int.TryParse(obj.ToString(), out result)) ? result : 0;
        }

        public static bool? ToNBoolean(this object obj)
        {
            try
            {
                if (obj.IsNull())
                    return null;
                return obj.ToBoolean();
            }
            catch
            {
                return null;
            }
        }

        public static DateTime? ToNDateTime(this object obj)
        {
            try
            {
                if (obj.IsNull())
                    return null;

                var cInfo = new CultureInfo("en-US");
                var dInfo = new DateTimeFormatInfo
                {
                    ShortDatePattern = "mm/dd/yy"
                };
                cInfo.DateTimeFormat = dInfo;
                return Convert.ToDateTime(obj, cInfo);
            }
            catch
            {
                return null;
            }
        }

        public static Decimal? ToNDecimal(this object obj)
        {
            if (obj.IsNull())
                return null;
            try
            {
                return obj.ToDecimal();
            }
            catch
            {
                return null;
            }
        }

        public static Double? ToNDouble(this object obj)
        {
            try
            {
                if (obj.IsNull())
                    return null;
                return obj.ToDouble();
            }
            catch
            {
                return null;
            }
        }

        public static Int32? ToNInt32(this object o)
        {
            if (o == null || o == DBNull.Value)
            {
                return null;
            }
            int result;
            if (Int32.TryParse(o.ToText(), out result))
            {
                return result;
            }
            return null;
        }

        public static DateTime? ToNTime(this object obj)
        {
            try
            {
                if (obj.IsNull())
                    return null;

                var cInfo = new CultureInfo("en-US");
                return Convert.ToDateTime(obj, cInfo);
            }
            catch
            {
                return null;
            }
        }

        public static string ToProperCase(this string str)
        {
            str = str.ToText();
            return System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }

        public static string ToPropertyName<T>(this Expression<Func<T, object>> expression)
        {
            var body = expression.Body as MemberExpression ?? ((UnaryExpression)expression.Body).Operand as MemberExpression;
            if (body != null)
            {
                return body.Member.Name;
            }
            return string.Empty;
        }

        public static string ToText(this object o)
        {
            if (o == null || o == DBNull.Value)
            {
                return string.Empty;
            }
            return o.ToString();
        }

        public static string ToJson(this object o, bool camelCase = false)
        {
            if (camelCase)
            {
                var settings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver() //new LowercaseContractResolver()
                };
                return JsonConvert.SerializeObject(o, settings);
            }
            return JsonConvert.SerializeObject(o);
        }

        public static T DeserializeObject<T>(this string json) where T : class
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static byte[] ReadFully(this Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public static TResult GetMax<TSource, TResult>(this IQueryable<TSource> model,
                                                     Expression<Func<TSource, TResult>> selector)
          where TSource : class
        {
            if (model != null)
            {
                var count = model.Count();
                if (count > 0)
                {
                    return model.Max(selector);
                }
            }
            return default(TResult);
        }

        public static TResult GetMax<TSource, TResult>(this IEnumerable<TSource> model, Func<TSource, TResult> selector)
            where TSource : class
        {
            if (model != null)
            {
                var count = model.Count();
                if (count > 0)
                {
                    return model.Max(selector);
                }
            }
            return default(TResult);
        }

        public static TResult GetMin<TSource, TResult>(this IEnumerable<TSource> model, Func<TSource, TResult> selector)
        where TSource : class
        {
            if (model != null)
            {
                var count = model.Count();
                if (count > 0)
                {
                    return model.Min(selector);
                }
            }
            return default(TResult);
        }

        /// <summary>Adds a single element to the end of an IEnumerable.</summary>
        /// <typeparam name="T">Type of enumerable to return.</typeparam>
        /// <returns>IEnumerable containing all the input elements, followed by the
        /// specified additional element.</returns>
        public static IEnumerable<T> Append<T>(this IEnumerable<T> source, T element)
        {
            if (source == null)
                throw new ArgumentNullException("source");
            return ConcatIterator(element, source, false);
        }
        private static IEnumerable<T> ConcatIterator<T>(T extraElement,
    IEnumerable<T> source, bool insertAtStart)
        {
            if (insertAtStart)
                yield return extraElement;
            foreach (var e in source)
                yield return e;
            if (!insertAtStart)
                yield return extraElement;
        }

    }
}
